<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Banco Azteca | Sistematización de actividades</title>
        <link rel="stylesheet"
              href="../css/vistaCumplimientoTareas/estilo.css" />

        <link rel="stylesheet"
              href="../css/vistaReporte.css" />
        <meta name="viewport"
              content="width=device-width, initial-scale=1, maximum-scale=1">

        <script type="text/javascript">

        </script>


    <body onload="nobackbutton(); init();">
        <style>
            table.tblrangeSlider {
                color: #989898;
                width: 100%;
            }

            .tRight {
                text-align: right !important;
            }
            class

            ui-slider-handle



            ui-state-default



            ui-corner-all
        </style>



        <div class="header">
            <div class="logo">
                <a href="/migestion/"><img
                        src="../images/vistaCumplimientoTareas/logo.svg"></a>
            </div>
            <div class="title">
                <div class="box tleft">
                    <a onclick="regresaVistaTerrit(${porcentajeG},${banderaCecoPadre},${nivelPerfil},${idCecoPadre},${idCeco}, '${nombreCeco}',${seleccionAno},${seleccionMes},${seleccionCanal},${seleccionPais});" class="hom"><img
                            src="../images/vistaCumplimientoTareas/regresar.svg"
                            class="imgBack"></a> <a href="#" class="active">
                        <img src="../images/vistaCumplimientoTareas/rev01W.svg">
                    </a>
                    <!--  <a href="#" class=" "><img
                            src="../images/vistaCumplimientoTareas/rev02W.svg"></a>
                    <a href="#" class=" "><img
                            src="../images/vistaCumplimientoTareas/rev03W.svg"></a>
                    <a href="#" class=" "><img
                            src="../images/vistaCumplimientoTareas/rev04W.svg"></a>
                    <a href="#" class=" "><img
                            src="../images/vistaCumplimientoTareas/rev05W.svg"></a>-->
                </div>
            </div>
        </div>
        <div class="wrapper">

            <!--	<c:set var="item" scope="session" value="${listaCheck}" /> -->

            <div class="hspacer"></div>
            <div class="box" >
                <div class="titPiso">
                    <h2>
                        <div>TEXTO 1</div>
                        <div id="nombreSucursal">TEXTO 2</div>
                    </h2>
                </div>

                <table class="bloquer">
                    <thead>
                        <tr>
                            <th class="tLeft">Resumen</th>
                    </thead>
                </table>


                <dl class="accordion5">
                    <dt class="Acofecha">
                        <div class="AcoTit">Fecha y hora</div>
                    </dt>
                    <dd>
                        <div class="inner2">

                            <table class="calendar">
                                <tbody>
                                    <tr>
                                        <td>Inicio:</td>
                                        <td><span id="fInicio">TEXTO 3</span></td>
                                        <td><span id="hInicio">TEXTO 4</span></td>
                                    </tr>
                                    <tr>
                                        <td>Final</td>
                                        <td><span id="fFin">TEXTO 5</span></td>
                                        <td><span id="hFin">TEXTO 6</span></td>
                                    </tr>
                                    <tr>
                                        <td>Envio</td>
                                        <td><span id="fEnvio">TEXTO 7</span></td>
                                        <td>Modo: <span id="hModo">TEXTO 8</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </dd>
                    <hr>
                    <dt class="Acofolios">
                        <div class="AcoTit">Folios</div>
                    </dt>
                    <dd>
                        <div class="inner2">
                            <table class="folio" id="tablaFolioContenido">
                                <tbody>
                                    <!--<c:forEach var="item" items="${listaFolios}"> -->
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO P</td>
                                            <td><span>TEXT 2 P</span></td>
                                        </tr>
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO P</td>
                                            <td><span>TEXT 2 P</span></td>
                                        </tr>
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO P</td>
                                            <td><span>TEXT 2 P</span></td>
                                        </tr>
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO P</td>
                                            <td><span>TEXT 2 P</span></td>
                                        </tr>
                                        <!-- </c:forEach> -->
                                    </tbody>
                                </table>
                            </div>
                        </dd>
                        <hr>
                        <dt class="AcoEvidencia">
                            <div class="AcoTit">Evidencia</div>
                        </dt>

                        <dd>
                            <div class="inner2">
                                <table class="evidencia" id="tablaEvidenciaContenido">
                                    <tbody>
                                        <!--<c:forEach var="item" items="${listaEvidencias}"> -->
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO EVI</td>
                                        </tr>
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO EVI</td>
                                        </tr>
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO EVI</td>
                                        </tr>
                                        <tr
                                            style="cursor: pointer;">
                                            <td>TEXTO EVI</td>
                                        </tr>
                                        <!--</c:forEach> -->
                                </tbody>
                            </table>
                        </div>
                    </dd>
                    <hr>
                </dl>
            </div>

            <form id="formTerri" method="POST" action="vistaTerritCumplimientoTareas.htm">
            </form>

            <!-- <div class="fspacer"></div> -->
        </div>

        <!-- jquery Start -->
        <script type="text/javascript"
        src="../js/vistaCumplimientoTareas/jquery.js"></script>

        <!-- progressbar -->
        <script>
                        function move() {
                            var elem = document.getElementById("myBar");
                            var width = 1;
                            var id = setInterval(frame, 10);
                            function frame() {
                                if (width >= 100) {
                                    clearInterval(id);
                                } else {
                                    width++;
                                    elem.style.width = width + '%';
                                }
                            }
                        }
        </script>

        <!-- dropdown -->
        <script type="text/javascript"
        src="../js/vistaCumplimientoTareas/menu.js"></script>

        <!-- accordion -->
        <script type="text/javascript">
                        jQuery(function () {
                            $(".subSeccion").hide();

                            var allPanels = $('.accordion5 > dd').hide();

                            jQuery('.accordion5 > dt').on('click', function () {
                                $this = $(this);
                                //the target panel content
                                $target = $this.next();

                                jQuery('.accordion5 > dt').removeClass('accordion-active');
                                jQuery('.accordion5 > dt').removeClass('Acofondo');

                                if ($target.hasClass("in")) {
                                    $this.removeClass('accordion-active');
                                    $target.slideUp();
                                    $target.removeClass("in");

                                } else {
                                    $this.addClass('accordion-active');
                                    jQuery('.accordion5 > dd').removeClass("in");
                                    $target.addClass("in");
                                    $this.addClass('Acofondo');

                                    jQuery('.accordion5 > dd').slideUp();
                                    $target.slideDown();
                                }
                            })

                        })
        </script>

        <!-- Slider -->
        <script
        src="../js/vistaCumplimientoTareas/owl.carousel.js"></script>
        <!-- Slider -->
        <script
        src="../js/vistaCumplimientoTareas/script-detalleCumplimientoTareas.js"></script>

        <script type="text/javascript">
                        $(document).ready(function () {
                            /*$( ".owl-theme .owl-controls .owl-buttons div.owl-prev" ).css("display":"none");*/

                            $("#owl-main").owlCarousel({
                                navigation: true, // Show next and prev buttons
                                slideSpeed: 300,
                                paginationSpeed: 400,
                                singleItem: true
                            });
                        });
        </script>
        <!-- UI RANGE SLIDER -->

        <script type="text/javascript"
        src="../js/vistaCumplimientoTareas/jquery-ui-rangeSlider.min.js"></script>
        <script type="text/javascript"
        src="../js/vistaCumplimientoTareas/jquery-rangeSlider.js"></script>
        <script type="text/javascript">
                        $(window).load(function () {
                            var json = $
                            {
                                json
                            }
                            ;
                            /* slider(min,	max, step, actual, indicador);
                             slider(500, 12000, 500, 2000, '.amount div');*/
                            slider(1, json.length, 1, 1, '.amount div');
                        });

                        $('#slider').slider({
                            change: function (event, ui) {
                                if (event.originalEvent) {
                                    //manual change
                                    cargaEvidencias(ui.value - 1);
                                } else {
                                    //programmatic change
                                }
                            }
                        });
        </script>
        <div class="modal"></div>
    </body>
</html>