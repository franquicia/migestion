<!DOCTYPE HTML>
<html>
    <head>
        <title>MiGestion - Error</title>
        <style type="text/css">
            .vertical-center {
                min-height: 100%;
                min-height: 100vh;
                display: flex;
                align-items: center;
            }
        </style>
        <link rel="stylesheet" type="text/css"
              href="../css/bootstrap/bootstrap.min.css">
        <!-- <link rel="stylesheet" type="text/css" href="css/header-style.css"> -->
        <script
        src="../js/jquery/jquery-2.2.4.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>

        <% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
        <% response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate"); %>
        <% response.addHeader("Pragma", "no-cache"); %>
        <% response.addHeader("Expires", "0"); %>
    </head>

    <header> </header>

    <body>
        <div class="container">
            <div class="row vertical-center">
                <div class="col-md-12">
                    <img
                        src="../images/error/algo_paso.png"
                        class="img-responsive center-block" /> <br /> <br /> <br /> <br />
                    <br />
                    <div class="center-block" style="max-width: 22em;">
                        <a href="/migestion/"> <img
                                src="../images/error/boton_volver.png"
                                class="img-responsive" />
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('#go-back').click(function () {
                    parent.history.back();
                    return false;
                });
            });
        </script>
    </body>
</html>