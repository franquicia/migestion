<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



        <script type="text/javascript" src="../js/script-menu.js"></script>

        <link rel="stylesheet" type="text/css" href="../css/estilos.css">
        <link rel="stylesheet" type="text/css" href="../css/header-style.css">
        <link rel="stylesheet" type="text/css" href="../css/menuCheck.css">
        <link rel="stylesheet" type="text/css" href="../css/carousel.css">

        <script	src="../js/jquery/jquery-2.2.4.min.js"></script>

        <script src="../js/script-storeIndex.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>

        <title>Carga Usuarios Externos</title>

    </head>
    <body>

        <tiles:insertTemplate template="/WEB-INF/templates/templateHeader.jsp" flush="true">
            <tiles:putAttribute name="cabecera" value="/WEB-INF/templates/templateHeader.jsp" />
        </tiles:insertTemplate>

        <tiles:insertTemplate template="/WEB-INF/views/menu.jsp" flush="true">
            <tiles:putAttribute name="menu" value="/WEB-INF/views/menu.jsp" />
        </tiles:insertTemplate>
        <h1>Subir Archivo Excel</h1>
        <br>
        <c:choose>
            <c:when test="${OKARCH=='OK'} ">
                <iframe src="http://10.53.33.82/franquicia/imagenes/${NomArchivo}" style="width: 90%;heigth:70%" frameborder="0"></iframe>
                <br>
                <br>
                <c:url value="/indicadores/getDataIndicadoresIPN.htm" var="uploadFilesUrl"/>
                <form:form method="POST" action="${uploadFilesUrl}" modelAttribute="command" name="form-uploadFiles" id="form-uploadFiles" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputFile">Actualizar el archivo</label>
                        <input type="hidden" id="fileAction" name="fileAction" value="${fileAction}"/>
                        <input type="file" id="uploadedFile" name="uploadedFile"/>
                        <input type="submit" class="btn btn-default btn-lg" value="Subir Archivo" onclick="return checkInput()"/>

                    </div>
                </form:form>
            </c:when>
            <c:otherwise>
                <c:url value="/indicadores/getDataIndicadoresIPN.htm" var="uploadFilesUrl"/>
                <form:form metod="POST" action="${uploadFilesUrl}" modelAtributte="command" name="form-uploadFiles" id="form-uploadFiles" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputFile">Subir Archivo</label>
                        <input type="hidden" id="fileAction" name="fileAction" value="${fileAction}"/>
                        <input type="file" id="uploadedFile" name="uploadedFile"/>
                        <input type="submit" class="btn btn-default btn-lg" value="Subir archivo" />

                    </div>
                </form:form>

            </c:otherwise>
        </c:choose>
        <script>
            function checkInput() {
                var oFile = document.getElementById("uploadedFile").files[0];

                if (tAction == null) {
                    alert("La ruta no puede ir vacía");
                    return false;
                } else {
                    if (oFile.size > (1024 * 1024 * 100)) {
                        alert("El archivo pesa más de 100MB!");
                        return false;
                    } else {
                        return true;
                    }
                }
            }

        </script>

    </body>
</html>