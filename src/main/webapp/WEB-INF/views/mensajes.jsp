<% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
<% response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate"); %>
<% response.addHeader("Pragma", "no-cache"); %>
<% response.addHeader("Expires", "0"); %>
<div id="content" class="carouselDiv"
     style="width: 60%; height: 100%; margin: 0 auto; padding: 10px">
    <!--  <img src="../images/franquicia_espera.jpg"/> -->

    <h3 align="center">Mi Gesti&oacute;n 2017</h3>

    <!-- Carousel -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width: 100%; height: 100%; left: 5%;">
        <!-- Indicadores -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <!--
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            -->
        </ol>
        <!-- Imagenes -->
        <div class="carousel-inner" role="listbox">
            <!-- <div class="item active">
                    <img src="../images/carousel/diapositivas/Diapositiva1.jpg"
                            alt="imagen1" style="height: 100%; width: 100%;">
            </div>

            <div class="item">
                    <img src="../images/carousel/diapositivas/Diapositiva2.jpg"
                            alt="imagen1" style="height: 100%; width: 100%;">
            </div>

            <div class="item">
                    <img src="../images/carousel/diapositivas/Diapositiva3.jpg"
                            alt="imagen1" style="height: 100%; width: 100%;">
            </div>

            <div class="item">
                    <img src="../images/carousel/diapositivas/Diapositiva4.jpg"
                            alt="imagen1" style="height: 100%; width: 100%;">
            </div>

            <div class="item">
                    <img src="../images/carousel/diapositivas/Diapositiva5.jpg"
                            alt="imagen1" style="height: 100%; width: 100%;">
            </div>

            <div class="item">
                    <img src="../images/carousel/diapositivas/Diapositiva6.jpg"
                            alt="imagen1" style="height: 100%; width: 100%;">
            </div>
            -->
            <div class="item active">
                <img src="../images/carousel/diapositivas/Diapositiva0.jpg"
                     alt="imagen1" style="height: 100%; width: 100%;">
            </div>

            <!-- Controles Izquierda y Derecha -->
            <a class="left carousel-control" href="#myCarousel" role="button"data-slide="prev">
                <span class="flechas" id="fAnterior"> <img
                        src="../images/carousel/slide/flechaizqover.png" alt="imagen2"
                        width="40" height="55">
                </span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button"
               data-slide="next"> <!--Imagenes Flecha Derecha --> <span
                    class="flechas" id="fSiguiente"> <img
                        src="../images/carousel/slide/flechaderover.png" alt="imagen2"
                        width="40" height="55">
                </span>
            </a>
        </div>
    </div>


</div>

