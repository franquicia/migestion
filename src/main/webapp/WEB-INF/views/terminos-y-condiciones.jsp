<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Banco Azteca | Términos y Condiciones</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/vistaCumplimientoTareas/estilo.css" />

    </head>
    <body>
        <div class="header">
            <div class="title">
                <b>TÉRMINOS Y CONDICIONES</b>
            </div>
        </div>
        <div class="wrapper">
            <iframe width="100%" height="100%" src="${pageContext.request.contextPath}/pdf/terminos-condiciones.pdf" frameborder="0" allowfullscreen></iframe>
        </div>
    </body>
</html>
