<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <script src="../js/jquery/jquery-2.2.4.min.js"></script>
        <script src="../css/jQuery/jQuery.js"></script>
        <script src="../js/adminBD/checkpreg.js"></script>
        <script src="../js/adminBD/arbolDes.js"></script>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Response</title>

        <style>
            table, th, td {
                border: 1px solid black;
            }

            th {
                background-color: #4CAF50;
                color: white;
            }
        </style>

    </head>
    <body>
        <!-- MODULO -->
        <c:choose>

            <c:when test="${tipo == 'CECO PASO PARAMS'}">
                <table>
                    <thead>
                        <tr>
                            <th>FCCCID</th>
                            <th>FIENTIDADID</th>
                            <th>FINUM_ECONOMICO</th>
                            <th>FCNOMBRECC</th>
                            <th>FCNOMBRE_ENT</th>
                            <th>FCCCID_PADRE</th>
                            <th>FIENTDID_PADRE</th>
                            <th>FCNOMBRECC_PAD</th>
                            <th>FCTIPOCANAL</th>
                            <th>FCNOMBTIPCANAL</th>
                            <th>FCCANAL</th>
                            <th>FISTATUSCCID</th>
                            <th>FCSTATUSCC</th>
                            <th>FITIPOCC</th>
                            <th>FIESTADOID</th>
                            <th>FCMUNICIPIO</th>
                            <th>FCTIPOOPERACION</th>
                            <th>FCTIPOSUCURSAL</th>
                            <th>FCNOMBRETIPOSUC</th>
                            <th>FCCALLE</th>
                            <th>FIRESPONSABLEID</th>
                            <th>FCRESPONSABLE</th>
                            <th>FCCP</th>
                            <th>FCTELEFONOS</th>
                            <th>FICLASIF_SIEID</th>
                            <th>FCCLASIF_SIE</th>
                            <th>FIGASTOXNEGID</th>
                            <th>FCGASTOXNEGOCIO</th>
                            <th>FDAPERTURA</th>
                            <th>FDCIERRE</th>
                            <th>FIPAISID</th>
                            <th>FCPAIS</th>
                            <th>FDCARGA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.fcccid}</td>
                                <td>${item.fientidadid}</td>
                                <td>${item.finum_economico}</td>
                                <td>${item.fcnombrecc}</td>
                                <td>${item.fcnombre_ent}</td>
                                <td>${item.fcccid_padre}</td>
                                <td>${item.fientdid_padre}</td>
                                <td>${item.fcnombrecc_pad}</td>
                                <td>${item.fctipocanal}</td>
                                <td>${item.fcnombtipcanal}</td>
                                <td>${item.fccanal}</td>
                                <td>${item.fistatusccid}</td>
                                <td>${item.fcstatuscc}</td>
                                <td>${item.fitipocc}</td>
                                <td>${item.fiestadoid}</td>
                                <td>${item.fcmunicipio}</td>
                                <td>${item.fctipooperacion}</td>
                                <td>${item.fctiposucursal}</td>
                                <td>${item.fcnombretiposuc}</td>
                                <td>${item.fccalle}</td>
                                <td>${item.firesponsableid}</td>
                                <td>${item.fcresponsable}</td>
                                <td>${item.fccp}</td>
                                <td>${item.fctelefonos}</td>
                                <td>${item.ficlasif_sieid}</td>
                                <td>${item.fcclasif_sie}</td>
                                <td>${item.figastoxnegid}</td>
                                <td>${item.fcgastoxnegocio}</td>
                                <td>${item.fdapertura}</td>
                                <td>${item.fdcierre}</td>
                                <td>${item.fipaisid}</td>
                                <td>${item.fcpais}</td>
                                <td>${item.fdcarga}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'SUCURSAL'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID SUCURSAL</th>
                            <th>ID PAIS</th>
                            <th>ID CANAL</th>
                            <th>NU SUCURSAL</th>
                            <th>NOMBRE SUC</th>
                            <th>LATITUD</th>
                            <th>LONGITUD</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.fisucursal_id}</td>
                                <td>${item.fipais}</td>
                                <td>${item.ficanal}</td>
                                <td>${item.fisucursal}</td>
                                <td>${item.fcnombreccSuc}</td>
                                <td>${item.fclongitude}</td>
                                <td>${item.fclatitude}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'LISTA TAREAS'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIID_PKTAREA</th>
                            <th>FIID_TAREA</th>
                            <th>FCNOM_TAREA</th>
                            <th>FIESTATUS</th>
                            <th>FIEST_VAC</th>
                            <th>FIID_USUARIO</th>
                            <th>FCTIPO_TAREA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idPkTarea}</td>
                                <td>${item.idTarea}</td>
                                <td>${item.nombreTarea}</td>
                                <td>${item.estatus}</td>
                                <td>${item.estatusVac}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.tipoTarea}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'LISTA GEOGRAFIA'}">
                <table>
                    <thead>
                        <tr>
                            <th>FCID_CECO</th>
                            <th>FCID_REGION</th>
                            <th>FCREGION</th>
                            <th>FCID_ZONA</th>
                            <th>FCZONA</th>
                            <th>FCID_TERRITORIO</th>
                            <th>FCTERRITORIO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idCeco}</td>
                                <td>${item.idRegion}</td>
                                <td>${item.nombreRegion}</td>
                                <td>${item.idZona}</td>
                                <td>${item.nombreZona}</td>
                                <td>${item.idTerritorio}</td>
                                <td>${item.nombreTerritorio}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'LISTA USUARIOS'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIID_USUARIO</th>
                            <th>FIID_PUESTO</th>
                            <th>FCID_CECO</th>
                            <th>FCNOMBRE</th>
                            <th>FIACTIVO</th>
                            <th>FDNACIMIENTO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idUsuario}</td>
                                <td>${item.idPuesto}</td>
                                <td>${item.idCeco}</td>
                                <td>${item.nombre}</td>
                                <td>${item.activo}</td>
                                <td>${item.fecha}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'LISTA PERIODOR'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIID_PER_REP</th>
                            <th>FIID_PERIODO</th>
                            <th>FIID_REPORTE</th>
                            <th>FCHORARIO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.id_per_rep}</td>
                                <td>${item.id_periodo}</td>
                                <td>${item.id_reporte}</td>
                                <td>${item.horario}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:when test="${tipo == 'LISTA PERIODO'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIID_PERIODO</th>
                            <th>FCDESCRIPCION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idPeriodo}</td>
                                <td>${item.descripcion}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'LISTA REPORTE'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIID_REPORTE</th>
                            <th>FCNOMBRE_REP</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idReporte}</td>
                                <td>${item.nombreReporte}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'USUARIOS PASO PARAMS'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIEMPLEADO</th>
                            <th>FCNOMBRE</th>
                            <th>FICC</th>
                            <th>FIFUNCION</th>
                            <th>FIPUESTO</th>
                            <th>FCDESCFUN</th>
                            <th>FCDESCPUESTO</th>
                            <th>FCPAISUSUARIO</th>
                            <th>FIFECHABAJA</th>
                            <th>FIEMPLEADOREP</th>
                            <th>FICCEMPREP</th>
                            <th>FIDISPONIBLE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.fiempleado}</td>
                                <td>${item.fcnombre}</td>
                                <td>${item.ficc}</td>
                                <td>${item.fifuncion}</td>
                                <td>${item.fipuesto}</td>
                                <td>${item.fcdescfun}</td>
                                <td>${item.fcdescpuesto}</td>
                                <td>${item.fcpaisUsuario}</td>
                                <td>${item.fifechabaja}</td>
                                <td>${item.fiempleadorep}</td>
                                <td>PAIS: ${item.ficcemprep}</td>
                                <td>${item.fidisponible}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'LISTA AGENDA'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIID_AGENDA</th>
                            <th>FIID_GOOGLE</th>
                            <th>FCTITULO</th>
                            <th>FCEVENTO</th>
                            <th>FIPERIODO</th>
                            <th>FIALERTA</th>
                            <th>FCHORARIO_INI</th>
                            <th>FCHORARIO_FIN</th>
                            <th>FIID_USUARIO</th>
                            <th>FCCORREO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idAgenda}</td>
                                <td>${item.idGoogle}</td>
                                <td>${item.titulo}</td>
                                <td>${item.evento}</td>
                                <td>${item.periodo}</td>
                                <td>${item.alerta}</td>
                                <td>${item.horarioIni}</td>
                                <td>${item.horarioFin}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.correo}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:when test="${tipo == 'LISTA CECOS'}">

                <table>
                    <thead>
                        <tr>
                            <th>ID CECO</th>
                            <th>IP PAIS</th>
                            <th>ID NEGOCIO</th>
                            <th>ID CANAL</th>
                            <th>ID ESTADO</th>
                            <th>DESC CECO</th>
                            <th>ID CECO SUPERIOR</th>
                            <th>ACTIVO</th>
                            <th>ID NIVEL</th>
                            <th>CALLE</th>
                            <th>CIUDAD</th>
                            <th>CP</th>
                            <th>NOMBRE CONTACTO</th>
                            <th>PUESTO CONTACTO</th>
                            <th>TELEFONO CONTACTO</th>
                            <th>FAX CONTACTO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idCeco}</td>
                                <td>${item.idPais}</td>
                                <td>${item.idNegocio}</td>
                                <td>${item.idCanal}</td>
                                <td>${item.idEstado}</td>
                                <td>${item.descCeco}</td>
                                <td>${item.idCecoSuperior}</td>
                                <td>${item.activo}</td>
                                <td>${item.idNivel}</td>
                                <td>${item.calle}</td>
                                <td>${item.cp}</td>
                                <td>${item.nombreContacto}</td>
                                <td>${item.puestoContacto}</td>
                                <td>${item.telefonoContacto}</td>
                                <td>${item.faxContacto}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'LISTA PARAMETROS'}">
                <c:forEach items="${res}" var="item">
                    CLAVE: ${item.clave} <br />
                    VALOR: ${item.valor} <br />
                    ACTIVO: ${item.activo} <br />
                    <br />
                    <br />
                </c:forEach>
            </c:when>

            <c:when test="${tipo == 'LISTA SUCURSALES'}">
                <c:forEach items="${res}" var="item">
                    ID SUCURSAL: ${item.idSucursal} <br />
                    ID PAIS: ${item.idPais} <br />
                    ID CANAL: ${item.idCanal} <br />
                    NU SUCURSAL: ${item.nuSucursal} <br />
                    NOMBRE SUC: ${item.nombresuc} <br />
                    LATITUD: ${item.latitud} <br />
                    LONGITUD: ${item.longitud} <br />
                    <br />
                    <br />
                </c:forEach>
            </c:when>

            <c:when test="${tipo == 'LISTA PERFILES'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID PERFIL</th>
                            <th>DESCRIPCION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idPerfil}</td>
                                <td>${item.descripcion}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'PERFILES USUARIO'}">
                <table align="left">
                    <thead>
                        <tr>
                            <th>ID USUARIO</th>
                            <th>ID PERFIL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idUsuario}</td>
                                <td>${item.idPerfil}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            
            <c:when test="${tipo == 'ERRORES'}">

                <c:forEach items="${res}" var="item">
                    ID LOG: ${item.idLog} <br />
                    FECHA ERROR: ${item.fechaError} <br />
                    CODIGO ERROR: ${item.codigoError} <br />
                    MENSAJE ERROR: ${item.mensajeError} <br />
                    ORIGEN ERROR: ${item.origenError}<br />
                    <br />
                    <br />
                </c:forEach>
            </c:when>

            <c:when test="${tipo == 'LISTA MOVIL'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID MOVIL</th>
                            <th>ID USUARIO</th>
                            <th>FECHA</th>
                            <th>SO</th>
                            <th>VERSION</th>
                            <th>VERSION APP</th>
                            <th>MODELO</th>
                            <th>FABRICANTE</th>
                            <th>NUMERO MOVIL</th>
                            <th>TIPO CONEXION</th>
                            <th>IDENTIFICADOR</th>
                            <th>TOKEN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idMovil}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.fecha}</td>
                                <td>${item.so}</td>
                                <td>${item.version}</td>
                                <td>${item.versionApp}</td>
                                <td>${item.modelo}</td>
                                <td>${item.fabricante}</td>
                                <td>${item.numMovil}</td>
                                <td>${item.tipoCon}</td>
                                <td>${item.identificador}</td>
                                <td>${item.token}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'REPORTE_MOVIL_INF_MENSUAL'}">
                <table>
                    <thead>
                        <tr>
                            <!--<th>MES</th>-->
                            <!--<th>ANIO</th>-->
                            <th>ID USUARIO</th>
                            <th>NOMBRE</th>
                            <th>CECO</th>
                            <th>NOMBRE_CECO</th>
                            <th>TOTAL DE SESIONES</th>
                            <th>ULTIMA SESION</th>

                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                    <!--<td>${item.mes}</td>-->
                                    <!--<td>${item.anio}</td>-->
                                <td>${item.idUsuario}</td>
                                <td>${item.nombreUsuario}</td>
                                <td>${item.idCeco}</td>
                                <td>${item.nombreCeco}</td>
                                <td>${item.numSesion}</td>
                                <td>${item.fechaUltimaSesion}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'REPORTE_MOVIL_INF_DETALLE'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID MOVIL</th>
                            <th>ID USUARIO</th>
                            <th>FECHA</th>
                            <th>SO</th>
                            <th>VERSION </th>
                            <th>VERSION APP</th>
                            <th>MODELO</th>
                            <th>FABRICANTE</th>
                            <th>NUM TELEFONO</th>
                            <th>TIPO CONEXION</th>
                            <th>IDENTIFICADOR</th>
                            <th>TOKEN GCM</th>



                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idMovil}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.fecha}</td>
                                <td>${item.so}</td>
                                <td>${item.version}</td>
                                <td>${item.versionApp}</td>
                                <td>${item.modelo}</td>
                                <td>${item.fabricante}</td>
                                <td>${item.numMovil}</td>
                                <td>${item.tipoCon}</td>
                                <td>${item.identificador}</td>
                                <td>${item.token}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'REPORTE_MENSUAL_CECO'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_USUARIO</th>
                            <th>NOMBRE_USUARIO</th>
                            <th>ID_PUESTO</th>
                            <th>NOMBRE_PUESTO</th>
                            <th>ID_CECO</th>
                            <th>NOMBRE_CECO </th>
                            <th>ID_CECO_SUPERIOR</th>
                            <th>NOMBRE_CECO_SUPERIOR</th>
                            <th>TOTAL_SESIONES</th>
                            <th>ULTIMA_SESION</th>

                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idUsuario}</td>
                                <td>${item.nombreUsuario}</td>

                                <td>${item.idPuesto}</td>
                                <td>${item.desPuesto}</td>

                                <td>${item.idCeco}</td>
                                <td>${item.nombreCeco}</td>
                                <td>${item.idCecoSuperior}</td>
                                <td>${item.nombreCecoSuperior}</td>
                                <td>${item.totalSesiones}</td>
                                <td>${item.fechaUltimaSesion}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'CECOS_HIJOS'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_CECO</th>
                            <th>FCNOMBRE</th>
                            <th>ID_CECO_SUPERIOR</th>


                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idCeco}</td>
                                <td>${item.descCeco}</td>
                                <td>${item.idCecoSuperior}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'GETANOTIFICA_AF'}">
                <table>
                    <thead>
                        <tr>
                            <th>FINOTIFICA</th>
                            <th>FC_IDCECO</th>
                            <th>FICOMPROMISO_P</th>
                            <th>FIAVANCE_P</th>
                            <th>FICOMPROMISO_A</th>
                            <th>FIAVANCE_A</th>
                            <th>FCFECHA_COMP</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idNotifica}</td>
                                <td>${item.ceco}</td>
                                <td>${item.compromisoP}</td>
                                <td>${item.avanceP}</td>
                                <td>${item.compromisoA}</td>
                                <td>${item.avanceA}</td>
                                <td>${item.fecha}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'Catalogo Vista'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_CATALOGO_VISTA</th>
                            <th>DESCRIPCION</th>
                            <th>MODULO_PADRE</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idCatVista}</td>
                                <td>${item.descripcion}</td>
                                <td>${item.idModPadre}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'Info Vista'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_INFO_VISTA</th>
                            <th>ID_CATALOGO_VISTA</th>
                            <th>ID_USUARIO</th>
                            <th>PLATAFORMA</th>
                            <th>FECHA</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idInfoVista}</td>
                                <td>${item.idCatVista}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.plataforma}</td>
                                <td>${item.fecha}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'CONSULTA_UPAX_1'}">
                <table>
                    <thead>
                        <tr>
                            <th>VALOR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${res}</td>
                        </tr>

                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'Filas'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_FILA</th>
                            <th>NUMERO_ECONOMICO</th>
                            <th>GRUPO</th>

                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idFila}</td>
                                <td>${item.numEconomico}</td>
                                <td>${item.idGrupo}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            

            

            <c:when test="${tipo == 'EMP_EXT'}">
                <table>
                    <thead>
                        <tr>
                            <th>EMPLEADO_NUM</th>
                            <th>NOMBRE</th>
                            <th>APPATERNO</th>
                            <th>APMATERNO</th>
                            <th>DIRECCION</th>
                            <th>NUM_CASA</th>
                            <th>NUM_CELULAR</th>
                            <th>RFCEMPLEADO</th>
                            <th>CENCOS_NUM</th>
                            <th>PUESTO</th>
                            <th>FECHA_INGRESO</th>
                            <th>FECHA_BAJA</th>
                            <th>FUNCION</th>
                            <th>USUARIO_ID</th>
                            <th>EMPLEADO_SIT</th>
                            <th>HONORARIOS</th>
                            <th>EMAIL</th>
                            <th>NUM_EMERGENC</th>
                            <th>PER_EMERGENC</th>
                            <th>NUM_EXTENSION</th>
                            <th>SUELDO</th>
                            <th>HORARIO_INI</th>
                            <th>HORARIO_FN</th>
                            <th>SEMESTRE</th>
                            <th>ACTIVIDADES</th>
                            <th>LIBERACION</th>
                            <th>ELABORACION</th>
                            <th>LLAVE</th>
                            <th>RED</th>
                            <th>LOTUS</th>
                            <th>CREDENCIAL</th>
                            <th>FOLIODATASEC</th>
                            <th>PAIS</th>
                            <th>NOMB_COMPLETO</th>
                            <th>ASISTENCIA</th>
                            <th>CURP</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.numEmpleado}</td>
                                <td>${item.nombre}</td>
                                <td>${item.aPaterno}</td>
                                <td>${item.aMaterno}</td>
                                <td>${item.direccion}</td>
                                <td>${item.numCasa}</td>
                                <td>${item.numCelular}</td>
                                <td>${item.RFCEmpleado}</td>
                                <td>${item.cencosNum}</td>
                                <td>${item.puesto}</td>
                                <td>${item.fechaIngreso}</td>
                                <td>${item.fechaBaja}</td>
                                <td>${item.funcion}</td>
                                <td>${item.usuarioID}</td>
                                <td>${item.empSituacion}</td>
                                <td>${item.honorarios}</td>
                                <td>${item.email}</td>
                                <td>${item.numEmergencia}</td>
                                <td>${item.perEmergencia}</td>
                                <td>${item.numExt}</td>
                                <td>${item.sueldo}</td>
                                <td>${item.horarioIn}</td>
                                <td>${item.horarioFin}</td>
                                <td>${item.semestre}</td>
                                <td>${item.actividades}</td>
                                <td>${item.liberacion}</td>
                                <td>${item.elaboracion}</td>
                                <td>${item.llave}</td>
                                <td>${item.red}</td>
                                <td>${item.lotus}</td>
                                <td>${item.credencial}</td>
                                <td>${item.folioDataSec}</td>
                                <td>${item.pais}</td>
                                <td>${item.nombreCompleto}</td>
                                <td>${item.asistencia}</td>
                                <td>${item.curp}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'TEL_SUC'}">
                <table>
                    <thead>
                        <tr>
                            <th>TEL_SUC_ID</th>
                            <th>NUMECO</th>
                            <th>TELEFONO</th>
                            <th>PROVEEDOR</th>
                            <th>STATUS_DISTRI</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idTelefono}</td>
                                <td>${item.idCeco}</td>
                                <td>${item.telefono}</td>
                                <td>${item.proveedor}</td>
                                <td>${item.estatus}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'INCIDENCIA'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_SERVICIO</th>
                            <th>SERVICIO</th>
                            <th>UBICACION</th>
                            <th>INCIDENCIA</th>
                            <th>PLANTILLA</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idTipo}</td>
                                <td>${item.servicio}</td>
                                <td>${item.ubicacion}</td>
                                <td>${item.incidencia}</td>
                                <td>${item.plantilla}</td>
                                <td>${item.status}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'PROVEEDORES'}">
                <table>
                    <thead>
                        <tr>
                            <th>MENU</th>
                            <th>ID PROVEEDOR</th>
                            <th>NOMBRE CORTO</th>
                            <th>RAZON SOCIAL</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.menu}</td>
                                <td>${item.idProveedor}</td>
                                <td>${item.nombreCorto}</td>
                                <td>${item.razonSocial}</td>
                                <td>${item.status}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'EMPMTTO'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID USUARIO</th>
                            <th>NOMBRE</th>
                            <th>CECO</th>
                            <th>PUESTO</th>
                            <th>TELEFONO</th>
                            <th>ID PUESTO</th>
                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>

                                <td>${item.idUsuario}</td>
                                <td>${item.nombre}</td>
                                <td>${item.descripcion}</td>
                                <td>${item.ceco}</td>
                                <td>${item.telefono}</td>
                                <td>${item.idPuesto}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'FORMATO7S'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_TABLA</th>
                            <th>PERIODO</th>
                            <th>ID USUARIO</th>
                            <th>NOMBRE</th>
                            <th>PREGUNTA_1</th>
                            <th>PREGUNTA_2</th>
                            <th>PREGUNTA_3</th>
                            <th>SUCURSAL_ID</th>
                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idtab}</td>
                                <td>${item.periodo}</td>
                                <td>${item.idusuario}</td>
                                <td>${item.nombre}</td>
                                <td>${item.preg1}</td>
                                <td>${item.preg2}</td>
                                <td>${item.preg3}</td>
                                <td>${item.idSucursal}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'CEDULA'}">
                <table>
                    <thead>
                        <tr>
                            <th>PERIODO</th>
                            <th>ID USUARIO</th>
                            <th>NOMBRE</th>
                            <th>PUESTO</th>
                            <th>BANDERA</th>
                            <th>FECHA</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.periodo}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.nombre}</td>
                                <td>${item.descPuesto}</td>
                                <td>${item.bandera}</td>
                                <td>${item.fecha}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'ESTPUESTO'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_MUEBLE</th>
                            <th>ID</th>
                            <th>SUCURSAL</th>
                            <th>MUEBLE 1 CAJON 1</th>
                            <th>MUEBLE1 CAJON2</th>
                            <th>MUEBLE1 CAJON3</th>
                            <th>MUEBLE2 CAJON4</th>
                            <th>MUEBLE2 CAJON5</th>
                            <th>MUEBLE2 CAJON6</th>
                            <th>FCPERIODO</th>
                            <th>BANDERA</th>
                            <th>PUESTO</th>


                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idMueble}</td>
                                <td>${item.idPuesto}</td>
                                <td>${item.ceco}</td>
                                <td>${item.mueble1caj1}</td>
                                <td>${item.mueble1caj2}</td>
                                <td>${item.mueble1caj3}</td>
                                <td>${item.mueble2caj1}</td>
                                <td>${item.mueble2caj2}</td>
                                <td>${item.mueble2caj6}</td>
                                <td>${item.periodo}</td>
                                <td>${item.bandera}</td>
                                <td>${item.puesto}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'PROGLIMPIEZA'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ID_AREA</th>
                            <th>SUCURSAL</th>
                            <th>AREA</th>
                            <th>ARTICULOS</th>
                            <th>FECUENCIA</th>
                            <th>HORARIO</th>
                            <th>RESPONSABLE</th>
                            <th>SUPERVISOR</th>
                            <th>PERIODO</th>
                            <th>BANDERA</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idProg}</td>
                                <td>${item.idArea}</td>
                                <td>${item.ceco}</td>
                                <td>${item.area}</td>
                                <td>${item.articulo}</td>
                                <td>${item.frecuencia}</td>
                                <td>${item.horario}</td>
                                <td>${item.responsable}</td>
                                <td>${item.supervisor}</td>
                                <td>${item.periodo}</td>
                                <td>${item.bandera}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'URL'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>RUTA</th>
                            <th>VERSION</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idArch}</td>
                                <td>${item.nombre}</td>
                                <td>${item.ruta}</td>
                                <td>${item.version}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'LOGIN'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ID_PROVEEDOR</th>
                            <th>PASSW</th>
                            <th>PERIODO</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idpass}</td>
                                <td>${item.idProveedor}</td>
                                <td>${item.passw}</td>
                                <td>${item.periodo}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'CUADRILLA'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_CUADRILLA</th>
                            <th>ID_PROVEEDOR</th>
                            <th>ZONA</th>
                            <th>LIDER CUADRILLA</th>
                            <th>PASSWORD</th>
                            <th>CORREO</th>
                            <th>SEGUNDO A CARGO</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idCuadrilla}</td>
                                <td>${item.idProveedor}</td>
                                <td>${item.zona}</td>
                                <td>${item.liderCuad}</td>
                                <td>${item.passw}</td>
                                <td>${item.correo}</td>
                                <td>${item.segundo}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'CUADRILLAPASS'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_CUADRILLA</th>
                            <th>ID_PROVEEDOR</th>
                            <th>ZONA</th>
                            <th>LIDER CUADRILLA</th>
                            <th>PASSWORD</th>
                            <th>CORREO</th>
                            <th>SEGUNDO A CARGO</th>



                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idCuadrilla}</td>
                                <td>${item.idProveedor}</td>
                                <td>${item.zona}</td>
                                <td>${item.liderCua}</td>
                                <td>${item.passw}</td>
                                <td>${item.correo}</td>
                                <td>${item.segundo}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'TKCUADRI'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ID_CUADRILLA</th>
                            <th>ID_PROVEEDOR</th>
                            <th>TICKET</th>
                            <th>ZONA</th>
                            <th>LIDER CUADRILLA</th>
                            <th>STATUS</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idtkCuadri}</td>
                                <td>${item.idCuadrilla}</td>
                                <td>${item.idProveedor}</td>
                                <td>${item.ticket}</td>
                                <td>${item.zona}</td>
                                <td>${item.liderCua}</td>
                                <td>${item.status}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'FOTOPLAN'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_PLANTILLA</th>
                            <th>CECO</th>
                            <th>USUARIO</th>
                            <th>RUTA FOTO</th>
                            <th>PERIODO</th>


                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idPlanti}</td>
                                <td>${item.ceco}</td>
                                <td>${item.usuario}</td>
                                <td>${item.ruta}</td>
                                <td>${item.periodo}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'BITACORAMMT'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_BITACORA</th>
                            <th>USUARIO</th>
                            <th>CECO</th>
                            <th>FOLIO</th>
                            <th>FECHA</th>
                            <th>NOMBRE PROVEEDOR</th>
                            <th>TIPO MTTO</th>
                            <th>FECHA_SOLICITUD</th>
                            <th>HR ENTRADA</th>
                            <th>HR SALIDA</th>
                            <th>PERSONAS</th>
                            <th>DETALLE</th>
                            <th>RUTA FIRMA PROVEEDOR</th>
                            <th>RUTA FIRMA GERENTE</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idAmbito}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.idCeco}</td>
                                <td>${item.idFolio}</td>
                                <td>${item.fechaVisita}</td>
                                <td>${item.nombreProvee}</td>
                                <td>${item.tipoMantento}</td>
                                <td>${item.fechaSolicitud}</td>
                                <td>${item.horaEntrada}</td>
                                <td>${item.horaSalida}</td>
                                <td>${item.numPersonas}</td>
                                <td>${item.detalle}</td>
                                <td>${item.rutaProveedor}</td>
                                <td>${item.rutaGerente}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'MINUCIAS'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>USUARIO</th>
                            <th>CECO</th>
                            <th>LUGAR HECHOS</th>
                            <th>FECHA</th>
                            <th>NOMBRE PROVEEDOR</th>
                            <th>DIRECCION</th>
                            <th>VOBO GERENTE</th>
                            <th>DESTRUYE</th>
                            <th>TESTIGO 1</th>
                            <th>TESTIGO 2</th>



                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idMinucia}</td>
                                <td>${item.idUsu}</td>
                                <td>${item.ceco}</td>
                                <td>${item.lugarEchos}</td>
                                <td>${item.fecha}</td>
                                <td>${item.direccion}</td>
                                <td>${item.vobo}</td>
                                <td>${item.destruye}</td>
                                <td>${item.testigo1}</td>
                                <td>${item.testigo2}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'MINUCIASEVI'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_EVIDENCIA</th>
                            <th>NOMBRE_ARCHIVO</th>
                            <th>RUTA</th>
                            <th>OSERVACIONES</th>
                            <th>CECO</th>


                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idevi}</td>
                                <td>${item.nombrearc}</td>
                                <td>${item.ruta}</td>
                                <td>${item.observac}</td>
                                <td>${item.ceco}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:when test="${tipo == 'FOTOAD'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>CECO</th>
                            <th>LUGAR ANTES</th>
                            <th>LUGAR DESPUES</th>
                            <th>EVIDENCIA ANTES</th>
                            <th>FECHA EVIDENCIA ANTES</th>
                            <th>EVIDENCIA DESPUES</th>
                            <th>FECHA EVIDENCIA DESPUES</th>
                            <th>PERIODO</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idEvento}</td>
                                <td>${item.idCeco}</td>
                                <td>${item.lugarAntes}</td>
                                <td>${item.lugarDespues}</td>
                                <td>${item.idEvidantes}</td>
                                <td>${item.fechaAntes}</td>
                                <td>${item.idEviddespues}</td>
                                <td>${item.fechaDespues}</td>
                                <td>${item.periodo}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'ACTFIJO'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ID DEPURA ACTFIJO</th>
                            <th>LUGAR_PLACA</th>
                            <th>LUGAR DESCRIPCION</th>
                            <th>MARCA</th>
                            <th>SERIE</th>
                            <th>CECO</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idActivoFijo}</td>
                                <td>${item.depActivoFijo}</td>
                                <td>${item.placa}</td>
                                <td>${item.descripcion}</td>
                                <td>${item.marca}</td>
                                <td>${item.serie}</td>
                                <td>${item.ceco}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'DEPACT'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>USUARIO ACTFIJO</th>
                            <th>CECO</th>
                            <th>UBICACION</th>
                            <th>INVOLUCRADOS</th>
                            <th>RAZON SOCIAL</th>
                            <th>RFC</th>
                            <th>DOMICILIO</th>
                            <th>RESPONSABLE</th>
                            <th>TESTIGO</th>
                            <th>FECHA</th>
                            <th>PRECIO</th>
                            <th>ID ACTFIJO</th>
                            <th>DESCRIPCION</th>
                            <th>SERIE</th>
                            <th>MARCA</th>
                            <th>PLACA</th>
                            <th>ID_EVIDEN</th>
                            <th>NOMBRE_ARC</th>
                            <th>RUTA</th>
                            <th>DESCR</th>
                            <th>PERIODO</th>
                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idDepAct}</td>
                                <td>${item.idusuario}</td>
                                <td>${item.ceco}</td>
                                <td>${item.ubicacion}</td>
                                <td>${item.involucrados}</td>
                                <td>${item.razonsoc}</td>
                                <td>${item.rfc}</td>
                                <td>${item.domicilio}</td>
                                <td>${item.responsable}</td>
                                <td>${item.testigo}</td>
                                <td>${item.fecha}</td>
                                <td>${item.precio}</td>
                                <td>${item.idactivofijo}</td>
                                <td>${item.descripcion}</td>
                                <td>${item.placa}</td>
                                <td>${item.marca}</td>
                                <td>${item.serie}</td>
                                <td>${item.idEvi}</td>
                                <td>${item.nombreArc}</td>
                                <td>${item.nombreArc}</td>
                                <td>${item.ruta}</td>
                                <td>${item.desc}</td>
                                <td>${item.periodo}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'EXPEDIENTEINAC'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>USUARIO ACTFIJO</th>
                            <th>CECO</th>
                            <th>FECHA</th>
                            <th>STATUS</th>
                            <th>DESCRIPCION</th>
                            <th>CIUDAD</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idExpediente}</td>
                                <td>${item.idGerente}</td>
                                <td>${item.idCeco}</td>
                                <td>${item.fecha}</td>
                                <td>${item.estatusExpediente}</td>
                                <td>${item.descripcion}</td>
                                <td>${item.Ciudad}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'EXPRESPONS'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>USUARIO </th>
                            <th>NOMBRE</th>
                            <th>STATUS</th>
                            <th>ID EXPEDIENTEINAC</th>


                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idGerente}</td>
                                <td>${item.identificador}</td>
                                <td>${item.nombreResp}</td>
                                <td>${item.estatusResp}</td>
                                <td>${item.idExpediente}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'AREAAPOYO'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID AREAAPOYO</th>
                            <th>CECO </th>
                            <th>USUARIO</th>
                            <th>FOLIO</th>
                            <th>AREA INVOLUCRADA</th>
                            <th>PRIORIDAD</th>
                            <th>STATUS</th>
                            <th>ID_EVID</th>
                            <th>NOMBRE ARCHIVO</th>
                            <th>RUTA</th>
                            <th>FECHA</th>
                            <th>OBSERVACIONES</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idArea}</td>
                                <td>${item.ceco}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.folio}</td>
                                <td>${item.areaInv}</td>
                                <td>${item.prioridad}</td>
                                <td>${item.status}</td>
                                <td>${item.ruta}</td>
                                <td>${item.idEvid}</td>
                                <td>${item.nombreArc}</td>
                                <td>${item.periodo}</td>
                                <td>${item.descr}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'DHL'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID GUIA</th>
                            <th>ID_EVIDENCIA </th>
                            <th>USUARIO</th>
                            <th>CECO</th>
                            <th>NOMBRE ARCHIVO</th>
                            <th>RUTA</th>
                            <th>FECHA</th>
                            <th>STATUS</th>
                            <th>OBSERVACIONES</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idGuia}</td>
                                <td>${item.idEvid}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.ceco}</td>
                                <td>${item.nombreArch}</td>
                                <td>${item.ruta}</td>
                                <td>${item.periodo}</td>
                                <td>${item.status}</td>
                                <td>${item.descr}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'CARTAASIG'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_CARTA</th>
                            <th>ID_USUARIO </th>
                            <th>CECO</th>
                            <th>NOMBRE ARCHIVO</th>
                            <th>RUTA</th>
                            <th>FECHA</th>
                            <th>STATUS</th>
                            <th>OBSERVACIONES</th>

                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idCarta}</td>
                                <td>${item.idUsuario}</td>
                                <td>${item.ceco}</td>
                                <td>${item.nombreArc}</td>
                                <td>${item.ruta}</td>
                                <td>${item.perido}</td>
                                <td>${item.status}</td>
                                <td>${item.observ}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

           


          

            <c:when test="${tipo == 'ESTPUESTOMUEBLE'}">
                <table>
                    <thead>
                        <tr>
                            <th>ID_MUEBLE</th>
                            <th>ID</th>
                            <th>SUCURSAL</th>
                            <th>MUEBLE 1 CAJON 1</th>
                            <th>MUEBLE1 CAJON2</th>
                            <th>MUEBLE1 CAJON3</th>
                            <th>MUEBLE2 CAJON4</th>
                            <th>MUEBLE2 CAJON5</th>
                            <th>MUEBLE2 CAJON6</th>
                            <th>FCPERIODO</th>
                            <th>BANDERA</th>
                            <th>PUESTO</th>


                        </tr>
                    </thead>
                    <tbody>


                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idMueble}</td>
                                <td>${item.idPuesto}</td>
                                <td>${item.ceco}</td>
                                <td>${item.mueble1caj1}</td>
                                <td>${item.mueble1caj2}</td>
                                <td>${item.mueble1caj3}</td>
                                <td>${item.mueble2caj1}</td>
                                <td>${item.mueble2caj2}</td>
                                <td>${item.mueble2caj6}</td>
                                <td>${item.periodo}</td>
                                <td>${item.bandera}</td>
                                <td>${item.puesto}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'HORAS_PICO_PASO'}">
                <table>
                    <thead>
                        <tr>
                            <th>FIPAIS</th>
                            <th>FICANAL</th>
                            <th>FCCANAL</th>
                            <th>FISUCURSAL</th>
                            <th>FCSUCURSAL</th>
                            <th>FISEMANA</th>
                            <th>FIHORASMETA</th>
                            <th>FIHORASCUMP</th>
                            <th>FIPORHRCUMP</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.fiPais}</td>
                                <td>${item.fiCanal}</td>
                                <td>${item.fcCanal}</td>
                                <td>${item.fiSucursal}</td>
                                <td>${item.fcSucursal}</td>
                                <td>${item.fiSemana}</td>
                                <td>${item.fiHorasMeta}</td>
                                <td>${item.fiHorasCump}</td>
                                <td>${item.fiHorasPorCump}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'PUESTO'}">
                <table align="left">
                    <thead>
                        <tr>
                            <th>ID_PUESTO</th>
                            <th>DESCRIPCION</th>

                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idPuesto}</td>
                                <td>${item.descripcion}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:when test="${tipo == 'PETICIONES_REMEDY'}">
                <table align="left">
                    <thead>
                        <tr>
                            <th>FECHA</th>
                            <th>ORIGEN</th>
                            <th>PETICIONES</th>

                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.fechaPeticion}</td>
                                <td>${item.origenPeticion}</td>
                                <td>${item.conteoPeticion}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>

            <c:when test="${tipo == 'PERFILESUSU'}">
                <table align="left">
                    <thead>
                        <tr>
                            <th>ID USUARIO</th>
                            <th>ID PERFIL</th>
                            <th>ID ACTOR</th>
                            <th>BANNUEVOFLUJ</th>
                            <th>TIPOPROY</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idUsuario}</td>
                                <td>${item.idPerfil}</td>
                                <td>${item.idActor}</td>
                                <td>${item.bandNvoEsq}</td>
                                <td>${item.tipoProyecto}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>


            <c:otherwise>
                <p>ERROR!!!!</p>
            </c:otherwise>
        </c:choose>
    </body>
</html>