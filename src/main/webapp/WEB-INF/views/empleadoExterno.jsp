<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



        <script type="text/javascript" src="../js/script-menu.js"></script>

        <link rel="stylesheet" type="text/css" href="../css/estilos.css">
        <link rel="stylesheet" type="text/css" href="../css/header-style.css">
        <link rel="stylesheet" type="text/css" href="../css/menuCheck.css">
        <link rel="stylesheet" type="text/css" href="../css/carousel.css">

        <script	src="../js/jquery/jquery-2.2.4.min.js"></script>

        <script src="../js/script-storeIndex.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>

        <title>Carga Usuarios Externos</title>

    </head>
    <body>

        <tiles:insertTemplate template="/WEB-INF/templates/templateHeader.jsp" flush="true">
            <tiles:putAttribute name="cabecera" value="/WEB-INF/templates/templateHeader.jsp" />
        </tiles:insertTemplate>

        <tiles:insertTemplate template="/WEB-INF/views/menu.jsp" flush="true">
            <tiles:putAttribute name="menu" value="/WEB-INF/views/menu.jsp" />
        </tiles:insertTemplate>
        <h1>Subir Archivo Excel</h1>
        <br>
        <c:choose>
            <c:when test="${OKARCH=='OK'} ">
                <iframe src="http://10.53.33.82/franquicia/imagenes/${NomArchivo}" style="width: 90%;heigth:70%" frameborder="0"></iframe>
                <br>
                <br>
                <c:url value="/central/uploadExpedienteFiles.htm" var="uploadFilesUrl"/>
                <form:form method="POST" action="${uploadFilesUrl}" modelAttribute="command" name="form-uploadFiles" id="form-uploadFiles" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputFile">Actualizar el archivo</label>
                        <input type="hidden" id="fileAction" name="fileAction" value="${fileAction}"/>
                        <input type="hidden" id="fileRoute" name="fileRoute" value="${fileRoute}}"/>
                        <input type="file" id="uploadedFile" name="uploadedFile"/>
                        <input type="submit" class="btn btn-default btn-lg" value="Subir Archivo" onclick="return checkInput()"/>
                        <p class="help-block">Tamaño maximo 100MB</p>
                    </div>
                </form:form>
            </c:when>
            <c:otherwise>
                <c:url value="/central/uploadExpedFiles.htm" var="uploadFilesUrl"/>
                <form:form metod="POST" action="${uploadFilesUrl}" modelAtributte="command" name="form-uploadFiles" id="form-uploadFiles" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputFile">Subir Archivo</label>
                        <input type="hidden" id="fileAction" name="fileAction" value="${fileAction}"/>
                        <input type="hidden" id="fileRoute" name="fileRoute" value="${fileRoute}"/>
                        <input type="file" id="uploadedFile" name="uploadedFile"/>
                        <input type="submit" class="btn btn-default btn-lg" value="Subir archivo" />
                        <p class="help-block">Tamaño Maximo 100MB</p>
                    </div>
                </form:form>

            </c:otherwise>
        </c:choose>
        <script>
            function checkInput() {
                var tAction = document.getElementById("fileRoute").value;
                var oFile = document.getElementById("uploadedFile").files[0];

                if (tAction == '' || tAction == null) {
                    alert("La ruta no puede ir vacía");
                    return false;
                } else {
                    if (oFile.size > (1024 * 1024 * 100)) {
                        alert("El archivo pesa más de 100MB!");
                        return false;
                    } else {
                        return true;
                    }
                }
            }
            function confirmRename()
            {
                var renameFile = document.getElementById("newFileName").value;
                var renameConfirm = confirm("Desea Renombrar el archivo");

                if (renameConfirm == true)
                {
                    if (renameConfirm == '' || renameConfirm == null)
                    {
                        alert("No puede ir El nombre del archivo vacio");
                        return false;
                    } else {
                        alert("Debes seleccionar un archivo antes de realizar otra accion")
                    }
                } else {
                    return false;
                }
            }

            function cofirmDelete()
            {
                var retVal = confirm("Desea eliminar Archivo");

                if (retVal == true)
                {
                    return true;
                } else {
                    return false;
                }
            }
        </script>

    </body>
</html>