<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
    <tiles:importAttribute name="javascripts"/>
    <tiles:importAttribute name="stylesheets"/>
    <head>
        <title>
            <tiles:insertAttribute name="title" ignore="true" />
        </title>

        <c:if test="${stylesheets !=null }">
            <c:forEach var="css" items="${stylesheets}">
                <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
            </c:forEach>
        </c:if>
        <style type="text/css">
            .vertical-center {
                min-height: 100%;
                min-height: 100vh;
                display: flex;
                align-items: center;
            }
        </style>
    </head>
    <body>
        <tiles:insertAttribute name="body"/>
        <c:if test="${javascripts !=null }">
            <c:forEach var="script" items="${javascripts}">
                <script src="<c:url value="${script}"/>"></script>
            </c:forEach>
        </c:if>
    </body>
</html>