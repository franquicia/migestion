/*VALIDA CAMPOS EN EL FORMULARIO SUBIR ARCHIVO*/
function validaFormArchive() {
    document.getElementById('error').innerHTML = '';
    if (document.getElementById("titulo").value == null
            || document.getElementById("titulo").valuelength == 0
            || /^\s*$/.test(document.getElementById("titulo").value)) {
        document.getElementById('error').innerHTML = '[ERROR] El campo TITULO debe tener un valor';
        return false;
    } else if (document.getElementById("descripcion").value == null
            || document.getElementById("descripcion").valuelength == 0
            || /^\s*$/.test(document.getElementById("descripcion").value)) {
        document.getElementById('error').innerHTML = '[ERROR] El campo DESCRIPCION debe tener un valor';
        return false;
    } else if (document.getElementById("file").value == null
            || document.getElementById("file").valuelength == 0
            || /^\s*$/.test(document.getElementById("descripcion").value)) {
        document.getElementById('error').innerHTML = '[ERROR] El campo FILE debe tener un valor';
        return false;
    }
}

/* VALIDACIONES SCHEDULER */
function initScheduler() {
    document.getElementById("hora").style.visibility = "hidden";
    document.getElementById("minutos").style.visibility = "hidden";
    document.getElementById("segundos").style.visibility = "hidden";
    document.getElementById("dia").style.visibility = "hidden";
    document.getElementById("mes").style.visibility = "hidden";
    document.getElementById("h").style.visibility = "hidden";
    document.getElementById("m").style.visibility = "hidden";
    document.getElementById("s").style.visibility = "hidden";
    document.getElementById("d").style.visibility = "hidden";
    document.getElementById("month").style.visibility = "hidden";
    document.getElementById("diaSemana").style.visibility = "hidden";
    document.getElementById("ds").style.visibility = "hidden";

    document.getElementById("log-btn0").style.visibility = "hidden";
    document.getElementById("log-btn2").style.visibility = "hidden";
    document.getElementById("log-btn3").style.visibility = "hidden";
    document.getElementById("tarea").style.visibility = "hidden";
}


function habilitaScheduler() {
    document.getElementById("hora").style.visibility = "visible";
    document.getElementById("minutos").style.visibility = "visible";
    document.getElementById("segundos").style.visibility = "visible";
    document.getElementById("dia").style.visibility = "visible";
    document.getElementById("mes").style.visibility = "visible";
    document.getElementById("h").style.visibility = "visible";
    document.getElementById("m").style.visibility = "visible";
    document.getElementById("s").style.visibility = "visible";
    document.getElementById("d").style.visibility = "visible";
    document.getElementById("month").style.visibility = "visible";
    document.getElementById("diaSemana").style.visibility = "visible";
    document.getElementById("ds").style.visibility = "visible";

    document.getElementById("log-btn0").style.visibility = "hidden";
    document.getElementById("log-btn1").style.visibility = "visible";
    document.getElementById("log-btn2").style.visibility = "visible";
    document.getElementById("log-btn3").style.visibility = "visible";
    document.getElementById("tarea").style.visibility = "hidden";


}

function validaScheduler() {
    var x = document.getElementsByName("id");
    var i;
    var flag;
    for (i = 0; i < x.length; i++) {
        if (document.getElementById("guarda").localeCompare("guarda") == -1) {
            if (x[i].checked == false) {
                flag = false;
            } else if (x[i].checked == true) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            alert("Selecciona una Tarea");
        }
    }
    return flag;
}

function validaGuardadoScheduler() {
    if (document.getElementById("nombreTarea").value == null
            || document.getElementById("nombreTarea").valuelength == 0
            || /^\s*$/.test(document.getElementById("nombreTarea").value)) {
        alert("INGRESA UN VALOR VALIDO");
        return false;
    } else
        return true;
}

function nuevaTarea() {
    document.getElementById("tarea").style.visibility = "visible";
    document.getElementById("hora").style.visibility = "visible";
    document.getElementById("minutos").style.visibility = "visible";
    document.getElementById("segundos").style.visibility = "visible";
    document.getElementById("dia").style.visibility = "visible";
    document.getElementById("mes").style.visibility = "visible";
    document.getElementById("h").style.visibility = "visible";
    document.getElementById("m").style.visibility = "visible";
    document.getElementById("s").style.visibility = "visible";
    document.getElementById("d").style.visibility = "visible";
    document.getElementById("month").style.visibility = "visible";
    document.getElementById("diaSemana").style.visibility = "visible";
    document.getElementById("ds").style.visibility = "visible";

    document.getElementById("log-btn0").style.visibility = "visible";
    document.getElementById("log-btn1").style.visibility = "hidden";
    document.getElementById("log-btn2").style.visibility = "hidden";
    document.getElementById("log-btn3").style.visibility = "hidden";
    var x = document.getElementsByName("id");
    var i;

    for (i = 0; i < x.length; i++) {
        x[i].checked = false;
    }
}

