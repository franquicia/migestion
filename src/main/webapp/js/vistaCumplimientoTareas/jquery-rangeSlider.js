function slider(min, max, step, value, indicador) {
    var totalPasos = (max - min) / step;				//Cantidad de pasos
    var pasos = $('#slider').width() / totalPasos;	//Ancho del paso en pixeles

    function init(ui) {
        /* Actualizar valor */
        var valor = getValor(ui, value);
        formato(valor);

        /* Position */
        var pos = position($(this), ui, value);
        $(indicador).css('left', pos);
    }

    $("#slider").slider({
        min: min,
        max: max,
        value: value,
        range: "min",
        step: step,
        animate: true,
        create: function (event, ui) {
            init(ui);
            indicadores();
        },
        slide: function (event, ui) {
            init(ui);
        }
    });

    /* Coloca las lineas indicadoras */
    function indicadores() {
        /* Elementos li */
        var lista = $(indicador).siblings('ul');
        for (var i = 1; i <= totalPasos; i++) {
            if (i != totalPasos) {
                lista.append('<li>|</li>');
            } else {
                lista.append('<li><table><tr><td class="tLeft">|</td><td class="tRight">|</td></tr></table></li>');
            }
        }
        ;

        /* Ancho de los li */
        lista.children().each(function (index, el) {
            var finalW = 99 / totalPasos;
            $(this).css('width', finalW + '%');
        });
    }

    /* Regresa valor de selección en formato monetareo */
    function formato(valor) {
        var moneda = valor;
        var formato = '$' + moneda.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $(indicador).text(formato);
    }

    /* Regresa la posición del indicador de acuerdo al valor de selección */
    function position(objeto, ui, value) {
        var valor = getValor(ui, value);
        var actual = (valor - min) / step;
        var finalPaso = (pasos * actual) - ($(indicador).outerWidth() * 0.5);
        return finalPaso;
    }

    /* Función que regresa el default del valor */
    function getValor(ui, value) {
        var valor;
        if (ui.value == null) {
            valor = value;
        } else {
            valor = ui.value;
        }
        return valor;
    }
}
;

function slider1(min, max, step, value, indicador) {
    var totalPasos = (max - min) / step;				//Cantidad de pasos
    var pasos = $('#slider1').width() / totalPasos;	//Ancho del paso en pixeles

    function init(ui) {
        /* Actualizar valor */
        var valor = getValor(ui, value);
        formato(valor);

        /* Position */
        var pos = position($(this), ui, value);
        $(indicador).css('left', pos);
    }

    $("#slider1").slider({
        min: min,
        max: max,
        value: value,
        range: "min",
        step: step,
        animate: true,
        create: function (event, ui) {
            init(ui);
            indicadores();
        },
        slide: function (event, ui) {
            init(ui);
        }
    });

    /* Coloca las lineas indicadoras */
    function indicadores() {
        /* Elementos li */
        var lista = $(indicador).siblings('ul');
        for (var i = 1; i <= totalPasos; i++) {
            if (i != totalPasos) {
                lista.append('<li>|</li>');
            } else {
                lista.append('<li><table><tr><td class="tLeft">|</td><td class="tRight">|</td></tr></table></li>');
            }
        }
        ;

        /* Ancho de los li */
        lista.children().each(function (index, el) {
            var finalW = 99 / totalPasos;
            $(this).css('width', finalW + '%');
        });
    }

    /* Regresa valor de selección en formato monetareo */
    function formato(valor) {
        var moneda = valor;
        var formato = '$' + moneda.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $(indicador).text(formato);
    }

    /* Regresa la posición del indicador de acuerdo al valor de selección */
    function position(objeto, ui, value) {
        var valor = getValor(ui, value);
        var actual = (valor - min) / step;
        var finalPaso = (pasos * actual) - ($(indicador).outerWidth() * 0.5);
        return finalPaso;
    }

    /* Función que regresa el default del valor */
    function getValor(ui, value) {
        var valor;
        if (ui.value == null) {
            valor = value;
        } else {
            valor = ui.value;
        }
        return valor;
    }
}
;