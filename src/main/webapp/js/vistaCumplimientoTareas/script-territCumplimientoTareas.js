$body = $("body");
$(document).on({
    ajaxStart: function () {
        $body.addClass("loading");
    },
    ajaxStop: function () {
        $body.removeClass("loading");
    }
});

$(window).load(function () {
    fecha();
});

function myFunction() {
    $body.removeClass("loading");
    location.reload();
}

function nobackbutton() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button" //chrome
    window.onhashchange = function () {
        window.location.hash = "";
    }
}

var porcentajeL = 0;

function fecha() {

    document.getElementById("idDiaActual").innerHTML = "";
    document.getElementById("idTotalDias").innerHTML = "";


    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yy = hoy.getFullYear();

    var meses = new Array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');

    var mesInicial = seleccionMesL;
    var anoInicial = seleccionAnoL;
    var diasMes = new Date(anoInicial || new Date().getFullYear(), mesInicial, 0).getDate();
    var ddF = dd;
    var porcentaje = 0;
    var por2 = "";
    var f = new Date();

    if ((f.getMonth() + 1) == mesInicial && f.getFullYear() == anoInicial) {
        porcentaje = ((f.getDate() - 1) * 100) / diasMes;
    } else {
        ddF = diasMes;
        porcentaje = 100;
    }

    por2 = "" + porcentaje;
    var porcenI = por2.split(".");
    porcentajeL = porcenI[0];

    document.getElementById("idDiaActual").innerHTML = ddF + " de " + meses[mm - 1] + " es de " + porcentajeL + "%";
    document.getElementById("idTotalDias").innerHTML = diasMes;
    document.getElementById("idBarPor").innerHTML = porcentajeL + "%";
    document.getElementById("idBarPor").style.width = porcentajeL + "%";

}


function filtraSucursal(numOp) {
    //idCecoHijo,idCheck,seleccionAno,seleccionMes,seleccionCanal,seleccionPais
    /*
     setTimeout(myFunction, 20000);
     $body.addClass("loading");
     $("#formDetalle").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
     $("#formDetalle").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+idCecoPadreL+"' />");
     $("#formDetalle").append("<input type='hidden' name='idCeco' id='idCeco' value='"+idCecoL+"' />");
     $("#formDetalle").append("<input type='hidden' name='idCecoHijo' id='idCecoHijo' value='"+idCecoHijo+"' />");
     $("#formDetalle").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+nombreCecoL+"' />");
     $("#formDetalle").append("<input type='hidden' name='idCheck' id='idCheck' value='"+idCheck+"' />");
     $("#formDetalle").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfilL+"' />");
     $("#formDetalle").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='"+banderaCecoPadreL+"' />");
     $("#formDetalle").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+seleccionAno+"' />");
     $("#formDetalle").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+seleccionMes+"' />");
     $("#formDetalle").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+seleccionCanal+"' />");
     $("#formDetalle").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+seleccionPais+"' />");
     */
    document.getElementById("formDetalle").submit();
    return true;
}
var valida = -1;
function muestraSucursal(numOp) {
    var adjuntaOtro = "";

    if (valida != numOp) {
        valida = numOp;
        document.getElementById("idLlenaSucursales" + numOp).innerHTML = "";
        //alert(" "+idCeco+" "+idCheck+" "+seleccionAno+" "+seleccionMes+" "+seleccionCanal+" "+seleccionPais);

        /*$.ajax({
         type : "GET",
         url : '../../ajaxFiltroSucursal.json?idCeco='+idCeco+'&seleccionAno='+seleccionAno
         + '&seleccionMes='+seleccionMes+'&seleccionCanal='+seleccionCanal+'&seleccionPais='+seleccionPais+'&valorPila='+1,
         contentType : "application/json; charset=utf-8",
         dataType : "json",
         delimiter : ",",
         success : function(json) {

         if (json == null) {

         $("#idLlenaSucursales"+numOp).append("<span class='Gris1'>Sin Datos</span>");
         }else{
         */
        for (var a = 1; a < 5; a++) {

            var adjunta = "";
            for (var b = 0; b < 1; b++) {
                adjunta += "<td style='width: 2%;'>" +
                        //(json[a][b].total<porcentajeG ? "<div class='radio rojo'>":"<div class='radio verde'>")+
                        "<div class='radio rojo'>" +
                        "<div class='radio1'>" +
                        numOp + "" + (a) + "%" +
                        "</div>" +
                        "</div>" +
                        "</td>";
            }

            adjuntaOtro += "<tr class='pointer' onclick='filtraSucursal(" + numOp + ");'>" +
                    "<td>ID CECO <span>NOMBRE suc</span></td>" +
                    adjunta +
                    "</tr>";

        }

        $("#idLlenaSucursales" + numOp).append("" +
                "<table class='bloquer subnivel'>" +
                "<tbody>" +
                "<tr> PAPA NUMERO" + numOp +
                //(Object.keys(json[0]).length<4 ? "<td colspan='4'><span>Zona</span></td>":"<td colspan='"+(Object.keys(json[0]).length)+(1)+"'><span>Zona</span></td>")+
                "</tr>" +
                adjuntaOtro + "" +
                "</tbody>" +
                "</table>");

        /*	var retornaB = varBefore(json[a][0].asignados,json[a][0].terminados);
         $("#"+json[a][0].idCeco).append("" +
         "<style>.accordion dt b"+json[a][0].idCeco+"::before{" +
         "content: '"+json[a][0].terminados+" de "+json[a][0].asignados+"';"+
         "background-image: "+retornaB+";"+
         "background-size:50px;"+
         "background-repeat: no-repeat;"+
         "text-align: center;"+
         "float: right;"+
         "width: 50px;"+
         "height: 24px;"+
         "}</style>");*/
    }
    ocultaPrueba();
    //	}
    //	},
    //error : function() {
    //alert('Favor de actualizar la pagina');
    //}
    //});
}


function ocultaPrueba() {
    jQuery(function () {
        $(".subSeccion").hide();
        var allPanels = $('.accordion > dd').hide();
        jQuery('.accordion > dt').on("click", function () {
            $this = $(this);
            //the target panel content
            $target = $this.next();

            jQuery('.accordion > dt').removeClass('accordion-active');
            if ($target.hasClass("in")) {
                $this.removeClass('accordion-active');
                $target.slideUp();
                $target.removeClass("in");
                $(".subSeccion").hide();

            } else {
                $this.addClass('accordion-active');
                jQuery('.accordion > dd').removeClass("in");
                $target.addClass("in");
                $(".subSeccion").show();

                jQuery('.accordion > dd').slideUp();
                $target.slideDown();
            }
        })
    })
}



function regresaVistaAnterior(nivelPerfil, idCecoPadre, nombreCeco, banderaCecoPadre, seleccionAno, seleccionMes, seleccionCanal, seleccionPais) {
    setTimeout(myFunction, 20000);
    $body.addClass("loading");
    /*if(nivelPerfil==0 || nivelPerfil==1){
     $("#formFiltro").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
     $("#formFiltro").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='"+banderaCecoPadre+"' />");
     $("#formFiltro").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfil+"' />");
     $("#formFiltro").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+idCecoPadre+"' />");
     $("#formFiltro").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+nombreCeco+"' />");
     $("#formFiltro").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+seleccionAno+"' />");
     $("#formFiltro").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+seleccionMes+"' />");
     $("#formFiltro").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+seleccionCanal+"' />");
     $("#formFiltro").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+seleccionPais+"' />");
     document.getElementById("formFiltro").submit();
     return true;
     }else{
     */	window.location.replace("vistaCumplimientoTareas.htm");
    //}
}
