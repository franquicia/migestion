/**
 * OCULTA LOS ELEMENTOS DEL MENU DESPLAZANDO EL DIV
 */
function ocultaMenu() {
    if ($("#menu").is(":hidden")) {
        $("#menu").show("slow");
    } else {
        $("#menu").slideUp();
    }
}
