package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CartaAsignacionAFDTO;
import com.gruposalinas.migestion.mappers.CartaAsignacionMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CartaAsignacionAFDAOImpl extends DefaultDAO implements CartaAsignacionAFDAO {

	private static Logger logger = LogManager.getLogger(CartaAsignacionAFDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaCarta;
	private DefaultJdbcCall jdbcBuscaFila;
	private DefaultJdbcCall jdbcBuscaInfoCarta;
	private DefaultJdbcCall jdbcActualizaCarta;

    private List<CartaAsignacionAFDTO> listaDetU;

    public void init() {

		
		jdbcEliminaCarta = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCARTASIG")
				.withProcedureName("SP_DEL_CARTA");

		jdbcBuscaInfoCarta = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCARTASIG")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_CARTAASI", new CartaAsignacionMapper());

		jdbcBuscaFila = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCARTASIG")
				.withProcedureName("SP_SEL_PARAM")
				.returningResultSet("RCL_CARTAASI", new CartaAsignacionMapper());



		jdbcActualizaCarta = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCARTASIG")
				.withProcedureName("SP_ACT_CARTA");


	}
	@Override
	public boolean elimina(String idCarta) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDCARTA",idCarta);
		
		out = jdbcEliminaCarta.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMCARTASIG.RCL_CARTAASI}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}

	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<CartaAsignacionAFDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<CartaAsignacionAFDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaInfoCarta.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMCARTASIG.SP_SEL_DETALLE}");
        listaDetU = (List<CartaAsignacionAFDTO>) out.get("RCL_CARTAASI");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las Carta");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(CartaAsignacionAFDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCARTA", bean.getIdCarta())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_ARCHIVO", bean.getNombreArc())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERVACION", bean.getObserv())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcActualizaCarta.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMCARTASIG.SP_ACT_CARTA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
