package com.gruposalinas.migestion.dao;

public interface TruncTableTokenDAO {

    public boolean truncTable() throws Exception;

}
