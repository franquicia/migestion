package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.DepuraActFijoDTO;
import com.gruposalinas.migestion.mappers.DepuraActFijoEvidRowMapper;
import com.gruposalinas.migestion.mappers.DepuraActFijoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DepuraActFijoDAOImpl extends DefaultDAO implements DepuraActFijoDAO {

	private static Logger logger = LogManager.getLogger(DepuraActFijoDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaDepAct;
	private DefaultJdbcCall jdbcBuscaInfoDepAct;
	private DefaultJdbcCall jdbcActualizaDepAct;
	
	private DefaultJdbcCall jdbcInsertaDepActEvi;
	private DefaultJdbcCall jdbcBuscaFilaEvi;
	private DefaultJdbcCall jdbcBuscaInfoDepActEvi;
	private DefaultJdbcCall jdbcActualizaDepActEvi;

    private List<DepuraActFijoDTO> listaDetU;
    private List<DepuraActFijoDTO> listaEvi;

    public void init() {

		
		jdbcEliminaDepAct = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDEPACTFIJ")
				.withProcedureName("SP_DEL_DEPACT");

		jdbcBuscaInfoDepAct = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDEPACTFIJ")
				.withProcedureName("SP_SEL_DEPACTFIJ")
				.returningResultSet("RCL_DEPACTFIJ", new DepuraActFijoRowMapper());





		jdbcActualizaDepAct = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDEPACTFIJ")
				.withProcedureName("SP_ACT_DEPACT");

		// Evidencia

		jdbcInsertaDepActEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDEPACTFIJ")
				.withProcedureName("SP_INS_DEPEVI");


		jdbcBuscaInfoDepActEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDEPACTFIJ")
				.withProcedureName("SP_SEL_DEPEVID")
				.returningResultSet("RCL_DEPEVI", new DepuraActFijoEvidRowMapper());

		jdbcBuscaFilaEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDEPACTFIJ")
				.withProcedureName("SP_SEL_PAEVI")
				.returningResultSet("RCL_DEPEVI", new DepuraActFijoEvidRowMapper());



		jdbcActualizaDepActEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDEPACTFIJ")
				.withProcedureName("SP_ACT_DEPEVI");



	}
	@Override
	public boolean elimina(int idDepAct) throws Exception {
		int respuesta = 0;

		Map<String, Object> out = null;

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_DEPACTFIJ",idDepAct);

		out = jdbcEliminaDepAct.execute(in);

		logger.info("Funcion ejecutada: {GESTION.PAADMDEPACTFIJ.SP_DEL_DEPACT}");

		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();


		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}


		return false;
	}
	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<DepuraActFijoDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<DepuraActFijoDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaInfoDepAct.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMDEPACTFIJ.SP_SEL_DEPACTFIJ}");
        listaDetU = (List<DepuraActFijoDTO>) out.get("RCL_DEPACTFIJ");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las DepAct");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(DepuraActFijoDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_DEPACTFIJ", bean.getIdDepAct())
                .addValue("PA_IDUSUARIO", bean.getIdusuario())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_UBICACION", bean.getUbicacion())
                .addValue("PA_INVOLUCRADOS", bean.getInvolucrados())
                .addValue("PA_RAZONSOC", bean.getRazonsoc())
                .addValue("PA_RFC", bean.getRfc())
                .addValue("PA_DOMICILIO", bean.getDomicilio())
                .addValue("PA_RESPONSABLE", bean.getResponsable())
                .addValue("PA_TESTIGO", bean.getTestigo())
                .addValue("PA_FECHA_HORA", bean.getFecha())
                .addValue("PA_PRECIO", bean.getPrecio());

        out = jdbcActualizaDepAct.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMDEPACTFIJ.SP_ACT_DEPACT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public int insertaEvi(DepuraActFijoDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_DEPACTFIJ", bean.getIdDepAct())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_NOMBRE", bean.getNombreArc())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERVACION", bean.getDesc());

        out = jdbcInsertaDepActEvi.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMDEPACTFIJ.SP_INS_DEPEVI}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_EVI");
        idNota = idbloc.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<DepuraActFijoDTO> obtieneDatosEvi(String ceco) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcBuscaFilaEvi.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMDEPACTFIJ.SP_SEL_PAEVI}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaEvi = (List<DepuraActFijoDTO>) out.get("RCL_DEPEVI");

        if (respuesta != 1) {
            logger.info("Algo paso al consular la evidencia ");
        }

        return listaEvi;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<DepuraActFijoDTO> obtieneInfoEvi() throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        out = jdbcBuscaInfoDepActEvi.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMDEPACTFIJ.SP_SEL_DEPACTFIJ}");
        listaEvi = (List<DepuraActFijoDTO>) out.get("RCL_DEPEVI");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las DepAct");
        } else {
            return listaEvi;
        }

        return listaEvi;
    }

    public boolean actualizaEvi(DepuraActFijoDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_EVI", bean.getIdEvi())
                .addValue("PA_DEPACTFIJ", bean.getIdDepAct())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_NOMBRE", bean.getNombreArc())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERVACION", bean.getDesc());

        out = jdbcActualizaDepActEvi.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMDEPACTFIJ.SP_ACT_DEPEVI}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
