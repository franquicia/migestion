package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CecoComboDTO;
import com.gruposalinas.migestion.domain.InfoAsesoresDigitalesDTO;
import com.gruposalinas.migestion.domain.NotificaAfDTO;
import com.gruposalinas.migestion.domain.TokensAsesoresDTO;
import com.gruposalinas.migestion.mappers.CecoAsesoresRowMapper;
import com.gruposalinas.migestion.mappers.NotificaAfRowMapper;
import com.gruposalinas.migestion.mappers.TokensAsesoresRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsesoresDigitalesDAOImpl extends DefaultDAO implements AsesoresDigitalesDAO {

    private Logger logger = LogManager.getLogger(AsesoresDigitalesDAOImpl.class);

    private DefaultJdbcCall jdbcGetCecos;
    private DefaultJdbcCall jdbcInsertCompAvan;
    private DefaultJdbcCall jdbcGetCompromisosAvances;
    private DefaultJdbcCall jdbcGetCecosToUpdate;
    private DefaultJdbcCall jdbcUpdateCeco;

    /*Administradores*/
    private DefaultJdbcCall jdbcConsulta;
    private DefaultJdbcCall jdbcInserta;
    private DefaultJdbcCall jdbcActualiza;
    private DefaultJdbcCall jdbcElimina;
    private DefaultJdbcCall jdbcTruncate;

    public void init() {

        jdbcGetCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PANOTIFASESORES")
                .withProcedureName("SP_GET_CECOS_SF")
                .returningResultSet("RCL_CECOS", new CecoAsesoresRowMapper());

        jdbcInsertCompAvan = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PANOTIFASESORES")
                .withProcedureName("SP_INSERT_INFO");

        jdbcGetCompromisosAvances = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PANOTIFASESORES")
                .withProcedureName("SP_GET_INFO")
                .returningResultSet("RCL_CONSULTA", new TokensAsesoresRowMapper());

        jdbcGetCecosToUpdate = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMNOTIFICA_A")
                .withProcedureName("SP_CONSULTA_CECO")
                .returningResultSet("RCL_CONSULTA", new NotificaAfRowMapper());

        jdbcUpdateCeco = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMNOTIFICA_A")
                .withProcedureName("SP_ACTUALIZA");

        /*JDBC para administradores de la tabla*/
        jdbcConsulta = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMNOTIFICA_A")
                .withProcedureName("SP_CONSULTA")
                .returningResultSet("RCL_CONSULTA", new NotificaAfRowMapper());

        jdbcInserta = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMNOTIFICA_A")
                .withProcedureName("SP_INSERTA");

        jdbcActualiza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMNOTIFICA_A")
                .withProcedureName("SP_ACTUALIZA");

        jdbcElimina = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMNOTIFICA_A")
                .withProcedureName("SP_ELIMINA");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CecoComboDTO> getCecos() throws Exception {

        Map<String, Object> out = null;
        List<CecoComboDTO> cecosSF = null;
        int ejecucion = 0;

        out = jdbcGetCecos.execute();

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_GET_CECOS_SF}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrio al consultar los CECOS");
        } else {
            cecosSF = (List<CecoComboDTO>) out.get("RCL_CECOS");
        }

        return cecosSF;
    }

    @Override
    public boolean insertInfoSIE(InfoAsesoresDigitalesDTO infoAsesoresDigitales) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", infoAsesoresDigitales.getCeco())
                .addValue("PA_COMPRO_P", infoAsesoresDigitales.getCompromiso_personales())
                .addValue("PA_AVANCE_P", infoAsesoresDigitales.getAvance_personales())
                .addValue("PA_COMPRO_A", infoAsesoresDigitales.getCompromiso_activaciones())
                .addValue("PA_AVANCE_A", infoAsesoresDigitales.getAvance_activaciones())
                .addValue("PA_FECHA_COMPRO", infoAsesoresDigitales.getFecha());

        out = jdbcInsertCompAvan.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_INSERT_INFO}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TokensAsesoresDTO> getCompromisosAvance() throws Exception {

        Map<String, Object> out = null;
        List<TokensAsesoresDTO> listaTokens = null;

        int ejecucion = 0;

        out = jdbcGetCompromisosAvances.execute();

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_GET_INFO}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrio al consultar los TOKENS");
        } else {
            listaTokens = (List<TokensAsesoresDTO>) out.get("RCL_CONSULTA");
        }

        return listaTokens;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NotificaAfDTO> getCecosToUpdate() throws Exception {

        Map<String, Object> out = null;
        List<NotificaAfDTO> cecos = null;

        int ejecucion = 0;

        out = jdbcGetCecosToUpdate.execute();

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_CONSULTA}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrio al consultar los cecos");
        } else {
            cecos = (List<NotificaAfDTO>) out.get("RCL_CONSULTA");
        }

        return cecos;
    }

    @Override
    public boolean updateCeco(NotificaAfDTO notificaAFDTO) {

        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOTIFICA", notificaAFDTO.getIdNotifica())
                .addValue("PA_CECO", notificaAFDTO.getCeco())
                .addValue("PA_COMPROMISO_P", notificaAFDTO.getCompromisoP())
                .addValue("PA_AVANCE_P", notificaAFDTO.getAvanceP())
                .addValue("PA_COMPROMISO_A", notificaAFDTO.getCompromisoA())
                .addValue("PA_AVANCE_A", notificaAFDTO.getAvanceA())
                .addValue("PA_FECHA_COMP", notificaAFDTO.getFecha());

        out = jdbcUpdateCeco.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMNOTIFICA_A.SP_ACTUALIZA}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NotificaAfDTO> consulta(String ceco) throws Exception {
        Map<String, Object> out = null;
        List<NotificaAfDTO> cecosSF = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcConsulta.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_CONSULTA}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrio al consultar la tabla NOTIFICA");
        } else {
            cecosSF = (List<NotificaAfDTO>) out.get("RCL_CONSULTA");
        }

        return cecosSF;

    }

    @Override
    public boolean inserta(NotificaAfDTO objInsertar) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", objInsertar.getCeco())
                .addValue("PA_COMPROMISO_P", objInsertar.getCompromisoP())
                .addValue("PA_AVANCE_P", objInsertar.getAvanceP())
                .addValue("PA_COMPROMISO_A", objInsertar.getCompromisoA())
                .addValue("PA_AVANCE_A", objInsertar.getAvanceA())
                .addValue("PA_FECHA_COMP", objInsertar.getFecha());

        out = jdbcInserta.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_INSERTA}");

        ejecucion = Integer.parseInt((String) out.get("PA_EJECUCION"));

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean actualiza(NotificaAfDTO objActualizar) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOTIFICA", objActualizar.getIdNotifica())
                .addValue("PA_CECO", objActualizar.getCeco())
                .addValue("PA_COMPROMISO_P", objActualizar.getCompromisoP())
                .addValue("PA_AVANCE_P", objActualizar.getAvanceP())
                .addValue("PA_COMPROMISO_A", objActualizar.getCompromisoA())
                .addValue("PA_AVANCE_A", objActualizar.getAvanceA())
                .addValue("PA_FECHA_COMP", objActualizar.getFecha());

        out = jdbcActualiza.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMNOTIFICA_A.SP_ACTUALIZA}");

        ejecucion = Integer.parseInt((String) out.get("PA_EJECUCION"));

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean elimina(int id) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOTIFICA", id);

        out = jdbcElimina.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_ELIMINA}");

        ejecucion = Integer.parseInt((String) out.get("PA_EJECUCION"));

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean trunca() throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        out = jdbcTruncate.execute();

        logger.info("Funcion ejecutada: {GESTION.PANOTIFASESORES.SP_TRUNCA}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

}
