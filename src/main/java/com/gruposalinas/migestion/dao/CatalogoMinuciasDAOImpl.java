package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;
import com.gruposalinas.migestion.mappers.CatalogoMinuciasEviRowMapper;
import com.gruposalinas.migestion.mappers.CatalogoMinuciasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CatalogoMinuciasDAOImpl extends DefaultDAO implements CatalogoMinuciasDAO {

	private static Logger logger = LogManager.getLogger(CatalogoMinuciasDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaMinucias;
	private DefaultJdbcCall jdbcBuscaMinuciaEvi;
	private DefaultJdbcCall jdbcBuscaInfoMinucias;
	private DefaultJdbcCall jdbcActualizaMinucias;
	private DefaultJdbcCall jdbcActualizaMinuciasEvi;

    private List<CatalogoMinuciasDTO> listaDetU;

    public void init() {

		jdbcEliminaMinucias = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCATMINUC")
				.withProcedureName("SP_DEL_MIN");

		jdbcBuscaInfoMinucias = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCATMINUC")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_CATMIN", new CatalogoMinuciasRowMapper());

		jdbcBuscaMinuciaEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCATMINUC")
				.withProcedureName("SP_SEL_EVID")
				.returningResultSet("RCL_CATMIN", new CatalogoMinuciasEviRowMapper());


		jdbcActualizaMinucias = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCATMINUC")
				.withProcedureName("SP_ACT_MIN");

		jdbcActualizaMinuciasEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCATMINUC")
				.withProcedureName("SP_ACT_EVIMIN");


	}

	
	@Override
	public boolean elimina(int idMinucia) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDMINUC",idMinucia);
		
		out = jdbcEliminaMinucias.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMCATMINUC.SP_DEL_MIN}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<CatalogoMinuciasDTO> obtieneDatosEvi(String idMinucia) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDMINUC",idMinucia);
		
		out = jdbcBuscaMinuciaEvi.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMCATMINUC.SP_SEL_EVID}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		listaDetU = (List<CatalogoMinuciasDTO>) out.get("RCL_CATMIN");
				
		if(respuesta !=  1){
			logger.info("Algo paso al consular la Fila ");
		}
		
		return listaDetU;
	}
	
	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoMinuciasDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<CatalogoMinuciasDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaInfoMinucias.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMCATMINUC.SP_SEL_DETALLE}");
        listaDetU = (List<CatalogoMinuciasDTO>) out.get("RCL_CATMIN");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las Minucias");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(CatalogoMinuciasDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDMINUC", bean.getIdMinucia())
                .addValue("PA_IDUSUARIO", bean.getIdUsu())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_LUGARECHO", bean.getLugarEchos())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_DIRECCION", bean.getDireccion())
                .addValue("PA_VOBOGERENTE", bean.getVobo())
                .addValue("PA_DESTRUYE", bean.getDestruye())
                .addValue("PA_TESTIGO1", bean.getTestigo1())
                .addValue("PA_TESTIGO2", bean.getTestigo2());

        out = jdbcActualizaMinucias.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMCATMINUC.SP_ACT_MIN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaEvi(CatalogoMinuciasDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDEVID", bean.getIdevi())
                .addValue("PA_IDMINUC", bean.getIdMinucia())
                .addValue("PA_ARCHIVONOM", bean.getNombrearc())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERVAC", bean.getObservac());

        out = jdbcActualizaMinuciasEvi.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMCATMINUC.SP_ACT_EVIMIN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
