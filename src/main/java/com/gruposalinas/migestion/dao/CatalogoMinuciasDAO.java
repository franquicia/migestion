package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;
import java.util.List;

public interface CatalogoMinuciasDAO {
	

	public boolean elimina(int idMinucia)throws Exception;


	public List<CatalogoMinuciasDTO> obtieneInfo() throws Exception;

	public boolean actualiza(CatalogoMinuciasDTO bean)throws Exception;

	public boolean actualizaEvi(CatalogoMinuciasDTO bean)throws Exception;
	
	public List<CatalogoMinuciasDTO> obtieneDatosEvi(String idMinucia) throws Exception; 
	
	
}
