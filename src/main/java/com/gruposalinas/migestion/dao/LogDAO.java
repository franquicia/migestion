package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.LogDTO;
import java.util.List;

public interface LogDAO {

    public List<LogDTO> obtieneErrores(String fecha) throws Exception;

    public boolean inserta(int codigo, String mensaje, String origen);
}
