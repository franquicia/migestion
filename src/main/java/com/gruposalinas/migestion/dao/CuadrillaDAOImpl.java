package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.mappers.CuadrillaPasswRowMapper;
import com.gruposalinas.migestion.mappers.CuadrillaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CuadrillaDAOImpl extends DefaultDAO implements CuadrillaDAO {

    private static Logger logger = LogManager.getLogger(CuadrillaDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaCuadrilla;
    private DefaultJdbcCall jdbcEliminaCuadrilla;
    private DefaultJdbcCall jdbcBuscaCuadrilla;
    private DefaultJdbcCall jdbcBuscaInfoCuadrilla;
    private DefaultJdbcCall jdbcActualizaCuadrilla;
    private DefaultJdbcCall jdbcCargaPass;
    private DefaultJdbcCall jdbcobtienePass;
    private DefaultJdbcCall jdbcEliminaCuadrillaProveedor;

    private List<CuadrillaDTO> listaDetU;

    public void init() {

        jdbcInsertaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_INS_CUAD");

        jdbcEliminaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_DEL_CUAD");

        jdbcBuscaInfoCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_CUADRI", new CuadrillaRowMapper());

        jdbcBuscaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_CUADRI", new CuadrillaRowMapper());

        jdbcActualizaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_ACT_CUAD");

        jdbcCargaPass = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_CARGAPAS");

        jdbcobtienePass = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_SEL_PASS")
                .returningResultSet("RCL_CUADRI", new CuadrillaPasswRowMapper());

        jdbcEliminaCuadrillaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADMCUADRILLA")
                .withProcedureName("SP_DEL_CUADPROV");

    }

    @Override
    public int inserta(CuadrillaDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDERCUAD", bean.getLiderCuad())
                .addValue("PA_CORREO", bean.getCorreo())
                .addValue("PA_SEGUNDO", bean.getSegundo())
                .addValue("PA_PASS", bean.getPassw());

        out = jdbcInsertaCuadrilla.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_INS_CUAD}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_IDCUADRI");
        idNota = idbloc.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    @Override
    public boolean elimina(String idCuadrilla) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", idCuadrilla);

        out = jdbcEliminaCuadrilla.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_DEL_CUAD}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<CuadrillaDTO> obtieneDatos(int idCuadrilla) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", idCuadrilla);

        out = jdbcBuscaCuadrilla.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<CuadrillaDTO>) out.get("RCL_CUADRI");

        if (respuesta != 1) {
            logger.info("Algo paso al consular la cuadrilla ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<CuadrillaDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<CuadrillaDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoCuadrilla.execute();

        logger.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_SEL_DETALLE}");
        listaDetU = (List<CuadrillaDTO>) out.get("RCL_CUADRI");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las Cuadrilla");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(CuadrillaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", bean.getIdCuadrilla())
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDERCUAD", bean.getLiderCuad())
                .addValue("PA_CORREO", bean.getCorreo())
                .addValue("PA_SEGUNDO", bean.getSegundo())
                .addValue("PA_PASS", bean.getPassw());

        out = jdbcActualizaCuadrilla.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_ACT_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargapass() throws Exception {
        int respuesta = 0;
        Map<String, Object> out = null;

        out = jdbcCargaPass.execute();

        logger.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_CARGAPAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CuadrillaDTO> obtienepass(int idCuadrilla, int idProveedor, int zona) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRILLA", idCuadrilla)
                .addValue("PA_PROVEED", idProveedor)
                .addValue("PA_ZONA", zona);

        out = jdbcobtienePass.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_SEL_PASS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<CuadrillaDTO>) out.get("RCL_CUADRI");

        if (respuesta != 1) {
            logger.info("Algo paso al consular la cuadrilla ");
        }

        return listaDetU;
    }

    @Override
    public boolean eliminaCuadrillaProveedor(int cuadrilla, int proveedor, int zona) throws Exception {

        Map< String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRILLA", cuadrilla)
                .addValue("PA_PROVEEDOR", proveedor)
                .addValue("PA_ZONA", zona);

        out = jdbcEliminaCuadrillaProveedor.execute(in);

        logger.info("Funcion Ejecutada: {GESTION.PADMCUADRILLA.SP_DEL_CUADPROV}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        boolean retorno = false;

        if (respuesta == 1) {
            retorno = true;
        } else {
            retorno = false;
            logger.info("Algo paso al eliminar la cuadrilla del proveedor ");
        }

        return retorno;
    }

}
