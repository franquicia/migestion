package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import java.util.List;

public interface ActivoFijoDAO {


	public int actualizaActivoFijo(ActivoFijoDTO bean)throws Exception;

	public int eliminaActivoFijo(int idActivoFijo)throws Exception;

	
	public List<ActivoFijoDTO>consultaActivoFijoAll()throws Exception;
}
