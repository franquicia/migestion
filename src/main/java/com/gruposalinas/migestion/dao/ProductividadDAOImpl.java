package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ClasificacionCecoDTO;
import com.gruposalinas.migestion.domain.ProductividadDTO;
import com.gruposalinas.migestion.mappers.ClasificacionCecoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ProductividadDAOImpl extends DefaultDAO implements ProductividadDAO {

    private Logger logger = LogManager.getLogger(ProductividadDAOImpl.class);

    private DefaultJdbcCall jdbcCargaProduvtividad;
    private DefaultJdbcCall jdbcTotalRegistros;
    private DefaultJdbcCall jdbcEliminaRegistros;
    private DefaultJdbcCall jdbcGetClasificacion;
    private DefaultJdbcCall jdbcGetClasificaciones;

    public void init() {

        jdbcCargaProduvtividad = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCTDAD")
                .withProcedureName("SP_INSERTA");

        jdbcTotalRegistros = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCTDAD")
                .withProcedureName("SP_CONTEOFECHA");

        jdbcEliminaRegistros = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCTDAD")
                .withProcedureName("SP_ELIMINAFECHA");

        jdbcGetClasificacion = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCTDAD")
                .withProcedureName("SP_GETCLASIFICA");

        jdbcGetClasificaciones = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCTDAD")
                .withProcedureName("SP_GETCLSFCECOS").returningResultSet("RCL_CONSULTA", new ClasificacionCecoRowMapper());
    }

    @Override
    public boolean cargaProductividad(ProductividadDTO datos) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NIVEL", datos.getNivel())
                .addValue("PA_FECHA", datos.getFecha())
                .addValue("PA_TIPOGEO", datos.getTipo_geografia())
                .addValue("PA_POSICION", datos.getPosicion())
                .addValue("PA_CONTRIBUCION", datos.getContribucion())
                .addValue("PA_POSCONT", datos.getPos_contribucion())
                .addValue("PA_PRESTAMOS", datos.getPrestamos())
                .addValue("PA_POSPRESTA", datos.getPos_prestamos())
                .addValue("PA_CAPTACION", datos.getCaptacion())
                .addValue("PA_POSCAPTA", datos.getPos_captacion())
                .addValue("PA_SIPA", datos.getSipa())
                .addValue("PA_POSSIPA", datos.getPos_sipa())
                .addValue("PA_SEGUROS", datos.getSeguros())
                .addValue("PA_POSSEGUROS", datos.getPos_seguros())
                .addValue("PA_GENTE", datos.getGente())
                .addValue("PA_POSGENTE", datos.getPos_gente())
                .addValue("PA_AFORE", datos.getAfore())
                .addValue("PA_POSAFORE", datos.getPos_afore())
                .addValue("PA_TOTAL", datos.getTotal())
                .addValue("PA_IDCC", datos.getId_cc())
                .addValue("PA_IDCC_PADRE", datos.getCc_padre())
                .addValue("PA_IDCECO", datos.getCc())
                .addValue("PA_IDEMPLEADO", datos.getEmpleado())
                .addValue("PA_DESDE", datos.getDesde())
                .addValue("PA_TREG", datos.getTreg())
                .addValue("PA_PERIODO", datos.getPeriodo());

        out = jdbcCargaProduvtividad.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCTDAD.SP_INSERTA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al inserta el registro ");
            return false;
        }

        return true;
    }

    @Override
    public int totalRegistros(String ultimoDomingoTrim) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        int total = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHA", ultimoDomingoTrim);

        out = jdbcTotalRegistros.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCTDAD.SP_CONTEOFECHA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        BigDecimal returnTotal = (BigDecimal) out.get("PA_TOTAL");
        total = returnTotal.intValue();

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al inserta el registro ");
        }

        return total;
    }

    @Override
    public boolean eliminaFecha(String ultimoDomingoTrim) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHA", ultimoDomingoTrim);

        out = jdbcEliminaRegistros.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCTDAD.SP_ELIMINAFECHA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al inserta el registro ");
        }

        return true;
    }

    @Override
    public int getClasificacion(String ceco) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        int clasificacion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcGetClasificacion.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCTDAD.SP_GETCLASIFICA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        BigDecimal returnClasificacion = (BigDecimal) out.get("PA_CLASIFICA");
        clasificacion = returnClasificacion.intValue();

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al obtener la clasificacion");
        }

        return clasificacion;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ClasificacionCecoDTO> gecCecosClasificacion(String ceco, int tipo) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        List<ClasificacionCecoDTO> clasificaciones = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_TIPO", tipo);

        out = jdbcGetClasificaciones.execute(in);

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        clasificaciones = (List<ClasificacionCecoDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al obtener la clasificacion");
        }

        return clasificaciones;
    }

}
