/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.IndicadorIPNClienteDTO;
import com.gruposalinas.migestion.domain.IndicadorIPNEquipoDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 */
interface IndicadoresIPNDAO {

    public List<IndicadorIPNClienteDTO> getIndicadoresIPNCliente(String idCeco, int top);

    public List<IndicadorIPNEquipoDTO> getIndicadoresIPNEquipo(String idCeco, int top);

    public int getDatoIndicadorIPNEquipo(List<IndicadorIPNEquipoDTO> datos);

    public int getDatoIndicadorIPNCliente(List<IndicadorIPNClienteDTO> datos);
}
