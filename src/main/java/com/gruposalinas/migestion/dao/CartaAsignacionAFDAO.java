package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CartaAsignacionAFDTO;
import java.util.List;

public interface CartaAsignacionAFDAO {

	public boolean elimina(String idCarta)throws Exception;

	public List<CartaAsignacionAFDTO> obtieneInfo() throws Exception; 
	
	public boolean actualiza(CartaAsignacionAFDTO bean)throws Exception;
	
	
}
