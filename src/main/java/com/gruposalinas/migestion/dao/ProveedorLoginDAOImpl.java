package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ProveedorLoginDTO;
import com.gruposalinas.migestion.mappers.ProvedorLoginRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ProveedorLoginDAOImpl extends DefaultDAO implements ProveedorLoginDAO {

    private static Logger logger = LogManager.getLogger(ProveedorLoginDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaProveLogin;
    private DefaultJdbcCall jdbcEliminaProveLogin;
    private DefaultJdbcCall jdbcBuscaProveedor;
    private DefaultJdbcCall jdbcBuscaInfoProveLogin;
    private DefaultJdbcCall jdbcActualizaProveLogin;

    private List<ProveedorLoginDTO> listaDetU;

    public void init() {

        jdbcInsertaProveLogin = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMUSERPASS")
                .withProcedureName("SP_INS_PAS");

        jdbcEliminaProveLogin = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMUSERPASS")
                .withProcedureName("SP_DEL_PAS");

        jdbcBuscaInfoProveLogin = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMUSERPASS")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_PASSW", new ProvedorLoginRowMapper());

        jdbcBuscaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMUSERPASS")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_PASSW", new ProvedorLoginRowMapper());

        jdbcActualizaProveLogin = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMUSERPASS")
                .withProcedureName("SP_ACT_PAS");

    }

    @Override
    public int inserta(ProveedorLoginDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_PASSW", bean.getPassw());

        out = jdbcInsertaProveLogin.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSERPASS.SP_INS_PAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_PASW");
        idNota = idbloc.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    @Override
    public boolean elimina(String idProveedor) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", idProveedor);

        out = jdbcEliminaProveLogin.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSERPASS.SP_DEL_PAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<ProveedorLoginDTO> obtieneDatos(int idProveedor) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", idProveedor);

        out = jdbcBuscaProveedor.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSERPASS.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<ProveedorLoginDTO>) out.get("RCL_PASSW");

        if (respuesta != 1) {
            logger.info("Algo paso al consular el Login del Proveedor ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<ProveedorLoginDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<ProveedorLoginDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoProveLogin.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMUSERPASS.SP_SEL_DETALLE}");
        listaDetU = (List<ProveedorLoginDTO>) out.get("RCL_PASSW");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las ProveLogin");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(ProveedorLoginDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_PASSW", bean.getPassw());

        out = jdbcActualizaProveLogin.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSERPASS.SP_ACT_PAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
