package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.FilasDTO;
import com.gruposalinas.migestion.mappers.FilasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FilasDAOImpl extends DefaultDAO implements FilasDAO {

    private static Logger logger = LogManager.getLogger(FilasDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaFilas;
    private DefaultJdbcCall jdbcEliminaFilas;
    private DefaultJdbcCall jdbcBuscaFila;
    private DefaultJdbcCall jdbcBuscaInfoFilas;
    private DefaultJdbcCall jdbcActualizaFilas;

    private List<FilasDTO> listaDetU;

    public void init() {

        jdbcInsertaFilas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMFILAS")
                .withProcedureName("SP_INS_FILAS");

        jdbcEliminaFilas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMFILAS")
                .withProcedureName("SP_DEL_FILAS");

        jdbcBuscaInfoFilas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMFILAS")
                .withProcedureName("SP_SEL_FILAS")
                .returningResultSet("RCL_FIL", new FilasRowMapper());

        jdbcBuscaFila = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMFILAS")
                .withProcedureName("SP_SEL_FILPAR")
                .returningResultSet("RCL_FIL", new FilasRowMapper());

        jdbcActualizaFilas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMFILAS")
                .withProcedureName("SP_ACT_FILAS");

    }

    @Override
    public int inserta(FilasDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NUMECO", bean.getNumEconomico())
                .addValue("PA_GRUPO", bean.getIdGrupo());

        out = jdbcInsertaFilas.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMFILAS.SP_INS_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_IDFILA");
        idNota = idbloc.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    @Override
    public boolean elimina(String idFila) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDFILA", idFila);

        out = jdbcEliminaFilas.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMFILAS.SP_DEL_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<FilasDTO> obtieneDatos(int idFila) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDFILA", idFila);

        out = jdbcBuscaFila.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMFILAS.SP_SEL_FILPAR}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<FilasDTO>) out.get("RCL_FIL");

        if (respuesta == 1) {
            logger.info("Algo paso al consular la Fila ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<FilasDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<FilasDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoFilas.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMFILAS.SP_SEL_FILAS}");
        listaDetU = (List<FilasDTO>) out.get("RCL_FIL");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error == 1) {
            logger.info("Algo paso al obtener las Filas");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(FilasDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDFILA", bean.getIdFila())
                .addValue("PA_NUMECO", bean.getNumEconomico())
                .addValue("PA_GRUPO", bean.getIdGrupo());

        out = jdbcActualizaFilas.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMFILAS.SP_ACT_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta == 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
