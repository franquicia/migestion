package com.gruposalinas.migestion.dao;

import java.util.List;

import com.gruposalinas.migestion.domain.MysteryDTO;


public interface MysteryDAO {
	
	public List<MysteryDTO> consultaCecos(String idCeco)throws Exception;
	
	public List<MysteryDTO> consultaNivel(String idCeco)throws Exception;
	

}
