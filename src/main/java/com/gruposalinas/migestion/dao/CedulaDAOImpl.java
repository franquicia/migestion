package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CedulaDTO;
import com.gruposalinas.migestion.mappers.CedulaPlantillaRowMapper;
import com.gruposalinas.migestion.mappers.CedulaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CedulaDAOImpl extends DefaultDAO implements CedulaDAO {

	private static Logger logger = LogManager.getLogger(CedulaDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaCedula;
	private DefaultJdbcCall jdbcBuscaInfoCedulas;
	private DefaultJdbcCall jdbcActualizaCedula;

    private List<CedulaDTO> listaDetU;

    public void init() {

		

		jdbcEliminaCedula = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCEDULA")
				.withProcedureName("SP_DEL_CED");

		jdbcBuscaInfoCedulas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCEDULA")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_CEDULA", new CedulaRowMapper());




		jdbcActualizaCedula = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMCEDULA")
				.withProcedureName("SP_ACT_CED");


	}

	
	@Override
	public boolean elimina(String idUsuario) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDUSUARIO",idUsuario);
		
		out = jdbcEliminaCedula.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMCEDULA.SP_DEL_CED}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}



	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<CedulaDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<CedulaDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaInfoCedulas.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMCEDULA.SP_SEL_DETALLE}");
        listaDetU = (List<CedulaDTO>) out.get("RCL_CEDULA");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las Cedula");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(CedulaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_IDSUC", bean.getIdSuc())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_RUTAFOTO", bean.getRutafoto())
                .addValue("PA_FIRMAACEPTA", bean.getRutaFirmaAcepta())
                .addValue("PA_FIRMAJEFE", bean.getRutaFirmaJefe())
                .addValue("PA_TIPOSANGRE", bean.getTipoSangre())
                .addValue("PA_SEGSOCIAL", bean.getIdSeguroSoc())
                .addValue("PA_ALERGIA", bean.getAlergia())
                .addValue("PA_DESCALERG", bean.getDescalergia())
                .addValue("PA_ENFERMEDAD", bean.getEnfermedad())
                .addValue("PA_DESCRENFER", bean.getDescEnfermedad())
                .addValue("PA_TRATAMIENTO", bean.getTratamiento())
                .addValue("PA_DESCRTRATAM", bean.getDescTratamiento())
                .addValue("PA_CONTACTO", bean.getContactoEmerg())
                .addValue("PA_TELEFONOCON", bean.getTelContacto())
                .addValue("PA_TELSOCIO", bean.getTelSocio());

        out = jdbcActualizaCedula.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMCEDULA.SP_ACT_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
