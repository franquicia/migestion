package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.CatalogoVistaDTO;
import com.gruposalinas.migestion.mappers.CatalogoVistaRowMapper;

public class CatalogoVistaDAOImpl extends DefaultDAO implements CatalogoVistaDAO {

    private static Logger logger = LogManager.getLogger(CatalogoVistaDAOImpl.class);

    DefaultJdbcCall jdbcConsultaCatalogoVista;
    DefaultJdbcCall jdbcConsultaCatalogoVistaParam;
    DefaultJdbcCall jdbcInsertaCatalogoVista;
    DefaultJdbcCall jdbcActualizaCatalogoVista;
    DefaultJdbcCall jdbcEliminaCatalogoVista;

    public void init() {

        jdbcConsultaCatalogoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAACATVISITA")
                .withProcedureName("SP_SEL_CATVISITA")
                .returningResultSet("RCL_CATVIS", new CatalogoVistaRowMapper());

        jdbcConsultaCatalogoVistaParam = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAACATVISITA")
                .withProcedureName("SP_SEL_CUALQPARAM")
                .returningResultSet("RCL_CATVIS", new CatalogoVistaRowMapper());

        jdbcInsertaCatalogoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAACATVISITA")
                .withProcedureName("SP_INS_CATVISITA");

        jdbcActualizaCatalogoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAACATVISITA")
                .withProcedureName("SP_ACT_CATVISITA");

        jdbcEliminaCatalogoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAACATVISITA")
                .withProcedureName("SP_DEL_CATVISITA");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CatalogoVistaDTO> buscaCatalogoVista(int idCatVista) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CatalogoVistaDTO> listaCeco = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_CATVIS", idCatVista);

        out = jdbcConsultaCatalogoVista.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAACATVISITA.SP_SEL_CATVISITA}");

        listaCeco = (List<CatalogoVistaDTO>) out.get("RCL_CATVIS");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al consultar los CatalogoVista");
        } else {
            return listaCeco;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CatalogoVistaDTO> buscaCatalogoVistaParam(String idCatVista, String descripcion, String moduloPadre) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CatalogoVistaDTO> listaCeco = null;
        logger.info("Entro a busca Catalogo Vista DAO ");

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_CATVIS", idCatVista)
                .addValue("P_DESCRVIS", descripcion)
                .addValue("P_IDMODPADRE", moduloPadre);
        logger.info("paso los parametros ");

        out = jdbcConsultaCatalogoVistaParam.execute(in);

        logger.info("paso la ejecucion ");

        logger.info("Funcion ejecutada:{FRANQUICIA.PAACATVISITA.SP_SEL_CATVISITA}");

        listaCeco = (List<CatalogoVistaDTO>) out.get("RCL_CATVIS");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al consultar los CatalogoVista");
        } else {
            return listaCeco;
        }

        return null;
    }

    @Override
    public boolean insertaCatalogoVista(CatalogoVistaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_DESCRVIS", bean.getDescripcion())
                .addValue("P_IDMODPADRE", bean.getIdModPadre());

        out = jdbcInsertaCatalogoVista.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAACATVISITA.SP_INS_CATVISITA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al insertar en CatalogoVista");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaCatalogoVista(CatalogoVistaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_CATVIS", bean.getIdCatVista())
                .addValue("P_DESCRVIS", bean.getDescripcion())
                .addValue("P_IDMODPADRE", bean.getIdModPadre());

        out = jdbcActualizaCatalogoVista.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAACATVISITA.SP_ACT_CATVISITA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al modificar en CatalogoVista");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaCatalogoVista(int idCatVista) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_CATVIS", idCatVista);

        out = jdbcEliminaCatalogoVista.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAACATVISITA.SP_DEL_CATVISITA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al eliminar en CatalogoVista");
        } else {
            return true;
        }

        return false;
    }

}
