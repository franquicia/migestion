package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.FotoPlantillaDTO;
import java.util.List;

public interface FotoPlantillaDAO {
	

	public boolean elimina(String ceco)throws Exception;

	
	public List<FotoPlantillaDTO> obtieneInfo() throws Exception; 
	
	public boolean actualiza(FotoPlantillaDTO bean)throws Exception;
	
	
}
