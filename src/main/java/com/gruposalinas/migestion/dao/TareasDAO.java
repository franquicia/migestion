package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TareaDTO;
import java.util.List;

public interface TareasDAO {

    public boolean insertaTarea(TareaDTO bean) throws Exception;

    public boolean actualizaTarea(TareaDTO bean) throws Exception;

    public boolean eliminaTarea(int idTarea) throws Exception;

    public List<TareaDTO> obtieneTareas() throws Exception;

    public List<TareaDTO> obtieneActivas() throws Exception;

}
