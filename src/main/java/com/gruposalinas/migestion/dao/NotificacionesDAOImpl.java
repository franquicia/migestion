package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import com.gruposalinas.migestion.mappers.UsuarioInfoTokenRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class NotificacionesDAOImpl extends DefaultDAO implements NotificacionesDAO {

    Logger logger = LogManager.getLogger(NotificacionesDAOImpl.class);

    private DefaultJdbcCall jdbcConsultaTokens;

    public void init() {

        jdbcConsultaTokens = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PANOTIFICAGES")
                .withProcedureName("SP_NOTIUSUARIOS").returningResultSet("RCL_CONSULTA", new UsuarioInfoTokenRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UsuarioInfoTokenDTO> consultaTokens(String ceco, String puesto) throws Exception {
        List<UsuarioInfoTokenDTO> tokens = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_PUESTO", puesto);

        out = jdbcConsultaTokens.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnejec.intValue();

        tokens = (List<UsuarioInfoTokenDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al obtener los tokens");
        }

        return tokens;
    }

}
