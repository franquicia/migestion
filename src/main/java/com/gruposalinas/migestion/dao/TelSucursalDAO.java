package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TelSucursalDTO;
import java.util.List;

public interface TelSucursalDAO {

    public List<TelSucursalDTO> obtieneTodosTelefonos() throws Exception;

    public List<TelSucursalDTO> obtieneTelefono(int idCeco) throws Exception;

    public boolean insertaTelefono(TelSucursalDTO bean) throws Exception;

    public boolean actualizaTelefono(TelSucursalDTO bean) throws Exception;

    public boolean eliminaTelefono(int idTelefono) throws Exception;

    public boolean depuraTelefono() throws Exception;

}
