package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TablasPasoDTO;
import com.gruposalinas.migestion.domain.TareaActDTO;
import java.util.List;

public interface TablasPasoDAO {

    public List<TablasPasoDTO> obtieneCecos(String idCeco, String idCecoSup, String descripcion) throws Exception;

    public List<TablasPasoDTO> obtieneSucursales(String numSucursal) throws Exception;

    public List<TablasPasoDTO> obtieneUsuarios(String idUsuario, String idPuesto, String idCeco, String idCecoSup) throws Exception;

    public List<TareaActDTO> obtieneTareas(String idTarea, String idUsuario, String estatus, String statusVac) throws Exception;

}
