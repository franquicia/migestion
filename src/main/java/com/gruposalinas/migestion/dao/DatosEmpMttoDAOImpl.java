package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import com.gruposalinas.migestion.mappers.DatosEmpMttoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DatosEmpMttoDAOImpl extends DefaultDAO implements DatosEmpMttoDAO {

    private static Logger logger = LogManager.getLogger(DatosEmpMttoDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaFila;
    private DefaultJdbcCall jdbcBuscaInfoFilas;

    private List<DatosEmpMttoDTO> listaDetU;

    public void init() {

        jdbcBuscaInfoFilas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILREM")
                .withProcedureName("SP_INFOEMPEXT")
                .returningResultSet("RCL_EMPLEADOEXT", new DatosEmpMttoRowMapper());

        jdbcBuscaFila = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILREM")
                .withProcedureName("SP_PARAMETRO")
                .returningResultSet("RCL_EMPLEADOEXT", new DatosEmpMttoRowMapper());

    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<DatosEmpMttoDTO> obtieneDatos(int idUsuario) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSU", idUsuario);

        out = jdbcBuscaFila.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILREM.SP_PARAMETRO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<DatosEmpMttoDTO>) out.get("RCL_EMPLEADOEXT");

        if (respuesta != 1) {
            logger.info("Algo paso al consular la informacion del Empleado externo");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<DatosEmpMttoDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<DatosEmpMttoDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoFilas.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILREM.SP_INFOEMPEXT}");
        listaDetU = (List<DatosEmpMttoDTO>) out.get("RCL_EMPLEADOEXT");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener la informacion de los empleados externos");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

}
