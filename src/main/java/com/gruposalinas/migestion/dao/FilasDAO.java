package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.FilasDTO;
import java.util.List;

public interface FilasDAO {

    public int inserta(FilasDTO bean) throws Exception;

    public boolean elimina(String idFila) throws Exception;

    public List<FilasDTO> obtieneDatos(int idFila) throws Exception;

    public List<FilasDTO> obtieneInfo() throws Exception;

    public boolean actualiza(FilasDTO bean) throws Exception;

}
