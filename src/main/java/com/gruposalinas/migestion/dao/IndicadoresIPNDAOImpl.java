/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.IndicadorIPNClienteDTO;
import com.gruposalinas.migestion.domain.IndicadorIPNEquipoDTO;
import com.gruposalinas.migestion.mappers.IndicadoresIPNRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class IndicadoresIPNDAOImpl extends DefaultDAO implements IndicadoresIPNDAO {

    private static final Logger logger = LogManager.getLogger(IndicadoresIPNDAOImpl.class);
    private DefaultJdbcCall jdbcConsultaClienteIPN;
    private DefaultJdbcCall jdbcConsultaEquipoIPN;
    private DefaultJdbcCall jdbcInsertaEquipoIPN;
    private DefaultJdbcCall jdbcInsertaClienteIPN;
    private MapSqlParameterSource params;
    private List<IndicadorIPNClienteDTO> listaIPNCliente;
    private List<IndicadorIPNEquipoDTO> listaIPNEquipo;

    public void init() {
        jdbcConsultaClienteIPN = new DefaultJdbcCall(getGtnJdbcTemplate());
        jdbcConsultaClienteIPN.withSchemaName("GESTION");
        jdbcConsultaClienteIPN.withCatalogName("PAADM_TAIND_IPN");
        jdbcConsultaClienteIPN.withProcedureName("SPGETIPNCLIENTE");
        jdbcConsultaClienteIPN.returningResultSet("PA_CONSULTA", new IndicadoresIPNRowMapper().new IndicadorClienteIPNRowMapper());

        jdbcConsultaEquipoIPN = new DefaultJdbcCall(getGtnJdbcTemplate());
        jdbcConsultaEquipoIPN.withSchemaName("GESTION");
        jdbcConsultaEquipoIPN.withCatalogName("PAADM_TAIND_IPN");
        jdbcConsultaEquipoIPN.withProcedureName("SPGETIPNEQUIPO");
        jdbcConsultaEquipoIPN.returningResultSet("PA_CONSULTA", new IndicadoresIPNRowMapper().new IndicadorEquipoIPNRowMapper());

        jdbcInsertaEquipoIPN = new DefaultJdbcCall(getGtnJdbcTemplate());
        jdbcInsertaEquipoIPN.withSchemaName("GESTION");
        jdbcInsertaEquipoIPN.withCatalogName("PAADM_TAIND_IPN");
        jdbcInsertaEquipoIPN.withProcedureName("SPINDATOEQUIPO");

        jdbcInsertaClienteIPN = new DefaultJdbcCall(getGtnJdbcTemplate());
        jdbcInsertaClienteIPN.withSchemaName("GESTION");
        jdbcInsertaClienteIPN.withCatalogName("PAADM_TAIND_IPN");
        jdbcInsertaClienteIPN.withProcedureName("SPINDATOCLIENTE");

    }

    @Override
    public List<IndicadorIPNClienteDTO> getIndicadoresIPNCliente(String idCeco, int top) {
        params = new MapSqlParameterSource();
        params.addValue("PA_CECO", idCeco);
        params.addValue("PA_RANCKING", top);
        Map<String, Object> out = jdbcConsultaClienteIPN.execute(params);
        logger.info("Funcion ejecutada: {GESTION.PAADM_TAIND_IPN.SPGETIPNCLIENTE}");
        listaIPNCliente = (List<IndicadorIPNClienteDTO>) out.get("PA_CONSULTA");
        return listaIPNCliente;
    }

    @Override
    public List<IndicadorIPNEquipoDTO> getIndicadoresIPNEquipo(String idCeco, int top) {
        params = new MapSqlParameterSource();
        params.addValue("PA_CECO", idCeco);
        params.addValue("PA_RANCKING", top);
        Map<String, Object> out = jdbcConsultaEquipoIPN.execute(params);
        logger.info("Funcion ejecutada: {GESTION.PAADM_TAIND_IPN.SPGETIPNEQUIPO}");
        listaIPNEquipo = (List<IndicadorIPNEquipoDTO>) out.get("PA_CONSULTA");
        return listaIPNEquipo;
    }

    @Override
    public int getDatoIndicadorIPNEquipo(List<IndicadorIPNEquipoDTO> datos) {
        int respuesta = 0;
        int contador = 0;
        for (IndicadorIPNEquipoDTO ind : datos) {

            SqlParameterSource ins = new MapSqlParameterSource()
                    .addValue("PA_IDTERRITORIO", ind.getFiTerritorio())
                    .addValue("PA_IDZONA", ind.getFiZona())
                    .addValue("PA_IDREGION", ind.getFiRegion())
                    .addValue("PA_SUCURSAL", ind.getFiSucursal())
                    .addValue("PA_NUMECONOMICO", ind.getFiNumEconomico())
                    .addValue("PA_SUCUDESC", ind.getFcSucursal())
                    .addValue("PA_DETRACTIVO", ind.getFiDetractivo())
                    .addValue("PA_PASIVO", ind.getFiPasivo())
                    .addValue("PA_PROMOTOR", ind.getFiPromotor())
                    .addValue("PA_TOTAL", ind.getFiTotal())
                    .addValue("PA_IPN", ind.getFiIPN())
                    .addValue("PA_Q", ind.getFiQ())
                    .addValue("PA_ANIO", ind.getFiAnio());
            Map<String, Object> out = jdbcInsertaEquipoIPN.execute(ins);
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            respuesta = resultado.intValue();
            logger.info("Funcion ejecutada: {GESTION.PAADM_TAIND_IPN.SPGETIPNEQUIPO}");
            if (respuesta != 1) {
                System.out.println("ERROR AL INSERTAR UN DATO IPN EQUIPO: " + ind.toString());
            } else {
                contador++;
                System.out.println("Se inserto");
            }

        }

        System.out.println("Se insertaron " + contador);
        return contador;
    }

    @Override
    public int getDatoIndicadorIPNCliente(List<IndicadorIPNClienteDTO> datos) {
        int respuesta = 0;
        int contador = 0;
        for (IndicadorIPNClienteDTO ind : datos) {

            SqlParameterSource ins = new MapSqlParameterSource()
                    .addValue("PA_TERRITORIO", ind.getFiTerritorio())
                    .addValue("PA_ZONA", ind.getFiZona())
                    .addValue("PA_REGION", ind.getFiRegion())
                    .addValue("PA_IDSUCURSAL", ind.getFiSucursal())
                    .addValue("PA_NUMECONOMICO", ind.getFiNumEconomico())
                    .addValue("PA_SUCURSAL", ind.getFcSucursal())
                    .addValue("PA_NUMEVAL", ind.getFiNumEval())
                    .addValue("PA_PROMOTOR", ind.getFiPromotor())
                    .addValue("PA_PASIVO", ind.getFiPasivo())
                    .addValue("PA_DETRACTIVO", ind.getFiDetractivo())
                    .addValue("PA_IPN", ind.getFiIPN())
                    .addValue("PA_Q", ind.getFiQ())
                    .addValue("PA_ANIO", ind.getFiAnio());
            System.out.println("ANTES" + ind.toString());
            Map<String, Object> out = jdbcInsertaClienteIPN.execute(ins);
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            respuesta = resultado.intValue();
            logger.info("Funcion ejecutada: {GESTION.PAADM_TAIND_IPN.SPINDATOCLIENTE}");
            if (respuesta != 1) {
                System.out.println("ERROR AL INSERTAR UN DATO IPN CLIENTE: " + ind.toString());
            } else {
                contador++;
                System.out.println("Se inserto");
            }

        }

        System.out.println("Se insertaron cliente " + contador);
        return contador;

    }
}
