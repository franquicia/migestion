package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.GuiaDHLDTO;
import com.gruposalinas.migestion.mappers.GuiaDhlCecoRowMapper;
import com.gruposalinas.migestion.mappers.GuiaDhlRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class GuiaDHLDAOImpl extends DefaultDAO implements GuiaDHLDAO {

	private static Logger logger = LogManager.getLogger(GuiaDHLDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaGuia;
	private DefaultJdbcCall jdbcEliminaGuiaEvi;
	private DefaultJdbcCall jdbcBuscaDetalle;
	private DefaultJdbcCall jdbcActualizaGuia;
	private DefaultJdbcCall jdbcActualizaEvi;

    private List<GuiaDHLDTO> listaDetU;

    public void init() {

		
		jdbcEliminaGuia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMGUIADHL")
				.withProcedureName("SP_DEL_GUIA");

		jdbcEliminaGuiaEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMGUIADHL")
				.withProcedureName("SP_DEL_GUIAEVI");

		jdbcBuscaDetalle = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMGUIADHL")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_DHL", new GuiaDhlRowMapper());



		jdbcActualizaGuia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMGUIADHL")
				.withProcedureName("SP_ACT_GUIA");

		jdbcActualizaEvi = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMGUIADHL")
				.withProcedureName("SP_ACT_GUIAEVID");


	}
	//guia
	
	@Override
	public boolean elimina(String idGuia) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_GUIA",idGuia);
		
		out = jdbcEliminaGuia.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMGUIADHL.SP_DEL_GUIA}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}
	
	//archivos de guia
	@Override
	public boolean eliminaEvi(String idEvid) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_GUIAEVID",idEvid);
		
		out = jdbcEliminaGuiaEvi.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMGUIADHL.SP_DEL_GUIAEVI}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}

	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<GuiaDHLDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
	
		int error = 0;
		
		out = jdbcBuscaDetalle.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMGUIADHL.SP_SEL_DETALLE}");
        listaDetU = (List<GuiaDHLDTO>) out.get("RCL_DHL");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las guias");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(GuiaDHLDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_GUIA", bean.getIdGuia())
                .addValue("PA_USUARIO", bean.getIdUsuario())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_CECO", bean.getCeco());

        out = jdbcActualizaGuia.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMGUIADHL.SP_ACT_GUIA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaEvid(GuiaDHLDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_GUIAEVID", bean.getIdEvid())
                .addValue("PA_GUIA", bean.getIdGuia())
                .addValue("PA_NOMBRE", bean.getNombreArch())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_OBSERVAC", bean.getDescr());

        out = jdbcActualizaEvi.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMGUIADHL.SP_ACT_GUIA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
