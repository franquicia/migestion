package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CuadrillaDTO;
import java.util.List;

public interface CuadrillaDAO {

    public int inserta(CuadrillaDTO bean) throws Exception;

    public boolean elimina(String idCuadrilla) throws Exception;

    public List<CuadrillaDTO> obtieneDatos(int idCuadrilla) throws Exception;

    public List<CuadrillaDTO> obtieneInfo() throws Exception;

    public boolean actualiza(CuadrillaDTO bean) throws Exception;

    public boolean cargapass() throws Exception;

    public List<CuadrillaDTO> obtienepass(int idCuadrilla, int idProveedor, int zona) throws Exception;

    public boolean eliminaCuadrillaProveedor(int cuadrilla, int proveedor, int zona) throws Exception;
}
