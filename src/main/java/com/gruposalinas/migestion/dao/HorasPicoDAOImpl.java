/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gruposalinas.migestion.domain.HorasPicoDTO;
import com.gruposalinas.migestion.mappers.HorasPicoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class HorasPicoDAOImpl extends DefaultDAO implements HorasPicoDAO {

    private static final Logger logger = LogManager.getLogger(HorasPicoDAOImpl.class);
    private DefaultJdbcCall jdbcConsulta;
    private DefaultJdbcCall jdbcConsultaPaso;
    private DefaultJdbcCall jdbcInsert;
    private MapSqlParameterSource params;

    public void init() {
        jdbcConsulta = new DefaultJdbcCall(getGtnJdbcTemplate());
        jdbcConsulta.withSchemaName("GESTION");
        jdbcConsulta.withCatalogName("PAADM_TAHRSPICO");
        jdbcConsulta.withProcedureName("SPGETHRSPICO");
        jdbcConsulta.returningResultSet("PA_CONSULTA", new HorasPicoRowMapper("HorasPico"));

        jdbcConsultaPaso = new DefaultJdbcCall(getGtnJdbcTemplate());
        jdbcConsultaPaso.withSchemaName("GESTION");
        jdbcConsultaPaso.withCatalogName("PAADM_TPHRSPICO");
        jdbcConsultaPaso.withProcedureName("SPGETHRSPICO");
        jdbcConsultaPaso.returningResultSet("PA_CONSULTA", new HorasPicoRowMapper("HorasPicoPaso"));

        jdbcInsert = new DefaultJdbcCall(getGtnJdbcTemplate());
        jdbcInsert.withSchemaName("GESTION");
        jdbcInsert.withCatalogName("PAADM_TAHRSPICO");
        jdbcInsert.withProcedureName("SPDISTHRSPICO");
    }

    @Override
    public List<HorasPicoDTO> getHorasPico(String fiSucursal, String fiAnio, String fiSemana) throws Exception {
        params = new MapSqlParameterSource();
        params.addValue("PA_FISUCURSAL", fiSucursal);
        params.addValue("PA_FIANIO", fiAnio);
        params.addValue("PA_FISEMANA", fiSemana);
        Map<String, Object> out = jdbcConsulta.execute(params);
        logger.info("Funcion ejecutada: {GESTION.PAADM_TAHRSPICO.SPGETHRSPICO}");
        List<HorasPicoDTO> lista = (List<HorasPicoDTO>) out.get("PA_CONSULTA");
        return lista;
    }

    @Override
    public List<HorasPicoDTO> getHorasPicoPaso(String fiSucursal, String fiSemana) throws Exception {
        params = new MapSqlParameterSource();
        params.addValue("PA_FISUCURSAL", fiSucursal);
        params.addValue("PA_FISEMANA", fiSemana);
        Map<String, Object> out = jdbcConsultaPaso.execute(params);
        logger.info("Funcion ejecutada: {GESTION.PAADM_TPHRSPICO.SPGETHRSPICO}");
        List<HorasPicoDTO> lista = (List<HorasPicoDTO>) out.get("PA_CONSULTA");
        return lista;
    }

    @Override
    public boolean setDistribucionHorasPico() throws Exception {
        Map<String, Object> out = jdbcInsert.execute();
        int success = ((BigDecimal) out.get("PA_RESEJECUCION")).intValue();
        logger.info("Funcion procesada: {GESTION.PAADM_TAHRSPICO.SPDISTHRSPICO}");
        if (success == 1) {
            int rowsAffected = ((BigDecimal) out.get("PA_CONTEOREGIST")).intValue();
            logger.info("Registros insertados " + rowsAffected);
            return true;
        } else {
            logger.error("Error al ejecutar: {GESTION.PAADM_TAHRSPICO.SPDISTHRSPICO}");
            return false;
        }
    }
}
