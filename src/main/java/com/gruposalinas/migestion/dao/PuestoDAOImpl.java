package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PuestoDTO;
import com.gruposalinas.migestion.mappers.PuestoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PuestoDAOImpl extends DefaultDAO implements PuestoDAO {

    private static Logger logger = LogManager.getLogger(PuestoDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaPuesto;
    private DefaultJdbcCall jdbcEliminaPuesto;
    private DefaultJdbcCall jdbcBuscaFila;
    private DefaultJdbcCall jdbcBuscaInfoPuesto;
    private DefaultJdbcCall jdbcActualizaPuesto;

    private List<PuestoDTO> listaDetU;

    public void init() {

        jdbcInsertaPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPUESTO")
                .withProcedureName("SP_INS_PUESTO");

        jdbcEliminaPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPUESTO")
                .withProcedureName("SP_DEL_PUESTO");

        jdbcBuscaInfoPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPUESTO")
                .withProcedureName("SP_SEL_PUESTOS")
                .returningResultSet("RCL_PUESTO", new PuestoRowMapper());

        jdbcBuscaFila = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPUESTO")
                .withProcedureName("SP_SEL_PUESTO")
                .returningResultSet("RCL_PUESTO", new PuestoRowMapper());

        jdbcActualizaPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPUESTO")
                .withProcedureName("SP_ACT_PUESTO");

    }

    @Override
    public int inserta(PuestoDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPUESTO", bean.getIdPuesto())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcInsertaPuesto.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPUESTO.SP_INS_PUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_FIIDPUESTO");
        idNota = idbloc.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    @Override
    public boolean elimina(String idPuesto) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PUESTO", idPuesto);

        out = jdbcEliminaPuesto.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPUESTO.SP_DEL_PUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<PuestoDTO> obtieneDatos(int idPuesto, String descripcion) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PUESTO", idPuesto)
                .addValue("PA_DESCRIPCION", descripcion);

        out = jdbcBuscaFila.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPUESTO.SP_SEL_PUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<PuestoDTO>) out.get("RCL_PUESTO");

        if (respuesta == 1) {
            logger.info("Algo paso al consular el puesto ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<PuestoDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<PuestoDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoPuesto.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPUESTO.SP_SEL_PUESTOS}");
        listaDetU = (List<PuestoDTO>) out.get("RCL_PUESTO");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las Puesto");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(PuestoDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PUESTO", bean.getIdPuesto())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcActualizaPuesto.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPUESTO.SP_ACT_PUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
