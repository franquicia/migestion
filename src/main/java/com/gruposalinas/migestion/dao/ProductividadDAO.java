package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ClasificacionCecoDTO;
import com.gruposalinas.migestion.domain.ProductividadDTO;
import java.util.List;

public interface ProductividadDAO {

    public boolean cargaProductividad(ProductividadDTO datos) throws Exception;

    public int totalRegistros(String ultimoDomingoTrim) throws Exception;

    public boolean eliminaFecha(String ultimoDomingoTrim) throws Exception;

    public int getClasificacion(String ceco) throws Exception;

    public List<ClasificacionCecoDTO> gecCecosClasificacion(String ceco, int tipo) throws Exception;

}
