package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TipificacionDTO;
import java.util.List;

public interface OperacionDAO {

    public List<TipificacionDTO> consulta() throws Exception;

    public List<TipificacionDTO> consultaPadres() throws Exception;

    public int inserta(TipificacionDTO bean) throws Exception;

    public boolean actualiza(TipificacionDTO bean) throws Exception;

    public boolean elimina(int idProducto) throws Exception;

    public List<TipificacionDTO> getHijos(int idPadre) throws Exception;

}
