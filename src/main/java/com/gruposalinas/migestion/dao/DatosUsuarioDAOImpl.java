package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.DatosUsuarioDTO;
import com.gruposalinas.migestion.mappers.DatosUsuarioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DatosUsuarioDAOImpl extends DefaultDAO implements DatosUsuarioDAO {

    private static final Logger logger = LogManager.getLogger(DatosUsuarioDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaUsuario;

    public void init() {

        jdbcBuscaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PADATOSUSR")
                .withProcedureName("OBTIENEDATOS")
                .returningResultSet("RCL_CONSULTA", new DatosUsuarioRowMapper());

    }

    @SuppressWarnings("unchecked")
    public List<DatosUsuarioDTO> obtieneDatos(String usuario) throws Exception {

        Map<String, Object> out = null;
        List<DatosUsuarioDTO> datosUsuario = null;
        int ejecucion = 0;

        try {

            SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", usuario);

            out = jdbcBuscaUsuario.execute(in);

            logger.info("Funcion ejecutada:{GESTION.PADATOSUSR.OBTIENEDATOS}");

            datosUsuario = (List<DatosUsuarioDTO>) out.get("RCL_CONSULTA");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
            ejecucion = respuestaEjec.intValue();

            if (ejecucion == 0) {
                logger.info("Ocurrio algo en la ejecucion al obtener los datos del usuario");
            }

        } catch (Exception e) {
            logger.info(e.getMessage());

        }

        return datosUsuario;
    }

}
