package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.ExternoPuestoDTO;

public class ExternoPuestoDAOImpl extends DefaultDAO implements ExternoPuestoDAO {

    private static Logger logger = LogManager.getLogger(ExternoPuestoDAOImpl.class);

    private DefaultJdbcCall jdbcActualizaPuesto;
    private DefaultJdbcCall jdbcActualizaPuestoExterno;

    public void init() {

        jdbcActualizaPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMUSUARIO")
                .withProcedureName("SP_ACTPUESTOEXT");

        jdbcActualizaPuestoExterno = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMUSUARIO")
                .withProcedureName("SP_ACTPUESTOEXT");

    }

    @Override
    public boolean actualizaPuesto(int numEmpleado, int puesto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", numEmpleado)
                .addValue("PA_PUESTO", puesto);

        out = jdbcActualizaPuesto.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PA_ADM_USUARIO.SP_ACTPUESTOEXT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar el Usuario id(" + numEmpleado + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaPuestoExterno(ExternoPuestoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getNumEmpleado())
                .addValue("PA_PUESTO", "" + bean.getPuesto());

        out = jdbcActualizaPuestoExterno.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PA_ADM_USUARIO.SP_ACTPUESTOEXT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al Actualizar el Usuario");
        } else {
            return true;
        }

        return false;
    }

}
