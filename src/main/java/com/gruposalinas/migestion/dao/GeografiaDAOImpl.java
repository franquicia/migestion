package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.GeografiaDTO;
import com.gruposalinas.migestion.mappers.GeografiaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class GeografiaDAOImpl extends DefaultDAO implements GeografiaDAO {

    private static Logger logger = LogManager.getLogger(GeografiaDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaGeo;
    private DefaultJdbcCall jdbcActualizaGeo;
    private DefaultJdbcCall jdbcEliminaGeo;
    private DefaultJdbcCall jdbcBuscaGeo;

    private List<GeografiaDTO> listaGeografia;

    public void init() {

        jdbcBuscaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_SEL_GEO")
                .returningResultSet("RCL_GEOGRAFIA", new GeografiaRowMapper());

        jdbcInsertaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_INS_GEO");

        jdbcActualizaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_ACT_GEO");

        jdbcEliminaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_DEL_GEO");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GeografiaDTO> obtieneGeografia(String idCeco, String idRegion, String idZona, String idTerritorio)
            throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDREG", idRegion)
                .addValue("PA_IDZONA", idZona)
                .addValue("PA_IDTERRITORIO", idTerritorio);

        out = jdbcBuscaGeo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMGEOGRAFIA.SP_SEL_GEO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaGeografia = (List<GeografiaDTO>) out.get("RCL_GEOGRAFIA");

        if (respuesta == 0) {
            logger.info("Algo paso al consular la Geografia ");
        }

        return listaGeografia;
    }

    @Override
    public boolean insertaGeografia(GeografiaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_IDREGION", bean.getIdRegion())
                .addValue("PA_REGION", bean.getNombreRegion())
                .addValue("PA_IDZONA", bean.getIdZona())
                .addValue("PA_ZONA", bean.getNombreZona())
                .addValue("PA_IDTERR", bean.getIdTerritorio())
                .addValue("PA_TERRITORIO", bean.getNombreTerritorio())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaGeo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMGEOGRAFIA.SP_INS_GEO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar la Geografia");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaGeografia(GeografiaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_IDREGION", bean.getIdRegion())
                .addValue("PA_REGION", bean.getNombreRegion())
                .addValue("PA_IDZONA", bean.getIdZona())
                .addValue("PA_ZONA", bean.getNombreZona())
                .addValue("PA_IDTERR", bean.getIdTerritorio())
                .addValue("PA_TERRITORIO", bean.getNombreTerritorio())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaGeo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMGEOGRAFIA.SP_ACT_GEO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al actualizar la Geografia ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaGeografia(String idCeco) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", idCeco);

        out = jdbcEliminaGeo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMGEOGRAFIA.SP_DEL_GEO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar la Geografia ");
        } else {
            return true;
        }

        return false;
    }
}
