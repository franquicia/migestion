package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;
import java.util.List;

public interface ProgLimpiezaDAO {


	public boolean elimina(int idProg)throws Exception;


	public List<ProgLimpiezaDTO> obtieneInfo() throws Exception;

	public boolean actualiza(ProgLimpiezaDTO bean)throws Exception;

	public int insertaPro(ProgLimpiezaDTO bean)throws Exception;


	public List<ProgLimpiezaDTO> obtieneInfoPro() throws Exception;

	public boolean actualizaPro(ProgLimpiezaDTO bean)throws Exception;



}
