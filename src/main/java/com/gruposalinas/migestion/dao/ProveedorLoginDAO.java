package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ProveedorLoginDTO;
import java.util.List;

public interface ProveedorLoginDAO {

    public int inserta(ProveedorLoginDTO bean) throws Exception;

    public boolean elimina(String idProveedor) throws Exception;

    public List<ProveedorLoginDTO> obtieneDatos(int idProveedor) throws Exception;

    public List<ProveedorLoginDTO> obtieneInfo() throws Exception;

    public boolean actualiza(ProveedorLoginDTO bean) throws Exception;

}
