package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ArchivosUrlDTO;
import java.util.List;

public interface ArchivosUrlDAO {

    public int inserta(ArchivosUrlDTO bean) throws Exception;

    public boolean elimina(String idArch) throws Exception;

    public List<ArchivosUrlDTO> obtieneDatos(int idArch) throws Exception;

    public List<ArchivosUrlDTO> obtieneInfo() throws Exception;

    public boolean actualiza(ArchivosUrlDTO bean) throws Exception;

}
