package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PeriodoReporteDTO;
import java.util.List;

public interface PeriodoReporteDAO {

    public List<PeriodoReporteDTO> obtienePeriodoReporte(String idsPerRep, String idPeriodo, String idReporte) throws Exception;

    public boolean insertaPeriodoReporte(int idPeriodo, int idReporte, String horario, int idPerRep, int commit) throws Exception;

    public boolean actualizaPeriodoReporte(int idPeriodo, int idReporte, String horario, int idPerRep, int commit) throws Exception;

    public boolean eliminaPeriodoReporte(String idPerRep) throws Exception;

}
