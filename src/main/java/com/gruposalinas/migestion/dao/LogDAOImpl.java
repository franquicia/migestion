package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.LogDTO;
import com.gruposalinas.migestion.mappers.LogRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class LogDAOImpl extends DefaultDAO implements LogDAO {

    private static Logger logger = LogManager.getLogger(LogDAOImpl.class);

    DefaultJdbcCall jdbcObtieneErrores;
    private DefaultJdbcCall jdbcInsertaUsuario;

    public void init() {

        jdbcObtieneErrores = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMERRORES")
                .withProcedureName("SP_SEL_ERRORES")
                .returningResultSet("RCL_ERRORES", new LogRowMapper());

        jdbcInsertaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_LOGRESULTADOS")
                .withProcedureName("SP_REGISTRAERR");
    }

    @SuppressWarnings("unchecked")
    public List<LogDTO> obtieneErrores(String fecha) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<LogDTO> listaErrores = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHA", fecha);
        out = jdbcObtieneErrores.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMERRORES.SP_SEL_ERRORES}");

        listaErrores = (List<LogDTO>) out.get("RCL_ERRORES");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Errores en la fecha: " + fecha);
        } else {
            return listaErrores;
        }

        return null;

    }

    @Override
    public boolean inserta(int codigo, String mensaje, String origen) {

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NCODIGOERROR", codigo)
                .addValue("PA_VMENSAJEERR", mensaje)
                .addValue("PA_VORIGENERR", origen);

        out = jdbcInsertaUsuario.execute(in);

        logger.info("Funci�n ejecutada:{migestion.PA_LOGRESULTADOS}");

        return true;
    }
}
