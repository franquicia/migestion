package com.gruposalinas.migestion.dao;

public interface AsignacionDAO {

    public int[] ejecutaAsignacion(String ceco, int puesto, int checklist);

    public int[] ejeuctaAsignacionEspecial(String usrNew, String usrOld);

}
