package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TokenUserCiscoSparkDTO;
import com.gruposalinas.migestion.mappers.TokenUserCiscoSparkRowMapper1;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TokenUserCiscoSparkDAOImpl extends DefaultDAO implements TokenUserCiscoSparkDAO {

    private static Logger logger = LogManager.getLogger(TokenUserCiscoSparkDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaTokenUserCiscoSpark;
    private DefaultJdbcCall jdbcObtieneTokenUserCiscoSpark;
    private DefaultJdbcCall jdbcEliminaTokenUserCiscoSpark;

    private List<TokenUserCiscoSparkDTO> listaTokens;

    public void init() {

        jdbcInsertaTokenUserCiscoSpark = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTOKENS")
                .withProcedureName("SP_INS_TOKENS");

        jdbcObtieneTokenUserCiscoSpark = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTOKENS")
                .withProcedureName("SP_SEL_TOKENS")
                .returningResultSet("RCL_TOKENS", new TokenUserCiscoSparkRowMapper1());

        jdbcEliminaTokenUserCiscoSpark = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTOKENS")
                .withProcedureName("SP_DEL_TOKENS_TS");
    }

    @Override
    public boolean insertaDispositivo(TokenUserCiscoSparkDTO data) throws Exception {
        // TODO Auto-generated method stub
        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                //.addValue("FIID_TOKENSP", data.getId())
                .addValue("PA_IDPERSON", data.getPersonId())
                .addValue("PA_TOKEN", data.getToken())
                .addValue("PA_EMAIL", data.getEmail())
                .addValue("PA_TIPO", data.getTipo())
                .addValue("PA_FECHA", data.getFecha());

        out = jdbcInsertaTokenUserCiscoSpark.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTOKENS.SP_INS_TOKENS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");

        int respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el evento");
            return false;
        } else {
            return true;
        }

    }

    @Override
    public List<TokenUserCiscoSparkDTO> obtienePorCorreo(String correo) throws Exception {
        // TODO Auto-generated method stub
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_EMAIL", correo);

        out = jdbcObtieneTokenUserCiscoSpark.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTOKENS.SP_BUSCA_TOKENS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaTokens = (List<TokenUserCiscoSparkDTO>) out.get("RCL_TOKENS");

        if (respuesta == 0) {
            logger.info("Algo paso al consular los eventos ");
        }

        return listaTokens;
    }

    @Override
    public boolean eliminaRegistro(String tokenApple) throws Exception {
        // TODO Auto-generated method stub
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TOKENS", tokenApple);

        out = jdbcEliminaTokenUserCiscoSpark.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTOKENS.SP_DEL_AGENDA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
