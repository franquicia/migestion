package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CecoComboDTO;
import com.gruposalinas.migestion.domain.CecoDTO;
import java.util.List;

public interface CecoDAO {

    public List<CecoDTO> buscaCecos(String idCeco, String idCecoP, String activo, String negocio, String canal, String pais, String nombreCC) throws Exception;

    public List<CecoDTO> buscaCecosSuperior(String idCeco, String negocio) throws Exception;

    public boolean insertaCeco(CecoDTO bean) throws Exception;

    public boolean actualizaCeco(CecoDTO bean) throws Exception;

    public boolean eliminaCeco(int ceco) throws Exception;

    public String negocioCeco(String ceco) throws Exception;

    public List<CecoComboDTO> getCecosCercanos(String ceco, String latitud, String longitud, int inserta_error);
}
