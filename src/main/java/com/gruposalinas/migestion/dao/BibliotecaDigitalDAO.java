package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.BibliotecaDigitalDTO;
import com.gruposalinas.migestion.domain.EnvioCorreoDTO;
import com.gruposalinas.migestion.domain.QuejaDTO;
import java.util.List;

public interface BibliotecaDigitalDAO {
	

	
	public boolean registraEnvio(EnvioCorreoDTO correoDTO)throws Exception;
		
}
