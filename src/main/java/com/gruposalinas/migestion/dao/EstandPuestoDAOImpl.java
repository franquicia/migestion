package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.EstandPuestoDTO;
import com.gruposalinas.migestion.mappers.EstandPuestoMuebleRowMapper;
import com.gruposalinas.migestion.mappers.EstandPuestoPlantillaRowMapper;
import com.gruposalinas.migestion.mappers.EstandPuestoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EstandPuestoDAOImpl extends DefaultDAO implements EstandPuestoDAO {

	private static Logger logger = LogManager.getLogger(EstandPuestoDAOImpl.class);
	

	private DefaultJdbcCall jdbcEliminaEstandPuesto;
	private DefaultJdbcCall jdbcBuscaParam;
	private DefaultJdbcCall jdbcBuscaInfoEstandPuesto;
	private DefaultJdbcCall jdbcActualizaEstandPuesto;
	private DefaultJdbcCall jdbcInsertaMueble;
	private DefaultJdbcCall jdbcBuscaInfoMueble;
	private DefaultJdbcCall jdbcBuscaParamMueble;
	private DefaultJdbcCall jdbcBuscaPlantillaMueble;
	private DefaultJdbcCall jdbcActualizaMueble;
	
	private List<EstandPuestoDTO> listaDetU;

    public void init() {

		


		jdbcEliminaEstandPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_DEL_ESTPUES");

		// por todo el detalle
		jdbcBuscaInfoEstandPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_ESTPUES", new EstandPuestoRowMapper());

		//por id
		jdbcBuscaParam = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_SEL_ID")
				.returningResultSet("RCL_ESTPUES", new EstandPuestoPlantillaRowMapper());

		
		jdbcActualizaEstandPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_ACT_ESTPUES");
		//mueble
		jdbcInsertaMueble = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_INS_MUEBLE");
		

		
		// por todo el detalle
		jdbcBuscaInfoMueble = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_SEL_DETALLEMUE")
				.returningResultSet("RCL_MUEBLE", new EstandPuestoMuebleRowMapper());
		
		//por id
		jdbcBuscaParamMueble = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_SEL_MUEBLE")
				.returningResultSet("RCL_MUEBLE", new EstandPuestoMuebleRowMapper());
		
		jdbcBuscaPlantillaMueble = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_SEL_IDMUEB")
				.returningResultSet("RCL_MUEBLE", new EstandPuestoMuebleRowMapper());
		
	
		
		jdbcActualizaMueble= (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMESTPUESTO")
				.withProcedureName("SP_ACT_MUEBLE");
		
		
			
	}

	@Override
	public boolean elimina(int idPuesto) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_FIID_TAB",idPuesto);
		
		out = jdbcEliminaEstandPuesto.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_DEL_ESTPUES}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<EstandPuestoDTO> obtieneDatos(String ceco) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CECO", ceco);

        out = jdbcBuscaParam.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_SEL_ID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<EstandPuestoDTO>) out.get("RCL_ESTPUES");

        if (respuesta != 1) {
            logger.info("Algo paso al consular La tabla Estandarizacion por puesto ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<EstandPuestoDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<EstandPuestoDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoEstandPuesto.execute();

		logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_SEL_DETALLE}");
		listaDetU = (List<EstandPuestoDTO>) out.get("RCL_ESTPUES");
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		error = resultado.intValue();
		if(error != 1){
			logger.info("Algo paso al obtener las EstandPuesto");
		}else{
			return listaDetU;
		}		
				
		return listaDetU;
	}
	
	@Override
	public boolean actualiza(EstandPuestoDTO bean) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TAB", bean.getIdPuesto())
                .addValue("PA_FIID_SUC", bean.getCeco())
                .addValue("PA_FIID_USU", bean.getIdUsuario())
                .addValue("PA_PUESTO", bean.getPuesto());

        out = jdbcActualizaEstandPuesto.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_ACT_ESTPUES}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    ///mueble
    @Override
    public int insertaMueble(EstandPuestoDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDESTAND", bean.getIdPuesto())
                .addValue("PA_FIID_SUC", bean.getCeco())
                .addValue("PA_MUEBLE1C1", bean.getMueble1caj1())
                .addValue("PA_MUEBLE1C2", bean.getMueble1caj2())
                .addValue("PA_MUEBLE1C3", bean.getMueble1caj3())
                .addValue("PA_MUEBLE2C4", bean.getMueble2caj1())
                .addValue("PA_MUEBLE2C5", bean.getMueble2caj2())
                .addValue("PA_MUEBLE2C6", bean.getMueble2caj6())
                .addValue("PA_STATUS", bean.getBandera());

        out = jdbcInsertaMueble.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_INS_MUEBLE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_FIID_TAB");
        idNota = idbloc.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

//con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<EstandPuestoDTO> obtieneDatosMueble(String idPuesto) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDESTAND", idPuesto);

        out = jdbcBuscaParamMueble.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_SEL_MUEBLE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<EstandPuestoDTO>) out.get("RCL_MUEBLE");

        if (respuesta != 1) {
            logger.info("Algo paso al consular La tabla Estandarizacion por puesto ");
        }

        return listaDetU;
    }
//Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<EstandPuestoDTO> obtieneInfoMueble() throws Exception {
        Map<String, Object> out = null;
        List<EstandPuestoDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoMueble.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_SEL_DETALLEMUE}");
        listaDetU = (List<EstandPuestoDTO>) out.get("RCL_MUEBLE");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las EstandPuesto");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

//para Plantilla
    @SuppressWarnings("unchecked")
    @Override
    public List<EstandPuestoDTO> obtieneDatosPlantillaMueble(String ceco) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_SUC", ceco);

        out = jdbcBuscaPlantillaMueble.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_SEL_IDMUEB}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<EstandPuestoDTO>) out.get("RCL_MUEBLE");

        if (respuesta != 1) {
            logger.info("Algo paso al consular La tabla Estandarizacion por puesto ");
        }

        return listaDetU;
    }

    @Override
    public boolean actualizaMueble(EstandPuestoDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TAB", bean.getIdMueble())
                .addValue("PA_IDESTAND", bean.getIdPuesto())
                .addValue("PA_FIID_SUC", bean.getCeco())
                .addValue("PA_MUEBLE1C1", bean.getMueble1caj1())
                .addValue("PA_MUEBLE1C2", bean.getMueble1caj2())
                .addValue("PA_MUEBLE1C3", bean.getMueble1caj3())
                .addValue("PA_MUEBLE2C4", bean.getMueble2caj1())
                .addValue("PA_MUEBLE2C5", bean.getMueble2caj2())
                .addValue("PA_MUEBLE2C6", bean.getMueble2caj6())
                .addValue("PA_STATUS", bean.getBandera());

        out = jdbcActualizaMueble.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMESTPUESTO.SP_ACT_MUEBLE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
