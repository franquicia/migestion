package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CecoComboDTO;
import com.gruposalinas.migestion.domain.InfoAsesoresDigitalesDTO;
import com.gruposalinas.migestion.domain.NotificaAfDTO;
import com.gruposalinas.migestion.domain.TokensAsesoresDTO;
import java.util.List;

public interface AsesoresDigitalesDAO {

    public List<CecoComboDTO> getCecos() throws Exception;

    public boolean insertInfoSIE(InfoAsesoresDigitalesDTO infoAsesoresDigitales) throws Exception;

    public List<TokensAsesoresDTO> getCompromisosAvance() throws Exception;

    public List<NotificaAfDTO> getCecosToUpdate() throws Exception;

    public boolean updateCeco(NotificaAfDTO notificaAFDTO) throws Exception;

    /*Metodos que sirven para el administrador de la tabla*/
    public List<NotificaAfDTO> consulta(String ceco) throws Exception;

    public boolean inserta(NotificaAfDTO objInsertar) throws Exception;

    public boolean actualiza(NotificaAfDTO objActualizar) throws Exception;

    public boolean elimina(int id) throws Exception;

    public boolean trunca() throws Exception;

}
