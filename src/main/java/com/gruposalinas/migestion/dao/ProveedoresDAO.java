package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ProveedoresDTO;
import java.util.List;

public interface ProveedoresDAO {

    public int inserta(ProveedoresDTO bean) throws Exception;

    public int insertaLimpieza(ProveedoresDTO bean) throws Exception;

    public boolean elimina(String idProveedor) throws Exception;

    public boolean depura() throws Exception;

    public List<ProveedoresDTO> obtieneDatos(int idProveedor) throws Exception;

    public List<ProveedoresDTO> obtieneInfo() throws Exception;

    public List<ProveedoresDTO> obtieneInfoLimpieza() throws Exception;

    public boolean actualiza(ProveedoresDTO bean) throws Exception;

}
