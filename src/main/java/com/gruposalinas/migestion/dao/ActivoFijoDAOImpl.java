package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import com.gruposalinas.migestion.mappers.ActivoFijoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import com.gruposalinas.migestion.mappers.ActivoFijoRowMapper;

public class ActivoFijoDAOImpl extends DefaultDAO implements ActivoFijoDAO{
	private static Logger logger = LogManager.getLogger(AgendaDAOImpl.class);

	private DefaultJdbcCall jdbcActualizaActivoFijo;
	private DefaultJdbcCall jdbcEliminaActivoFijo;

	private DefaultJdbcCall jdbcConsultaActivoFijoAll;

	public void init(){


		jdbcActualizaActivoFijo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
			.withSchemaName("GESTION")
			.withCatalogName("PAADMACTFIJO")
			.withProcedureName("SP_ACT_ACTFIJO");
		jdbcEliminaActivoFijo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
			.withSchemaName("GESTION")
			.withCatalogName("PAADMACTFIJO")
			.withProcedureName("SP_DEL_ACTFIJO");

		jdbcConsultaActivoFijoAll = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
			.withSchemaName("GESTION")
			.withCatalogName("PAADMACTFIJO")
			.withProcedureName("SP_SEL_ACTFIJO_A")
			.returningResultSet("RCL_ACTFIJO", new ActivoFijoRowMapper());
	}

	@Override
	public int actualizaActivoFijo(ActivoFijoDTO bean) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_ACTFIJO", bean.getIdActivoFijo())
                .addValue("PA_FIID_DEPACTFIJO", bean.getDepActivoFijo())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_FCPLACA", bean.getPlaca())
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FCMARCA", bean.getMarca())
                .addValue("PA_FCSERIE", bean.getMarca());

        out = jdbcActualizaActivoFijo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMACTFIJO.SP_ACT_ACTFIJO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al actualizar los datos de la tabla GETAACTFIJO.");
        } else {
            return respuesta;
        }
        return respuesta;
    }

    @Override
    public int eliminaActivoFijo(int idActivoFijo) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_FIID_ACTFIJO",idActivoFijo);
		
		out = jdbcEliminaActivoFijo.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMACTFIJO.SP_DEL_ACTFIJO}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		if(respuesta !=  1){
			logger.info("Algo paso al eliminar los datos de la tabla GETAACTFIJO.");
		}else{
			return respuesta;
		}	
		return respuesta;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<ActivoFijoDTO> consultaActivoFijoAll() throws Exception {
		
		Map<String, Object> out = null;
		int ejecucion = 0;
		List<ActivoFijoDTO> lista = null;
		
		out = jdbcConsultaActivoFijoAll.execute();
		
		logger.info("Funcion ejecutada: {GESTION.PAADMACTFIJO.SP_SEL_ACTFIJO_A}");
		
		BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
		ejecucion = returnEjecucion.intValue();
				
		if(ejecucion == 0){
			lista = new ArrayList<ActivoFijoDTO>();
			logger.info("Ocurrio un problema al obtener los datos de la tabla GETAACTFIJO.");
		}else{
			lista = (List<ActivoFijoDTO>) out.get("RCL_ACTFIJO");
		}

        return lista;
    }

}
