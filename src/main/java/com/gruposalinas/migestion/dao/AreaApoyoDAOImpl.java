package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.AreaApoyoDTO;
import com.gruposalinas.migestion.mappers.AreaApoyoCecoRowMapper;
import com.gruposalinas.migestion.mappers.AreaApoyoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AreaApoyoDAOImpl extends DefaultDAO implements AreaApoyoDAO {

	private static Logger logger = LogManager.getLogger(AreaApoyoDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaAreaApo;
	private DefaultJdbcCall jdbcEliminaEvid;
	private DefaultJdbcCall jdbcBuscaDetalle;
	private DefaultJdbcCall jdbcActualizaEvid;

    private List<AreaApoyoDTO> listaDetU;

    public void init() {

		
		jdbcEliminaEvid = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMAREAAPOYO")
				.withProcedureName("SP_DEL_EVID");

		jdbcEliminaAreaApo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMAREAAPOYO")
				.withProcedureName("SP_DEL_AREAAP");




		jdbcBuscaDetalle = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMAREAAPOYO")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_AREAP", new AreaApoyoRowMapper());


		jdbcActualizaEvid = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMAREAAPOYO")
				.withProcedureName("SP_ACT_EVID");


	}

	
	@Override
	public boolean elimina(String idArea) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDAREAAP",idArea);
		
		out = jdbcEliminaAreaApo.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMAREAAPOYO.SP_DEL_AREAAP}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}
	
	@Override
	public boolean eliminaEvi(String idEvid) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_EVID",idEvid);
		
		out = jdbcEliminaEvid.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMAREAAPOYO.SP_DEL_EVID}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}

	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<AreaApoyoDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<AreaApoyoDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaDetalle.execute();

		logger.info("Funcion ejecutada: {GESTION.PAADMAREAAPOYO.SP_SEL_DETALLE}");
		listaDetU = (List<AreaApoyoDTO>) out.get("RCL_AREAP");
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		error = resultado.intValue();
		if(error != 1){
			logger.info("Algo paso al obtener las AreaApo");
		}else{
			return listaDetU;
		}		
				
		return listaDetU;
	}
	
	
	@Override
	public boolean actualizaEvi(AreaApoyoDTO bean) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDAREAAP", bean.getIdArea())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_NOMBRE", bean.getNombreArc())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_GUIAEVID", bean.getIdEvid())
                .addValue("PA_OBSERVAC", bean.getDescr());

        out = jdbcActualizaEvid.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMAREAAPOYO.SP_ACT_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
