package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.ReporteDTO;
import com.gruposalinas.migestion.mappers.ReporteRowMapper;

public class ReporteDAOImpl extends DefaultDAO implements ReporteDAO {

    private static Logger logger = LogManager.getLogger(ReporteDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaReporte;
    private DefaultJdbcCall jdbcActualizaReporte;
    private DefaultJdbcCall jdbcEliminaReporte;
    private DefaultJdbcCall jdbcBuscaReporte;

    private List<ReporteDTO> listaReporte;

    public void init() {

        jdbcBuscaReporte = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMREPORTE")
                .withProcedureName("SP_SEL_REPORTE")
                .returningResultSet("RCL_REPORTE", new ReporteRowMapper());

        jdbcInsertaReporte = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMREPORTE")
                .withProcedureName("SP_INS_REPORTE");

        jdbcActualizaReporte = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMREPORTE")
                .withProcedureName("SP_ACT_REPORTE");

        jdbcEliminaReporte = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMREPORTE")
                .withProcedureName("SP_DEL_REPORTE");

    }

    @Override
    public List<ReporteDTO> obtieneReporte(String idReporte, String nombre) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDREPORTE", idReporte)
                .addValue("PA_NOMBRE", nombre);

        out = jdbcBuscaReporte.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMREPORTE.SP_SEL_REPORTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaReporte = (List<ReporteDTO>) out.get("RCL_REPORTE");

        if (respuesta == 0) {
            logger.info("Algo paso al consular los reportes ");
        }

        return listaReporte;
    }

    @Override
    public boolean insertaReporte(int idReporte, String nombre, int commit) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", nombre)
                .addValue("PA_IDREPORTE", idReporte)
                .addValue("PA_COMMIT", commit);

        out = jdbcInsertaReporte.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMREPORTE.SP_INS_REPORTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el reporte");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaReporte(int idReporte, String nombre, int commit) throws Exception {

        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", nombre)
                .addValue("PA_IDREPORTE", idReporte)
                .addValue("PA_COMMIT", commit);

        out = jdbcActualizaReporte.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMREPORTE.SP_ACT_REPORTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al actualizar el reporte ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaReporte(String idReporte) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDREPORTE", idReporte);

        out = jdbcEliminaReporte.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMREPORTE.SP_DEL_REPORTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar el reporte ");
        } else {
            return true;
        }

        return false;
    }

}
