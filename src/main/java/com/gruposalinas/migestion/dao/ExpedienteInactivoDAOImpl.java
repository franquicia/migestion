package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.ExpedienteInactivoDTO;
import com.gruposalinas.migestion.mappers.ExpedienteInactivoRowMapper;
import com.gruposalinas.migestion.mappers.ResponsableExpRowMapper;

public class ExpedienteInactivoDAOImpl extends DefaultDAO implements ExpedienteInactivoDAO {

	private static Logger logger = LogManager.getLogger(ExpedienteInactivoDAOImpl.class);
	
	private DefaultJdbcCall jdbcActualizaEstatus;
	private DefaultJdbcCall jdbcConsultaExpediente;
	private DefaultJdbcCall jdbcConsultaResponsable;
	private DefaultJdbcCall jdbcEliminaExpediente;
	private DefaultJdbcCall jdbcEliminaResponsable;

	public void init(){
		

		jdbcActualizaEstatus = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAEXPEDIENTEPASI")
				.withProcedureName("SP_ACT_ESTATUS");

        jdbcConsultaExpediente = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAEXPEDIENTEPASI")
                .withProcedureName("SP_SEL_EXPEDENTE")
                .returningResultSet("RCL_EXPEDIENTE", new ExpedienteInactivoRowMapper());

        jdbcConsultaResponsable = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAEXPEDIENTEPASI")
                .withProcedureName("SP_SEL_RESPONSABLE")
                .returningResultSet("RCL_RESPONSABLE", new ResponsableExpRowMapper());

        jdbcEliminaExpediente = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAEXPEDIENTEPASI")
                .withProcedureName("SP_DEL_EXP");

		jdbcEliminaResponsable = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAEXPEDIENTEPASI")
				.withProcedureName("SP_DEL_RESPONSABLES");
	}

	@Override
	public boolean actualizaEstatus(int estatus, int idExpediente) throws Exception {
		
		Map<String, Object> out = null;
		int error = 0;
		
		
		SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ESTATUS", estatus)
				.addValue("PA_EXPEDIENTE", idExpediente);
		
		out = jdbcActualizaEstatus.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASI.SP_ACT_ESTATUS}");
		
		BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
		error = errorReturn.intValue();
	
		
		if(error == 0){
			logger.info("Algo ocurrió al actualizar el estatus");
		}else{
			return true;
		}
		
		return false;
	}
	@Override
	public List<ExpedienteInactivoDTO> buscaExpediente(String idExpediente, String idEstatus, String idCeco, String idUsuario,
			String fechaI, String fechaF) throws Exception {
		Map<String , Object> out = null;
		
		List<ExpedienteInactivoDTO> listaExpediente = null;
		int error = 0;
		
		SqlParameterSource in = new MapSqlParameterSource().addValue("PA_EXPEDIENTE", idExpediente)
				                                           .addValue("PA_ESTATUS", idEstatus)
				                                           .addValue("PA_IDCECO", idCeco)
				                                           .addValue("PA_IDUSUA", idUsuario)
				                                           .addValue("PA_FECHAI", fechaI)
				                                           .addValue("PA_FECHAF", fechaF);
		
		out = jdbcConsultaExpediente.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASI.SP_SEL_EXPEDENTE}");
		
		listaExpediente = (List<ExpedienteInactivoDTO>) out.get("RCL_EXPEDIENTE");
		
		BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
		error = errorReturn.intValue();
		
		if(error == 0){
			logger.info("Algo ocurrió al obtener el Expediente");
		}else{
			return listaExpediente;
		}
				
		return null;
	}
	@Override
	public List<ExpedienteInactivoDTO> buscaResponsable(String idExpediente, String idEstatus, String idUsuario)
			throws Exception {
		Map<String , Object> out = null;
		
		List<ExpedienteInactivoDTO> listaExpediente = null;
		int error = 0;
		
		SqlParameterSource in = new MapSqlParameterSource().addValue("PA_EXPEDIENTE", idExpediente)
				                                           .addValue("PA_ESTATUS", idEstatus)
				                                           .addValue("PA_IDUSUA", idUsuario);
		
		out = jdbcConsultaResponsable.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASI.SP_SEL_RESPONSABLE}");
		
		listaExpediente = (List<ExpedienteInactivoDTO>) out.get("RCL_RESPONSABLE");
		
		BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
		error = errorReturn.intValue();
		
		if(error == 0){
			logger.info("Algo ocurrió al obtener el Expediente");
		}else{
			return listaExpediente;
		}
				
		return null;
	}
	@Override
	public boolean eliminaExpediente(int idExpediente) throws Exception {
		
		Map<String, Object> out = null;
		int error = 0;
		
		
		SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDEXPEDIENTE", idExpediente);
		
		out = jdbcEliminaExpediente.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASI.SP_DEL_EXP}");
		
		BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
		error = errorReturn.intValue();
	
		
		if(error == 0){
			logger.info("Algo ocurrió al eliminar el expediente");
		}else{
			return true;
		}
		
		return false;
	}
	@Override
	public boolean eliminaResponsables(int idExpediente) throws Exception {
		
		Map<String, Object> out = null;
		int error = 0;
		
		
		SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDEXPEDIENTE", idExpediente);
		
		out = jdbcEliminaResponsable.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASI.SP_DEL_RESPONSABLES}");
		
		BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
		error = errorReturn.intValue();
	
		
		if(error == 0){
			logger.info("Algo ocurrió al eliminar los responsables");
		}else{
			return true;
		}
		
		return false;
	}

}
