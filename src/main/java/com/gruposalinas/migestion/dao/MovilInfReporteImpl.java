package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.MovilInfoDTO;
import com.gruposalinas.migestion.domain.MovilinfReporteDTO;
import com.gruposalinas.migestion.domain.ReporteMensualMovilDTO;
import com.gruposalinas.migestion.mappers.CecoRowMapper;
import com.gruposalinas.migestion.mappers.MovilInfoNuevoRowMapper;
import com.gruposalinas.migestion.mappers.MovilInfoReporteRowMapper;
import com.gruposalinas.migestion.mappers.ReporteMensualMovilMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class MovilInfReporteImpl extends DefaultDAO implements MovilInfReporteDAO {

    private static Logger logger = LogManager.getLogger(MovilInfReporteImpl.class);

    DefaultJdbcCall jdbcReporteMensual;
    DefaultJdbcCall jdbcDetalleUsuario;
    DefaultJdbcCall jdbcReporteMensualCeco;
    DefaultJdbcCall jdbcCecosHijo;
    DefaultJdbcCall jdbcReporteSemanal;

    public void init() {

        jdbcReporteMensual = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPORTE_MOVILINF")
                .withProcedureName("SPREPORTE_MENSUAL")
                .returningResultSet("RCL_REP_MOVI", new MovilInfoReporteRowMapper());

        jdbcDetalleUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPORTE_MOVILINF")
                .withProcedureName("SPREPORTE_DETALLE_USR")
                .returningResultSet("RCL_REP_MOVI", new MovilInfoNuevoRowMapper());

        jdbcReporteMensualCeco = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPORTE_MOVILINF")
                .withProcedureName("SPREP_MENSUAL_CECO")
                .returningResultSet("RCL_REPMENSUAL", new ReporteMensualMovilMapper());

        jdbcCecosHijo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPORTE_MOVILINF")
                .withProcedureName("SPCECOS_HIJOS")
                .returningResultSet("RCL_HIJOS", new CecoRowMapper());

        jdbcReporteSemanal = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPORTE_MOVILINF")
                .withProcedureName("SPREP_SEMANAL_CECO")
                .returningResultSet("RCL_REPSEMANA", new ReporteMensualMovilMapper());

    }

    @SuppressWarnings("unchecked")
    public List<MovilinfReporteDTO> getReporteMensual(String mes, String anio) throws Exception {
        Map<String, Object> out = null;
        List<MovilinfReporteDTO> listaUsuario = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("VL_FCMES", mes)
                .addValue("VL_FCANIO", anio);

        out = jdbcReporteMensual.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAREPORTE_MOVILINF.SPREPORTE_MENSUAL}");

        listaUsuario = (List<MovilinfReporteDTO>) out.get("RCL_REP_MOVI");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el reporte mensual");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

    @SuppressWarnings("unchecked")
    public List<MovilInfoDTO> getDetalleUsuarios(int idUsuario, String mes, String anio) throws Exception {
        Map<String, Object> out = null;
        List<MovilInfoDTO> listaUsuario = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("VL_USR", idUsuario)
                .addValue("VL_FCMES", mes)
                .addValue("VL_FCANIO", anio);

        out = jdbcDetalleUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAREPORTE_MOVILINF.SPREPORTE_DETALLE_USR}");

        listaUsuario = (List<MovilInfoDTO>) out.get("RCL_REP_MOVI");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el reporte mensual");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

    @SuppressWarnings("unchecked")
    public List<ReporteMensualMovilDTO> getReporteMensualCeco(int pais, int negocio, int ceco, String mes, String anio) throws Exception {
        Map<String, Object> out = null;
        List<ReporteMensualMovilDTO> listaUsuario = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("VL_FCMES", mes)
                .addValue("VL_FCANIO", anio)
                .addValue("VL_FICECO", ceco)
                .addValue("VL_FINEGOCIO", negocio)
                .addValue("VL_FIPAIS", pais);

        out = jdbcReporteMensualCeco.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAREPORTE_MOVILINF.SPREP_MENSUAL_CECO}");

        listaUsuario = (List<ReporteMensualMovilDTO>) out.get("RCL_REPMENSUAL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el reporte mensual");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

    @SuppressWarnings("unchecked")
    public List<CecoDTO> getCecosHijos(int pais, int negocio, String ceco) throws Exception {
        Map<String, Object> out = null;
        List<CecoDTO> listaUsuario = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("VL_FCCECO", ceco)
                .addValue("VL_FINEGOCIO", negocio)
                .addValue("VL_FIPAIS", pais);

        out = jdbcCecosHijo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAREPORTE_MOVILINF.SPCECOS_HIJOS}");

        listaUsuario = (List<CecoDTO>) out.get("RCL_HIJOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el reporte mensual");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

    @SuppressWarnings("unchecked")
    public List<ReporteMensualMovilDTO> getReporteSemanalCeco(int pais, int negocio, int ceco, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<ReporteMensualMovilDTO> listaUsuario = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("VL_FCFEC_INI", fechaInicio)
                .addValue("VL_FCFEC_FIN", fechaFin)
                .addValue("VL_FICECO", ceco)
                .addValue("VL_FINEGOCIO", negocio)
                .addValue("VL_FIPAIS", pais);

        out = jdbcReporteSemanal.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAREPORTE_MOVILINF.SPREP_SEMANAL_CECO}");

        listaUsuario = (List<ReporteMensualMovilDTO>) out.get("RCL_REPSEMANA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el reporte mensual");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

}
