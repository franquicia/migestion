package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ExternoPuestoDTO;

public interface ExternoPuestoDAO {

    public boolean actualizaPuesto(int numEmpleado, int puesto) throws Exception;

    public boolean actualizaPuestoExterno(ExternoPuestoDTO bean) throws Exception;

}
