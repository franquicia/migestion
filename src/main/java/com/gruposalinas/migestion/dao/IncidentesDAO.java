package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.IncidentesDTO;
import java.util.List;

public interface IncidentesDAO {

    public List<IncidentesDTO> obtieneIncidentes(String idIncidente, String idUsuario) throws Exception;

    public boolean insertaIncidentes(String idIncidente, int idUsuario, int commit) throws Exception;

    public boolean actualizaIncidente(String idIncidente, int idUsuario, int commit) throws Exception;

    public boolean eliminaIncidente(String idIncidente) throws Exception;

}
