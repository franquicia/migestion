package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import java.util.List;
import java.util.Map;

public interface ReporteChecklistDAO {

	public Map<String, Object> obtieneTotalCheck(int idUsuario, int idReporte, int idCheck);
	
	public Map<String, Object>obtieneSucursalesCheck(int idUsuario,int idReporte, String idCeco, int idPais, int idCanal, int bandera, int idCheck)throws Exception;

	public Map<String, Object> obtieneDetalleCheck(int idCeco, int idReporte);

	public List<ReporteChecklistDTO> obtieneListaCheck (int idUsuario);
	
	public List<ReporteChecklistDTO> obtieneFechaTermino (int idChecklist, int idUsuario, int idCeco);
			
}
