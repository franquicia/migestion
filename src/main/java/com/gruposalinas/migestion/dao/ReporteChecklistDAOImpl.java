package com.gruposalinas.migestion.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import com.gruposalinas.migestion.mappers.FechaTerCheckRowMapper;
import com.gruposalinas.migestion.mappers.PeriodoReporteRowMapper;
import com.gruposalinas.migestion.mappers.ReporteCheckPorcDet;
import com.gruposalinas.migestion.mappers.ReporteCheckPorcentaje;
import com.gruposalinas.migestion.mappers.ReporteChecklistRowMapper;
import com.gruposalinas.migestion.mappers.ReporteListaCheckRowMapper;
import com.gruposalinas.migestion.mappers.TareaSucursalesRowMapper;
import com.gruposalinas.migestion.mappers.UsuarioTareaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;

public class ReporteChecklistDAOImpl   extends DefaultDAO implements ReporteChecklistDAO  {
	
	private static Logger logger = LogManager.getLogger(ReporteChecklistDAOImpl.class);
	
	private DefaultJdbcCall jdbcSucursalesCheck;
	private DefaultJdbcCall jdbcTotalCheck;
	private DefaultJdbcCall jdbcDetalleCheck;
	private DefaultJdbcCall jdbcListaCheck;
	private DefaultJdbcCall jdbcFechaTermino;
	
	
	
	public void init(){
		
		jdbcTotalCheck = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("FRANQUICIA")
				.withCatalogName("PAGNLCHECKLIST")
				.withProcedureName("SP_REPORTE_TCHECK")
				.returningResultSet("RCL_PERIODO", new PeriodoReporteRowMapper());
		
		jdbcSucursalesCheck = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("FRANQUICIA")
				.withCatalogName("PAGNLCHECKLIST")
				.withProcedureName("SP_REPORTE_CHECK")
				.returningResultSet("RCL_SUCURSALES", new TareaSucursalesRowMapper())
				.returningResultSet("RCL_PERIODO", new PeriodoReporteRowMapper());
		
		jdbcDetalleCheck = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("FRANQUICIA")
				.withCatalogName("PAGNLCHECKLIST")
				.withProcedureName("SP_REPORTE_DCHECK")
				.returningResultSet("RCL_USUARIOS", new UsuarioTareaRowMapper())
				.returningResultSet("RCL_CHECKS", new ReporteChecklistRowMapper());
		
		jdbcListaCheck = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("FRANQUICIA")
				.withCatalogName("PAGNLCHECKLIST")
				.withProcedureName("SP_LISTA_CHECK")
				.returningResultSet("RCL_CHECK", new ReporteListaCheckRowMapper());
		
		jdbcFechaTermino = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("FRANQUICIA")
				.withCatalogName("PAGESTION")
				.withProcedureName("SP_FECHA_TERM")
				.returningResultSet("RCL_TERMINO", new FechaTerCheckRowMapper());
		
	
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object>obtieneSucursalesCheck(int idUsuario,int idReporte, String idCeco, int idPais, int idCanal, int bandera, int idCheck) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		Map<String, Object> lista = null;
		List<ReporteChecklistDTO> listaCheck = null;
		List<PeriodoReporteRowMapper> listaPeriodo = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_USUARIO", idUsuario) 
				.addValue("PA_REPORTE", idReporte)
				.addValue("PA_CC", idCeco)
				.addValue("PA_PAIS", idPais)
				.addValue("PA_CANAL", idCanal)
				.addValue("PA_BANDERA", bandera)
				.addValue("PA_IDCHECK", idReporte);
	
		out = jdbcSucursalesCheck.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAGNLCHECKLIST.SP_REPORTE_CHECK}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		listaCheck = (List<ReporteChecklistDTO>) out.get("RCL_SUCURSALES");
		listaPeriodo = (List<PeriodoReporteRowMapper>) out.get("RCL_PERIODO");
				
		if(respuesta ==  0){
			logger.info("Algo paso al consular las sucursales con checklist ");
		}
		

    	lista = new HashMap<String, Object>();
    	lista.put("listaCheck",listaCheck);
    	lista.put("listaPeriodo",listaPeriodo);
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object>obtieneTotalCheck(int idUsuario,int idReporte, int idCheck) {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		Map<String, Object> lista = null;
		List<PeriodoReporteRowMapper> listaPeriodo = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_USUARIO", idUsuario).addValue("PA_REPORTE", idReporte).addValue("PA_IDCHECK", idCheck);
		
		out = jdbcTotalCheck.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAGNLCHECKLIST.SP_REPORTE_TOTAL}");

		BigDecimal total = (BigDecimal) out.get("PA_TOTAL");
		int totalCheck = total.intValue();
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		listaPeriodo = (List<PeriodoReporteRowMapper>) out.get("RCL_PERIODO");
		
		if(respuesta ==  0){
			logger.info("Algo paso al generar el total checklist realizados");
		}	

    	lista = new HashMap<String, Object>();
    	lista.put("listaPeriodo",listaPeriodo);
    	lista.put("total",totalCheck);
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> obtieneDetalleCheck(int idCeco, int idReporte) {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		Map<String, Object> lista = null;
		List<ReporteChecklistDTO> listaUsuarios = null;
		List<ReporteChecklistDTO> listaCheckUsuario = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_CC", idCeco)
				.addValue("PA_REPORTE", idReporte);
	
		out = jdbcDetalleCheck.execute(in);
		
		logger.info("Funcion ejecutada: {FRANQUICIA.PAGNLCHECKLIST.SP_REPORTE_DCHECK}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		listaUsuarios = (List<ReporteChecklistDTO>) out.get("RCL_USUARIOS");
		listaCheckUsuario = (List<ReporteChecklistDTO>) out.get("RCL_CHECKS");
				
		if(respuesta ==  0){
			logger.info("Algo paso al consular el detalle de los checklist ");
		}
		

    	lista = new HashMap<String, Object>();
    	lista.put("listaUsuarios",listaUsuarios);
    	lista.put("listaCheckUsuario",listaCheckUsuario);
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReporteChecklistDTO> obtieneListaCheck(int idUsuario) {
		List<ReporteChecklistDTO> listaCheck = null;
		Map<String, Object> out = null;
		int ejecucion = 0;

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDUSUARIO", idUsuario);
		out = jdbcListaCheck.execute(in);
		
		BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
		ejecucion = returnejec.intValue();
		
		listaCheck = (List<ReporteChecklistDTO>) out.get("RCL_CHECK");
		
		if(ejecucion == 0){
			logger.info("Ocurrio un problema al obtener los checklist");
		}		

		return listaCheck;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReporteChecklistDTO> obtieneFechaTermino(int idChecklist, int idUsuario, int idCeco) {
		List<ReporteChecklistDTO> listaCheck = null;
		Map<String, Object> out = null;
		int ejecucion = 0;

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDUSUARIO", idUsuario)
		        .addValue("PA_CHECKLIST", idChecklist)
		        .addValue("PA_IDCECO", idCeco);
		
		out = jdbcFechaTermino.execute(in);
		
		BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
		ejecucion = returnejec.intValue();
		
		listaCheck = (List<ReporteChecklistDTO>) out.get("RCL_TERMINO");
		
		if(ejecucion == 0){
			logger.info("Ocurrio un problema al obtener la fecha Termino");
		}		

		return listaCheck;
	}

}
