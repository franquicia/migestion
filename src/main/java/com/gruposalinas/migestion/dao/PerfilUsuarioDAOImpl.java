package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PerfilUsuarioDTO;
import com.gruposalinas.migestion.mappers.PerfilUsuarioComRowMapper;
import com.gruposalinas.migestion.mappers.PerfilUsuarioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PerfilUsuarioDAOImpl extends DefaultDAO implements PerfilUsuarioDAO {

    private static Logger logger = LogManager.getLogger(PerfilDAOImpl.class);

    private DefaultJdbcCall jdbcObtienePerfiles;
    private DefaultJdbcCall jdbcObtienePerfilesNue;
    private DefaultJdbcCall jdbcInsertaPerfilUsuario;
    private DefaultJdbcCall jdbcInsertaPerfilUsuarioNvo;
    private DefaultJdbcCall jdbcActualizaPerfilUsuario;
    private DefaultJdbcCall jdbcEliminaPerfilUsuario;

    public void init() {

        jdbcObtienePerfiles = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_SEL_PERFILUSU")
                .returningResultSet("RCL_USUARIO", new PerfilUsuarioRowMapper());

        jdbcObtienePerfilesNue = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_SEL_NUEV")
                .returningResultSet("RCL_USUARIO", new PerfilUsuarioComRowMapper());

        jdbcInsertaPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_INS_PERFILUSU");

        jdbcInsertaPerfilUsuarioNvo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_INS_NVO");

        jdbcActualizaPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_ACT_PERFILUSU");

        jdbcEliminaPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_DEL_PERFILUSU");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PerfilUsuarioDTO> obtienePerfiles(String idUsuario, String idPerfil) throws Exception {

        Map<String, Object> out = null;
        List<PerfilUsuarioDTO> listaUsuarioPerfil = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", idUsuario).addValue("PA_ID_PERFIL", idPerfil);

        out = jdbcObtienePerfiles.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILUSU.SP_SEL_PERFILUSU}");

        listaUsuarioPerfil = (List<PerfilUsuarioDTO>) out.get("RCL_USUARIO");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al obtener los perfiles ");
        } else {
            return listaUsuarioPerfil;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PerfilUsuarioDTO> obtienePerfilesNue(String idUsuario, String idPerfil) throws Exception {

        Map<String, Object> out = null;
        List<PerfilUsuarioDTO> listaUsuarioPerfil = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", idUsuario).addValue("PA_ID_PERFIL", idPerfil);

        out = jdbcObtienePerfilesNue.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILUSU.SP_SEL_PERFILUSU}");

        listaUsuarioPerfil = (List<PerfilUsuarioDTO>) out.get("RCL_USUARIO");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al obtener los perfiles ");
        } else {
            return listaUsuarioPerfil;
        }

        return null;
    }

    @Override
    public boolean insertaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_USUARIO", perfilUsuarioDTO.getIdUsuario())
                .addValue("PA_ID_PERFIL", perfilUsuarioDTO.getIdPerfil());

        out = jdbcInsertaPerfilUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILUSU.SP_INS_PERFILUSU}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al insertar Perfil Usuario");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean insertaPerfilUsuarioNvo(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_USUARIO", perfilUsuarioDTO.getIdUsuario())
                .addValue("PA_ID_PERFIL", perfilUsuarioDTO.getIdPerfil())
                .addValue("PA_IDACTOR", perfilUsuarioDTO.getIdActor())
                .addValue("PA_BANDNUEVESQ", perfilUsuarioDTO.getBandNvoEsq())
                .addValue("PA_TIPOPROYE", perfilUsuarioDTO.getTipoProyecto());;

        out = jdbcInsertaPerfilUsuarioNvo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILUSU.SP_INS_PERFILUSU}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al insertar Perfil Usuario");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_USUARIO", perfilUsuarioDTO.getIdUsuario())
                .addValue("PA_ID_PERFIL", perfilUsuarioDTO.getIdPerfil());

        out = jdbcActualizaPerfilUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILUSU.SP_ACT_PERFILUSU}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al actualizar Perfil Usuario");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPerfilUsaurio(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_USUARIO", perfilUsuarioDTO.getIdUsuario())
                .addValue("PA_ID_PERFIL", perfilUsuarioDTO.getIdPerfil());

        out = jdbcEliminaPerfilUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILUSU.SP_DEL_PERFILUSU}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al eliminar Perfil Usuario");
        } else {
            return true;
        }

        return false;
    }

}
