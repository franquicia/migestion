package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.MovilInfoDTO;
import com.gruposalinas.migestion.domain.MovilinfReporteDTO;
import com.gruposalinas.migestion.domain.ReporteMensualMovilDTO;
import java.util.List;

public interface MovilInfReporteDAO {

    public List<MovilinfReporteDTO> getReporteMensual(String mes, String anio) throws Exception;

    public List<MovilInfoDTO> getDetalleUsuarios(int idUsuario, String mes, String anio) throws Exception;

    public List<ReporteMensualMovilDTO> getReporteMensualCeco(int pais, int negocio, int ceco, String mes, String anio) throws Exception;

    public List<CecoDTO> getCecosHijos(int pais, int negocio, String ceco) throws Exception;

    public List<ReporteMensualMovilDTO> getReporteSemanalCeco(int pais, int negocio, int ceco, String fechaInicio, String fechaFin) throws Exception;

}
