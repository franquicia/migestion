package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ParametroDTO;
import java.util.List;

public interface ParametroDAO {

    public List<ParametroDTO> obtieneParametro() throws Exception;

    public List<ParametroDTO> obtieneParametro(String idParametro) throws Exception;

    public boolean insertaParametro(ParametroDTO bean) throws Exception;

    public boolean actualizaParametro(ParametroDTO bean) throws Exception;

    public boolean eliminaParametros(String clave) throws Exception;
}
