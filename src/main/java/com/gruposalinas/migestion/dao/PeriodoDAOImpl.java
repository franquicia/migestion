package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PeriodoDTO;
import com.gruposalinas.migestion.mappers.PeriodoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PeriodoDAOImpl extends DefaultDAO implements PeriodoDAO {

    private static Logger logger = LogManager.getLogger(PeriodoDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaPeriodo;
    private DefaultJdbcCall jdbcActualizaPeriodo;
    private DefaultJdbcCall jdbcEliminaPeriodo;
    private DefaultJdbcCall jdbcBuscaPeriodo;

    private List<PeriodoDTO> listaPeriodo;

    public void init() {

        jdbcBuscaPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_SEL_PERIODO")
                .returningResultSet("RCL_PERIODO", new PeriodoRowMapper());

        jdbcInsertaPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_INS_PERIODO");

        jdbcActualizaPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_ACT_PERIODO");

        jdbcEliminaPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_DEL_PERIODO");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PeriodoDTO> obtienePeriodo(String idPeriodo, String descripcion) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPERIODO", idPeriodo)
                .addValue("PA_DESCRIPCION", descripcion);

        out = jdbcBuscaPeriodo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERIODO.SP_SEL_PERIODO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaPeriodo = (List<PeriodoDTO>) out.get("RCL_PERIODO");

        if (respuesta == 0) {
            logger.info("Algo paso al consular los periodos ");
        }

        return listaPeriodo;
    }

    @Override
    public boolean insertaPeriodo(String idPeriodo, String descripcion, int commit) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_DESCRIPCION", descripcion)
                .addValue("PA_IDPERIODO", idPeriodo)
                .addValue("PA_COMMIT", commit);

        out = jdbcInsertaPeriodo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERIODO.SP_INS_PERIODO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el periodo");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaPeriodo(String idPeriodo, String descripcion, int commit) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPERIODO", idPeriodo)
                .addValue("PA_DESCRIPCION", descripcion)
                .addValue("PA_COMMIT", commit);

        out = jdbcActualizaPeriodo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERIODO.SP_ACT_PERIODO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al actualizar el periodo ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPeriodo(String idPeriodo) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDINCIDENTE", idPeriodo);

        out = jdbcEliminaPeriodo.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERIODO.SP_DEL_PERIODO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar el periodo ");
        } else {
            return true;
        }

        return false;
    }

}
