package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.AgendaDTO;
import com.gruposalinas.migestion.mappers.AgendaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AgendaDAOImpl extends DefaultDAO implements AgendaDAO {

    private static Logger logger = LogManager.getLogger(AgendaDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaAgenda;
    private DefaultJdbcCall jdbcActualizaAgenda;
    private DefaultJdbcCall jdbcEliminaAgenda;
    private DefaultJdbcCall jdbcBuscaAgenda;
    private DefaultJdbcCall jdbcBuscaEventosAgenda;

    private List<AgendaDTO> listaAgenda;

    public void init() {

        jdbcBuscaAgenda = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMAGENDA")
                .withProcedureName("SP_SEL_AGENDA")
                .returningResultSet("RCL_AGENDA", new AgendaRowMapper());

        jdbcBuscaEventosAgenda = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMAGENDA")
                .withProcedureName("SP_AGENDA_EV")
                .returningResultSet("RCL_AGENDA", new AgendaRowMapper());

        jdbcInsertaAgenda = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMAGENDA")
                .withProcedureName("SP_INS_AGENDA");

        jdbcActualizaAgenda = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMAGENDA")
                .withProcedureName("SP_ACT_AGENDA");

        jdbcEliminaAgenda = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMAGENDA")
                .withProcedureName("SP_DEL_AGENDA");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AgendaDTO> obtieneNotas(String idAgenda, String idUsuario) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDAGENDA", idAgenda)
                .addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcBuscaAgenda.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMAGENDA.SP_SEL_AGENDA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaAgenda = (List<AgendaDTO>) out.get("RCL_AGENDA");

        if (respuesta == 0) {
            logger.info("Algo paso al consular los eventos ");
        }

        return listaAgenda;
    }

}
