package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TablasPasoDTO;
import com.gruposalinas.migestion.domain.TareaActDTO;
import com.gruposalinas.migestion.mappers.PasoCecoRowMapper;
import com.gruposalinas.migestion.mappers.PasoSucursalRowMapper;
import com.gruposalinas.migestion.mappers.PasoUsuarioRowMapper;
import com.gruposalinas.migestion.mappers.TareaPasoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TablasPasoImpl extends DefaultDAO implements TablasPasoDAO {

    Logger logger = LogManager.getLogger(TablasPasoImpl.class);

    private DefaultJdbcCall jdbcConsultaUsuariosPaso;
    private DefaultJdbcCall jdbcConsultaCecosPaso;
    private DefaultJdbcCall jdbcConsultaSucursalesPaso;
    private DefaultJdbcCall jdbcConsultaTareaPaso;

    public void init() {

        jdbcConsultaUsuariosPaso = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPASO")
                .withProcedureName("SP_SEL_USUARIOS")
                .returningResultSet("RCL_PUSUARIOS", new PasoUsuarioRowMapper());

        jdbcConsultaCecosPaso = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPASO")
                .withProcedureName("SP_SEL_CECOS")
                .returningResultSet("RCL_PCECOS", new PasoCecoRowMapper());

        jdbcConsultaSucursalesPaso = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPASO")
                .withProcedureName("SP_SEL_SUCURSAL")
                .returningResultSet("RCL_SUCURSAL", new PasoSucursalRowMapper());

        jdbcConsultaSucursalesPaso = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPASO")
                .withProcedureName("SP_SEL_TAREAS")
                .returningResultSet("RCL_TAREAS", new TareaPasoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TablasPasoDTO> obtieneCecos(String idCeco, String idCecoSup, String descripcion) throws Exception {
        List<TablasPasoDTO> listaCecos = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDCECOP", idCecoSup)
                .addValue("PA_DESCRIPCION", descripcion);

        out = jdbcConsultaCecosPaso.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnejec.intValue();

        listaCecos = (List<TablasPasoDTO>) out.get("RCL_PCECOS");

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al obtener los cecos de paso");
        }

        return listaCecos;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TablasPasoDTO> obtieneSucursales(String numSucursal) throws Exception {
        List<TablasPasoDTO> listaSucursales = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDNUMSUC", numSucursal);

        out = jdbcConsultaSucursalesPaso.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnejec.intValue();

        listaSucursales = (List<TablasPasoDTO>) out.get("RCL_SUCURSAL");

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al obtener los sucursales de paso");
        }

        return listaSucursales;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TablasPasoDTO> obtieneUsuarios(String idUsuario, String idPuesto, String idCeco, String idCecoSup)
            throws Exception {
        List<TablasPasoDTO> listaCecos = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDCECO_P", idCecoSup)
                .addValue("PA_PUESTO", idPuesto)
                .addValue("PA_USUARIO", idUsuario);

        out = jdbcConsultaUsuariosPaso.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnejec.intValue();

        listaCecos = (List<TablasPasoDTO>) out.get("RCL_PUSUARIOS");

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al obtener los usuarios de paso");
        }

        return listaCecos;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TareaActDTO> obtieneTareas(String idTarea, String idUsuario, String estatus, String statusVac)
            throws Exception {
        List<TareaActDTO> listaTarea = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTAREA", idTarea)
                .addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_ESTATUS", estatus)
                .addValue("PA_ESTATUS_VAC", statusVac);

        out = jdbcConsultaTareaPaso.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnejec.intValue();

        listaTarea = (List<TareaActDTO>) out.get("RCL_TAREAS");

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al obtener las tareas de paso");
        }

        return listaTarea;
    }

}
