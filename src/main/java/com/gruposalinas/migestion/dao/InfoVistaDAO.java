package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.InfoVistaDTO;
import java.util.List;

public interface InfoVistaDAO {

    public List<InfoVistaDTO> buscaInfoVista(int idInfoVista) throws Exception;

    public List<InfoVistaDTO> buscaInfoVistaParam(String idInfoVista, String idCatVista, String idUsuario, String plataforma, String fecha) throws Exception;

    public boolean insertaInfoVista(InfoVistaDTO bean) throws Exception;

    public boolean actualizaInfoVista(InfoVistaDTO bean) throws Exception;

    public boolean eliminaInfoVista(int idInfoVista) throws Exception;

}
