package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.FotosAntesDespuesDTO;
import java.util.List;

public interface FotosAntesDespuesDAO {


	public int actualizaFotoAntesDesp(FotosAntesDespuesDTO bean)throws Exception;

	public int eliminaFotoAntesDesp(int idEvento)throws Exception;


	public List<FotosAntesDespuesDTO>consultaFotoAntesDespUlt()throws Exception;

}
