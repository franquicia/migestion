package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import java.util.List;

public interface NotificacionesDAO {

    public List<UsuarioInfoTokenDTO> consultaTokens(String ceco, String puesto) throws Exception;

}
