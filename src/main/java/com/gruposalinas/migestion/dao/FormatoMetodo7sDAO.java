package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.FormatoMetodo7sDTO;
import java.util.List;

public interface FormatoMetodo7sDAO {
	

	public boolean elimina(int idtab, String idusuario)throws Exception;


	
	public List<FormatoMetodo7sDTO> obtieneInfo() throws Exception; 
	
	public boolean actualiza(FormatoMetodo7sDTO bean)throws Exception;
	
	
}
