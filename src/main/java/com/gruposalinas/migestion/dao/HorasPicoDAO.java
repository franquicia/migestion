/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.HorasPicoDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public interface HorasPicoDAO {

    public List<HorasPicoDTO> getHorasPico(String fiSucursal, String fiAnio, String fiSemana) throws Exception;

    public List<HorasPicoDTO> getHorasPicoPaso(String fiSucursal, String fiSemana) throws Exception;

    public boolean setDistribucionHorasPico() throws Exception;
}
