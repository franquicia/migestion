package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.GeografiaDTO;
import java.util.List;

public interface GeografiaDAO {

    public List<GeografiaDTO> obtieneGeografia(String idCeco, String idRegion, String idZona, String idTerritorio) throws Exception;

    public boolean insertaGeografia(GeografiaDTO bean) throws Exception;

    public boolean actualizaGeografia(GeografiaDTO bean) throws Exception;

    public boolean eliminaGeografia(String idCeco) throws Exception;

}
