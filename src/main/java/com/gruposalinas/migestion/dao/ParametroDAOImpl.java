package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.mappers.ParametroRowMapper;
import com.gruposalinas.migestion.util.UtilString;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ParametroDAOImpl extends DefaultDAO implements ParametroDAO {

    private static Logger logger = LogManager.getLogger(ParametroDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneParametro;
    DefaultJdbcCall jdbcInsertaParametro;
    DefaultJdbcCall jdbcActualizaParametro;
    DefaultJdbcCall jdbcEliminaParametro;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_SEL_G_PARAM")
                .returningResultSet("RCL_PARAM", new ParametroRowMapper());

        jdbcObtieneParametro = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_PARAM", new ParametroRowMapper());

        jdbcInsertaParametro = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_INS_PARAM");

        jdbcActualizaParametro = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_ACT_PARAM");

        jdbcEliminaParametro = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_DEL_PARAM");
    }

    @SuppressWarnings("unchecked")
    public List<ParametroDTO> obtieneParametro() throws Exception {
        Map<String, Object> out = null;
        List<ParametroDTO> listaParametro = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_SEL_G_PARAM}");

        listaParametro = (List<ParametroDTO>) out.get("RCL_PARAM");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Parametros");
        } else {
            return listaParametro;
        }

        return listaParametro;

    }

    @SuppressWarnings("unchecked")
    public List<ParametroDTO> obtieneParametro(String clave) throws Exception {
        Map<String, Object> out = null;
        List<ParametroDTO> listaParametro = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", clave);

        out = jdbcObtieneParametro.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_SEL_PARAM}");

        listaParametro = (List<ParametroDTO>) out.get("RCL_PARAM");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el Parametro con la clave (" + clave + ")");
        } else {
            return listaParametro;
        }

        return null;
    }

    public boolean insertaParametro(ParametroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", bean.getClave())
                .addValue("PA_VAL_PARAM", bean.getValor())
                .addValue("PA_ACTIVO_PARAM", bean.getActivo());

        out = jdbcInsertaParametro.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_INS_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Parametro");
        } else {
            return true;
        }

        return false;

    }

    public boolean actualizaParametro(ParametroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", bean.getClave())
                .addValue("PA_VAL_PARAM", bean.getValor())
                .addValue("PA_ACTIVO_PARAM", bean.getActivo());

        out = jdbcActualizaParametro.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_ACT_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            String clave = UtilString.cleanParameter(bean.getClave())
                    .replace('\n', '_').replace('\r', '_');
            logger.info("Algo ocurrió al actualizar el Parametro clave( " + clave + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaParametros(String clave) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", clave);

        out = jdbcEliminaParametro.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_DEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            clave = UtilString.cleanParameter(clave)
                    .replace('\n', '_').replace('\r', '_');
            logger.info("Algo ocurrió al borrar el Parametro clave(" + clave + ")");
        } else {
            return true;
        }

        return false;

    }
}
