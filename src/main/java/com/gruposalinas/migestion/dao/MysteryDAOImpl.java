package com.gruposalinas.migestion.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.MysteryDTO;
import com.gruposalinas.migestion.mappers.MysteryRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;

public class MysteryDAOImpl extends DefaultDAO implements MysteryDAO {


	Logger logger = LogManager.getLogger(MysteryDAOImpl.class);

	private DefaultJdbcCall jdbcConsultaCecos;
	
	private DefaultJdbcCall jdbcConsultaNiveles;
	
	
	
	public void init(){
		
		jdbcConsultaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAREPORTESGESTION")
				.withProcedureName("SP_REP_MISTERY")
				.returningResultSet("RCL_CECOS", new MysteryRowMapper());
		
		jdbcConsultaNiveles = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAREPORTESGESTION")
				.withProcedureName("SP_REP_NIVELES")
				.returningResultSet("RCL_CECOS", new MysteryRowMapper());
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MysteryDTO> consultaCecos(String idCeco) throws Exception {
		List<MysteryDTO> listaPeriodo = null;
		Map<String, Object> out = null;
		int ejecucion = 0;
				
		SqlParameterSource in = new  MapSqlParameterSource().addValue("PA_CECO", idCeco);
		
		out = jdbcConsultaCecos.execute(in);
		
		BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
		ejecucion = returnejec.intValue();
		
		listaPeriodo = (List<MysteryDTO>) out.get("RCL_CECOS");
		
		if(ejecucion == 0){
			logger.info("Ocurrio un problema al obtener los Cecos");
		}		

		return listaPeriodo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MysteryDTO> consultaNivel(String idCeco) throws Exception {
		List<MysteryDTO> listaPeriodo = null;
		Map<String, Object> out = null;
		int ejecucion = 0;
				
		SqlParameterSource in = new  MapSqlParameterSource().addValue("PA_CECO", idCeco);
		
		out = jdbcConsultaNiveles.execute(in);
		
		BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
		ejecucion = returnejec.intValue();
		
		listaPeriodo = (List<MysteryDTO>) out.get("RCL_CECOS");
		
		if(ejecucion == 0){
			logger.info("Ocurrio un problema al obtener los Cecos");
		}		

		return listaPeriodo;
	}

}
