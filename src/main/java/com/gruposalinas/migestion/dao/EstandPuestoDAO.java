package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.EstandPuestoDTO;
import java.util.List;

public interface EstandPuestoDAO {
	


	public boolean elimina(int idPuesto)throws Exception;

	public List<EstandPuestoDTO> obtieneDatos(String ceco) throws Exception;

	
	public List<EstandPuestoDTO> obtieneInfo() throws Exception; 
	
	public boolean actualiza(EstandPuestoDTO bean)throws Exception;
	// mueble
	
	public int insertaMueble(EstandPuestoDTO bean)throws Exception;
	
	public List<EstandPuestoDTO> obtieneDatosMueble(String idPuesto) throws Exception; 
	
	public List<EstandPuestoDTO> obtieneDatosPlantillaMueble(String  ceco) throws Exception; 
	
	public List<EstandPuestoDTO> obtieneInfoMueble() throws Exception; 
	
	public boolean actualizaMueble(EstandPuestoDTO bean)throws Exception;
	
}
