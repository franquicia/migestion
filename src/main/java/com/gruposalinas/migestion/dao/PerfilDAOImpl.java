package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PerfilDTO;
import com.gruposalinas.migestion.mappers.PerfilRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PerfilDAOImpl extends DefaultDAO implements PerfilDAO {

    private static Logger logger = LogManager.getLogger(PerfilDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtienePerfil;
    DefaultJdbcCall jdbcInsertaPerfil;
    DefaultJdbcCall jdbcInsertaPerfilN;
    DefaultJdbcCall jdbcActualizaPerfil;
    DefaultJdbcCall jdbcEliminaPerfil;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFIL")
                .withProcedureName("SP_SEL_G_PERFIL")
                .returningResultSet("RCL_PERFIL", new PerfilRowMapper());

        jdbcObtienePerfil = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFIL")
                .withProcedureName("SP_SEL_PERFIL")
                .returningResultSet("RCL_PERFIL", new PerfilRowMapper());

        jdbcInsertaPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFIL")
                .withProcedureName("SP_INS_PERFIL");

        jdbcInsertaPerfilN = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFIL")
                .withProcedureName("SP_INS_N_PERFIL");

        jdbcActualizaPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFIL")
                .withProcedureName("SP_ACT_PERFIL");

        jdbcEliminaPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFIL")
                .withProcedureName("SP_DEL_PERFIL");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PerfilDTO> obtienePerfil() throws Exception {
        Map<String, Object> out = null;
        List<PerfilDTO> listaPerfil = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFIL.SP_SEL_G_PERFIL}");

        listaPerfil = (List<PerfilDTO>) out.get("RCL_PERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Perfiles");
        } else {
            return listaPerfil;
        }

        return listaPerfil;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PerfilDTO> obtienePerfil(int idPerfil) throws Exception {
        Map<String, Object> out = null;
        List<PerfilDTO> listaPerfil = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_PERFIL", idPerfil);

        out = jdbcObtienePerfil.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFIL.SP_SEL_PERFIL}");

        listaPerfil = (List<PerfilDTO>) out.get("RCL_PERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Perfil con id(" + idPerfil + ")");
        } else {
            return listaPerfil;
        }

        return null;
    }

    @Override
    public int insertaPerfil(PerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idPerfil = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcInsertaPerfil.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFIL.SP_INS_PERFIL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        BigDecimal idPerfilReturn = (BigDecimal) out.get("PA_IDPERFIL");
        idPerfil = idPerfilReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Perfil");
        } else {
            return idPerfil;
        }

        return idPerfil;
    }

    @Override
    public boolean actualizaPerfil(PerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDPERFIL", bean.getIdPerfil())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcActualizaPerfil.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFIL.SP_ACT_PERFIL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar el Perfil  id( " + bean.getIdPerfil() + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPerfil(int idPerfil) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDPERFIL", idPerfil);

        out = jdbcEliminaPerfil.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFIL.SP_DEL_PERFIL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Perfil id(" + idPerfil + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public int insertaPerfilN(PerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idPerfil = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPERFIL", bean.getIdPerfil())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcInsertaPerfilN.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFIL.SP_INS_PERFIL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        idPerfil = 0;

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Perfil");
        } else {
            return idPerfil;
        }

        return idPerfil;
    }

}
