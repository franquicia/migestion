package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.FotoPlantillaDTO;
import com.gruposalinas.migestion.mappers.FotoPlantillaRowMapper;
import com.gruposalinas.migestion.mappers.FotoPlantillaUsuRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FotoPlantillaDAOImpl extends DefaultDAO implements FotoPlantillaDAO {

	private static Logger logger = LogManager.getLogger(FotoPlantillaDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaFotoPlanti;
	private DefaultJdbcCall jdbcBuscaInfoFotoPlanti;
	private DefaultJdbcCall jdbcActualizaFotoPlanti;

    private List<FotoPlantillaDTO> listaDetU;

    public void init() {

		
		jdbcEliminaFotoPlanti = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMFOTOPLAN")
				.withProcedureName("SP_DEL_FOTOPLAN");

		jdbcBuscaInfoFotoPlanti = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMFOTOPLAN")
				.withProcedureName("SP_SEL_FOTOPLAN_U")
				.returningResultSet("RCL_FOTOPLAN", new FotoPlantillaRowMapper());



		jdbcActualizaFotoPlanti = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMFOTOPLAN")
				.withProcedureName("SP_ACT_FOTOPLAN");


	}

	
	@Override
	public boolean elimina(String ceco) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_FIID_CECO",ceco);
		
		out = jdbcEliminaFotoPlanti.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMFOTOPLAN.SP_DEL_FOTOPLAN}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}

	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<FotoPlantillaDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<FotoPlantillaDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaInfoFotoPlanti.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMFOTOPLAN.SP_SEL_FOTOPLAN_U}");
        listaDetU = (List<FotoPlantillaDTO>) out.get("RCL_FOTOPLAN");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error == 1) {
            logger.info("Algo paso al obtener las FotoPlanti");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(FotoPlantillaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", bean.getCeco())
                .addValue("PA_FIID_USUARIO", bean.getUsuario())
                .addValue("PA_FCRUTAFOTO", bean.getRuta());

        out = jdbcActualizaFotoPlanti.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMFOTOPLAN.SP_ACT_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
