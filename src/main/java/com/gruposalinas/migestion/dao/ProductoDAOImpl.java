package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TipificacionDTO;
import com.gruposalinas.migestion.mappers.ProductoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ProductoDAOImpl extends DefaultDAO implements ProductoDAO {

    private Logger logger = LogManager.getLogger(ProductoDAOImpl.class);

    private DefaultJdbcCall jdbcConsulta;
    private DefaultJdbcCall jdbcConsultaPadres;
    private DefaultJdbcCall jdbcInserta;
    private DefaultJdbcCall jdbcActualiza;
    private DefaultJdbcCall jdbcElimina;
    private DefaultJdbcCall jdbcgetHijos;

    public void init() {

        jdbcConsulta = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCT")
                .withProcedureName("SP_CONSULTA")
                .returningResultSet("RCL_CONSULTA", new ProductoRowMapper());

        jdbcConsultaPadres = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCT")
                .withProcedureName("SP_CONSULTA_P")
                .returningResultSet("RCL_CONSULTA", new ProductoRowMapper());

        jdbcInserta = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCT")
                .withProcedureName("SP_INSERTA");

        jdbcActualiza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCT")
                .withProcedureName("SP_ACTUALIZA");

        jdbcElimina = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCT")
                .withProcedureName("SP_ELIMINA");

        jdbcgetHijos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPRODUCT")
                .withProcedureName("SP_GETHIJO")
                .returningResultSet("RCL_CONSULTA", new ProductoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TipificacionDTO> consulta() throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        List<TipificacionDTO> lista = null;

        out = jdbcConsulta.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCT.SP_CONSULTA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            lista = new ArrayList<TipificacionDTO>();
            logger.info("Ocurrio un problema al obtener los Productos");
        } else {
            lista = (List<TipificacionDTO>) out.get("RCL_CONSULTA");
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TipificacionDTO> consultaPadres() throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        List<TipificacionDTO> lista = null;

        out = jdbcConsultaPadres.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCT.SP_CONSULTA_P}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            lista = new ArrayList<TipificacionDTO>();
            logger.info("Ocurrio un problema al obtener los Productos");
        } else {
            lista = (List<TipificacionDTO>) out.get("RCL_CONSULTA");
        }

        return lista;
    }

    @Override
    public int inserta(TipificacionDTO bean) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        int idCreado = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PRODUCTO", bean.getDesc())
                .addValue("PA_PADRE", bean.getPadre());

        out = jdbcInserta.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCT.SP_INSERTA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al inserta el Producto");
        } else {
            BigDecimal idReturn = (BigDecimal) out.get("PA_IDCREADO");
            idCreado = idReturn.intValue();
        }

        return idCreado;
    }

    @Override
    public boolean actualiza(TipificacionDTO bean) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        boolean respuesta = false;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPRODUCTO", bean.getIdOpeProd())
                .addValue("PA_PRODUCTO", bean.getDesc())
                .addValue("PA_PADRE", bean.getPadre());

        out = jdbcActualiza.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCT.SP_ACTUALIZA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al actualizar el Producto");
        } else {
            respuesta = true;
        }

        return respuesta;
    }

    @Override
    public boolean elimina(int idProducto) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        boolean respuesta = false;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPRODUCTO", idProducto);

        out = jdbcElimina.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCT.SP_ELIMINA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Ocurrio un problema al elminar el Producto");
        } else {
            respuesta = true;
        }

        return respuesta;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TipificacionDTO> getHijos(int idPadre) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        List<TipificacionDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPADRE", idPadre);

        out = jdbcgetHijos.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPRODUCT.SP_GETHIJO}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            lista = new ArrayList<TipificacionDTO>();
            logger.info("Ocurrio un problema al obtener los Productos");
        } else {
            lista = (List<TipificacionDTO>) out.get("RCL_CONSULTA");
        }

        return lista;
    }

}
