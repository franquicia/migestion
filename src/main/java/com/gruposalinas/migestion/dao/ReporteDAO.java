package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ReporteDTO;
import java.util.List;

public interface ReporteDAO {

    public List<ReporteDTO> obtieneReporte(String idReporte, String nombre) throws Exception;

    public boolean insertaReporte(int idReporte, String nombre, int commit) throws Exception;

    public boolean actualizaReporte(int idReporte, String nombre, int commit) throws Exception;

    public boolean eliminaReporte(String idReporte) throws Exception;
}
