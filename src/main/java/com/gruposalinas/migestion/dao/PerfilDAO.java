package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PerfilDTO;
import java.util.List;

public interface PerfilDAO {

    public List<PerfilDTO> obtienePerfil() throws Exception;

    public List<PerfilDTO> obtienePerfil(int idPerfil) throws Exception;

    public int insertaPerfil(PerfilDTO bean) throws Exception;

    public int insertaPerfilN(PerfilDTO bean) throws Exception;

    public boolean actualizaPerfil(PerfilDTO bean) throws Exception;

    public boolean eliminaPerfil(int idPerfil) throws Exception;

}
