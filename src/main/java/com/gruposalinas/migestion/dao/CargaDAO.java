package com.gruposalinas.migestion.dao;

public interface CargaDAO {

    public boolean cargaUsuariosPaso() throws Exception;

    public boolean cargaCecosPaso() throws Exception;

    public boolean cargaSucursalesPaso() throws Exception;

    public boolean cargaPuestos() throws Exception;

    public boolean cargaUsuarios() throws Exception;

    public boolean cargaCecos() throws Exception;

    public boolean cargaSucursales() throws Exception;

    public boolean cargaGeografia() throws Exception;

    public boolean cargaTareas() throws Exception;

    public boolean cargaLimpiaCecos() throws Exception;

    public boolean cargaUsuariosFRQ() throws Exception;

    public boolean cargaUsuariosExternos() throws Exception;

    public boolean cargaPerfilesMantenimiento() throws Exception;

}
