package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.DepuraActFijoDTO;
import java.util.List;

public interface DepuraActFijoDAO {
	

	public boolean elimina(int idDepAct)throws Exception;

	
	public List<DepuraActFijoDTO> obtieneInfo() throws Exception; 
	
	public boolean actualiza(DepuraActFijoDTO bean)throws Exception;
	//Evidencia
	public int insertaEvi(DepuraActFijoDTO bean)throws Exception;
	
	public List<DepuraActFijoDTO> obtieneDatosEvi(String ceco) throws Exception; 
	
	public List<DepuraActFijoDTO> obtieneInfoEvi() throws Exception; 
	
	public boolean actualizaEvi(DepuraActFijoDTO bean)throws Exception;
	
	
}
