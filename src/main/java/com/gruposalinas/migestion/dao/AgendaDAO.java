package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.AgendaDTO;
import java.util.List;

public interface AgendaDAO {
	
	public List<AgendaDTO> obtieneNotas(String idAgenda, String idUsuario) throws Exception;
}
