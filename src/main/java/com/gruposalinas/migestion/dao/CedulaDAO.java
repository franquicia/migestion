package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CedulaDTO;
import java.util.List;

public interface CedulaDAO {
	

	public boolean elimina(String idUsuario)throws Exception;


	
	public List<CedulaDTO> obtieneInfo() throws Exception; 
	
	public boolean actualiza(CedulaDTO bean)throws Exception;
	
	
}
