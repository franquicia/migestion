package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.IncidentesDTO;
import com.gruposalinas.migestion.mappers.IncidenteRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class IncidentesDAOImpl extends DefaultDAO implements IncidentesDAO {

    private static Logger logger = LogManager.getLogger(IncidentesDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaIncidente;
    private DefaultJdbcCall jdbcActualizaIncidente;
    private DefaultJdbcCall jdbcEliminaIncidente;
    private DefaultJdbcCall jdbcBuscaIncidente;

    private List<IncidentesDTO> listaIncidente;

    public void init() {

        jdbcBuscaIncidente = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENTES")
                .withProcedureName("SP_SEL_INCIDENTE")
                .returningResultSet("RCL_INCIDENTE", new IncidenteRowMapper());

        jdbcInsertaIncidente = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENTES")
                .withProcedureName("SP_INS_INCIDENTE");

        jdbcActualizaIncidente = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENTES")
                .withProcedureName("SP_ACT_INCIDENTE");

        jdbcEliminaIncidente = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENTES")
                .withProcedureName("SP_DEL_INCIDENTE");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<IncidentesDTO> obtieneIncidentes(String idIncidente, String idUsuario) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDINCIDENTE", idIncidente)
                .addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcBuscaIncidente.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENTES.SP_SEL_INCIDENTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaIncidente = (List<IncidentesDTO>) out.get("RCL_INCIDENTE");

        if (respuesta == 0) {
            logger.info("Algo paso al consular los incidentes ");
        }

        return listaIncidente;
    }

    @Override
    public boolean insertaIncidentes(String idIncidente, int idUsuario, int commit) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDINCIDENTE", idIncidente)
                .addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_COMMIT", commit);

        out = jdbcInsertaIncidente.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENTES.SP_INS_INCIDENTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el incidente");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaIncidente(String idIncidente, int idUsuario, int commit) throws Exception {

        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDINCIDENTE", idIncidente)
                .addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_COMMIT", commit);

        out = jdbcActualizaIncidente.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENTES.SP_ACT_INCIDENTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al actualizar el incidente ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaIncidente(String idIncidente) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDINCIDENTE", idIncidente);

        out = jdbcEliminaIncidente.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENTES.SP_DEL_INCIDENTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar el Incidente ");
        } else {
            return true;
        }

        return false;
    }

}
