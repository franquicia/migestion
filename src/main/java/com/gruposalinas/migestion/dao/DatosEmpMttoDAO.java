package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import java.util.List;

public interface DatosEmpMttoDAO {

    //con param
    public List<DatosEmpMttoDTO> obtieneDatos(int idUsuario) throws Exception;
    //sin param

    public List<DatosEmpMttoDTO> obtieneInfo() throws Exception;

}
