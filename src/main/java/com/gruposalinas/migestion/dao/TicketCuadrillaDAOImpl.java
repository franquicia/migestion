package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.mappers.TicketCuadrillaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TicketCuadrillaDAOImpl extends DefaultDAO implements TicketCuadrillaDAO {

    private static Logger logger = LogManager.getLogger(TicketCuadrillaDAOImpl.class);

    private DefaultJdbcCall jdbcInsertatkCuadrilla;
    private DefaultJdbcCall jdbcEliminatkCuadrilla;
    private DefaultJdbcCall jdbcBuscaFila;
    private DefaultJdbcCall jdbcBuscaInfotkCuadrilla;
    private DefaultJdbcCall jdbcBuscaCuadrillaTK;
    private DefaultJdbcCall jdbcActualizatkCuadrilla;

    private List<TicketCuadrillaDTO> listaDetU;

    public void init() {

        jdbcInsertatkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_INS_TK");

        jdbcEliminatkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_DEL_TK");

        jdbcBuscaInfotkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_TK", new TicketCuadrillaRowMapper());

        jdbcBuscaFila = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_TK", new TicketCuadrillaRowMapper());

        jdbcBuscaCuadrillaTK = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_SEL_CUADDET")
                .returningResultSet("RCL_TK", new TicketCuadrillaRowMapper());

        jdbcActualizatkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_ACT_TK");

    }

    @Override
    public int inserta(TicketCuadrillaDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRI", bean.getIdCuadrilla())
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_TICKET", bean.getTicket())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDER", bean.getLiderCua())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcInsertatkCuadrilla.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_INS_TK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_TKCUA");
        idNota = idbloc.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    @Override
    public boolean elimina(String ticket) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TICKET", ticket);

        out = jdbcEliminatkCuadrilla.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_DEL_TK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<TicketCuadrillaDTO> obtieneDatos(int idCuadrilla) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", idCuadrilla);

        out = jdbcBuscaFila.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<TicketCuadrillaDTO>) out.get("RCL_TK");

        if (respuesta != 1) {
            logger.info("Algo paso al consular la cuadrilla ");
        }

        return listaDetU;
    }

    //busca tk cuadrillas
    @SuppressWarnings("unchecked")
    @Override
    public List<TicketCuadrillaDTO> obtieneCuadrillaTK(int idCuadrilla, int idProveedor, int zona) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", idCuadrilla)
                .addValue("PA_PROVEEDOR", idProveedor)
                .addValue("PA_ZONA", zona);

        out = jdbcBuscaCuadrillaTK.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_SEL_CUADDET}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<TicketCuadrillaDTO>) out.get("RCL_TK");

        if (respuesta != 1) {
            logger.info("Algo paso al consular los tickets de la cuadrilla ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<TicketCuadrillaDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        out = jdbcBuscaInfotkCuadrilla.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_SEL_DETALLE}");
        listaDetU = (List<TicketCuadrillaDTO>) out.get("RCL_TK");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener los tk de la Cuadrilla");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(TicketCuadrillaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRI", bean.getIdCuadrilla())
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_TICKET", bean.getTicket())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDER", bean.getLiderCua())
                .addValue("PA_STATUS", bean.getStatus());
        out = jdbcActualizatkCuadrilla.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_ACT_TK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
