package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.BitacoraMntoDTO;
import java.util.List;

public interface BitacoraMantenimientoDAO {


	public int actualizaBitacoraMnto(BitacoraMntoDTO bean)throws Exception;

	public int eliminaBitacoraMnto(int idBitacora)throws Exception;

	
	public List<BitacoraMntoDTO>consultaBitacoraMntoIds(int idAmbito)throws Exception;
}
