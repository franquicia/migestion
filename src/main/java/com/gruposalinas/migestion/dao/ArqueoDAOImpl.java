package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ArqueoDTO;
import com.gruposalinas.migestion.mappers.ArqueoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ArqueoDAOImpl extends DefaultDAO implements ArqueoDAO {

    Logger logger = LogManager.getLogger(ArqueoDAOImpl.class);

    private DefaultJdbcCall jdbcConsultaCecos;

    public void init() {

        jdbcConsultaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPORTESGESTION")
                .withProcedureName("SP_REP_ARQUEO")
                .returningResultSet("RCL_DATOS", new ArqueoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ArqueoDTO> consultaCeco(String ceco) throws Exception {
        List<ArqueoDTO> listaCecos = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NUMS", ceco);

        out = jdbcConsultaCecos.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnejec.intValue();

        listaCecos = (List<ArqueoDTO>) out.get("RCL_DATOS");

       
		if(ejecucion == 0){
			logger.info("Ocurrio un problema al obtener el nombre del ceco");
		}		

		return listaCecos;
	}


}
