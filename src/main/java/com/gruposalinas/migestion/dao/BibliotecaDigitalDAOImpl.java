package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.BibliotecaDigitalDTO;
import com.gruposalinas.migestion.domain.EnvioCorreoDTO;
import com.gruposalinas.migestion.domain.QuejaDTO;
import com.gruposalinas.migestion.mappers.BibliotecaDigitalRowMapper;

public class BibliotecaDigitalDAOImpl extends DefaultDAO implements BibliotecaDigitalDAO{

	private Logger logger = LogManager.getLogger(BibliotecaDigitalDAOImpl.class);
	
	private DefaultJdbcCall jdbcEnviaCorreo;
	
	
	public void init() {
	
		
		
		jdbcEnviaCorreo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMDOCENVIA")
				.withProcedureName("SP_INSERTA");
		
	}
	

	@Override
	public boolean registraEnvio(EnvioCorreoDTO correoDTO) throws Exception {
		
		Map<String, Object> out = null;
		
		int ejecucion = 0 ;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDARCHIVO", correoDTO.getIdArchvio())
				.addValue("PA_EMAIL", correoDTO.getEmail())
				.addValue("PA_SUCURSAL", correoDTO.getSucursal())
				.addValue("PA_FECHA_SOLICITADO", correoDTO.getFechaSolicitada());
		
		out = jdbcEnviaCorreo.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMDOCENVIA.SP_INSERTA}");
		
		BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
		ejecucion = ejecucionReturn.intValue();
		
		if(ejecucion == 1) 
			return true;
		
		return false;
	}

}
