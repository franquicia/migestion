package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ExternoDTO;
import java.util.List;

public interface UsuarioExternoDAO {

    public List<ExternoDTO> obtieneUsuarioExterno() throws Exception;

    public List<ExternoDTO> obtieneUsuarioExterno(int numEmpleado) throws Exception;

    public boolean insertaUsuarioExterno(ExternoDTO bean) throws Exception;

    public boolean actualizaUsuarioExterno(ExternoDTO bean) throws Exception;

    public boolean eliminaUsuarioExterno(int numEmpleado) throws Exception;

    public boolean depuraUsuarioExterno() throws Exception;

    public boolean actualizaPuesto(int numEmpleado, int puesto) throws Exception;

}
