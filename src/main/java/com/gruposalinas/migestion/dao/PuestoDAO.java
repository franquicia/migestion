package com.gruposalinas.migestion.dao;

import java.util.List;

import com.gruposalinas.migestion.domain.PuestoDTO;

public interface PuestoDAO {

    public int inserta(PuestoDTO bean) throws Exception;

    public boolean elimina(String idPuesto) throws Exception;

    public List<PuestoDTO> obtieneDatos(int idPuesto, String descripcion) throws Exception;

    public List<PuestoDTO> obtieneInfo() throws Exception;

    public boolean actualiza(PuestoDTO bean) throws Exception;

}
