package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.mappers.IncidenciasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class IncidenciasDAOImpl extends DefaultDAO implements IncidenciasDAO {

    private static Logger logger = LogManager.getLogger(IncidenciasDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaIncidencia;
    private DefaultJdbcCall jdbcInsertaIncidenciaLimpieza;
    private DefaultJdbcCall jdbcEliminaIncidencia;
    private DefaultJdbcCall jdbcBuscaIncidencia;
    private DefaultJdbcCall jdbcBuscaIncidencias;
    private DefaultJdbcCall jdbcBuscaIncidenciasLimpieza;
    private DefaultJdbcCall jdbcActualizaIncidencia;
    private DefaultJdbcCall jdbcDepuraIncidencia;

    private List<IncidenciasDTO> listaDetU;

    public void init() {

        jdbcInsertaIncidencia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_INS_INCID");

        jdbcInsertaIncidenciaLimpieza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_INS_INCID_LIMPIEZA");

        jdbcEliminaIncidencia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_DEL_INCID");

        jdbcBuscaIncidencias = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_SERVICIOS", new IncidenciasRowMapper());

        jdbcBuscaIncidenciasLimpieza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_SEL_DETALLE_LIMPIEZA")
                .returningResultSet("RCL_SERVICIOS", new IncidenciasRowMapper());

        jdbcBuscaIncidencia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_SERVICIOS", new IncidenciasRowMapper());

        jdbcActualizaIncidencia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_ACT_INCID");

        jdbcDepuraIncidencia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMINCIDENCIA")
                .withProcedureName("SP_DEPURA_INCID");

    }

    @Override
    public int inserta(IncidenciasDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDSERV", bean.getIdTipo())
                .addValue("PA_SERVICIO", bean.getServicio())
                .addValue("PA_INCIDENCIA", bean.getIncidencia())
                .addValue("PA_UBICACION", bean.getUbicacion())
                .addValue("PA_PLANTILLA", bean.getPlantilla())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcInsertaIncidencia.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_INS_INCID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return 1;
        }

        return idNota;
    }

    @Override
    public int insertaLimpieza(IncidenciasDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDSERV", bean.getIdTipo())
                .addValue("PA_SERVICIO", bean.getServicio())
                .addValue("PA_INCIDENCIA", bean.getIncidencia())
                .addValue("PA_UBICACION", bean.getUbicacion())
                .addValue("PA_PLANTILLA", bean.getPlantilla())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcInsertaIncidenciaLimpieza.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_INS_INCID_LIMPIEZA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return 1;
        }

        return idNota;
    }

    @Override
    public boolean elimina(String idTipo) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPO", idTipo);

        out = jdbcEliminaIncidencia.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_DEL_INCID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<IncidenciasDTO> obtieneDatos(int idTipo) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPO", idTipo);

        out = jdbcBuscaIncidencia.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<IncidenciasDTO>) out.get("RCL_SERVICIOS");

        if (respuesta != 1) {
            logger.info("Algo paso al consular la incidencia ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<IncidenciasDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<IncidenciasDTO> lista = null;
        int error = 0;

        out = jdbcBuscaIncidencias.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_SEL_DETALLE}");
        listaDetU = (List<IncidenciasDTO>) out.get("RCL_SERVICIOS");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las incidencias");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    //Sin ningun parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<IncidenciasDTO> obtieneInfoLimpieza() throws Exception {
        Map<String, Object> out = null;
        List<IncidenciasDTO> lista = null;
        int error = 0;

        out = jdbcBuscaIncidenciasLimpieza.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_SEL_DETALLE_LIMPIEZA}");
        listaDetU = (List<IncidenciasDTO>) out.get("RCL_SERVICIOS");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las incidencias");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(IncidenciasDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPO", bean.getIdTipo())
                .addValue("PA_SERVICIO", bean.getServicio())
                .addValue("PA_INCIDENCIA", bean.getIncidencia())
                .addValue("PA_UBICACION", bean.getUbicacion())
                .addValue("PA_PLANTILLA", bean.getPlantilla())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcActualizaIncidencia.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_ACT_INCID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean depura() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcDepuraIncidencia.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMINCIDENCIA.SP_DEPURA_INCID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al depurar la tabla Incidencias");
        } else {
            return true;
        }

        return false;
    }

}
