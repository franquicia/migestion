package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.CatalogoVistaDTO;
import java.util.List;

public interface CatalogoVistaDAO {

    public List<CatalogoVistaDTO> buscaCatalogoVista(int idCatVista) throws Exception;

    public List<CatalogoVistaDTO> buscaCatalogoVistaParam(String idCatVista, String descripcion, String moduloPadre) throws Exception;

    public boolean insertaCatalogoVista(CatalogoVistaDTO bean) throws Exception;

    public boolean actualizaCatalogoVista(CatalogoVistaDTO bean) throws Exception;

    public boolean eliminaCatalogoVista(int idCatVista) throws Exception;

}
