package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.LoginDTO;
import com.gruposalinas.migestion.mappers.LoginRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class LoginDAOImpl extends DefaultDAO implements LoginDAO {

    private static final Logger logger = LogManager.getLogger(LoginDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaUsuario;

    public void init() {
        jdbcBuscaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("migestion")
                .withCatalogName("PA_LOGIN")
                .withProcedureName("SP_BUSCA_USUARIO")
                .returningResultSet("PA_CUR_DATOS", new LoginRowMapper());

    }

    @SuppressWarnings("unchecked")
    public List<LoginDTO> buscaUsuario(int usuario) throws Exception {
        Map<String, Object> out = null;
        List<LoginDTO> datosUsuario = null;
        int respuesta = 0;

        try {

            SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", usuario);

            out = jdbcBuscaUsuario.execute(in);

            logger.info("Funci�n ejecutada:{migestion.PA_LOGIN.SP_BUSCA_USUARIO}");

            datosUsuario = (List<LoginDTO>) out.get("PA_CUR_DATOS");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            respuesta = respuestaEjec.intValue();

            if (respuesta == 1) {
                throw new Exception("Algo ocurrió en el SP");
            }

        } catch (Exception e) {
            logger.info(e.getMessage());

            //System.out.println("try de dao implemen" + e.getMessage());
            //throw new Exception();
        }

        return datosUsuario;
    }

    @Override
    public Map<String, Object> buscaUsuarioCheck(int noEmpleado) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }
}
