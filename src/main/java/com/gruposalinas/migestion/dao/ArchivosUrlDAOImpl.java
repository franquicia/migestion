package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ArchivosUrlDTO;
import com.gruposalinas.migestion.mappers.ArchivoUrlRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ArchivosUrlDAOImpl extends DefaultDAO implements ArchivosUrlDAO {

    private static Logger logger = LogManager.getLogger(ArchivosUrlDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaArchivosUrl;
    private DefaultJdbcCall jdbcEliminaArchivosUrl;
    private DefaultJdbcCall jdbcBuscaArchivoid;
    private DefaultJdbcCall jdbcBuscaInfoArchivosUrl;
    private DefaultJdbcCall jdbcActualizaArchivosUrl;

    private List<ArchivosUrlDTO> listaDetU;

    public void init() {

        jdbcInsertaArchivosUrl = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMARCURL")
                .withProcedureName("SP_INS_ARCH");

        jdbcEliminaArchivosUrl = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMARCURL")
                .withProcedureName("SP_DEL_ARC");

        jdbcBuscaInfoArchivosUrl = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMARCURL")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_ARCH", new ArchivoUrlRowMapper());

        jdbcBuscaArchivoid = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMARCURL")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_ARCH", new ArchivoUrlRowMapper());

        jdbcActualizaArchivosUrl = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMARCURL")
                .withProcedureName("SP_ACT_ARCH");

    }

    @Override
    public int inserta(ArchivosUrlDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_VERSION", bean.getVersion());

        out = jdbcInsertaArchivosUrl.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMARCURL.SP_INS_ARCH}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_IDARCH");
        idNota = idbloc.intValue();

        if (respuesta != 0) {
            logger.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    @Override
    public boolean elimina(String idArch) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_ARCH", idArch);

        out = jdbcEliminaArchivosUrl.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMARCURL.SP_DEL_ARC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("unchecked")
    @Override
    public List<ArchivosUrlDTO> obtieneDatos(int idArch) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_ARCH", idArch);

        out = jdbcBuscaArchivoid.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMARCURL.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<ArchivosUrlDTO>) out.get("RCL_ARCH");

        if (respuesta == 1) {
            logger.info("Algo paso al consular la la tabla de Archivos URL ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("unchecked")
    @Override
    public List<ArchivosUrlDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<ArchivosUrlDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoArchivosUrl.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMARCURL.SP_SEL_DETALLE}");
        listaDetU = (List<ArchivosUrlDTO>) out.get("RCL_ARCH");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las ArchivosUrl");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(ArchivosUrlDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDARCH", bean.getIdArch())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_VERSION", bean.getVersion());

        out = jdbcActualizaArchivosUrl.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMARCURL.SP_ACT_ARCH}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
