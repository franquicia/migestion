package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TareaActDTO;
import java.util.List;
import java.util.Map;

public interface TareaActDAO {

	public List<TareaActDTO> obtieneTareaCom(String idTarea, String status, String estatusVac, String idUsuario, String tipoTarea) throws Exception; 
	
	public boolean insertaTareas (TareaActDTO bean)throws Exception;
	
	public boolean actualizaTareas(TareaActDTO bean)throws Exception;
	
	public boolean eliminaTareas(String idTarea)throws Exception;

}
