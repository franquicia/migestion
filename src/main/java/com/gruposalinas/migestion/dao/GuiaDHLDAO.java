package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.GuiaDHLDTO;
import java.util.List;

public interface GuiaDHLDAO {

	public boolean elimina(String idGuia)throws Exception;

	public boolean eliminaEvi(String idEvid)throws Exception;

	public List<GuiaDHLDTO> obtieneInfo() throws Exception; 
	
	public boolean actualiza(GuiaDHLDTO bean)throws Exception;
	
	public boolean actualizaEvid(GuiaDHLDTO bean)throws Exception;
	
	
}
