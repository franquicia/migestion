package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.FilasDTO;
import com.gruposalinas.migestion.domain.FormatoMetodo7sDTO;
import com.gruposalinas.migestion.mappers.FormatoMetodo7sPlantillaRowMapper;
import com.gruposalinas.migestion.mappers.FormatoMetodo7sRowMapper;

public class FormatoMetodo7sDAOImpl extends DefaultDAO implements FormatoMetodo7sDAO {

	private static Logger logger = LogManager.getLogger(FormatoMetodo7sDAOImpl.class);
	
	private DefaultJdbcCall jdbcElimina;
	private DefaultJdbcCall jdbcBuscaInfo;
	private DefaultJdbcCall jdbcActualiza;

    private List<FormatoMetodo7sDTO> listaDetU;
    private List<FormatoMetodo7sDTO> listaDetUPlantilla;

    public void init() {

		

		jdbcElimina = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMFORM7S")
				.withProcedureName("SP_DEL_FORM");

		jdbcBuscaInfo = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMFORM7S")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_FORMATO", new FormatoMetodo7sRowMapper());





		jdbcActualiza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMFORM7S")
				.withProcedureName("SP_ACT_FORM");


	}

	
	@Override
	public boolean elimina(int idtab,String idusuario) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDTABLA",idtab)
				.addValue("PA_IDUSUARIO",idusuario);
		
		out = jdbcElimina.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMFORM7S.SP_DEL_FORM}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}


	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<FormatoMetodo7sDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<FilasDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaInfo.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMFORM7S.SP_SEL_DETALLE}");
        listaDetU = (List<FormatoMetodo7sDTO>) out.get("RCL_FORMATO");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener el detalle ");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(FormatoMetodo7sDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdtab())
                .addValue("PA_IDUSUARIO", bean.getIdusuario())
                .addValue("PA_IDSUC", bean.getIdSucursal())
                .addValue("PA_PUESTO", bean.getPuesto())
                .addValue("PA_PREG1", bean.getPreg1())
                .addValue("PA_PREG2", bean.getPreg2())
                .addValue("PA_PREG3", bean.getPreg3());

        out = jdbcActualiza.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMFORM7S.SP_ACT_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
