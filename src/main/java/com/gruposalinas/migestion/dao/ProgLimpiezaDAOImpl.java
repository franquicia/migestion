package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;
import com.gruposalinas.migestion.mappers.ProgLimpiezaAreaRowMapper;
import com.gruposalinas.migestion.mappers.ProgLimpiezaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

;

public class ProgLimpiezaDAOImpl extends DefaultDAO implements ProgLimpiezaDAO {

	private static Logger logger = LogManager.getLogger(ProgLimpiezaDAOImpl.class);
	
	private DefaultJdbcCall jdbcEliminaProgLimp;

	private DefaultJdbcCall jdbcBuscaInfoProgLimp;
	private DefaultJdbcCall jdbcActualizaProgLimp;

	private DefaultJdbcCall jdbcInsertaProgLimpza;
	private DefaultJdbcCall jdbcBuscaInfoProgLimpza;
	private DefaultJdbcCall jdbcActualizaProgLimpza;

    private List<ProgLimpiezaDTO> listaDetU;

    public void init() {

		

		jdbcEliminaProgLimp = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMPROGLIMPZA")
				.withProcedureName("SP_DEL_PROG");

		jdbcBuscaInfoProgLimp = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMPROGLIMPZA")
				.withProcedureName("SP_SEL_DETALLE")
				.returningResultSet("RCL_PROGLZ", new ProgLimpiezaRowMapper());



		jdbcActualizaProgLimp = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMPROGLIMPZA")
				.withProcedureName("SP_ACT_PROG");

		// detalle
		jdbcInsertaProgLimpza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMPROGLIMPZA")
				.withProcedureName("SP_INS_PROGARE");



		jdbcBuscaInfoProgLimpza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMPROGLIMPZA")
				.withProcedureName("SP_SEL_DET")
				.returningResultSet("RCL_PROGLZ", new ProgLimpiezaAreaRowMapper());




		jdbcActualizaProgLimpza = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMPROGLIMPZA")
				.withProcedureName("SP_ACT_PROGAREA");


	}
	
	@Override
	public boolean elimina(int idProg) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;
		
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_FIID_TAB",idProg);
		
		out = jdbcEliminaProgLimp.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMPROGLIMPZA.SP_DEL_PROG}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		
		if(respuesta !=  1){
			logger.info("Algo paso al elimnar el evento ");
		}else{
			return true;
		}	
	
		
		return false;
	}

	//Sin ningun parametro
	@SuppressWarnings("unchecked")
	@Override
	public List<ProgLimpiezaDTO> obtieneInfo() throws Exception{
		Map<String,Object> out = null;
		List<ProgLimpiezaDTO> lista = null;
		int error = 0;
		
		out = jdbcBuscaInfoProgLimp.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPROGLIMPZA.SP_SEL_DETALLE}");
        listaDetU = (List<ProgLimpiezaDTO>) out.get("RCL_PROGLZ");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las ProgLimp");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualiza(ProgLimpiezaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TAB", bean.getIdProg())
                .addValue("PA_FIID_SUC", bean.getCeco())
                .addValue("PA_FIID_USU", bean.getIdUsuario());

        out = jdbcActualizaProgLimp.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPROGLIMPZA.SP_ACT_PROG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public int insertaPro(ProgLimpiezaDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_IDPROG", bean.getIdProg())
				.addValue("PA_FIID_SUC",bean.getCeco())
				.addValue("PA_AREA", bean.getArea())
				.addValue("PA_ARTICULO", bean.getArticulo())
				.addValue("PA_FRECUENCIA", bean.getFrecuencia())
				.addValue("PA_HORARIO",bean.getHorario())
				.addValue("PA_RESPONSABLE", bean.getResponsable())
				.addValue("PA_SUPERVISOR", bean.getSupervisor());
			
		
		out = jdbcInsertaProgLimpza.execute(in);
		
		logger.info("Funcion ejecutada: {GESTION.PAADMPROGLIMPZA.SP_INS_PROGARE}");
		
		BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		respuesta = resultado.intValue();
		
		BigDecimal idbloc = (BigDecimal) out.get("PA_FIID_TAB");
		idNota = idbloc.intValue();
		
		if(respuesta !=  1){
			logger.info("Algo paso al insertar el evento en la tabla de programa de limpieza");
		}else{
			return idNota;
		}	
		
		return idNota;
	}
	
		//Sin ningun parametro
		@SuppressWarnings("unchecked")
		@Override
		public List<ProgLimpiezaDTO> obtieneInfoPro() throws Exception{
			Map<String,Object> out = null;
			List<ProgLimpiezaDTO> lista = null;
			int error = 0;
			
			out = jdbcBuscaInfoProgLimpza.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPROGLIMPZA.SP_SEL_DET}");
        listaDetU = (List<ProgLimpiezaDTO>) out.get("RCL_PROGLZ");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo paso al obtener las ProgLimp");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @Override
    public boolean actualizaPro(ProgLimpiezaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TAB", bean.getIdArea())
                .addValue("PA_IDPROG", bean.getIdProg())
                .addValue("PA_FIID_SUC", bean.getCeco())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_ARTICULO", bean.getArticulo())
                .addValue("PA_FRECUENCIA", bean.getFrecuencia())
                .addValue("PA_HORARIO", bean.getHorario())
                .addValue("PA_RESPONSABLE", bean.getResponsable())
                .addValue("PA_SUPERVISOR", bean.getSupervisor());

        out = jdbcActualizaProgLimpza.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPROGLIMPZA.SP_ACT_PROGAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            logger.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }
}
