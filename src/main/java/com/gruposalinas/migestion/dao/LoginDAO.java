package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.LoginDTO;
import java.util.List;
import java.util.Map;

public interface LoginDAO {

    public List<LoginDTO> buscaUsuario(int noEmpleado) throws Exception;

    public Map<String, Object> buscaUsuarioCheck(int noEmpleado) throws Exception;
}
