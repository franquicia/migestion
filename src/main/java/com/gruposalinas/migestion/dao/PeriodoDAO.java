package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PeriodoDTO;
import java.util.List;

public interface PeriodoDAO {

    public List<PeriodoDTO> obtienePeriodo(String idPeriodo, String descripcion) throws Exception;

    public boolean insertaPeriodo(String idPeriodo, String descripcion, int commit) throws Exception;

    public boolean actualizaPeriodo(String idPeriodo, String descripcion, int commit) throws Exception;

    public boolean eliminaPeriodo(String idPeriodo) throws Exception;

}
