package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import com.gruposalinas.migestion.mappers.CecosTokenRowMapper;
import com.gruposalinas.migestion.mappers.UsuarioTokenRowMapper;

public class ObtieneTokenDAOImpl extends DefaultDAO implements ObtieneTokenDAO {

    Logger logger = LogManager.getLogger(ObtieneTokenDAOImpl.class);

    private DefaultJdbcCall jdbcConsultaUsuarios;
    private DefaultJdbcCall jdbcConsultaTokens;

    public void init() {

        jdbcConsultaUsuarios = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPTOKEN")
                .withProcedureName("SPOBTUSR_CECO").returningResultSet("RCL_USERS", new CecosTokenRowMapper());

        jdbcConsultaTokens = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAREPTOKEN")
                .withProcedureName("SPOBTUSR_TOKEN").returningResultSet("RCL_USERS", new UsuarioTokenRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UsuarioInfoTokenDTO> consultaUsuarios(int ceco) throws Exception {
        List<UsuarioInfoTokenDTO> tokens = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECOP", ceco);

        out = jdbcConsultaUsuarios.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_ERROR");
        ejecucion = returnejec.intValue();

        tokens = (List<UsuarioInfoTokenDTO>) out.get("RCL_USERS");

        if (ejecucion == 1) {
            logger.info("Ocurrio un problema al obtener los usuarios para tokens");
        }

        return tokens;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UsuarioInfoTokenDTO> consultaTokens(int idUsuario) throws Exception {
        List<UsuarioInfoTokenDTO> tokens = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USU", idUsuario);

        out = jdbcConsultaTokens.execute(in);

        BigDecimal returnejec = (BigDecimal) out.get("PA_ERROR");
        ejecucion = returnejec.intValue();

        tokens = (List<UsuarioInfoTokenDTO>) out.get("RCL_USERS");

        if (ejecucion == 1) {
            logger.info("Ocurrio un problema al obtener los tokens");
        }

        return tokens;
    }

}
