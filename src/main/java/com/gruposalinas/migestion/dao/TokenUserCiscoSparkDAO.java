package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TokenUserCiscoSparkDTO;
import java.util.List;

public interface TokenUserCiscoSparkDAO {

    public boolean insertaDispositivo(TokenUserCiscoSparkDTO data) throws Exception;

    public List<TokenUserCiscoSparkDTO> obtienePorCorreo(String correo) throws Exception;

    public boolean eliminaRegistro(String tokenApple) throws Exception;

}
