package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.DatosUsuarioDTO;
import java.util.List;

public interface DatosUsuarioDAO {

    public List<DatosUsuarioDTO> obtieneDatos(String usuario) throws Exception;

}
