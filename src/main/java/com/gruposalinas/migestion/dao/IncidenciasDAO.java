package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.IncidenciasDTO;
import java.util.List;

public interface IncidenciasDAO {

    public int inserta(IncidenciasDTO bean) throws Exception;

    public int insertaLimpieza(IncidenciasDTO bean) throws Exception;

    public boolean elimina(String idTipo) throws Exception;

    public boolean depura() throws Exception;

    public List<IncidenciasDTO> obtieneDatos(int idTipo) throws Exception;

    public List<IncidenciasDTO> obtieneInfo() throws Exception;

    public List<IncidenciasDTO> obtieneInfoLimpieza() throws Exception;

    public boolean actualiza(IncidenciasDTO bean) throws Exception;

}
