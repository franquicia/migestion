package com.gruposalinas.migestion.dao;

import java.util.ArrayList;

import com.gruposalinas.migestion.domain.PeticionDTO;

public interface PeticionesRemedyDAO {

    public boolean insertaPeticion(String origen) throws Exception;

    public int eliminaPeticiones(String fechaIncio, String fechaFin) throws Exception;

    public ArrayList<PeticionDTO> consultaPeticiones(String fechaIncio, String fechaFin) throws Exception;

}
