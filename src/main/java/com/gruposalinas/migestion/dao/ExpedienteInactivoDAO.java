package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ExpedienteInactivoDTO;
import java.util.List;

public interface ExpedienteInactivoDAO {
	
	public List<ExpedienteInactivoDTO> buscaExpediente(String idExpediente, String idEstatus, String idCeco, String idUsuario, String fechaI, String fechaF) throws Exception;
	
	public List<ExpedienteInactivoDTO> buscaResponsable(String idExpediente, String idEstatus, String idUsuario) throws Exception;

	public boolean actualizaEstatus(int estatus, int idExpediente) throws Exception;
	
	public boolean eliminaExpediente(int idExpediente) throws Exception;
	
	public boolean eliminaResponsables(int idExpediente) throws Exception;
	
	
}
