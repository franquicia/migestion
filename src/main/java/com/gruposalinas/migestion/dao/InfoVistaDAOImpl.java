package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.InfoVistaDTO;
import com.gruposalinas.migestion.mappers.InfoVistaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class InfoVistaDAOImpl extends DefaultDAO implements InfoVistaDAO {

    private static Logger logger = LogManager.getLogger(InfoVistaDAOImpl.class);

    DefaultJdbcCall jdbcConsultaInfoVista;
    DefaultJdbcCall jdbcConsultaInfoVistaParam;
    DefaultJdbcCall jdbcInsertaInfoVista;
    DefaultJdbcCall jdbcActualizaInfoVista;
    DefaultJdbcCall jdbcEliminaInfoVista;

    public void init() {

        jdbcConsultaInfoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PARAREPINFOVIS")
                .withProcedureName("SP_SEL_INF_VIS")
                .returningResultSet("RCL_INFV", new InfoVistaRowMapper());

        jdbcConsultaInfoVistaParam = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PARAREPINFOVIS")
                .withProcedureName("SP_SEL_SINPAR")
                .returningResultSet("RCL_INFV", new InfoVistaRowMapper());

        jdbcInsertaInfoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PARAREPINFOVIS")
                .withProcedureName("SP_INS_INF_VIS");

        jdbcActualizaInfoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PARAREPINFOVIS")
                .withProcedureName("SP_ACT_INF_VIS");

        jdbcEliminaInfoVista = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PARAREPINFOVIS")
                .withProcedureName("SP_DEL_INF_VIS");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InfoVistaDTO> buscaInfoVista(int idInfoVista) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<InfoVistaDTO> listaCeco = null;

        logger.info("Entro en el dao Consulta Info Vista");

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_INFVIS", idInfoVista);

        out = jdbcConsultaInfoVista.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PARAREPINFOVIS.SP_SEL_INF_VIS}");

        listaCeco = (List<InfoVistaDTO>) out.get("RCL_INFV");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al consultar los InfoVista");
        } else {
            return listaCeco;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InfoVistaDTO> buscaInfoVistaParam(String idInfoVista, String idCatVista, String idUsuario, String plataforma, String fecha) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<InfoVistaDTO> listaCeco = null;

        logger.info("Entro en el dao Consulta Info Vista sin Param");

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_INFVIS", idInfoVista)
                .addValue("P_IDCATVIS", idCatVista)
                .addValue("P_ID_USU", idUsuario)
                .addValue("P_PLATAF", plataforma)
                .addValue("P_FECHA", fecha);

        out = jdbcConsultaInfoVistaParam.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PARAREPINFOVIS.SP_SEL_INF_VIS}");

        listaCeco = (List<InfoVistaDTO>) out.get("RCL_INFV");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al consultar los InfoVista");
        } else {
            return listaCeco;
        }

        return null;
    }

    @Override
    public boolean insertaInfoVista(InfoVistaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_CATV", bean.getIdCatVista())
                .addValue("P_FIIDUSU", bean.getIdUsuario())
                .addValue("P_PLATAF", bean.getPlataforma())
                .addValue("P_FECHA", bean.getFecha());

        out = jdbcInsertaInfoVista.execute(in);

        //logger.info("Funcion ejecutada:{GESTION.PARAREPINFOVIS.SP_INS_INF_VIS}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            //logger.info("Algo ocurrio al insertar en InfoVista");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaInfoVista(InfoVistaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_INFVIS", bean.getIdInfoVista())
                .addValue("P_FIID_CATV", bean.getIdCatVista())
                .addValue("P_FIIDUSU", bean.getIdUsuario())
                .addValue("P_PLATAF", bean.getPlataforma())
                .addValue("P_FECHA", bean.getFecha());

        out = jdbcActualizaInfoVista.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PARAREPINFOVIS.SP_ACT_INF_VIS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al modificar en InfoVista");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaInfoVista(int idInfoVista) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("P_FIID_INFVIS", idInfoVista);

        out = jdbcEliminaInfoVista.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PARAREPINFOVIS.SP_DEL_INF_VIS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al eliminar en InfoVista");
        } else {
            return true;
        }

        return false;
    }

}
