package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.BitacoraMntoDTO;
import com.gruposalinas.migestion.mappers.BitacoraMantenimientoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class BitacoraMantenimientoDAOImpl extends DefaultDAO implements BitacoraMantenimientoDAO {

    private static Logger logger = LogManager.getLogger(AgendaDAOImpl.class);

		private DefaultJdbcCall jdbcActualizaBitacoraMnto;
		private DefaultJdbcCall jdbcEliminaBitacoraMnto;
		private DefaultJdbcCall jdbcConsultaBitacoraMntoIds;

		public void init(){

			jdbcActualizaBitacoraMnto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMABITAAMBT")
				.withProcedureName("SP_ACT_BITAAMBT");
			jdbcEliminaBitacoraMnto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMABITAAMBT")
				.withProcedureName("SP_DEL_BITAAMBT");

			jdbcConsultaBitacoraMntoIds = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMABITAAMBT")
				.withProcedureName("SP_SEL_BITAAMBT_U")
				.returningResultSet("RCL_BITAAMBT", new BitacoraMantenimientoRowMapper());
		}
		

		
		@Override
		public int actualizaBitacoraMnto(BitacoraMntoDTO bean) throws Exception {
			int respuesta = 0;
			
			Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                //.addValue("PA_FIID_BITAMTTO",bean.getIdAmbito())
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FCID_FOLIO", bean.getIdFolio())
                .addValue("PA_FCFECHA_VISITA", bean.getFechaVisita())
                .addValue("PA_FCNOMBRE_PROVEE", bean.getNombreProvee())
                .addValue("PA_FCTIPO_MTTO", bean.getTipoMantento())
                .addValue("PA_FCFECHA_SOLICIT", bean.getFechaSolicitud())
                .addValue("PA_FCHORA_ENTRADA", bean.getHoraEntrada())
                .addValue("PA_FCHORA_SALIDA", bean.getHoraSalida())
                .addValue("PA_FCNUM_PERSONAS", bean.getNumPersonas())
                .addValue("PA_FCDETALLE", bean.getDetalle())
                .addValue("PA_FCRUTAPROVEED", bean.getRutaProveedor())
                .addValue("PA_FCRUTAGERENTE", bean.getRutaGerente());

        out = jdbcActualizaBitacoraMnto.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMABITAAMBT.SP_ACT_BITAAMBT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al actualizar la bitacora de mantenimiento.");
        } else {
            return respuesta;
        }
        return respuesta;
    }

    @Override
    public int eliminaBitacoraMnto(int idBitacora) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

			SqlParameterSource in = new MapSqlParameterSource()
					.addValue("PA_FIID_BITAMTTO",idBitacora);
			
			out = jdbcEliminaBitacoraMnto.execute(in);
			
			logger.info("Funcion ejecutada: {GESTION.PAADMABITAAMBT.SP_DEL_BITAAMBT}");
			
			BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
			respuesta = resultado.intValue();
			
			if(respuesta ==  0){
				logger.info("Algo paso al eliminar la bitacora de mantenimiento.");
			}else{
				return respuesta;
			}	
			return respuesta;
		}
		

		@SuppressWarnings("unchecked")
		@Override
		public List<BitacoraMntoDTO> consultaBitacoraMntoIds(int idAmbito) throws Exception {
			
			Map<String, Object> out = null;
			int ejecucion = 0;
			List<BitacoraMntoDTO> lista = null;
			
			
			SqlParameterSource in = new MapSqlParameterSource()
					.addValue("PA_FCID_CECO", idAmbito);
					
					
			out = jdbcConsultaBitacoraMntoIds.execute(in);
			
			logger.info("Funcion ejecutada: {GESTION.PAADMABITAAMBT.SP_SEL_BITAAMBT_U}");
			
			BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
			ejecucion = returnEjecucion.intValue();
					
			if(ejecucion != 1){
				lista = new ArrayList<BitacoraMntoDTO>();
				logger.info("Ocurrio un problema al obtener los datos de la bitacora de mantenimiento.");
			}else{
				lista = (List<BitacoraMntoDTO>) out.get("RCL_BITAAMBT");
			}

        return lista;
    }
}
