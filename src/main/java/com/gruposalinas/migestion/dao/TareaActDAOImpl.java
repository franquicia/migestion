package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TareaActDTO;
import com.gruposalinas.migestion.mappers.PeriodoReporteRowMapper;
import com.gruposalinas.migestion.mappers.ReporteTareaRowMapper;
import com.gruposalinas.migestion.mappers.TareaRowMapper;
import com.gruposalinas.migestion.mappers.TareaSucursalesRowMapper;
import com.gruposalinas.migestion.mappers.UsuarioTareaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.TareaActDTO;
import com.gruposalinas.migestion.mappers.PeriodoReporteRowMapper;
import com.gruposalinas.migestion.mappers.ReporteTareaRowMapper;
import com.gruposalinas.migestion.mappers.TareaRowMapper;
import com.gruposalinas.migestion.mappers.TareaSucursalesRowMapper;
import com.gruposalinas.migestion.mappers.UsuarioTareaRowMapper;

public class TareaActDAOImpl  extends DefaultDAO implements TareaActDAO  {
	
	private static Logger logger = LogManager.getLogger(TareaActDAOImpl.class);
	
	private DefaultJdbcCall jdbcObtieneTareas;
	private DefaultJdbcCall jdbcInsertaTareas;
	private DefaultJdbcCall jdbcActualizaTareas;
	private DefaultJdbcCall jdbcEliminaTareas;

	private List<TareaActDTO> listaTareaAdm;
	
	public void init(){
		

		jdbcObtieneTareas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
				.withSchemaName("GESTION")
				.withCatalogName("PAADMTAREACOM")
				.withProcedureName("SP_SEL_TAREA")
				.returningResultSet("RCL_TAREA", new TareaRowMapper());
		

        jdbcInsertaTareas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTAREACOM")
                .withProcedureName("SP_INS_TAREA");

        jdbcActualizaTareas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTAREACOM")
                .withProcedureName("SP_ACT_TAREA");

        jdbcEliminaTareas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTAREACOM")
                .withProcedureName("SP_DEL_TAREA");

    }

	@SuppressWarnings("unchecked")
	@Override
	public List<TareaActDTO> obtieneTareaCom(String idTarea, String status, String estatusVac, String idUsuario,
			String tipoTarea) throws Exception {
		int respuesta = 0;
		
		Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTAREA", idTarea)
                .addValue("PA_ESTATUS", status)
                .addValue("PA_ESTATUS_VAC", estatusVac)
                .addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_TIPOTAREA", tipoTarea);

        out = jdbcObtieneTareas.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTAREACOM.SP_SEL_TAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaTareaAdm = (List<TareaActDTO>) out.get("RCL_TAREA");

        if (respuesta == 0) {
            logger.info("Algo paso al consular las Tareas ");
        }

        return listaTareaAdm;
    }

    @Override
    public boolean insertaTareas(TareaActDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombreTarea())
                .addValue("PA_IDTAREA", bean.getIdTarea())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_ESTATUS_VAC", bean.getEstatusVac())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_TIPOTAREA", bean.getTipoTarea())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_PKIDTAREA", bean.getIdPkTarea());

        out = jdbcInsertaTareas.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTAREACOM.SP_INS_TAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar la Tareas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaTareas(TareaActDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombreTarea())
                .addValue("PA_IDTAREA", bean.getIdTarea())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_ESTATUS_VAC", bean.getEstatusVac())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_TIPOTAREA", bean.getTipoTarea())
                .addValue("PA_PKIDTAREA", bean.getIdPkTarea())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaTareas.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTAREACOM.SP_ACT_TAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al actualizar la tarea");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaTareas(String idTarea) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PKIDTAREA", idTarea);

        out = jdbcEliminaTareas.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTAREACOM.SP_DEL_TAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar la Tarea ");
        } else {
            return true;
        }

        return false;
    }

}
