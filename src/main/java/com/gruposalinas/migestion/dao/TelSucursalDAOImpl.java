package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TelSucursalDTO;
import com.gruposalinas.migestion.mappers.TelSucursalRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TelSucursalDAOImpl extends DefaultDAO implements TelSucursalDAO {

    private static Logger logger = LogManager.getLogger(TelSucursalDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneTelefono;
    DefaultJdbcCall jdbcInsertaTelefono;
    DefaultJdbcCall jdbcActualizaTelefono;
    DefaultJdbcCall jdbcEliminaTelefono;
    DefaultJdbcCall jdbcDepuraTelefono;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTELSUCURS")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_TELSUC", new TelSucursalRowMapper());//ESTE SI

        jdbcObtieneTelefono = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTELSUCURS")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_TELSUC", new TelSucursalRowMapper());//ESTE SI

        jdbcInsertaTelefono = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTELSUCURS")
                .withProcedureName("SP_INS_TELSUC");//ESTE SI

        jdbcActualizaTelefono = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTELSUCURS")
                .withProcedureName("SP_ACT_TELSUC");//ESTE SI

        jdbcEliminaTelefono = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTELSUCURS")
                .withProcedureName("SP_DEL_TELSUC");

        jdbcDepuraTelefono = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTELSUCURS")
                .withProcedureName("SP_DEPURA_TELSUC");//ESTE SI

    }

    @Override
    public List<TelSucursalDTO> obtieneTodosTelefonos() throws Exception {
        Map<String, Object> out = null;
        List<TelSucursalDTO> listaTelefonos = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMTELSUCURS.SP_SEL_G_USUARIO}");

        listaTelefonos = (List<TelSucursalDTO>) out.get("RCL_TELSUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al obtener los Telefonos");
        } else {
            return listaTelefonos;
        }

        return listaTelefonos;
    }

    @Override
    public List<TelSucursalDTO> obtieneTelefono(int idCeco) throws Exception {
        Map<String, Object> out = null;
        List<TelSucursalDTO> listaTelefonos = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_SUC", idCeco);

        out = jdbcObtieneTelefono.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTELSUCURS.SP_SEL_USUARIO}");

        listaTelefonos = (List<TelSucursalDTO>) out.get("RCL_TELSUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al obtener el telefono con id(" + idCeco + ")");
        } else {
            return listaTelefonos;
        }

        return null;
    }

    @Override
    public boolean insertaTelefono(TelSucursalDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTEL", bean.getIdTelefono())
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_TELEFONO", bean.getTelefono())
                .addValue("PA_PROVEEDOR", bean.getProveedor())
                .addValue("PA_ESTATUS", bean.getEstatus());

        out = jdbcInsertaTelefono.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTELSUCURS.SP_INS_USUARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al insertar el Telefono");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaTelefono(TelSucursalDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTEL", bean.getIdTelefono())
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_TELEFONO", bean.getTelefono())
                .addValue("PA_PROVEEDOR", bean.getProveedor())
                .addValue("PA_ESTATUS", bean.getEstatus());

        out = jdbcActualizaTelefono.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTELSUCURS.SP_INS_USUARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al insertar el Telefono");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaTelefono(int idTelefono) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTEL", idTelefono);

        out = jdbcEliminaTelefono.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMTELSUCURS.SP_DEL_USUARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al borrar el Telefono id(" + idTelefono + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean depuraTelefono() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcDepuraTelefono.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMTELSUCURS.SP_DEL_USUARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al depurar la tabla de Telefonos");
        } else {
            return true;
        }

        return false;
    }
}
