package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CargaDAOImpl extends DefaultDAO implements CargaDAO {

    private static Logger logger = LogManager.getLogger(CargaDAOImpl.class);

    DefaultJdbcCall jdbcCargaUsuarioPaso;
    DefaultJdbcCall jdbcCargaCecosPaso;
    DefaultJdbcCall jdbcCargaSucursalesPaso;
    DefaultJdbcCall jdbcCargaPuestos;
    DefaultJdbcCall jdbcCargaUsuarios;
    DefaultJdbcCall jdbcCargaCecos;
    DefaultJdbcCall jdbcCargaSucursales;
    DefaultJdbcCall jdbcCargaGeografia;
    DefaultJdbcCall jdbcCargaTareas;
    DefaultJdbcCall jdbcCargaLimpiaEspacios;
    DefaultJdbcCall jdbcCargaUsuariosFRQ;
    DefaultJdbcCall jdbcCargaUsuariosExterno;
    DefaultJdbcCall jdbcCargaPerfilesMantenimiento;

    public void init() {

        jdbcCargaUsuarioPaso = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_USUARIOS");

        jdbcCargaCecosPaso = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_CECOS");

        jdbcCargaSucursalesPaso = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_SUCURSALES");

        jdbcCargaPuestos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_PUESTOS");

        jdbcCargaUsuarios = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_USUARIOS_TA");

        jdbcCargaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_CECOS_TA");

        jdbcCargaSucursales = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_SUCURSALES_TA");

        jdbcCargaGeografia = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_GEOGRAFIA")
                .withProcedureName("SP_CARGA_GEOGRAFIA");

        jdbcCargaTareas = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS")
                .withProcedureName("SP_CARGA_TAREAS_TA");

        jdbcCargaLimpiaEspacios = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACECOSUTIL")
                .withProcedureName("SP_QUITA_ESPACIOS");
        jdbcCargaUsuariosFRQ = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PACARGAS2")
                .withProcedureName("SP_CARGAUSUFRQ");

        jdbcCargaUsuariosExterno = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_CARGAUSUEXT");

        jdbcCargaPerfilesMantenimiento = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILREM")
                .withProcedureName("SP_PERFILESMTTO");

    }

    @Override
    public boolean cargaUsuariosPaso() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaUsuarioPaso.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_USUARIOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Usuarios de Paso ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaCecosPaso() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaCecosPaso.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_CECOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Cecos de Paso");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaSucursalesPaso() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaSucursalesPaso.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_SUCURSALES}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Sucursales de Paso");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaPuestos() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaPuestos.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_PUESTOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Puestos");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaUsuarios() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaUsuarios.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_USUARIOS_TA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Usuarios");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaCecos() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaCecos.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_CECOS_TA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Cecos");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaSucursales() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaSucursales.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_SUCURSALES_TA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Sucursales");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaGeografia() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaGeografia.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_GEOGRAFIA.SP_CARGA_GEOGRAFIA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Carga de Geografia");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaTareas() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaTareas.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGA_TAREAS_TA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Tareas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaLimpiaCecos() throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        out = jdbcCargaLimpiaEspacios.execute();

        logger.info("Funcion ejecutada: {GESTION.PACECOSUTIL.SP_QUITA_ESPACIOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al limpar los nombres de los CECOS");
        } else {
            return true;
        }

        return false;

    }

    @Override
    public boolean cargaUsuariosFRQ() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaUsuariosFRQ.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAS.SP_CARGAUSUFRQ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Usuarios de FRANQUICIA");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaUsuariosExternos() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaUsuariosExterno.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMEMPEXTERNO.SP_CARGAUSUEXT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga de Usuarios Externos");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaPerfilesMantenimiento() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaPerfilesMantenimiento.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMPERFILREM.SP_PERFILESMTTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al insertar la Carga Perfiles de Mantenimiento");
        } else {
            return true;
        }

        return false;
    }

}
