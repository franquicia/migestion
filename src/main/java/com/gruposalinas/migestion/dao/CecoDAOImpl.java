package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.CecoComboDTO;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.mappers.CecoComboRowMapper;
import com.gruposalinas.migestion.mappers.CecoRowMapper;
import com.gruposalinas.migestion.mappers.CecoSuperiorRowMapper;

public class CecoDAOImpl extends DefaultDAO implements CecoDAO {

    private static Logger logger = LogManager.getLogger(CecoDAOImpl.class);

    DefaultJdbcCall jdbcConsultaCecos;
    DefaultJdbcCall jdbcConsultaCecosSuperior;
    DefaultJdbcCall jdbcInsertaCecos;
    DefaultJdbcCall jdbcActualizaCecos;
    DefaultJdbcCall jdbcEliminaCecos;
    DefaultJdbcCall jdbcNegocioCecos;
    DefaultJdbcCall jdbcGetCercanos;

    public void init() {

        jdbcConsultaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMCECO")
                .withProcedureName("SP_SEL_CECO")
                .returningResultSet("RCL_CECO", new CecoRowMapper());

        jdbcConsultaCecosSuperior = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMCECO")
                .withProcedureName("SP_SEL_CECO_SUP")
                .returningResultSet("RCL_CECO", new CecoSuperiorRowMapper());

        jdbcInsertaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMCECO")
                .withProcedureName("SP_INS_CECO");

        jdbcActualizaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMCECO")
                .withProcedureName("SP_ACT_CECO");

        jdbcEliminaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMCECO")
                .withProcedureName("SP_DEL_CECO");

        jdbcNegocioCecos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMCECO")
                .withProcedureName("SP_NEG_CECO");

        jdbcGetCercanos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PALOCALIZACECO")
                .withProcedureName("SP_GET_CERCANAS")
                .returningResultSet("RCL_SUCURSALES", new CecoComboRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CecoDTO> buscaCecos(String idCeco, String idCecoP, String activo, String negocio, String canal,
            String pais, String nombreCC) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaCeco = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CECO", idCeco)
                .addValue("PA_FIID_CCP", idCecoP)
                .addValue("PA_ACTIVO", activo)
                .addValue("PA_NEGOCIO", negocio)
                .addValue("PA_CANAL", canal)
                .addValue("PA_PAIS", pais)
                .addValue("PA_NOMBRE", nombreCC);

        out = jdbcConsultaCecos.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCECO.SP_SEL_CECO}");

        listaCeco = (List<CecoDTO>) out.get("RCL_CECO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurri� al consultar los CECO");
        } else {
            return listaCeco;
        }

        return null;
    }

    @Override
    public boolean insertaCeco(CecoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIID_PAIS", bean.getIdPais())
                .addValue("PA_FIID_NEGOCIO", bean.getIdNegocio())
                .addValue("PA_FIID_CANAL", bean.getIdCanal())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_FCNOMBRE", bean.getDescCeco())
                .addValue("PA_FICECO_SUPERIOR", bean.getIdCecoSuperior())
                .addValue("PA_FIACTIVO", bean.getActivo())
                .addValue("PA_FIID_NIVEL", bean.getIdNivel())
                .addValue("PA_FCCALLE", bean.getCalle())
                .addValue("PA_FCCIUDAD", bean.getCiudad())
                .addValue("PA_FCCP", bean.getCp())
                .addValue("PA_FCNOMBRE_CTO", bean.getNombreContacto())
                .addValue("PA_FCPUESTO_CTO", bean.getPuestoContacto())
                .addValue("PA_FCTELEFONO_CTO", bean.getTelefonoContacto())
                .addValue("PA_FCFAX_CTO", bean.getFaxContacto())
                .addValue("PA_FCUSUARIO_MOD", bean.getUsuarioModifico())
                .addValue("PA_FDFECHA_MOD", bean.getFechaModifico());

        out = jdbcInsertaCecos.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCECO.SP_INS_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurri� al insertar CECO");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaCeco(CecoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIID_PAIS", bean.getIdPais()).addValue("PA_FIID_NEGOCIO", bean.getIdNegocio())
                .addValue("PA_FIID_CANAL", bean.getIdCanal()).addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_FCNOMBRE", bean.getDescCeco()).addValue("PA_FICECO_SUPERIOR", bean.getIdCecoSuperior())
                .addValue("PA_FIACTIVO", bean.getActivo()).addValue("PA_FIID_NIVEL", bean.getIdNivel())
                .addValue("PA_FCCALLE", bean.getCalle()).addValue("PA_FCCIUDAD", bean.getCiudad())
                .addValue("PA_FCCP", bean.getCp()).addValue("PA_FCNOMBRE_CTO", bean.getNombreContacto())
                .addValue("PA_FCPUESTO_CTO", bean.getPuestoContacto())
                .addValue("PA_FCTELEFONO_CTO", bean.getTelefonoContacto())
                .addValue("PA_FCFAX_CTO", bean.getFaxContacto()).addValue("PA_FCUSUARIO_MOD", bean.getUsuarioModifico())
                .addValue("PA_FDFECHA_MOD", bean.getFechaModifico());

        out = jdbcActualizaCecos.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCECO.SP_ACT_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurri� al insertar CECO");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaCeco(int ceco) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", ceco);

        out = jdbcEliminaCecos.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCECO.SP_DEL_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurri� al elimnar CECO");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<CecoDTO> buscaCecosSuperior(String idCeco, String negocio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaCeco = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CECO", idCeco)
                .addValue("PA_NEGOCIO", negocio);

        out = jdbcConsultaCecosSuperior.execute(in);

//		System.out.println("Ceco Suoerior" + idCeco);
        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCECO.SP_SEL_CECO_SUP}");

        listaCeco = (List<CecoDTO>) out.get("RCL_CECO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurri� al consultar el Ceco Superior");
        } else {
            return listaCeco;
        }

        return null;
    }

    @Override
    public String negocioCeco(String ceco) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        String idNegocio = "";

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", ceco);

        out = jdbcNegocioCecos.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCECO.SP_NEG_CECO}");

        BigDecimal negocio = (BigDecimal) out.get("PA_NEGOCIO");
        idNegocio = negocio.intValue() + "";

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurri� al consultar el negocio");
        } else {
            return idNegocio;
        }

        return idNegocio;
    }

    public List<CecoComboDTO> getCecosCercanos(String ceco, String latitud, String longitud, int inserta_error) {

        Map<String, Object> out = null;
        List<CecoComboDTO> lista = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_LATITUD", latitud)
                .addValue("PA_LONGITUD", longitud)
                .addValue("PA_INSERTA_ERR", inserta_error);

        out = jdbcGetCercanos.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PALOCALIZACECO.SP_GET_CERCANAS}");

        lista = (List<CecoComboDTO>) out.get("RCL_SUCURSALES");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo al consultar las sucursales cercanas");
        } else {
            return lista;
        }

        return null;
    }
}
