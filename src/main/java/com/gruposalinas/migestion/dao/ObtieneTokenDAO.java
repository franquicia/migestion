package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import java.util.List;

public interface ObtieneTokenDAO {

    public List<UsuarioInfoTokenDTO> consultaUsuarios(int ceco) throws Exception;

    public List<UsuarioInfoTokenDTO> consultaTokens(int idUsuario) throws Exception;

}
