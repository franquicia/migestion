package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.ExternoDTO;
import com.gruposalinas.migestion.mappers.UsuarioExternoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class UsuarioExternoDAOImpl extends DefaultDAO implements UsuarioExternoDAO {

    private static Logger logger = LogManager.getLogger(UsuarioExternoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneUsuario;
    DefaultJdbcCall jdbcInsertaUsuario;
    DefaultJdbcCall jdbcActualizaUsuario;
    DefaultJdbcCall jdbcEliminaUsuario;
    DefaultJdbcCall jdbcdepuraUsuarios;
    DefaultJdbcCall jdbcActualizapuesto;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_EMPEXT", new UsuarioExternoRowMapper());//ESTE SI

        jdbcObtieneUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_EMPEXT", new UsuarioExternoRowMapper());//ESTE SI

        jdbcInsertaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_INS_EMPEXT");//ESTE SI

        jdbcActualizaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_ACT_EMPEXT");//ESTE SI

        jdbcEliminaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_DEL_EMPEXT");

        jdbcdepuraUsuarios = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_DEPURA_EMPEXT");

        jdbcActualizapuesto = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMEMPEXTERNO")
                .withProcedureName("SP_ACTPUESTO");

    }

    @Override
    public List<ExternoDTO> obtieneUsuarioExterno() throws Exception {
        Map<String, Object> out = null;
        List<ExternoDTO> listaUsuario = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMUSUARIO.SP_SEL_G_USUARIO}");

        listaUsuario = (List<ExternoDTO>) out.get("RCL_EMPEXT");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al obtener los Usuarios");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

    @Override
    public List<ExternoDTO> obtieneUsuarioExterno(int numEmpleado) throws Exception {
        Map<String, Object> out = null;
        List<ExternoDTO> listaUsuario = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_EMP", numEmpleado);

        out = jdbcObtieneUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSUARIO.SP_SEL_USUARIO}");

        listaUsuario = (List<ExternoDTO>) out.get("RCL_EMPEXT");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al obtener el Usuario con id(" + numEmpleado + ")");
        } else {
            return listaUsuario;
        }

        return null;
    }

    @Override
    public boolean insertaUsuarioExterno(ExternoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", bean.getNumEmpleado())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_DIRECCION", bean.getDireccion())
                .addValue("PA_NUMCASA", bean.getNumCasa())
                .addValue("PA_NUMCEL", bean.getNumCelular())
                .addValue("PA_RFC", bean.getRFCEmpleado())
                .addValue("PA_CENTROCOSTO", Integer.parseInt(bean.getCencosNum()))
                .addValue("PA_PUESTO", "" + bean.getPuesto())
                .addValue("PA_FEC_INGRESO", bean.getFechaIngreso())
                .addValue("PA_FEC_BAJA", bean.getFechaBaja())
                .addValue("PA_FUNCION", bean.getFuncion())
                .addValue("PA_IDUSU", "" + bean.getUsuarioID())
                .addValue("PA_APATERNO", bean.getaPaterno())
                .addValue("PA_AMATERNO", bean.getaMaterno())
                .addValue("PA_EMPSITUACION", bean.getEmpSituacion())
                .addValue("PA_HONORARIOS", bean.getHonorarios())
                .addValue("PA_EMAIL", bean.getEmail())
                .addValue("PA_NUMEMERG", bean.getNumEmergencia())
                .addValue("PA_PERS_EMERG", bean.getPerEmergencia())
                .addValue("PA_NUM_EXTENSION", bean.getNumExt())
                .addValue("PA_SUELDO", Integer.parseInt(bean.getSueldo()))
                .addValue("PA_HORARIOINIC", bean.getHorarioIn())
                .addValue("PA_HORARIOFIN", bean.getHorarioFin())
                .addValue("PA_SEMESTRE", bean.getSemestre())
                .addValue("PA_ACTIVIDADES", bean.getActividades())
                .addValue("PA_LIBERACION", bean.getLiberacion())
                .addValue("PA_FEC_ELABORAC", bean.getElaboracion())
                .addValue("PA_LLAVE", bean.getLlave())
                .addValue("PA_RED", bean.getRed())
                .addValue("PA_LOTUS", bean.getLotus())
                .addValue("PA_CREDENCIAL", bean.getCredencial())
                .addValue("PA_DATASEC", bean.getFolioDataSec())
                .addValue("PA_PAIS", bean.getPais())
                .addValue("PA_NOMB_COMPLETO", bean.getNombreCompleto())
                .addValue("PA_ASISTENCIA", bean.getAsistencia())
                .addValue("PA_CURP", bean.getCurp());

        out = jdbcInsertaUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSUARIO.SP_INS_USUARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al insertar el Usuario");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaUsuarioExterno(ExternoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", bean.getNumEmpleado())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_DIRECCION", bean.getDireccion())
                .addValue("PA_NUMCASA", bean.getNumCasa())
                .addValue("PA_NUMCEL", bean.getNumCelular())
                .addValue("PA_RFC", bean.getRFCEmpleado())
                .addValue("PA_CENTROCOSTO", Integer.parseInt(bean.getCencosNum()))
                .addValue("PA_PUESTO", "" + bean.getPuesto())
                .addValue("PA_FEC_INGRESO", bean.getFechaIngreso())
                .addValue("PA_FEC_BAJA", bean.getFechaBaja())
                .addValue("PA_FUNCION", bean.getFuncion())
                .addValue("PA_IDUSU", "" + bean.getUsuarioID())
                .addValue("PA_APATERNO", bean.getaPaterno())
                .addValue("PA_AMATERNO", bean.getaMaterno())
                .addValue("PA_EMPSITUACION", bean.getEmpSituacion())
                .addValue("PA_HONORARIOS", bean.getHonorarios())
                .addValue("PA_EMAIL", bean.getEmail())
                .addValue("PA_NUMEMERG", bean.getNumEmergencia())
                .addValue("PA_PERS_EMERG", bean.getPerEmergencia())
                .addValue("PA_NUM_EXTENSION", bean.getNumExt())
                .addValue("PA_SUELDO", Integer.parseInt(bean.getSueldo()))
                .addValue("PA_HORARIOINIC", bean.getHorarioIn())
                .addValue("PA_HORARIOFIN", bean.getHorarioFin())
                .addValue("PA_SEMESTRE", bean.getSemestre())
                .addValue("PA_ACTIVIDADES", bean.getActividades())
                .addValue("PA_LIBERACION", bean.getLiberacion())
                .addValue("PA_FEC_ELABORAC", bean.getElaboracion())
                .addValue("PA_LLAVE", bean.getLlave())
                .addValue("PA_RED", bean.getRed())
                .addValue("PA_LOTUS", bean.getLotus())
                .addValue("PA_CREDENCIAL", bean.getCredencial())
                .addValue("PA_DATASEC", bean.getFolioDataSec())
                .addValue("PA_PAIS", bean.getPais())
                .addValue("PA_NOMB_COMPLETO", bean.getNombreCompleto())
                .addValue("PA_ASISTENCIA", bean.getAsistencia())
                .addValue("PA_CURP", bean.getCurp());

        out = jdbcActualizaUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSUARIO.SP_INS_USUARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al insertar el Usuario");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaUsuarioExterno(int numEmpleado) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", numEmpleado);

        out = jdbcEliminaUsuario.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSUARIO.SP_DEL_USUARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al borrar el Usuario id(" + numEmpleado + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean depuraUsuarioExterno() throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        out = jdbcdepuraUsuarios.execute();

        logger.info("Funcion ejecutada: {GESTION.PAADMUSUARIO.SP_DEPURA_EMPEXT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaPuesto(int numEmpleado, int puesto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", numEmpleado).addValue("PA_PUESTO", puesto);

        out = jdbcActualizapuesto.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMUSUARIO.SP_ACTPUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo paso al actualizar el Usuario id(" + numEmpleado + ")");
        } else {
            return true;
        }

        return false;
    }
}
