package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import java.util.List;

public interface TicketCuadrillaDAO {

    public int inserta(TicketCuadrillaDTO bean) throws Exception;

    public boolean elimina(String ticket) throws Exception;

    public List<TicketCuadrillaDTO> obtieneDatos(int idCuadrilla) throws Exception;

    public List<TicketCuadrillaDTO> obtieneInfo() throws Exception;

    public List<TicketCuadrillaDTO> obtieneCuadrillaTK(int idCuadrilla, int idProveedor, int zona) throws Exception;

    public boolean actualiza(TicketCuadrillaDTO bean) throws Exception;

}
