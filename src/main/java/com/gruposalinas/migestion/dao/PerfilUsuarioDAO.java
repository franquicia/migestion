package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.PerfilUsuarioDTO;
import java.util.List;

public interface PerfilUsuarioDAO {

    public List<PerfilUsuarioDTO> obtienePerfiles(String idUsuario, String idPerfil) throws Exception;

    public List<PerfilUsuarioDTO> obtienePerfilesNue(String idUsuario, String idPerfil) throws Exception;

    public boolean insertaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;

    public boolean insertaPerfilUsuarioNvo(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;

    public boolean actualizaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;

    public boolean eliminaPerfilUsaurio(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;

}
