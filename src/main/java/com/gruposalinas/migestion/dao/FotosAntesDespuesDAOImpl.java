package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.FotosAntesDespuesDTO;
import com.gruposalinas.migestion.mappers.FotosAntesDespuesRowMapper;
import com.gruposalinas.migestion.mappers.FotosAntesDespuesRowMapper2;

public class FotosAntesDespuesDAOImpl extends DefaultDAO implements FotosAntesDespuesDAO{
	
	private static Logger logger = LogManager.getLogger(AgendaDAOImpl.class);
	
			private DefaultJdbcCall jdbcActualizaFotosAntDesp;
			private DefaultJdbcCall jdbcEliminaFotosAntDesp;
			private DefaultJdbcCall jdbcConsultaFotosAntDespUlt;

			public void init(){


				jdbcActualizaFotosAntDesp = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
					.withSchemaName("GESTION")
					.withCatalogName("PAADMFOTANDES")
					.withProcedureName("SP_ACT_FOTANDES");

				jdbcEliminaFotosAntDesp = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
					.withSchemaName("GESTION")
					.withCatalogName("PAADMFOTANDES")
					.withProcedureName("SP_DEL_FOTANDES");

				jdbcConsultaFotosAntDespUlt = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
					.withSchemaName("GESTION")
					.withCatalogName("PAADMFOTANDES")
					.withProcedureName("SP_SEL_FOTANDES_U")
					.returningResultSet("RCL_FOTANDES", new FotosAntesDespuesRowMapper2());


			}


			@Override
			public int actualizaFotoAntesDesp(FotosAntesDespuesDTO bean) throws Exception {
				int respuesta = 0;
				
				Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_EVID", bean.getIdEvento())
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FCLUGARANT", bean.getLugarAntes())
                .addValue("PA_FCLUGARDESP", bean.getLugarDespues())
                .addValue("PA_FCID_EVIDANT", bean.getIdEvidantes())
                .addValue("PA_FDFECHAANTES", bean.getFechaAntes())
                .addValue("PA_FDFECHADESPUES", bean.getFechaDespues())
                .addValue("PA_FCID_EVIDDESP", bean.getIdEviddespues());

        out = jdbcActualizaFotosAntDesp.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMFOTANDES.SP_ACT_FOTANDES}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            logger.info("Algo paso al actualizar la tabla GETAFOTOANTDESP.");
        } else {
            return respuesta;
        }
        return respuesta;
    }

    @Override
    public int eliminaFotoAntesDesp(int idEvento) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

				SqlParameterSource in = new MapSqlParameterSource()
						.addValue("PA_FIID_EVID",idEvento);
				
				out = jdbcEliminaFotosAntDesp.execute(in);
				
				logger.info("Funcion ejecutada: {GESTION.PAADMFOTANDES.SP_DEL_FOTANDES}");
				
				BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
				respuesta = resultado.intValue();
				
				if(respuesta ==  0){
					logger.info("Algo paso al eliminar la tabla GETAFOTOANTDESP.");
				}else{
					return respuesta;
				}	
				return respuesta;
			}
			

			@SuppressWarnings("unchecked")
			@Override
			public List<FotosAntesDespuesDTO> consultaFotoAntesDespUlt() throws Exception {
				
				Map<String, Object> out = null;
				int ejecucion = 0;
				List<FotosAntesDespuesDTO> lista = null;
				
				
				SqlParameterSource in = new MapSqlParameterSource();
				
				out = jdbcConsultaFotosAntDespUlt.execute(in);
				
				logger.info("Funcion ejecutada: {GESTION.PAADMFOTANDES.SP_SEL_FOTANDES_U}");
				
				BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
				ejecucion = returnEjecucion.intValue();
						
				if(ejecucion != 1){
					lista = new ArrayList<FotosAntesDespuesDTO>();
					logger.info("Ocurrio un problema al obtener los datos de la tabla GETAFOTOANTDESP al la fecha máxima.");
				}else{
					lista = (List<FotosAntesDespuesDTO>) out.get("RCL_FOTANDES");
				}

				return lista;
			}
}
