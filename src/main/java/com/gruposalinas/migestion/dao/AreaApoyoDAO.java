package com.gruposalinas.migestion.dao;

import com.gruposalinas.migestion.domain.AreaApoyoDTO;
import java.util.List;

public interface AreaApoyoDAO {

	public boolean elimina(String idArea)throws Exception;

	public boolean eliminaEvi(String idEvid)throws Exception;

	public List<AreaApoyoDTO> obtieneInfo() throws Exception;

	
	public boolean actualizaEvi(AreaApoyoDTO bean)throws Exception;
	
	
}
