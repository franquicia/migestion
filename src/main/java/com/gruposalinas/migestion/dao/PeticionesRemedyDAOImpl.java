package com.gruposalinas.migestion.dao;

import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gruposalinas.migestion.domain.PeticionDTO;
import com.gruposalinas.migestion.mappers.PeticionRowMapper;

public class PeticionesRemedyDAOImpl extends DefaultDAO implements PeticionesRemedyDAO {

    private static Logger logger = LogManager.getLogger(PeticionesRemedyDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaPeticion;
    private DefaultJdbcCall jdbcEliminaPeticiones;
    private DefaultJdbcCall jdbcConsultaPeticiones;

    public void init() {

        jdbcInsertaPeticion = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPETIREMEDY")
                .withProcedureName("SP_INSERTA");

        jdbcEliminaPeticiones = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPETIREMEDY")
                .withProcedureName("SP_ELIMINA");

        jdbcConsultaPeticiones = (DefaultJdbcCall) new DefaultJdbcCall(getGtnJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPETIREMEDY")
                .withProcedureName("SP_CONTEO")
                .returningResultSet("RCL_CONSULTA", new PeticionRowMapper());

    }

    @Override
    public boolean insertaPeticion(String origen) throws Exception {

        Map<String, Object> out = null;

        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ORIGEN", origen);

        out = jdbcInsertaPeticion.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPETIREMEDY.SP_INSERTA}");

        BigDecimal respuesta = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = respuesta.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar la peticion de remedy");
        } else {
            return true;
        }

        return false;
    }

    /**
     * @return Si es -1 algo ocurrio al eliminar, si es >= 0 se eliminaron
     * correctamente
     *
     */
    @Override
    public int eliminaPeticiones(String fechaIncio, String fechaFin) throws Exception {

        Map<String, Object> out = null;

        int ejecucion = 0;
        int rowsAfectados = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHA_INI", fechaIncio)
                .addValue("PA_FECHA_FIN", fechaFin);

        out = jdbcEliminaPeticiones.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPETIREMEDY.SP_ELIMINA}");

        BigDecimal respuesta = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = respuesta.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al eliminar las peticiones de remedy");
            return -1;
        } else {

            BigDecimal eliminados = (BigDecimal) out.get("PA_AFECTADOS");
            rowsAfectados = eliminados.intValue();

            return rowsAfectados;
        }

    }

    @Override
    public ArrayList<PeticionDTO> consultaPeticiones(String fechaIncio, String fechaFin) throws Exception {
        Map<String, Object> out = null;

        ArrayList<PeticionDTO> listaPeticiones = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHA_INI", fechaIncio)
                .addValue("PA_FECHA_FIN", fechaFin);

        out = jdbcConsultaPeticiones.execute(in);

        logger.info("Funcion ejecutada: {GESTION.PAADMPETIREMEDY.SP_CONTEO}");

        BigDecimal respuesta = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = respuesta.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las peticiones de remedy");

        } else {

            listaPeticiones = (ArrayList<PeticionDTO>) out.get("RCL_CONSULTA");

        }

        return listaPeticiones;
    }

}
