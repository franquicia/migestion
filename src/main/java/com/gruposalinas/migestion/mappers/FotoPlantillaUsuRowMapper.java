package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.FotoPlantillaDTO;

public class FotoPlantillaUsuRowMapper implements RowMapper<FotoPlantillaDTO> {

    @Override
    public FotoPlantillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FotoPlantillaDTO fp = new FotoPlantillaDTO();

        fp.setIdPlanti(rs.getInt("FIID_FOTOPLANTI"));
        fp.setCeco(rs.getString("FCID_CECO"));
        fp.setUsuario(rs.getInt("FIID_USUARIO"));
        fp.setNombre(rs.getString("FCNOMBRE"));
        fp.setPuesto(rs.getString("FCDESCRIPCION"));
        fp.setRuta(rs.getString("FCRUTAFOTO"));
        fp.setPeriodo(rs.getString("FCPERIODO"));

        return fp;
    }

}
