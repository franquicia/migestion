package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;

public class ProgLimpiezaAreaRowMapper implements RowMapper<ProgLimpiezaDTO> {

    @Override
    public ProgLimpiezaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProgLimpiezaDTO pl = new ProgLimpiezaDTO();

        pl.setIdArea(rs.getInt("FIID_IDAREA"));
        pl.setIdProg(rs.getInt("FIID_PROGLIMP"));
        pl.setCeco(rs.getString("FCID_CECO"));
        pl.setArea(rs.getString("FCAREALIMPIEZA"));
        pl.setArticulo(rs.getString("FCARTICULO"));
        pl.setFrecuencia(rs.getString("FCFRECUENCIA"));
        pl.setHorario(rs.getString("FCHORARIO"));
        pl.setResponsable(rs.getString("FCRESPONSABLE"));
        pl.setSupervisor(rs.getString("FCSUPERVISOR"));
        pl.setPeriodo(rs.getString("FCPERIODO"));

        //pl.setBandera(rs.getInt("BANDERA"));
        return pl;
    }

}
