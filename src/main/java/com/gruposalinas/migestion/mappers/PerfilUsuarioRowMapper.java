package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.PerfilUsuarioDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PerfilUsuarioRowMapper implements RowMapper<PerfilUsuarioDTO> {

    @Override
    public PerfilUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();

        perfilUsuarioDTO.setIdPerfil(rs.getInt("FIID_PERFIL"));
        perfilUsuarioDTO.setIdUsuario(rs.getInt("FIIDUSUARIO"));

        return perfilUsuarioDTO;
    }

}
