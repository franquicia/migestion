package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.InfoVistaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class InfoVistaRowMapper implements RowMapper<InfoVistaDTO> {

    @Override
    public InfoVistaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        InfoVistaDTO infoVistaDTO = new InfoVistaDTO();

        infoVistaDTO.setIdInfoVista(rs.getInt("FIID_INF_VIS"));
        infoVistaDTO.setIdCatVista(rs.getInt("FIID_CATVIS"));
        infoVistaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        infoVistaDTO.setPlataforma(rs.getInt("FCPLATAFORMA"));
        infoVistaDTO.setFecha(rs.getString("FECHA"));

        return infoVistaDTO;
    }

}
