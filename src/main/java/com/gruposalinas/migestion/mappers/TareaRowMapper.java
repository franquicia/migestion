package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TareaActDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TareaRowMapper implements RowMapper<TareaActDTO> {

    @Override
    public TareaActDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TareaActDTO tareaDTO = new TareaActDTO();

        tareaDTO.setIdPkTarea(rs.getInt("FIID_PKTAREA"));
        tareaDTO.setIdTarea(rs.getInt("FIID_TAREA"));
        tareaDTO.setNombreTarea(rs.getString("FCNOM_TAREA"));
        tareaDTO.setEstatus(rs.getInt("FIESTATUS"));
        tareaDTO.setEstatusVac(rs.getInt("FIEST_VAC"));
        tareaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        tareaDTO.setTipoTarea(rs.getString("FCTIPO_TAREA"));

        return tareaDTO;
    }

}
