package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.NotificaAfDTO;

public class NotificaAfRowMapper implements RowMapper<NotificaAfDTO> {

    @Override
    public NotificaAfDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        NotificaAfDTO notificaAFDTO = new NotificaAfDTO();

        notificaAFDTO.setIdNotifica(rs.getInt("FINOTIFICA"));
        notificaAFDTO.setCeco(rs.getString("FC_IDCECO"));
        notificaAFDTO.setCompromisoA(rs.getString("FICOMPROMISO_A"));
        notificaAFDTO.setAvanceA(rs.getString("FIAVANCE_A"));
        notificaAFDTO.setCompromisoP(rs.getString("FICOMPROMISO_P"));
        notificaAFDTO.setAvanceP(rs.getString("FIAVANCE_P"));
        notificaAFDTO.setFecha(rs.getString("FCFECHA_COMP"));

        return notificaAFDTO;
    }

}
