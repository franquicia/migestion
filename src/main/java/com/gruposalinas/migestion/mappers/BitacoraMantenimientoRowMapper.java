package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.BitacoraMntoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class BitacoraMantenimientoRowMapper implements RowMapper<BitacoraMntoDTO> {

    @Override
    public BitacoraMntoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        BitacoraMntoDTO bitacora = new BitacoraMntoDTO();
        bitacora.setIdAmbito(rs.getInt("FIID_BITAMTTO"));
        bitacora.setIdUsuario(rs.getInt("FIID_USUARIO"));
        bitacora.setIdCeco(rs.getString("FCID_CECO"));
        bitacora.setIdFolio(rs.getString("FCID_FOLIO"));
        bitacora.setFechaVisita(rs.getString("FCFECHA_VISITA"));
        bitacora.setNombreProvee(rs.getString("FCNOMBRE_PROVEE"));
        bitacora.setTipoMantento(rs.getString("FCTIPO_MTTO"));
        bitacora.setFechaSolicitud(rs.getString("FCFECHA_SOLICIT"));
        bitacora.setHoraEntrada(rs.getString("FCHORA_ENTRADA"));
        bitacora.setHoraSalida(rs.getString("FCHORA_SALIDA"));
        bitacora.setNumPersonas(rs.getString("FCNUM_PERSONAS"));
        bitacora.setDetalle(rs.getString("FCDETALLE"));
        bitacora.setRutaProveedor(rs.getString("FCRUTAPROVEED"));
        bitacora.setRutaGerente(rs.getString("FCRUTAGERENTE"));
        return bitacora;
    }
}
