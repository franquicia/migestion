package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TokenUserCiscoSparkDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TokenUserCiscoSparkRowMapper1 implements RowMapper {

    public TokenUserCiscoSparkDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TokenUserCiscoSparkDTO userCiscoSparkDTO = new TokenUserCiscoSparkDTO();

        userCiscoSparkDTO.setToken(rs.getString("FCTOKEN"));

        //System.out.println("Cambio");
        return userCiscoSparkDTO;
    }

}
