package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.Usuario_ADTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class Usuario_ARowMapper implements RowMapper<Usuario_ADTO> {

    public Usuario_ADTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        Usuario_ADTO usuarioADTO = new Usuario_ADTO();

        usuarioADTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        usuarioADTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
        usuarioADTO.setIdCeco(rs.getString("FCID_CECO"));
        usuarioADTO.setNombre(rs.getString("FCNOMBRE"));
        usuarioADTO.setActivo(rs.getInt("FIACTIVO"));
        usuarioADTO.setFecha(rs.getString("FDNACIMIENTO"));

        return usuarioADTO;
    }
}
