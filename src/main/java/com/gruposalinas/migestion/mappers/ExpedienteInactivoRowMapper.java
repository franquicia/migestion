package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.ExpedienteInactivoDTO;

public class ExpedienteInactivoRowMapper implements RowMapper<ExpedienteInactivoDTO> {

    @Override
    public ExpedienteInactivoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ExpedienteInactivoDTO expedienteDTO = new ExpedienteInactivoDTO();

        expedienteDTO.setIdExpediente(rs.getInt("FIID_EXPPASIVO"));
        expedienteDTO.setIdGerente(rs.getInt("FIID_USUARIO"));
        expedienteDTO.setIdCeco(rs.getString("FIID_CECO"));
        expedienteDTO.setFecha(rs.getString("FECHA"));
        expedienteDTO.setEstatusExpediente(rs.getInt("FIESTATUS"));
        expedienteDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        expedienteDTO.setCiudad(rs.getString("FCCIUDAD"));

        return expedienteDTO;
    }

}
