package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.EstandPuestoDTO;

public class EstandPuestoMuebleRowMapper implements RowMapper<EstandPuestoDTO> {

    @Override
    public EstandPuestoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        EstandPuestoDTO estp = new EstandPuestoDTO();

        estp.setIdMueble(rs.getInt("FIID_MUEBLE"));
        estp.setIdPuesto(rs.getInt("FIID_ESTPUESTO"));
        estp.setCeco(rs.getString("FCID_CECO"));
        estp.setMueble1caj1(rs.getString("FIID_CAJON1"));
        estp.setMueble1caj2(rs.getString("FIID_CAJON2"));
        estp.setMueble1caj3(rs.getString("FIID_CAJON3"));
        estp.setMueble2caj1(rs.getString("FIID_CAJON4"));
        estp.setMueble2caj2(rs.getString("FIID_CAJON5"));
        estp.setMueble2caj6(rs.getString("FIID_CAJON6"));
        estp.setPeriodo(rs.getString("FCPERIODO"));
        estp.setBandera(rs.getInt("FISTATUS"));
        estp.setPuesto(rs.getInt("FCPUESTO"));
        //estp.setBandera(rs.getInt("FISTATUS"));

        return estp;
    }

}
