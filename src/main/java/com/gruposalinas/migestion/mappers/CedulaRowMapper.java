package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.CedulaDTO;

public class CedulaRowMapper implements RowMapper<CedulaDTO> {

    @Override
    public CedulaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CedulaDTO ced = new CedulaDTO();

        ced.setIdUsuario(rs.getInt("FIID_USUARIO"));
        ced.setIdSuc(rs.getInt("FCID_CECO"));
        ced.setFecha(rs.getString("FCFECHA"));
        ced.setPeriodo(rs.getString("FCPERIODO"));
        ced.setNombre(rs.getString("FCNOMBRE"));
        ced.setDescPuesto(rs.getString("FCDESCRIPCION"));
        ced.setRutafoto(rs.getString("FCRUTAFOTO"));
        ced.setRutaFirmaAcepta(rs.getString("FCFIRMAACEPTA"));
        ced.setRutaFirmaJefe(rs.getString("FCFIRMAJEFEINM"));
        ced.setTipoSangre(rs.getString("FCTIPOSANGRE"));
        ced.setIdSeguroSoc(rs.getString("FCSEGUROSOC"));
        ced.setAlergia(rs.getInt("FCALERGIA"));
        ced.setDescalergia(rs.getString("FCDESCALERGIA"));
        ced.setEnfermedad(rs.getInt("FCENFERMEDAD"));
        ced.setDescEnfermedad(rs.getString("FCDESCENFERMED"));
        ced.setTratamiento(rs.getInt("FCTRATAMESP"));
        ced.setDescTratamiento(rs.getString("FCDESCTRATAM"));
        ced.setContactoEmerg(rs.getString("FCCONTACTOEMER"));
        ced.setTelContacto(rs.getString("FCTELEFONO"));
        ced.setTelSocio(rs.getString("FCTELEFONOSOC"));
        ced.setBandera(rs.getInt("BANDERA"));

        return ced;
    }

}
