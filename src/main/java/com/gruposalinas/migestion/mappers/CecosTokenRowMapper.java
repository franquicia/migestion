package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecosTokenRowMapper implements RowMapper<UsuarioInfoTokenDTO> {

    @Override
    public UsuarioInfoTokenDTO mapRow(ResultSet rs, int cont) throws SQLException {

        UsuarioInfoTokenDTO usuarioInfoTokenDTO = new UsuarioInfoTokenDTO();
        usuarioInfoTokenDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));

        return usuarioInfoTokenDTO;
    }

}
