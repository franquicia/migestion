package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.CecoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoSuperiorRowMapper implements RowMapper<CecoDTO> {

    public CecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CecoDTO cecoDTO = new CecoDTO();

        cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
        cecoDTO.setDescCeco(rs.getString("FCNOMBRE"));
        cecoDTO.setIdCecoSuperior(rs.getInt("FICECO_SUPERIOR"));
        cecoDTO.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        cecoDTO.setIdPais(rs.getInt("FIID_PAIS"));
        cecoDTO.setIdNivel(rs.getInt("LEVEL"));

        return cecoDTO;
    }

}
