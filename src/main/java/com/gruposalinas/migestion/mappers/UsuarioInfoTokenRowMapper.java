package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UsuarioInfoTokenRowMapper implements RowMapper<UsuarioInfoTokenDTO> {

    @Override
    public UsuarioInfoTokenDTO mapRow(ResultSet rs, int cont) throws SQLException {

        UsuarioInfoTokenDTO usuarioInfoTokenDTO = new UsuarioInfoTokenDTO();
        usuarioInfoTokenDTO.setCeco(rs.getString("FCID_CECO"));
        usuarioInfoTokenDTO.setNombre(rs.getString("FCNOMBRE"));
        usuarioInfoTokenDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        usuarioInfoTokenDTO.setNombrececo(rs.getString("FCNOMBRECECO"));
        usuarioInfoTokenDTO.setToken(rs.getString("FCTOKEN"));

        return usuarioInfoTokenDTO;
    }

}
