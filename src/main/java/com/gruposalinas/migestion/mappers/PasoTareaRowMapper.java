package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TablasPasoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PasoTareaRowMapper implements RowMapper<TablasPasoDTO> {

    public TablasPasoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TablasPasoDTO tareasDTO = new TablasPasoDTO();

        tareasDTO.setFiid_tarea(rs.getString("FIID_TAREA"));
        tareasDTO.setFcnom_tarea(rs.getString("FCNOM_TAREA"));
        tareasDTO.setFiestatus(rs.getString("FIESTATUS"));
        tareasDTO.setFiest_vac(rs.getString("FIEST_VAC"));
        tareasDTO.setFiid_usuario(rs.getString("FIID_USUARIO"));

        return tareasDTO;
    }

}
