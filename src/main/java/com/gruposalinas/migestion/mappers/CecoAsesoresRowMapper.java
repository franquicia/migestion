package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.CecoComboDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoAsesoresRowMapper implements RowMapper<CecoComboDTO> {

    @Override
    public CecoComboDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CecoComboDTO cecoComboDTO = new CecoComboDTO();

        cecoComboDTO.setCeco(rs.getString("FCID_CECO"));
        cecoComboDTO.setNombre(rs.getString("FCNOMBRE"));

        return cecoComboDTO;
    }

}
