package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ExpedienteInactivoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ResponsableExpRowMapper implements RowMapper<ExpedienteInactivoDTO> {

    @Override
    public ExpedienteInactivoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ExpedienteInactivoDTO expedienteDTO = new ExpedienteInactivoDTO();

        expedienteDTO.setIdResponsable(rs.getInt("FIID_RESPONSABLE"));
        expedienteDTO.setIdentificador(rs.getInt("FIID_USUARIO"));
        expedienteDTO.setNombreResp(rs.getString("FCNOMBRE"));
        expedienteDTO.setEstatusResp(rs.getInt("FIESTATUSRES"));
        expedienteDTO.setIdExpediente(rs.getInt("FIID_EXPPASIVO"));

        return expedienteDTO;

    }

}
