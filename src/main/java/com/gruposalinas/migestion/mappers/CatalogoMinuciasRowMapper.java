package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;

public class CatalogoMinuciasRowMapper implements RowMapper<CatalogoMinuciasDTO> {

    @Override
    public CatalogoMinuciasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CatalogoMinuciasDTO cm = new CatalogoMinuciasDTO();

        cm.setIdMinucia(rs.getInt("FIID_MINUCIA"));
        cm.setIdUsu(rs.getInt("FIID_USUARIO"));
        cm.setCeco(rs.getString("FCID_CECO"));
        cm.setLugarEchos(rs.getString("FCLUGAR_HECHOS"));
        cm.setFecha(rs.getString("FECHA_HORA"));
        cm.setDireccion(rs.getString("FCDIRECCION"));
        cm.setVobo(rs.getString("FCVOBO_GERENTE"));
        cm.setDestruye(rs.getString("FCDESTRUYE"));
        cm.setTestigo1(rs.getString("FCTESTIGO1"));
        cm.setTestigo2(rs.getString("FCTESTIGO2"));

        return cm;
    }

}
