package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.IndicadorIPNClienteDTO;
import com.gruposalinas.migestion.domain.IndicadorIPNEquipoDTO;

public class IndicadoresIPNRowMapper {

    public class IndicadorClienteIPNRowMapper implements RowMapper<IndicadorIPNClienteDTO> {

        private IndicadorIPNClienteDTO indicadorIPNCliente;

        @Override
        public IndicadorIPNClienteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            indicadorIPNCliente = new IndicadorIPNClienteDTO();
            indicadorIPNCliente.setFiCeco((rs.getObject("FICECO") == null ? null : rs.getBigDecimal("FICECO").intValue()));
            indicadorIPNCliente.setFcCeco(rs.getString("FCDESCECO"));
            indicadorIPNCliente.setFiQ((rs.getObject("FIQ") == null ? null : rs.getBigDecimal("FIQ").intValue()));
            indicadorIPNCliente.setFcQ(rs.getString("FCQ"));
            indicadorIPNCliente.setFiAnio((rs.getObject("FIANIO") == null ? null : rs.getBigDecimal("FIANIO").intValue()));
            indicadorIPNCliente.setFiPromotor((rs.getObject("FIPROMOTOR") == null ? null : rs.getBigDecimal("FIPROMOTOR").intValue()));
            indicadorIPNCliente.setFiPasivo((rs.getObject("FIPASIVO") == null ? null : rs.getBigDecimal("FIPASIVO").intValue()));
            indicadorIPNCliente.setFiDetractivo((rs.getObject("FIDETRACTIVO") == null ? null : rs.getBigDecimal("FIDETRACTIVO").intValue()));
            indicadorIPNCliente.setFiIPN((rs.getObject("FIIPN") == null ? null : rs.getBigDecimal("FIIPN")));
            return indicadorIPNCliente;
        }
    }

    public class IndicadorEquipoIPNRowMapper implements RowMapper<IndicadorIPNEquipoDTO> {

        private IndicadorIPNEquipoDTO indicadorIPNEquipo;

        @Override
        public IndicadorIPNEquipoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            indicadorIPNEquipo = new IndicadorIPNEquipoDTO();
            indicadorIPNEquipo.setFiCeco((rs.getObject("FICECO") == null ? null : rs.getBigDecimal("FICECO").intValue()));
            indicadorIPNEquipo.setFcCeco(rs.getString("FCDESCECO"));
            indicadorIPNEquipo.setFiQ((rs.getObject("FIQ") == null ? null : rs.getBigDecimal("FIQ").intValue()));
            indicadorIPNEquipo.setFcQ(rs.getString("FCQ"));
            indicadorIPNEquipo.setFiAnio((rs.getObject("FIANIO") == null ? null : rs.getBigDecimal("FIANIO").intValue()));
            indicadorIPNEquipo.setFiPromotor((rs.getObject("FIPROMOTOR") == null ? null : rs.getBigDecimal("FIPROMOTOR").intValue()));
            indicadorIPNEquipo.setFiPasivo((rs.getObject("FIPASIVO") == null ? null : rs.getBigDecimal("FIPASIVO").intValue()));
            indicadorIPNEquipo.setFiDetractivo((rs.getObject("FIDETRACTIVO") == null ? null : rs.getBigDecimal("FIDETRACTIVO").intValue()));
            indicadorIPNEquipo.setFiIPN((rs.getObject("FIIPN") == null ? null : rs.getBigDecimal("FIIPN")));
            return indicadorIPNEquipo;
        }
    }

}
