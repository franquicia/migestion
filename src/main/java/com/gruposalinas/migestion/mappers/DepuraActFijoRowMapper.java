package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.DepuraActFijoDTO;

public class DepuraActFijoRowMapper implements RowMapper<DepuraActFijoDTO> {

    @Override
    public DepuraActFijoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        DepuraActFijoDTO dep = new DepuraActFijoDTO();

        dep.setIdDepAct(rs.getInt("FIID_DEPACTFIJO"));
        dep.setIdusuario(rs.getInt("FIID_USUARIO"));
        dep.setCeco(rs.getString("FCID_CECO"));
        dep.setUbicacion(rs.getString("FCUBICACION"));
        dep.setInvolucrados(rs.getString("FCINVOLUCRADOS"));
        dep.setRazonsoc(rs.getString("FCRAZONSOCIAL"));
        dep.setRfc(rs.getString("FCRFC"));
        dep.setDomicilio(rs.getString("FCDOMICILIO"));
        dep.setResponsable(rs.getString("FCNOMBRESPONSA"));
        dep.setTestigo(rs.getString("FCTESTIGO"));
        dep.setFecha(rs.getString("FECHA_HORA"));
        dep.setPrecio(rs.getDouble("FCPRECIO"));
        dep.setIdactivofijo(rs.getInt("FIID_ACTFIJO"));
        dep.setDescripcion(rs.getString("FCDESCRIPCION"));
        dep.setSerie(rs.getString("FCSERIE"));
        dep.setMarca(rs.getString("FCMARCA"));
        dep.setPlaca(rs.getString("FCPLACA"));

        return dep;
    }

}
