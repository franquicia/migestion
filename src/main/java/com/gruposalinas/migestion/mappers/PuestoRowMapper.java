package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.PuestoDTO;

public class PuestoRowMapper implements RowMapper<PuestoDTO> {

    @Override
    public PuestoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PuestoDTO puesto = new PuestoDTO();

        puesto.setIdPuesto(rs.getInt("FIID_PUESTO"));
        puesto.setDescripcion(rs.getString("FCDESCRIPCION"));

        return puesto;
    }

}
