package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;

public class ProgLimpiezaRowMapper implements RowMapper<ProgLimpiezaDTO> {

    @Override
    public ProgLimpiezaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProgLimpiezaDTO pl = new ProgLimpiezaDTO();

        pl.setIdProg(rs.getInt("FIID_PROGLIMP"));
        pl.setIdUsuario(rs.getInt("FIID_USUARIO"));
        pl.setCeco(rs.getString("FCID_CECO"));
        pl.setPeriodo(rs.getString("FCPERIODO"));

        //pl.setBandera(rs.getInt("BANDERA"));
        return pl;
    }

}
