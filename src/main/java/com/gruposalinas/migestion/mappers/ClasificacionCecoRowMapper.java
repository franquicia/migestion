package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ClasificacionCecoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ClasificacionCecoRowMapper implements RowMapper<ClasificacionCecoDTO> {

    @Override
    public ClasificacionCecoDTO mapRow(ResultSet rs, int arg1) throws SQLException {
        ClasificacionCecoDTO clasificacionCecoDTO = new ClasificacionCecoDTO();

        clasificacionCecoDTO.setCeco(rs.getString("FCIDCECO"));
        clasificacionCecoDTO.setCecoPadre(rs.getString("FIIDCC_PADRE"));
        clasificacionCecoDTO.setClasificacion(rs.getInt("CLASIFICACION"));
        clasificacionCecoDTO.setTipoGeografia(rs.getString("FITIPOGEO"));
        clasificacionCecoDTO.setNivel(rs.getString("FINIVEL"));

        return clasificacionCecoDTO;
    }

}
