package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteCheckPorcentaje implements RowMapper<ReporteChecklistDTO> {

    @Override
    public ReporteChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ReporteChecklistDTO checkDTO = new ReporteChecklistDTO();

        checkDTO.setConteo(rs.getInt("PORCENTAJE"));

        return checkDTO;
    }
}
