package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ParametroDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ParametroRowMapper implements RowMapper<ParametroDTO> {

    public ParametroDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ParametroDTO parametroDTO = new ParametroDTO();

        parametroDTO.setClave(rs.getString("FCCVE_PARAM"));
        parametroDTO.setValor(rs.getString("FCVALOR"));
        parametroDTO.setActivo(rs.getInt("FIACTIVO"));

        return parametroDTO;
    }
}
