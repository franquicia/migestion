package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ProveedoresDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ProveedoresRowMapper implements RowMapper<ProveedoresDTO> {

    @Override
    public ProveedoresDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProveedoresDTO proveedores = new ProveedoresDTO();

        proveedores.setMenu(rs.getString("FCMENU"));
        proveedores.setIdProveedor(rs.getInt("FIIDPROVEEDOR"));
        proveedores.setNombreCorto(rs.getString("FCNOMBRECORTO"));
        proveedores.setRazonSocial(rs.getString("FCRAZONSOCIAL"));
        proveedores.setStatus(rs.getString("FCSTATUS"));

        try {
            proveedores.setTipoProveedor(rs.getInt("FCTIPOPROVEEDOR"));
        } catch (Exception e) {
            proveedores.setTipoProveedor(0);
        }

        return proveedores;
    }

}
