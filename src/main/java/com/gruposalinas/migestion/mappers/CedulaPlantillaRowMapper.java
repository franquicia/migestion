package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.CedulaDTO;

public class CedulaPlantillaRowMapper implements RowMapper<CedulaDTO> {

    @Override
    public CedulaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CedulaDTO ced = new CedulaDTO();

        ced.setIdUsuario(rs.getInt("FIID_USUARIO"));
        ced.setNombre(rs.getString("USUARIO"));
        ced.setIdSuc(rs.getInt("CECOID"));
        ced.setDescPuesto(rs.getString("FCDESCRIPCION"));
        ced.setPeriodo(rs.getString("FCPERIODO"));
        ced.setFecha(rs.getString("FCFECHA"));
        ced.setBandera(rs.getInt("BANDERA"));
        ced.setRutafoto(rs.getString("FCRUTAFOTO"));

        return ced;
    }

}
