package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ReporteDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteRowMapper implements RowMapper<ReporteDTO> {

    public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ReporteDTO reporteDTO = new ReporteDTO();

        reporteDTO.setIdReporte(rs.getInt("FIID_REPORTE"));
        reporteDTO.setNombreReporte(rs.getString("FCNOMBRE_REP"));

        return reporteDTO;

    }

}
