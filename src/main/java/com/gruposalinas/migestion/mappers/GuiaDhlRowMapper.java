package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.GuiaDHLDTO;

public class GuiaDhlRowMapper implements RowMapper<GuiaDHLDTO> {

    @Override
    public GuiaDHLDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        GuiaDHLDTO guia = new GuiaDHLDTO();

        guia.setIdGuia(rs.getInt("FIID_GUIA"));
        guia.setIdEvid(rs.getInt("FIID_EVIDHL"));
        guia.setIdUsuario(rs.getInt("FIID_USUARIO"));
        guia.setCeco(rs.getString("FCID_CECO"));
        guia.setRuta(rs.getString("FCRUTA"));
        guia.setNombreArch(rs.getString("FC_NOMBREARC"));
        guia.setPeriodo(rs.getString("FCPERIODO"));
        guia.setStatus(rs.getInt("FCSTATUS"));
        guia.setDescr(rs.getString("FDESCRIPCION"));

        return guia;
    }

}
