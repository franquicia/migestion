package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.FotosAntesDespuesDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FotosAntesDespuesRowMapper2 implements RowMapper<FotosAntesDespuesDTO> {

    @Override
    public FotosAntesDespuesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FotosAntesDespuesDTO fotos = new FotosAntesDespuesDTO();

        fotos.setIdEvento(rs.getInt("FIID_EVID"));
        fotos.setIdCeco(rs.getString("FCID_CECO"));
        fotos.setLugarAntes(rs.getString("FCLUGARANT"));
        fotos.setLugarDespues(rs.getString("FCLUGARDESP"));
        fotos.setIdEvidantes(rs.getString("FCID_EVIDANT"));
        fotos.setFechaAntes(rs.getString("FECHAANTES"));
        fotos.setFechaDespues(rs.getString("FECHADESPUES"));
        fotos.setIdEviddespues(rs.getString("FCID_EVIDDESP"));
        fotos.setPeriodo(rs.getString("FCPERIODO"));

        return fotos;
    }
}
