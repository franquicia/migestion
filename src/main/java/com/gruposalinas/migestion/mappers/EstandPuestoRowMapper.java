package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.EstandPuestoDTO;

public class EstandPuestoRowMapper implements RowMapper<EstandPuestoDTO> {

    @Override
    public EstandPuestoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        EstandPuestoDTO estp = new EstandPuestoDTO();

        estp.setIdPuesto(rs.getInt("FIID_ESTPUESTO"));
        estp.setIdUsuario(rs.getInt("FIID_USUARIO"));
        estp.setCeco(rs.getString("FCID_CECO"));
        estp.setMueble1caj1(rs.getString("FIID_M1C1"));
        estp.setMueble1caj2(rs.getString("FIID_M1C2"));
        estp.setMueble1caj3(rs.getString("FIID_M1C3"));
        estp.setMueble2caj1(rs.getString("FIID_M2C1"));
        estp.setMueble2caj2(rs.getString("FIID_M2C2"));
        estp.setPeriodo(rs.getString("FCPERIODO"));
        estp.setPuesto(rs.getInt("FCPUESTO"));

        return estp;
    }

}
