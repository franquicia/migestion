package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.CatalogoVistaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CatalogoVistaRowMapper implements RowMapper<CatalogoVistaDTO> {

    @Override
    public CatalogoVistaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CatalogoVistaDTO catalogoVistaDTO = new CatalogoVistaDTO();

        catalogoVistaDTO.setIdCatVista(rs.getInt("FIID_CATVIS"));
        catalogoVistaDTO.setDescripcion(rs.getString("FCDESC_VIS"));
        catalogoVistaDTO.setIdModPadre(rs.getInt("FIID_MODPADRE"));;

        return catalogoVistaDTO;
    }

}
