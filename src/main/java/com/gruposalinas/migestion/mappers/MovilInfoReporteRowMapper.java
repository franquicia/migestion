package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.MovilinfReporteDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class MovilInfoReporteRowMapper implements RowMapper<MovilinfReporteDTO> {

    public MovilinfReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        MovilinfReporteDTO movilInfo = new MovilinfReporteDTO();

        //movilInfo.setMes(rs.getString("MES"));
        //movilInfo.setAnio(rs.getString("ANIO"));
        movilInfo.setIdUsuario(rs.getInt("FIID_USUARIO"));
        movilInfo.setNombreUsuario(rs.getString("FCNOMBRE"));
        movilInfo.setIdCeco(rs.getString("FCID_CECO"));
        movilInfo.setNombreCeco(rs.getString("FCNOMBRE_CECO"));
        movilInfo.setNumSesion(rs.getInt("TOTAL_SESIONES"));
        movilInfo.setFechaUltimaSesion(rs.getString("ULTIMA_SESION"));

        return movilInfo;
    }
}
