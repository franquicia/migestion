package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.FilasDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FilasRowMapper implements RowMapper<FilasDTO> {

    @Override
    public FilasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FilasDTO filas = new FilasDTO();

        filas.setIdFila(rs.getInt("FIIDFILA"));
        filas.setNumEconomico(rs.getInt("FIIDNUMECONOM"));
        filas.setIdGrupo(rs.getInt("FIIDGRUPO"));

        return filas;
    }

}
