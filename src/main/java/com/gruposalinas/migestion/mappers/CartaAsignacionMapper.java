package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.CartaAsignacionAFDTO;

public class CartaAsignacionMapper implements RowMapper<CartaAsignacionAFDTO> {

    @Override
    public CartaAsignacionAFDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CartaAsignacionAFDTO carta = new CartaAsignacionAFDTO();

        carta.setIdCarta(rs.getInt("FIID_CAARTA"));
        carta.setIdUsuario(rs.getInt("FIID_USUARIO"));
        carta.setCeco(rs.getString("FCID_CECO"));
        carta.setNombreArc(rs.getString("FCNOMBRE"));
        carta.setRuta(rs.getString("FCRUTA"));
        carta.setObserv(rs.getString("FCOBSERVACION"));
        carta.setPerido(rs.getString("FCPERIODO"));
        carta.setStatus(rs.getInt("FCACTIVO"));
        return carta;
    }

}
