package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.FormatoMetodo7sDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FormatoMetodo7sPlantillaRowMapper implements RowMapper<FormatoMetodo7sDTO> {

    @Override
    public FormatoMetodo7sDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FormatoMetodo7sDTO formato = new FormatoMetodo7sDTO();

        //formato.setPeriodo(rs.getString("FIID_REV7S"));
        formato.setIdSucursal(rs.getInt("FCID_CECO"));
        formato.setNombre(rs.getString("USUARIO"));
        formato.setPeriodo(rs.getString("FCPERIODO"));
        formato.setIdusuario(rs.getInt("FIID_USUARIO"));
        formato.setBandera(rs.getInt("BANDERA"));
        formato.setPuesto(rs.getString("FCDESCRIPCION"));
        //formato.setPreg1(rs.getInt("FCID_PREG1"));
        //formato.setPreg2(rs.getInt("FCID_PREG2"));
        //formato.setPreg3(rs.getInt("FCID_PREG3"));

        return formato;
    }

}
