package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TokensAsesoresDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TokensAsesoresRowMapper implements RowMapper<TokensAsesoresDTO> {

    @Override
    public TokensAsesoresDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TokensAsesoresDTO tokensAsesoresDTO = new TokensAsesoresDTO();

        tokensAsesoresDTO.setToken(rs.getString("FCTOKEN"));
        tokensAsesoresDTO.setNombreUsuario(rs.getString("FCNOMBRE"));
        tokensAsesoresDTO.setCompromisoPerso(rs.getString("FICOMPROMISO_P"));
        tokensAsesoresDTO.setAvancePerso(rs.getString("FIAVANCE_P"));
        tokensAsesoresDTO.setCompromisoActi(rs.getString("FICOMPROMISO_A"));
        tokensAsesoresDTO.setAvanceActi(rs.getString("FIAVANCE_A"));

        return tokensAsesoresDTO;
    }

}
