package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TablasPasoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PasoSucursalRowMapper implements RowMapper<TablasPasoDTO> {

    public TablasPasoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TablasPasoDTO sucursalDTO = new TablasPasoDTO();

        sucursalDTO.setFisucursal_id(rs.getString("FISUCURSAL_ID"));
        sucursalDTO.setFipais(rs.getString("FIPAIS"));
        sucursalDTO.setFicanal(rs.getString("FICANAL"));
        sucursalDTO.setFisucursal(rs.getString("FISUCURSAL"));
        sucursalDTO.setFcnombreccSuc(rs.getString("FCNOMBRECC"));
        sucursalDTO.setFclongitude(rs.getString("FCLONGITUDE"));
        sucursalDTO.setFclatitude(rs.getString("FCLATITUDE"));

        return sucursalDTO;
    }

}
