package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TareaActDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteTareaRowMapper implements RowMapper<TareaActDTO> {

    @Override
    public TareaActDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TareaActDTO tareaDTO = new TareaActDTO();

        tareaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        tareaDTO.setIdTarea(rs.getInt("FIID_TAREA"));
        tareaDTO.setNombreTarea(rs.getString("FCNOM_TAREA"));

        return tareaDTO;
    }
}
