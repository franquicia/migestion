package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TareaActDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UsuarioTareaRowMapper implements RowMapper<TareaActDTO> {

    @Override
    public TareaActDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TareaActDTO tareaDTO = new TareaActDTO();

        tareaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        tareaDTO.setNombreUsuario(rs.getString("FCNOMBRE"));
        tareaDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
        tareaDTO.setDesPuesto(rs.getString("FCDESCRIPCION"));
        tareaDTO.setPorcentajeUsua(rs.getInt("PORCENTAJE"));

        return tareaDTO;
    }

}
