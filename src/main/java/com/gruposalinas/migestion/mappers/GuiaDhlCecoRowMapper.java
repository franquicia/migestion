package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.GuiaDHLDTO;

public class GuiaDhlCecoRowMapper implements RowMapper<GuiaDHLDTO> {

    @Override
    public GuiaDHLDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        GuiaDHLDTO guia = new GuiaDHLDTO();

        guia.setIdGuia(rs.getInt("FIID_GUIA"));
        guia.setIdUsuario(rs.getInt("FIID_USUARIO"));
        guia.setCeco(rs.getString("FCID_CECO"));
        guia.setPeriodo(rs.getString("FCPERIODO"));
        guia.setStatus(rs.getInt("FCSTATUS"));

        return guia;
    }

}
