package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FechaTerCheckRowMapper implements RowMapper<ReporteChecklistDTO> {

    @Override
    public ReporteChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ReporteChecklistDTO checkDTO = new ReporteChecklistDTO();

        checkDTO.setFechaTermino(rs.getString("FECHA_TERMINO"));

        return checkDTO;
    }

}
