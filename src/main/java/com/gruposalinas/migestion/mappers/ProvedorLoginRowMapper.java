package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.ProveedorLoginDTO;

public class ProvedorLoginRowMapper implements RowMapper<ProveedorLoginDTO> {

    @Override
    public ProveedorLoginDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProveedorLoginDTO log = new ProveedorLoginDTO();

        log.setIdpass(rs.getInt("FIID_PASSW"));
        log.setIdProveedor(rs.getInt("FIID_PROVEEDOR"));
        log.setPassw(rs.getString("FCPASSWORD"));
        log.setPeriodo(rs.getString("FCPERIODO"));

        return log;
    }

}
