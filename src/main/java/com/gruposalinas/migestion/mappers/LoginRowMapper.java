package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.LoginDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class LoginRowMapper implements RowMapper<LoginDTO> {

    public LoginDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        LoginDTO loginDTO = new LoginDTO();

        loginDTO.setNoEmpleado(rs.getInt("FIID_USUARIO"));
        loginDTO.setNombreEmpelado(rs.getString("FCNOMBRE"));
        loginDTO.setIdCeco("" + (rs.getInt("FCID_CECO")));
        loginDTO.setDescCeco(rs.getString("FCNOM_CECO"));
        loginDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
        loginDTO.setDescPuesto(rs.getString("FCDESCRIPCION"));
        loginDTO.setIdperfil(rs.getInt("FIID_PERFIL"));

        return loginDTO;
    }

}
