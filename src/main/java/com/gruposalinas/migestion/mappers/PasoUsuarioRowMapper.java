package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TablasPasoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PasoUsuarioRowMapper implements RowMapper<TablasPasoDTO> {

    public TablasPasoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TablasPasoDTO usuarioADTO = new TablasPasoDTO();

        usuarioADTO.setFiempleado(rs.getString("FIEMPLEADO"));
        usuarioADTO.setFcnombre(rs.getString("FCNOMBRE"));
        usuarioADTO.setFicc(rs.getString("FICC"));
        usuarioADTO.setFifuncion(rs.getString("FIFUNCION"));
        usuarioADTO.setFipuesto(rs.getString("FIPUESTO"));
        usuarioADTO.setFcdescfun(rs.getString("FCDESCFUN"));
        usuarioADTO.setFcdescpuesto(rs.getString("FCDESCPUESTO"));
        usuarioADTO.setFcpaisUsuario(rs.getString("FCPAIS"));
        usuarioADTO.setFifechabaja(rs.getString("FIFECHABAJA"));
        usuarioADTO.setFiempleadorep(rs.getString("FIEMPLEADOREP"));
        usuarioADTO.setFiccemprep(rs.getString("FICCEMPREP"));
        usuarioADTO.setFidisponible(rs.getString("FIDISPONIBLE"));

        return usuarioADTO;
    }

}
