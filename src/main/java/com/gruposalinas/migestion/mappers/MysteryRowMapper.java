package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.MysteryDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class MysteryRowMapper implements RowMapper<MysteryDTO> {

    @Override
    public MysteryDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        MysteryDTO misteryDTO = new MysteryDTO();

        misteryDTO.setIdCeco(rs.getString("FCID_CECO"));
        misteryDTO.setNombreCeco(rs.getString("FCNOMBRE"));
        misteryDTO.setNivel(rs.getInt("NIVEL"));

        return misteryDTO;
    }
}
