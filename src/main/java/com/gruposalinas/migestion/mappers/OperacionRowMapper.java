package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TipificacionDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class OperacionRowMapper implements RowMapper<TipificacionDTO> {

    @Override
    public TipificacionDTO mapRow(ResultSet rs, int cont) throws SQLException {

        TipificacionDTO tipografiaDTO = new TipificacionDTO();
        tipografiaDTO.setIdOpeProd(rs.getInt("FIIDOPERACION"));
        tipografiaDTO.setDesc(rs.getString("FCDESC"));
        tipografiaDTO.setPadre(rs.getInt("FIDPADRE"));

        return tipografiaDTO;
    }

}
