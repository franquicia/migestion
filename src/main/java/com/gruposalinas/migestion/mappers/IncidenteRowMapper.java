package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.IncidentesDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class IncidenteRowMapper implements RowMapper<IncidentesDTO> {

    public IncidentesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        IncidentesDTO indicenteDTO = new IncidentesDTO();

        indicenteDTO.setIdIncidente(rs.getInt("FIID_INCIDENTE"));
        indicenteDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));

        return indicenteDTO;

    }

}
