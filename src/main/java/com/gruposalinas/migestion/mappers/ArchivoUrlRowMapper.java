package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.ArchivosUrlDTO;

public class ArchivoUrlRowMapper implements RowMapper<ArchivosUrlDTO> {

    @Override
    public ArchivosUrlDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ArchivosUrlDTO au = new ArchivosUrlDTO();

        au.setIdArch(rs.getInt("FIID_ARCHIVO"));
        au.setNombre(rs.getString("FCNOMBRE_ARC"));
        au.setRuta(rs.getString("FCRUTA"));
        au.setVersion(rs.getString("FCVERSION"));

        return au;
    }

}
