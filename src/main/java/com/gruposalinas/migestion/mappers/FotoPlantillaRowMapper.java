package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.FotoPlantillaDTO;

public class FotoPlantillaRowMapper implements RowMapper<FotoPlantillaDTO> {

    @Override
    public FotoPlantillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FotoPlantillaDTO fp = new FotoPlantillaDTO();

        fp.setIdPlanti(rs.getInt("FIID_FOTOPLANTI"));
        fp.setCeco(rs.getString("FCID_CECO"));
        fp.setUsuario(rs.getInt("FIID_USUARIO"));
        fp.setRuta(rs.getString("FCRUTAFOTO"));
        fp.setPeriodo(rs.getString("FCPERIODO"));

        return fp;
    }

}
