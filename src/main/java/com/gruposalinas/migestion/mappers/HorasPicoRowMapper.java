package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.HorasPicoDTO;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class HorasPicoRowMapper implements RowMapper<HorasPicoDTO> {

    private HorasPicoDTO horasPico;
    private String type;

    public HorasPicoRowMapper(String type) {
        this.type = type;
    }

    @Override
    public HorasPicoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        horasPico = new HorasPicoDTO();
        if (type.equals("HorasPico")) {
            horasPico.setFiSucursal(rs.getObject("FISUCURSAL") == null ? null : rs.getBigDecimal("FISUCURSAL").intValue());
            horasPico.setFcSucursal(rs.getString("FCSUCURSAL"));
            horasPico.setFiPais(rs.getObject("FIPAIS") == null ? null : rs.getBigDecimal("FIPAIS").intValue());
            horasPico.setFiCanal(rs.getObject("FICANAL") == null ? null : rs.getBigDecimal("FICANAL").intValue());
            horasPico.setFcCanal(rs.getString("FCCANAL"));
            horasPico.setFiAnio(rs.getObject("FIANIO") == null ? null : rs.getBigDecimal("FIANIO").intValue());
            horasPico.setFiSemana(rs.getObject("FISEMANA") == null ? null : rs.getBigDecimal("FISEMANA").intValue());
            horasPico.setFiHorasMeta(rs.getObject("FIHORASMETA") == null ? null : rs.getBigDecimal("FIHORASMETA").intValue());
            horasPico.setFiHorasCump(rs.getObject("FIHORASCUMP") == null ? null : rs.getBigDecimal("FIHORASCUMP").intValue());
            horasPico.setFiHorasPorCump((BigDecimal) rs.getObject("FIPORHRCUMP"));
            horasPico.setFcUsuarioMod(rs.getString("FCUSUARIO_MOD"));
            horasPico.setFdFechaMod((Timestamp) rs.getObject("FDFECHA_MOD"));
        } else {
            horasPico.setFiSucursal(rs.getObject("FISUCURSAL") == null ? null : rs.getBigDecimal("FISUCURSAL").intValue());
            horasPico.setFcSucursal(rs.getString("FCSUCURSAL"));
            horasPico.setFiPais(rs.getObject("FIPAIS") == null ? null : rs.getBigDecimal("FIPAIS").intValue());
            horasPico.setFiCanal(rs.getObject("FICANAL") == null ? null : rs.getBigDecimal("FICANAL").intValue());
            horasPico.setFcCanal(rs.getString("FCCANAL"));
            horasPico.setFiSemana(rs.getObject("FISEMANA") == null ? null : rs.getBigDecimal("FISEMANA").intValue());
            horasPico.setFiHorasMeta(rs.getObject("FIHORASMETA") == null ? null : rs.getBigDecimal("FIHORASMETA").intValue());
            horasPico.setFiHorasCump(rs.getObject("FIHORASCUMP") == null ? null : rs.getBigDecimal("FIHORASCUMP").intValue());
            horasPico.setFiHorasPorCump((BigDecimal) rs.getObject("FIPORHRCUMP"));
            horasPico.setFcUsuarioMod(rs.getString("FCUSUARIO_MOD"));
            horasPico.setFdFechaMod((Timestamp) rs.getObject("FDFECHA_MOD"));
        }
        return horasPico;
    }
}
