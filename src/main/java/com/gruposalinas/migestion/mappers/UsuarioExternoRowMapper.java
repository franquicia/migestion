package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ExternoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UsuarioExternoRowMapper implements RowMapper<ExternoDTO> {

    public ExternoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ExternoDTO usuarioExt = new ExternoDTO();

        usuarioExt.setNumEmpleado(rs.getInt("FIEMPLEADO_NUM"));
        usuarioExt.setNombre(rs.getString("FCNOMBRE"));
        usuarioExt.setDireccion(rs.getString("FCDIRECCION"));
        usuarioExt.setNumCasa(rs.getInt("FCNUM_CASA"));
        usuarioExt.setNumCelular(rs.getInt("FCNUM_CELULAR"));
        usuarioExt.setRFCEmpleado(rs.getString("FCRFC_EMPLEADO"));
        usuarioExt.setCencosNum(rs.getString("FCCENCOS_NUM"));
        usuarioExt.setPuesto(rs.getInt("FCPUESTO"));
        usuarioExt.setFechaIngreso(rs.getString("FCFECHA_INGRESO"));
        usuarioExt.setFechaBaja(rs.getString("FCFECHA_BAJA"));
        usuarioExt.setFuncion(rs.getString("FCFUNCION"));
        usuarioExt.setUsuarioID(rs.getInt("FIUSUARIO_ID"));
        usuarioExt.setaPaterno(rs.getString("FCAPPATERNO"));
        usuarioExt.setaMaterno(rs.getString("FCAPMATERNO"));
        usuarioExt.setEmpSituacion(rs.getInt("FIEMPLEADO_SIT"));
        usuarioExt.setHonorarios(rs.getInt("FIHONORARIOS"));
        usuarioExt.setEmail(rs.getString("FCEMAIL"));
        usuarioExt.setNumEmergencia(rs.getInt("FCNUM_EMERGENC"));
        usuarioExt.setPerEmergencia(rs.getString("FCPER_EMERGENC"));
        usuarioExt.setNumExt(rs.getInt("FCNUM_EXTENSION"));
        usuarioExt.setSueldo(rs.getString("FISUELDO"));
        usuarioExt.setHorarioIn(rs.getString("FCHORARIO_INI"));
        usuarioExt.setHorarioFin(rs.getString("FCHORARIO_FIN"));
        usuarioExt.setSemestre(rs.getInt("FISEMESTRE"));
        usuarioExt.setActividades(rs.getString("FCACTIVIDADES"));
        usuarioExt.setLiberacion(rs.getString("FDLIBERACION"));
        usuarioExt.setElaboracion(rs.getString("FDELABORACION"));
        usuarioExt.setLlave(rs.getInt("FILLAVE"));
        usuarioExt.setRed(rs.getInt("FIRED"));
        usuarioExt.setLotus(rs.getInt("FILOTUS"));
        usuarioExt.setCredencial(rs.getInt("FICREDENCIAL"));
        usuarioExt.setFolioDataSec(rs.getInt("FIFOLIODATASEC"));
        usuarioExt.setPais(rs.getString("FCPAIS"));
        usuarioExt.setNombreCompleto(rs.getString("FCNOMB_COMPLETO"));
        usuarioExt.setAsistencia(rs.getString("FCASISTENCIA"));
        usuarioExt.setCurp(rs.getString("FCCURP"));

        return usuarioExt;
    }
}
