package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;

public class CatalogoMinuciasEviRowMapper implements RowMapper<CatalogoMinuciasDTO> {

    @Override
    public CatalogoMinuciasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CatalogoMinuciasDTO cm = new CatalogoMinuciasDTO();
        cm.setCeco(rs.getString("FCID_CECO"));
        cm.setIdevi(rs.getInt("FIID_MINUCIAEVI"));
        cm.setNombrearc(rs.getString("FC_NOMBREARC"));
        cm.setRuta(rs.getString("FCRUTA"));
        cm.setObservac(rs.getString("FCDESCRIPCION"));

        return cm;
    }

}
