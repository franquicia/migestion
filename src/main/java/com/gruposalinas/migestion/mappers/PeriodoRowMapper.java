package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.PeriodoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PeriodoRowMapper implements RowMapper<PeriodoDTO> {

    public PeriodoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PeriodoDTO periodoDTO = new PeriodoDTO();

        periodoDTO.setIdPeriodo(rs.getInt("FIID_PERIODO"));
        periodoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return periodoDTO;

    }
}
