package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.PeticionDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PeticionRowMapper implements RowMapper<PeticionDTO> {

    @Override
    public PeticionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PeticionDTO peticionDto = new PeticionDTO();

        peticionDto.setOrigenPeticion(rs.getString("FCORIGEN"));
        peticionDto.setFechaPeticion(rs.getString("FCFECHA"));
        peticionDto.setConteoPeticion(rs.getInt("FIPETICIONES"));

        return peticionDto;
    }

}
