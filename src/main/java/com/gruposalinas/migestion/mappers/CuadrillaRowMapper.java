package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.CuadrillaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CuadrillaRowMapper implements RowMapper<CuadrillaDTO> {

    @Override
    public CuadrillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CuadrillaDTO cuad = new CuadrillaDTO();

        cuad.setIdCuadrilla(rs.getInt("FIID_CUADRILLA"));
        cuad.setIdProveedor(rs.getInt("FIID_PROVEEDOR"));
        cuad.setZona(rs.getInt("FIID_ZONA"));
        cuad.setLiderCuad(rs.getString("FCLIDER_CUAD"));
        cuad.setCorreo(rs.getString("FCCORREO"));
        cuad.setSegundo(rs.getString("FCSEG_CARGO"));
        cuad.setPassw(rs.getString("FCPASSW"));

        return cuad;
    }

}
