package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ActivoFijoRowMapper implements RowMapper<ActivoFijoDTO> {

    @Override
    public ActivoFijoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ActivoFijoDTO activoFijo = new ActivoFijoDTO();

        activoFijo.setIdActivoFijo(rs.getInt("FIID_ACTFIJO"));
        activoFijo.setDepActivoFijo(rs.getInt("FIID_DEPACTFIJO"));
        activoFijo.setPlaca(rs.getString("FCPLACA"));
        activoFijo.setDescripcion(rs.getString("FCDESCRIPCION"));
        activoFijo.setCeco(rs.getString("FCID_CECO"));
        activoFijo.setMarca(rs.getString("FCMARCA"));
        activoFijo.setSerie(rs.getString("FCSERIE"));

        return activoFijo;
    }
}
