package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.ReporteMensualMovilDTO;

public class ReporteMensualMovilMapper implements RowMapper<ReporteMensualMovilDTO> {

    private int idUsuario;
    private String nombreUsuario;
    private String idCeco;
    private String nombreCeco;
    private String idCecoSuperior;
    private String nombreCecoSuperior;
    private int totalSesiones;
    private String fechaUltimaSesion;

    public ReporteMensualMovilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ReporteMensualMovilDTO reporte = new ReporteMensualMovilDTO();

        reporte.setIdUsuario(rs.getInt("FIID_USUARIO"));
        reporte.setNombreUsuario(rs.getString("FCNOMBRE"));
        reporte.setIdCeco(rs.getString("FCID_CECO"));
        reporte.setNombreCeco(rs.getString("NOMBRE_CECO"));
        reporte.setIdCecoSuperior(rs.getString("FICECO_SUPERIOR"));
        reporte.setNombreCecoSuperior(rs.getString("FCNOMBRE_CECO_SUPERIOR"));
        reporte.setTotalSesiones(rs.getInt("SESIONES"));
        reporte.setFechaUltimaSesion(rs.getString("ULTIMA_SESION"));

        reporte.setIdPuesto(rs.getInt("FIID_PUESTO"));
        reporte.setDesPuesto(rs.getString("FCDESCRIPCION_PUESTO"));

        return reporte;
    }

}
