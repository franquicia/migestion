package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.AgendaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AgendaRowMapper implements RowMapper<AgendaDTO> {

    @Override
    public AgendaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        AgendaDTO agendaDTO = new AgendaDTO();

        agendaDTO.setIdAgenda(rs.getInt("FIID_AGENDA"));
        agendaDTO.setIdGoogle(rs.getString("FIID_GOOGLE"));
        agendaDTO.setTitulo(rs.getString("FCTITULO"));
        agendaDTO.setEvento(rs.getString("FCEVENTO"));
        agendaDTO.setPeriodo(rs.getInt("FIPERIODO"));
        agendaDTO.setAlerta(rs.getInt("FIALERTA"));
        agendaDTO.setHorarioIni(rs.getString("FCHORARIO_INI"));
        agendaDTO.setHorarioFin(rs.getString("FCHORARIO_FIN"));
        agendaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));

        return agendaDTO;
    }

}
