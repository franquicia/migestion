package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.BibliotecaDigitalDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class BibliotecaDigitalRowMapper implements RowMapper<BibliotecaDigitalDTO> {

    @Override
    public BibliotecaDigitalDTO mapRow(ResultSet rs, int cont) throws SQLException {

        BibliotecaDigitalDTO bibliotecaDigitalDTO = new BibliotecaDigitalDTO();

        bibliotecaDigitalDTO.setIdCategoria(rs.getInt("FIID_CATEGORIA"));
        bibliotecaDigitalDTO.setDescripcionCategoria(rs.getString("FCDESCRIPCION"));
        //bibliotecaDigitalDTO.setRutaIconoCategoria(rs.getString("FCRUTA_ICONO"));
        //bibliotecaDigitalDTO.setNombreIconoCategoria(rs.getString("FCNOMBRE_ICONO"));
        bibliotecaDigitalDTO.setIdDetalleCategoria(rs.getInt("FIID_ITEM"));
        bibliotecaDigitalDTO.setDescripcionDetalleCategoria(rs.getString("DESC_ITEM"));
        bibliotecaDigitalDTO.setTipoCategoria(rs.getString("FCTIPO"));
        bibliotecaDigitalDTO.setIdArchivo(rs.getInt("FIID_ARCHIVO"));
        bibliotecaDigitalDTO.setRutaArchivo(rs.getString("FCRUTA_ARCHIVO"));
        bibliotecaDigitalDTO.setNombreArchivo(rs.getString("FCNOMBRE"));
        bibliotecaDigitalDTO.setOrdenCategoria(rs.getInt("FIORDEN"));
        bibliotecaDigitalDTO.setOrdenItem(rs.getInt("ORDEN_ITEM"));

        return bibliotecaDigitalDTO;
    }

}
