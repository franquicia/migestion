package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TelSucursalDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TelSucursalRowMapper implements RowMapper<TelSucursalDTO> {

    public TelSucursalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TelSucursalDTO telefonoSucursal = new TelSucursalDTO();

        telefonoSucursal.setIdTelefono(rs.getInt("FITEL_SUC_ID"));
        telefonoSucursal.setIdCeco(rs.getString("FCNUMECO"));
        telefonoSucursal.setTelefono(rs.getString("FCTELEFONO"));
        telefonoSucursal.setProveedor(rs.getString("FCPROVEEDOR"));
        telefonoSucursal.setEstatus(rs.getString("FCSTATUS_DISTRI"));

        return telefonoSucursal;
    }
}
