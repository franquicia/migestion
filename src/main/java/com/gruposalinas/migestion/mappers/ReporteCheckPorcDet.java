package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteCheckPorcDet implements RowMapper<ReporteChecklistDTO> {

    @Override
    public ReporteChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ReporteChecklistDTO checkDTO = new ReporteChecklistDTO();

        checkDTO.setIdCeco(rs.getInt("FCID_CECO"));
        checkDTO.setNombreCeco(rs.getString("FCNOMBRE"));
        checkDTO.setConteo(rs.getInt("PORCENTAJE"));
        checkDTO.setNivel(rs.getInt("NIVEL"));
        checkDTO.setFechaTermino(rs.getString("FDTERMINO"));

        return checkDTO;
    }

}
