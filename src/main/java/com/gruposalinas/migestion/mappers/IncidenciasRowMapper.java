package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.IncidenciasDTO;

public class IncidenciasRowMapper implements RowMapper<IncidenciasDTO> {

    @Override
    public IncidenciasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        IncidenciasDTO incidencia = new IncidenciasDTO();

        incidencia.setIdTipo(rs.getInt("FIIDTIPO"));
        incidencia.setServicio(rs.getString("FCTIPOSERVICIO"));
        incidencia.setUbicacion(rs.getString("FCUBICACION"));
        incidencia.setIncidencia(rs.getString("FCINCIDENCIA"));
        incidencia.setPlantilla(rs.getString("FCPLANTILLA"));
        incidencia.setStatus(rs.getString("FCSTATUS"));

        return incidencia;
    }

}
