package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteListaCheckRowMapper implements RowMapper<ReporteChecklistDTO> {

    @Override
    public ReporteChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ReporteChecklistDTO checkDTO = new ReporteChecklistDTO();

        checkDTO.setIdCheck(rs.getInt("FIID_CHECKLIST"));
        checkDTO.setNombreCheck(rs.getString("FCNOMBRE"));
        checkDTO.setIdGrupo(rs.getInt("FIID_GRUPO"));
        checkDTO.setIdOrden(rs.getInt("FIORDEN"));

        return checkDTO;
    }

}
