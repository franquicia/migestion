package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.PeriodoReporteDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PeriodoReporteRowMapper implements RowMapper<PeriodoReporteDTO> {

    public PeriodoReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PeriodoReporteDTO peridoReporteDTO = new PeriodoReporteDTO();

        peridoReporteDTO.setId_per_rep(rs.getInt("FIID_PER_REP"));
        peridoReporteDTO.setId_reporte(rs.getInt("FIID_REPORTE"));
        peridoReporteDTO.setId_periodo(rs.getInt("FIID_PERIODO"));
        peridoReporteDTO.setHorario(rs.getString("FCHORARIO"));

        return peridoReporteDTO;

    }
}
