package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.DepuraActFijoDTO;

public class DepuraActFijoEvidRowMapper implements RowMapper<DepuraActFijoDTO> {

    @Override
    public DepuraActFijoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        DepuraActFijoDTO dep = new DepuraActFijoDTO();

        dep.setIdEvi(rs.getInt("FIID_EVIDACT"));
        dep.setIdDepAct(rs.getInt("FIID_DEPACTFIJO"));
        dep.setCeco(rs.getString("FCID_CECO"));
        dep.setNombreArc(rs.getString("FCNOMBRE"));
        dep.setRuta(rs.getString("FCRUTA"));
        dep.setDesc(rs.getString("FCDESCRIPCION"));
        dep.setPeriodo(rs.getString("FCPERIODO"));
        return dep;
    }

}
