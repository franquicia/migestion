package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.AreaApoyoDTO;

public class AreaApoyoRowMapper implements RowMapper<AreaApoyoDTO> {

    @Override
    public AreaApoyoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        AreaApoyoDTO ap = new AreaApoyoDTO();

        ap.setIdArea(rs.getInt("FIID_AREAAPOY"));
        ap.setCeco(rs.getString("FCID_CECO"));
        ap.setIdUsuario(rs.getInt("FIID_USUARIO"));
        ap.setFolio(rs.getString("FCID_FOLIO"));
        ap.setAreaInv(rs.getString("FCAREA_INVOL"));
        ap.setPrioridad(rs.getInt("FCPRIORIDAD"));
        ap.setStatus(rs.getInt("FCSTATUS"));
        ap.setIdEvid(rs.getInt("FIID_EVID"));
        ap.setNombreArc(rs.getString("FC_NOMBREARC"));
        ap.setRuta(rs.getString("FCRUTA"));

        return ap;
    }

}
