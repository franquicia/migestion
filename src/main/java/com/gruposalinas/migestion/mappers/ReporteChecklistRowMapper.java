package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteChecklistRowMapper implements RowMapper<ReporteChecklistDTO> {

    @Override
    public ReporteChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ReporteChecklistDTO checkDTO = new ReporteChecklistDTO();

        checkDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        checkDTO.setIdCheck(rs.getInt("FIID_CHECKLIST"));
        checkDTO.setNombreCheck(rs.getString("FCNOMBRE"));

        return checkDTO;
    }

}
