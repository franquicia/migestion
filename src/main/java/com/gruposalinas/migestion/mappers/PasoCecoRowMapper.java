package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TablasPasoDTO;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PasoCecoRowMapper implements RowMapper<TablasPasoDTO> {

    public TablasPasoDTO mapRow(java.sql.ResultSet rs, int rowNum) throws SQLException {

        TablasPasoDTO cecoDTO = new TablasPasoDTO();

        cecoDTO.setFcccid(rs.getString("FCCCID"));
        cecoDTO.setFientidadid(rs.getString("FIENTIDADID"));
        cecoDTO.setFinum_economico(rs.getString("FINUM_ECONOMICO"));
        cecoDTO.setFcnombrecc(rs.getString("FCNOMBRECC"));
        cecoDTO.setFcnombre_ent(rs.getString("FCNOMBRE_ENT"));
        cecoDTO.setFcccid_padre(rs.getString("FCCCID_PADRE"));
        cecoDTO.setFientdid_padre(rs.getString("FIENTDID_PADRE"));
        cecoDTO.setFcnombrecc_pad(rs.getString("FCNOMBRECC_PAD"));
        cecoDTO.setFctipocanal(rs.getString("FCTIPOCANAL"));
        cecoDTO.setFcnombtipcanal(rs.getString("FCNOMBTIPCANAL"));
        cecoDTO.setFccanal(rs.getString("FCCANAL"));
        cecoDTO.setFistatusccid(rs.getString("FISTATUSCCID"));
        cecoDTO.setFcstatuscc(rs.getString("FCSTATUSCC"));
        cecoDTO.setFitipocc(rs.getString("FITIPOCC"));
        cecoDTO.setFiestadoid(rs.getString("FIESTADOID"));
        cecoDTO.setFcmunicipio(rs.getString("FCMUNICIPIO"));
        cecoDTO.setFctipooperacion(rs.getString("FCTIPOOPERACION"));
        cecoDTO.setFctiposucursal(rs.getString("FCTIPOSUCURSAL"));
        cecoDTO.setFcnombretiposuc(rs.getString("FCNOMBRETIPOSUC"));
        cecoDTO.setFccalle(rs.getString("FCCALLE"));
        cecoDTO.setFiresponsableid(rs.getString("FIRESPONSABLEID"));
        cecoDTO.setFcresponsable(rs.getString("FCRESPONSABLE"));
        cecoDTO.setFccp(rs.getString("FCCP"));
        cecoDTO.setFctelefonos(rs.getString("FCTELEFONOS"));
        cecoDTO.setFiclasif_sieid(rs.getString("FICLASIF_SIEID"));
        cecoDTO.setFcclasif_sie(rs.getString("FCCLASIF_SIE"));
        cecoDTO.setFigastoxnegid(rs.getString("FIGASTOXNEGID"));
        cecoDTO.setFcgastoxnegocio(rs.getString("FCGASTOXNEGOCIO"));
        cecoDTO.setFdapertura(rs.getString("FDAPERTURA"));
        cecoDTO.setFdcierre(rs.getString("FDCIERRE"));
        cecoDTO.setFipaisid(rs.getString("FIPAISID"));
        cecoDTO.setFcpais(rs.getString("FCPAIS"));
        cecoDTO.setFdcarga(rs.getString("FDCARGA"));

        return cecoDTO;

    }

}
