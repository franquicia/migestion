package com.gruposalinas.migestion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;

public class TicketCuadrillaRowMapper implements RowMapper<TicketCuadrillaDTO> {

    @Override
    public TicketCuadrillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TicketCuadrillaDTO tk = new TicketCuadrillaDTO();

        tk.setIdtkCuadri(rs.getInt("FIID_TKCUAD"));
        tk.setIdCuadrilla(rs.getInt("FIID_CUADRILLA"));
        tk.setIdProveedor(rs.getInt("FIID_PROVEEDOR"));
        tk.setTicket(rs.getString("FIID_TICKET"));
        tk.setZona(rs.getInt("FIID_ZONA"));
        tk.setLiderCua(rs.getString("FCLIDER_CUAD"));
        tk.setStatus(rs.getInt("FCSTATUS"));

        return tk;
    }

}
