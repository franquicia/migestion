package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.TareaActDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TareaSucursalesRowMapper implements RowMapper<TareaActDTO> {

    @Override
    public TareaActDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TareaActDTO tareaDTO = new TareaActDTO();

        tareaDTO.setIdCeco(rs.getInt("FCID_CECO"));
        tareaDTO.setNombreCeco(rs.getString("FCNOMBRE"));
        tareaDTO.setConteo(rs.getInt("CONTEO"));
        tareaDTO.setPorcentaje(rs.getInt("PORCENTAJE"));
        tareaDTO.setNivel(rs.getInt("NIVEL"));

        return tareaDTO;
    }
}
