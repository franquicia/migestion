package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.ArqueoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ArqueoRowMapper implements RowMapper<ArqueoDTO> {

    @Override
    public ArqueoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ArqueoDTO arqueoDTO = new ArqueoDTO();

        arqueoDTO.setIdCeco(rs.getInt("FCID_CECO"));
        arqueoDTO.setNombreCeco(rs.getString("FCNOMBRE"));

        return arqueoDTO;
    }

}
