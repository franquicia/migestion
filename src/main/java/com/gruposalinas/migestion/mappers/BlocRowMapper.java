package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.BlocDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class BlocRowMapper implements RowMapper<BlocDTO> {

    @Override
    public BlocDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        BlocDTO blocDTO = new BlocDTO();

        blocDTO.setIdBloc(rs.getInt("FIID_BLOC"));
        blocDTO.setTitulo(rs.getString("FCTITULO"));
        blocDTO.setNota(rs.getString("FCNOTA"));
        blocDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        blocDTO.setFechaCreacion(rs.getString("FDFECHA_CREA"));

        return blocDTO;
    }

}
