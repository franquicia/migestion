package com.gruposalinas.migestion.mappers;

import com.gruposalinas.migestion.domain.GeografiaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class GeografiaRowMapper implements RowMapper<GeografiaDTO> {

    public GeografiaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        GeografiaDTO geografiaDTO = new GeografiaDTO();

        geografiaDTO.setIdCeco(rs.getString("FCID_CECO"));
        geografiaDTO.setIdRegion(rs.getInt("FCID_REGION"));
        geografiaDTO.setNombreRegion(rs.getString("FCREGION"));
        geografiaDTO.setIdZona(rs.getInt("FCID_ZONA"));
        geografiaDTO.setNombreZona(rs.getString("FCZONA"));
        geografiaDTO.setIdTerritorio(rs.getInt("FCID_TERRITORIO"));
        geografiaDTO.setNombreTerritorio(rs.getString("FCTERRITORIO"));

        return geografiaDTO;

    }

}
