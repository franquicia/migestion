package com.gruposalinas.migestion.servicios.servidor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.AprobacionesRemedyHilos;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.DatosEmpMttoBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.ServiciosRemedyBI;
import com.gruposalinas.migestion.business.TicketCuadrillaBI;
import com.gruposalinas.migestion.business.TicketMantenimientoBI;
import com.gruposalinas.migestion.cliente.RemedyStub.Aprobacion;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfInfoIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfTracking;
import com.gruposalinas.migestion.cliente.RemedyStub.InfoIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.Tracking;
import com.gruposalinas.migestion.cliente.RemedyStub._Calificacion;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.StrCipher;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import com.gruposalinas.migestion.util.UtilGTN;

@Controller
@RequestMapping("/servicios")
public class ServiciosMantenimiento {

    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100; // 100MB

    private static Logger logger = LogManager.getLogger(ServiciosMantenimiento.class);

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    TicketMantenimientoBI ticketManrenimientoBI;

    @Autowired
    ServiciosRemedyBI serviciosRemedyBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    DatosEmpMttoBI datosEmpMttoBI;

    @Autowired
    TicketCuadrillaBI ticketCuadrillaBI;

    //http://localhost:8080/migestion/servicios/getBandejaCuadrilla.json?idUsuario=<?>&idUsuarioBandeja=<?>&cuadrilla=<?>&zona=<?>
    //http://localhost:8080/
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBandejaCuadrilla", method = RequestMethod.GET)
    public @ResponseBody
    String getBandejaCuadrilla(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            if (urides.split("&").length == 4) {
                String idUsuario = urides.split("&")[0].split("=")[1];
                String proveedor = urides.split("&")[1].split("=")[1];
                String cuadrilla = urides.split("&")[2].split("=")[1];
                String zona = urides.split("&")[3].split("=")[1];

                res = ticketManrenimientoBI.getBandejaCuadrilla(proveedor, Integer.parseInt(cuadrilla), Integer.parseInt(zona));

            } else if (urides.split("&").length == 5) { //Paginado

                String idUsuario = urides.split("&")[0].split("=").length == 2 ? urides.split("&")[0].split("=")[1] : "";
                String idCuadrilla = urides.split("&")[1].split("=").length == 2 ? urides.split("&")[1].split("=")[1] : "";
                String ticket = urides.split("&")[2].split("=").length == 2 ? urides.split("&")[2].split("=")[1] : "";
                String sucursal = urides.split("&")[3].split("=").length == 2 ? urides.split("&")[3].split("=")[1] : "";
                String pagina = urides.split("&")[4].split("=").length == 2 ? urides.split("&")[4].split("=")[1] : "";

                res = ticketManrenimientoBI.getBandejaCuadrillaPaginado(idCuadrilla, ticket, sucursal, Integer.parseInt(pagina));

            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            res = "{}";
        }

        return res;

    }

    //http://localhost:8080/migestion/servicios/getBandejaCuadrillaNew.json?idUsuario=<?>&idUsuarioBandeja=<?>&cuadrilla=<?>&zona=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBandejaCuadrillaNew", method = RequestMethod.GET)
    public @ResponseBody
    String getBandejaCuadrillaNew(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        int idLlave = 0;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = urides.split("&")[0].split("=")[1];
            String proveedor = urides.split("&")[1].split("=")[1];
            String cuadrilla = urides.split("&")[2].split("=")[1];
            String zona = urides.split("&")[3].split("=")[1];

            res = ticketManrenimientoBI.getBandejaCuadrilla(proveedor, Integer.parseInt(cuadrilla), Integer.parseInt(zona));

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            res = "{}";
        }

        return UtilGTN.encrypResult(idLlave, res);

    }

    // http://localhost:8080/migestion/servicios/getCuadrillasNew.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getCuadrillasNew", method = RequestMethod.GET)
    public @ResponseBody
    String getCuadrillasNew(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;

        String resCifrado = "";

        UtilCryptoGS cifra = null;
        StrCipher cifraIOS = null;

        int idLlave = 0;
        try {

            cifra = new UtilCryptoGS();
            cifraIOS = new StrCipher();

            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if (urides.length() > 0) {
                String idProveedor = urides.split("&")[1].split("=")[1];
                if (idProveedor.matches("\\d+(\\.\\d+)?")) {
                    List<CuadrillaDTO> lista = cuadrillaBI.obtieneDatos(Integer.parseInt(idProveedor));

                    ArrayList<CuadrillaDTO> ZNorte = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZCentro = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZSur = new ArrayList<CuadrillaDTO>();

                    if (!lista.isEmpty() && lista != null) {
                        for (CuadrillaDTO aux : lista) {
                            if (aux.getZona() == 0) {
                                ZNorte.add(aux);
                            }
                            if (aux.getZona() == 1) {
                                ZCentro.add(aux);
                            }
                            if (aux.getZona() == 2) {
                                ZSur.add(aux);
                            }
                        }
                    }

                    JsonArray ZNJsonArray = new JsonArray();
                    if (!ZNorte.isEmpty() && ZNorte != null) {

                        for (CuadrillaDTO aux : ZNorte) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZNJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZCJsonArray = new JsonArray();
                    if (!ZCentro.isEmpty() && ZCentro != null) {

                        for (CuadrillaDTO aux : ZCentro) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZCJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZSJsonArray = new JsonArray();
                    if (!ZSur.isEmpty() && ZSur != null) {

                        for (CuadrillaDTO aux : ZSur) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZSJsonArray.add(incJsonObj);
                        }

                    }

                    JsonObject envoltorioJsonObj = new JsonObject();

                    if (lista == null) {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    } else {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    }
                    //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                    res = envoltorioJsonObj.toString();

                } else {
                    return UtilGTN.encrypResult(idLlave, "{}");

                }
            } else {

                return UtilGTN.encrypResult(idLlave, "{}");
            }
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return UtilGTN.encrypResult(idLlave, "{}");
        }

        if (res != null) {
            //return res;
            return UtilGTN.encrypResult(idLlave, res);
        } else {

            return UtilGTN.encrypResult(idLlave, "{}");
        }
    }

    // http://localhost:8080/migestion/servicios/getIncidentesSucursal.json?ceco=<?>&idTicket=<?>&pagina<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesSucursal", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesSucursalHilos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;

        HashMap< String, Object> mapResult = null;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String ceco = urides.split("&")[1].split("=")[1];

            // int idFolio=0;
            //ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.ConsultaFoliosSucursal(numSucursal,"0");
            int idTicket = 0;
            int pagina = 0;

            boolean nuevo = false;
            try {

                String incidente = urides.split("&")[2].split("=").length == 2
                        ? urides.split("&")[2].split("=")[1] : "0";

                String paginaStr = urides.split("&")[3].split("=").length == 2
                        ? urides.split("&")[3].split("=")[1] : "0";

                idTicket = incidente != null && !incidente.equals("") ? Integer.parseInt(incidente) : 0;

                pagina = paginaStr != null && !paginaStr.equals("") ? Integer.parseInt(paginaStr) : 1;

                nuevo = true;

            } catch (NumberFormatException numberEx) {
                logger.info(numberEx);
                numberEx.printStackTrace();
                idTicket = 0;
                nuevo = true;
            } catch (Exception e) {
                logger.info(e);
                e.printStackTrace();
                idTicket = 0;

            }

            ArrayOfInfoIncidente arrIncidentesAbiertos = null;
            ArrayOfInfoIncidente arrIncidentesCerrados = null;

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray incidenciasJsonArray = new JsonArray();

            ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

            if (nuevo) {

                String[] params = {ceco, idTicket + "", "", "", "", "" + pagina, ""};

                mapResult = serviciosRemedyBI.consultaFoliosPaginado(params);

                arrIncidentesAbiertos = (ArrayOfInfoIncidente) mapResult.get("arrayIncidente");

                InfoIncidente[] arregloInc = null;
                if (arrIncidentesAbiertos != null) {
                    arregloInc = arrIncidentesAbiertos.getInfoIncidente();
                    if (arregloInc != null && arregloInc.length > 0) {

                        for (InfoIncidente aux : arregloInc) {

                            normal.add(aux);

                        }
                    }
                }

            } else {

                int numSucursal = 0;
                String cecoS = ceco + "";
                if (cecoS.length() == 6) {
                    numSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
                }
                if (cecoS.length() == 5) {
                    numSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
                }

                arrIncidentesAbiertos = serviciosRemedyBI.ConsultaFoliosSucursal(numSucursal, "1");
                arrIncidentesCerrados = serviciosRemedyBI.ConsultaFoliosSucursal(numSucursal, "2");

                //-----------------------Folios Abiertos--------------------------
                InfoIncidente[] arregloInc = null;
                if (arrIncidentesAbiertos != null) {
                    arregloInc = arrIncidentesAbiertos.getInfoIncidente();
                    if (arregloInc != null && arregloInc.length > 0) {

                        for (InfoIncidente aux : arregloInc) {
                            //        if (aux.getPrioridad().getValue().contains("Normal")) {
                            //            normal.add(aux);
                            //        }
                            //        if (aux.getPrioridad().getValue().contains("Critica")) {
                            Critica.add(aux);
                            //        }
                        }
                    } else {
                        arrIncidentesAbiertos = serviciosRemedyBI.ConsultaFoliosSucursal(Integer.parseInt(ceco), "3");

                        if (arrIncidentesAbiertos != null) {
                            arregloInc = arrIncidentesAbiertos.getInfoIncidente();
                            if (arregloInc != null && arregloInc.length > 0) {
                                for (InfoIncidente aux : arregloInc) {
                                    Critica.add(aux);
                                }
                            }
                        }

                    }
                }
                //-----------------------Folios Cerrados--------------------------
                if (arrIncidentesCerrados != null) {
                    arregloInc = arrIncidentesCerrados.getInfoIncidente();
                    if (arregloInc != null && arregloInc.length > 0) {

                        for (InfoIncidente aux : arregloInc) {
                            //        if (aux.getPrioridad().getValue().contains("Normal")) {
                            //            normal.add(aux);
                            //        }
                            //        if (aux.getPrioridad().getValue().contains("Critica")) {
                            Critica.add(aux);
                            //        }
                        }
                    } else {
                        arrIncidentesCerrados = serviciosRemedyBI.ConsultaFoliosSucursal(Integer.parseInt(ceco), "4");
                        if (arrIncidentesCerrados != null) {
                            arregloInc = arrIncidentesCerrados.getInfoIncidente();
                            if (arregloInc != null && arregloInc.length > 0) {
                                for (InfoIncidente aux : arregloInc) {
                                    Critica.add(aux);
                                }
                            }
                        }

                    }
                }
            }

            //--------------Ejecuta hilos aprobaciones--------------------
            ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

            if (Critica != null && !Critica.isEmpty()) {
                for (InfoIncidente aux : Critica) {
                    aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                }
            }

            if (normal != null && !normal.isEmpty()) {
                for (InfoIncidente aux : normal) {
                    aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                }
            }

            for (int i = 0; i < aprHilos.size(); i++) {
                aprHilos.get(i).run();
            }

            boolean terminaron = false;

            long startTime1 = System.currentTimeMillis();
            logger.info("Entro a verificar hilos");
            if (aprHilos == null || aprHilos.isEmpty()) {
                terminaron = true;
            }
            while (!terminaron) {
                for (int i = 0; i < aprHilos.size(); i++) {
                    if (aprHilos.get(i).getRespuesta() != null) {
                        terminaron = true;
                    } else {
                        terminaron = false;
                        break;
                    }
                    if (!aprHilos.get(i).isAlive()) {
                        terminaron = true;
                    } else {
                        terminaron = false;
                        break;
                    }
                }
            }

            HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
            for (int i = 0; i < aprHilos.size(); i++) {
                AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
            }

            long endTime1 = System.currentTimeMillis() - startTime1;
            logger.info("TIEMPO RESP en Hilos: " + endTime1);
            //-------------------------------------------------------------

            List<Integer> ticketsErroneos = new ArrayList<>();
            incidenciasJsonArray = arrOrdenadoSucursalHilos(Critica, normal, AprobacionesHashMap, ticketsErroneos);
            //incidenciasJsonArray = arrOrdenadoSucursalHilos(Critica, normal, AprobacionesHashMap);

            if (arrIncidentesAbiertos == null) {
                envoltorioJsonObj.add("FOLIOS", null);
                envoltorioJsonObj.addProperty("TOTAL_TICKETS", 0);
                envoltorioJsonObj.addProperty("TICKETS_ERRONEOS", 0);

            } else {
                envoltorioJsonObj.add("FOLIOS", incidenciasJsonArray);
                if (mapResult != null) {
                    envoltorioJsonObj.addProperty("TOTAL_TICKETS", (Integer) mapResult.get("numTotalIncidentes"));
                }

                JsonElement result = new GsonBuilder().create().toJsonTree(ticketsErroneos);
                envoltorioJsonObj.add("TICKETS_ERRONEOS", result);

            }
            //logger.info("JSON ID FOLIOS SUCURSAL: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    //http://localhost:8080/migestion/servicios/getBandejaTickets.json?idUsuario=<?>&idUsuarioBandeja=<?>&tipoUsuario=<?>
    //Nuevo consumo
    //http://localhost:8080/migestion/servicios/getBandejaTickets.json?idUsuario=<?>&sucursal=<?>&idTicket=<?>&idProveedor=<?>&idSupervisor=<?>&idCoordinador=&pagina<?>&idCuadrilla=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBandejaTickets", method = RequestMethod.GET)
    public @ResponseBody
    String getBandejaTickets(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            if (urides.split("&").length == 2) {

                String idUsuario = urides.split("&")[0].split("=")[1];
                String tipoUsuario = urides.split("&")[1].split("=")[1];

                res = ticketManrenimientoBI.getBandejaTickets(idUsuario, Integer.parseInt(tipoUsuario));

            } else if (urides.split("&").length == 3) {
                String idUsuario = urides.split("&")[0].split("=")[1];
                //String razonSocial = urides.split("&")[1].split("=")[1];
                String idProveedor = urides.split("&")[1].split("=")[1];
                String tipoUsuario = urides.split("&")[2].split("=")[1];

                //Implementa buscar el idProveedor
                List<ProveedoresDTO> proveedoresArray = proveedoresBI.obtieneDatos(Integer.parseInt(idProveedor));
                ProveedoresDTO proveedorDTO = null;

                if (proveedoresArray != null && proveedoresArray.size() > 0) {

                    proveedorDTO = proveedoresArray.get(0);
                    res = ticketManrenimientoBI.getBandejaTickets(proveedorDTO.getRazonSocial(), Integer.parseInt(tipoUsuario));

                } else {
                    res = "{}";
                }

                //res = ticketManrenimientoBI.getBandejaTickets(razonSocial, Integer.parseInt(tipoUsuario));
            } else { //nuevo paginado

                String idUsuario = urides.split("&")[0].split("=").length == 2 ? urides.split("&")[0].split("=")[1] : "";;
                String cecoStr = urides.split("&")[1].split("=").length == 2 ? urides.split("&")[1].split("=")[1] : "";
                String ticketStr = urides.split("&")[2].split("=").length == 2 ? urides.split("&")[2].split("=")[1] : "";
                String idProovedorStr = urides.split("&")[3].split("=").length == 2 ? urides.split("&")[3].split("=")[1] : "";
                String idSupervisor = urides.split("&")[4].split("=").length == 2 ? urides.split("&")[4].split("=")[1] : "";
                String idCoordinador = urides.split("&")[5].split("=").length == 2 ? urides.split("&")[5].split("=")[1] : "";
                String paginaStr = urides.split("&")[6].split("=").length == 2 ? urides.split("&")[6].split("=")[1] : "";

                String idCuadrilla = urides.split("&").length == 8
                        ? urides.split("&")[7].split("=").length == 2 ? urides.split("&")[7].split("=")[1] : ""
                        : "";

                res = ticketManrenimientoBI.getBandejaTicketsPaginado(
                        cecoStr,
                        ticketStr,
                        idProovedorStr,
                        idSupervisor,
                        idCoordinador,
                        paginaStr,
                        idCuadrilla);

            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            res = "{}";
        }

        return res;

    }

    //public JsonArray arrOrdenadoSucursalHilos(ArrayList<InfoIncidente> Critica, ArrayList<InfoIncidente> normal, HashMap<String, Aprobacion> AprobacionesHashMap) {
    public JsonArray arrOrdenadoSucursalHilos(ArrayList<InfoIncidente> Critica, ArrayList<InfoIncidente> normal, HashMap<String, Aprobacion> AprobacionesHashMap, List<Integer> ticketsErroneos) {
        JsonArray arrJsonArray = new JsonArray();
        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();

        HashMap<String, String> EmpleadosRemedyHashMap = serviciosRemedyBI.consultaUsuariosRemedy();

        //List<Integer> ticketsErroneos = new ArrayList<>();
        while (!Critica.isEmpty()) {
            int pos = 0;
            for (int i = 0; i < Critica.size(); i++) {
                if (Critica.get(pos).getFechaCreacion().compareTo(Critica.get(i).getFechaCreacion()) <= 0) {
                    pos = i;
                }
            }
            InfoIncidente aux = Critica.get(pos);

            //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
            JsonObject incJsonObj = new JsonObject();

            aux.getAutorizaCambioMonto();
            incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
            incJsonObj.addProperty("ESTADO", aux.getEstado());
            incJsonObj.addProperty("FALLA", aux.getFalla());
            incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
            incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
            incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
            incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
            incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
            incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());
            String idCliente = aux.getIdCorpCliente();
            if (idCliente != null) {
                if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                    List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                    if (!datos_cliente.isEmpty() && datos_cliente != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                            incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                            incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                            EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                        incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                    incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                    incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                }
            } else {
                incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
            }
            String idCord = aux.getIDCorpCoordinador();
            if (idCord != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                }
            } else {
                incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
            }

            if (idCord != null) {
                if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                    List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(idCord));
                    if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                            incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            EmpleadosHashMap.put(idCord.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("COORDINADOR", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                    incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                }

            } else {
                incJsonObj.addProperty("COORDINADOR", "* Sin información");
            }
            String idSup = aux.getIdCorpSupervisor();

            if (idSup != null) {
                idSup = aux.getIdCorpSupervisor().trim();
                if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                }
            } else {
                incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
            }

            if (idSup != null) {
                if (!idSup.matches("[^0-9]*")) {

                    if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(idSup));

                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                EmpleadosHashMap.put(idSup.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                        incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                        incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }
            } else {
                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
            }

            incJsonObj.addProperty("NOTAS", aux.getNotas());
            incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
            incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

            if (aux.getProveedor() != null) {
                if (!lista.isEmpty()) {
                    for (ProveedoresDTO aux3 : lista) {
                        if (aux3.getStatus().contains("Activo")) {
                            if (aux.getProveedor().toLowerCase().trim()
                                    .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                incJsonObj.addProperty("MENU", aux3.getMenu());
                                incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                break;
                            }

                        }
                    }
                }
            } else {
                String nomCorto = null;
                incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                incJsonObj.addProperty("MENU", nomCorto);
                incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
            }

            incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
            incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
            incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
            incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
            incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

            incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
            incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
            Date date = aux.getFechaCierre() != null ? aux.getFechaCierre().getTime() : fechaNula();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
            date = aux.getFechaCreacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CREACION", "" + date1);
            date = aux.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
            date = aux.getFechaProgramada().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
            incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
            incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());
            String autCleinte = aux.getAutorizacionCliente();
            if (autCleinte != null && !autCleinte.equals("")) {
                if (autCleinte.contains("Rechazado")) {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                }
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
            }

            if (aux.getTipoAtencion() != null) {
                incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("TIPO_ATENCION", x);
            }
            if (aux.getOrigen() != null) {
                incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("ORIGEN", x);
            }
            //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
            String cord = null;
            if (aux.getLatitudInicio() != null) {
                incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
            } else {
                incJsonObj.addProperty("LATITUD_INICIO", cord);
            }

            if (aux.getLongitudInicio() != null) {
                incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
            } else {
                incJsonObj.addProperty("LONGITUD_INICIO", cord);
            }

            if (aux.getLatitudCierre() != null) {
                incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
            } else {
                incJsonObj.addProperty("LATITUD_FIN", cord);
            }

            if (aux.getLongitudCierre() != null) {
                incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
            } else {
                incJsonObj.addProperty("LONGITUD_FIN", cord);
            }

            //----------------------------------------------------------------------------------
            date = aux.getFechaInicioAtencion() != null ? aux.getFechaInicioAtencion().getTime() : fechaNula();

            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

            int idIncidente = aux.getIdIncidencia();

            List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
            int idProvedor = 0;
            int idCuadrilla = 0;
            int idZona = 0;
            for (TicketCuadrillaDTO aux2 : lista2) {
                if (aux2.getStatus() != 0) {
                    idProvedor = aux2.getIdProveedor();
                    idCuadrilla = aux2.getIdCuadrilla();
                    idZona = aux2.getZona();
                }

            }
            if (idProvedor != 0) {
                List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                for (CuadrillaDTO aux2 : lista3) {
                    if (aux2.getZona() == idZona) {
                        if (aux2.getIdCuadrilla() == idCuadrilla) {
                            incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                            incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux2.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                            incJsonObj.addProperty("ZONA", aux2.getZona());
                        }
                    }
                }
            } else {
                String auxiliar = null;
                //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                incJsonObj.addProperty("LIDER", auxiliar);
                incJsonObj.addProperty("CORREO", auxiliar);
                incJsonObj.addProperty("SEGUNDO", auxiliar);
                incJsonObj.addProperty("ZONA", auxiliar);
            }

            if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);

                String estadoApr = aprobacion.getEstadoAprobacion();
                boolean flag1 = false, flag2 = false;
                if (estadoApr != null) {
                    String porvAprob = aprobacion.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_CMONTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                }

                Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                estadoApr = aprobacion2.getEstadoAprobacion();
                if (estadoApr != null) {
                    String porvAprob = aprobacion2.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                }

                if (flag1 == true && flag2 == true) {
                    incJsonObj.addProperty("BANDERAS", 1);
                } else {
                    incJsonObj.addProperty("BANDERAS", 0);
                }
            } else {
                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                incJsonObj.addProperty("BANDERAS", 0);
            }

            //-----------------------  Consulta Traking  --------------------
            ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

            JsonArray incidenciasJsonArray = new JsonArray();

            ArrayList<Date> RechazoCliente = new ArrayList<Date>();
            ArrayList<Date> inicioAtencion = new ArrayList<Date>();
            ArrayList<Date> cierreTecnico = new ArrayList<Date>();
            ArrayList<Date> cancelados = new ArrayList<Date>();
            ArrayList<Date> atencionGarantia = new ArrayList<Date>();
            ArrayList<Date> cierreGarantia = new ArrayList<Date>();
            ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null) {
                    for (Tracking aux2 : traking) {
                        String detalle = aux2.getDetalle();
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date2 = aux2.getFechaEnvio().getTime();
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                            RechazoCliente.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                            inicioAtencion.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                            cierreTecnico.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado") || motivoEstado.trim().toLowerCase().equals("cancelado no aplica") || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                            cancelados.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                            atencionGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                            cierreGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("recibido por atender") && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                            rechazoProveedor.add(date2);
                        }
                    }
                }
            }

            int posRC = 0, posRP = 0;

            String cadAux = null;
            if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < RechazoCliente.size(); i++) {
                    if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = RechazoCliente.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                posRC = pos2;
            } else {

                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
            }

            if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < inicioAtencion.size(); i++) {
                    if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = inicioAtencion.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION", cadAux);
            }

            if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreTecnico.size(); i++) {
                    if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreTecnico.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                incJsonObj.addProperty("FECHA_CIERRE", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                incJsonObj.addProperty("FECHA_CIERRE", cadAux);
            }

            if (cancelados != null && !cancelados.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cancelados.size(); i++) {
                    if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cancelados.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CANCELACION", date3);
            } else {
                incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
            }
            //
            if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < atencionGarantia.size(); i++) {
                    if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = atencionGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
            }

            if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreGarantia.size(); i++) {
                    if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
            }

            if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < rechazoProveedor.size(); i++) {
                    if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = rechazoProveedor.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                posRP = pos2;
            } else {
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
            }

            //BANDERA_RECHAZO_CIERRE_PREVIO
            if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null && !RechazoCliente.isEmpty()) {
                if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }
            } else {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
            }

            //------------------------------------------------------------------
            int cal = 0;
            _Calificacion auxEval = aux.getEvalCliente();
            if (auxEval != null) {
                if (auxEval.getValue().contains("NA")) {
                    cal = 0;
                }
                if (auxEval.getValue().contains("Item1")) {
                    cal = 1;
                }
                if (auxEval.getValue().contains("Item2")) {
                    cal = 2;
                }
                if (auxEval.getValue().contains("Item3")) {
                    cal = 3;
                }
                if (auxEval.getValue().contains("Item4")) {
                    cal = 4;
                }
                if (auxEval.getValue().contains("Item5")) {
                    cal = 5;
                }
            }

            incJsonObj.addProperty("CALIFICACION", cal);

            incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

            date = aux.getFechaCierreAdmin().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

            incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

            //------------------------------ COMENTARIOS PARA PROVEEDOR --------------------------
            //if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
            /*Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 2);
             if(aprobacion!=null) {
             incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
             incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
             date = aprobacion.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
             incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
             }*/
            if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                date = aprobacion.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
                Date dateI = aux.getFechaInicioAtencion().getTime();

                if (date.compareTo(dateI) >= 0) {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }
            } else {
                String auxApr = null;
                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
            }

            //-----------------------------------------------------------------------------------
            //----------------------------FECHA CANCELACION----------------------------------------
            /*if( aux.getEstado().trim().toLowerCase().equals("atendido") && (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))) {
             date = aux.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_CANCELACION", "" + date1);
             }*/
            //-------------------------------------------------------------------------------------
            incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
            incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
            incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());

            arrJsonArray.add(incJsonObj);

            Critica.remove(pos);
        }

        while (!normal.isEmpty()) {

            int pos = 0;
            int ticket = 0;
            try {

                for (int i = 0; i < normal.size(); i++) {
                    if (normal.get(pos).getFechaCreacion().compareTo(normal.get(i).getFechaCreacion()) <= 0) {
                        pos = i;
                    }
                }//Se puede quitar ya
                InfoIncidente aux = normal.get(pos);

                JsonObject incJsonObj = new JsonObject();
                ticket = aux.getIdIncidencia();

                incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
                incJsonObj.addProperty("ESTADO", aux.getEstado());
                incJsonObj.addProperty("FALLA", aux.getFalla());
                incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
                incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
                incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
                incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
                incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
                incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());

                String idCliente = aux.getIdCorpCliente();
                if (idCliente != null && !idCliente.isEmpty()) {
                    if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                        List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                        if (!datos_cliente.isEmpty() && datos_cliente != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                                incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                                incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                                EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                            incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                        incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                        incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                    }
                } else {
                    incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                    incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                }
                String idCord = aux.getIDCorpCoordinador();
                if (idCord != null && !idCord.isEmpty()) {
                    if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                        incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                    }
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                }
                if (idCord != null && !idCord.isEmpty()) {
                    if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                        List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                        if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                                incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                EmpleadosHashMap.put(idCord.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("COORDINADOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                        incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("COORDINADOR", "* Sin información");
                }
                String idSup = aux.getIdCorpSupervisor().trim();
                if (idSup != null && !idSup.isEmpty()) {
                    if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                        incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                    }
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                }
                if (idSup != null && !idSup.isEmpty()) {
                    if (!idSup.matches("[^0-9]*")) {

                        if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                            List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                            if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                                for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                    incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                    incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                    EmpleadosHashMap.put(idSup.trim(), emp_aux);
                                }
                            } else {
                                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                            }
                        } else {
                            DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                            incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                        }

                    } else {
                        incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                        incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                    }
                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }

                // incJsonObj.addProperty("COORDINADOR", "COORDINADOR JUAN PEREZ");
                incJsonObj.addProperty("NOTAS", aux.getNotas());
                incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
                incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());
                if (aux.getProveedor() != null) {
                    if (!lista.isEmpty()) {
                        for (ProveedoresDTO aux3 : lista) {
                            if (aux3.getStatus().contains("Activo")) {
                                if (aux.getProveedor().toLowerCase().trim()
                                        .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                    incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                    incJsonObj.addProperty("MENU", aux3.getMenu());
                                    incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                    incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                    break;
                                }

                            }
                        }
                    }
                } else {
                    String nomCorto = null;
                    incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                    incJsonObj.addProperty("MENU", nomCorto);
                    incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                    incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
                }
                incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
                incJsonObj.addProperty("SUCURSAL", aux.getSucursal());

                incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
                incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
                incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

                incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
                incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
                String autCleinte = aux.getAutorizacionCliente();
                if (autCleinte != null && !autCleinte.equals("")) {
                    if (autCleinte.contains("Rechazado")) {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                    } else {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                    }
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
                }
                Date date = aux.getFechaCierre() != null ? aux.getFechaCierre().getTime() : fechaNula();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
                date = aux.getFechaCreacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CREACION", "" + date1);
                date = aux.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
                date = aux.getFechaProgramada() != null ? aux.getFechaProgramada().getTime() : fechaNula();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
                incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
                incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());

                if (aux.getTipoAtencion() != null) {
                    incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("TIPO_ATENCION", x);
                }
                if (aux.getOrigen() != null) {
                    incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("ORIGEN", x);
                }

                //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
                String cord = null;
                if (aux.getLatitudInicio() != null) {
                    incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
                } else {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                }

                if (aux.getLongitudInicio() != null) {
                    incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
                } else {
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }

                if (aux.getLatitudCierre() != null) {
                    incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
                } else {
                    incJsonObj.addProperty("LATITUD_FIN", cord);
                }

                if (aux.getLongitudCierre() != null) {
                    incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
                } else {
                    incJsonObj.addProperty("LONGITUD_FIN", cord);
                }

                //----------------------------------------------------------------------------------
                date = aux.getFechaInicioAtencion() != null ? aux.getFechaInicioAtencion().getTime() : fechaNula();
                //date = aux.getFechaInicioAtencion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

                int idIncidente = aux.getIdIncidencia();

                List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
                int idProvedor = 0;
                int idCuadrilla = 0;
                int idZona = 0;
                for (TicketCuadrillaDTO aux2 : lista2) {
                    if (aux2.getStatus() != 0) {
                        idProvedor = aux2.getIdProveedor();
                        idCuadrilla = aux2.getIdCuadrilla();
                        idZona = aux2.getZona();
                    }

                }
                if (idProvedor != 0) {
                    List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                    for (CuadrillaDTO aux2 : lista3) {
                        if (aux2.getZona() == idZona) {
                            if (aux2.getIdCuadrilla() == idCuadrilla) {
                                incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                                incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                                incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                                incJsonObj.addProperty("CORREO", aux2.getCorreo());
                                incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                                incJsonObj.addProperty("ZONA", aux2.getZona());
                            }
                        }
                    }
                } else {
                    String auxiliar = null;
                    //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                    incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                    incJsonObj.addProperty("LIDER", auxiliar);
                    incJsonObj.addProperty("CORREO", auxiliar);
                    incJsonObj.addProperty("SEGUNDO", auxiliar);
                    incJsonObj.addProperty("ZONA", auxiliar);
                }

                if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                    Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);
                    String estadoApr = aprobacion.getEstadoAprobacion();

                    boolean flag1 = false, flag2 = false;
                    if (estadoApr != null) {
                        String porvAprob = aprobacion.getSolicitadoPor();
                        if (porvAprob.contains("" + idProvedor)) {
                            incJsonObj.addProperty("BANDERA_CMONTO", 1);
                            flag1 = true;
                        } else {
                            incJsonObj.addProperty("BANDERA_CMONTO", 0);
                        }
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }

                    Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                    estadoApr = aprobacion2.getEstadoAprobacion();
                    if (estadoApr != null) {
                        String porvAprob = aprobacion2.getSolicitadoPor();
                        if (porvAprob.contains("" + idProvedor)) {
                            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                            flag1 = true;
                        } else {
                            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                        }
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }

                    if (flag1 == true && flag2 == true) {
                        incJsonObj.addProperty("BANDERAS", 1);
                    } else {
                        incJsonObj.addProperty("BANDERAS", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    incJsonObj.addProperty("BANDERAS", 0);
                }

                //-----------------------  Consulta Traking  --------------------
                ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

                JsonArray incidenciasJsonArray = new JsonArray();

                ArrayList<Date> RechazoCliente = new ArrayList<Date>();
                ArrayList<Date> inicioAtencion = new ArrayList<Date>();
                ArrayList<Date> cierreTecnico = new ArrayList<Date>();
                ArrayList<Date> cancelados = new ArrayList<Date>();
                ArrayList<Date> atencionGarantia = new ArrayList<Date>();
                ArrayList<Date> cierreGarantia = new ArrayList<Date>();
                ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

                if (arrTraking != null) {
                    Tracking[] traking = arrTraking.getTracking();
                    if (traking != null) {
                        for (Tracking aux2 : traking) {
                            String detalle = aux2.getDetalle();
                            String arrAux[] = detalle.split("\\|\\|");
                            String estado = detalle.split("\\|\\|")[1];
                            String motivoEstado = detalle.split("\\|\\|")[3];
                            Date date2 = aux2.getFechaEnvio().getTime();
                            if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                                RechazoCliente.add(date2);
                            }
                            if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                                inicioAtencion.add(date2);
                            }
                            if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                                cierreTecnico.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado") || motivoEstado.trim().toLowerCase().equals("cancelado no aplica") || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                                cancelados.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                                atencionGarantia.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("atendido") && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                                cierreGarantia.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("recibido por atender") && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                                rechazoProveedor.add(date2);
                            }
                        }
                    }
                }

                int posRC = 0, posRP = 0;

                String cadAux = null;
                if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < RechazoCliente.size(); i++) {
                        if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = RechazoCliente.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                    posRC = pos2;
                } else {

                    incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
                }

                if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < inicioAtencion.size(); i++) {
                        if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = inicioAtencion.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_ATENCION", date3);

                } else {
                    incJsonObj.addProperty("FECHA_ATENCION", cadAux);

                }

                if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cierreTecnico.size(); i++) {
                        if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cierreTecnico.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                    incJsonObj.addProperty("FECHA_CIERRE", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                    incJsonObj.addProperty("FECHA_CIERRE", cadAux);
                }

                if (cancelados != null && !cancelados.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cancelados.size(); i++) {
                        if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cancelados.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CANCELACION", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
                }
                //
                if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < atencionGarantia.size(); i++) {
                        if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = atencionGarantia.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
                } else {
                    incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
                }

                if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cierreGarantia.size(); i++) {
                        if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cierreGarantia.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
                }

                if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < rechazoProveedor.size(); i++) {
                        if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = rechazoProveedor.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                    posRP = pos2;
                } else {
                    incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
                }

                //BANDERA_RECHAZO_CIERRE_PREVIO
                if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null && !RechazoCliente.isEmpty()) {
                    if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                        incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                    } else {
                        incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }

                //------------------------------------------------------------------
                int cal = 0;
                _Calificacion auxEval = aux.getEvalCliente();
                if (auxEval != null) {
                    if (auxEval.getValue().contains("NA")) {
                        cal = 0;
                    }
                    if (auxEval.getValue().contains("Item1")) {
                        cal = 1;
                    }
                    if (auxEval.getValue().contains("Item2")) {
                        cal = 2;
                    }
                    if (auxEval.getValue().contains("Item3")) {
                        cal = 3;
                    }
                    if (auxEval.getValue().contains("Item4")) {
                        cal = 4;
                    }
                    if (auxEval.getValue().contains("Item5")) {
                        cal = 5;
                    }
                }

                incJsonObj.addProperty("CALIFICACION", cal);
                incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

                date = aux.getFechaCierreAdmin() != null ? aux.getFechaCierreAdmin().getTime() : fechaNula();
                //date = aux.getFechaCierreAdmin().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

                incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

                //------------------------------ COMENTARIOS PARA PROVEEDOR --------------------------
                //if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
                /*
                 Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 2);
                 if(aprobacion!=null) {
                 incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                 incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                 date = aprobacion.getFechaModificacion().getTime();
                 date1 = format1.format(date);
                 incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                 incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
                 }
                 */
                if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                    Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                    incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                    incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                    date = aprobacion.getFechaModificacion().getTime();
                    date1 = format1.format(date);
                    incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                    incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());

                    Date dateI = aux.getFechaInicioAtencion() != null ? aux.getFechaInicioAtencion().getTime() : fechaNula();
                    //Date dateI = aux.getFechaInicioAtencion().getTime();

                    if (date.compareTo(dateI) >= 0) {
                        incJsonObj.addProperty("LATITUD_INICIO", cord);
                        incJsonObj.addProperty("LONGITUD_INICIO", cord);
                    }
                } else {
                    String auxApr = null;
                    incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
                }

                //-----------------------------------------------------------------------------------
                //----------------------------FECHA CANCELACION----------------------------------------
                /*if( aux.getEstado().trim().toLowerCase().equals("atendido") && (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))) {
                 date = aux.getFechaModificacion().getTime();
                 date1 = format1.format(date);
                 incJsonObj.addProperty("FECHA_CANCELACION", "" + date1);
                 }*/
                //-------------------------------------------------------------------------------------
                incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
                incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
                incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());
                arrJsonArray.add(incJsonObj);

                normal.remove(pos);

            } catch (Exception e) {
                logger.info(e);
                e.printStackTrace();

                normal.remove(pos);

                ticketsErroneos.add(ticket);

                logger.info("TICKET_NO_VALIDO {" + ticket + "}");

            }

        }

        return arrJsonArray;
    }

    public Date fechaNula() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date nullDate = null;
        try {
            nullDate = sdf.parse("00010101");
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
        }

        return nullDate;

    }

}
