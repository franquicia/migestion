package com.gruposalinas.migestion.servicios.servidor;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gruposalinas.migestion.business.NotificacionesBI;

@Controller
@RequestMapping("/notificacionesService")
public class ServicioNotificaciones {

    @Autowired
    NotificacionesBI notificacionesBI;

    //http://localhost:8080/migestion/notificacionesService/enviaNotificacion.json?token=<?>&title=<?>&message=<?>
    @RequestMapping(value = "/enviaNotificacion", method = RequestMethod.GET)
    public @ResponseBody
    boolean enviaNotificacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String token = request.getParameter("token");
        String title = request.getParameter("title");
        String message = request.getParameter("message");

        boolean res = notificacionesBI.enviarNotificacionJSON(token, title, message);

        return res;
    }

    //http://localhost:8080/migestion/notificacionesService/enviaNotificacion.json?ceco=<?>&puesto=<?>&title=<?>&message=<?>
    @RequestMapping(value = "/enviaNotificacionCeco", method = RequestMethod.GET)
    public @ResponseBody
    boolean enviaNotificacionCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String ceco = request.getParameter("ceco");
        String puesto = request.getParameter("puesto");
        String message = request.getParameter("message");
        String title = request.getParameter("title");

        try {
            notificacionesBI.enviaNotificacionesCeco(ceco, puesto, message, title);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;

    }

    //migestion/notificacionesService/enviaNotificacionNueva.json?token=<?>&title=<?>&message=<?>
    //migestion/notificacionesService/enviaNotificacionNueva.json?token=eWsU98X2T0Y:APA91bEaCdh9V6IB7KMzzB6es239i1TQfyxNU1VkbqJ2vFR4biifznjtJFxj_yovt-havdy1_V9_Gj1sMsJdL6w3D7CeS6jM3iTt0rkwHr0p5fyEEBd_55O2Slfk2XKmqRWD3LQ2yr7D&title=Test&message=Hola Mundo
    @RequestMapping(value = "/enviaNotificacionNueva", method = RequestMethod.GET)
    public @ResponseBody
    String enviaNotificacionNueva(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String token = request.getParameter("token");
        String title = request.getParameter("title");
        String message = request.getParameter("message");

        String res = notificacionesBI.enviarNotificacionJSONSinProxy(token, title, message).toString();

        return res.toString();
    }

}
