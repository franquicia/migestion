package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.ProductividadBI;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/consultasService")
public class ServiciosConsulta {

    //private Logger logger = LogManager.getLogger(ServiciosConsulta.class);
    @Autowired
    ProductividadBI productividadBi;

    //http://localhost:8080/migestion/consultasService/getClasificacion.json?ceco=<?>
    @RequestMapping(value = "/getClasificacion", method = RequestMethod.GET)
    public @ResponseBody
    int getClasificacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String ceco = request.getParameter("ceco");

        int clasificacion = productividadBi.getClasificacion(ceco);

        return clasificacion;
    }

    //http://localhost:8080/migestion/consultasService/getClasificaciones.json?ceco=<?>&tipo=<?>
    @RequestMapping(value = "/getClasificaciones", method = RequestMethod.GET)
    public @ResponseBody
    String getClasificaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String ceco = request.getParameter("ceco");
        String tipo = request.getParameter("tipo");

        System.out.println("ceco " + ceco);
        System.out.println("tipo " + tipo);

        String clasificacion = productividadBi.gecCecosClasificacion(ceco, Integer.valueOf(tipo));

        return clasificacion;
    }

}
