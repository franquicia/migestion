package com.gruposalinas.migestion.servicios.servidor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.Usuario_ABI;
import com.gruposalinas.migestion.domain.Usuario_ADTO;

@Controller
@RequestMapping("/services")
public class Services {

    @Autowired
    Usuario_ABI usuarioBI;

    private static final Logger logger = LogManager.getLogger(Services.class);

    // http://localhost:8080/checklist/services/getCecoByUsuario.json
    @RequestMapping(value = "/getCecoByUsuario", method = RequestMethod.POST)
    public @ResponseBody
    String getCecoByUsuario(HttpServletRequest request, HttpServletResponse response) {

        String json = "";
        JsonObject object = new JsonObject();
        try {
            String contenido = request.getQueryString();
            logger.info("contenido:  " + contenido);
            if (contenido != null && contenido.contains("idUsuario=")) {
                String idUsuario = contenido.split("=")[1];
                List<Usuario_ADTO> lista = usuarioBI.obtieneUsuario(Integer.parseInt(idUsuario));

                if (!lista.isEmpty()) {
                    object.addProperty("ceco", lista.get(0).getIdCeco());
                    json = object.toString();
                } else {
                    object.add("ceco", null);
                    json = object.toString();
                }
            } else {
                object.add("ceco", null);
                json = object.toString();
            }

            response.setContentType("application/json");
            response.getWriter().write(json);

        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

    // http://localhost:8080/checklist/services/getCecoByUsuarioGET.json?idUsuario=<?>
    @RequestMapping(value = "/getCecoByUsuarioGET", method = RequestMethod.GET)
    public @ResponseBody
    String getCecoByUsuarioGET(HttpServletRequest request, HttpServletResponse response) {

        String json = "";
        JsonObject object = new JsonObject();
        try {
            String idUsuario = request.getParameter("idUsuario");
            List<Usuario_ADTO> lista = usuarioBI.obtieneUsuario(Integer.parseInt(idUsuario));
            if (!lista.isEmpty()) {
                object.addProperty("ceco", lista.get(0).getIdCeco());
                json = object.toString();
            } else {
                object.add("ceco", null);
                json = object.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

}
