package com.gruposalinas.migestion.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.AprobacionesRemedyLimpiezaHilos;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.DatosEmpMttoBI;
import com.gruposalinas.migestion.business.IncidenciasBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.ServiciosRemedyLimpiezaBI;
import com.gruposalinas.migestion.business.TicketCuadrillaLimpiezaBI;
import com.gruposalinas.migestion.business.TicketLimpiezaBI;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Aprobacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfInformacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfTracking;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Informacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Tracking;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub._Calificacion;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import com.gruposalinas.migestion.domain.FolioRemedyDTO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.domain.TicketLimpiezaDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.StrCipher;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/servicios")
public class ServiciosLimpieza {

    private static Logger logger = LogManager.getLogger(ServiciosLimpieza.class);

    @Autowired
    ServiciosRemedyLimpiezaBI serviciosRemedyLimpiezaBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    TicketLimpiezaBI ticketLimpiezaBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired // _serviceClient.getOptions().setProperty(HTTPConstants.CHUNKED,false);
    TicketCuadrillaLimpiezaBI ticketCuadrillaLimpiezaBI;

    @Autowired
    CecoBI cecoBI;

    private DatosEmpMttoBI datosEmpMttoBI;

    /////////////////// Gestion Movil de Limpieza ///////////////////
    // http://localhost:8080/migestion/servicios/getTipoIncidenciasLimpieza.json
    @RequestMapping(value = "/getTipoIncidenciasLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getTipoIncidenciasLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {

            List<IncidenciasDTO> lista = incidenciasBI.obtieneInfoLimpieza();

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray incidenciasJsonArray = new JsonArray();

            if (!lista.isEmpty() && lista != null) {
                for (IncidenciasDTO ins : lista) {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("ID_TIPO", ins.getIdTipo());
                    insJsonObj.addProperty("N1_SERVICIO", ins.getServicio());
                    insJsonObj.addProperty("N2_UBICACION", ins.getUbicacion());
                    insJsonObj.addProperty("N3_INCIDENCIA", ins.getIncidencia());
                    insJsonObj.addProperty("PLANTILLA", ins.getPlantilla());
                    insJsonObj.addProperty("STATUS", ins.getStatus());
                    incidenciasJsonArray.add(insJsonObj);
                }
            }

            if (lista.isEmpty()) {
                envoltorioJsonObj.add("FALLAS", null);
            } else {
                envoltorioJsonObj.add("FALLAS", incidenciasJsonArray);
            }
            // logger.info("JSON Calificaciones: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/altaIncidenteLimpiezaPost.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/altaIncidenteLimpiezaPost", method = RequestMethod.POST)
    public @ResponseBody
    String altaIncidenteLimpiezaPost(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        // UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        // String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                // urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

            } else if (idLlave == 666) {
                // urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            int ceco = 0;
            String nomSucursal = "";
            String telSucursal = "";
            int numEmpleado = 0;
            String nombre = "";
            String puesto = "";
            String comentario = "";
            String puestoAlterno = "";
            String telUsrAlterno = "";
            String nomUsrAlterno = "";
            String servicio = "";
            String ubicacion = "";
            String incidencia = "";
            String nomAdjunto = "";
            String adjunto = "";
            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ceco = rec.getInt("ceco");
                nomSucursal = rec.getString("nomSucursal");
                telSucursal = rec.getString("telSucursal");
                numEmpleado = rec.getInt("numEmpleado");
                nombre = rec.getString("nombre");
                puesto = rec.getString("puesto");
                comentario = rec.getString("comentario");
                puestoAlterno = rec.getString("puestoAlterno");
                telUsrAlterno = rec.getString("telUsrAlterno");
                nomUsrAlterno = rec.getString("nomUsrAlterno");
                servicio = rec.getString("servicio");
                ubicacion = rec.getString("ubicacion");
                incidencia = rec.getString("incidencia");
                if (!rec.getString("nomAdjunto").contains("null")) {

                    nomAdjunto = rec.getString("nomAdjunto");

                    if (!rec.getString("nomAdjunto").contains(".jpg")) {

                        nomAdjunto = rec.getString("nomAdjunto") + ".jpg";
                    }

                } else {
                    nomAdjunto = null;
                }
                if (!rec.getString("adjunto").contains("null")) {
                    adjunto = rec.getString("adjunto");
                } else {
                    adjunto = null;
                }
            }

            int noSucursal = 0;
            String cecoS = ceco + "";
            if (cecoS.length() == 6) {
                noSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
            }
            if (cecoS.length() == 5) {
                noSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
            }
            CorreosLotus correosLotus = new CorreosLotus();
            // logger.info("Correo SUCURSAL: " +
            // correosLotus.validaUsuarioLotusCorreos("331952"));
            // logger.info("Correo SUCURSAL: " +
            // correosLotus.validaUsuarioLotusCorreos("G0100"));
            // logger.info("Correo SUCURSAL:
            // "+correosLotus.validaUsuarioLotusCorreos(cecoS));
            String correo = "";

            correo = correosLotus.validaCorreoSuc(cecoS);

            FolioRemedyDTO bean = new FolioRemedyDTO();
            bean.setCeco(ceco);
            bean.setComentario(comentario);
            bean.setCorreo(correo);
            bean.setIncidencia(incidencia);
            bean.setNombre(nombre);
            bean.setNomSucursal(nomSucursal);
            bean.setNomUsrAlterno(nomUsrAlterno);
            bean.setNoSucursal(noSucursal);
            bean.setNumEmpleado(numEmpleado);
            bean.setPuesto(puesto);
            bean.setPuestoAlterno(puestoAlterno);
            bean.setServicio(servicio);
            bean.setTelSucursal(telSucursal);
            bean.setTelUsrAlterno(telUsrAlterno);
            bean.setUbicacion(ubicacion);
            bean.setNomAdjunto(nomAdjunto);
            bean.setAdjunto(adjunto);

            try {

                ParametroBI parametro = (ParametroBI) GTNAppContextProvider.getApplicationContext().getBean("parametroBI");
                List<ParametroDTO> parametros = parametro.obtieneParametros("validaCecoLim");
                String valor = parametros.get(0).getValor();

                if (valor.equals("1") && noSucursal != 0) {

                    int cecoRedUnica = generaCecoRedUnica(noSucursal, String.valueOf(ceco));
                    bean.setCeco(cecoRedUnica);

                }

            } catch (Exception e) {

                logger.info("AP con el parámetro validaCecoM" + e);

            }

            logger.info(bean.getNomAdjunto());
            int idFolio = serviciosRemedyLimpiezaBI.levantaFolio(bean);
            // int idFolio = 1;

            JsonObject envoltorioJsonObj = new JsonObject();

            if (idFolio == 0) {
                bean.setNoSucursal(ceco);
                // idFolio = serviciosRemedyLimpiezaBI.levantaFolio(bean);
                if (idFolio == 0) {
                    envoltorioJsonObj.add("FOLIO", null);
                } else {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("ID_FOLIO", idFolio);
                    envoltorioJsonObj.add("FOLIO", insJsonObj);
                }
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("ID_FOLIO", idFolio);
                envoltorioJsonObj.add("FOLIO", insJsonObj);
            }
            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    public int generaCecoRedUnica(int prefijoSucursal, String ceco) {

        String eco = ceco.substring(2, ceco.length());

        switch (prefijoSucursal) {

            case 39:
                logger.info("39 Recibido CECO: " + ceco);
                return Integer.parseInt("52" + eco);

            case 92:
                return Integer.parseInt("48" + eco);

            case 48:
                return Integer.parseInt("48" + eco);

            case 52:
                return Integer.parseInt("52" + eco);

            case 55:
    		//Validar el canal del ceco
    		try {

                List<CecoDTO> res = cecoBI.buscaCecos((prefijoSucursal + "" + eco), "", "", "", "", "", "");

                int idCanal = 0;

                if (res.get(0).getIdCanal() == 21) {

                    return Integer.parseInt("48" + eco);

                } else {

                    return Integer.parseInt("55" + eco);
                }

            } catch (Exception e) {

                return Integer.parseInt("55" + eco);
            }

            default:
                return Integer.parseInt(ceco);

        }

    }

    // http://localhost:8080/migestion/servicios/getIncidentesSucursal.json?ceco=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesLimpiezaSucursal", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesLimpiezaSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        ArrayList<TicketLimpiezaDTO> arrIncidentesAbiertos = null;
        String jsonRepresentation = null;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String ceco = urides.split("&")[1].split("=")[1];

            // int idFolio=0;
            // ArrayOfInformacion arrIncidentes =
            // serviciosRemedyLimpiezaBI.ConsultaFoliosSucursal(numSucursal,"0");
            int numSucursal = 0;
            String cecoS = ceco + "";

            if (cecoS.length() == 6) {
                numSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
            }
            if (cecoS.length() == 5) {
                numSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
            }

            arrIncidentesAbiertos = serviciosRemedyLimpiezaBI.ConsultaFoliosSucursal(numSucursal, "0");

            ArrayList<TicketLimpiezaDTO> ejemplo_ = new ArrayList<>();
            ejemplo_.add(new TicketLimpiezaDTO());
            Gson gson = new GsonBuilder().serializeNulls().create();
            jsonRepresentation = gson.toJson(arrIncidentesAbiertos);
            System.out.println(jsonRepresentation);

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        String respuesta = null;

        if (jsonRepresentation == null) {
            respuesta = "{\"FOLIOS\":[]}";
        } else {
            respuesta = "{\"FOLIOS\":" + jsonRepresentation + "}";
        }
        return respuesta;
    }

    // http://localhost:8080/migestion/servicios/getDetalleIncidenteLimpieza.json?idFolio=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleIncidenteLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getDetalleIncidenteLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idFolio = urides.split("&")[1].split("=")[1];

            // int numSucursal=Integer.parseInt(ceco.substring(2, 6));
            String imagen = serviciosRemedyLimpiezaBI.ConsultaAdjunto(Integer.parseInt(idFolio));
            ArrayOfTracking arrTraking = serviciosRemedyLimpiezaBI.ConsultaBitacora(Integer.parseInt(idFolio));

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray incidenciasJsonArray = new JsonArray();
            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null) {
                    for (Tracking aux : traking) {
                        String detalle = aux.getDetalle();
                        if (detalle.contains("||")) {

                            String arrAux[] = detalle.split("\\|\\|");
                            String estado = detalle.split("\\|\\|")[1];
                            String motivoEstado = detalle.split("\\|\\|")[3];
                            Date date = aux.getFechaEnvio().getTime();
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String date1 = format1.format(date);

                            // logger.info("estado: " + estado + ", motivoEstado: " + motivoEstado + ",
                            // fecha: " + date1);
                            JsonObject detalleJsonObj = new JsonObject();
                            detalleJsonObj.addProperty("ESTADO", estado);
                            detalleJsonObj.addProperty("MOTIVO_ESTADO", motivoEstado);
                            detalleJsonObj.addProperty("FECHA", date1);
                            incidenciasJsonArray.add(detalleJsonObj);
                        }

                    }
                }
            }

            if (arrTraking == null) {
                envoltorioJsonObj.add("TRAKING", null);
                envoltorioJsonObj.addProperty("IMAGEN", imagen);
            } else {
                envoltorioJsonObj.add("TRAKING", incidenciasJsonArray);
                envoltorioJsonObj.addProperty("IMAGEN", imagen);
            }
            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getAceptaClienteLimpieza.json?idUsuario=<?>&idIncidente=<?>&calificaion=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAceptaClienteLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getAceptaClienteLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            int cal = Integer.parseInt(urides.split("&")[2].split("=")[1]);
            String justificacion = urides.split("&")[3].split("=")[1];

            boolean autoriza = false;

            boolean actIncidente = false;

            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en recepción")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("cierre técnico")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyLimpiezaBI.aceptaCliente(idIncidente, cal, justificacion);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/postRechazaClienteLimpieza.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postRechazaClienteLimpieza", method = RequestMethod.POST)
    public @ResponseBody
    String postRechazaClienteLimpieza(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {
        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyLimpiezaBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";
            int dias = 0;

            String nomAdjunto = "";
            String adjunto = "";
            String solicitante = "";
            String aprobador = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                // logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("idFolio");
                justificacion = rec.getString("justificacion");
                nomAdjunto = (rec.getString("nomAdjunto").contains(".jpg")) ? rec.getString("nomAdjunto") : rec.getString("nomAdjunto") + ".jpg";
                adjunto = rec.getString("adjunto");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
            }
            boolean autoriza = false;

            boolean actIncidente = false;

            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en recepción")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("cierre técnico")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyLimpiezaBI.rechazaCliente(Integer.parseInt(idFolio), justificacion,
                        nomAdjunto, adjunto, solicitante, aprobador);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    /////////////////// Gestion Movil de Limpieza ///////////////////
    /////////////////// Proveedores de Limpieza ///////////////////
    // http://localhost:8080/migestion/servicios/getProveedoresLimpieza.json
    @RequestMapping(value = "/getProveedoresLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getProveedoresLimpieza(HttpServletRequest request, HttpServletResponse response) {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int noSucursal = 0;
            int idCeco = noSucursal;

            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();
            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray provJsonArray = new JsonArray();

            if (!lista.isEmpty()) {
                for (ProveedoresDTO aux : lista) {
                    if (aux.getStatus().contains("Activo")) {
                        JsonObject provJsonObj = new JsonObject();
                        provJsonObj.addProperty("ID_PROVEEDOR", aux.getIdProveedor());
                        provJsonObj.addProperty("MENU", aux.getMenu());
                        provJsonObj.addProperty("NOMBRE_CORTO", aux.getNombreCorto());
                        provJsonObj.addProperty("RAZON_SOCIAL", aux.getRazonSocial());
                        provJsonObj.addProperty("STATUS", aux.getStatus());
                        provJsonArray.add(provJsonObj);
                    }
                }
            }
            if (lista.isEmpty()) {
                envoltorioJsonObj.add("PROVEEDORES", null);
            } else {
                envoltorioJsonObj.add("PROVEEDORES", provJsonArray);
            }
            // logger.info("JSON Calificaciones: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }
        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getBandejaTicketsLimpieza.json?idUsuario=<?>&idUsuarioBandeja=<?>&tipoUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBandejaTicketsLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getBandejaTicketsLimpieza(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            if (urides.split("&").length == 2) {

                String idUsuario = urides.split("&")[0].split("=")[1];
                String tipoUsuario = urides.split("&")[1].split("=")[1];

                res = ticketLimpiezaBI.getBandejaTickets(idUsuario, Integer.parseInt(tipoUsuario));
            } else {
                String idUsuario = urides.split("&")[0].split("=")[1];
                // String razonSocial = urides.split("&")[1].split("=")[1];
                String idProveedor = urides.split("&")[1].split("=")[1];
                String tipoUsuario = urides.split("&")[2].split("=")[1];

                // Implementa buscar el idProveedor
                List<ProveedoresDTO> proveedoresArray = proveedoresBI.obtieneDatos(Integer.parseInt(idProveedor));
                ProveedoresDTO proveedorDTO = null;

                if (proveedoresArray != null && proveedoresArray.size() > 0) {

                    proveedorDTO = proveedoresArray.get(0);
                    // res = ticketLimpiezaBI.getBandejaTickets(proveedorDTO.getRazonSocial(),
                    // Integer.parseInt(tipoUsuario));
                    res = ticketLimpiezaBI.getBandejaTickets(proveedorDTO.getNombreCorto(),
                            Integer.parseInt(tipoUsuario));

                } else {
                    res = "{}";
                }

                // res = ticketManrenimientoBI.getBandejaTickets(razonSocial,
                // Integer.parseInt(tipoUsuario));
            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            res = "{}";
        }

        return res;

    }

    // http://localhost:8080/migestion/servicios/getBandejaCuadrillaLimpieza.json?idUsuario=<?>&idUsuarioBandeja=<?>&cuadrilla=<?>&zona=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBandejaCuadrillaLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getBandejaCuadrillaLimpieza(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            // String idUsuario = urides.split("&")[0].split("=")[1];
            String proveedor = urides.split("&")[1].split("=")[1];
            String cuadrilla = urides.split("&")[2].split("=")[1];
            String zona = urides.split("&")[3].split("=")[1];

            res = ticketLimpiezaBI.getBandejaCuadrilla(proveedor, Integer.parseInt(cuadrilla), Integer.parseInt(zona));

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            res = "{}";
        }

        return res;

    }

    // http://localhost:8080/migestion/servicios/getDetalleIncidenteClickLimpieza.json?idUsuario=<?>&idTicket=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleIncidenteClickLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getDetalleIncidenteClickLimpieza(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        JsonObject respuesta = new JsonObject();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idTicket = urides.split("&")[1].split("=")[1];

            int idFolio = Integer.parseInt(idTicket);

            ArrayOfInformacion arrIncidentes = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(idFolio);

            if (arrIncidentes.getInformacion() != null && arrIncidentes.getInformacion().length > 0) {

                AprobacionesRemedyLimpiezaHilos hiloAprobacion = new AprobacionesRemedyLimpiezaHilos(idFolio, 1);
                hiloAprobacion.start();

                while (hiloAprobacion.isAlive()) {

                }

                hiloAprobacion.getRespuesta();

                respuesta = arrOrdenado(arrIncidentes.getInformacion()[0]);

            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        return respuesta.toString();
    }

    // http://localhost:8080/migestion/servicios/getAceptaTicketProvLimpieza.json?idUsuario=<?>&idIncidente=<?>&idProveedor=<?>&idCuadrilla=<?>&zona=<?>
    // @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAceptaTicketProvLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getAceptaTicketProvLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String idProvedor = urides.split("&")[2].split("=")[1];
            String idCuadrilla = urides.split("&")[3].split("=")[1];
            String idZona = urides.split("&")[4].split("=")[1];

            boolean rechazaProveeedor = false;

            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {

                        if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("asignado por aceptar")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }

            String lider = "";

            if (resp == 1) {
                resp = 2;
            } else {

                List<CuadrillaDTO> lista2 = cuadrillaBI.obtieneDatos(Integer.parseInt(idProvedor));
                for (CuadrillaDTO aux2 : lista2) {
                    int idCuadrillaI = Integer.parseInt(idCuadrilla);
                    int idZonaI = Integer.parseInt(idZona);
                    if (aux2.getIdCuadrilla() == idCuadrillaI && aux2.getZona() == idZonaI) {
                        lider = aux2.getLiderCuad();
                    }
                }

                rechazaProveeedor = serviciosRemedyLimpiezaBI.autorizaProveedor(idIncidente, "",
                        idProvedor + "_" + idZona + "_" + idCuadrilla);

            }

            if (resp != 2) {
                if (rechazaProveeedor) {
                    List<TicketCuadrillaDTO> lista2 = ticketCuadrillaLimpiezaBI.obtieneDatos(idIncidente);
                    for (TicketCuadrillaDTO aux2 : lista2) {
                        if (aux2.getStatus() != 0) {
                            aux2.setStatus(0);
                            ticketCuadrillaLimpiezaBI.actualiza(aux2);
                            // logger.info("modificando status" +
                            // ticketCuadrillaLimpiezaBI.actualiza(aux2));
                        }
                    }

                    TicketCuadrillaDTO bean = new TicketCuadrillaDTO();
                    bean.setIdCuadrilla(Integer.parseInt(idCuadrilla));
                    bean.setIdProveedor(Integer.parseInt(idProvedor));
                    bean.setTicket("" + idIncidente);
                    bean.setZona(Integer.parseInt(idZona));
                    bean.setLiderCua(lider);
                    bean.setStatus(1);
                    ticketCuadrillaLimpiezaBI.inserta(bean);
                    // logger.info(ticketCuadrillaLimpiezaBI.inserta(bean));
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!rechazaProveeedor) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getDeclinaTicketProvLimpieza.json?idUsuario=<?>&idIncidente=<?>&justificacion=<?>
    // @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDeclinaTicketProvLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getDeclinaTicketProvLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String justificacion = urides.split("&")[2].split("=")[1];
            String idProveedor = urides.split("&")[3].split("=")[1];

            boolean rechazaProveeedor = false;

            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {

                        if ((aux.getEstado().trim().toLowerCase().equals("en proceso") && aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor"))
                                || (aux.getEstado().trim().toLowerCase().equals("en recepción") && aux.getMotivoEstado().trim().toLowerCase().equals("en validacion"))
                                || (aux.getEstado().trim().toLowerCase().equals("recibido por atender") && aux.getMotivoEstado().trim().toLowerCase().equals("asignado por aceptar"))) {

                            resp = 0;

                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {

                /*
				 * List<ProveedoresDTO> proveedoresArray =
				 * proveedoresBI.obtieneDatos(Integer.parseInt(idProveedor)); ProveedoresDTO
				 * proveedorDTO= null;
				 *
				 * if(proveedoresArray != null && proveedoresArray.size() > 0) {
				 *
				 * proveedorDTO = proveedoresArray.get(0); //res =
				 * ticketLimpiezaBI.getBandejaTickets(proveedorDTO.getRazonSocial(),
				 * Integer.parseInt(tipoUsuario)); res =
				 * ticketLimpiezaBI.getBandejaTickets(proveedorDTO.getNombreCorto(),
				 * Integer.parseInt(tipoUsuario));
				 *
				 * }else { res = "{}"; }
                 */
                rechazaProveeedor = serviciosRemedyLimpiezaBI.declinaProveedor(idIncidente, justificacion, idProveedor);// pide
                // el
                // id
                // Proveedor
            }

            if (resp != 2) {
                if (rechazaProveeedor) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!rechazaProveeedor) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/postCierreTecnicoRemotoLimpieza.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postCierreTecnicoRemotoLimpieza", method = RequestMethod.POST)
    public @ResponseBody
    String postCierreTecnicoRemotoLimpieza(@RequestBody String provider,
            HttpServletRequest request, HttpServletResponse response, Model model)
            throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyLimpiezaBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                //// logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //// logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("postCierreTecnicoRemotoLimpieza LIMPIEZA JSON IOS:  " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                //logger.info("postCierreTecnicoRemotoLimpieza LIMPIEZA JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            logger.info("postCierreTecnicoRemotoLimpieza Cierre técnico remoto LIMPIEZA USUARIO: " + usuario);
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";

            String nomAdjunto = "";
            String adjunto = "";
            String nomAdjunto2 = "";
            String adjunto2 = "";

            String solicitante = "";
            String aprobador = "";
            String clienteAvisado = "";
            // String OT = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                logger.info("postCierreTecnicoRemotoLimpieza Cierre técnico remoto CONTENIDO: " + rec);
                //// logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = (rec.getString("nomAdjunto").contains(".jpg")) ? rec.getString("nomAdjunto") : rec.getString("nomAdjunto") + ".jpg";
                adjunto = rec.getString("adjunto");
                nomAdjunto2 = (rec.getString("nomAdjunto2").contains(".jpg")) ? rec.getString("nomAdjunto2") : rec.getString("nomAdjunto2") + ".jpg";
                adjunto2 = rec.getString("adjunto2");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
                clienteAvisado = rec.getString("clienteAvisado");
                // OT = rec.getString("OT");
            }

            // Validacion para no avanzar si las fotos pesan mas de 2.0 mb y no impactar
            // monitoreo
            if (adjunto.getBytes().length > 2500000) {
                logger.info("Cierre técnico remoto ADUNTO 1 MUY PESADO");
                // logger.info("Usuario = " + usuario);
                // logger.info("folio = " + idFolio);
                // logger.info("solicitante = " + solicitante);
                // logger.info("Adjunto 1 size = " + adjunto.length());
                return "{}";
            }

            if (adjunto2.getBytes().length > 2500000) {
                logger.info("Cierre técnico remoto ADUNTO 2 MUY PESADO");
                // logger.info("Usuario = " + usuario);
                // logger.info("folio = " + idFolio);
                // logger.info("solicitante = " + solicitante);
                // logger.info("Adjunto 2 size = " + adjunto2.length());
                return "{}";
            }

            boolean actIncidente = false;

            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        logger.info("postCierreTecnicoRemotoLimpieza: " + aux);

                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux
                                .getMotivoEstado().trim().toLowerCase().equals("rechazado por cliente"))) {
                            resp = 0;
                            logger.info("postCierreTecnicoRemotoLimpieza - SI procede cierre ");

                        } else {
                            resp = 1;
                            logger.info("postCierreTecnicoRemotoLimpieza - NO procede cierre ");
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
                logger.info("postCierreTecnicoRemotoLimpieza - NO procede cierre 2");

            } else {
                logger.info("postCierreTecnicoRemotoLimpieza - SI procede cierre en espera de idFolio ");

                if (idFolio != null && !idFolio.equals("")) {
                    actIncidente = serviciosRemedyLimpiezaBI.cierreTecnicoRemoto(Integer.parseInt(idFolio),
                            justificacion, nomAdjunto, adjunto, nomAdjunto2, adjunto2, solicitante, aprobador,
                            clienteAvisado);
                    logger.info("postCierreTecnicoRemotoLimpieza - SI procede cierre con idFolio: " + idFolio);
                    logger.info("Respuesta postCierreTecnicoRemotoLimpieza: " + actIncidente);

                }
            }

            logger.info("postCierreTecnicoRemotoLimpieza valor de resp: " + resp);
            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();
            logger.info("postCierreTecnicoRemotoLimpieza valor de Obj " + resp);

        } catch (Exception e) {
            logger.info("AP postCierreTecnicoRemotoLimpieza" + e);
            e.printStackTrace();
            logger.info("postCierreTecnicoRemotoLimpieza respuesta {}");
            return "{}";
        }

        if (res != null) {
            logger.info("postCierreTecnicoRemotoLimpieza respuesta " + res.toString());
            return res.toString();
        } else {
            logger.info("postCierreTecnicoRemotoLimpieza respuesta {}");
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/postCierreTecnicoSitioLimpieza.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postCierreTecnicoSitioLimpieza", method = RequestMethod.POST)
    public @ResponseBody
    String postCierreTecnicoSitioLimpieza(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyLimpiezaBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";

            String nomAdjunto = "";
            String adjunto = "";
            String nomAdjunto2 = "";
            String adjunto2 = "";

            // String latitud = "";
            // String longitud = "";
            String solicitante = "";
            String aprobador = "";

            String clienteAvisado = "";
            // String OT = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                //// logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = (rec.getString("nomAdjunto").contains(".jpg")) ? rec.getString("nomAdjunto") : rec.getString("nomAdjunto") + ".jpg";
                adjunto = rec.getString("adjunto");
                nomAdjunto2 = (rec.getString("nomAdjunto2").contains(".jpg")) ? rec.getString("nomAdjunto2") : rec.getString("nomAdjunto2") + ".jpg";
                adjunto2 = rec.getString("adjunto2");
                // latitud = rec.getString("latitud");
                // longitud = rec.getString("longitud");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
                clienteAvisado = rec.getString("clienteAvisado");
                // OT = rec.getString("OT");
            }

            // Validacion para no avanzar si las fotos pesan mas de 2.0 mb y no impactar
            // monitoreo
            if (adjunto.getBytes().length > 2000000) {
                // logger.info("Usuario = " + usuario);
                // logger.info("folio = " + idFolio);
                // logger.info("solicitante = " + solicitante);
                // logger.info("Adjunto 1 size = " + adjunto.length());
                return "{}";
            }

            if (adjunto2.getBytes().length > 2000000) {
                // logger.info("Usuario = " + usuario);
                // logger.info("folio = " + idFolio);
                // logger.info("solicitante = " + solicitante);
                // logger.info("Adjunto 2 size = " + adjunto2.length());
                return "{}";
            }

            boolean actIncidente = false;

            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux
                                .getMotivoEstado().trim().toLowerCase().equals("rechazado por cliente"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                if (idFolio != null && !idFolio.equals("")) {
                    // actIncidente =
                    // serviciosRemedyLimpiezaBI.cierreTecnicoSitio(Integer.parseInt(idFolio),
                    // justificacion, nomAdjunto, adjunto, nomAdjunto2, adjunto2, latitud, longitud,
                    // solicitante, aprobador, clienteAvisado, OT);
                    actIncidente = serviciosRemedyLimpiezaBI.cierreTecnicoSitio(Integer.parseInt(idFolio),
                            justificacion, nomAdjunto, adjunto, nomAdjunto2, adjunto2, solicitante, aprobador,
                            clienteAvisado);
                }
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //// logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getCuadrillasLimpieza.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getCuadrillasLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getCuadrillas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if (urides.length() > 0) {
                String idProveedor = urides.split("&")[1].split("=")[1];
                if (idProveedor.matches("\\d+(\\.\\d+)?")) {
                    List<CuadrillaDTO> lista = cuadrillaBI.obtieneDatos(Integer.parseInt(idProveedor));

                    ArrayList<CuadrillaDTO> ZNorte = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZCentro = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZSur = new ArrayList<CuadrillaDTO>();

                    if (!lista.isEmpty() && lista != null) {
                        for (CuadrillaDTO aux : lista) {
                            if (aux.getZona() == 0) {
                                ZNorte.add(aux);
                            }
                            if (aux.getZona() == 1) {
                                ZCentro.add(aux);
                            }
                            if (aux.getZona() == 2) {
                                ZSur.add(aux);
                            }
                        }
                    }

                    JsonArray ZNJsonArray = new JsonArray();
                    if (!ZNorte.isEmpty() && ZNorte != null) {

                        for (CuadrillaDTO aux : ZNorte) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZNJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZCJsonArray = new JsonArray();
                    if (!ZCentro.isEmpty() && ZCentro != null) {

                        for (CuadrillaDTO aux : ZCentro) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZCJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZSJsonArray = new JsonArray();
                    if (!ZSur.isEmpty() && ZSur != null) {

                        for (CuadrillaDTO aux : ZSur) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZSJsonArray.add(incJsonObj);
                        }

                    }

                    JsonObject envoltorioJsonObj = new JsonObject();

                    if (lista == null) {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    } else {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    }
                    // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                    res = envoltorioJsonObj.toString();
                } else {
                    return "{}";
                }
            } else {
                return "{}";
            }
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getAltaCuadrillaLimpieza.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getAltaCuadrillaLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getAltaCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if (urides.length() > 0) {
                String idProveedor = urides.split("&")[1].split("=")[1];
                String zona = urides.split("&")[2].split("=")[1];
                String liderCuad = urides.split("&")[3].split("=")[1];
                String correo = urides.split("&")[4].split("=")[1];
                String segundo = urides.split("&")[5].split("=")[1];
                String password = urides.split("&")[6].split("=")[1];

                CuadrillaDTO cua = new CuadrillaDTO();

                cua.setIdProveedor(Integer.parseInt(idProveedor));
                cua.setZona(Integer.parseInt(zona));
                cua.setLiderCuad(liderCuad);
                cua.setCorreo(correo);
                cua.setSegundo(segundo);
                cua.setPassw(password);

                int resp = cuadrillaBI.inserta(cua);

                JsonObject envoltorioJsonObj = new JsonObject();

                if (resp != 0) {

                    boolean respuesta = serviciosRemedyLimpiezaBI.crearCuadrillas(idProveedor + "_" + zona + "_" + resp,
                            segundo, password, correo, liderCuad, Integer.parseInt(idProveedor), "55", zona);
                    // logger.info("respuesta: " + respuesta);

                    envoltorioJsonObj.addProperty("ALTA", 1);
                } else {
                    envoltorioJsonObj.addProperty("ALTA", 0);

                }

                // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();
            } else {
                return "{}";
            }
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getModificaCuadrillaLimpieza.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getModificaCuadrillaLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getModificaCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idCuadrilla = urides.split("&")[1].split("=")[1];
            String idProveedor = urides.split("&")[2].split("=")[1];
            String zona = urides.split("&")[3].split("=")[1];
            String liderCuad = urides.split("&")[4].split("=")[1];
            String correo = urides.split("&")[5].split("=")[1];
            String segundo = urides.split("&")[6].split("=")[1];
            String pwd = urides.split("&")[7].split("=")[1];

            CuadrillaDTO cua = new CuadrillaDTO();

            cua.setIdCuadrilla(Integer.parseInt(idCuadrilla));
            cua.setIdProveedor(Integer.parseInt(idProveedor));
            cua.setZona(Integer.parseInt(zona));
            cua.setLiderCuad(liderCuad);
            cua.setCorreo(correo);
            cua.setSegundo(segundo);
            cua.setPassw(pwd);

            boolean resp = cuadrillaBI.actualiza(cua);

            JsonObject envoltorioJsonObj = new JsonObject();

            if (resp) {
                boolean respuesta = serviciosRemedyLimpiezaBI.actualizaCuadrillas(
                        idProveedor + "_" + zona + "_" + idCuadrilla, segundo, pwd, correo, liderCuad,
                        Integer.parseInt(idProveedor), "55", zona);
                envoltorioJsonObj.addProperty("MODIFICADO", 1);
            } else {
                envoltorioJsonObj.addProperty("MODIFICADO", 0);
            }

            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    /////////////////// Proveedores de Limpieza ///////////////////
    /////////////////// Metodos procesamiento de Limpieza ///////////////////
    // http://localhost:8080/migestion/servicios/getInfoCierreLimpieza.json?idUsuario=<?>&idTicket=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getInfoCierreLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getInfoCierreLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String respuesta = null;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idTicket = urides.split("&")[1].split("=")[1];

            int idFolio = Integer.parseInt(idTicket);
            int opcion = Integer.parseInt(urides.split("&")[2].split("=")[1]);

            respuesta = serviciosRemedyLimpiezaBI.getInfoAprobaciones(idFolio, opcion);

            if (respuesta == null) {
                return "{}";
            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        return respuesta;
    }

    // public JsonObject arrOrdenado(Informacion incidente, Aprobacion
    // AprobacionesHashMap) {
    public JsonObject arrOrdenado(Informacion incidente) {

        JsonArray arrJsonArray = new JsonArray();
        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();
        // HashMap<String, String> EmpleadosRemedyHashMap =
        // serviciosRemedyLimpiezaBI.consultaUsuariosRemedy();

        Informacion aux = incidente;

        JsonObject incJsonObj = new JsonObject();

        // aux.getAutorizaCambioMonto();
        incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
        incJsonObj.addProperty("ESTADO", aux.getEstado());
        incJsonObj.addProperty("FALLA", aux.getFalla());
        incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
        incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
        incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
        incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
        incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
        incJsonObj.addProperty("NUMEMP_COORDINADOR", 0);

        String idCliente = aux.getIdCorpCliente();
        if (idCliente != null) {
            if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                datosEmpMttoBI = (DatosEmpMttoBI) GTNAppContextProvider.getApplicationContext()
                        .getBean("datosEmpMttoBI");
                List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI
                        .obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                if (!datos_cliente.isEmpty() && datos_cliente != null) {
                    for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                        incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                        incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                        EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                    }
                } else {
                    incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                    incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                }
            } else {
                DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
            }
        } else {
            incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
            incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
        }
        // String idCord = aux.getIDCorpCoordinador();

        incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
        incJsonObj.addProperty("COORDINADOR", "* Sin información");

        // String idSup = aux.getIdCorpSupervisor();
        String idSup = null;

        if (idSup != null) {
            /*
			 * if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
			 * incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información"); } else {
			 * incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
			 * }
             */
        } else {
            incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
        }
        if (idSup != null) {
            /*
			 * if (!idSup.matches("[^0-9]*")) {
			 *
			 * if (!EmpleadosHashMap.containsKey(idSup.trim())) { List<DatosEmpMttoDTO>
			 * datos_supervidor =
			 * datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));
			 *
			 * if (!datos_supervidor.isEmpty() && datos_supervidor != null) { for
			 * (DatosEmpMttoDTO emp_aux : datos_supervidor) {
			 * incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " +
			 * emp_aux.getNombre()); incJsonObj.addProperty("NOM_SUPERVISOR",
			 * emp_aux.getNombre()); EmpleadosHashMap.put(idSup.trim(), emp_aux); } } else {
			 * incJsonObj.addProperty("SUPERVISOR", "* Sin información");
			 * incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información"); } } else {
			 * DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
			 * incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " +
			 * emp_aux.getNombre()); incJsonObj.addProperty("NOM_SUPERVISOR",
			 * emp_aux.getNombre()); }
			 *
			 * } else { incJsonObj.addProperty("SUPERVISOR", "* Sin información");
			 * incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información"); }
             */
        } else {
            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
        }

        incJsonObj.addProperty("NOTAS", aux.getNotas());
        incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
        incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

        if (aux.getProveedor() != null) {
            if (!lista.isEmpty()) {
                for (ProveedoresDTO aux3 : lista) {
                    if (aux3.getStatus().contains("Activo")) {
                        if (aux.getProveedor().toLowerCase().trim()
                                .contains(aux3.getNombreCorto().toLowerCase().trim())) {
                            incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                            incJsonObj.addProperty("MENU", aux3.getMenu());
                            incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                            incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                            break;
                        }

                    }
                }
            }
        } else {
            String nomCorto = null;
            incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
            incJsonObj.addProperty("MENU", nomCorto);
            incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
            incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
        }

        incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
        incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
        incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
        incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
        incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

        incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
        incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
        Date date; // = aux.getFechaCierre().getTime();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1; // = format1.format(date);
        incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "0001-01-01 00:00:00");
        // date = aux.getFechaCreacion().getTime();
        // date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_CREACION", format1.format(aux.getFechaModificacion().getTime()));
        date = aux.getFechaModificacion().getTime();
        date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_MODIFICACION", date1);
        // date = aux.getFechaProgramada().getTime();
        // date1 = format1.format(date);
        // incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
        incJsonObj.addProperty("FECHA_PROGRAMADA", "0001-01-01 00:00:00");
        incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
        incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());
        String autCleinte = aux.getAutorizacionCliente();
        if (autCleinte != null && !autCleinte.equals("")) {
            if (autCleinte.contains("Rechazado")) {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
            }
        } else {
            incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
        }

        if (aux.getTipoAtencion() != null) {
            incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
        } else {
            String x = null;
            incJsonObj.addProperty("TIPO_ATENCION", x);
        }
        if (aux.getOrigen() != null) {
            incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
        } else {
            String x = null;
            incJsonObj.addProperty("ORIGEN", x);
        }
        // -----------------Obtiene coordenadas de inicio y fin de ticket
        // -------------------
        incJsonObj.addProperty("LATITUD_FIN", "");
        incJsonObj.addProperty("LONGITUD_FIN", "");

        // ----------------------------------------------------------------------------------
        /*
		 * date = aux.getFechaInicioAtencion().getTime(); date1 = format1.format(date);
		 * incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);
         */
        incJsonObj.addProperty("FECHA_INICIO_ATENCION", "0001-01-01 00:00:00");

        int idIncidente = aux.getIdIncidencia();

        List<TicketCuadrillaDTO> lista2 = ticketCuadrillaLimpiezaBI.obtieneDatos(idIncidente);
        int idProvedor = 0;
        int idCuadrilla = 0;
        int idZona = 0;
        for (TicketCuadrillaDTO aux2 : lista2) {
            if (aux2.getStatus() != 0) {
                idProvedor = aux2.getIdProveedor();
                idCuadrilla = aux2.getIdCuadrilla();
                idZona = aux2.getZona();
            }

        }
        if (idProvedor != 0) {
            List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
            for (CuadrillaDTO aux2 : lista3) {
                if (aux2.getZona() == idZona) {
                    if (aux2.getIdCuadrilla() == idCuadrilla) {
                        incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                        incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                        incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                        incJsonObj.addProperty("CORREO", aux2.getCorreo());
                        incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                        incJsonObj.addProperty("ZONA", aux2.getZona());
                    }
                }
            }
        } else {
            String auxiliar = null;
            // incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
            incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
            incJsonObj.addProperty("LIDER", auxiliar);
            incJsonObj.addProperty("CORREO", auxiliar);
            incJsonObj.addProperty("SEGUNDO", auxiliar);
            incJsonObj.addProperty("ZONA", auxiliar);
        }

        if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor")
                || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada")
                || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
            // Aprobacion aprobacion =
            // serviciosRemedyLimpiezaBI.consultarAprobaciones(idIncidente, 3);

            boolean flag1 = false, flag2 = false;
            /*
			 * Aprobacion aprobacion = null;
			 *
			 * String estadoApr = null;
			 *
			 * if(aprobacion!=null) { estadoApr = aprobacion.getEstadoAprobacion();
			 *
			 * if (estadoApr != null) { String porvAprob = aprobacion.getSolicitadoPor(); if
			 * (porvAprob.contains("" + idProvedor)) {
			 * incJsonObj.addProperty("BANDERA_CMONTO", 1); flag1 = true; } else {
			 * incJsonObj.addProperty("BANDERA_CMONTO", 0); } } else {
			 * incJsonObj.addProperty("BANDERA_CMONTO", 0); } }
             */

            incJsonObj.addProperty("BANDERA_CMONTO", 0);

            // Aprobacion aprobacion2 =
            // serviciosRemedyLimpiezaBI.consultarAprobaciones(idIncidente, 1);
            /*
			 * Aprobacion aprobacion2 = null;
			 *
			 * if(aprobacion2!=null) { estadoApr = aprobacion2.getEstadoAprobacion(); if
			 * (estadoApr != null) { String porvAprob = aprobacion2.getSolicitadoPor(); if
			 * (porvAprob.contains("" + idProvedor)) {
			 * incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1); flag1 = true; } else {
			 * incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0); } } else {
			 * incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0); } }
             */
            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);

            if (flag1 == true && flag2 == true) {
                incJsonObj.addProperty("BANDERAS", 1);
            } else {
                incJsonObj.addProperty("BANDERAS", 0);
            }
        } else {
            incJsonObj.addProperty("BANDERA_CMONTO", 0);
            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
            incJsonObj.addProperty("BANDERAS", 0);
        }

        // ----------------------- Consulta Traking --------------------
        ArrayOfTracking arrTraking = serviciosRemedyLimpiezaBI.ConsultaBitacora(aux.getIdIncidencia());

        JsonArray incidenciasJsonArray = new JsonArray();

        ArrayList<Date> RechazoCliente = new ArrayList<Date>();
        ArrayList<Date> inicioAtencion = new ArrayList<Date>();
        ArrayList<Date> cierreTecnico = new ArrayList<Date>();
        ArrayList<Date> cancelados = new ArrayList<Date>();
        ArrayList<Date> atencionGarantia = new ArrayList<Date>();
        ArrayList<Date> cierreGarantia = new ArrayList<Date>();
        ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

        if (arrTraking != null) {
            Tracking[] traking = arrTraking.getTracking();
            if (traking != null) {
                for (Tracking aux2 : traking) {
                    String detalle = aux2.getDetalle();

                    if (detalle.contains("||")) {
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date2 = aux2.getFechaEnvio().getTime();
                        if (estado.toLowerCase().trim().equals("en recepción")
                                && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                            RechazoCliente.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en proceso")
                                && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                            inicioAtencion.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en recepción")
                                && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                            cierreTecnico.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido")
                                && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado")
                                || motivoEstado.trim().toLowerCase().equals("cancelado no aplica")
                                || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                            cancelados.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("en proceso")
                                && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                            atencionGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido")
                                && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                            cierreGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("recibido por atender")
                                && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                            rechazoProveedor.add(date2);
                        }
                    }

                }
            }
        }

        int posRC = 0, posRP = 0;

        String cadAux = null;
        if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < RechazoCliente.size(); i++) {
                if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = RechazoCliente.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
            posRC = pos2;
        } else {

            incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
        }

        if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < inicioAtencion.size(); i++) {
                if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = inicioAtencion.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_ATENCION", date3);
        } else {
            incJsonObj.addProperty("FECHA_ATENCION", cadAux);
        }

        if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cierreTecnico.size(); i++) {
                if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cierreTecnico.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
            incJsonObj.addProperty("FECHA_CIERRE", date3);
        } else {
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
            incJsonObj.addProperty("FECHA_CIERRE", cadAux);
        }

        if (cancelados != null && !cancelados.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cancelados.size(); i++) {
                if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cancelados.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_CANCELACION", date3);
        } else {
            incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
        }
        //
        if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < atencionGarantia.size(); i++) {
                if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = atencionGarantia.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
        } else {
            incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
        }

        if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cierreGarantia.size(); i++) {
                if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cierreGarantia.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
        } else {
            incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
        }

        if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < rechazoProveedor.size(); i++) {
                if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = rechazoProveedor.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
            posRP = pos2;
        } else {
            incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
        }

        // BANDERA_RECHAZO_CIERRE_PREVIO
        if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null
                && !RechazoCliente.isEmpty()) {
            if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
            } else {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
            }
        } else {
            incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
        }

        // ------------------------------------------------------------------
        int cal = 0;
        _Calificacion auxEval = aux.getEvalCliente();
        if (auxEval != null) {

            if (auxEval.getValue().contains("NA")) {
                cal = 1;
            }
            if (auxEval.getValue().contains("E0a50")) {
                cal = 2;
            }
            if (auxEval.getValue().contains("E51a69")) {
                cal = 3;
            }
            if (auxEval.getValue().contains("E70a89")) {
                cal = 4;
            }
            if (auxEval.getValue().contains("E90a100")) {
                cal = 5;
            }
        }

        incJsonObj.addProperty("CALIFICACION", cal);

        incJsonObj.addProperty("MONTO_REAL", 0.0);
        incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "0001-01-01 00:00:00");
        incJsonObj.addProperty("DIAS_APLAZAMIENTO", "0");
        /*
		 * incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());
		 *
		 * date = aux.getFechaCierreAdmin().getTime(); date1 = format1.format(date);
		 * incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);
		 * incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());
         */

        // ------------------------------ COMENTARIOS PARA PROVEEDOR
        // --------------------------
        // if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
        /*
		 * Aprobacion aprobacion =
		 * serviciosRemedyLimpiezaBI.consultarAprobaciones(idIncidente, 2);
		 * if(aprobacion!=null) { incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR",
		 * aprobacion.getJustSolicitante());
		 * incJsonObj.addProperty("AUTORIZACION_PROVEEDOR",
		 * aux.getAutorizacionProveedor()); date =
		 * aprobacion.getFechaModificacion().getTime(); date1 = format1.format(date);
		 * incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
		 * incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
		 * }
         */
 /*
		 * if (AprobacionesHashMap != null) { //Aprobacion aprobacion =
		 * AprobacionesHashMap;
		 *
		 * //incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR",
		 * aprobacion.getJustSolicitante());
		 * incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", "");
		 * incJsonObj.addProperty("AUTORIZACION_PROVEEDOR",
		 * aux.getAutorizacionProveedor()); /*date =
		 * aprobacion.getFechaModificacion().getTime(); date1 = format1.format(date);
		 * incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
         */
        // incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "0001-01-01 00:00:00");
        // incJsonObj.addProperty("DECLINAR_PROVEEDOR", "");
        // incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
        // Date dateI = aux.getFechaInicioAtencion().getTime();

        /*
		 * if (date.compareTo(dateI) >= 0) { incJsonObj.addProperty("LATITUD_INICIO",
		 * cord); incJsonObj.addProperty("LONGITUD_INICIO", cord); }
         */
        incJsonObj.addProperty("LATITUD_INICIO", "");
        incJsonObj.addProperty("LONGITUD_INICIO", "");
        /* } else */ {
            String auxApr = null;
            incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
            incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
            incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
            incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
        }

        // -----------------------------------------------------------------------------------
        // ----------------------------FECHA
        // CANCELACION----------------------------------------
        /*
		 * if( aux.getEstado().trim().toLowerCase().equals("atendido") &&
		 * (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||
		 * aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux
		 * .getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))
		 * ) { date = aux.getFechaModificacion().getTime(); date1 =
		 * format1.format(date); incJsonObj.addProperty("FECHA_CANCELACION", "" +
		 * date1); }
         */
        // -------------------------------------------------------------------------------------
        incJsonObj.addProperty("RESOLUCION", "");
        incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", 0.0);
        incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
        /*
		 * incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
		 * incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO",
		 * aux.getMontoEstimadoAutorizado());
         */

        return incJsonObj;

    }

    // http://localhost:8080/migestion/servicios/getConsultarAprobacionesLimpieza.json?idUsuario=<?>&idIncidente=<?>&opcion=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getConsultarAprobacionesLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getConsultarAprobaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String res1 = urides.split("&")[1].split("=")[1];
            String res2 = urides.split("&")[2].split("=")[1];
            if (!res1.matches("[^0-9]+$") && !res2.matches("[^0-9]+$")) {
                int idIncidente = Integer.parseInt(res1);
                int opcion = Integer.parseInt(res2);

                Aprobacion aprobacion = serviciosRemedyLimpiezaBI.consultarAprobaciones(idIncidente, opcion);

                JsonObject envoltorioJsonObj = new JsonObject();

                if (aprobacion != null) {
                    JsonObject insJsonObj = new JsonObject();
                    if (aprobacion.getA1_Base() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("ARCHIVO_1", arch1);

                    } else {
                        // insJsonObj.addProperty("ARCHIVO_1",aprobacion.getA1_Base().toString());
                        InputStream in = aprobacion.getA1_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64 codec = new Base64();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(Base64.encodeBase64(byteArray), "UTF-8");
                        insJsonObj.addProperty("ARCHIVO_1", encoded);
                    }
                    if (aprobacion.getA2_Base() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("ARCHIVO_2", arch1);
                    } else {
                        // insJsonObj.addProperty("ARCHIVO_2",aprobacion.getA2_Base().toString());
                        InputStream in = aprobacion.getA2_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64 codec = new Base64();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(Base64.encodeBase64(byteArray), "UTF-8");
                        insJsonObj.addProperty("ARCHIVO_2", encoded);

                    }

                    if (aprobacion.getJustificacion() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("JUSTIFICACION_APROBACION", arch1);
                    } else {
                        insJsonObj.addProperty("JUSTIFICACION_APROBACION", aprobacion.getJustificacion());
                    }

                    if (aprobacion.getEstadoAprobacion() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("ESTADO_APROBACION", arch1);
                    } else {
                        insJsonObj.addProperty("ESTADO_APROBACION", aprobacion.getEstadoAprobacion());
                    }

                    Date date = aprobacion.getFechaModificacion().getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date1 = format1.format(date);
                    insJsonObj.addProperty("FECHA", "" + date1);

                    envoltorioJsonObj.add("APROBACION", insJsonObj);

                    res = envoltorioJsonObj.toString();

                }

            } else {
                return "{}";
            }
            res1 = null;
            res2 = null;
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/cerrarSinOTLimpieza.json?idUsuario=<?>&idIncidente=<?>&resolucion=<?>&montivoEstado=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/cerrarSinOTLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String cerrarSinOT(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String resolucion = urides.split("&")[2].split("=")[1];
            String montivoEstado = urides.split("&")[3].split("=")[1];

            // logger.info("params " + idIncidente + ", " + resolucion + ", " +
            // montivoEstado);
            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        if ((aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("por asignar"))
                                || (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("rechazado proveedor"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }

            boolean actIncidente = false;
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyLimpiezaBI.cierreSinOT(idIncidente, resolucion, montivoEstado);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //// logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/asignaProveedorLimpieza.json?idUsuario=<?>&idIncidente=<?>&proveedor=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/asignaProveedorLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String asignaProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if (urides.length() > 0) {
                int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
                String proveedor = urides.split("&")[2].split("=")[1];
                /*
				 * String fecha = urides.split("&")[3].split("=")[1]; String monto =
				 * urides.split("&")[4].split("=")[1]; String origen =
				 * urides.split("&")[5].split("=")[1]; String solicitante =
				 * urides.split("&")[6].split("=")[1]; String aprobador =
				 * urides.split("&")[7].split("=")[1]; String mensaje =
				 * urides.split("&")[8].split("=")[1]; String tipoAten =
				 * urides.split("&")[9].split("=")[1];
                 */

                ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaBI.ConsultaDetalleFolio(idIncidente);
                int resp = 0;
                if (auxIncidente != null) {
                    Informacion[] arregloInc = auxIncidente.getInformacion();
                    if (arregloInc != null) {
                        for (Informacion aux : arregloInc) {
                            if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                    && (aux.getMotivoEstado().trim().toLowerCase().equals("por asignar") || aux
                                    .getMotivoEstado().trim().toLowerCase().equals("rechazado proveedor"))) {
                                resp = 0;
                            } else {
                                resp = 1;
                            }
                        }
                    }
                }

                boolean actIncidente = false;
                if (resp == 1) {
                    resp = 2;
                } else {
                    // actIncidente = serviciosRemedyLimpiezaBI.asignarProveedor(idIncidente,
                    // proveedor, fecha, monto, origen, solicitante, aprobador, mensaje, tipoAten);
                    actIncidente = serviciosRemedyLimpiezaBI.asignarProveedor(idIncidente, proveedor);
                }

                if (resp != 2) {
                    if (actIncidente) {
                        resp = 1;
                    } else {
                        resp = 0;
                    }
                }

                JsonObject envoltorioJsonObj = new JsonObject();

                if (!actIncidente) {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("RES", resp);
                    envoltorioJsonObj.add("MODIFICADO", insJsonObj);
                } else {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("RES", resp);
                    envoltorioJsonObj.add("MODIFICADO", insJsonObj);
                }
                //// logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();
            } else {
                return "{}";
            }
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    /////////////////// Metodos procesamiento de Limpieza ///////////////////
}
