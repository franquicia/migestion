package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.ActivoFijoBI;
import com.gruposalinas.migestion.business.ArchivoUrlBI;
import com.gruposalinas.migestion.business.AreaApoyoBI;
import com.gruposalinas.migestion.business.BitacoraMantenimientoBI;
import com.gruposalinas.migestion.business.CartaAsignacionAFBI;
import com.gruposalinas.migestion.business.CatalogoMinuciasBI;
import com.gruposalinas.migestion.business.CatalogoVistaBI;
import com.gruposalinas.migestion.business.CedulaBI;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.DatosEmpMttoBI;
import com.gruposalinas.migestion.business.DepuraActFijoBI;
import com.gruposalinas.migestion.business.EstandPuestoBI;
import com.gruposalinas.migestion.business.FilasBI;
import com.gruposalinas.migestion.business.FormatoMetodo7sBI;
import com.gruposalinas.migestion.business.FotoPlantillaBI;
import com.gruposalinas.migestion.business.FotosAntesDespuesBI;
import com.gruposalinas.migestion.business.GuiaDHLBI;
import com.gruposalinas.migestion.business.HorasPicoBI;
import com.gruposalinas.migestion.business.IncidenciasBI;
import com.gruposalinas.migestion.business.InfoVistaBI;
import com.gruposalinas.migestion.business.OperacionBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.business.PeticionesRemedyBI;
import com.gruposalinas.migestion.business.ProductoBI;
import com.gruposalinas.migestion.business.ProgLimpiezaBI;
import com.gruposalinas.migestion.business.ProveedorLoginBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.PuestoBI;
import com.gruposalinas.migestion.business.ServiciosRemedyBI;
import com.gruposalinas.migestion.business.TelSucursalBI;
import com.gruposalinas.migestion.business.TicketCuadrillaBI;
import com.gruposalinas.migestion.business.UsuarioExternoBI;
import com.gruposalinas.migestion.business.ef.ExtraccionFinancieraBI;
import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import com.gruposalinas.migestion.domain.ArchivosUrlDTO;
import com.gruposalinas.migestion.domain.AreaApoyoDTO;
import com.gruposalinas.migestion.domain.BitacoraMntoDTO;
import com.gruposalinas.migestion.domain.CartaAsignacionAFDTO;
import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;
import com.gruposalinas.migestion.domain.CatalogoVistaDTO;
import com.gruposalinas.migestion.domain.CedulaDTO;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import com.gruposalinas.migestion.domain.DepuraActFijoDTO;
import com.gruposalinas.migestion.domain.EstandPuestoDTO;
import com.gruposalinas.migestion.domain.ExternoDTO;
import com.gruposalinas.migestion.domain.FilasDTO;
import com.gruposalinas.migestion.domain.FormatoMetodo7sDTO;
import com.gruposalinas.migestion.domain.FotoPlantillaDTO;
import com.gruposalinas.migestion.domain.FotosAntesDespuesDTO;
import com.gruposalinas.migestion.domain.FranquiciaCalDTO;
import com.gruposalinas.migestion.domain.FranquiciaLigasDTO;
import com.gruposalinas.migestion.domain.GuiaDHLDTO;
import com.gruposalinas.migestion.domain.HorasPicoDTO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.domain.InfoVistaDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.domain.PeticionDTO;
import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;
import com.gruposalinas.migestion.domain.ProveedorLoginDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.PuestoDTO;
import com.gruposalinas.migestion.domain.TelSucursalDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.domain.TipificacionDTO;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/catalogosService")
public class ServiciosConsultaCatalogos {

    private static final Logger logger = LogManager.getLogger(ServiciosConsultaCatalogos.class);

    @Autowired
    OperacionBI operacionBI;

    @Autowired
    ProductoBI productoBI;

    @Autowired
    ParametroBI parametroBI;

    @Autowired
    ExtraccionFinancieraBI extraccionFinancieraBI;


    @Autowired
    CatalogoVistaBI catalogoVistaBI;

    @Autowired
    InfoVistaBI infoVistaBI;

    @Autowired
    FilasBI filaBI;


    @Autowired
    UsuarioExternoBI usuarioexternoBI;

    @Autowired
    TelSucursalBI telsucursalBI;

    @Autowired
    ServiciosRemedyBI serviciosRemedyBI;

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    DatosEmpMttoBI datosEmpMttoBI;

    @Autowired
    FormatoMetodo7sBI formatoMetodo7sBI;

    @Autowired
    CedulaBI cedulaBI;

    @Autowired
    EstandPuestoBI estandPuestoBI;

    @Autowired
    ProgLimpiezaBI progLimpiezaBI;

    @Autowired
    ArchivoUrlBI archivoUrlBI;

    @Autowired
    BitacoraMantenimientoBI bitacoraMantenimientoBI;

    @Autowired
    ProveedorLoginBI proveedorLoginBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    TicketCuadrillaBI ticketCuadrillaBI;

    @Autowired
    FotoPlantillaBI fotoPlantillaBI;

    @Autowired
    CatalogoMinuciasBI catalogoMinuciasBI;

    @Autowired
    FotosAntesDespuesBI fotosAntesDespuesBI;

    @Autowired
    DepuraActFijoBI depuraActFijoBI;

    @Autowired
    ActivoFijoBI activoFijoBI;

    @Autowired
    AreaApoyoBI areaApoyoBI;

    @Autowired
    GuiaDHLBI guiaDHLBI;

    @Autowired
    CartaAsignacionAFBI cartaAsignacionAFBI;


    @Autowired
    PuestoBI puestoBI;

    @Autowired
    HorasPicoBI horasPicoBI;

    @Autowired
    PeticionesRemedyBI peticionesRemedyBI;

    // http://localhost:8080/migestion/catalogosService/getOperacion.json
    @RequestMapping(value = "/getOperacion", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getOperacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;
        try {
            lista = operacionBI.consulta();
        } catch (Exception e) {
            return null;
        }
        return lista;
    }

    // http://localhost:8080/migestion/catalogosService/getProducto.json
    @RequestMapping(value = "/getProducto", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getProducto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;
        try {
            lista = productoBI.consulta();
        } catch (Exception e) {
            return null;
        }
        return lista;
    }

    // http://localhost:8080/migestion/catalogosService/getProductoHijos.json?idPadre=<?>
    @RequestMapping(value = "/getProductoHijos", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getProductoHijos(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;
        try {
            String idPadre = request.getParameter("idPadre");
            lista = productoBI.getHijos(Integer.valueOf(idPadre));
        } catch (Exception e) {
            return null;
        }
        return lista;
    }

    // http://localhost:8080/migestion/catalogosService/getOperacionHijos.json?idPadre=<?>
    @RequestMapping(value = "/getOperacionHijos", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getOperacionHijos(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;
        try {
            String idPadre = request.getParameter("idPadre");
            lista = operacionBI.getHijos(Integer.valueOf(idPadre));

        } catch (Exception e) {
            return null;
        }
        return lista;
    }

    // http://localhost:8080/migestion/catalogosService/getParametros.json
    @RequestMapping(value = "/getParametros", method = RequestMethod.GET)
    public ModelAndView getParametros(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ParametroDTO> lista = parametroBI.obtieneParametro();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "LISTA PARAMETROS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getCatalogoVistaById.json?idCatVista=<?>
    @RequestMapping(value = "/getCatalogoVistaById", method = RequestMethod.GET)
    public ModelAndView getCatalogoVistaById(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idCatVista = Integer.parseInt(request.getParameter("idCatVista"));

            List<CatalogoVistaDTO> res = catalogoVistaBI.buscaCatalogoVista(idCatVista);

            //logger.info("LISTA: " + res.size());
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "Catalogo Vista");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }


    // http://localhost:8080/migestion/catalogosService/getInfoVistaById.json?idInfoVista=<?>
    @RequestMapping(value = "/getInfoVistaById", method = RequestMethod.GET)
    public ModelAndView getInfoVistaById(HttpServletRequest request, HttpServletResponse response) {

        try {
            //logger.info("Entro al metodo consulta INFO VISTA");
            int idInfoVista = Integer.parseInt(request.getParameter("idInfoVista"));

            List<InfoVistaDTO> res = infoVistaBI.buscaInfoVista(idInfoVista);

            //logger.info("Tamanio de res: " + res);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "Info Vista");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getCatalogoVista.json?idCatVista=<?>&descripcion=<?>&moduloPadre=<?>
    @RequestMapping(value = "/getCatalogoVista", method = RequestMethod.GET)
    public ModelAndView getCatalogoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCatVista = request.getParameter("idCatVista");
            String descripcion = request.getParameter("setDescripcion");
            String moduloPadre = request.getParameter("moduloPadre");

            // logger.info("Entro a consulta catalogo Service");
            List<CatalogoVistaDTO> res = catalogoVistaBI.buscaCatalogoVistaParam(idCatVista, descripcion, moduloPadre);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "Catalogo Vista");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getInfoVista.json?idInfoVista=<?>&idCatVista=<?>&idUsuario=<?>&plataforma=<?>&fecha=<?>
    @RequestMapping(value = "/getInfoVista", method = RequestMethod.GET)
    public ModelAndView getInfoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idInfoVista = request.getParameter("idInfoVista");
            String idCatVista = request.getParameter("idCatVista");
            String idUsuario = request.getParameter("idUsuario");
            String plataforma = request.getParameter("plataforma");
            String fecha = request.getParameter("fecha");

            List<InfoVistaDTO> res = infoVistaBI.buscaInfoVistaParam(idInfoVista, idCatVista, idUsuario, plataforma, fecha);

            //logger.info("Tamaño del res: " + res.size());
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "Info Vista");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getFila.json?idFila=<?>
    @RequestMapping(value = "/getFila", method = RequestMethod.GET)
    public ModelAndView getFila(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idfi = 0;
            if (request.getParameter("idFila") != null) {
                idfi = Integer.parseInt(request.getParameter("idFila"));
            }
            List<FilasDTO> lista = filaBI.obtieneDatos(idfi);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "Filas");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getFilas.json?
    @RequestMapping(value = "/getFilas", method = RequestMethod.GET)
    public ModelAndView getFilas(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<FilasDTO> lista = filaBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "Filas");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

   

    // http://localhost:8080/migestion/catalogosService/getEmpleadoExterno.json?numEmpleado=<?>
    @RequestMapping(value = "/getEmpleadoExterno", method = RequestMethod.GET)
    public ModelAndView getEmpleadoExterno(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idemp = 0;
            if (request.getParameter("numEmpleado") != null) {
                idemp = Integer.parseInt(request.getParameter("numEmpleado"));
            }
            List<ExternoDTO> listausuario = usuarioexternoBI.obtieneUsuario(idemp);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(listausuario);
            mv.addObject("tipo", "EMP_EXT");
            mv.addObject("res", listausuario);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getEmpleadosExternos.json?
    @RequestMapping(value = "/getEmpleadosExternos", method = RequestMethod.GET)
    public ModelAndView getEmpleadosExternos(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ExternoDTO> listausuarios = usuarioexternoBI.obtieneUsuario();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(listausuarios);
            mv.addObject("tipo", "EMP_EXT");
            mv.addObject("res", listausuarios);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getTelefonosucursal.json?idCeco=<?>
    @RequestMapping(value = "/getTelefonosucursal", method = RequestMethod.GET)
    public ModelAndView getTelefonosucursal(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idCeco = 0;
            if (request.getParameter("idCeco") != null) {
                idCeco = Integer.parseInt(request.getParameter("idCeco"));
            }
            List<TelSucursalDTO> listaTelefono = telsucursalBI.obtienetelefono(idCeco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(listaTelefono);
            mv.addObject("tipo", "TEL_SUC");
            mv.addObject("res", listaTelefono);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getTelefonosSucursal.json?
    @RequestMapping(value = "/getTelefonosSucursal", method = RequestMethod.GET)
    public ModelAndView getTelefonosSucursal(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<TelSucursalDTO> listaTelesonos = telsucursalBI.obtienetelefono();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(listaTelesonos);
            mv.addObject("tipo", "TEL_SUC");
            mv.addObject("res", listaTelesonos);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getFallasRemedy.json
    @RequestMapping(value = "/getFallasRemedy", method = RequestMethod.GET)
    public ModelAndView getFallasRemedy(HttpServletRequest request, HttpServletResponse response) {
        try {

            serviciosRemedyBI.imprimefallas();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargo la tabla de fallas");
            mv.addObject("res", true);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getCatalogoIncidencia.json?idTipo=<?>
    @RequestMapping(value = "/getCatalogoIncidencia", method = RequestMethod.GET)
    public ModelAndView getCatalogoIncidencia(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idTipo = 0;
            if (request.getParameter("idTipo") != null) {
                idTipo = Integer.parseInt(request.getParameter("idTipo"));
            }
            List<IncidenciasDTO> lista = incidenciasBI.obtieneDatos(idTipo);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "INCIDENCIA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getCatalogoIncidencias.json?
    @RequestMapping(value = "/getCatalogoIncidencias", method = RequestMethod.GET)
    public ModelAndView getCatalogoIncidencias(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<IncidenciasDTO> lista = incidenciasBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "INCIDENCIA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getCatalogoProveedor.json?idProveedor=<?>
    @RequestMapping(value = "/getCatalogoProveedor", method = RequestMethod.GET)
    public ModelAndView getCatalogoProveedor(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idProveedor = 0;
            if (request.getParameter("idProveedor") != null) {
                idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            }
            List<ProveedoresDTO> lista = proveedoresBI.obtieneDatos(idProveedor);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "PROVEEDORES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getCatalogoProveedores.json?
    @RequestMapping(value = "/getCatalogoProveedores", method = RequestMethod.GET)
    public ModelAndView getCatalogoProveedores(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "PROVEEDORES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getCatalogoProveedoresLimpieza.json?
    @RequestMapping(value = "/getCatalogoProveedoresLimpieza", method = RequestMethod.GET)
    public ModelAndView getCatalogoProveedoresLimpieza(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "PROVEEDORES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getEmpMtto.json?idUsuario=<?>
    @RequestMapping(value = "/getEmpMtto", method = RequestMethod.GET)
    public ModelAndView getEmpMtto(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idUsuario = 0;
            if (request.getParameter("idUsuario") != null) {
                idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            }
            List<DatosEmpMttoDTO> lista = datosEmpMttoBI.obtieneDatos(idUsuario);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "EMPMTTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getEmpleadosMtto.json?
    @RequestMapping(value = "/getEmpleadosMtto", method = RequestMethod.GET)
    public ModelAndView getEmpleadosMtto(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<DatosEmpMttoDTO> lista = datosEmpMttoBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "EMPMTTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }


    // http://localhost:8080/migestion/catalogosService/getFormatoMedicionMetodDetalle.json?
    @RequestMapping(value = "/getFormatoMedicionMetodDetalle", method = RequestMethod.GET)
    public ModelAndView getFormatoMedicionMetodDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<FormatoMetodo7sDTO> lista = formatoMetodo7sBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "FORMATO7S");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }




    // http://localhost:8080/migestion/catalogosService/getCedulas.json?
    @RequestMapping(value = "/getCedulas", method = RequestMethod.GET)
    public ModelAndView getCedulas(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<CedulaDTO> lista = cedulaBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "CEDULA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getEstandPuestoid.json?ceco=<?>
    @RequestMapping(value = "/getEstandPuestoid", method = RequestMethod.GET)
    public ModelAndView getEstandPuestoid(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");

            List<EstandPuestoDTO> lista = estandPuestoBI.obtieneDatos(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "ESTPUESTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }


    // http://localhost:8080/migestion/catalogosService/getEstandPuestoDetalle.json?
    @RequestMapping(value = "/getEstandPuestoDetalle", method = RequestMethod.GET)
    public ModelAndView getEstandPuestoDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<EstandPuestoDTO> lista = estandPuestoBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "ESTPUESTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getPuestoLimpiezaDetalle.json?
    @RequestMapping(value = "/getPuestoLimpiezaDetalle", method = RequestMethod.GET)
    public ModelAndView getPuestoLimpiezaDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ProgLimpiezaDTO> lista = progLimpiezaBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "PROGLIMPIEZA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getArchivoUrlID.json?idArch=<?>
    @RequestMapping(value = "/getArchivoUrlID", method = RequestMethod.GET)
    public ModelAndView getArchivoUrlID(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idArch = Integer.parseInt(request.getParameter("idArch"));

            List<ArchivosUrlDTO> lista = archivoUrlBI.obtieneDatos(idArch);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "URL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getArchivoUrl.json?
    @RequestMapping(value = "/getArchivoUrl", method = RequestMethod.GET)
    public ModelAndView getArchivoUrl(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ArchivosUrlDTO> lista = archivoUrlBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "URL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    //http://localhost:8080/migestion/catalogosService/getLogProveedor.json?idProveedor=<?>
    @RequestMapping(value = "/getLogProveedor", method = RequestMethod.GET)
    public ModelAndView getLogProveedor(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));

            List<ProveedorLoginDTO> lista = proveedorLoginBI.obtieneDatos(idProveedor);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "LOGIN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getLogProveedores.json?
    @RequestMapping(value = "/getLogProveedores", method = RequestMethod.GET)
    public ModelAndView getLogProveedores(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ProveedorLoginDTO> lista = proveedorLoginBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "LOGIN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getCuadrilla.json?idCuadrilla=<?>
    @RequestMapping(value = "/getCuadrilla", method = RequestMethod.GET)
    public ModelAndView getCuadrilla(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCuadrilla = Integer.parseInt(request.getParameter("idCuadrilla"));

            List<CuadrillaDTO> lista = cuadrillaBI.obtieneDatos(idCuadrilla);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "CUADRILLA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getCuadrillas.json?
    @RequestMapping(value = "/getCuadrillas", method = RequestMethod.GET)
    public ModelAndView getCuadrillas(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<CuadrillaDTO> lista = cuadrillaBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "CUADRILLA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/getTkCuadrilla.json?idCuadrilla=<?>
    @RequestMapping(value = "/getTkCuadrilla", method = RequestMethod.GET)
    public ModelAndView getTkCuadrilla(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCuadrilla = Integer.parseInt(request.getParameter("idCuadrilla"));

            List<TicketCuadrillaDTO> lista = ticketCuadrillaBI.obtieneDatos(idCuadrilla);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "TKCUADRI");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getTkCuadrillas.json?
    @RequestMapping(value = "/getTkCuadrillas", method = RequestMethod.GET)
    public ModelAndView getTkCuadrillas(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<TicketCuadrillaDTO> lista = ticketCuadrillaBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "TKCUADRI");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getTicketxCuadrilla.json?idCuadrilla=<?>&idProveedor=<?>&zona=<?>
    @RequestMapping(value = "/getTicketxCuadrilla", method = RequestMethod.GET)
    public ModelAndView getTicketxCuadrilla(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCuadrilla = Integer.parseInt(request.getParameter("idCuadrilla"));
            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            int zona = Integer.parseInt(request.getParameter("zona"));

            List<TicketCuadrillaDTO> lista = ticketCuadrillaBI.obtieneCuadrillaTK(idCuadrilla, idProveedor, zona);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "TKCUADRI");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getFotoPlantillaDetalle.json?
    @RequestMapping(value = "/getFotoPlantillaDetalle", method = RequestMethod.GET)
    public ModelAndView getFotoPlantillaDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<FotoPlantillaDTO> lista = fotoPlantillaBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "FOTOPLAN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getBitMttoid.json?idAmbito=<?>
    @RequestMapping(value = "/getBitMttoid", method = RequestMethod.GET)
    public ModelAndView getBitMttoid(HttpServletRequest request, HttpServletResponse response) {
        try {

            Integer idAmbito = Integer.parseInt(request.getParameter("idAmbito"));

            List<BitacoraMntoDTO> lista = bitacoraMantenimientoBI.consultaBitacoraMntoIds(idAmbito);
            ModelAndView mv = new ModelAndView("muestraServicios");
            ////logger.info(lista);
            mv.addObject("tipo", "BITACORAMMT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            ////logger.info("Ocurrio Algo");
            return null;

        }
    }


    // http://localhost:8080/migestion/catalogosService/getMinucias.json?
    @RequestMapping(value = "/getMinucias", method = RequestMethod.GET)
    public ModelAndView getMinucias(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<CatalogoMinuciasDTO> lista = catalogoMinuciasBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "MINUCIAS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }


    // http://localhost:8080/migestion/catalogosService/getFotoAntDesDet.json?
    @RequestMapping(value = "/getFotoAntDesDet", method = RequestMethod.GET)
    public ModelAndView getFotoAntDesDet(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<FotosAntesDespuesDTO> lista = fotosAntesDespuesBI.consultaFotoAntesDespUlt();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "FOTOAD");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }


    // http://localhost:8080/migestion/catalogosService/getDepActFijo.json?
    @RequestMapping(value = "/getDepActFijo", method = RequestMethod.GET)
    public ModelAndView getDepActFijo(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<DepuraActFijoDTO> lista = depuraActFijoBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "DEPACT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getActivoFijos.json?
    @RequestMapping(value = "/getActivoFijos", method = RequestMethod.GET)
    public ModelAndView getActivoFijos(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ActivoFijoDTO> lista = activoFijoBI.consultaActivoFijoAll();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "ACTFIJO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }

    }

    // http://localhost:8080/migestion/catalogosService/getCuadrillaPassword.json?idCuadrilla=<?>&idProveedor=<?>&zona=<?>
    @RequestMapping(value = "/getCuadrillaPassword", method = RequestMethod.GET)
    public ModelAndView getCuadrillaPassword(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCuadrilla = Integer.parseInt(request.getParameter("idCuadrilla"));
            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            int zona = Integer.parseInt(request.getParameter("zona"));

            List<CuadrillaDTO> lista = cuadrillaBI.obtienepass(idCuadrilla, idProveedor, zona);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "CUADRILLAPASS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }


    // http://localhost:8080/migestion/catalogosService/getGuiaDHLDetalle.json?
    @RequestMapping(value = "/getGuiaDHLDetalle", method = RequestMethod.GET)
    public ModelAndView getGuiaDHLDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<GuiaDHLDTO> lista = guiaDHLBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "DHL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }


    // http://localhost:8080/migestion/catalogosService/getAreaApoyoDetalle.json?idArea=<?>
    @RequestMapping(value = "/getAreaApoyoDetalle", method = RequestMethod.GET)
    public ModelAndView getAreaApoyoDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<AreaApoyoDTO> lista = areaApoyoBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "AREAAPOYO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getMinuciaEvi.json?idMinucia=<?>
    @RequestMapping(value = "/getMinuciaEvi", method = RequestMethod.GET)
    public ModelAndView getMinuciaEvi(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idMinucia = request.getParameter("idMinucia");

            List<CatalogoMinuciasDTO> lista = catalogoMinuciasBI.obtieneDatosEvi(idMinucia);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "MINUCIASEVI");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }


    // http://localhost:8080/migestion/catalogosService/getCartaAsig.json?
    @RequestMapping(value = "/getCartaAsig", method = RequestMethod.GET)
    public ModelAndView getCartaAsig(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<CartaAsignacionAFDTO> lista = cartaAsignacionAFBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "CARTAASIG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }


    // http://localhost:8080/migestion/catalogosService/getMuebleid.json?idPuesto=<?>
    @RequestMapping(value = "/getMuebleid", method = RequestMethod.GET)
    public ModelAndView getMuebleid(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idPuesto = request.getParameter("idPuesto");

            List<EstandPuestoDTO> lista = estandPuestoBI.obtieneDatosMueble(idPuesto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "ESTPUESTOMUEBLE");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getEstandPuePlantiMueble.json?ceco=<?>
    @RequestMapping(value = "/getEstandPuePlantiMueble", method = RequestMethod.GET)
    public ModelAndView getEstandPuePlantiMueble(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");

            List<EstandPuestoDTO> lista = estandPuestoBI.obtieneDatosPlantillaMueble(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "ESTPUESTOMUEBLE");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getEstandPueDetalleMueble.json?
    @RequestMapping(value = "/getEstandPueDetalleMueble", method = RequestMethod.GET)
    public ModelAndView getEstandPueDetalleMueble(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<EstandPuestoDTO> lista = estandPuestoBI.obtieneInfoMueble();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "ESTPUESTOMUEBLE");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

  
    // http://localhost:8080/migestion/catalogosService/getPuestoLimpiezaDetalleArea.json?
    @RequestMapping(value = "/getPuestoLimpiezaDetalleArea", method = RequestMethod.GET)
    public ModelAndView getPuestoLimpiezaDetalleArea(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ProgLimpiezaDTO> lista = progLimpiezaBI.obtieneInfoPro();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "PROGLIMPIEZA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getDepActFijoEviCeco.json?ceco=<?>
    @RequestMapping(value = "/getDepActFijoEviCeco", method = RequestMethod.GET)
    public ModelAndView getDepActFijoEviCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");

            List<DepuraActFijoDTO> lista = depuraActFijoBI.obtieneDatosEvi(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "DEPACT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getDepActFijoEviDet.json?
    @RequestMapping(value = "/getDepActFijoEviDet", method = RequestMethod.GET)
    public ModelAndView getDepActFijoEviDet(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<DepuraActFijoDTO> lista = depuraActFijoBI.obtieneInfoEvi();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "DEPACT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getPuesto.json?idPuesto=<?>descripcion=<?>
    @RequestMapping(value = "/getPuesto", method = RequestMethod.GET)
    public ModelAndView getPuesto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String descripcion = request.getParameter("descripcion");
            int idPuesto = Integer.parseInt(request.getParameter("idPuesto"));

            List<PuestoDTO> lista = puestoBI.obtieneDatos(idPuesto, descripcion);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "PUESTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getPuestoDet.json
    @RequestMapping(value = "/getPuestoDet", method = RequestMethod.GET)
    public ModelAndView getPuestoDet(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<PuestoDTO> lista = puestoBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "PUESTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getHorasPicoPaso.json
    @RequestMapping(value = "/getHorasPicoPaso", method = RequestMethod.GET)
    public ModelAndView getHorasPicoPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSucursal = request.getParameter("idSucursal");
            String semana = request.getParameter("semana");
            List<HorasPicoDTO> lista = horasPicoBI.getHorasPicoPaso(idSucursal, semana);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "HORAS_PICO_PASO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }

    // http://localhost:8080/migestion/catalogosService/getPeticionesRemedy.json?fechaInicio=<?>&fechaFin=<?>
    @RequestMapping(value = "/getPeticionesRemedy", method = RequestMethod.GET)
    public ModelAndView getPeticionesRemedy(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            List<PeticionDTO> lista = peticionesRemedyBI.consultaPeticiones(fechaInicio, fechaFin);

            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "PETICIONES_REMEDY");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;

        }
    }
}
