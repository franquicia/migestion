package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.ArchivoUrlBI;
import com.gruposalinas.migestion.domain.ArchivosUrlDTO;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/archivos")

public class ServicioArchivos {

    @Autowired
    ArchivoUrlBI archivoUrlBI;

    private static Logger logger = LogManager.getLogger(ServicioArchivos.class);

    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100; // 100MB

    //http://localhost:8080/migestion/archivos/documentoExpediente.htm
    @RequestMapping(value = "/documentoExpediente", method = RequestMethod.GET)
    public ModelAndView getFileCenter(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());

        //String fileRoute="http://10.53.33.82/franquicia/imagenes/";
        String fileRoute = "/franquicia/documentos/";

        File r = null;

        String rootPath = File.listRoots()[0].getAbsolutePath();
        Calendar cal = Calendar.getInstance();

        // DEFINIR RUTA
        String rutaImagen = "";
        // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
        // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
        // NUEVO
        // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist
        try {
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                rutaImagen = "/franquicia/documentos/";
            } else {
                // DONDE GUARDO CUANDO ESTE EN checklist
                rutaImagen = "/franquicia/documentos/";

            }
        } catch (UnknownHostException e1) {
            // TODO Auto-generated catch block
            // e1.printStackTrace();

            rutaImagen = "/franquicia/documentos/";
        }

        String ruta = rootPath + rutaImagen;

        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        String ceco = "" + request.getSession().getAttribute("sucursal");

        File route = new File(ruta);

        mv.addObject("OKARCH", "NO");

        mv.addObject("fileAction", "subirArchivo");

        return mv;
    }

    //http://localhost:8080/migestion/archivos/documentoExpediente.htm
    @RequestMapping(value = "/documentoExpediente", method = RequestMethod.POST)
    public ModelAndView postFileCenter(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction)
            throws IOException {

        ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());

        //String fileRoute="http://10.53.33.82/franquicia/imagenes/";
        String ceco = "" + request.getSession().getAttribute("sucursal");

        fileRoute = "/franquicia/documentos/";

        File r = null;

        String rootPath = File.listRoots()[0].getAbsolutePath();
        Calendar cal = Calendar.getInstance();

        // DEFINIR RUTA
        String rutaImagen = "";
        // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
        // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
        // NUEVO
        // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist
        rutaImagen = "/franquicia/documentos/";

        String ruta = rootPath + rutaImagen;

        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        File route = new File(ruta);
        //File route = new File(fileRoute);

        //List<String> lista = new ArrayList<String>();
        String[] lista = route.list();

        if (new File(fileRoute).isDirectory()) {
            logger.info("El directorio " + fileRoute + " si existe");
            mv.addObject("listaArchivos", new File(fileRoute).list());
            for (String arch : lista) {
                if (arch.contains(ceco)) {
                    logger.info("Si se encontro");
                    mv.addObject("fileAction", "subirArchivo");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NomArchivo", arch);
                    break;
                } else {
                    mv.addObject("OKARCH", "NO");
                    logger.info("No se encontro");
                }
            }

        } else {
            logger.info("El directorio no existe");
            mv.addObject("directorio", "noExiste");
        }
        mv.addObject("fileAction", "subirArchivo");

        return mv;

    }

    //http://localhost:8080/migestion/archivos/uploadExpedFiles.htm
    @RequestMapping(value = "/uploadExpedFiles", method = RequestMethod.POST)
    public ModelAndView postUploadFiles(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false) MultipartFile uploadedFile,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction,
            @RequestParam(value = "seleccionArch", required = false) String seleccionArch)
            throws IOException {

        ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());

        String ceco = "" + request.getSession().getAttribute("sucursal");
        fileRoute = "/franquicia/documentos/";

        File r = null;

        String rootPath = File.listRoots()[0].getAbsolutePath();
        Calendar cal = Calendar.getInstance();

        // DEFINIR RUTA
        String rutaImagen = "";
        // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
        // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
        // NUEVO
        // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist
        try {
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                rutaImagen = "/franquicia/documentos/";
            } else {
                // DONDE GUARDO CUANDO ESTE EN checklist
                rutaImagen = "/franquicia/documentos/";

            }
        } catch (UnknownHostException e1) {
            // TODO Auto-generated catch block
            // e1.printStackTrace();

            rutaImagen = "/franquicia/documentos/";
        }

        String ruta = rootPath + rutaImagen;

        // System.out.println(cal.getTime().toString());
        // System.out.println(File.listRoots()[0].getAbsolutePath());
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        File route = new File(ruta);

        if (route.exists()) {
            if (uploadedFile.getSize() < MAX_FILE_SIZE) {
                if (checkContentType(uploadedFile.getContentType())) {
                    FileOutputStream fileOutput = null;
                    BufferedOutputStream bufferOutput = null;
                    try {
                        logger.info("archivo: " + seleccionArch);
                        int opcion = Integer.parseInt(seleccionArch);
                        String archivo = "";
                        if (opcion != 0) {

                            switch (opcion) {
                                case 1:
                                    archivo = "Atender-Mejor.pdf";
                                    break;
                                case 2:
                                    archivo = "Vender-Mejor.pdf";
                                    break;
                                case 3:
                                    archivo = "Concentracion-Masiva.pdf";
                                    break;
                                case 4:
                                    archivo = "Tecnologica.pdf";
                                    break;
                                case 5:
                                    archivo = "Geologico.pdf";
                                    break;
                                case 6:
                                    archivo = "Hidrometeorologico.pdf";
                                    break;
                                case 7:
                                    archivo = "Metodologia-7S.pdf";
                                    break;
                                case 8:
                                    archivo = "Activos.pdf";
                                    break;
                                case 9:
                                    archivo = "Aire.pdf";
                                    break;
                                default:
                                    archivo = null;
                                    break;
                            }

                            String[] lista = route.list();
                            boolean flag = false;
                            if (new File(fileRoute).isDirectory()) {
                                logger.info("El directorio " + fileRoute + " si existe");
                                mv.addObject("listaArchivos", new File(fileRoute).list());
                                for (String arch : lista) {
                                    if (arch.contains(archivo)) {
                                        flag = true;
                                        break;
                                    }
                                }

                            } else {
                                logger.info("El directorio no existe");
                                mv.addObject("directorio", "noExiste");
                                if (r.mkdirs()) {
                                    logger.info("SE HA CREADA LA CARPETA");
                                } else {
                                    logger.info("EL DIRECTORIO YA EXISTE");
                                }
                            }

                            if (flag == true) {
                                List<ArchivosUrlDTO> lista2 = archivoUrlBI.obtieneInfo();
                                for (ArchivosUrlDTO aux : lista2) {
                                    if (aux.getNombre().contains(archivo.trim())) {
                                        int version = Integer.parseInt(aux.getVersion());
                                        version++;
                                        aux.setVersion("" + version);
                                        logger.info(archivoUrlBI.actualiza(aux));
                                        break;
                                    }
                                }
                            } else {
                                ArchivosUrlDTO au = new ArchivosUrlDTO();
                                au.setNombre(archivo);
                                au.setRuta(ruta + archivo);
                                au.setVersion("1");
                                logger.info(archivoUrlBI.inserta(au));
                            }

                            fileOutput = new FileOutputStream(ruta + archivo);
                            //fileOutput = new FileOutputStream(ruta + uploadedFile.getOriginalFilename());

                            bufferOutput = new BufferedOutputStream(fileOutput);

                            bufferOutput.write((byte[]) uploadedFile.getBytes());
                            bufferOutput.close();

                            File uploadedFile2 = new File(ruta + uploadedFile.getOriginalFilename());
                            String cad = ruta + ceco + uploadedFile.getOriginalFilename();
                            //uploadedFile2.renameTo(new File(ruta + ceco+uploadedFile.getOriginalFilename()));

                            logger.info("Archivo " + uploadedFile.getOriginalFilename() + " subio exitosamente!");

                            mv.addObject("OKARCH", "OK");
                            mv.addObject("NomArchivo", archivo);
                        } else {
                            mv.addObject("OKARCH", "NO");
                            mv.addObject("archivoSubido", "no");
                        }
                    } catch (Exception e) {
                        mv.addObject("archivoSubido", "no");
                        //e.printStackTrace();
                        logger.info("No se subió el archivo");
                    } finally {
                        try {
                            if (fileOutput != null) {
                                fileOutput.close();
                            }
                            if (bufferOutput != null) {
                                bufferOutput.close();
                            }
                        } catch (Exception e) {
                            logger.info("No se cerraron las conexiones");
                            //e.printStackTrace();
                        }
                    }
                } else {
                    mv.addObject("archivoSubido", "no-compatible");
                    logger.info("El archivo " + uploadedFile.getOriginalFilename() + " tiene una extensión incompatible.");
                }
            } else {
                mv.addObject("archivoSubido", "size");
                logger.info("El archivo " + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
            }
        } else {
            mv.addObject("archivoSubido", "no");
            logger.info("El directorio " + fileRoute + " no existe");
        }

        mv.addObject("fileAction", fileAction);
        mv.addObject("fileRoute", fileRoute);

        return mv;

    }

    // http://localhost:8080/migestion/archivos/conteoDocumentos.json
    @RequestMapping(value = "/conteoDocumentos", method = RequestMethod.GET)
    public ModelAndView eliminaCanal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Conteo de Archivos");

            String fileRoute = "/franquicia/imagenes/";
            String ceco = "" + request.getSession().getAttribute("sucursal");

            String rootPath = File.listRoots()[0].getAbsolutePath();
            String ruta = rootPath + fileRoute;

            File route = new File(ruta);
            //File route = new File(fileRoute);

            //List<String> lista = new ArrayList<String>();
            String[] lista = route.list();

            int conteo = 0;
            for (String arch : lista) {
                if (arch.contains(".pdf") || arch.contains(".PDF")) {
                    conteo++;
                }

            }

            mv.addObject("res", conteo);
            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/migestion/archivos/deleteFiles.htm
    @RequestMapping(value = "/deleteFiles2", method = RequestMethod.POST)
    public ModelAndView postListFiles(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = true) String fileRoute,
            @RequestParam(value = "fileAction", required = true) String fileAction)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

        List<String> listaArchivosEliminar = new ArrayList<String>();
        List<String> auxList = new ArrayList<String>();
        List<String> fullList = new ArrayList<String>();

        String ruta = File.listRoots()[0].getAbsolutePath() + fileRoute;

        File directory = new File(ruta);

        for (String video : directory.list()) {
            fullList.add(video);
        }

        if (request.getParameterValues("listaArchivosEliminar") != null) {
            for (String video : request.getParameterValues("listaArchivosEliminar")) {
                listaArchivosEliminar.add(video);
            }

            if (listaArchivosEliminar.size() > 0) {
                if (directory.exists()) {
                    File deleteFile;

                    for (String file : listaArchivosEliminar) {
                        deleteFile = new File(ruta + file);

                        if (deleteFile.delete()) {
                            logger.info("El archivo " + file + " fue eliminado");
                            auxList.add(file);
                            mv.addObject("eliminado", "si");
                        } else {
                            logger.info("El archivo " + file + " no fue eliminado");
                            mv.addObject("eliminado", "no");
                        }
                    }

                    deleteFile = null;

                    if (auxList.isEmpty()) {
                        mv.addObject("listaArchivos", fullList);
                    } else {
                        fullList.removeAll(auxList);
                        mv.addObject("listaArchivos", fullList);
                    }
                } else {
                    logger.info("El directorio no existe, no se pueden eliminar los vídeos");
                    mv.addObject("eliminado", "no");
                    mv.addObject("listaArchivos", fullList);
                }
            } else {
                logger.info("La lista se encuentra vacía, no se puede eliminar ningún vídeo");
                mv.addObject("eliminado", "no");
            }
        } else {
            mv.addObject("eliminado", "no-vacio");
            mv.addObject("listaArchivos", fullList);
        }

        mv.addObject("fileAction", fileAction);
        mv.addObject("fileRoute", fileRoute);

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/renameFiles.htm
    @RequestMapping(value = "/renameFiles2", method = RequestMethod.POST)
    public ModelAndView postRenameFiles(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = true) String fileRoute,
            @RequestParam(value = "fileAction", required = true) String fileAction,
            @RequestParam(value = "oldFileName", required = true) String oldFileName,
            @RequestParam(value = "newFileName", required = true) String newFileName)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

        File directory = new File(fileRoute);

        if (oldFileName != null) {
            logger.info("NOT NULL");
        } else {
            logger.info("NULL");
        }

        if (directory.isDirectory()) {
            File uploadedFile = new File(fileRoute + oldFileName);

            if (uploadedFile.isFile()) {
                try {
                    if (uploadedFile.renameTo(new File(fileRoute + newFileName))) {
                        logger.info("El archivo " + uploadedFile.getName() + " fue renombrado exitosamente");
                        mv.addObject("fileAction", fileAction);
                        mv.addObject("fileRoute", fileRoute);
                        mv.addObject("renombrado", "si");
                    } else {
                        logger.info("El archivo " + uploadedFile.getName() + " no pudo ser renombrado");
                        mv.addObject("fileAction", fileAction);
                        mv.addObject("fileRoute", fileRoute);
                        mv.addObject("renombrado", "no");
                    }
                } catch (Exception e) {
                    logger.info("ERROR " + e.getMessage());
                }
            } else {
                uploadedFile = null;
                logger.info("El archivo no existe o su nombre no es correcto.");
                mv.addObject("renombrado", "no");
            }

            List<String> fullList = new ArrayList<String>();

            for (String video : directory.list()) {
                fullList.add(video);
            }

            mv.addObject("listaArchivos", fullList);
        } else {
            logger.info("El directorio no existe o no es correcto.");
            mv.addObject("renombrado", "no");
        }

        return mv;

    }

    /**
     * audio/aac	//AAC audio/mp4	//AAC (Firefox) video/x-msvideo //AVI
     * application/octet-stream	//BIN text/css	//CSS application/msword	//DOC
     * application/epub+zip	//EPUB image/gif	//GIF text/html	//HTM / HTML
     * image/x-icon	//ICO application/java-archive	//JAR image/jpeg	//JPG / JPEG
     * application/javascript	//JS application/json	//JSON video/mpeg	//MPEG
     * audio/mpeg	//MP3 video/mp4	//MP4 video/quicktime	//MOV image/png	//PNG
     * application/pdf	//PDF application/vnd.ms-powerpoint	//PPT
     * application/x-rar-compressed	//RAR application/rtf	//RTF audio/x-wav
     * //WAV video/x-ms-wmv	//WMV application/xhtml+xml	//XHMTL
     * application/vnd.ms-excel	//XLS application/xml	//XML application/zip
     * //ZIP
     *
     * application/vnd.openxmlformats-officedocument.wordprocessingml.document
     * //DOCX application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
     * //XLSX
     * application/vnd.openxmlformats-officedocument.presentationml.presentation
     * //PPTX
     *
     */
    boolean checkContentType(String contentType) {

        String[] contentTypeList = {"audio/aac", "video/x-msvideo", "application/octet-stream", "text/css", "application/msword", "application/epub+zip",
            "image/gif", "text/html", "image/x-icon", "application/java-archive", "image/jpeg", "application/javascript", "application/json", "video/mpeg",
            "audio/mpeg", "video/mp4", "image/png", "application/pdf", "application/vnd.ms-powerpoint", "application/x-rar-compressed", "application/rtf",
            "audio/x-wav", "application/xhtml+xml", "application/vnd.ms-excel", "application/xml", "application/zip",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "audio/mp4", "video/quicktime",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "video/x-ms-wmv",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            null};

        for (String string : contentTypeList) {
            if (string.equals(contentType)) {
                return true;
            }
        }

        return false;
    }

    /*
	 * ****************************** METODOS PARA EL REPLICADO DE IMAGENES
	 * ******************************
     */
    // TRUE CUANDO ESTOY EN checklist
    public boolean verificaServidor(String ipActual) {
        boolean resp = false;
        String[] ipFrq = {"10.53.33.74", "10.53.33.75", "10.53.33.76", "10.53.33.77"};

        for (String data : ipFrq) {
            if (data.contains(ipActual)) {
                return true;
            }
        }
        return resp;
    }

    public String codificarBase64(byte[] cadena) throws UnsupportedEncodingException {
        return new String(Base64.encodeBase64(cadena), "UTF-8");
    }

}

class FileParametersExped {

    private MultipartFile video;
    private String fileAction;
    private String fileRoute;
    private String newFileName;
    private String oldFileName;

    public MultipartFile getVideo() {
        return video;
    }

    public void setVideo(MultipartFile video) {
        this.video = video;
    }

    public String getFileAction() {
        return fileAction;
    }

    public void setFileAction(String fileAction) {
        this.fileAction = fileAction;
    }

    public String getFileRoute() {
        return fileRoute;
    }

    public void setFileRoute(String fileRoute) {
        this.fileRoute = fileRoute;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getOldFileName() {
        return oldFileName;
    }

    public void setOldFileName(String oldFileName) {
        this.oldFileName = oldFileName;
    }
}
