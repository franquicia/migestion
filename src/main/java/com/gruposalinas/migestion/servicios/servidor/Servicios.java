package com.gruposalinas.migestion.servicios.servidor;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.business.ActivoFijoBI;
import com.gruposalinas.migestion.business.AgendaBI;
import com.gruposalinas.migestion.business.AncleoCajaBI;
import com.gruposalinas.migestion.business.AprobacionesRemedyHilos;
import com.gruposalinas.migestion.business.ArchivoUrlBI;
import com.gruposalinas.migestion.business.AreaApoyoBI;
import com.gruposalinas.migestion.business.BibliotecaDigitalBI;
import com.gruposalinas.migestion.business.BitacoraMantenimientoBI;
import com.gruposalinas.migestion.business.CajerosAutomaticosBI;
import com.gruposalinas.migestion.business.CartaAsignacionAFBI;
import com.gruposalinas.migestion.business.CatalogoMinuciasBI;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.business.CedulaBI;
import com.gruposalinas.migestion.business.CorreoBI;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.DatosEmpMttoBI;
import com.gruposalinas.migestion.business.DatosUsuarioBI;
import com.gruposalinas.migestion.business.DepuraActFijoBI;
import com.gruposalinas.migestion.business.EstandPuestoBI;
import com.gruposalinas.migestion.business.ExpedienteInactivoBI;
import com.gruposalinas.migestion.business.FormatoMetodo7sBI;
import com.gruposalinas.migestion.business.FotoPlantillaBI;
import com.gruposalinas.migestion.business.FotosAntesDespuesBI;
import com.gruposalinas.migestion.business.GuiaDHLBI;
import com.gruposalinas.migestion.business.HorasPicoBI;
import com.gruposalinas.migestion.business.IncidenciasBI;
import com.gruposalinas.migestion.business.IncidentesAdmBI;
import com.gruposalinas.migestion.business.IncidentesBI;
import com.gruposalinas.migestion.business.IndicadoresIPNBI;
import com.gruposalinas.migestion.business.InfoVistaBI;
import com.gruposalinas.migestion.business.MovilInfoBI;
import com.gruposalinas.migestion.business.MysteryBI;
import com.gruposalinas.migestion.business.MysteryShopperBI;
import com.gruposalinas.migestion.business.OperacionBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.business.ProductividadBI;
import com.gruposalinas.migestion.business.ProductoBI;
import com.gruposalinas.migestion.business.ProgLimpiezaBI;
import com.gruposalinas.migestion.business.ProveedorLoginBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.ReporteChecklistBI;
import com.gruposalinas.migestion.business.SIEPortalBI;
import com.gruposalinas.migestion.business.ServiciosRemedyBI;
import com.gruposalinas.migestion.business.SesionFirmaBI;
import com.gruposalinas.migestion.business.SocioUnicoBI;
import com.gruposalinas.migestion.business.TareaActBI;
import com.gruposalinas.migestion.business.TelSucursalBI;
import com.gruposalinas.migestion.business.TicketCuadrillaBI;
import com.gruposalinas.migestion.business.TicketMantenimientoBI;
import com.gruposalinas.migestion.business.Usuario_ABI;
import com.gruposalinas.migestion.business.ef.CaptacionThread;
import com.gruposalinas.migestion.business.ef.ExtraccionCaptacionThreadBI;
import com.gruposalinas.migestion.business.ef.ExtraccionFinancieraBI;
import com.gruposalinas.migestion.business.ef.ExtraccionFinancieraGraficasBI;
import com.gruposalinas.migestion.business.ef.ExtraccionFinancieraThreadBI;
import com.gruposalinas.migestion.business.ef.IndicadoresBI;
import com.gruposalinas.migestion.business.ef.IndicadoresDetallesBI;
import com.gruposalinas.migestion.business.ef.IndicadoresNegocioCapBI;
import com.gruposalinas.migestion.business.ef.IndicadoresRestBI;
import com.gruposalinas.migestion.business.ef.IndicadoresTiempoRealBI;
import com.gruposalinas.migestion.business.ef.IndicadoresTotalesBI;
import com.gruposalinas.migestion.business.ef.IndicadoresTotalesCaptacionBI;
import com.gruposalinas.migestion.business.ef.InfoTiempoRealBI;
import com.gruposalinas.migestion.business.ef.PorcentajeTiempoRealHijosBI;
import com.gruposalinas.migestion.business.ef.TiempoRealGraficaBI;
import com.gruposalinas.migestion.cliente.RemedyStub.Aprobacion;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfInfoIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfTracking;
import com.gruposalinas.migestion.cliente.RemedyStub.InfoIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.Tracking;
import com.gruposalinas.migestion.cliente.RemedyStub._Calificacion;
import com.gruposalinas.migestion.domain.CecoComboDTO;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import com.gruposalinas.migestion.domain.DatosPerfilDTO;
import com.gruposalinas.migestion.domain.DatosUsuarioDTO;
import com.gruposalinas.migestion.domain.EncuestaDTO;
import com.gruposalinas.migestion.domain.FolioRemedyDTO;
import com.gruposalinas.migestion.domain.HorasPicoDTO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.domain.IndicadorIPNClienteDTO;
import com.gruposalinas.migestion.domain.IndicadorIPNEquipoDTO;
import com.gruposalinas.migestion.domain.MovilInfoDTO;
import com.gruposalinas.migestion.domain.MysteryDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.domain.ProveedorLoginDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
import com.gruposalinas.migestion.domain.TelSucursalDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.domain.TipificacionDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.resources.GTNConstantes.rubrosNegocio;
import com.gruposalinas.migestion.util.StrCipher;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import com.gruposalinas.migestion.util.UtilRequestParameters;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/servicios")
public class Servicios {

    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100; // 100MB

    private static Logger logger = LogManager.getLogger(Servicios.class);

    final GsonBuilder builder = new GsonBuilder();
    final Gson gson = builder.create();

    @Autowired
    ProductividadBI extraccionBI;

    @Autowired
    SesionFirmaBI sesionFirmaBI;

    @Autowired
    TareaActBI tareaActBI;

    @Autowired
    AncleoCajaBI ancleoCajaBI;

    @Autowired
    DatosUsuarioBI datosUsuarioBI;

    @Autowired
    ReporteChecklistBI reporteChecklistBI;

    @Autowired
    IncidentesBI incidenteBI;

    @Autowired
    ProductoBI productoBI;

    @Autowired
    OperacionBI operacionBI;

    @Autowired
    IncidentesAdmBI incidentesAdmBI;

    @Autowired
    ExtraccionFinancieraBI extraccionFinancieraBI;

    @Autowired
    ParametroBI parametroBI;

    @Autowired
    CorreoBI correoBI;

    @Autowired
    AgendaBI agendaBI;

    @Autowired
    MysteryShopperBI mysteryShopperBI;

    @Autowired
    MysteryBI mysteryBI;

    @Autowired
    IndicadoresBI indicadoresBI;

    @Autowired
    IndicadoresTiempoRealBI indicadoresTiempoRealBI;

    @Autowired
    IndicadoresRestBI indicadoresRestBI;

    @Autowired
    IndicadoresDetallesBI indicadoresDetallesBI;

    @Autowired
    CajerosAutomaticosBI cajerosAutomaticosBI;

    @Autowired
    IndicadoresTotalesBI indicadoresTotalesBI;

    @Autowired
    InfoTiempoRealBI indicadoresInfoTiempoRealBI;

    @Autowired
    PorcentajeTiempoRealHijosBI porcentajeTiempoRealHijosBI;

    @Autowired
    ReporteChecklistBI checkBI;

    @Autowired
    TareaActBI tareaBI;

    @Autowired
    CecoBI cecoBi;

    @Autowired
    BibliotecaDigitalBI bibliotecaDigitalBI;

    @Autowired
    MovilInfoBI movilInfoBI;

    @Autowired
    Usuario_ABI usuarioBI;

    @Autowired
    SocioUnicoBI socioUnicoBI;

    @Autowired
    ExtraccionFinancieraGraficasBI graficasColocacionBI;

    @Autowired
    TiempoRealGraficaBI tiempoRealGraficas;

    @Autowired
    InfoVistaBI infoVistaBI;

    @Autowired
    TelSucursalBI telsucursalBI;

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    ServiciosRemedyBI serviciosRemedyBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    DatosEmpMttoBI datosEmpMttoBI;

    @Autowired
    FormatoMetodo7sBI formatoMetodo7sBI;

    @Autowired
    CedulaBI cedulaBI;

    @Autowired
    EstandPuestoBI estandPuestoBI;

    @Autowired
    ProgLimpiezaBI progLimpiezaBI;

    @Autowired
    ArchivoUrlBI archivoUrlBI;

    @Autowired
    ProveedorLoginBI proveedorLoginBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    FotoPlantillaBI fotoPlantillaBI;

    @Autowired
    BitacoraMantenimientoBI bitacoraMantenimientoBI;

    @Autowired
    CatalogoMinuciasBI catalogoMinuciasBI;

    @Autowired
    TicketCuadrillaBI ticketCuadrillaBI;

    @Autowired
    FotosAntesDespuesBI fotosAntesDespuesBI;

    @Autowired
    DepuraActFijoBI depuraActFijoBI;

    @Autowired
    ActivoFijoBI activoFijoBI;

    @Autowired
    ExpedienteInactivoBI expedienteInactivoBI;

    @Autowired
    AreaApoyoBI areaApoyoBI;

    @Autowired
    GuiaDHLBI guiaDHLBI;

    @Autowired
    SIEPortalBI siePortalBI;

    @Autowired
    CartaAsignacionAFBI cartaAsignacionAFBI;

    @Autowired
    TicketMantenimientoBI ticketManrenimientoBI;

    @Autowired
    IndicadoresTotalesCaptacionBI indicadoresTotalesCaptacionBI;

    @Autowired
    HorasPicoBI horasPicoBI;

    @Autowired
    IndicadoresIPNBI indicadoresIPNBI;

    @Autowired
    CecoBI cecoBI;

    /*----------------------------
     /*-----------------------------------------------------------Login por Firma azteca-----------------------------------------------------------*/

 /*
     * // http://localhost:8080/migestion/servicios/validaFirmaAzteca.json?id=<?>&
     * usuarioAdmin=1&firma=<?>&key=1
     *
     * @SuppressWarnings({ "static-access" })
     *
     * @RequestMapping(value = "/validaFirmaAzteca", method = RequestMethod.GET)
     * public @ResponseBody String consumeWebService(HttpServletRequest request,
     * HttpServletResponse response) throws IOException {
     *
     * String ticketSession = ""; String json = ""; DatosUsuarioDTO datosUsuarioDTO
     * = null; Gson g = new GsonBuilder().serializeNulls().create();
     *
     * try { UtilCryptoGS cifra = new UtilCryptoGS(); StrCipher cifraIOS = new
     * StrCipher(); String uri = request.getQueryString(); String uriAp =
     * request.getParameter("token");
     *
     * int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]); uri =
     * uri.split("&")[0]; String urides = "";
     *
     * if (idLlave == 7) { urides = (cifraIOS.decrypt(uri,
     * GTNConstantes.getLlaveEncripcionLocalIOS())); } else if (idLlave == 666) {
     * urides = (cifra.decryptParams(uri)); }
     *
     * String idUsuario = urides.split("&")[0].split("=")[1]; String usuarioAdmin =
     * urides.split("&")[1].split("=")[1]; String token =
     * urides.split("&")[2].split("=")[1]; String key =
     * urides.split("&")[3].split("=")[1]; String flag = "";
     *
     * String[] usuariosP = { "515725", "722878", "203977", "196228", "189140",
     * "191841", "189870", "189871", "191312", "331952", "643965", "664899" };
     *
     * if (urides.split("&").length == 4) flag = "0"; else flag =
     * urides.split("&")[4].split("=")[1];
     *
     * logger.info("Usuario: " + idUsuario); logger.info("Firma: " + token);
     * logger.info("key: " + key); logger.info("flag: " + flag);
     *
     *
     * boolean bandera = false;
     *
     * if (usuarioAdmin.compareTo("0") != 0) { for (int i = 0; i < usuariosP.length;
     * i++) { if (usuarioAdmin.compareTo(usuariosP[i]) == 0) { bandera = true;
     * break; } } } String ws = GTNConstantes.XML_WS_FIRMA_AZTECA;
     *
     * if (bandera) { ws = ws.replace("@idEmpleado", String.valueOf(usuarioAdmin));
     * ws = ws.replace("@token", token); } else { ws = ws.replace("@idEmpleado",
     * String.valueOf(idUsuario)); ws = ws.replace("@token", token); }
     *
     * if (flag.equals("1")) {
     *
     * if (SesionFirmaBI.wsTokenUsuario(GTNConstantes.URL_WS_FIRMA_AZTECA, ws)) {
     * ticketSession = "TEMP"; } else { ticketSession = "ERROR"; } // ticketSession
     * = sesionFirmaBI.createSession(idUsuario, token, key); } else { //
     * ticketSession = "TEMP";
     *
     * if (SesionFirmaBI.wsTokenUsuario(GTNConstantes.URL_WS_FIRMA_AZTECA, ws)) {
     * ticketSession = "TEMP"; } else { ticketSession = "ERROR"; } } if
     * (!ticketSession.equals("ERROR")) {
     *
     * datosUsuarioDTO = new DatosUsuarioDTO(); datosUsuarioDTO =
     * datosUsuarioBI.buscaUsuario(idUsuario);
     * datosUsuarioDTO.setTicket(ticketSession);
     *
     * }
     *
     * } catch (Exception e) { logger.info("Ocurrio algo ");
     *
     * }
     *
     * json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + "}";
     *
     * return json; }
     */
    // http://localhost:8080/migestion/servicios/validaFirmaAzteca.json?id=<?>&usuarioAdmin=1&firma=<?>&key=1
    @SuppressWarnings({"static-access", "unchecked"})
    @RequestMapping(value = "/validaFirmaAzteca7", method = RequestMethod.GET)
    public @ResponseBody
    String consumeWebService(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        List<DatosPerfilDTO> datosPerfil = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String usuarioAdmin = urides.split("&")[1].split("=")[1];
            String token = urides.split("&")[2].split("=")[1];
            String key = urides.split("&")[3].split("=")[1];
            String flag = "";

            String[] usuariosP = {"515725", "722878", "203977", "196228", "189140", "191841", "189870", "189871",
                "191312", "331952", "643965", "664899", "666462",
                "176361", "73429", "165943", "73437", "73433", "73610", "73601", "73606", "370566", "202622",
                "82144", "177870", "40525", "10018465"};

            if (urides.split("&").length == 4) {
                flag = "0";
            } else {
                flag = urides.split("&")[4].split("=")[1];
            }

            //logger.info("Usuario: " + idUsuario);
            //logger.info("Firma: " + token);
            //logger.info("key: " + key);
            //logger.info("flag: " + flag);

            /* Usuarios */
            boolean bandera = false;

            if (usuarioAdmin.compareTo("0") != 0) {
                for (int i = 0; i < usuariosP.length; i++) {
                    if (usuarioAdmin.compareTo(usuariosP[i]) == 0) {
                        bandera = true;
                        break;
                    }
                }
            }
            String ws = GTNConstantes.XML_WS_FIRMA_AZTECA;

            if (bandera) {
                ws = ws.replace("@idEmpleado", String.valueOf(usuarioAdmin));
                ws = ws.replace("@token", token);
            } else {
                ws = ws.replace("@idEmpleado", String.valueOf(idUsuario));
                ws = ws.replace("@token", token);
            }

            if (flag.equals("1")) {

                if (SesionFirmaBI.wsTokenUsuario(GTNConstantes.URL_WS_FIRMA_AZTECA, ws)) {
                    ticketSession = "TEMP";
                } else {
                    ticketSession = "ERROR";
                }
                // ticketSession = sesionFirmaBI.createSession(idUsuario, token, key);
            } else {
                // ticketSession = "TEMP";

                if (SesionFirmaBI.wsTokenUsuario(GTNConstantes.URL_WS_FIRMA_AZTECA, ws)) {
                    ticketSession = "TEMP";
                } else {
                    ticketSession = "ERROR";
                }
            }
            if (!ticketSession.equals("ERROR")) {

                datosUsuarioDTO = new DatosUsuarioDTO();
                Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario);
                datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
                datosUsuarioDTO.setTicket(ticketSession);

                datosPerfil = (List<DatosPerfilDTO>) lista.get("datosPerfil");

            }

        } catch (Exception e) {
            //logger.info("Ocurrio algo ");

        }

        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + ",\"datosPerfil\":" + g.toJson(datosPerfil) + "}";

        return json;
    }

    /**
     * ********************************
     * VALIDA NUEVO SERVICIO TOKEN ********************************
     */
    // http://localhost:8080/migestion/servicios/validaFirmaAzteca.json?id=<?>&usuarioAdmin=1&firma=<?>&key=1
    @SuppressWarnings({"static-access", "unchecked"})
    @RequestMapping(value = "/validaFirmaAztecaOld", method = RequestMethod.GET)
    public @ResponseBody
    String consumeWebService7(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        //logger.info("VALIDA NUEVA FIRMA");
        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        List<DatosPerfilDTO> datosPerfil = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String usuarioAdmin = urides.split("&")[1].split("=")[1];
            String ip = GTNConstantes.getIpOrigen();
            String token = urides.split("&")[2].split("=")[1];
            String ntc = "1";

            String[] usuariosP = {"515725", "722878", "203977", "196228", "189140", "191841", "189870", "189871",
                "191312", "331952", "643965", "664899", "666462",
                "176361", "73429", "165943", "73437", "73433", "73610", "73601", "73606", "370566", "202622",
                "82144", "177870", "40525", "10018465"};

            /*logger.info("Usuario: " + idUsuario);
            logger.info("UsuarioAdmin: " + usuarioAdmin);
            logger.info("Ip: " + ip);
            logger.info("Firma: " + token);
            logger.info("ntc: " + ntc);*/

 /* Usuarios */
            boolean bandera = false;

            // logger.info("usuarioAdmin.compareTo(\"0\"): " + usuarioAdmin.compareTo("0"));
            if (usuarioAdmin.compareTo("0") != 0) {
                for (int i = 0; i < usuariosP.length; i++) {
                    if (usuarioAdmin.compareTo(usuariosP[i]) == 0) {
                        bandera = true;
                        break;
                    }
                }
            }

            JsonObject jsonResult;

            //logger.info("bandera: " + bandera);
            if (bandera) {
                //logger.info("si");

                try {
                    Usuario_ABI usuarioBI = new Usuario_ABI();
                    jsonResult = usuarioBI.validaUsuario(usuarioAdmin, ip, token, ntc);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en el servicio validaUsuario():" + e.getMessage());
                    jsonResult = null;
                }
            } else {
                logger.info("no: ");

                try {
                    Usuario_ABI usuarioBI = new Usuario_ABI();
                    jsonResult = usuarioBI.validaUsuario(idUsuario, ip, token, ntc);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en el servicio validaUsuario():" + e.getMessage());
                    jsonResult = null;
                }
            }

            if (jsonResult != null) {

                if (jsonResult.get("resultado").toString().contains("0")) {
                    //logger.info("firma valida");
                    ticketSession = "TEMP";
                } else {
                    //logger.info("firma no valida");
                    ticketSession = "ERROR";
                }

                if (!ticketSession.equals("ERROR")) {

                    //logger.info("carga");
                    datosUsuarioDTO = new DatosUsuarioDTO();
                    Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario);
                    datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
                    datosUsuarioDTO.setTicket(ticketSession);

                    datosPerfil = (List<DatosPerfilDTO>) lista.get("datosPerfil");

                }
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo ");
        }

        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + ",\"datosPerfil\":" + g.toJson(datosPerfil) + "}";

        return json;
    }

    /**
     * ********************************
     * VALIDA NUEVO SERVICIO TOKEN (Metodo getData() )
     * ********************************
     */
    // http://localhost:8080/migestion/servicios/validaFirmaAzteca.json?id=<?>&usuarioAdmin=1&firma=<?>&key=1
    @SuppressWarnings({"static-access", "unchecked"})
    @RequestMapping(value = "/validaFirmaAzteca", method = RequestMethod.GET)
    public @ResponseBody
    String consumeWebServiceGetData(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        //logger.info("VALIDA NUEVA FIRMA");
        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        List<DatosPerfilDTO> datosPerfil = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String usuarioAdmin = urides.split("&")[1].split("=")[1];
            // String ip = GTNConstantes.getIpOrigen();
            String token = urides.split("&")[2].split("=")[1];
            String ntc = "1";

            String[] usuariosP = {"515725", "722878", "203977", "196228", "189140", "191841", "189870", "189871",
                "191312", "331952", "643965", "664899", "666462",
                "176361", "73429", "165943", "73437", "73433", "73610", "73601", "73606", "370566", "202622",
                "82144", "177870", "40525", "10018465"};

            logger.info("Usuario: " + idUsuario);
            logger.info("UsuarioAdmin: " + usuarioAdmin);
            // logger.info("Ip: " + ip);
            logger.info("Firma: " + token);
            // logger.info("ntc: " + ntc);

            // Usuarios
            boolean bandera = false;

            logger.info("usuarioAdmin.compareTo(\"0\"): " + usuarioAdmin.compareTo("0"));

            if (usuarioAdmin.compareTo("0") != 0) {
                for (int i = 0; i < usuariosP.length; i++) {
                    if (usuarioAdmin.compareTo(usuariosP[i]) == 0) {
                        bandera = true;
                        break;
                    }
                }
            }

            JsonObject jsonResult;

            // logger.info("bandera: " +bandera);
            if (bandera) {
                logger.info("si");
                jsonResult = new JsonObject();
                jsonResult.addProperty("resultado", 0);
            } else {
                logger.info("no: ");

                try {

                    jsonResult = usuarioBI.validaUsuario(idUsuario, token);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en el servicio validaUsuario():" + e.getMessage());
                    jsonResult = null;
                }
            }

            if (jsonResult != null) {

                if (jsonResult.get("resultado").toString().contains("0")) {
                    logger.info("firma valida");
                    ticketSession = "TEMP";
                } else {
                    logger.info("Token_Info (firma no valida para usuario: " + idUsuario + ")");
                    ticketSession = "ERROR";
                }

                if (!ticketSession.equals("ERROR")) {

                    logger.info("carga");

                    datosUsuarioDTO = new DatosUsuarioDTO();
                    Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario);
                    datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
                    datosUsuarioDTO.setTicket(ticketSession);

                    datosPerfil = (List<DatosPerfilDTO>) lista.get("datosPerfil");

                }
            }
        } catch (Exception e) {
            //logger.info("Ocurrio algo ");
        }

        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + ",\"datosPerfil\":" + g.toJson(datosPerfil) + "}";

        return json;
    }

    /**
     * ********************************
     * SERVICIO PARA SALTAR LOGUIN ********************************
     */
    @SuppressWarnings({"static-access", "unchecked"})
    @RequestMapping(value = "/validaFirmaAztecaTest", method = RequestMethod.GET)
    public @ResponseBody
    String consumeWebServiceGetDataTest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        logger.info("VALIDA NUEVA FIRMA");

        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        List<DatosPerfilDTO> datosPerfil = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String usuarioAdmin = urides.split("&")[1].split("=")[1];

            datosUsuarioDTO = new DatosUsuarioDTO();
            Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario);
            datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
            datosUsuarioDTO.setTicket(ticketSession);

            datosPerfil = (List<DatosPerfilDTO>) lista.get("datosPerfil");

        } catch (Exception e) {
            logger.info("Ocurrio algo ");
        }

        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + ",\"datosPerfil\":" + g.toJson(datosPerfil) + "}";

        return json;
    }

    /**
     * ********************************
     * SERVICIO PARA SALTAR LOGUIN ********************************
     */
    /**
     * ********************************
     * VALIDA NUEVO SERVICIO TOKEN ********************************
     */
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/validaFirmaAzteca2", method = RequestMethod.GET)
    public @ResponseBody
    String consumeWebService2(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        logger.info("VALIDA NUEVA FIRMA");

        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        List<DatosPerfilDTO> datosPerfil = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String usuarioAdmin = urides.split("&")[1].split("=")[1];
            // String ip = GTNConstantes.getIpOrigen();
            String token = urides.split("&")[2].split("=")[1];
            String ntc = "1";

            String[] usuariosP = {"515725", "722878", "203977", "196228", "189140", "191841", "189870", "189871",
                "191312", "331952", "643965", "664899", "666462",
                "176361", "73429", "165943", "73437", "73433", "73610", "73601", "73606", "370566", "202622",
                "82144", "177870", "40525", "10018465"};

            logger.info("Usuario: " + idUsuario);
            logger.info("UsuarioAdmin: " + usuarioAdmin);
            // logger.info("Ip: " + ip);
            logger.info("Firma: " + token);
            // logger.info("ntc: " + ntc);

            // Usuarios
            boolean bandera = false;

            logger.info("usuarioAdmin.compareTo(\"0\"): " + usuarioAdmin.compareTo("0"));

            if (usuarioAdmin.compareTo("0") != 0) {
                for (int i = 0; i < usuariosP.length; i++) {
                    if (usuarioAdmin.compareTo(usuariosP[i]) == 0) {
                        bandera = true;
                        break;
                    }
                }
            }

            JsonObject jsonResult;

            // logger.info("bandera: " +bandera);
            if (bandera) {
                logger.info("si");

                try {

                    jsonResult = usuarioBI.validaUsuario(usuarioAdmin, token);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en el servicio validaUsuario():" + e.getMessage());
                    jsonResult = null;
                }
            } else {
                logger.info("no: ");

                try {

                    jsonResult = usuarioBI.validaUsuario(idUsuario, token);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en el servicio validaUsuario():" + e.getMessage());
                    jsonResult = null;
                }
            }

            if (jsonResult != null) {

                if (jsonResult.get("resultado").toString().contains("0")) {
                    logger.info("firma valida");
                    ticketSession = "TEMP";
                } else {
                    logger.info("Token_Info (firma no valida para usuario: " + idUsuario + ")");
                    ticketSession = "ERROR";
                }

                if (!ticketSession.equals("ERROR")) {

                    logger.info("carga");

                    datosUsuarioDTO = new DatosUsuarioDTO();
                    Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario);
                    datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
                    datosUsuarioDTO.setTicket(ticketSession);

                    datosPerfil = (List<DatosPerfilDTO>) lista.get("datosPerfil");

                }
            }
        } catch (Exception e) {
            //logger.info("Ocurrio algo ");
        }

        json = "respuesta({\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + "})";

        return json;
    }

    /*----------------------------
     /*-----------------------------------------------------------Login Nuevo-----------------------------------------------------------*/
    // http://localhost:8080/migestion/servicios/createSession.json?id=<?>&firma=<?>&key=<?>&flag=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/createSession", method = RequestMethod.GET)
    public @ResponseBody
    String loginRestFirma(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        Gson g = new GsonBuilder().serializeNulls().create();
        List<DatosPerfilDTO> datosPerfil = null;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = urides.split("&")[0].split("=")[1];
            String token = urides.split("&")[1].split("=")[1];
            String key = urides.split("&")[2].split("=")[1];
            String flag = "";

            if (urides.split("&").length == 3) {
                flag = "0";
            } else {
                flag = urides.split("&")[3].split("=")[1];
            }

            //logger.info("Usuario: " + idUsuario);
            //logger.info("Firma: " + token);
            //logger.info("key: " + key);
            //logger.info("flag: " + flag);

            /* Usuarios */
            if (flag.equals("1")) {
                ticketSession = sesionFirmaBI.createSession(idUsuario, token, key);
            } else {
                ticketSession = "TEMP";
            }

            if (!ticketSession.equals("ERROR")) {

                datosUsuarioDTO = new DatosUsuarioDTO();
                // datosUsuarioDTO = datosUsuarioBI.buscaUsuario(idUsuario);
                Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario);
                datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
                datosUsuarioDTO.setTicket(ticketSession);
                datosPerfil = (List<DatosPerfilDTO>) lista.get("datosPerfil");

            }

        } catch (Exception e) {
            //e.printStackTrace();
            //logger.info("Ocurrio un algo ");

        }

        // json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + "}";
        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + ",\"datosPerfil\":" + g.toJson(datosPerfil) + "}";

        return json;
    }

    // http://localhost:8080/migestion/servicios/getValidSession.json?idusuario=<?>&ticket=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getValidSession", method = RequestMethod.GET)
    public @ResponseBody
    boolean loginFirmaSession(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean ticketSession = false;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            // String idUsuario = urides.split("&")[0].split("=")[1];
            String ticket = urides.split("&")[1].split("=")[1];

            ticketSession = sesionFirmaBI.checkSession(ticket);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ticketSession;
    }

    // http://localhost:8080/migestion/servicios/closeSession.json?id=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/closeSession", method = RequestMethod.GET)
    public @ResponseBody
    boolean closeSession(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean ticketSession = false;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = urides.split("&")[0].split("=")[1];
            ticketSession = sesionFirmaBI.closeSession(idUsuario);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ticketSession;
    }

    // http://localhost:8080/migestion/servicios/getDataSession.json?id=<?>&ticket=<?>&refer=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDataSession", method = RequestMethod.GET)
    public @ResponseBody
    String getDataSession(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        JsonObject data = new JsonObject();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String ticket = urides.split("&")[1].split("=")[1];
            String refer = urides.split("&")[2].split("=")[1];
            data = sesionFirmaBI.getDataSession(ticket, refer);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data.toString();
    }

    // http://localhost:8080/migestion/servicios/getSurvey.json?Usuario=<?>&ceco=<?>&idTarea=<?>&surveyType=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getSurvey", method = RequestMethod.GET)
    public ModelAndView getSurvey(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException, KeyException, GeneralSecurityException {

        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        String usuario = (urides.split("&")[0]).split("=")[1];
        String ceco = (urides.split("&")[1]).split("=")[1];
        String idTarea = (urides.split("&")[2]).split("=")[1];
        String surveyType = (urides.split("&")[3]).split("=")[1];

        /// encuesta/fetchSurvey?surveyType=C&unitId=186596&userId=730298&projectId=131598
        String parametros = String.format("surveyType=%s&unitId=%s&userId=%s&projectId=%s", surveyType, ceco, usuario,
                idTarea);

        HttpURLConnection conn = null;
        JsonObject jsonObject = null;
        try {
            conn = (HttpURLConnection) new URL(
                    GTNConstantes.getURLProdServicioRest() + GTNConstantes.MTD_FETCH_SURVEY + parametros)
                    .openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            jsonObject = (JsonObject) new JsonParser().parse(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            if (jsonObject.has("CdMsj")) {
                mv.addObject("surveyData", "");
            } else {
                EncuestaDTO surveyData = new GsonBuilder().create().fromJson(jsonObject, EncuestaDTO.class);
                mv.addObject("surveyData", surveyData);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null)// Fortify NULL
            {
                conn.disconnect();
            }
            if (jsonObject != null) {
                jsonObject = null;
            }
        }

        return mv;
    }

    ///////// FIN SERVICIOS TAREAS
    // http://localhost:8080/migestion/servicios/createIncident.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/createIncident", method = RequestMethod.POST)
    public @ResponseBody
    String createIncident(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];

            JsonObject obj = incidenteBI.creaIncidente(contenido);

            if (obj != null) {

                String idIncidente = obj.get("idIncidente").getAsString();

                if (obj.get("result").getAsBoolean()) {

                    incidentesAdmBI.insertaIncidentes(idIncidente, Integer.parseInt(usuario), 1);

                    // Aqui se llama al nuevo bissnes
                }

            }

            json = "{\"incidente\":" + obj.toString() + "}";

        } catch (Exception e) {

        }

        return json;
    }

    // REPORTES DE FRANQUICIA APP
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getPorcentajeFranquiciaCECO", method = RequestMethod.GET)
    public @ResponseBody
    String getPorcentajeFranquiciaCECO(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {
        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";
        Map<String, Object> resultado = null;
        String json = "";
        Gson g = new Gson();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        // System.out.println("URIDES= " + urides);
        int idUsuario = Integer.parseInt((urides.split("&")[0]).split("=")[1]);
        String idReporte = (urides.split("&")[1]).split("=")[1];
        int idCheck = Integer.parseInt((urides.split("&")[2]).split("=")[1]);

        try {

            resultado = reporteChecklistBI.obtieneTotalCheck(idUsuario, Integer.parseInt(idReporte), idCheck);
            json = "{\"porcentajeFranquicia\":" + g.toJson(resultado) + "}";

        } catch (Exception e) {
            logger.info("Ocurrio algo... ");
        }
        return json;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getReporteFranquicia", method = RequestMethod.GET)
    public @ResponseBody
    String getReporteFranquicia(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";
        Map<String, Object> resultado = null;
        String json = "";
        Gson g = new GsonBuilder().serializeNulls().create();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }
        // System.out.println("URIDES= " + urides);
        int idUsuario = Integer.parseInt((urides.split("&")[0]).split("=")[1]);
        String idCeco = (urides.split("&")[1]).split("=")[1];
        String idPais = (urides.split("&")[2]).split("=")[1];
        String idCanal = (urides.split("&")[3]).split("=")[1];
        String bandera = (urides.split("&")[4]).split("=")[1];
        String idReporte = (urides.split("&")[5]).split("=")[1];
        int idCheck = Integer.parseInt((urides.split("&")[5]).split("=")[1]);

        try {
            resultado = reporteChecklistBI.obtieneSucursalesCheck(idUsuario, Integer.parseInt(idReporte), idCeco,
                    Integer.parseInt(idPais), Integer.parseInt(idCanal), Integer.parseInt(bandera), idCheck);
            json = "{\"reporteFranquicia\":" + g.toJson(resultado) + "}";

        } catch (Exception e) {
            logger.info("Ocurri� algo... ");
        }
        return json;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getReporteFranquiciaDetalle", method = RequestMethod.GET)
    public @ResponseBody
    String getReporteFranquiciaDetalle(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";
        Map<String, Object> resultado = null;
        String json = "";
        Gson g = new Gson();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }
        // System.out.println("URIDES= " + urides);
        // int idUsuario =
        // Integer.parseInt((urides.split("&")[0]).split("=")[1]);
        String idCeco = (urides.split("&")[1]).split("=")[1];
        String idReporte = (urides.split("&")[2]).split("=")[1];

        try {
            resultado = reporteChecklistBI.obtieneDetalleCheck(Integer.parseInt(idCeco), Integer.parseInt(idReporte));
            json = "{\"detalleReporteFranquicia\":" + g.toJson(resultado) + "}";

        } catch (Exception e) {
            logger.info("Ocurri� algo... ");
        }
        return json;
    }

    // http://localhost:8080/migestion/servicios/getProductos.json?id=<?>
    @RequestMapping(value = "/getProductos", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getProductos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;

        try {
            /*
             * UtilCryptoGS cifra = new UtilCryptoGS(); StrCipher cifraIOS = new
             * StrCipher(); String uri = request.getQueryString(); String uriAp =
             * request.getParameter("token"); int idLlave =
             * Integer.parseInt((uriAp.split(";")[1]).split("=")[1]); uri =
             * uri.split("&")[0]; /*String urides = "";
             *
             * if (idLlave == 7) { urides = (cifraIOS.decrypt(uri,
             * GTNConstantes.getLlaveEncripcionLocalIOS())); } else if (idLlave == 666) {
             * urides = (cifra.decryptParams(uri)); }
             */
            // String idUsuario = urides.split("&")[0].split("=")[1];
            lista = productoBI.consultaPadres();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    // http://localhost:8080/migestion/servicios/getProductosHijos.json?id=<?>&padre=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getProductosHijos", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getProductosHijos(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            // String idUsuario = urides.split("&")[0].split("=")[1];
            String padre = urides.split("&")[1].split("=")[1];

            lista = productoBI.getHijos(Integer.valueOf(padre));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    // http://localhost:8080/migestion/servicios/getOperaciones.json?id=<?>
    @RequestMapping(value = "/getOperaciones", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getOperaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;

        try {
            /*
             * UtilCryptoGS cifra = new UtilCryptoGS(); StrCipher cifraIOS = new
             * StrCipher(); String uri = request.getQueryString(); String uriAp =
             * request.getParameter("token"); int idLlave =
             * Integer.parseInt((uriAp.split(";")[1]).split("=")[1]); uri =
             * uri.split("&")[0]; String urides = "";
             *
             * if (idLlave == 7) { urides = (cifraIOS.decrypt(uri,
             * GTNConstantes.getLlaveEncripcionLocalIOS())); } else if (idLlave == 666) {
             * urides = (cifra.decryptParams(uri)); }
             */
            // String idUsuario = urides.split("&")[0].split("=")[1];
            lista = operacionBI.consultaPadres();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    // http://localhost:8080/migestion/servicios/getOperacionesHijos.json?id=<?>&padre=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getOperacionesHijos", method = RequestMethod.GET)
    public @ResponseBody
    List<TipificacionDTO> getOperacionesHijos(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<TipificacionDTO> lista = null;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            // String idUsuario = urides.split("&")[0].split("=")[1];
            String padre = urides.split("&")[1].split("=")[1];

            lista = operacionBI.getHijos(Integer.valueOf(padre));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getChecklistUsr", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistUsr(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";
        List<ReporteChecklistDTO> resultado = null;
        String json = "";
        Gson g = new Gson();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }
        // System.out.println("URIDES= " + urides);
        int idUsuario = Integer.parseInt((urides.split("&")[0]).split("=")[1]);

        try {
            resultado = reporteChecklistBI.obtieneListaCheck(idUsuario);
            json = "{\"listaCheck\":" + g.toJson(resultado) + "}";

        } catch (Exception e) {
            logger.info("Ocurri� algo... ");
        }
        return json;
    }

    // CONSUMO COLOCACION PARA TIEMPO REAL
    // GET FECHA PROXIMO DOMINGO
    private String getNextSunday() {

        logger.info("SERVICIOS GET NEXT SUNDAY :");

        String fecha = "";
        Calendar c = Calendar.getInstance();
        // Set the calendar to monday of the current week
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        // Print dates of the current week starting on Monday
        DateFormat df = new SimpleDateFormat("EEE yyyyMMdd");
        for (int i = 0; i < 7; i++) {
            String dia = df.format(c.getTime());
            logger.info("DIA CON FORMATO:" + dia);

            if (dia.contains("Sun") || dia.contains("sun") || dia.contains("dom") || dia.contains("Dom")) {

                fecha = dia.substring(4, dia.length());
                logger.info("SERVICIOS GET NEXT SUNDAY UN DIA:");
                logger.info(fecha);

            }
            c.add(Calendar.DATE, 1);
        }

        logger.info("SERVICIOS GET NEXT SUNDAY FECHA DE RETORNO :" + fecha);
        return fecha;
    }

    public Object listFormatGson(String object, Type listType) {
        Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .create();

        try {
            return gson.fromJson(object, listType);
        } catch (Exception e) {
            return gson.fromJson("[]", listType);
        }
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getFechaTermino", method = RequestMethod.GET)
    public @ResponseBody
    String getFechaTermino(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        int idCheck = 0;
        int idCeco = 0;

        String json = "";

        List<ReporteChecklistDTO> respuesta = null;

        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
        idCheck = Integer.parseInt(urides.split("&")[1].split("=")[1]);
        idCeco = Integer.parseInt(urides.split("&")[2].split("=")[1]);

        try {
            Gson g = new GsonBuilder().serializeNulls().create();
            respuesta = reporteChecklistBI.obtieneFechaTermino(idCheck, idUsuario, idCeco);
            json = "{\"checklist\":" + g.toJson(respuesta) + "}";

            logger.info("respuesta " + respuesta);

        } catch (Exception e) {
            //logger.info(e);
        }

        return json;
    }

    // http://localhost:8080/migestion/servicios/getTipificaciones.json?id=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTipificaciones", method = RequestMethod.GET)
    public @ResponseBody
    String getTipificaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        JsonObject res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            res = incidenteBI.getTipificacion();
        } catch (Exception e) {
            //logger.info(e);
        }
        logger.info(res.toString());
        return res.toString();
    }
    // http://localhost:8080/migestion/servicios/getTotalTiempoReal.json?id=<?>ceco=<?>&nivel=<?>

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTotalTiempoReal", method = RequestMethod.GET)
    public @ResponseBody
    String getTotalTiempoReal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String ceco = urides.split("&")[1].split("=")[1];
            String nivel = urides.split("&")[2].split("=")[1];

            res = indicadoresTiempoRealBI.getRealTotal(ceco, nivel);

        } catch (Exception e) {
            //logger.info(e);
            logger.info("AP: getTotalTiempoReal = " + request.getQueryString());
        }

        //logger.info(res.toString());
        return res.toString();

    }

    // Servicio para obtener COLOCACION PERSONALES y COLOCACION CONSUMO
    // getPorcentajesColocacion
    // http://localhost:8080/migestion/servicios/getPorcentajesColocacion.json?idUsuario=865949&geografia=480100
    // - MEGA DF LA LUNA&nivel=T
    /*
     * @SuppressWarnings("static-access")
     *
     * @RequestMapping(value = "/getPorcentajesColocacion", method =
     * RequestMethod.GET) public @ResponseBody String
     * getPorcentajesColocacion(HttpServletRequest request, HttpServletResponse
     * response) throws UnsupportedEncodingException {
     *
     * Map<String, String> res = null;
     *
     * try{ UtilCryptoGS cifra = new UtilCryptoGS(); StrCipher cifraIOS = new
     * StrCipher(); String uri = request.getQueryString(); String uriAp =
     * request.getParameter("token"); int idLlave =
     * Integer.parseInt((uriAp.split(";")[1]).split("=")[1]); uri =
     * uri.split("&")[0]; String urides = "";
     *
     * if (idLlave == 7) { urides = (cifraIOS.decrypt(uri,
     * GTNConstantes.getLlaveEncripcionLocalIOS())); <<<<<<< .mine } else if
     * (idLlave == 666) {< ======= } else if (idLlave == 666) { urides =
     * (cifra.decryptParams(uri)); }
     *
     * String geografia = urides.split("&")[1].split("=")[1].replace("%20", " ");
     * //T,G,Z String nivel = urides.split("&")[2].split("=")[1];
     *
     *
     * res = indicadoresBI.getPorcentajesColocacion(geografia, nivel);
     *
     * >>>>>>> .r294
     *
     * } catch (Exception e) { logger.info(e); }
     *
     * //logger.info(respuestaService.toString());
     *
     * return res.toString();
     *
     * }
     */
    // http://localhost:8080/migestion/servicios/getCecosCercanos.json?idUsuario=191312&ceco=202297&latitud=<?>&longitud=<?>&valida_dist
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCecosCercanos", method = RequestMethod.GET)
    public @ResponseBody
    List<CecoComboDTO> getCecosCercanos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        List<CecoComboDTO> respuesta = null;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String ceco = urides.split("&")[1].split("=")[1];
            String latitud = urides.split("&")[2].split("=")[1];
            String longitud = urides.split("&")[3].split("=")[1];
            String valida_err = urides.split("&")[4].split("=")[1];

            respuesta = cecoBi.getCecosCercanos(ceco, latitud, longitud, Integer.parseInt(valida_err));

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
        // logger.info(respuestaService.toString());

        return respuesta;

    }

    // http://localhost:8080/migestion/servicios/getParametro.json?idUsuario=191312&parametro=<?>
    @RequestMapping(value = "/getParametro", method = RequestMethod.GET)
    public @ResponseBody
    String getParametro(HttpServletRequest request, HttpServletResponse response) {
        String valorParametro = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String parametro = urides.split("&")[1].split("=")[1];

            List<ParametroDTO> listaParametro = null;

            listaParametro = parametroBI.obtieneParametros(parametro);

            if (listaParametro != null) {
                if (listaParametro.size() > 0) {
                    valorParametro = listaParametro.get(0).getValor();
                } else {
                    valorParametro = "";
                }
            } else {
                valorParametro = "";
            }

        } catch (Exception e) {
            // logger.info("Ocurrio algo al ejecutar el servicio getDataBiblioteca "
            // +e.getMessage());
            valorParametro = "";
            e.printStackTrace();
        }

        return "{ \"valorParametro\":" + valorParametro + "}";
    }

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/estadisticaDispositivo", method = RequestMethod.POST)
    public @ResponseBody
    boolean setEstadisticaDispositivo(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        String fabricante = null;
        String identificador = null;
        String modelo = null;
        String so = null;
        String versionSO = null;
        String numCelular = null;
        String numEmpleado = null;
        String tipoConexion = null;
        String versionApp = null;
        String token = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        //logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + json);
                JSONObject jsonObject = new JSONObject(json);

                fabricante = jsonObject.getString("fabricante");
                identificador = jsonObject.getString("identificador");
                modelo = jsonObject.getString("modelo");
                so = jsonObject.getString("so");
                versionSO = jsonObject.getString("versionSO");
                numCelular = jsonObject.getString("numCelular");
                numEmpleado = jsonObject.getString("numEmpleado");
                tipoConexion = jsonObject.getString("tipoConexion");
                versionApp = jsonObject.getString("versionApp");
                token = jsonObject.getString("tokenUsuario");

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID: " + json);

                JSONObject jsonObject = new JSONObject(json);

                fabricante = jsonObject.getString("fabricante");
                identificador = jsonObject.getString("identificador");
                modelo = jsonObject.getString("modelo");
                so = jsonObject.getString("so");
                versionSO = jsonObject.getString("versionSO");
                numCelular = jsonObject.getString("numCelular");
                numEmpleado = jsonObject.getString("numEmpleado");
                tipoConexion = jsonObject.getString("tipoConexion");
                versionApp = jsonObject.getString("versionApp");
                token = jsonObject.getString("tokenUsuario");

            }

            int usuario = Integer.parseInt(urides.split("=")[1]);

            MovilInfoDTO movilInfoDTO = new MovilInfoDTO();
            movilInfoDTO.setIdUsuario(usuario);
            movilInfoDTO.setSo(so);
            movilInfoDTO.setVersion(versionSO);
            movilInfoDTO.setModelo(modelo);
            movilInfoDTO.setFabricante(fabricante);
            movilInfoDTO.setNumMovil(numCelular);
            movilInfoDTO.setTipoCon(tipoConexion);
            movilInfoDTO.setVersionApp(versionApp);
            movilInfoDTO.setIdentificador(identificador);
            movilInfoDTO.setToken(token);

            logger.info("MOVIL DTO: " + movilInfoDTO);

            boolean infoMovil = movilInfoBI.insertaInfoMovil(movilInfoDTO);

            if (!infoMovil) {
                logger.info("Ocurrió algo  AL INSERTA LA INFORMACIÓN DEL MOVIL");
            }
            return infoMovil;

        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e.getMessage());
        }

        return false;
    }

    // http://localhost:8080/migestion/servicios/enviaDocumento.json?idUsuario=<?>&archivo=<?>&idArchivo=<?>&email=<?>&categoria=<?>&textoItem=<?>&sucursal=<?>&fechaSolicitado=<?>
    @RequestMapping(value = "/enviaDocumento", method = RequestMethod.GET)
    public @ResponseBody
    boolean enviaDocumento(HttpServletRequest request, HttpServletResponse response) {

        boolean respuesta = false;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String nombreArchivo = urides.split("&")[1].split("=")[1];
            String idArchivo = urides.split("&")[2].split("=")[1];
            String email = urides.split("&")[3].split("=")[1];
            String menu = urides.split("&")[4].split("=")[1];
            String itemMenu = urides.split("&")[5].split("=")[1];
            String sucursal = urides.split("&")[6].split("=")[1];
            String fechaSolicitado = urides.split("&")[7].split("=")[1];

            // File adjunto = new
            // File("http://10.51.210.237/biblioteca_digital/archivos/"+nombreArchivo);
            respuesta = bibliotecaDigitalBI.enviaDocumento(email, null, menu, itemMenu, nombreArchivo,
                    Integer.parseInt(idArchivo), sucursal, fechaSolicitado);

        } catch (Exception e) {
            e.printStackTrace();
            return respuesta;
        }

        return respuesta;
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCecoNivel", method = RequestMethod.GET)
    public @ResponseBody
    String getCecoNivel(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        int idCeco = 0;
        String json = "";

        List<MysteryDTO> respuesta = null;

        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
        idCeco = Integer.parseInt(urides.split("&")[1].split("=")[1]);

        try {
            Gson g = new GsonBuilder().serializeNulls().create();
            respuesta = mysteryBI.consultaNivel(idCeco + "");
            json = "{\"listaCeco\":" + g.toJson(respuesta) + "}";

            logger.info("respuesta " + respuesta);

        } catch (Exception e) {
            //logger.info(e);
        }

        return json;
    }

    // PARA VALIDAR UN VALOR ANTES DE PASARLO AL FRONT
    public String xssMostrar(String stringInput) {
        stringInput = stringInput.replaceAll("&amp;", "&");
        stringInput = stringInput.replaceAll("&Amp;", "&");

        stringInput = stringInput.replaceAll("&lt;", "<");
        stringInput = stringInput.replaceAll("&Lt;", "<");

        stringInput = stringInput.replace("&gt;", ">");
        stringInput = stringInput.replace("&Gt;", ">");

        stringInput = stringInput.replace("&quot;", "\"");
        stringInput = stringInput.replace("&Quot;", "\"");

        stringInput = stringInput.replace("&apos;", "'");
        stringInput = stringInput.replace("&Apos;", "'");
        return stringInput;
    }

    // PARA VALIDAR UN PARÁMETRO ANTES DE GUARDARLO EN BD
    public static String cleanParameter(String parameter) {
        if (parameter != null) {
            if (parameter.matches("\\d{4}/\\d{2}/\\d{2}") || parameter.matches("\\d{2}/\\d{2}/\\d{4}")) {
                parameter = parameter.replaceAll(":", "").replaceAll("\\\\", "").replaceAll("\\.", "");
                parameter = parameter.replaceAll("%2e", "").replaceAll("%2E", "").replaceAll("%2f", "")
                        .replaceAll("%2F", "").replaceAll("%5c", "").replaceAll("%5C", "");
            } else {
                parameter = parameter.replaceAll(":", "").replaceAll("/", "").replaceAll("\\\\", "").replaceAll("\\.",
                        "");
                parameter = parameter.replaceAll("%2e", "").replaceAll("%2E", "").replaceAll("%2f", "")
                        .replaceAll("%2F", "").replaceAll("%5c", "").replaceAll("%5C", "");
            }
        }

        return parameter;
    }

    private int round(double d) {
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - (double) i;
        if (result < 0.5) {
            return d < 0 ? -i : i;
        } else {
            return d < 0 ? -(i + 1) : i + 1;
        }
    }

    public class AsesoresZR {

        public int idCeco;
        public String nomCeco;

        AsesoresZR(int idCeco, String nomCeco) {
            this.idCeco = idCeco;
            this.nomCeco = nomCeco;
        }

        public void setIdCeco(int idCeco) {
            this.idCeco = idCeco;
        }

        public void setNomCeco(String nomCeco) {
            this.nomCeco = nomCeco;
        }

        public int getIdCeco() {
            return this.idCeco;
        }

        public String getNomCeco() {
            return this.nomCeco;
        }
    }

    private String getNameToday() {

        String hoy = "";

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {

            case Calendar.MONDAY:
                hoy = "lunes";
                break;
            case Calendar.TUESDAY:
                hoy = "martes";
                break;
            case Calendar.WEDNESDAY:
                hoy = "miercoles";
                break;
            case Calendar.THURSDAY:
                hoy = "jueves";
                break;
            case Calendar.FRIDAY:
                hoy = "viernes";
                break;
            case Calendar.SATURDAY:
                hoy = "sabado";
                break;
            case Calendar.SUNDAY:
                hoy = "domingo";
                break;
        }

        return hoy;

    }

    /*
    public DatosRHDTO getMetodoDatosRH(String strCC, String strPeriodo) {

        logger.info("************* ENTRE AL Metodo getDatosRH *************");

        String json = "";
        DatosRHDTO datosRHDTO = null;

        logger.info("Ceco: " + strCC + "   periodo: " + strPeriodo);

        String fecha = "";

        // ****************Saca fecha para la llamada al servicio*********************
        try {
            int iAnio = Integer.parseInt(strPeriodo.substring(0, 4));
            int iMes = Integer.parseInt(strPeriodo.substring(4, 6));
            int iDia = Integer.parseInt(strPeriodo.substring(6, strPeriodo.length()));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            Calendar calendar3 = new GregorianCalendar(iAnio, iMes - 1, iDia);
            int diaSem = calendar3.get(Calendar.DAY_OF_WEEK);

            if (diaSem == 2 || diaSem == 3) {
                calendar3.add(Calendar.DATE, -7);
                fecha = sdf.format(calendar3.getTime());
            } else {
                fecha = sdf.format(calendar3.getTime());
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo 1.. ");
            fecha = strPeriodo;
        }
        // **************************************************************************

        try {
            json = MiGestionPruebas.consulta(strCC, fecha);
            // logger.info("JSON SALDA: "+json);

            if (json != null && !json.equals("null") && !json.isEmpty()) {
                JSONArray jsonArr = new JSONArray(json);

                String datosFinal = jsonArr.getString(jsonArr.length() - 1);
                JSONObject jsonObjFinal = new JSONObject(datosFinal);

                datosRHDTO = new DatosRHDTO();
                datosRHDTO.setGarantiaTotal(MiGestionPruebas.validaDecimalEntero(
                        Math.rint((Double.parseDouble(jsonObjFinal.getString("Garantia total"))) * 1) / 1));
                datosRHDTO.setGarantizados(Integer.parseInt(jsonObjFinal.getString("Garantizados")));
                datosRHDTO.setNoGarantizados(Integer.parseInt(jsonObjFinal.getString("No garantizados")));

            } else {
                datosRHDTO = null;
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo 2... ");
            datosRHDTO = null;
        }

        logger.info("************* SALI DE Metodo getDatosRH *************");
        return datosRHDTO;
    }
     */
    /**
     * ********************************
     * VALIDA NUEVO SERVICIO TOKEN ********************************
     */
    // http://localhost:8080/migestion/servicios/validaUsuario.json?usuario=<?>&ip=<?>&token=<?>&ntc=<?>
    @RequestMapping(value = "/validaUsuario", method = RequestMethod.GET)
    public @ResponseBody
    String validaUsuario(HttpServletRequest request, HttpServletResponse response, Model model)
            throws KeyException, GeneralSecurityException, IOException {

        JsonObject jsonResult;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String usuario = urides.split("&")[0].split("=")[1];
            String ip = urides.split("&")[1].split("=")[1];
            String token = urides.split("&")[2].split("=")[1];
            String ntc = urides.split("&")[3].split("=")[1];

            Usuario_ABI usuarioBI = new Usuario_ABI();
            jsonResult = usuarioBI.validaUsuario(usuario, ip, token, ntc);

        } catch (Exception e) {
            logger.info("Ocurrio algo en el servicio validaUsuario():" + e.getMessage());
            return null;
        }
        return jsonResult.toString();
    }

    /**
     * ********************************
     * VALIDA NUEVO SERVICIO TOKEN ********************************
     */
    // http://localhost:8080/migestion/servicios/getDatosEmpleadoSU.json?idUsuario=<?>&idEmpleado=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDatosEmpleadoSU", method = RequestMethod.GET)
    public @ResponseBody
    String getDatosEmpleadoSU(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String usuarioSesion = urides.split("&")[0].split("=")[1];
            String idEmpleado = urides.split("&")[1].split("=")[1];

            res = socioUnicoBI.datosEmpleado(Integer.parseInt(idEmpleado));

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }
        // logger.info(respuestaService.toString());

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getSuenoSU.json?idUsuario=<?>&idEmpleado=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getSuenoSU", method = RequestMethod.GET)
    public @ResponseBody
    String getSueñoSU(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String usuarioSesion = urides.split("&")[0].split("=")[1];
            String idEmpleado = urides.split("&")[1].split("=")[1];

            res = socioUnicoBI.getSueno(Integer.parseInt(idEmpleado));

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }
        // logger.info(respuestaService.toString());

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getEmpleadosFestejosSU.json?idUsuario=<?>&idEmpleado=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getEmpleadosFestejosSU", method = RequestMethod.GET)
    public @ResponseBody
    String getEmpleadosFestejosSU(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String usuarioSesion = urides.split("&")[0].split("=")[1];
            String idEmpleado = urides.split("&")[1].split("=")[1];

            res = socioUnicoBI.getEmpleadosFestejados(Integer.parseInt(idEmpleado));

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }
        // logger.info(respuestaService.toString());

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getDataGraficaColocacion.json?idEmpleado=<?>&fecha=<?>&geografia=<?>&nivel=<?>&indicador=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDataGraficaColocacion", method = RequestMethod.GET)
    public @ResponseBody
    String getDataGraficaColocacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idEmpleado = urides.split("&")[0].split("=")[1];
            String fecha = urides.split("&")[1].split("=")[1];
            String geografia = urides.split("&")[2].split("=")[1].replace("%20", " ");
            String nivel = urides.split("&")[3].split("=")[1];
            String indicador = urides.split("&")[4].split("=")[1];

            graficasColocacionBI.setFecha(fecha);
            graficasColocacionBI.setGeografia(geografia);
            graficasColocacionBI.setNivel(nivel);
            graficasColocacionBI.setIdIndicador(Integer.parseInt(indicador));

            res = graficasColocacionBI.getData();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        // logger.info(respuestaService.toString());
        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    @SuppressWarnings("static-access")
    // http://10.50.50.109.47:8080/migestion/servicios/getInfoVista.json
    @RequestMapping(value = "/getInfoVista", method = RequestMethod.POST)
    public @ResponseBody
    boolean getInfoVista(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        /*logger.info("ENTRE A getInfoVista: ");

        boolean retorno = true;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String json = "";

        UtilCryptoGS descifra = new UtilCryptoGS();
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider, "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());
                //logger.info("JSON IOS: " + json);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                json = descifra.decryptParams(provider);
                logger.info("JSON ANDROID: " + json);
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo en llave de getInfoVista");
            retorno = false;
        }

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjectHijo = jsonArray.getJSONObject(i);
                InfoVistaDTO obj = new InfoVistaDTO();
                obj.setIdCatVista(jsonObjectHijo.getInt("idCatalogo"));
                obj.setIdUsuario(Integer.parseInt(urides.split("=")[1]));
                obj.setPlataforma(jsonObjectHijo.getInt("plataforma"));
                obj.setFecha(jsonObjectHijo.getString("fecha"));

                //logger.info("Estado del activo " + infoVistaBI.insertaInfoVista(obj));
            }
            retorno = true;
        } catch (Exception e) {
            //logger.info(e);
            retorno = false;
        }

        //logger.info("SALI DE getInfoVista: " + retorno);

        return retorno;*/
        return true;
    }

    // http://localhost:8080/migestion/servicios/getDataGraficaTiempoReal.json?idEmpleado=<?>&ceco=<?>&producto=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDataGraficaTiempoReal", method = RequestMethod.GET)
    public @ResponseBody
    String getDataGraficaTiempoReal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idEmpleado = urides.split("&")[0].split("=")[1];
            String ceco = urides.split("&")[1].split("=")[1];
            String producto = urides.split("&")[2].split("=")[1].replace("%20", " ");

            tiempoRealGraficas.setCeco(ceco);
            tiempoRealGraficas.setProducto(Integer.parseInt(producto));

            res = tiempoRealGraficas.getData();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        // logger.info(respuestaService.toString());
        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // --------------------------------------------------------------- Proyecto
    // Mantenimiento
    // ---------------------------------------------------------------------
    // http://localhost:8080/migestion/servicios/getTelefonoSucursal.json?ceco=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTelefonoSucursal", method = RequestMethod.GET)
    public @ResponseBody
    String getTelefonoSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String ceco = urides.split("&")[1].split("=")[1];
            // String idAlcance = urides.split("&")[2].split("=")[1];
            int noSucursal = 0;
            String cecoS = ceco + "";
            if (cecoS.length() == 6) {
                noSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
            }
            if (cecoS.length() == 5) {
                noSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
            }

            int idCeco = noSucursal;

            List<TelSucursalDTO> listaTelefono = telsucursalBI.obtienetelefono(idCeco);

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray telefonoJsonArray = new JsonArray();

            if (!listaTelefono.isEmpty() && listaTelefono != null) {
                for (TelSucursalDTO telefono : listaTelefono) {
                    JsonObject telJsonObj = new JsonObject();
                    telJsonObj.addProperty("ID_CECO", telefono.getIdCeco());
                    telJsonObj.addProperty("TELEFONO", telefono.getTelefono());
                    telJsonObj.addProperty("ESTATUS", telefono.getEstatus());
                    telJsonObj.addProperty("PROVEDOR", telefono.getProveedor());
                    telefonoJsonArray.add(telJsonObj);
                }

            }

            if (listaTelefono.isEmpty()) {
                envoltorioJsonObj.add("TELEFONOS", null);
            } else {
                envoltorioJsonObj.add("TELEFONOS", telefonoJsonArray);
            }
            //logger.info("JSON Calificaciones: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getTipoIncidencias.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTipoIncidencias", method = RequestMethod.GET)
    public @ResponseBody
    String getTipoIncidencias(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            // String ceco = urides.split("&")[1].split("=")[1];
            // String idAlcance = urides.split("&")[2].split("=")[1];

            List<IncidenciasDTO> lista = incidenciasBI.obtieneInfo();

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray incidenciasJsonArray = new JsonArray();

            if (!lista.isEmpty() && lista != null) {
                for (IncidenciasDTO ins : lista) {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("ID_TIPO", ins.getIdTipo());
                    insJsonObj.addProperty("N1_SERVICIO", ins.getServicio());
                    insJsonObj.addProperty("N2_UBICACION", ins.getUbicacion());
                    insJsonObj.addProperty("N3_INCIDENCIA", ins.getIncidencia());
                    insJsonObj.addProperty("PLANTILLA", ins.getPlantilla());
                    insJsonObj.addProperty("STATUS", ins.getStatus());
                    incidenciasJsonArray.add(insJsonObj);
                }
            }

            if (lista.isEmpty()) {
                envoltorioJsonObj.add("FALLAS", null);
            } else {
                envoltorioJsonObj.add("FALLAS", incidenciasJsonArray);
            }
            //logger.info("JSON Calificaciones: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/altaIncidente.json?ceco=<?>&nomSucursal=<?>&
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/altaIncidente", method = RequestMethod.GET)
    public @ResponseBody
    String altaIncidente(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            // String ceco = urides.split("&")[1].split("=")[1];
            // String idAlcance = urides.split("&")[2].split("=")[1];

            int ceco = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            int noSucursal = 0;
            String cecoS = ceco + "";
            if (cecoS.length() == 6) {
                noSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
            }
            if (cecoS.length() == 5) {
                noSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
            }
            String nomSucursal = urides.split("&")[2].split("=")[1];
            String telSucursal = urides.split("&")[3].split("=")[1];
            CorreosLotus correosLotus = new CorreosLotus();
            logger.info("Correo SUCURSAL: " + correosLotus.validaUsuarioLotusCorreos("331952"));
            logger.info("Correo SUCURSAL: " + correosLotus.validaUsuarioLotusCorreos("G0100"));
            // logger.info("Correo SUCURSAL:
            // "+correosLotus.validaUsuarioLotusCorreos(cecoS));
            String correo = "";

            if (cecoS.length() == 6) {
                correo = correosLotus.validaUsuarioLotusCorreos("G" + cecoS.substring(2, cecoS.length()));
            }
            if (cecoS.length() == 5) {
                correo = correosLotus.validaUsuarioLotusCorreos("G" + cecoS.substring(1, cecoS.length()));
            }

            int numEmpleado = Integer.parseInt(urides.split("&")[4].split("=")[1]);
            String nombre = urides.split("&")[5].split("=")[1];
            String puesto = urides.split("&")[6].split("=")[1];
            String comentario = urides.split("&")[7].split("=")[1];
            String puestoAlterno = urides.split("&")[8].split("=")[1];
            String telUsrAlterno = urides.split("&")[9].split("=")[1];
            String nomUsrAlterno = urides.split("&")[10].split("=")[1];
            String servicio = urides.split("&")[11].split("=")[1];
            String ubicacion = urides.split("&")[12].split("=")[1];
            String incidencia = urides.split("&")[13].split("=")[1];
            String nomAdjunto = null;
            String adjunto = null;
            if (urides.split("&").length >= 16) {
                nomAdjunto = urides.split("&")[14].split("=")[1];
                adjunto = urides.split("&")[15].split("=")[1];
            }
            // else {
            // nomAdjunto="sinImagen";
            // }

            FolioRemedyDTO bean = new FolioRemedyDTO();
            bean.setCeco(ceco);
            bean.setComentario(comentario);
            bean.setCorreo(correo);
            bean.setIncidencia(incidencia);
            bean.setNombre(nombre);
            bean.setNomSucursal(nomSucursal);
            bean.setNomUsrAlterno(nomUsrAlterno);
            bean.setNoSucursal(noSucursal);
            bean.setNumEmpleado(numEmpleado);
            bean.setPuesto(puesto);
            bean.setPuestoAlterno(puestoAlterno);
            bean.setServicio(servicio);
            bean.setTelSucursal(telSucursal);
            bean.setTelUsrAlterno(telUsrAlterno);
            bean.setUbicacion(ubicacion);
            bean.setNomAdjunto(nomAdjunto);
            bean.setAdjunto(adjunto);

            // int idFolio=0;
            int idFolio = serviciosRemedyBI.levantaFolio(bean);

            JsonObject envoltorioJsonObj = new JsonObject();

            if (idFolio == 0) {
                envoltorioJsonObj.add("FOLIO", null);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("ID_FOLIO", idFolio);
                envoltorioJsonObj.add("FOLIO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    /*
    // http://localhost:8080/migestion/servicios/getIncidentesSucursal.json?ceco=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesSucursal", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesSucursalHilos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String ceco = urides.split("&")[1].split("=")[1];

            // int idFolio=0;
            //ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.ConsultaFoliosSucursal(numSucursal,"0");
            int numSucursal = 0;
            String cecoS = ceco + "";
            if (cecoS.length() == 6) {
                numSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
            }
            if (cecoS.length() == 5) {
                numSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
            }

            ArrayOfInfoIncidente arrIncidentesAbiertos = serviciosRemedyBI.ConsultaFoliosSucursal(numSucursal, "1");
            ArrayOfInfoIncidente arrIncidentesCerrados = serviciosRemedyBI.ConsultaFoliosSucursal(numSucursal, "2");

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray incidenciasJsonArray = new JsonArray();

            ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

            //-----------------------Folios Abiertos--------------------------
            InfoIncidente[] arregloInc = null;
            if (arrIncidentesAbiertos != null) {
                arregloInc = arrIncidentesAbiertos.getInfoIncidente();
                if (arregloInc != null && arregloInc.length > 0) {

                    for (InfoIncidente aux : arregloInc) {
                        //        if (aux.getPrioridad().getValue().contains("Normal")) {
                        //            normal.add(aux);
                        //        }
                        //        if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                        //        }
                    }
                } else {
                    arrIncidentesAbiertos = serviciosRemedyBI.ConsultaFoliosSucursal(Integer.parseInt(ceco), "3");

                    if (arrIncidentesAbiertos != null) {
                        arregloInc = arrIncidentesAbiertos.getInfoIncidente();
                        if (arregloInc != null && arregloInc.length > 0) {
                            for (InfoIncidente aux : arregloInc) {
                                Critica.add(aux);
                            }
                        }
                    }

                }
            }
            //-----------------------Folios Cerrados--------------------------
            if (arrIncidentesCerrados != null) {
                arregloInc = arrIncidentesCerrados.getInfoIncidente();
                if (arregloInc != null && arregloInc.length > 0) {

                    for (InfoIncidente aux : arregloInc) {
                        //        if (aux.getPrioridad().getValue().contains("Normal")) {
                        //            normal.add(aux);
                        //        }
                        //        if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                        //        }
                    }
                } else {
                    arrIncidentesCerrados = serviciosRemedyBI.ConsultaFoliosSucursal(Integer.parseInt(ceco), "4");
                    if (arrIncidentesCerrados != null) {
                        arregloInc = arrIncidentesCerrados.getInfoIncidente();
                        if (arregloInc != null && arregloInc.length > 0) {
                            for (InfoIncidente aux : arregloInc) {
                                Critica.add(aux);
                            }
                        }
                    }

                }
            }
            //--------------Ejecuta hilos aprobaciones--------------------
            ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

            if (Critica != null && !Critica.isEmpty()) {
                for (InfoIncidente aux : Critica) {
                    aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                }
            }

            if (normal != null && !normal.isEmpty()) {
                for (InfoIncidente aux : normal) {
                    aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                }
            }

            for (int i = 0; i < aprHilos.size(); i++) {
                aprHilos.get(i).run();
            }

            boolean terminaron = false;

            long startTime1 = System.currentTimeMillis();
            logger.info("Entro a verificar hilos");
            if (aprHilos == null || aprHilos.isEmpty()) {
                terminaron = true;
            }
            while (!terminaron) {
                for (int i = 0; i < aprHilos.size(); i++) {
                    if (aprHilos.get(i).getRespuesta() != null) {
                        terminaron = true;
                    } else {
                        terminaron = false;
                        break;
                    }
                    if (!aprHilos.get(i).isAlive()) {
                        terminaron = true;
                    } else {
                        terminaron = false;
                        break;
                    }
                }
            }

            HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
            for (int i = 0; i < aprHilos.size(); i++) {
                AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
            }

            long endTime1 = System.currentTimeMillis() - startTime1;
            logger.info("TIEMPO RESP en Hilos: " + endTime1);
            //-------------------------------------------------------------

            incidenciasJsonArray = arrOrdenadoSucursalHilos(Critica, normal, AprobacionesHashMap);

            if (arrIncidentesAbiertos == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("FOLIOS", incidenciasJsonArray);
            }
            //logger.info("JSON ID FOLIOS SUCURSAL: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
           // logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }
     */
 /*public JsonArray arrOrdenadoSucursalHilos(ArrayList<InfoIncidente> Critica, ArrayList<InfoIncidente> normal, HashMap<String, Aprobacion> AprobacionesHashMap) {
        JsonArray arrJsonArray = new JsonArray();
        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();

        HashMap<String, String> EmpleadosRemedyHashMap = serviciosRemedyBI.consultaUsuariosRemedy();

        while (!Critica.isEmpty()) {
            int pos = 0;
            for (int i = 0; i < Critica.size(); i++) {
                if (Critica.get(pos).getFechaCreacion().compareTo(Critica.get(i).getFechaCreacion()) <= 0) {
                    pos = i;
                }
            }
            InfoIncidente aux = Critica.get(pos);

            //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
            JsonObject incJsonObj = new JsonObject();

            aux.getAutorizaCambioMonto();
            incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
            incJsonObj.addProperty("ESTADO", aux.getEstado());
            incJsonObj.addProperty("FALLA", aux.getFalla());
            incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
            incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
            incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
            incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
            incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
            incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());
            String idCliente = aux.getIdCorpCliente();
            if (idCliente != null) {
                if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                    List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                    if (!datos_cliente.isEmpty() && datos_cliente != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                            incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                            incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                            EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                        incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                    incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                    incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                }
            } else {
                incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
            }
            String idCord = aux.getIDCorpCoordinador();
            if (idCord != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                }
            } else {
                incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
            }

            if (idCord != null) {
                if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                    List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(idCord));
                    if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                            incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            EmpleadosHashMap.put(idCord.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("COORDINADOR", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                    incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                }

            } else {
                incJsonObj.addProperty("COORDINADOR", "* Sin información");
            }
            String idSup = aux.getIdCorpSupervisor();

            if (idSup != null) {
                idSup = aux.getIdCorpSupervisor().trim();
                if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                }
            } else {
                incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
            }

            if (idSup != null) {
                if (!idSup.matches("[^0-9]*")) {

                    if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(idSup));

                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                EmpleadosHashMap.put(idSup.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                        incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                        incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }
            } else {
                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
            }

            incJsonObj.addProperty("NOTAS", aux.getNotas());
            incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
            incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

            if (aux.getProveedor() != null) {
                if (!lista.isEmpty()) {
                    for (ProveedoresDTO aux3 : lista) {
                        if (aux3.getStatus().contains("Activo")) {
                            if (aux.getProveedor().toLowerCase().trim()
                                    .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                incJsonObj.addProperty("MENU", aux3.getMenu());
                                incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                break;
                            }

                        }
                    }
                }
            } else {
                String nomCorto = null;
                incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                incJsonObj.addProperty("MENU", nomCorto);
                incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
            }

            incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
            incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
            incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
            incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
            incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

            incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
            incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
            Date date = aux.getFechaCierre().getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
            date = aux.getFechaCreacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CREACION", "" + date1);
            date = aux.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
            date = aux.getFechaProgramada().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
            incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
            incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());
            String autCleinte = aux.getAutorizacionCliente();
            if (autCleinte != null && !autCleinte.equals("")) {
                if (autCleinte.contains("Rechazado")) {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                }
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
            }

            if (aux.getTipoAtencion() != null) {
                incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("TIPO_ATENCION", x);
            }
            if (aux.getOrigen() != null) {
                incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("ORIGEN", x);
            }
            //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
            String cord = null;
            if (aux.getLatitudInicio() != null) {
                incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
            } else {
                incJsonObj.addProperty("LATITUD_INICIO", cord);
            }

            if (aux.getLongitudInicio() != null) {
                incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
            } else {
                incJsonObj.addProperty("LONGITUD_INICIO", cord);
            }

            if (aux.getLatitudCierre() != null) {
                incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
            } else {
                incJsonObj.addProperty("LATITUD_FIN", cord);
            }

            if (aux.getLongitudCierre() != null) {
                incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
            } else {
                incJsonObj.addProperty("LONGITUD_FIN", cord);
            }

            //----------------------------------------------------------------------------------
            date = aux.getFechaInicioAtencion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

            int idIncidente = aux.getIdIncidencia();

            List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
            int idProvedor = 0;
            int idCuadrilla = 0;
            int idZona = 0;
            for (TicketCuadrillaDTO aux2 : lista2) {
                if (aux2.getStatus() != 0) {
                    idProvedor = aux2.getIdProveedor();
                    idCuadrilla = aux2.getIdCuadrilla();
                    idZona = aux2.getZona();
                }

            }
            if (idProvedor != 0) {
                List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                for (CuadrillaDTO aux2 : lista3) {
                    if (aux2.getZona() == idZona) {
                        if (aux2.getIdCuadrilla() == idCuadrilla) {
                            incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                            incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux2.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                            incJsonObj.addProperty("ZONA", aux2.getZona());
                        }
                    }
                }
            } else {
                String auxiliar = null;
                //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                incJsonObj.addProperty("LIDER", auxiliar);
                incJsonObj.addProperty("CORREO", auxiliar);
                incJsonObj.addProperty("SEGUNDO", auxiliar);
                incJsonObj.addProperty("ZONA", auxiliar);
            }

            if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);

                String estadoApr = aprobacion.getEstadoAprobacion();
                boolean flag1 = false, flag2 = false;
                if (estadoApr != null) {
                    String porvAprob = aprobacion.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_CMONTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                }

                Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                estadoApr = aprobacion2.getEstadoAprobacion();
                if (estadoApr != null) {
                    String porvAprob = aprobacion2.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                }

                if (flag1 == true && flag2 == true) {
                    incJsonObj.addProperty("BANDERAS", 1);
                } else {
                    incJsonObj.addProperty("BANDERAS", 0);
                }
            } else {
                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                incJsonObj.addProperty("BANDERAS", 0);
            }

            //-----------------------  Consulta Traking  --------------------
            ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

            JsonArray incidenciasJsonArray = new JsonArray();

            ArrayList<Date> RechazoCliente = new ArrayList<Date>();
            ArrayList<Date> inicioAtencion = new ArrayList<Date>();
            ArrayList<Date> cierreTecnico = new ArrayList<Date>();
            ArrayList<Date> cancelados = new ArrayList<Date>();
            ArrayList<Date> atencionGarantia = new ArrayList<Date>();
            ArrayList<Date> cierreGarantia = new ArrayList<Date>();
            ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null) {
                    for (Tracking aux2 : traking) {
                        String detalle = aux2.getDetalle();
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date2 = aux2.getFechaEnvio().getTime();
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                            RechazoCliente.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                            inicioAtencion.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                            cierreTecnico.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado") || motivoEstado.trim().toLowerCase().equals("cancelado no aplica") || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                            cancelados.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                            atencionGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                            cierreGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("recibido por atender") && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                            rechazoProveedor.add(date2);
                        }
                    }
                }
            }

            int posRC = 0, posRP = 0;

            String cadAux = null;
            if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < RechazoCliente.size(); i++) {
                    if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = RechazoCliente.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                posRC = pos2;
            } else {

                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
            }

            if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < inicioAtencion.size(); i++) {
                    if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = inicioAtencion.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION", cadAux);
            }

            if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreTecnico.size(); i++) {
                    if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreTecnico.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                incJsonObj.addProperty("FECHA_CIERRE", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                incJsonObj.addProperty("FECHA_CIERRE", cadAux);
            }

            if (cancelados != null && !cancelados.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cancelados.size(); i++) {
                    if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cancelados.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CANCELACION", date3);
            } else {
                incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
            }
            //
            if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < atencionGarantia.size(); i++) {
                    if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = atencionGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
            }

            if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreGarantia.size(); i++) {
                    if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
            }

            if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < rechazoProveedor.size(); i++) {
                    if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = rechazoProveedor.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                posRP = pos2;
            } else {
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
            }

            //BANDERA_RECHAZO_CIERRE_PREVIO
            if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null && !RechazoCliente.isEmpty()) {
                if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }
            } else {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
            }

            //------------------------------------------------------------------
            int cal = 0;
            _Calificacion auxEval = aux.getEvalCliente();
            if (auxEval != null) {
                if (auxEval.getValue().contains("NA")) {
                    cal = 0;
                }
                if (auxEval.getValue().contains("Item1")) {
                    cal = 1;
                }
                if (auxEval.getValue().contains("Item2")) {
                    cal = 2;
                }
                if (auxEval.getValue().contains("Item3")) {
                    cal = 3;
                }
                if (auxEval.getValue().contains("Item4")) {
                    cal = 4;
                }
                if (auxEval.getValue().contains("Item5")) {
                    cal = 5;
                }
            }

            incJsonObj.addProperty("CALIFICACION", cal);

            incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

            date = aux.getFechaCierreAdmin().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

            incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

            //------------------------------ COMENTARIOS PARA PROVEEDOR --------------------------
            //if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
            Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 2);
             if(aprobacion!=null) {
             incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
             incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
             date = aprobacion.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
             incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
             }
            if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                date = aprobacion.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
                Date dateI = aux.getFechaInicioAtencion().getTime();

                if (date.compareTo(dateI) >= 0) {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }
            } else {
                String auxApr = null;
                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
            }

            //-----------------------------------------------------------------------------------
            //----------------------------FECHA CANCELACION----------------------------------------
            if( aux.getEstado().trim().toLowerCase().equals("atendido") && (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))) {
             date = aux.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_CANCELACION", "" + date1);
             }
            //-------------------------------------------------------------------------------------
            incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
            incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
            incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());

            arrJsonArray.add(incJsonObj);

            Critica.remove(pos);
        }

        while (!normal.isEmpty()) {
            int pos = 0;
            for (int i = 0; i < normal.size(); i++) {
                if (normal.get(pos).getFechaCreacion().compareTo(normal.get(i).getFechaCreacion()) <= 0) {
                    pos = i;
                }
            }
            InfoIncidente aux = normal.get(pos);

            JsonObject incJsonObj = new JsonObject();
            incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
            incJsonObj.addProperty("ESTADO", aux.getEstado());
            incJsonObj.addProperty("FALLA", aux.getFalla());
            incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
            incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
            incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
            incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
            incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
            incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());

            String idCliente = aux.getIdCorpCliente();
            if (idCliente != null) {
                if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                    List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                    if (!datos_cliente.isEmpty() && datos_cliente != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                            incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                            incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                            EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                        incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                    incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                    incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                }
            } else {
                incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
            }
            String idCord = aux.getIDCorpCoordinador();
            if (idCord != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                }
            } else {
                incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
            }
            if (idCord != null) {
                if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                    List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                    if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                            incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            EmpleadosHashMap.put(idCord.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("COORDINADOR", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                    incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                }

            } else {
                incJsonObj.addProperty("COORDINADOR", "* Sin información");
            }
            String idSup = aux.getIdCorpSupervisor().trim();
            if (idSup != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                }
            } else {
                incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
            }
            if (idSup != null) {
                if (!idSup.matches("[^0-9]*")) {

                    if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                EmpleadosHashMap.put(idSup.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                        incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                        incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }
            } else {
                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
            }

            // incJsonObj.addProperty("COORDINADOR", "COORDINADOR JUAN PEREZ");
            incJsonObj.addProperty("NOTAS", aux.getNotas());
            incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
            incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());
            if (aux.getProveedor() != null) {
                if (!lista.isEmpty()) {
                    for (ProveedoresDTO aux3 : lista) {
                        if (aux3.getStatus().contains("Activo")) {
                            if (aux.getProveedor().toLowerCase().trim()
                                    .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                incJsonObj.addProperty("MENU", aux3.getMenu());
                                incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                break;
                            }

                        }
                    }
                }
            } else {
                String nomCorto = null;
                incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                incJsonObj.addProperty("MENU", nomCorto);
                incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
            }
            incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
            incJsonObj.addProperty("SUCURSAL", aux.getSucursal());

            incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
            incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
            incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

            incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
            incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
            String autCleinte = aux.getAutorizacionCliente();
            if (autCleinte != null && !autCleinte.equals("")) {
                if (autCleinte.contains("Rechazado")) {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                }
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
            }
            Date date = aux.getFechaCierre().getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
            date = aux.getFechaCreacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CREACION", "" + date1);
            date = aux.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
            date = aux.getFechaProgramada().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
            incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
            incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());

            if (aux.getTipoAtencion() != null) {
                incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("TIPO_ATENCION", x);
            }
            if (aux.getOrigen() != null) {
                incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("ORIGEN", x);
            }

            //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
            String cord = null;
            if (aux.getLatitudInicio() != null) {
                incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
            } else {
                incJsonObj.addProperty("LATITUD_INICIO", cord);
            }

            if (aux.getLongitudInicio() != null) {
                incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
            } else {
                incJsonObj.addProperty("LONGITUD_INICIO", cord);
            }

            if (aux.getLatitudCierre() != null) {
                incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
            } else {
                incJsonObj.addProperty("LATITUD_FIN", cord);
            }

            if (aux.getLongitudCierre() != null) {
                incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
            } else {
                incJsonObj.addProperty("LONGITUD_FIN", cord);
            }

            //----------------------------------------------------------------------------------
            date = aux.getFechaInicioAtencion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

            int idIncidente = aux.getIdIncidencia();

            List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
            int idProvedor = 0;
            int idCuadrilla = 0;
            int idZona = 0;
            for (TicketCuadrillaDTO aux2 : lista2) {
                if (aux2.getStatus() != 0) {
                    idProvedor = aux2.getIdProveedor();
                    idCuadrilla = aux2.getIdCuadrilla();
                    idZona = aux2.getZona();
                }

            }
            if (idProvedor != 0) {
                List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                for (CuadrillaDTO aux2 : lista3) {
                    if (aux2.getZona() == idZona) {
                        if (aux2.getIdCuadrilla() == idCuadrilla) {
                            incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                            incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux2.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                            incJsonObj.addProperty("ZONA", aux2.getZona());
                        }
                    }
                }
            } else {
                String auxiliar = null;
                //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                incJsonObj.addProperty("LIDER", auxiliar);
                incJsonObj.addProperty("CORREO", auxiliar);
                incJsonObj.addProperty("SEGUNDO", auxiliar);
                incJsonObj.addProperty("ZONA", auxiliar);
            }

            if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);
                String estadoApr = aprobacion.getEstadoAprobacion();

                boolean flag1 = false, flag2 = false;
                if (estadoApr != null) {
                    String porvAprob = aprobacion.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_CMONTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                }

                Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                estadoApr = aprobacion2.getEstadoAprobacion();
                if (estadoApr != null) {
                    String porvAprob = aprobacion2.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                }

                if (flag1 == true && flag2 == true) {
                    incJsonObj.addProperty("BANDERAS", 1);
                } else {
                    incJsonObj.addProperty("BANDERAS", 0);
                }
            } else {
                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                incJsonObj.addProperty("BANDERAS", 0);
            }

            //-----------------------  Consulta Traking  --------------------
            ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

            JsonArray incidenciasJsonArray = new JsonArray();

            ArrayList<Date> RechazoCliente = new ArrayList<Date>();
            ArrayList<Date> inicioAtencion = new ArrayList<Date>();
            ArrayList<Date> cierreTecnico = new ArrayList<Date>();
            ArrayList<Date> cancelados = new ArrayList<Date>();
            ArrayList<Date> atencionGarantia = new ArrayList<Date>();
            ArrayList<Date> cierreGarantia = new ArrayList<Date>();
            ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null) {
                    for (Tracking aux2 : traking) {
                        String detalle = aux2.getDetalle();
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date2 = aux2.getFechaEnvio().getTime();
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                            RechazoCliente.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                            inicioAtencion.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                            cierreTecnico.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado") || motivoEstado.trim().toLowerCase().equals("cancelado no aplica") || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                            cancelados.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                            atencionGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                            cierreGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("recibido por atender") && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                            rechazoProveedor.add(date2);
                        }
                    }
                }
            }

            int posRC = 0, posRP = 0;

            String cadAux = null;
            if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < RechazoCliente.size(); i++) {
                    if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = RechazoCliente.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                posRC = pos2;
            } else {

                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
            }

            if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < inicioAtencion.size(); i++) {
                    if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = inicioAtencion.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION", date3);

            } else {
                incJsonObj.addProperty("FECHA_ATENCION", cadAux);

            }

            if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreTecnico.size(); i++) {
                    if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreTecnico.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                incJsonObj.addProperty("FECHA_CIERRE", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                incJsonObj.addProperty("FECHA_CIERRE", cadAux);
            }

            if (cancelados != null && !cancelados.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cancelados.size(); i++) {
                    if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cancelados.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CANCELACION", date3);
            } else {
                incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
            }
            //
            if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < atencionGarantia.size(); i++) {
                    if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = atencionGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
            }

            if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreGarantia.size(); i++) {
                    if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
            }

            if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < rechazoProveedor.size(); i++) {
                    if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = rechazoProveedor.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                posRP = pos2;
            } else {
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
            }

            //BANDERA_RECHAZO_CIERRE_PREVIO
            if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null && !RechazoCliente.isEmpty()) {
                if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }
            } else {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
            }

            //------------------------------------------------------------------
            int cal = 0;
            _Calificacion auxEval = aux.getEvalCliente();
            if (auxEval != null) {
                if (auxEval.getValue().contains("NA")) {
                    cal = 0;
                }
                if (auxEval.getValue().contains("Item1")) {
                    cal = 1;
                }
                if (auxEval.getValue().contains("Item2")) {
                    cal = 2;
                }
                if (auxEval.getValue().contains("Item3")) {
                    cal = 3;
                }
                if (auxEval.getValue().contains("Item4")) {
                    cal = 4;
                }
                if (auxEval.getValue().contains("Item5")) {
                    cal = 5;
                }
            }

            incJsonObj.addProperty("CALIFICACION", cal);
            incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

            date = aux.getFechaCierreAdmin().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

            incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

            //------------------------------ COMENTARIOS PARA PROVEEDOR --------------------------
            //if(aux.getAutorizacionProveedor().toLowerCase().contains(""))

             Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 2);
             if(aprobacion!=null) {
             incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
             incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
             date = aprobacion.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
             incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
             }

            if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                date = aprobacion.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());

                Date dateI = aux.getFechaInicioAtencion().getTime();

                if (date.compareTo(dateI) >= 0) {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }
            } else {
                String auxApr = null;
                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
            }

            //-----------------------------------------------------------------------------------
            //----------------------------FECHA CANCELACION----------------------------------------
            if( aux.getEstado().trim().toLowerCase().equals("atendido") && (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))) {
             date = aux.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_CANCELACION", "" + date1);
             }
            //-------------------------------------------------------------------------------------
            incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
            incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
            incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());
            arrJsonArray.add(incJsonObj);

            normal.remove(pos);
        }

        return arrJsonArray;
    }*/
    // http://localhost:8080/migestion/servicios/altaIncidentePost.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/altaIncidentePost", method = RequestMethod.POST)
    public @ResponseBody
    String altaIncidentePost(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                ////logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                ////logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                ////logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            int ceco = 0;
            String nomSucursal = "";
            String telSucursal = "";
            int numEmpleado = 0;
            String nombre = "";
            String puesto = "";
            String comentario = "";
            String puestoAlterno = "";
            String telUsrAlterno = "";
            String nomUsrAlterno = "";
            String servicio = "";
            String ubicacion = "";
            String incidencia = "";
            String nomAdjunto = "";
            String adjunto = "";
            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ceco = rec.getInt("ceco");
                nomSucursal = rec.getString("nomSucursal");
                telSucursal = rec.getString("telSucursal");
                numEmpleado = rec.getInt("numEmpleado");
                nombre = rec.getString("nombre");
                puesto = rec.getString("puesto");
                comentario = rec.getString("comentario");
                puestoAlterno = rec.getString("puestoAlterno");
                telUsrAlterno = rec.getString("telUsrAlterno");
                nomUsrAlterno = rec.getString("nomUsrAlterno");
                servicio = rec.getString("servicio");
                ubicacion = rec.getString("ubicacion");
                incidencia = rec.getString("incidencia");
                if (!rec.getString("nomAdjunto").contains("null")) {
                    nomAdjunto = rec.getString("nomAdjunto");
                } else {
                    nomAdjunto = null;
                }
                if (!rec.getString("adjunto").contains("null")) {
                    adjunto = rec.getString("adjunto");
                } else {
                    adjunto = null;
                }
            }

            int noSucursal = 0;
            int prefijoSucursal = 0;

            String cecoS = ceco + "";
            if (cecoS.length() == 6) {
                noSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
                prefijoSucursal = Integer.parseInt(cecoS.substring(0, 2));
            }
            if (cecoS.length() == 5) {
                noSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
            }
            CorreosLotus correosLotus = new CorreosLotus();
            //logger.info("Correo SUCURSAL: " + correosLotus.validaUsuarioLotusCorreos("331952"));
            //logger.info("Correo SUCURSAL: " + correosLotus.validaUsuarioLotusCorreos("G0100"));
            // logger.info("Correo SUCURSAL:
            // "+correosLotus.validaUsuarioLotusCorreos(cecoS));
            String correo = "";

            correo = correosLotus.validaCorreoSuc(cecoS);

            //Objeto a enviar en Remedy
            FolioRemedyDTO bean = new FolioRemedyDTO();
            bean.setCeco(ceco);
            bean.setComentario(comentario);

            bean.setCorreo(correo);
            bean.setIncidencia(incidencia);
            bean.setNombre(nombre);
            bean.setNomSucursal(nomSucursal);
            bean.setNomUsrAlterno(nomUsrAlterno);
            bean.setNoSucursal(noSucursal);
            bean.setNumEmpleado(numEmpleado);
            bean.setPuesto(puesto);
            bean.setPuestoAlterno(puestoAlterno);
            bean.setServicio(servicio);
            bean.setTelSucursal(telSucursal);
            bean.setTelUsrAlterno(telUsrAlterno);
            bean.setUbicacion(ubicacion);
            bean.setNomAdjunto(nomAdjunto);
            bean.setAdjunto(adjunto);

            try {

                ParametroBI parametro = (ParametroBI) GTNAppContextProvider.getApplicationContext().getBean("parametroBI");
                List<ParametroDTO> parametros = parametro.obtieneParametros("validaCecoM");
                String valor = parametros.get(0).getValor();

                if (valor.equals("1") && prefijoSucursal != 0) {

                    int cecoRedUnica = generaCecoRedUnica(prefijoSucursal, String.valueOf(ceco));
                    bean.setCeco(cecoRedUnica);

                }

            } catch (Exception e) {

                logger.info("AP con el parámetro validaCecoM" + e);

            }

            int idFolio = serviciosRemedyBI.levantaFolio(bean);

            JsonObject envoltorioJsonObj = new JsonObject();

            if (idFolio == 0) {
                bean.setNoSucursal(ceco);
                idFolio = serviciosRemedyBI.levantaFolio(bean);
                if (idFolio == 0) {
                    envoltorioJsonObj.add("FOLIO", null);
                } else {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("ID_FOLIO", idFolio);
                    envoltorioJsonObj.add("FOLIO", insJsonObj);
                }
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("ID_FOLIO", idFolio);
                envoltorioJsonObj.add("FOLIO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            // logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    public int generaCecoRedUnica(int prefijoSucursal, String ceco) {

        String eco = ceco.substring(2, ceco.length());

        switch (prefijoSucursal) {

            case 39:
                logger.info("39 Recibido CECO: " + ceco);
                return Integer.parseInt("52" + eco);

            case 92:
                return Integer.parseInt("48" + eco);

            case 48:
                return Integer.parseInt("48" + eco);

            case 52:
                return Integer.parseInt("52" + eco);

            case 55:
    		//Validar el canal del ceco
    		try {

                List<CecoDTO> res = cecoBI.buscaCecos((prefijoSucursal + "" + eco), "", "", "", "", "", "");

                int idCanal = 0;

                if (res.get(0).getIdCanal() == 21) {

                    return Integer.parseInt("48" + eco);

                } else {

                    return Integer.parseInt("55" + eco);
                }

            } catch (Exception e) {

                return Integer.parseInt("55" + eco);
            }

            default:
                return Integer.parseInt(ceco);

        }

    }

    // http://localhost:8080/migestion/servicios/getTrakingIncidente.json?idFolio=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTrakingIncidente", method = RequestMethod.GET)
    public @ResponseBody
    String getTrakingIncidente(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idFolio = urides.split("&")[1].split("=")[1];

            // int numSucursal=Integer.parseInt(ceco.substring(2, 6));
            JsonObject envoltorioJsonObj = new JsonObject();

            String imagen = serviciosRemedyBI.ConsultaAdjunto(Integer.parseInt(idFolio));

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
            int resp = 0;
            boolean flagAcepta = false;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("atendido")) {

                            if (aux.getAutorizacionCliente().trim().toLowerCase().equals("aceptado")) {
                                flagAcepta = true;
                            }

                            if (aux.getLatitudInicio() != null) {
                                envoltorioJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
                            } else {
                                envoltorioJsonObj.addProperty("LATITUD_INICIO", "19.3093385");
                            }

                            if (aux.getLongitudInicio() != null) {
                                envoltorioJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
                            } else {
                                envoltorioJsonObj.addProperty("LONGITUD_INICIO", "-99.1872787");
                            }

                            if (aux.getLatitudCierre() != null) {
                                envoltorioJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
                            } else {
                                envoltorioJsonObj.addProperty("LATITUD_FIN", "19.3093385");
                            }

                            if (aux.getLongitudCierre() != null) {
                                envoltorioJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
                            } else {
                                envoltorioJsonObj.addProperty("LONGITUD_FIN", "-99.1872787");
                            }

                            Date date = aux.getFechaCierre().getTime();
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String date1 = format1.format(date);
                            envoltorioJsonObj.addProperty("FECHA_CIERRE", "" + date1);

                            date = aux.getFechaProgramada().getTime();
                            date1 = format1.format(date);
                            envoltorioJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);

                            date = aux.getFechaCierre().getTime();
                            date1 = format1.format(date);
                            envoltorioJsonObj.addProperty("FECHA_CIERRE", "" + date1);

                            envoltorioJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());

                            envoltorioJsonObj.addProperty("ACEPTA_CLIENTE", flagAcepta);

                            int cal = 0;
                            _Calificacion auxEval = aux.getEvalCliente();
                            if (auxEval.getValue().contains("NA")) {
                                cal = 0;
                            }
                            if (auxEval.getValue().contains("Item1")) {
                                cal = 1;
                            }
                            if (auxEval.getValue().contains("Item2")) {
                                cal = 2;
                            }
                            if (auxEval.getValue().contains("Item3")) {
                                cal = 3;
                            }
                            if (auxEval.getValue().contains("Item4")) {
                                cal = 4;
                            }
                            if (auxEval.getValue().contains("Item5")) {
                                cal = 5;
                            }

                            envoltorioJsonObj.addProperty("CALIFICACION", cal);

                            envoltorioJsonObj.addProperty("PROVEEDOR", aux.getProveedor());
                            if (aux.getProveedor() != null) {
                                if (!lista.isEmpty()) {
                                    for (ProveedoresDTO aux3 : lista) {
                                        if (aux3.getStatus().contains("Activo")) {
                                            if (aux.getProveedor().toLowerCase().trim()
                                                    .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                                envoltorioJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                                envoltorioJsonObj.addProperty("MENU", aux3.getMenu());
                                                envoltorioJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                                envoltorioJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                                break;
                                            }

                                        }
                                    }
                                }
                            } else {
                                String nomCorto = null;
                                envoltorioJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                                envoltorioJsonObj.addProperty("MENU", nomCorto);
                                envoltorioJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                                envoltorioJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
                            }

                            resp = 1;
                        }
                    }
                }
            }
            Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(Integer.parseInt(idFolio), 4);

            if (aprobacion != null) {

                if (aprobacion.getA1_Base() == null) {
                    String arch1 = null;
                    envoltorioJsonObj.addProperty("ARCHIVO_1", arch1);

                } else {
                    // insJsonObj.addProperty("ARCHIVO_1",aprobacion.getA1_Base().toString());
                    InputStream in = aprobacion.getA1_Base().getInputStream();
                    byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                    Base64.Encoder codec = Base64.getEncoder();
                    // String encoded = Base64.encodeBase64String(byteArray);
                    String encoded = new String(codec.encode(byteArray), "UTF-8");
                    envoltorioJsonObj.addProperty("ARCHIVO_1", encoded);
                }
                if (aprobacion.getA2_Base() == null) {
                    String arch1 = null;
                    envoltorioJsonObj.addProperty("ARCHIVO_2", arch1);
                } else {
                    // insJsonObj.addProperty("ARCHIVO_2",aprobacion.getA2_Base().toString());
                    InputStream in = aprobacion.getA2_Base().getInputStream();
                    byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                    Base64.Encoder codec = Base64.getEncoder();
                    // String encoded = Base64.encodeBase64String(byteArray);
                    String encoded = new String(codec.encode(byteArray), "UTF-8");
                    envoltorioJsonObj.addProperty("ARCHIVO_2", encoded);

                }
                if (aprobacion.getEstadoAprobacion() == null) {
                    String arch1 = null;
                    envoltorioJsonObj.addProperty("ESTADO_APROBACION", arch1);
                } else {
                    envoltorioJsonObj.addProperty("ESTADO_APROBACION", aprobacion.getEstadoAprobacion());
                }

                if (aprobacion.getJustAprobador() == null) {
                    String arch1 = null;
                    envoltorioJsonObj.addProperty("JUSTIFICACION_APROBADOR", arch1);
                } else {
                    envoltorioJsonObj.addProperty("JUSTIFICACION_APROBADOR", aprobacion.getJustAprobador());
                }

                if (aprobacion.getJustSolicitante() == null) {
                    String arch1 = null;
                    envoltorioJsonObj.addProperty("JUSTIFICACION_SOLICITANTE", arch1);
                } else {
                    envoltorioJsonObj.addProperty("JUSTIFICACION_SOLICITANTE", aprobacion.getJustSolicitante());
                }

                if (aprobacion.getSolicitadoPara() == null) {
                    String arch1 = null;
                    envoltorioJsonObj.addProperty("SOLICITADO_PARA", arch1);
                } else {
                    envoltorioJsonObj.addProperty("SOLICITADO_PARA", aprobacion.getSolicitadoPara());
                }

                if (aprobacion.getSolicitadoPor() == null) {
                    String arch1 = null;
                    envoltorioJsonObj.addProperty("SOLICITADO_POR", arch1);
                } else {
                    envoltorioJsonObj.addProperty("SOLICITADO_POR", aprobacion.getSolicitadoPor());
                }
            }

            if (auxIncidente == null) {

                envoltorioJsonObj.addProperty("IMAGEN", imagen);
            } else {

                envoltorioJsonObj.addProperty("IMAGEN", imagen);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            // logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getIncidentesSupervisor.json?idSupervisor=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesSupervisor", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesSupervisor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idSupervisor = urides.split("&")[1].split("=")[1];

            int idSupervisorI = Integer.parseInt(idSupervisor);

            // int idFolio=0;
            ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.ConsultaFoliosSupervidor(idSupervisorI, "1");
            ArrayOfInfoIncidente arrIncidentesCerrdos = serviciosRemedyBI.ConsultaFoliosSupervidor(idSupervisorI, "2");

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();
            if (arrIncidentes != null) {
                InfoIncidente[] arregloInc = arrIncidentes.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()+ ", ");
                        if (aux.getEstado().contains("Recibido por atender")) {
                            RPA.add(aux);
                        }
                        if (aux.getEstado().contains("En Proceso")) {
                            EP.add(aux);
                        }
                        if (aux.getEstado().contains("En Recepción")) {
                            EPC.add(aux);
                        }
                        if (aux.getEstado().contains("Atendido")) {
                            Atendidos.add(aux);
                        }
                        if (aux.getEstado().contains("Pendiente")) {
                            PPA.add(aux);
                        }
                    }
                }
            }

            if (arrIncidentesCerrdos != null) {
                InfoIncidente[] arregloInc = arrIncidentesCerrdos.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()+ ", ");
                        if (aux.getEstado().contains("Recibido por atender")) {
                            RPA.add(aux);
                        }
                        if (aux.getEstado().contains("En Proceso")) {
                            EP.add(aux);
                        }
                        if (aux.getEstado().contains("En Recepción")) {
                            EPC.add(aux);
                        }
                        if (aux.getEstado().contains("Atendido")) {
                            Atendidos.add(aux);
                        }
                        if (aux.getEstado().contains("Pendiente")) {
                            PPA.add(aux);
                        }
                    }
                }
            }

            // -------------------------------------------------------------------------------
            JsonArray AtendidosJsonArray = new JsonArray();
            if (!Atendidos.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : Atendidos) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }

                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------

                AtendidosJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPCJsonArray = new JsonArray();
            if (!EPC.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EPC) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }

                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------

                EPCJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPJsonArray = new JsonArray();
            if (!EP.isEmpty() && RPA != null) {

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EP) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                EPJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray RPAJsonArray = new JsonArray();
            if (!RPA.isEmpty() && RPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : RPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                RPAJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray PPAJsonArray = new JsonArray();
            if (!PPA.isEmpty() && PPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : PPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                PPAJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);

            }

            if (arrIncidentes == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", RPAJsonArray);
                envoltorioJsonObj.add("EN_PROCESO", EPJsonArray);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", EPCJsonArray);
                envoltorioJsonObj.add("ATENDIDOS", AtendidosJsonArray);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", PPAJsonArray);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getIncidentesSupervisorXML.json?idSupervisor=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesSupervisorXML", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesSupervisorXML(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String estatus = "-";

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        try {

            URL url = new URL("http://10.54.21.38/Arsys/WCFMantenimiento/Service.svc" + "?WSDL");
            rc = (HttpURLConnection) url.openConnection();

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:elek=\"http://Elektra.com\">\n"
                    + "   <soapenv:Header/>\n"
                    + "   <soapenv:Body>\n"
                    + "      <tem:ConsultarIncidentes>\n"
                    + "         <!--Optional:-->\n"
                    + "         <tem:authentication>\n"
                    + "            <elek:Password>demoWsM</elek:Password>\n"
                    + "            <elek:UserName>demoWsM</elek:UserName>\n"
                    + "         </tem:authentication>\n"
                    + "         <!--Optional:-->\n"
                    + "         <tem:filtro>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:CoordinadorAsignado>0</elek:CoordinadorAsignado>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:FechaCierre></elek:FechaCierre>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:FechaCreacion></elek:FechaCreacion>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:FechaModificacion></elek:FechaModificacion>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:IdIncidente>9861302</elek:IdIncidente>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:NoSucursal>0</elek:NoSucursal>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:NoTicketProveedor>0</elek:NoTicketProveedor>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:NombreTecnico></elek:NombreTecnico>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:OpcionEstado>0</elek:OpcionEstado>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:Proveedor></elek:Proveedor>\n"
                    + "            <!--Optional:-->\n"
                    + "            <elek:SupervisorAsignado>0</elek:SupervisorAsignado>\n"
                    + "         </tem:filtro>\n"
                    + "      </tem:ConsultarIncidentes>\n"
                    + "   </soapenv:Body>\n"
                    + "</soapenv:Envelope>";

            //logger.info("PETICION SOA: " + xml);
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.setRequestProperty("SOAPAction", "http://tempuri.org/IService/ConsultarIncidentes");
            rc.connect();

            try {
                wr = new OutputStreamWriter(rc.getOutputStream());
                wr.write(xml, 0, xml.length());
                wr.flush();
            } catch (Exception e) {
                wr.close();
                wr = null;
            }

            try {
                rd = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            } catch (Exception e) {
                logger.info("Servicio " + e.toString());
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            estatus = sb.toString();

        } catch (Exception e) {
            logger.info("Servicio " + e.toString());
            e.printStackTrace();
            estatus = e.getMessage();
        }

        try {
            if (wr != null) {
                wr.close();
            }
        } catch (Exception e) {
            logger.info("Servicio " + e.toString());
        }

        try {
            if (rd != null) {
                rd.close();
            }
        } catch (Exception e) {
            logger.info("Servicio " + e.toString());
        }

        try {
            if (rc != null) {
                rc.disconnect();
            }
        } catch (Exception e) {
            logger.info("Servicio " + e.toString());
        }

        String res = null;

        if (res != null) {
            return res.toString();
        } else {
            return "" + estatus;
        }

        /*
         try {
         UtilCryptoGS cifra = new UtilCryptoGS();
         StrCipher cifraIOS = new StrCipher();
         String uri = request.getQueryString();
         String uriAp = request.getParameter("token");
         int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
         uri = uri.split("&")[0];
         String urides = "";

         if (idLlave == 7) {
         urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
         } else if (idLlave == 666) {
         urides = (cifra.decryptParams(uri));
         }
         String idSupervisor = urides.split("&")[1].split("=")[1];

         int idSupervisorI = Integer.parseInt(idSupervisor);

         // int idFolio=0;
         ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.ConsultaFoliosSupervidor(idSupervisorI,"3");
         ArrayOfInfoIncidente arrIncidentesCerrdos = serviciosRemedyBI.ConsultaFoliosSupervidor(idSupervisorI,"4");

         JsonObject envoltorioJsonObj = new JsonObject();

         // ----------------------Agrupar folios por tipo de
         // estado-----------------------
         ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();
         if (arrIncidentes != null) {
         InfoIncidente[] arregloInc = arrIncidentes.getInfoIncidente();
         if (arregloInc != null) {
         for (InfoIncidente aux : arregloInc) {
         logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()
         + ", ");
         if (aux.getEstado().contains("Recibido por atender")) {
         RPA.add(aux);
         }
         if (aux.getEstado().contains("En Proceso")) {
         EP.add(aux);
         }
         if (aux.getEstado().contains("En Recepción")) {
         EPC.add(aux);
         }
         if (aux.getEstado().contains("Atendido")) {
         Atendidos.add(aux);
         }
         if (aux.getEstado().contains("Pendiente")) {
         PPA.add(aux);
         }
         }
         }
         }

         if (arrIncidentesCerrdos != null) {
         InfoIncidente[] arregloInc = arrIncidentesCerrdos.getInfoIncidente();
         if (arregloInc != null) {
         for (InfoIncidente aux : arregloInc) {
         logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()
         + ", ");
         if (aux.getEstado().contains("Recibido por atender")) {
         RPA.add(aux);
         }
         if (aux.getEstado().contains("En Proceso")) {
         EP.add(aux);
         }
         if (aux.getEstado().contains("En Recepción")) {
         EPC.add(aux);
         }
         if (aux.getEstado().contains("Atendido")) {
         Atendidos.add(aux);
         }
         if (aux.getEstado().contains("Pendiente")) {
         PPA.add(aux);
         }
         }
         }
         }


         // -------------------------------------------------------------------------------

         JsonArray AtendidosJsonArray = new JsonArray();
         if (!Atendidos.isEmpty() && RPA != null) {
         ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

         for (InfoIncidente aux : Atendidos) {
         if (aux.getPrioridad().getValue().contains("Normal")) {
         normal.add(aux);
         }
         if (aux.getPrioridad().getValue().contains("Critica")) {
         Critica.add(aux);
         }
         }

         //--------------Ejecuta hilos aprobaciones--------------------
         ArrayList<AprobacionesRemedyHilos> aprHilos=new ArrayList<AprobacionesRemedyHilos>();

         if(Critica!=null && !Critica.isEmpty()) {
         for(InfoIncidente aux:Critica) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         if(normal!=null && !normal.isEmpty()) {
         for(InfoIncidente aux:normal) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         for(int i=0;i<aprHilos.size();i++) {
         aprHilos.get(i).run();
         }


         boolean terminaron = false;

         long startTime1 = System.currentTimeMillis();
         logger.info("Entro a verificar hilos");
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         while (!terminaron) {
         for (int i = 0; i < aprHilos.size(); i++) {
         if (aprHilos.get(i).getRespuesta() != null) {
         terminaron = true;
         } else {
         terminaron = false;
         break;
         }
         if(!aprHilos.get(i).isAlive()){
         terminaron = true;
         }
         else {
         terminaron = false;
         break;
         }
         }
         }

         HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
         for(int i=0;i<aprHilos.size();i++) {
         AprobacionesHashMap.put(""+aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
         }

         long endTime1 = System.currentTimeMillis() - startTime1;
         logger.info("TIEMPO RESP en Hilos: " + endTime1);
         //-------------------------------------------------------------

         AtendidosJsonArray = arrOrdenadoSupervisor(Critica, normal,AprobacionesHashMap);
         }

         JsonArray EPCJsonArray = new JsonArray();
         if (!EPC.isEmpty() && RPA != null) {
         ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

         for (InfoIncidente aux : EPC) {
         if (aux.getPrioridad().getValue().contains("Normal")) {
         normal.add(aux);
         }
         if (aux.getPrioridad().getValue().contains("Critica")) {
         Critica.add(aux);
         }
         }

         //--------------Ejecuta hilos aprobaciones--------------------
         ArrayList<AprobacionesRemedyHilos> aprHilos=new ArrayList<AprobacionesRemedyHilos>();

         if(Critica!=null && !Critica.isEmpty()) {
         for(InfoIncidente aux:Critica) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         if(normal!=null && !normal.isEmpty()) {
         for(InfoIncidente aux:normal) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         for(int i=0;i<aprHilos.size();i++) {
         aprHilos.get(i).run();
         }


         boolean terminaron = false;

         long startTime1 = System.currentTimeMillis();
         logger.info("Entro a verificar hilos");
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         while (!terminaron) {
         for (int i = 0; i < aprHilos.size(); i++) {
         if (aprHilos.get(i).getRespuesta() != null) {
         terminaron = true;
         } else {
         terminaron = false;
         break;
         }
         if(!aprHilos.get(i).isAlive()){
         terminaron = true;
         }
         else {
         terminaron = false;
         break;
         }
         }
         }

         HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
         for(int i=0;i<aprHilos.size();i++) {
         AprobacionesHashMap.put(""+aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
         }

         long endTime1 = System.currentTimeMillis() - startTime1;
         logger.info("TIEMPO RESP en Hilos: " + endTime1);
         //-------------------------------------------------------------

         EPCJsonArray = arrOrdenadoSupervisor(Critica, normal,AprobacionesHashMap);
         }

         JsonArray EPJsonArray = new JsonArray();
         if (!EP.isEmpty() && RPA != null) {

         ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

         for (InfoIncidente aux : EP) {
         if (aux.getPrioridad().getValue().contains("Normal")) {
         normal.add(aux);
         }
         if (aux.getPrioridad().getValue().contains("Critica")) {
         Critica.add(aux);
         }
         }
         //--------------Ejecuta hilos aprobaciones--------------------
         ArrayList<AprobacionesRemedyHilos> aprHilos=new ArrayList<AprobacionesRemedyHilos>();

         if(Critica!=null && !Critica.isEmpty()) {
         for(InfoIncidente aux:Critica) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         if(normal!=null && !normal.isEmpty()) {
         for(InfoIncidente aux:normal) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         for(int i=0;i<aprHilos.size();i++) {
         aprHilos.get(i).run();
         }


         boolean terminaron = false;

         long startTime1 = System.currentTimeMillis();
         logger.info("Entro a verificar hilos");
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         while (!terminaron) {
         for (int i = 0; i < aprHilos.size(); i++) {
         if (aprHilos.get(i).getRespuesta() != null) {
         terminaron = true;
         } else {
         terminaron = false;
         break;
         }
         if(!aprHilos.get(i).isAlive()){
         terminaron = true;
         }
         else {
         terminaron = false;
         break;
         }
         }
         }

         HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
         for(int i=0;i<aprHilos.size();i++) {
         AprobacionesHashMap.put(""+aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
         }

         long endTime1 = System.currentTimeMillis() - startTime1;
         logger.info("TIEMPO RESP en Hilos: " + endTime1);
         //-------------------------------------------------------------
         EPJsonArray = arrOrdenadoSupervisor(Critica, normal,AprobacionesHashMap);

         }

         JsonArray RPAJsonArray = new JsonArray();
         if (!RPA.isEmpty() && RPA != null) {
         // ArrayList<InfoIncidente> ordenar=RPA;

         ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

         for (InfoIncidente aux : RPA) {
         if (aux.getPrioridad().getValue().contains("Normal")) {
         normal.add(aux);
         }
         if (aux.getPrioridad().getValue().contains("Critica")) {
         Critica.add(aux);
         }
         }
         //--------------Ejecuta hilos aprobaciones--------------------
         ArrayList<AprobacionesRemedyHilos> aprHilos=new ArrayList<AprobacionesRemedyHilos>();

         if(Critica!=null && !Critica.isEmpty()) {
         for(InfoIncidente aux:Critica) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         if(normal!=null && !normal.isEmpty()) {
         for(InfoIncidente aux:normal) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         for(int i=0;i<aprHilos.size();i++) {
         aprHilos.get(i).run();
         }


         boolean terminaron = false;

         long startTime1 = System.currentTimeMillis();
         logger.info("Entro a verificar hilos");
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         while (!terminaron) {
         for (int i = 0; i < aprHilos.size(); i++) {
         if (aprHilos.get(i).getRespuesta() != null) {
         terminaron = true;
         } else {
         terminaron = false;
         break;
         }
         if(!aprHilos.get(i).isAlive()){
         terminaron = true;
         }
         else {
         terminaron = false;
         break;
         }
         }
         }

         HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
         for(int i=0;i<aprHilos.size();i++) {
         AprobacionesHashMap.put(""+aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
         }

         long endTime1 = System.currentTimeMillis() - startTime1;
         logger.info("TIEMPO RESP en Hilos: " + endTime1);
         //-------------------------------------------------------------
         RPAJsonArray = arrOrdenadoSupervisor(Critica, normal,AprobacionesHashMap);

         }

         JsonArray PPAJsonArray = new JsonArray();
         if (!PPA.isEmpty() && PPA != null) {
         // ArrayList<InfoIncidente> ordenar=RPA;

         ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
         ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

         for (InfoIncidente aux : PPA) {
         if (aux.getPrioridad().getValue().contains("Normal")) {
         normal.add(aux);
         }
         if (aux.getPrioridad().getValue().contains("Critica")) {
         Critica.add(aux);
         }
         }
         //--------------Ejecuta hilos aprobaciones--------------------
         ArrayList<AprobacionesRemedyHilos> aprHilos=new ArrayList<AprobacionesRemedyHilos>();

         if(Critica!=null && !Critica.isEmpty()) {
         for(InfoIncidente aux:Critica) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         if(normal!=null && !normal.isEmpty()) {
         for(InfoIncidente aux:normal) {
         aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(),1,2));
         }
         }

         for(int i=0;i<aprHilos.size();i++) {
         aprHilos.get(i).run();
         }


         boolean terminaron = false;

         long startTime1 = System.currentTimeMillis();
         logger.info("Entro a verificar hilos");
         if(aprHilos==null || aprHilos.isEmpty())
         terminaron = true;
         while (!terminaron) {
         for (int i = 0; i < aprHilos.size(); i++) {
         if (aprHilos.get(i).getRespuesta() != null) {
         terminaron = true;
         } else {
         terminaron = false;
         break;
         }
         if(!aprHilos.get(i).isAlive()){
         terminaron = true;
         }
         else {
         terminaron = false;
         break;
         }
         }
         }

         HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
         for(int i=0;i<aprHilos.size();i++) {
         AprobacionesHashMap.put(""+aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
         }

         long endTime1 = System.currentTimeMillis() - startTime1;
         logger.info("TIEMPO RESP en Hilos: " + endTime1);
         //-------------------------------------------------------------
         PPAJsonArray = arrOrdenadoSupervisor(Critica, normal,AprobacionesHashMap);

         }

         if (arrIncidentes == null) {
         envoltorioJsonObj.add("FOLIOS", null);
         } else {
         envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", RPAJsonArray);
         envoltorioJsonObj.add("EN_PROCESO", EPJsonArray);
         envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", EPCJsonArray);
         envoltorioJsonObj.add("ATENDIDOS", AtendidosJsonArray);
         envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", PPAJsonArray);
         }
         //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
         res = envoltorioJsonObj.toString();

         } catch (Exception e) {
         logger.info(e);
         return "{}";
         }



         if (res != null)
         return res.toString();
         else
         return "{}";*/
    }

    public JsonArray arrOrdenadoSupervisor(ArrayList<InfoIncidente> Critica, ArrayList<InfoIncidente> normal, HashMap<String, Aprobacion> AprobacionesHashMap) {
        JsonArray arrJsonArray = new JsonArray();
        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();
        HashMap<String, String> EmpleadosRemedyHashMap = serviciosRemedyBI.consultaUsuariosRemedy();

        while (!Critica.isEmpty()) {
            int pos = 0;
            for (int i = 0; i < Critica.size(); i++) {
                if (Critica.get(pos).getFechaCreacion().compareTo(Critica.get(i).getFechaCreacion()) >= 0) {
                    pos = i;
                }
            }
            InfoIncidente aux = Critica.get(pos);

            //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
            JsonObject incJsonObj = new JsonObject();

            aux.getAutorizaCambioMonto();
            incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
            incJsonObj.addProperty("ESTADO", aux.getEstado());
            incJsonObj.addProperty("FALLA", aux.getFalla());
            incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
            incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
            incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
            incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
            incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
            incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());
            String idCliente = aux.getIdCorpCliente();
            if (idCliente != null) {
                if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                    List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                    if (!datos_cliente.isEmpty() && datos_cliente != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                            incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                            incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                            EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                        incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                    incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                    incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                }
            } else {
                incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
            }
            String idCord = aux.getIDCorpCoordinador();
            if (idCord != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                }
            } else {
                incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
            }
            if (idCord != null) {
                if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                    List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                    if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                            incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            EmpleadosHashMap.put(idCord.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("COORDINADOR", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                    incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                }

            } else {
                incJsonObj.addProperty("COORDINADOR", "* Sin información");
            }
            String idSup = aux.getIdCorpSupervisor().trim();
            if (idSup != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                }
            } else {
                incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
            }
            if (idSup != null) {
                if (!idSup.matches("[^0-9]*")) {

                    if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                EmpleadosHashMap.put(idSup.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                        incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                        incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }
            } else {
                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
            }

            incJsonObj.addProperty("NOTAS", aux.getNotas());
            incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
            incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

            if (aux.getProveedor() != null) {
                if (!lista.isEmpty()) {
                    for (ProveedoresDTO aux3 : lista) {
                        if (aux3.getStatus().contains("Activo")) {
                            if (aux.getProveedor().toLowerCase().trim()
                                    .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                incJsonObj.addProperty("MENU", aux3.getMenu());
                                incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                break;
                            }

                        }
                    }
                }
            } else {
                String nomCorto = null;
                incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                incJsonObj.addProperty("MENU", nomCorto);
                incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
            }

            incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
            incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
            incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
            incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
            incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

            incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
            incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
            Date date = aux.getFechaCierre().getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
            date = aux.getFechaCreacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CREACION", "" + date1);
            date = aux.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
            date = aux.getFechaProgramada().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
            incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
            incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());
            String autCleinte = aux.getAutorizacionCliente();
            if (autCleinte != null && !autCleinte.equals("")) {
                if (autCleinte.contains("Rechazado")) {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                }
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
            }

            if (aux.getTipoAtencion() != null) {
                incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("TIPO_ATENCION", x);
            }
            if (aux.getOrigen() != null) {
                incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("ORIGEN", x);
            }
            //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
            String cord = null;
            if (aux.getLatitudInicio() != null) {
                incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
            } else {
                incJsonObj.addProperty("LATITUD_INICIO", cord);
            }

            if (aux.getLongitudInicio() != null) {
                incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
            } else {
                incJsonObj.addProperty("LONGITUD_INICIO", cord);
            }

            if (aux.getLatitudCierre() != null) {
                incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
            } else {
                incJsonObj.addProperty("LATITUD_FIN", cord);
            }

            if (aux.getLongitudCierre() != null) {
                incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
            } else {
                incJsonObj.addProperty("LONGITUD_FIN", cord);
            }

            //----------------------------------------------------------------------------------
            date = aux.getFechaInicioAtencion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

            int idIncidente = aux.getIdIncidencia();

            List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
            int idProvedor = 0;
            int idCuadrilla = 0;
            int idZona = 0;
            for (TicketCuadrillaDTO aux2 : lista2) {
                if (aux2.getStatus() != 0) {
                    idProvedor = aux2.getIdProveedor();
                    idCuadrilla = aux2.getIdCuadrilla();
                    idZona = aux2.getZona();
                }

            }
            if (idProvedor != 0) {
                List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                for (CuadrillaDTO aux2 : lista3) {
                    if (aux2.getZona() == idZona) {
                        if (aux2.getIdCuadrilla() == idCuadrilla) {
                            incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                            incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux2.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                            incJsonObj.addProperty("ZONA", aux2.getZona());
                        }
                    }
                }
            } else {
                String auxiliar = null;
                //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                incJsonObj.addProperty("LIDER", auxiliar);
                incJsonObj.addProperty("CORREO", auxiliar);
                incJsonObj.addProperty("SEGUNDO", auxiliar);
                incJsonObj.addProperty("ZONA", auxiliar);
            }

            if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);

                String estadoApr = aprobacion.getEstadoAprobacion();
                boolean flag1 = false, flag2 = false;
                if (estadoApr != null) {
                    String porvAprob = aprobacion.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_CMONTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                }

                Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                estadoApr = aprobacion2.getEstadoAprobacion();
                if (estadoApr != null) {
                    String porvAprob = aprobacion2.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                }

                if (flag1 == true && flag2 == true) {
                    incJsonObj.addProperty("BANDERAS", 1);
                } else {
                    incJsonObj.addProperty("BANDERAS", 0);
                }
            } else {
                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                incJsonObj.addProperty("BANDERAS", 0);
            }

            //-----------------------  Consulta Traking  --------------------
            ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

            JsonArray incidenciasJsonArray = new JsonArray();

            ArrayList<Date> RechazoCliente = new ArrayList<Date>();
            ArrayList<Date> inicioAtencion = new ArrayList<Date>();
            ArrayList<Date> cierreTecnico = new ArrayList<Date>();
            ArrayList<Date> cancelados = new ArrayList<Date>();
            ArrayList<Date> atencionGarantia = new ArrayList<Date>();
            ArrayList<Date> cierreGarantia = new ArrayList<Date>();
            ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null) {
                    for (Tracking aux2 : traking) {
                        String detalle = aux2.getDetalle();
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date2 = aux2.getFechaEnvio().getTime();
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                            RechazoCliente.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                            inicioAtencion.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                            cierreTecnico.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado") || motivoEstado.trim().toLowerCase().equals("cancelado no aplica") || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                            cancelados.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                            atencionGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                            cierreGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("recibido por atender") && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                            rechazoProveedor.add(date2);
                        }
                    }
                }
            }

            int posRC = 0, posRP = 0;

            String cadAux = null;
            if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < RechazoCliente.size(); i++) {
                    if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = RechazoCliente.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                posRC = pos2;
            } else {

                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
            }

            if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < inicioAtencion.size(); i++) {
                    if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = inicioAtencion.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION", cadAux);
            }

            if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreTecnico.size(); i++) {
                    if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreTecnico.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                incJsonObj.addProperty("FECHA_CIERRE", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                incJsonObj.addProperty("FECHA_CIERRE", cadAux);
            }

            if (cancelados != null && !cancelados.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cancelados.size(); i++) {
                    if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cancelados.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CANCELACION", date3);
            } else {
                incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
            }
            //
            if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < atencionGarantia.size(); i++) {
                    if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = atencionGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
            }

            if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreGarantia.size(); i++) {
                    if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
            }

            if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < rechazoProveedor.size(); i++) {
                    if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = rechazoProveedor.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                posRP = pos2;
            } else {
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
            }

            //BANDERA_RECHAZO_CIERRE_PREVIO
            if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null && !RechazoCliente.isEmpty()) {
                if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }
            } else {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
            }

            //------------------------------------------------------------------
            int cal = 0;
            _Calificacion auxEval = aux.getEvalCliente();
            if (auxEval != null) {
                if (auxEval.getValue().contains("NA")) {
                    cal = 0;
                }
                if (auxEval.getValue().contains("Item1")) {
                    cal = 1;
                }
                if (auxEval.getValue().contains("Item2")) {
                    cal = 2;
                }
                if (auxEval.getValue().contains("Item3")) {
                    cal = 3;
                }
                if (auxEval.getValue().contains("Item4")) {
                    cal = 4;
                }
                if (auxEval.getValue().contains("Item5")) {
                    cal = 5;
                }
            }

            incJsonObj.addProperty("CALIFICACION", cal);

            incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

            date = aux.getFechaCierreAdmin().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

            incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

            //------------------------------ COMENTARIOS PARA PROVEEDOR --------------------------
            //if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
            /*Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 2);
             if(aprobacion!=null) {
             incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
             incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
             date = aprobacion.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
             incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
             }*/
            if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                date = aprobacion.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());

                Date dateI = aux.getFechaInicioAtencion().getTime();

                if (date.compareTo(dateI) >= 0) {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }
            } else {
                String auxApr = null;
                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
            }

            //-----------------------------------------------------------------------------------
            //----------------------------FECHA CANCELACION----------------------------------------
            /*if( aux.getEstado().trim().toLowerCase().equals("atendido") && (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))) {
             date = aux.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_CANCELACION", "" + date1);
             }*/
            //-------------------------------------------------------------------------------------
            incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
            incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
            incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());

            arrJsonArray.add(incJsonObj);

            Critica.remove(pos);
        }

        while (!normal.isEmpty()) {
            int pos = 0;
            for (int i = 0; i < normal.size(); i++) {
                if (normal.get(pos).getFechaCreacion().compareTo(normal.get(i).getFechaCreacion()) >= 0) {
                    pos = i;
                }
            }
            InfoIncidente aux = normal.get(pos);

            JsonObject incJsonObj = new JsonObject();
            incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
            incJsonObj.addProperty("ESTADO", aux.getEstado());
            incJsonObj.addProperty("FALLA", aux.getFalla());
            incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
            incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
            incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
            incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
            incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
            incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());

            String idCliente = aux.getIdCorpCliente();
            if (idCliente != null) {
                if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                    List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                    if (!datos_cliente.isEmpty() && datos_cliente != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                            incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                            incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                            EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                        incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                    incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                    incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                }
            } else {
                incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
            }
            String idCord = aux.getIDCorpCoordinador();
            if (idCord != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                }
            } else {
                incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
            }
            if (idCord != null) {
                if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                    List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                    if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                            incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            EmpleadosHashMap.put(idCord.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("COORDINADOR", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                    incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                }

            } else {
                incJsonObj.addProperty("COORDINADOR", "* Sin información");
            }
            String idSup = aux.getIdCorpSupervisor().trim();
            if (idSup != null) {
                if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                }
            } else {
                incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
            }
            if (idSup != null) {
                if (!idSup.matches("[^0-9]*")) {

                    if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                EmpleadosHashMap.put(idSup.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                        incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                        incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }
            } else {
                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
            }

            // incJsonObj.addProperty("COORDINADOR", "COORDINADOR JUAN PEREZ");
            incJsonObj.addProperty("NOTAS", aux.getNotas());
            incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
            incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());
            if (aux.getProveedor() != null) {
                if (!lista.isEmpty()) {
                    for (ProveedoresDTO aux3 : lista) {
                        if (aux3.getStatus().contains("Activo")) {
                            if (aux.getProveedor().toLowerCase().trim()
                                    .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                incJsonObj.addProperty("MENU", aux3.getMenu());
                                incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                break;
                            }

                        }
                    }
                }
            } else {
                String nomCorto = null;
                incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                incJsonObj.addProperty("MENU", nomCorto);
                incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
            }
            incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
            incJsonObj.addProperty("SUCURSAL", aux.getSucursal());

            incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
            incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
            incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

            incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
            incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
            String autCleinte = aux.getAutorizacionCliente();
            if (autCleinte != null && !autCleinte.equals("")) {
                if (autCleinte.contains("Rechazado")) {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                }
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
            }
            Date date = aux.getFechaCierre().getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
            date = aux.getFechaCreacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CREACION", "" + date1);
            date = aux.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
            date = aux.getFechaProgramada().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
            incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
            incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());

            if (aux.getTipoAtencion() != null) {
                incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("TIPO_ATENCION", x);
            }
            if (aux.getOrigen() != null) {
                incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("ORIGEN", x);
            }

            //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
            String cord = null;
            if (aux.getLatitudInicio() != null) {
                incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
            } else {
                incJsonObj.addProperty("LATITUD_INICIO", cord);
            }

            if (aux.getLongitudInicio() != null) {
                incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
            } else {
                incJsonObj.addProperty("LONGITUD_INICIO", cord);
            }

            if (aux.getLatitudCierre() != null) {
                incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
            } else {
                incJsonObj.addProperty("LATITUD_FIN", cord);
            }

            if (aux.getLongitudCierre() != null) {
                incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
            } else {
                incJsonObj.addProperty("LONGITUD_FIN", cord);
            }

            //----------------------------------------------------------------------------------
            date = aux.getFechaInicioAtencion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

            int idIncidente = aux.getIdIncidencia();

            List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
            int idProvedor = 0;
            int idCuadrilla = 0;
            int idZona = 0;
            for (TicketCuadrillaDTO aux2 : lista2) {
                if (aux2.getStatus() != 0) {
                    idProvedor = aux2.getIdProveedor();
                    idCuadrilla = aux2.getIdCuadrilla();
                    idZona = aux2.getZona();
                }

            }
            if (idProvedor != 0) {
                List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                for (CuadrillaDTO aux2 : lista3) {
                    if (aux2.getZona() == idZona) {
                        if (aux2.getIdCuadrilla() == idCuadrilla) {
                            incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                            incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux2.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                            incJsonObj.addProperty("ZONA", aux2.getZona());
                        }
                    }
                }
            } else {
                String auxiliar = null;
                //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                incJsonObj.addProperty("LIDER", auxiliar);
                incJsonObj.addProperty("CORREO", auxiliar);
                incJsonObj.addProperty("SEGUNDO", auxiliar);
                incJsonObj.addProperty("ZONA", auxiliar);
            }

            if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);
                String estadoApr = aprobacion.getEstadoAprobacion();

                boolean flag1 = false, flag2 = false;
                if (estadoApr != null) {
                    String porvAprob = aprobacion.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_CMONTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                }

                Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                estadoApr = aprobacion2.getEstadoAprobacion();
                if (estadoApr != null) {
                    String porvAprob = aprobacion2.getSolicitadoPor();
                    if (porvAprob.contains("" + idProvedor)) {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                        flag1 = true;
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                }

                if (flag1 == true && flag2 == true) {
                    incJsonObj.addProperty("BANDERAS", 1);
                } else {
                    incJsonObj.addProperty("BANDERAS", 0);
                }
            } else {
                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                incJsonObj.addProperty("BANDERAS", 0);
            }

            //-----------------------  Consulta Traking  --------------------
            ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

            JsonArray incidenciasJsonArray = new JsonArray();

            ArrayList<Date> RechazoCliente = new ArrayList<Date>();
            ArrayList<Date> inicioAtencion = new ArrayList<Date>();
            ArrayList<Date> cierreTecnico = new ArrayList<Date>();
            ArrayList<Date> cancelados = new ArrayList<Date>();
            ArrayList<Date> atencionGarantia = new ArrayList<Date>();
            ArrayList<Date> cierreGarantia = new ArrayList<Date>();
            ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null) {
                    for (Tracking aux2 : traking) {
                        String detalle = aux2.getDetalle();
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date2 = aux2.getFechaEnvio().getTime();
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                            RechazoCliente.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                            inicioAtencion.add(date2);
                        }
                        if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                            cierreTecnico.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado") || motivoEstado.trim().toLowerCase().equals("cancelado no aplica") || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                            cancelados.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                            atencionGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("atendido") && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                            cierreGarantia.add(date2);
                        }

                        if (estado.toLowerCase().trim().equals("recibido por atender") && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                            rechazoProveedor.add(date2);
                        }
                    }
                }
            }

            int posRC = 0, posRP = 0;

            String cadAux = null;
            if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < RechazoCliente.size(); i++) {
                    if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = RechazoCliente.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                posRC = pos2;
            } else {

                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
            }

            if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < inicioAtencion.size(); i++) {
                    if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = inicioAtencion.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION", date3);

            } else {
                incJsonObj.addProperty("FECHA_ATENCION", cadAux);

            }

            if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreTecnico.size(); i++) {
                    if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreTecnico.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                incJsonObj.addProperty("FECHA_CIERRE", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                incJsonObj.addProperty("FECHA_CIERRE", cadAux);
            }

            if (cancelados != null && !cancelados.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cancelados.size(); i++) {
                    if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cancelados.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CANCELACION", date3);
            } else {
                incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
            }
            //
            if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < atencionGarantia.size(); i++) {
                    if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = atencionGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
            }

            if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < cierreGarantia.size(); i++) {
                    if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = cierreGarantia.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
            } else {
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
            }

            if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                int pos2 = 0;
                for (int i = 0; i < rechazoProveedor.size(); i++) {
                    if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                        pos2 = i;
                    }
                }
                Date date2 = rechazoProveedor.get(pos2);
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date3 = format2.format(date2);
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                posRP = pos2;
            } else {
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
            }

            //BANDERA_RECHAZO_CIERRE_PREVIO
            if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null && !RechazoCliente.isEmpty()) {
                if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }
            } else {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
            }

            //------------------------------------------------------------------
            int cal = 0;
            _Calificacion auxEval = aux.getEvalCliente();
            if (auxEval != null) {
                if (auxEval.getValue().contains("NA")) {
                    cal = 0;
                }
                if (auxEval.getValue().contains("Item1")) {
                    cal = 1;
                }
                if (auxEval.getValue().contains("Item2")) {
                    cal = 2;
                }
                if (auxEval.getValue().contains("Item3")) {
                    cal = 3;
                }
                if (auxEval.getValue().contains("Item4")) {
                    cal = 4;
                }
                if (auxEval.getValue().contains("Item5")) {
                    cal = 5;
                }
            }

            incJsonObj.addProperty("CALIFICACION", cal);
            incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

            date = aux.getFechaCierreAdmin().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

            incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

            //------------------------------ COMENTARIOS PARA PROVEEDOR --------------------------
            //if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
            /*
             Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 2);
             if(aprobacion!=null) {
             incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
             incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
             date = aprobacion.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
             incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
             }
             */
            if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                date = aprobacion.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());

                Date dateI = aux.getFechaInicioAtencion().getTime();

                if (date.compareTo(dateI) >= 0) {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }
            } else {
                String auxApr = null;
                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
            }

            //-----------------------------------------------------------------------------------
            //----------------------------FECHA CANCELACION----------------------------------------
            /*if( aux.getEstado().trim().toLowerCase().equals("atendido") && (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))) {
             date = aux.getFechaModificacion().getTime();
             date1 = format1.format(date);
             incJsonObj.addProperty("FECHA_CANCELACION", "" + date1);
             }*/
            //-------------------------------------------------------------------------------------
            incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
            incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
            incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());
            arrJsonArray.add(incJsonObj);

            normal.remove(pos);
        }

        return arrJsonArray;
    }

    // http://localhost:8080/migestion/servicios/getProveedores.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getProveedores", method = RequestMethod.GET)
    public @ResponseBody
    String getProveedores(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int noSucursal = 0;
            int idCeco = noSucursal;

            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray provJsonArray = new JsonArray();

            if (!lista.isEmpty()) {
                for (ProveedoresDTO aux : lista) {
                    if (aux.getStatus().contains("Activo")) {
                        JsonObject provJsonObj = new JsonObject();
                        provJsonObj.addProperty("ID_PROVEEDOR", aux.getIdProveedor());
                        provJsonObj.addProperty("MENU", aux.getMenu());
                        provJsonObj.addProperty("NOMBRE_CORTO", aux.getNombreCorto());
                        provJsonObj.addProperty("RAZON_SOCIAL", aux.getRazonSocial());
                        provJsonObj.addProperty("STATUS", aux.getStatus());
                        provJsonArray.add(provJsonObj);

                    }
                }
            }
            if (lista.isEmpty()) {
                envoltorioJsonObj.add("PROVEEDORES", null);
            } else {
                envoltorioJsonObj.add("PROVEEDORES", provJsonArray);
            }

            logger.info("JSON PROVEEDORES: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();
        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }
        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getProveedoresXML.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getProveedoresXML", method = RequestMethod.GET)
    public @ResponseBody
    String getProveedoresXML(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String estatus = "-";

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        try {

            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 8080));

            URL url = new URL("http://10.54.21.38/Arsys/WCFMantenimiento/Service.svc" + "?WSDL");
            rc = (HttpURLConnection) url.openConnection(proxy);

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:elek=\"http://Elektra.com\">\n"
                    + "   <soapenv:Header/>\n"
                    + "   <soapenv:Body>\n"
                    + "      <tem:ConsultaProveedores>\n"
                    + "         <!--Optional:-->\n"
                    + "         <tem:authentication>\n"
                    + "            <elek:Password>demoWsM</elek:Password>\n"
                    + "            <elek:UserName>demoWsM</elek:UserName>\n"
                    + "         </tem:authentication>\n"
                    + "         <!--Optional:-->\n"
                    + "         <tem:fecha>2019-03-01</tem:fecha>\n"
                    + "      </tem:ConsultaProveedores>\n"
                    + "   </soapenv:Body>\n"
                    + "</soapenv:Envelope>\n"
                    + "";

            //logger.info("PETICION SOA: " + xml);
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.setRequestProperty("SOAPAction", "http://tempuri.org/IService/ConsultaProveedores");
            rc.connect();

            try {
                wr = new OutputStreamWriter(rc.getOutputStream());
                wr.write(xml, 0, xml.length());
                wr.flush();
            } catch (Exception e) {
                wr.close();
                wr = null;
            }

            try {
                rd = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            } catch (Exception e) {
                logger.info(" " + e.toString());
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            estatus = sb.toString();

        } catch (Exception e) {
            logger.info(" " + e.toString());
            e.printStackTrace();
            estatus = e.getMessage();
        }

        try {
            if (wr != null) {
                wr.close();
            }
        } catch (Exception e) {
            logger.info(" " + e.toString());
        }

        try {
            if (rd != null) {
                rd.close();
            }
        } catch (Exception e) {
            logger.info(" " + e.toString());
        }

        try {
            if (rc != null) {
                rc.disconnect();
            }
        } catch (Exception e) {
            logger.info(" " + e.toString());
        }

        String res = null;

        if (res != null) {
            return res.toString();
        } else {
            return "" + estatus;
        }

    }

    // http://localhost:8080/migestion/servicios/solicitarAutorizacion.json?
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/solicitarAutorizacion", method = RequestMethod.GET)
    public @ResponseBody
    String solicitarAutorizacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String justificacion = urides.split("&")[2].split("=")[1];
            String monto = urides.split("&")[3].split("=")[1];
            String solicitante = urides.split("&")[4].split("=")[1];
            String aprobador = urides.split("&")[5].split("=")[1];

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("por asignar")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }

            boolean actIncidente = false;
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyBI.AutorizaCoordinador(idIncidente, justificacion, monto, solicitante,
                        aprobador);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("AUTORICACION_COORDINADOR", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("AUTORICACION_COORDINADOR", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/cerrarSinOT.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/cerrarSinOT", method = RequestMethod.GET)
    public @ResponseBody
    String cerrarSinOT(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String resolucion = urides.split("&")[2].split("=")[1];
            String montivoEstado = urides.split("&")[3].split("=")[1];

            //logger.info("params " + idIncidente + ", " + resolucion + ", " + montivoEstado);
            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("por asignar")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }

            boolean actIncidente = false;
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyBI.cierreSinOT(idIncidente, resolucion, montivoEstado);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/asignaProveedor.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/asignaProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String asignaProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if (urides.length() > 0) {
                int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
                String proveedor = urides.split("&")[2].split("=")[1];
                String fecha = urides.split("&")[3].split("=")[1];
                String monto = urides.split("&")[4].split("=")[1];
                String origen = urides.split("&")[5].split("=")[1];
                String solicitante = urides.split("&")[6].split("=")[1];
                String aprobador = urides.split("&")[7].split("=")[1];
                String mensaje = urides.split("&")[8].split("=")[1];
                String tipoAten = urides.split("&")[9].split("=")[1];

                ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
                int resp = 0;
                if (auxIncidente != null) {
                    InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                    if (arregloInc != null) {
                        for (InfoIncidente aux : arregloInc) {
                            if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                    && (aux.getMotivoEstado().trim().toLowerCase().equals("por asignar") || aux
                                    .getMotivoEstado().trim().toLowerCase().equals("autorizado por asignar")
                                    || aux.getMotivoEstado().trim().toLowerCase().equals("rechazado proveedor"))) {
                                resp = 0;
                            } else {
                                resp = 1;
                            }
                        }
                    }
                }

                boolean actIncidente = false;
                if (resp == 1) {
                    resp = 2;
                } else {
                    actIncidente = serviciosRemedyBI.asignarProveedor(idIncidente, proveedor, fecha, monto, origen, solicitante, aprobador, mensaje, tipoAten);
                }

                if (resp != 2) {
                    if (actIncidente) {
                        resp = 1;
                    } else {
                        resp = 0;
                    }
                }

                JsonObject envoltorioJsonObj = new JsonObject();

                if (!actIncidente) {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("RES", resp);
                    envoltorioJsonObj.add("MODIFICADO", insJsonObj);
                } else {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("RES", resp);
                    envoltorioJsonObj.add("MODIFICADO", insJsonObj);
                }
                ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();
            } else {
                return "{}";
            }
        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/asignaGarantia.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/asignaGarantia", method = RequestMethod.GET)
    public @ResponseBody
    String asignaGarantia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String comentario = urides.split("&")[2].split("=")[1];

            //logger.info("ENTRO A GARANTIA incidente:" + idIncidente + " comentario=" + comentario);
            // boolean actIncidente = true;
            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("por asignar")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }

            boolean actIncidente = false;
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyBI.posibleGarantia(idIncidente, comentario);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getIncidentesCoordinador.json?idCoordinador=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesCoordinador", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesCoordinador(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idCoordinador = urides.split("&")[1].split("=")[1];

            int idCoordinadorI = Integer.parseInt(idCoordinador);

            // int idFolio=0;
            ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.ConsultaFoliosCoordinador(idCoordinadorI, "1");
            ArrayOfInfoIncidente arrIncidentesCerrados = serviciosRemedyBI.ConsultaFoliosCoordinador(idCoordinadorI, "2");

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();

            ArrayList<String> supervisores = new ArrayList<String>();

            if (arrIncidentes != null) {
                InfoIncidente[] arregloInc = arrIncidentes.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
                        if (aux.getEstado().contains("Recibido por atender")) {
                            RPA.add(aux);
                        }
                        if (aux.getEstado().contains("En Proceso")) {
                            EP.add(aux);
                        }
                        if (aux.getEstado().contains("En Recepción")) {
                            EPC.add(aux);
                        }
                        if (aux.getEstado().contains("Atendido")) {
                            Atendidos.add(aux);
                        }
                        if (aux.getEstado().contains("Pendiente")) {
                            PPA.add(aux);
                        }
                        if (supervisores.isEmpty()) {
                            supervisores.add(aux.getIdCorpSupervisor().trim());
                        } else {
                            boolean flagSup = false;
                            for (String sup : supervisores) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    if (sup.equals(aux.getIdCorpSupervisor().trim())) {
                                        flagSup = true;
                                        break;
                                    }
                                }
                            }
                            if (flagSup == false) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    supervisores.add(aux.getIdCorpSupervisor().trim());
                                }
                            }
                        }

                    }
                }
            }

            if (arrIncidentesCerrados != null) {
                InfoIncidente[] arregloInc = arrIncidentesCerrados.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
                        if (aux.getEstado().contains("Recibido por atender")) {
                            RPA.add(aux);
                        }
                        if (aux.getEstado().contains("En Proceso")) {
                            EP.add(aux);
                        }
                        if (aux.getEstado().contains("En Recepción")) {
                            EPC.add(aux);
                        }
                        if (aux.getEstado().contains("Atendido")) {
                            Atendidos.add(aux);
                        }
                        if (aux.getEstado().contains("Pendiente")) {
                            PPA.add(aux);
                        }
                        if (supervisores.isEmpty()) {
                            supervisores.add(aux.getIdCorpSupervisor().trim());
                        } else {
                            boolean flagSup = false;
                            for (String sup : supervisores) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    if (sup.equals(aux.getIdCorpSupervisor().trim())) {
                                        flagSup = true;
                                        break;
                                    }
                                }
                            }
                            if (flagSup == false) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    supervisores.add(aux.getIdCorpSupervisor().trim());
                                }
                            }
                        }

                    }
                }
            }

            // -------------------------------------------------------------------------------
            JsonArray supervisoresJsonArray = new JsonArray();
            if (!supervisores.isEmpty() && supervisores != null) {
                for (String sup : supervisores) {
                    JsonObject incJsonObj = new JsonObject();
                    if (!sup.matches("[^0-9]*")) {

                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(sup));
                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getNombre());
                                supervisoresJsonArray.add(incJsonObj);
                            }
                        }
                    } else {
                        System.out.println("No es un número");
                        incJsonObj.addProperty("SUPERVISOR", "JUAN PEREZ");
                        supervisoresJsonArray.add(incJsonObj);
                    }
                }
            }

            // --------------------------------------------------------------------------------
            JsonArray AtendidosJsonArray = new JsonArray();
            if (!Atendidos.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : Atendidos) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                AtendidosJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPCJsonArray = new JsonArray();
            if (!EPC.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EPC) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                EPCJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPJsonArray = new JsonArray();
            if (!EP.isEmpty() && RPA != null) {

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EP) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                EPJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray RPAJsonArray = new JsonArray();
            if (!RPA.isEmpty() && RPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : RPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                RPAJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray PPAJsonArray = new JsonArray();
            if (!PPA.isEmpty() && PPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : PPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                //logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                PPAJsonArray = arrOrdenadoSupervisor(Critica, normal, AprobacionesHashMap);

            }

            if (arrIncidentes == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", RPAJsonArray);
                envoltorioJsonObj.add("EN_PROCESO", EPJsonArray);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", EPCJsonArray);
                envoltorioJsonObj.add("ATENDIDOS", AtendidosJsonArray);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", PPAJsonArray);
                envoltorioJsonObj.add("SUPERVISORES", supervisoresJsonArray);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/autorizaProveedor.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/autorizaProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String autorizaProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String justificacion = urides.split("&")[2].split("=")[1];
            String opcion = urides.split("&")[3].split("=")[1];
            boolean autoriza = false;
            if (opcion.contains("1")) {
                autoriza = true;
            }

            boolean actIncidente = serviciosRemedyBI.autorizaProveedor(idIncidente, justificacion, autoriza, "");
            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", actIncidente);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", actIncidente);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            // logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getAutorizaCMonto.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAutorizaCMonto", method = RequestMethod.POST)
    public @ResponseBody
    String getAutorizaCMonto(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                ////logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                ////logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                ////logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";
            String monto = "";
            int numEmpleado = 0;
            String nomAdjunto = "";
            String adjunto = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ////logger.info("getAutorizaMonto: " + rec);
                numEmpleado = rec.getInt("numEmpleado");
                autoriza = rec.getBoolean("autoriza");
                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                monto = rec.getString("monto");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
            }

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));

            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        String estado = aux.getEstado().trim().toLowerCase();
                        String motEstado = aux.getMotivoEstado().trim().toLowerCase();
                        if (estado.equals("pendiente por atender") && motEstado.equals("cotización por aprobar")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else if (idFolio != null && !idFolio.equals("")) {
                actIncidente = serviciosRemedyBI.autorizaCambioMonto(Integer.parseInt(idFolio), autoriza, justificacion,
                        monto, nomAdjunto, adjunto);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            // boolean actIncidente=serviciosRemedyBI.cierreSinOT(idIncidente, resolucion,
            // montivoEstado);
            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getAutorizaCoordinador.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAutorizaCoordinador", method = RequestMethod.POST)
    public @ResponseBody
    String getAutorizaCoordinador(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                ////logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                ////logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                ////logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";
            String monto = "";
            int numEmpleado = 0;
            String nomAdjunto = "";
            String adjunto = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ////logger.info("getAutorizaMonto: " + rec.toString());
                numEmpleado = rec.getInt("numEmpleado");
                autoriza = rec.getBoolean("autoriza");
                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
                monto = rec.getString("monto");
            }

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("pendiente por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("por autorizar monto")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else if (idFolio != null && !idFolio.equals("")) {
                actIncidente = serviciosRemedyBI.autorizaCoordinador(Integer.parseInt(idFolio), autoriza, justificacion,
                        nomAdjunto, adjunto, monto);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getAutorizaCTiempo.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAutorizaCTiempo", method = RequestMethod.POST)
    public @ResponseBody
    String getAutorizaCTiempo(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                ////logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                ////logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                ////logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";
            String monto = "";
            int numEmpleado = 0;
            String nomAdjunto = "";
            String adjunto = "";
            int dias = 0;
            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ////logger.info("getAutorizaMonto: " + rec.toString());
                numEmpleado = rec.getInt("numEmpleado");
                autoriza = rec.getBoolean("autoriza");
                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
                dias = rec.getInt("dias");
            }

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("pendiente por atender")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("duración por aprobar")
                                || aux.getMotivoEstado() == null
                                || aux.getMotivoEstado().trim().toLowerCase().equals(""))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else if (idFolio != null && !idFolio.equals("")) {
                actIncidente = serviciosRemedyBI.autorizaAplazamiento(Integer.parseInt(idFolio), autoriza,
                        justificacion, nomAdjunto, adjunto, dias);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/reasignaProveedor.json?idUsuario=<?>&idIncidente=<?>&justificacion=<?>&proveedor=<?>&fecha=<?>&monto=<?>&tipoAtencion=<?>&solicitante=<?>&aprobador=<?>
    // @SuppressWarnings("static-access")
    @RequestMapping(value = "/reasignaProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String reasignaProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String justificacion = urides.split("&")[2].split("=")[1];
            String proveedor = urides.split("&")[3].split("=")[1];
            String fecha = urides.split("&")[4].split("=")[1];
            String monto = urides.split("&")[5].split("=")[1];
            String origen = urides.split("&")[6].split("=")[1];
            String solicitante = urides.split("&")[7].split("=")[1];
            String aprobador = urides.split("&")[8].split("=")[1];
            String mensaje = urides.split("&")[9].split("=")[1];
            String tipoAten = urides.split("&")[10].split("=")[1];

            boolean actIncidente = false;
            boolean rechazaProveeedor = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {

                        if ((aux.getEstado().trim().toLowerCase().equals("recibido por atender") && aux.getMotivoEstado().trim().toLowerCase().equals("asignado por aceptar"))
                                || (aux.getEstado().trim().toLowerCase().equals("en proceso") && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                rechazaProveeedor = serviciosRemedyBI.autorizaProveedor(idIncidente, justificacion, false, "");

                if (rechazaProveeedor == true) {
                    actIncidente = serviciosRemedyBI.asignarProveedor(idIncidente, proveedor, fecha, monto, origen, solicitante, aprobador, mensaje, tipoAten);
                }
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getConsultarAprobaciones.json?idUsuario=<?>&idIncidente=<?>&opcion=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getConsultarAprobaciones", method = RequestMethod.GET)
    public @ResponseBody
    String getConsultarAprobaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String res1 = urides.split("&")[1].split("=")[1];
            String res2 = urides.split("&")[2].split("=")[1];
            if (!res1.matches("[^0-9]+$") && !res2.matches("[^0-9]+$")) {
                int idIncidente = Integer.parseInt(res1);
                int opcion = Integer.parseInt(res2);

                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, opcion);

                JsonObject envoltorioJsonObj = new JsonObject();

                if (aprobacion == null) {
                    envoltorioJsonObj.add("APROBACION", null);
                    if (opcion == 7) {
                        JsonObject insJsonObj = new JsonObject();
                        String arch1 = null;
                        insJsonObj.addProperty("ARCHIVO_1", arch1);
                        insJsonObj.addProperty("ARCHIVO_2", arch1);
                        insJsonObj.addProperty("ESTADO_APROBACION", arch1);
                        insJsonObj.addProperty("JUSTIFICACION_APROBADOR", arch1);
                        insJsonObj.addProperty("JUSTIFICACION_SOLICITANTE",
                                "El sistema identificó una Posible Garantía en uno de los servicios previos, el área de Control y Garantías ya se encuentra validando la información");
                        insJsonObj.addProperty("SOLICITADO_PARA", arch1);
                        insJsonObj.addProperty("SOLICITADO_POR", arch1);
                        envoltorioJsonObj.add("APROBACION", insJsonObj);
                    }

                } else {
                    JsonObject insJsonObj = new JsonObject();
                    if (aprobacion.getA1_Base() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("ARCHIVO_1", arch1);

                    } else {
                        // insJsonObj.addProperty("ARCHIVO_1",aprobacion.getA1_Base().toString());
                        InputStream in = aprobacion.getA1_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        insJsonObj.addProperty("ARCHIVO_1", encoded);
                    }
                    if (aprobacion.getA2_Base() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("ARCHIVO_2", arch1);
                    } else {
                        // insJsonObj.addProperty("ARCHIVO_2",aprobacion.getA2_Base().toString());
                        InputStream in = aprobacion.getA2_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        insJsonObj.addProperty("ARCHIVO_2", encoded);

                    }
                    if (aprobacion.getEstadoAprobacion() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("ESTADO_APROBACION", arch1);
                    } else {
                        insJsonObj.addProperty("ESTADO_APROBACION", aprobacion.getEstadoAprobacion());
                    }

                    if (aprobacion.getJustAprobador() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("JUSTIFICACION_APROBADOR", arch1);
                    } else {
                        insJsonObj.addProperty("JUSTIFICACION_APROBADOR", aprobacion.getJustAprobador());
                    }

                    if (aprobacion.getJustSolicitante() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("JUSTIFICACION_SOLICITANTE", arch1);
                    } else {
                        insJsonObj.addProperty("JUSTIFICACION_SOLICITANTE", aprobacion.getJustSolicitante());
                    }

                    if (aprobacion.getSolicitadoPara() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("SOLICITADO_PARA", arch1);
                    } else {
                        insJsonObj.addProperty("SOLICITADO_PARA", aprobacion.getSolicitadoPara());
                    }

                    if (aprobacion.getSolicitadoPor() == null) {
                        String arch1 = null;
                        insJsonObj.addProperty("SOLICITADO_POR", arch1);
                    } else {
                        insJsonObj.addProperty("SOLICITADO_POR", aprobacion.getSolicitadoPor());
                    }

                    Date date = aprobacion.getFechaModificacion().getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date1 = format1.format(date);
                    insJsonObj.addProperty("FECHA", "" + date1);

                    envoltorioJsonObj.add("APROBACION", insJsonObj);

                    if (opcion == 7) {
                        JsonObject insJsonObj1 = new JsonObject();
                        String arch1 = null;
                        insJsonObj1.addProperty("ARCHIVO_1", arch1);
                        insJsonObj1.addProperty("ARCHIVO_2", arch1);
                        insJsonObj1.addProperty("ESTADO_APROBACION", arch1);
                        insJsonObj1.addProperty("JUSTIFICACION_APROBADOR", arch1);
                        insJsonObj1.addProperty("JUSTIFICACION_SOLICITANTE",
                                "El sistema identificó una Posible Garantía en uno de los servicios previos, el área de Control y Garantías ya se encuentra validando la información");
                        insJsonObj1.addProperty("SOLICITADO_PARA", arch1);
                        insJsonObj1.addProperty("SOLICITADO_POR", arch1);
                        envoltorioJsonObj.add("APROBACION", insJsonObj1);
                    }
                }
                ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();

            } else {
                return "{}";
            }
            res1 = null;
            res2 = null;
        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getAceptaCliente.json?idUsuario=<?>&idIncidente=<?>&calificaion=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAceptaCliente", method = RequestMethod.GET)
    public @ResponseBody
    String getAceptaCliente(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            int cal = Integer.parseInt(urides.split("&")[2].split("=")[1]);

            boolean autoriza = false;

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en recepción")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("cierre técnico")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyBI.aceptaCliente(idIncidente, cal);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            // logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/postRechazaCliente.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postRechazaCliente", method = RequestMethod.POST)
    public @ResponseBody
    String postRechazaCliente(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {
        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";
            int dias = 0;

            String nomAdjunto = "";
            String adjunto = "";
            String solicitante = "";
            String aprobador = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ////logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("idFolio");
                justificacion = rec.getString("justificacion");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
            }
            boolean autoriza = false;

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en recepción")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("cierre técnico")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyBI.rechazaCliente(Integer.parseInt(idFolio), justificacion, nomAdjunto, adjunto, solicitante, aprobador);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getCierreSupervisor.json?idUsuario=<?>&idIncidente<?>&correcto<?>&justificacion<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCierreSupervisor", method = RequestMethod.GET)
    public @ResponseBody
    String getCierreSupervisor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            int correcto = Integer.parseInt(urides.split("&")[2].split("=")[1]);
            String justificacion = urides.split("&")[3].split("=")[1];

            boolean autoriza = false;

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en recepción")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("rechazado por cliente")) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyBI.cierreSupervisor(idIncidente, correcto, justificacion);
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getDetalleIncidente.json?idFolio=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleIncidente", method = RequestMethod.GET)
    public @ResponseBody
    String getDetalleIncidente(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idFolio = urides.split("&")[1].split("=")[1];

            // int numSucursal=Integer.parseInt(ceco.substring(2, 6));
            String imagen = serviciosRemedyBI.ConsultaAdjunto(Integer.parseInt(idFolio));
            ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(Integer.parseInt(idFolio));

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray incidenciasJsonArray = new JsonArray();
            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null) {
                    for (Tracking aux : traking) {
                        String detalle = aux.getDetalle();
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date = aux.getFechaEnvio().getTime();
                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String date1 = format1.format(date);

                        logger.info("estado: " + estado + ", motivoEstado: " + motivoEstado + ", fecha: " + date1);
                        JsonObject detalleJsonObj = new JsonObject();
                        detalleJsonObj.addProperty("ESTADO", estado);
                        detalleJsonObj.addProperty("MOTIVO_ESTADO", motivoEstado);
                        detalleJsonObj.addProperty("FECHA", date1);
                        incidenciasJsonArray.add(detalleJsonObj);
                    }
                }
            }

            if (arrTraking == null) {
                envoltorioJsonObj.add("TRAKING", null);
                envoltorioJsonObj.addProperty("IMAGEN", imagen);
            } else {
                envoltorioJsonObj.add("TRAKING", incidenciasJsonArray);
                envoltorioJsonObj.addProperty("IMAGEN", imagen);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // --------------------------------------------------------------- Terminan
    //*****
    // Servicios Proyecto Mantenimiento
    //*****
    // ---------------------------------------------------------------------
    // ---------------------------------------------------------------
    //*****
    //Inician Servicios Proveedor Mantenimiento
    //*****
    //---------------------------------------------------------------------
    // http://localhost:8080/migestion/servicios/postSolicitaAplazamiento.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postSolicitaAplazamiento", method = RequestMethod.POST)
    public @ResponseBody
    String postSolicitaAplazamiento(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";
            int dias = 0;

            String nomAdjunto = "";
            String adjunto = "";
            String solicitante = "";
            String aprobador = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ////logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
                dias = rec.getInt("dias");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
            }

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                if (idFolio != null && !idFolio.equals("")) {
                    actIncidente = serviciosRemedyBI.solicitaAplazamiento(Integer.parseInt(idFolio), solicitante,
                            aprobador, justificacion, nomAdjunto, adjunto, dias);
                }
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getSolicitaCMonto.json?idUsuario=<?>&idIncidente=<?>&justificacion=<?>&monto=<?>&solicitante=<?>&aprobador=<?>
    // @SuppressWarnings("static-access")
    @RequestMapping(value = "/getSolicitaCMonto", method = RequestMethod.GET)
    public @ResponseBody
    String getSolicitaCMonto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String justificacion = urides.split("&")[2].split("=")[1];
            String monto = urides.split("&")[3].split("=")[1];
            String solicitante = urides.split("&")[4].split("=")[1];
            String aprobador = urides.split("&")[5].split("=")[1];

            boolean actIncidente = false;
            boolean rechazaProveeedor = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {

                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                actIncidente = serviciosRemedyBI.solicitaCambioMonto(idIncidente, solicitante, aprobador, justificacion, monto, "", "");
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/postSolicitaCMonto.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postSolicitaCMonto", method = RequestMethod.POST)
    public @ResponseBody
    String postSolicitaCMonto(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";
            String monto = "";

            String nomAdjunto = "";
            String adjunto = "";
            String solicitante = "";
            String aprobador = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                //logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
                monto = rec.getString("monto");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
            }

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                if (idFolio != null && !idFolio.equals("")) {
                    actIncidente = serviciosRemedyBI.solicitaCambioMonto(Integer.parseInt(idFolio), solicitante, aprobador, justificacion, monto, nomAdjunto, adjunto);
                }
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    public JsonArray arrOrdenadoProveedor(ArrayList<InfoIncidente> Critica, ArrayList<InfoIncidente> normal, HashMap<String, Aprobacion> AprobacionesHashMap) {
        JsonArray arrJsonArray = new JsonArray();
        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();
        HashMap<String, String> EmpleadosRemedyHashMap = serviciosRemedyBI.consultaUsuariosRemedy();

        ArrayList<String> foliosErrores = new ArrayList<String>();
        String folioActual = "";
        int posActual = 0;

        while (!Critica.isEmpty()) {

            try {

                int pos = 0;
                for (int i = 0; i < Critica.size(); i++) {
                    if (Critica.get(pos).getFechaCreacion().compareTo(Critica.get(i).getFechaCreacion()) >= 0) {
                        pos = i;
                    }
                }

                posActual = pos;
                InfoIncidente aux = Critica.get(pos);

                logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
                folioActual = aux.getIdIncidencia() + "";
                JsonObject incJsonObj = new JsonObject();

                aux.getAutorizaCambioMonto();
                incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
                incJsonObj.addProperty("ESTADO", aux.getEstado());
                incJsonObj.addProperty("FALLA", aux.getFalla());
                incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
                incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
                incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
                incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
                incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
                incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());
                String idCliente = aux.getIdCorpCliente();
                if (idCliente != null) {
                    if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                        List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI
                                .obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                        if (!datos_cliente.isEmpty() && datos_cliente != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                                incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                                incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                                EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                            incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                        incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                        incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                    }
                } else {
                    incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                    incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                }
                String idCord = aux.getIDCorpCoordinador();
                if (idCord != null) {
                    if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                        incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                    }
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                }
                if (idCord != null) {
                    if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                        List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI
                                .obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                        if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                                incJsonObj.addProperty("COORDINADOR",
                                        emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                EmpleadosHashMap.put(idCord.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("COORDINADOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                        incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("COORDINADOR", "* Sin información");
                }

                String idSup = aux.getIdCorpSupervisor().trim();
                if (idSup != null) {
                    if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                        incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                    }
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                }
                if (idSup != null) {
                    if (!idSup.matches("[^0-9]*")) {

                        if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                            List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI
                                    .obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                            if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                                for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                    incJsonObj.addProperty("SUPERVISOR",
                                            emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                    incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                    EmpleadosHashMap.put(idSup.trim(), emp_aux);
                                }
                            } else {
                                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                            }
                        } else {
                            DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                            incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                        }

                    } else {
                        incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                        incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                    }
                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }

                incJsonObj.addProperty("NOTAS", aux.getNotas());
                incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
                incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

                if (aux.getProveedor() != null) {
                    if (!lista.isEmpty()) {
                        for (ProveedoresDTO aux3 : lista) {
                            if (aux3.getStatus().contains("Activo")) {
                                if (aux.getProveedor().toLowerCase().trim()
                                        .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                    incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                    incJsonObj.addProperty("MENU", aux3.getMenu());
                                    incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                    incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                    break;
                                }

                            }
                        }
                    }
                } else {
                    String nomCorto = null;
                    incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                    incJsonObj.addProperty("MENU", nomCorto);
                    incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                    incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
                }

                incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
                incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
                incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
                incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
                incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

                incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
                incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
                Date date = aux.getFechaCierre().getTime();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
                date = aux.getFechaCreacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CREACION", "" + date1);
                date = aux.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
                date = aux.getFechaProgramada().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
                incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
                incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());
                String autCleinte = aux.getAutorizacionCliente();
                if (autCleinte != null && !autCleinte.equals("")) {
                    if (autCleinte.contains("Rechazado")) {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                    } else {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                    }
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
                }

                if (aux.getTipoAtencion() != null) {
                    incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("TIPO_ATENCION", x);
                }
                if (aux.getOrigen() != null) {
                    incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("ORIGEN", x);
                }
                // -----------------Obtiene coordenadas de inicio y fin de ticket
                // -------------------
                String cord = null;
                if (aux.getLatitudInicio() != null) {
                    incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
                } else {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                }

                if (aux.getLongitudInicio() != null) {
                    incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
                } else {
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }

                if (aux.getLatitudCierre() != null) {
                    incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
                } else {
                    incJsonObj.addProperty("LATITUD_FIN", cord);
                }

                if (aux.getLongitudCierre() != null) {
                    incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
                } else {
                    incJsonObj.addProperty("LONGITUD_FIN", cord);
                }

                // ----------------------------------------------------------------------------------
                date = aux.getFechaInicioAtencion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

                int idIncidente = aux.getIdIncidencia();

                List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
                int idProvedor = 0;
                int idCuadrilla = 0;
                int idZona = 0;
                for (TicketCuadrillaDTO aux2 : lista2) {
                    if (aux2.getStatus() != 0) {
                        idProvedor = aux2.getIdProveedor();
                        idCuadrilla = aux2.getIdCuadrilla();
                        idZona = aux2.getZona();
                    }

                }
                if (idProvedor != 0) {
                    List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                    for (CuadrillaDTO aux2 : lista3) {
                        if (aux2.getZona() == idZona) {
                            if (aux2.getIdCuadrilla() == idCuadrilla) {
                                incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                                incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                                incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                                incJsonObj.addProperty("CORREO", aux2.getCorreo());
                                incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                                incJsonObj.addProperty("ZONA", aux2.getZona());
                            }
                        }
                    }
                } else {
                    String auxiliar = null;
                    // incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                    incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                    incJsonObj.addProperty("LIDER", auxiliar);
                    incJsonObj.addProperty("CORREO", auxiliar);
                    incJsonObj.addProperty("SEGUNDO", auxiliar);
                    incJsonObj.addProperty("ZONA", auxiliar);
                }

                if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor")
                        || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada")
                        || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                    Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);
                    String estadoApr = aprobacion.getEstadoAprobacion();

                    boolean flag1 = false, flag2 = false;
                    if (estadoApr != null) {
                        String porvAprob = aprobacion.getSolicitadoPor();
                        if (porvAprob.contains("" + idProvedor)) {

                            if (estadoApr.trim().toLowerCase().equals("aceptado")) {
                                incJsonObj.addProperty("BANDERA_CMONTO", 1);
                            } else if (estadoApr.trim().toLowerCase().equals("rechazado")) {
                                incJsonObj.addProperty("BANDERA_CMONTO", 2);
                            } else {
                                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                            }

                            flag1 = true;
                        } else {
                            incJsonObj.addProperty("BANDERA_CMONTO", 0);
                        }
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }

                    Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                    estadoApr = aprobacion2.getEstadoAprobacion();
                    if (estadoApr != null) {
                        String porvAprob = aprobacion2.getSolicitadoPor();
                        if (porvAprob.contains("" + idProvedor)) {

                            if (estadoApr.trim().toLowerCase().equals("aceptado")) {
                                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                            } else if (estadoApr.trim().toLowerCase().equals("rechazado")) {
                                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 2);
                            } else {
                                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                            }
                            flag2 = true;
                        } else {
                            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                        }
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }

                    if (flag1 == true && flag2 == true) {
                        incJsonObj.addProperty("BANDERAS", 1);
                    } else {
                        incJsonObj.addProperty("BANDERAS", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    incJsonObj.addProperty("BANDERAS", 0);
                }

                // ----------------------- Consulta Traking --------------------
                ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

                JsonArray incidenciasJsonArray = new JsonArray();

                ArrayList<Date> RechazoCliente = new ArrayList<Date>();
                ArrayList<Date> inicioAtencion = new ArrayList<Date>();
                ArrayList<Date> cierreTecnico = new ArrayList<Date>();
                ArrayList<Date> cancelados = new ArrayList<Date>();
                ArrayList<Date> atencionGarantia = new ArrayList<Date>();
                ArrayList<Date> cierreGarantia = new ArrayList<Date>();
                ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

                if (arrTraking != null) {
                    Tracking[] traking = arrTraking.getTracking();
                    if (traking != null) {
                        for (Tracking aux2 : traking) {
                            String detalle = aux2.getDetalle();
                            String arrAux[] = detalle.split("\\|\\|");
                            String estado = detalle.split("\\|\\|")[1];
                            String motivoEstado = detalle.split("\\|\\|")[3];
                            Date date2 = aux2.getFechaEnvio().getTime();
                            if (estado.toLowerCase().trim().equals("en recepción")
                                    && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                                RechazoCliente.add(date2);
                            }
                            if (estado.toLowerCase().trim().equals("en proceso")
                                    && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                                inicioAtencion.add(date2);
                            }
                            if (estado.toLowerCase().trim().equals("en recepción")
                                    && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                                cierreTecnico.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase()
                                    .equals("cancelado duplicado")
                                    || motivoEstado.trim().toLowerCase().equals("cancelado no aplica")
                                    || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                                cancelados.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("en proceso")
                                    && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                                atencionGarantia.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("atendido")
                                    && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                                cierreGarantia.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("recibido por atender")
                                    && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                                rechazoProveedor.add(date2);
                            }
                        }
                    }
                }

                int posRC = 0, posRP = 0;

                String cadAux = null;
                if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < RechazoCliente.size(); i++) {
                        if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = RechazoCliente.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                    posRC = pos2;
                } else {

                    incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
                }

                if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < inicioAtencion.size(); i++) {
                        if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = inicioAtencion.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_ATENCION", date3);
                } else {
                    incJsonObj.addProperty("FECHA_ATENCION", cadAux);
                }

                if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cierreTecnico.size(); i++) {
                        if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cierreTecnico.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                    incJsonObj.addProperty("FECHA_CIERRE", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                    incJsonObj.addProperty("FECHA_CIERRE", cadAux);
                }

                if (cancelados != null && !cancelados.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cancelados.size(); i++) {
                        if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cancelados.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CANCELACION", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
                }
                //
                if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < atencionGarantia.size(); i++) {
                        if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = atencionGarantia.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
                } else {
                    incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
                }

                if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cierreGarantia.size(); i++) {
                        if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cierreGarantia.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
                }

                if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < rechazoProveedor.size(); i++) {
                        if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = rechazoProveedor.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                    posRP = pos2;
                } else {
                    incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
                }

                // BANDERA_RECHAZO_CIERRE_PREVIO
                if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null
                        && !RechazoCliente.isEmpty()) {
                    if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                        incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                    } else {
                        incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }

                // ------------------------------------------------------------------
                int cal = 0;
                _Calificacion auxEval = aux.getEvalCliente();
                if (auxEval != null) {
                    if (auxEval.getValue().contains("NA")) {
                        cal = 0;
                    }
                    if (auxEval.getValue().contains("Item1")) {
                        cal = 1;
                    }
                    if (auxEval.getValue().contains("Item2")) {
                        cal = 2;
                    }
                    if (auxEval.getValue().contains("Item3")) {
                        cal = 3;
                    }
                    if (auxEval.getValue().contains("Item4")) {
                        cal = 4;
                    }
                    if (auxEval.getValue().contains("Item5")) {
                        cal = 5;
                    }
                }

                incJsonObj.addProperty("CALIFICACION", cal);

                incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

                date = aux.getFechaCierreAdmin().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

                incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

                // ------------------------------ COMENTARIOS PARA PROVEEDOR
                // --------------------------
                // if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
                /*
                 * Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente,
                 * 2); if(aprobacion!=null) { incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR",
                 * aprobacion.getJustSolicitante());
                 * incJsonObj.addProperty("AUTORIZACION_PROVEEDOR",
                 * aux.getAutorizacionProveedor()); date =
                 * aprobacion.getFechaModificacion().getTime(); date1 = format1.format(date);
                 * incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                 * incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
                 * }
                 */
                if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                    Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                    incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                    incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                    date = aprobacion.getFechaModificacion().getTime();
                    date1 = format1.format(date);
                    incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                    incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());

                    Date dateI = aux.getFechaInicioAtencion().getTime();

                    if (date.compareTo(dateI) >= 0) {
                        incJsonObj.addProperty("LATITUD_INICIO", cord);
                        incJsonObj.addProperty("LONGITUD_INICIO", cord);
                    }
                } else {
                    String auxApr = null;
                    incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
                }

                // -----------------------------------------------------------------------------------
                // ----------------------------FECHA
                // CANCELACION----------------------------------------
                /*
                 * if( aux.getEstado().trim().toLowerCase().equals("atendido") &&
                 * (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||
                 * aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux
                 * .getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))
                 * ) { date = aux.getFechaModificacion().getTime(); date1 =
                 * format1.format(date); incJsonObj.addProperty("FECHA_CANCELACION", "" +
                 * date1); }
                 */
                // -------------------------------------------------------------------------------------
                incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
                incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
                incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());

                arrJsonArray.add(incJsonObj);

                Critica.remove(pos);
            } catch (Exception e) {
                Critica.remove(posActual);
                foliosErrores.add(folioActual);
            }
        }

        while (!normal.isEmpty()) {

            try {

                int pos = 0;
                for (int i = 0; i < normal.size(); i++) {
                    if (normal.get(pos).getFechaCreacion().compareTo(normal.get(i).getFechaCreacion()) >= 0) {
                        pos = i;
                    }
                }

                posActual = pos;
                InfoIncidente aux = normal.get(pos);

                folioActual = aux.getIdIncidencia() + "";

                JsonObject incJsonObj = new JsonObject();
                incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
                incJsonObj.addProperty("ESTADO", aux.getEstado());
                incJsonObj.addProperty("FALLA", aux.getFalla());
                incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
                incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
                incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
                incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
                incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
                incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());

                String idCliente = aux.getIdCorpCliente();
                if (idCliente != null) {
                    if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                        List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                        if (!datos_cliente.isEmpty() && datos_cliente != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                                incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                                incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                                EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                            incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                        incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                        incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                    }
                } else {
                    incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                    incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                }
                String idCord = aux.getIDCorpCoordinador();
                if (idCord != null) {
                    if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                        incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                    }
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                }
                if (idCord != null) {
                    if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                        List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                        if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                                incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                EmpleadosHashMap.put(idCord.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("COORDINADOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                        incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("COORDINADOR", "* Sin información");
                }
                String idSup = aux.getIdCorpSupervisor().trim();
                if (idSup != null) {
                    if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                        incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                    }
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                }
                if (idSup != null) {
                    if (!idSup.matches("[^0-9]*")) {

                        if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                            List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                            if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                                for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                    incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                    incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                    EmpleadosHashMap.put(idSup.trim(), emp_aux);
                                }
                            } else {
                                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                            }
                        } else {
                            DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                            incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                        }

                    } else {
                        incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                        incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                    }
                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }

                // incJsonObj.addProperty("COORDINADOR", "COORDINADOR JUAN PEREZ");
                incJsonObj.addProperty("NOTAS", aux.getNotas());
                incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
                incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());
                if (aux.getProveedor() != null) {
                    if (!lista.isEmpty()) {
                        for (ProveedoresDTO aux3 : lista) {
                            if (aux3.getStatus().contains("Activo")) {
                                if (aux.getProveedor().toLowerCase().trim()
                                        .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                    incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                    incJsonObj.addProperty("MENU", aux3.getMenu());
                                    incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                    incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                    break;
                                }

                            }
                        }
                    }
                } else {
                    String nomCorto = null;
                    incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                    incJsonObj.addProperty("MENU", nomCorto);
                    incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                    incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
                }
                incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
                incJsonObj.addProperty("SUCURSAL", aux.getSucursal());

                incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
                incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
                incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

                incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
                incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
                String autCleinte = aux.getAutorizacionCliente();
                if (autCleinte != null && !autCleinte.equals("")) {
                    if (autCleinte.contains("Rechazado")) {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                    } else {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                    }
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
                }
                Date date = aux.getFechaCierre().getTime();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
                date = aux.getFechaCreacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CREACION", "" + date1);
                date = aux.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
                date = aux.getFechaProgramada().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
                incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
                incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());

                if (aux.getTipoAtencion() != null) {
                    incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("TIPO_ATENCION", x);
                }
                if (aux.getOrigen() != null) {
                    incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("ORIGEN", x);
                }

                //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
                String cord = null;
                if (aux.getLatitudInicio() != null) {
                    incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
                } else {
                    incJsonObj.addProperty("LATITUD_INICIO", cord);
                }

                if (aux.getLongitudInicio() != null) {
                    incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
                } else {
                    incJsonObj.addProperty("LONGITUD_INICIO", cord);
                }

                if (aux.getLatitudCierre() != null) {
                    incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
                } else {
                    incJsonObj.addProperty("LATITUD_FIN", cord);
                }

                if (aux.getLongitudCierre() != null) {
                    incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
                } else {
                    incJsonObj.addProperty("LONGITUD_FIN", cord);
                }

                //----------------------------------------------------------------------------------
                date = aux.getFechaInicioAtencion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

                int idIncidente = aux.getIdIncidencia();

                List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
                int idProvedor = 0;
                int idCuadrilla = 0;
                int idZona = 0;
                for (TicketCuadrillaDTO aux2 : lista2) {
                    if (aux2.getStatus() != 0) {
                        idProvedor = aux2.getIdProveedor();
                        idCuadrilla = aux2.getIdCuadrilla();
                        idZona = aux2.getZona();
                    }

                }
                if (idProvedor != 0) {
                    List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                    for (CuadrillaDTO aux2 : lista3) {
                        if (aux2.getZona() == idZona) {
                            if (aux2.getIdCuadrilla() == idCuadrilla) {
                                incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                                incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                                incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                                incJsonObj.addProperty("CORREO", aux2.getCorreo());
                                incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                                incJsonObj.addProperty("ZONA", aux2.getZona());
                            }
                        }
                    }
                } else {
                    String auxiliar = null;
                    //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                    incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
                    incJsonObj.addProperty("LIDER", auxiliar);
                    incJsonObj.addProperty("CORREO", auxiliar);
                    incJsonObj.addProperty("SEGUNDO", auxiliar);
                    incJsonObj.addProperty("ZONA", auxiliar);
                }

                if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
                    Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);
                    String estadoApr = aprobacion.getEstadoAprobacion();

                    boolean flag1 = false, flag2 = false;
                    if (estadoApr != null) {
                        String porvAprob = aprobacion.getSolicitadoPor();
                        if (porvAprob.contains("" + idProvedor)) {

                            if (estadoApr.trim().toLowerCase().equals("aceptado")) {
                                incJsonObj.addProperty("BANDERA_CMONTO", 1);
                            } else if (estadoApr.trim().toLowerCase().equals("rechazado")) {
                                incJsonObj.addProperty("BANDERA_CMONTO", 2);
                            } else {
                                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                            }

                            flag1 = true;
                        } else {
                            incJsonObj.addProperty("BANDERA_CMONTO", 0);
                        }
                    } else {
                        incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    }

                    Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
                    estadoApr = aprobacion2.getEstadoAprobacion();
                    if (estadoApr != null) {
                        String porvAprob = aprobacion2.getSolicitadoPor();
                        if (porvAprob.contains("" + idProvedor)) {

                            if (estadoApr.trim().toLowerCase().equals("aceptado")) {
                                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                            } else if (estadoApr.trim().toLowerCase().equals("rechazado")) {
                                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 2);
                            } else {
                                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                            }
                            flag2 = true;
                        } else {
                            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                        }
                    } else {
                        incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    }

                    if (flag1 == true && flag2 == true) {
                        incJsonObj.addProperty("BANDERAS", 1);
                    } else {
                        incJsonObj.addProperty("BANDERAS", 0);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                    incJsonObj.addProperty("BANDERAS", 0);
                }

                //-----------------------  Consulta Traking  --------------------
                ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

                JsonArray incidenciasJsonArray = new JsonArray();

                ArrayList<Date> RechazoCliente = new ArrayList<Date>();
                ArrayList<Date> inicioAtencion = new ArrayList<Date>();
                ArrayList<Date> cierreTecnico = new ArrayList<Date>();
                ArrayList<Date> cancelados = new ArrayList<Date>();
                ArrayList<Date> atencionGarantia = new ArrayList<Date>();
                ArrayList<Date> cierreGarantia = new ArrayList<Date>();
                ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

                if (arrTraking != null) {
                    Tracking[] traking = arrTraking.getTracking();
                    if (traking != null) {
                        for (Tracking aux2 : traking) {
                            String detalle = aux2.getDetalle();
                            String arrAux[] = detalle.split("\\|\\|");
                            String estado = detalle.split("\\|\\|")[1];
                            String motivoEstado = detalle.split("\\|\\|")[3];
                            Date date2 = aux2.getFechaEnvio().getTime();
                            if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                                RechazoCliente.add(date2);
                            }
                            if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                                inicioAtencion.add(date2);
                            }
                            if (estado.toLowerCase().trim().equals("en recepción") && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                                cierreTecnico.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("atendido") && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado") || motivoEstado.trim().toLowerCase().equals("cancelado no aplica") || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                                cancelados.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("en proceso") && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                                atencionGarantia.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("atendido") && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                                cierreGarantia.add(date2);
                            }

                            if (estado.toLowerCase().trim().equals("recibido por atender") && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                                rechazoProveedor.add(date2);
                            }
                        }
                    }
                }

                int posRC = 0, posRP = 0;

                String cadAux = null;
                if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < RechazoCliente.size(); i++) {
                        if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = RechazoCliente.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
                    posRC = pos2;
                } else {

                    incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
                }

                if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < inicioAtencion.size(); i++) {
                        if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = inicioAtencion.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_ATENCION", date3);

                } else {
                    incJsonObj.addProperty("FECHA_ATENCION", cadAux);

                }

                if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cierreTecnico.size(); i++) {
                        if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cierreTecnico.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
                    incJsonObj.addProperty("FECHA_CIERRE", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
                    incJsonObj.addProperty("FECHA_CIERRE", cadAux);
                }

                if (cancelados != null && !cancelados.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cancelados.size(); i++) {
                        if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cancelados.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CANCELACION", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
                }
                //
                if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < atencionGarantia.size(); i++) {
                        if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = atencionGarantia.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
                } else {
                    incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
                }

                if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < cierreGarantia.size(); i++) {
                        if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = cierreGarantia.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
                } else {
                    incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
                }

                if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
                    int pos2 = 0;
                    for (int i = 0; i < rechazoProveedor.size(); i++) {
                        if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                            pos2 = i;
                        }
                    }
                    Date date2 = rechazoProveedor.get(pos2);
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date3 = format2.format(date2);
                    incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
                    posRP = pos2;
                } else {
                    incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
                }

                //BANDERA_RECHAZO_CIERRE_PREVIO
                if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null && !RechazoCliente.isEmpty()) {
                    if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                        incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
                    } else {
                        incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                    }
                } else {
                    incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                }

                //------------------------------------------------------------------
                int cal = 0;
                _Calificacion auxEval = aux.getEvalCliente();
                if (auxEval != null) {
                    if (auxEval.getValue().contains("NA")) {
                        cal = 0;
                    }
                    if (auxEval.getValue().contains("Item1")) {
                        cal = 1;
                    }
                    if (auxEval.getValue().contains("Item2")) {
                        cal = 2;
                    }
                    if (auxEval.getValue().contains("Item3")) {
                        cal = 3;
                    }
                    if (auxEval.getValue().contains("Item4")) {
                        cal = 4;
                    }
                    if (auxEval.getValue().contains("Item5")) {
                        cal = 5;
                    }
                }

                incJsonObj.addProperty("CALIFICACION", cal);
                incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

                date = aux.getFechaCierreAdmin().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

                incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

                //------------------------------ COMENTARIOS PARA PROVEEDOR --------------------------
                //if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
                /*
                 Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 2);
                 if(aprobacion!=null) {
                 incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                 incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                 date = aprobacion.getFechaModificacion().getTime();
                 date1 = format1.format(date);
                 incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                 incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
                 }
                 */
                if (AprobacionesHashMap.containsKey("" + idIncidente)) {
                    Aprobacion aprobacion = AprobacionesHashMap.get("" + idIncidente);

                    incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
                    incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
                    date = aprobacion.getFechaModificacion().getTime();

                    date1 = format1.format(date);
                    incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
                    incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());

                    Date dateI = aux.getFechaInicioAtencion().getTime();

                    if (date.compareTo(dateI) >= 0) {
                        incJsonObj.addProperty("LATITUD_INICIO", cord);
                        incJsonObj.addProperty("LONGITUD_INICIO", cord);
                    }

                } else {
                    String auxApr = null;
                    incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
                    incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
                }

                //-----------------------------------------------------------------------------------
                //----------------------------FECHA CANCELACION----------------------------------------
                /*if( aux.getEstado().trim().toLowerCase().equals("atendido") && (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux.getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))) {
                 date = aux.getFechaModificacion().getTime();
                 date1 = format1.format(date);
                 incJsonObj.addProperty("FECHA_CANCELACION", "" + date1);
                 }*/
                //-------------------------------------------------------------------------------------
                incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
                incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
                incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());
                arrJsonArray.add(incJsonObj);

                normal.remove(pos);

            } catch (Exception e) {
                normal.remove(posActual);
                foliosErrores.add(folioActual);
            }

        }

        return arrJsonArray;
    }

    // http://localhost:8080/migestion/servicios/getIncidentesProveedor.json?proveedor=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String proveedor = urides.split("&")[1].split("=")[1];
            ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.consultaFoliosProveedor(proveedor.trim(), "1");
            ArrayOfInfoIncidente arrIncidentesCerrados = serviciosRemedyBI.consultaFoliosProveedor(proveedor.trim(), "2");

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();

            ArrayList<String> supervisores = new ArrayList<String>();

            if (arrIncidentes != null) {
                InfoIncidente[] arregloInc = arrIncidentes.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()+ ", ");
                        if (aux.getEstado().contains("Recibido por atender")) {
                            RPA.add(aux);
                        }
                        if (aux.getEstado().contains("En Proceso")) {
                            EP.add(aux);
                        }
                        if (aux.getEstado().contains("En Recepción")) {
                            EPC.add(aux);
                        }
                        if (aux.getEstado().contains("Atendido")) {
                            Atendidos.add(aux);
                        }
                        if (aux.getEstado().contains("Pendiente")) {
                            PPA.add(aux);
                        }
                        if (supervisores.isEmpty()) {
                            if (aux.getIdCorpSupervisor() != null) {
                                supervisores.add(aux.getIdCorpSupervisor().trim());
                            }
                        } else {
                            boolean flagSup = false;
                            String supervisor = aux.getIdCorpSupervisor();
                            if (supervisor != null) {
                                for (String sup : supervisores) {
                                    if (sup.equals(aux.getIdCorpSupervisor().trim())) {
                                        flagSup = true;
                                        break;
                                    }
                                }
                                if (flagSup == false) {
                                    supervisores.add(aux.getIdCorpSupervisor().trim());
                                }
                            }
                        }

                    }
                }
            }

            if (arrIncidentesCerrados != null) {
                InfoIncidente[] arregloInc = arrIncidentesCerrados.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()+ ", ");
                        if (aux.getEstado().contains("Recibido por atender")) {
                            RPA.add(aux);
                        }
                        if (aux.getEstado().contains("En Proceso")) {
                            EP.add(aux);
                        }
                        if (aux.getEstado().contains("En Recepción")) {
                            EPC.add(aux);
                        }
                        if (aux.getEstado().contains("Atendido")) {
                            Atendidos.add(aux);
                        }
                        if (aux.getEstado().contains("Pendiente")) {
                            PPA.add(aux);
                        }
                        if (supervisores.isEmpty()) {
                            if (aux.getIdCorpSupervisor() != null) {
                                supervisores.add(aux.getIdCorpSupervisor().trim());
                            }
                        } else {
                            boolean flagSup = false;
                            String supervisor = aux.getIdCorpSupervisor();
                            if (supervisor != null) {
                                for (String sup : supervisores) {
                                    if (sup.equals(aux.getIdCorpSupervisor().trim())) {
                                        flagSup = true;
                                        break;
                                    }
                                }
                                if (flagSup == false) {
                                    supervisores.add(aux.getIdCorpSupervisor().trim());
                                }
                            }
                        }

                    }
                }
            }

            // -------------------------------------------------------------------------------
            JsonArray supervisoresJsonArray = new JsonArray();
            if (!supervisores.isEmpty() && supervisores != null) {
                for (String sup : supervisores) {
                    JsonObject incJsonObj = new JsonObject();
                    if (!sup.matches("[^0-9]*")) {

                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(sup));
                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getNombre());
                                supervisoresJsonArray.add(incJsonObj);
                            }
                        }
                    } else {
                        System.out.println("No es un número");
                        incJsonObj.addProperty("SUPERVISOR", "JUAN PEREZ");
                        supervisoresJsonArray.add(incJsonObj);
                    }
                }
            }

            // --------------------------------------------------------------------------------
            JsonArray AtendidosJsonArray = new JsonArray();
            if (!Atendidos.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : Atendidos) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }

                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------

                AtendidosJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPCJsonArray = new JsonArray();
            if (!EPC.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EPC) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                EPCJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPJsonArray = new JsonArray();
            if (!EP.isEmpty() && RPA != null) {

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EP) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                EPJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray RPAJsonArray = new JsonArray();
            if (!RPA.isEmpty() && RPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : RPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                RPAJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray PPAJsonArray = new JsonArray();
            if (!PPA.isEmpty() && PPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : PPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int i = 0; i < aprHilos.size(); i++) {
                    aprHilos.get(i).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int i = 0; i < aprHilos.size(); i++) {
                        if (aprHilos.get(i).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(i).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int i = 0; i < aprHilos.size(); i++) {
                    AprobacionesHashMap.put("" + aprHilos.get(i).getIdFolio(), aprHilos.get(i).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                PPAJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);

            }

            if (arrIncidentes == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", RPAJsonArray);
                envoltorioJsonObj.add("EN_PROCESO", EPJsonArray);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", EPCJsonArray);
                envoltorioJsonObj.add("ATENDIDOS", AtendidosJsonArray);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", PPAJsonArray);
                envoltorioJsonObj.add("SUPERVISORES", supervisoresJsonArray);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            //UtilGTN.printErrorLog(null, e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/loginProveedor.json?idUsuario=<?>&idProveedor=<?>&password=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/loginProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String loginProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = urides.split("&")[1].split("=")[1];
            String passwordAPP = urides.split("&")[2].split("=")[1];
            if (idUsuario.matches("\\d+(\\.\\d+)?")) {

                List<ProveedorLoginDTO> lista = proveedorLoginBI.obtieneDatos(Integer.parseInt(idUsuario));

                String passwordBD = "";
                for (ProveedorLoginDTO aux : lista) {
                    passwordBD = aux.getPassw();
                }

                int resp = 0;
                int igual = 0;
                if (!passwordBD.equals("")) {
                    if (passwordAPP.trim().equals(passwordBD.trim())) {
                        resp = 1;
                        if (passwordBD.trim().equals(idUsuario.trim())) {
                            igual = 1;
                        }
                    }

                }

                JsonObject envoltorioJsonObj = new JsonObject();
                if (resp == 1) {
                    List<ProveedoresDTO> lista2 = proveedoresBI.obtieneDatos(Integer.parseInt(idUsuario));

                    for (ProveedoresDTO aux : lista2) {
                        envoltorioJsonObj.addProperty("EXITO", resp);
                        envoltorioJsonObj.addProperty("IGUAL", igual);
                        envoltorioJsonObj.addProperty("ID_PROVEEDOR", aux.getIdProveedor());
                        envoltorioJsonObj.addProperty("STATUS", aux.getStatus());
                        envoltorioJsonObj.addProperty("MENU", aux.getMenu());
                        envoltorioJsonObj.addProperty("NOM_CORTO", aux.getNombreCorto());
                        envoltorioJsonObj.addProperty("RAZON_SOCIAL", aux.getRazonSocial());
                        envoltorioJsonObj.addProperty("TIPO_PROVEEDOR", aux.getTipoProveedor());

                    }

                } else {
                    String x = null;
                    envoltorioJsonObj.addProperty("EXITO", resp);
                    envoltorioJsonObj.addProperty("IGUAL", igual);
                    envoltorioJsonObj.addProperty("ID_PROVEEDOR", x);
                    envoltorioJsonObj.addProperty("STATUS", x);
                    envoltorioJsonObj.addProperty("MENU", x);
                    envoltorioJsonObj.addProperty("NOM_CORTO", x);
                    envoltorioJsonObj.addProperty("RAZON_SOCIAL", x);
                    envoltorioJsonObj.addProperty("TIPO_PROVEEDOR", x);
                }

                //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();
            } else {
                return "{}";
            }
        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getModificaPasswordProveedor.json?idUsiuario=<?>&idProveedor=<?>&password=<?>&passwordNuevo=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getModificaPasswordProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String getModificaPasswordProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = urides.split("&")[1].split("=")[1];
            String password1 = urides.split("&")[2].split("=")[1];
            String password2 = urides.split("&")[3].split("=")[1];

            List<ProveedorLoginDTO> lista = proveedorLoginBI.obtieneDatos(Integer.parseInt(idUsuario));

            String passwordBD = "";
            for (ProveedorLoginDTO aux : lista) {
                passwordBD = aux.getPassw();
            }

            int resp = 0;
            int igual = 0;
            if (!passwordBD.equals("")) {
                if (password1.trim().equals(passwordBD.trim())) {
                    resp = 1;
                    if (password2.trim().equals(idUsuario.trim())) {
                        igual = 1;
                    }
                }

            }

            boolean res2 = false;
            if (resp == 1) {
                if (igual == 0) {
                    ProveedorLoginDTO log = new ProveedorLoginDTO();

                    log.setIdProveedor(Integer.parseInt(idUsuario));
                    log.setPassw(password2);

                    res2 = proveedorLoginBI.actualiza(log);
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();
            if (resp == 0) {
                envoltorioJsonObj.addProperty("RES", 2);
            } else {
                if (res2 == true) {
                    envoltorioJsonObj.addProperty("RES", 1);
                } else {
                    envoltorioJsonObj.addProperty("RES", 0);
                }
            }

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getCuadrillas.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getCuadrillas", method = RequestMethod.GET)
    public @ResponseBody
    String getCuadrillas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if (urides.length() > 0) {
                String idProveedor = urides.split("&")[1].split("=")[1];
                if (idProveedor.matches("\\d+(\\.\\d+)?")) {
                    List<CuadrillaDTO> lista = cuadrillaBI.obtieneDatos(Integer.parseInt(idProveedor));

                    ArrayList<CuadrillaDTO> ZNorte = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZCentro = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZSur = new ArrayList<CuadrillaDTO>();

                    if (!lista.isEmpty() && lista != null) {
                        for (CuadrillaDTO aux : lista) {
                            if (aux.getZona() == 0) {
                                ZNorte.add(aux);
                            }
                            if (aux.getZona() == 1) {
                                ZCentro.add(aux);
                            }
                            if (aux.getZona() == 2) {
                                ZSur.add(aux);
                            }
                        }
                    }

                    JsonArray ZNJsonArray = new JsonArray();
                    if (!ZNorte.isEmpty() && ZNorte != null) {

                        for (CuadrillaDTO aux : ZNorte) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZNJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZCJsonArray = new JsonArray();
                    if (!ZCentro.isEmpty() && ZCentro != null) {

                        for (CuadrillaDTO aux : ZCentro) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZCJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZSJsonArray = new JsonArray();
                    if (!ZSur.isEmpty() && ZSur != null) {

                        for (CuadrillaDTO aux : ZSur) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZSJsonArray.add(incJsonObj);
                        }

                    }

                    JsonObject envoltorioJsonObj = new JsonObject();

                    if (lista == null) {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    } else {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    }
                    //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                    res = envoltorioJsonObj.toString();
                } else {
                    return "{}";
                }
            } else {
                return "{}";
            }
        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    /*// http://localhost:8080/migestion/servicios/getCuadrillasNew.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getCuadrillasNew", method = RequestMethod.GET)
    public @ResponseBody
    String getCuadrillasNew(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;

        String resCifrado = "";

        UtilCryptoGS cifra = null;
        StrCipher cifraIOS = null;

        int idLlave=0;
        try {

        	 cifra = new UtilCryptoGS();
             cifraIOS = new StrCipher();

            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if(urides.length()>0){
            String idProveedor = urides.split("&")[1].split("=")[1];
                if(idProveedor.matches("\\d+(\\.\\d+)?")){
                    List<CuadrillaDTO> lista = cuadrillaBI.obtieneDatos(Integer.parseInt(idProveedor));

                    ArrayList<CuadrillaDTO> ZNorte = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZCentro = new ArrayList<CuadrillaDTO>();
                    ArrayList<CuadrillaDTO> ZSur = new ArrayList<CuadrillaDTO>();

                    if (!lista.isEmpty() && lista != null) {
                        for (CuadrillaDTO aux : lista) {
                            if (aux.getZona() == 0) {
                                ZNorte.add(aux);
                            }
                            if (aux.getZona() == 1) {
                                ZCentro.add(aux);
                            }
                            if (aux.getZona() == 2) {
                                ZSur.add(aux);
                            }
                        }
                    }

                    JsonArray ZNJsonArray = new JsonArray();
                    if (!ZNorte.isEmpty() && ZNorte != null) {

                        for (CuadrillaDTO aux : ZNorte) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZNJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZCJsonArray = new JsonArray();
                    if (!ZCentro.isEmpty() && ZCentro != null) {

                        for (CuadrillaDTO aux : ZCentro) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZCJsonArray.add(incJsonObj);
                        }

                    }

                    JsonArray ZSJsonArray = new JsonArray();
                    if (!ZSur.isEmpty() && ZSur != null) {

                        for (CuadrillaDTO aux : ZSur) {
                            JsonObject incJsonObj = new JsonObject();
                            incJsonObj.addProperty("ZONA", aux.getZona());
                            incJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                            incJsonObj.addProperty("LIDER", aux.getLiderCuad());
                            incJsonObj.addProperty("CORREO", aux.getCorreo());
                            incJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                            incJsonObj.addProperty("PASSW", aux.getPassw());
                            ZSJsonArray.add(incJsonObj);
                        }

                    }

                    JsonObject envoltorioJsonObj = new JsonObject();

                    if (lista == null) {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    } else {
                        envoltorioJsonObj.add("ZONA_NORTE", ZNJsonArray);
                        envoltorioJsonObj.add("ZONA_CENTRO", ZCJsonArray);
                        envoltorioJsonObj.add("ZONA_SUR", ZSJsonArray);
                    }
                    //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                    res = envoltorioJsonObj.toString();


                }else{
                	return UtilGTN.encrypResult(idLlave,"{}");

                }
            }else{

            	return UtilGTN.encrypResult(idLlave,"{}");
            }
        } catch (Exception e) {

             return UtilGTN.encrypResult(idLlave,"{}");
        }

        if (res != null) {
            //return res;
        	return UtilGTN.encrypResult(idLlave,res);
        } else {

        	return UtilGTN.encrypResult(idLlave,"{}");
        }
    }*/
    // http://localhost:8080/migestion/servicios/getAltaCuadrilla.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getAltaCuadrilla", method = RequestMethod.GET)
    public @ResponseBody
    String getAltaCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            if (urides.length() > 0) {
                String idProveedor = urides.split("&")[1].split("=")[1];
                String zona = urides.split("&")[2].split("=")[1];
                String liderCuad = urides.split("&")[3].split("=")[1];
                String correo = urides.split("&")[4].split("=")[1];
                String segundo = urides.split("&")[5].split("=")[1];
                String password = urides.split("&")[6].split("=")[1];

                CuadrillaDTO cua = new CuadrillaDTO();

                cua.setIdProveedor(Integer.parseInt(idProveedor));
                cua.setZona(Integer.parseInt(zona));
                cua.setLiderCuad(liderCuad);
                cua.setCorreo(correo);
                cua.setSegundo(segundo);
                cua.setPassw(password);

                int resp = cuadrillaBI.inserta(cua);

                JsonObject envoltorioJsonObj = new JsonObject();

                if (resp != 0) {

                    boolean respuesta = serviciosRemedyBI.crearCuadrillas(idProveedor + "_" + zona + "_" + resp, segundo, password, correo, liderCuad, Integer.parseInt(idProveedor), "55", zona);
                    logger.info("respuesta: " + respuesta);

                    envoltorioJsonObj.addProperty("ALTA", 1);
                } else {
                    envoltorioJsonObj.addProperty("ALTA", 0);

                }
                /*
             envoltorioJsonObj.add("ZONA_CENTRO", 1);
             envoltorioJsonObj.add("ZONA_SUR", 1);*/

                //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();
            } else {
                return "{}";
            }
        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getModificaCuadrilla.json?idUsuario=<?>&id=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getModificaCuadrilla", method = RequestMethod.GET)
    public @ResponseBody
    String getModificaCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idCuadrilla = urides.split("&")[1].split("=")[1];
            String idProveedor = urides.split("&")[2].split("=")[1];
            String zona = urides.split("&")[3].split("=")[1];
            String liderCuad = urides.split("&")[4].split("=")[1];
            String correo = urides.split("&")[5].split("=")[1];
            String segundo = urides.split("&")[6].split("=")[1];
            String pwd = urides.split("&")[7].split("=")[1];

            CuadrillaDTO cua = new CuadrillaDTO();

            cua.setIdCuadrilla(Integer.parseInt(idCuadrilla));
            cua.setIdProveedor(Integer.parseInt(idProveedor));
            cua.setZona(Integer.parseInt(zona));
            cua.setLiderCuad(liderCuad);
            cua.setCorreo(correo);
            cua.setSegundo(segundo);
            cua.setPassw(pwd);

            boolean resp = cuadrillaBI.actualiza(cua);

            JsonObject envoltorioJsonObj = new JsonObject();

            if (resp) {
                boolean respuesta = serviciosRemedyBI.actualizaCuadrillas(idProveedor + "_" + zona + "_" + idCuadrilla, segundo, pwd, correo, liderCuad, Integer.parseInt(idProveedor), "55", zona);
                envoltorioJsonObj.addProperty("MODIFICADO", 1);
            } else {
                envoltorioJsonObj.addProperty("MODIFICADO", 0);
            }

            /*
             envoltorioJsonObj.add("ZONA_CENTRO", 1);
             envoltorioJsonObj.add("ZONA_SUR", 1);*/
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();
        } catch (Exception e) {
            //logger.info(e);

            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getDeclinaTicketProv.json?idUsuario=<?>&idIncidente=<?>&justificacion=<?>
    // @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDeclinaTicketProv", method = RequestMethod.GET)
    public @ResponseBody
    String getDeclinaTicketProv(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String justificacion = urides.split("&")[2].split("=")[1];

            boolean rechazaProveeedor = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {

                        if ((aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("asignado por aceptar")) || (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                rechazaProveeedor = serviciosRemedyBI.autorizaProveedor(idIncidente, justificacion, false, "");
            }

            if (resp != 2) {
                if (rechazaProveeedor) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!rechazaProveeedor) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getAceptaTicketProv.json?idUsuario=<?>&idIncidente=<?>&idProveedor=<?>&idCuadrilla=<?>&zona=<?>&editar=<?>
    // @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAceptaTicketProv", method = RequestMethod.GET)
    public @ResponseBody
    String getAceptaTicketProv(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            //String justificacion = urides.split("&")[2].split("=")[1];
            String idProvedor = urides.split("&")[2].split("=")[1];
            String idCuadrilla = urides.split("&")[3].split("=")[1];
            String idZona = urides.split("&")[4].split("=")[1];

            String editar = "";
            try {
                editar = urides.split("&")[5].split("=")[1];
            } catch (Exception e) {
                editar = "0";
            }

            JsonObject envoltorioJsonObj = null;

            if (editar.equals("1")) {

                envoltorioJsonObj = new JsonObject();

                List<CuadrillaDTO> lista2 = cuadrillaBI.obtieneDatos(Integer.parseInt(idProvedor));
                String lider = "";;
                for (CuadrillaDTO aux2 : lista2) {
                    int idCuadrillaI = Integer.parseInt(idCuadrilla);
                    int idZonaI = Integer.parseInt(idZona);
                    if (aux2.getIdCuadrilla() == idCuadrillaI && aux2.getZona() == idZonaI) {
                        lider = aux2.getLiderCuad();
                    }
                }

                serviciosRemedyBI.autorizaProveedor(idIncidente, lider, true, idProvedor + "_" + idZona + "_" + idCuadrilla);

                ticketCuadrillaBI.elimina(idIncidente + "");

                TicketCuadrillaDTO bean = new TicketCuadrillaDTO();
                bean.setIdCuadrilla(Integer.parseInt(idCuadrilla));
                bean.setIdProveedor(Integer.parseInt(idProvedor));
                bean.setZona(Integer.parseInt(idZona));
                bean.setTicket("" + idIncidente);
                bean.setStatus(1);

                ticketCuadrillaBI.inserta(bean);

                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", 1);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);

            } else {

                boolean rechazaProveeedor = false;

                ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
                int resp = 0;
                if (auxIncidente != null) {
                    InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                    if (arregloInc != null) {
                        for (InfoIncidente aux : arregloInc) {
                            if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                    && aux.getMotivoEstado().trim().toLowerCase().equals("asignado por aceptar")) {
                                resp = 0;
                            } else {
                                resp = 1;
                            }
                        }
                    }
                }
                if (resp == 1) {
                    resp = 2;
                } else {

                    List<CuadrillaDTO> lista2 = cuadrillaBI.obtieneDatos(Integer.parseInt(idProvedor));
                    String lider = "";;
                    for (CuadrillaDTO aux2 : lista2) {
                        int idCuadrillaI = Integer.parseInt(idCuadrilla);
                        int idZonaI = Integer.parseInt(idZona);
                        if (aux2.getIdCuadrilla() == idCuadrillaI && aux2.getZona() == idZonaI) {
                            lider = aux2.getLiderCuad();
                        }
                    }

                    rechazaProveeedor = serviciosRemedyBI.autorizaProveedor(idIncidente, lider, true, idProvedor + "_" + idZona + "_" + idCuadrilla);

                }

                if (resp != 2) {
                    if (rechazaProveeedor) {
                        List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
                        for (TicketCuadrillaDTO aux2 : lista2) {
                            if (aux2.getStatus() != 0) {
                                aux2.setStatus(0);
                                //logger.info("modificando status" + ticketCuadrillaBI.actualiza(aux2));
                                logger.info("modificando status" + ticketCuadrillaBI.elimina(aux2.getTicket())); //Se elimina el registro. patra que pueda insertar el nuevo
                            }
                        }

                        TicketCuadrillaDTO bean = new TicketCuadrillaDTO();
                        bean.setIdCuadrilla(Integer.parseInt(idCuadrilla));
                        bean.setIdProveedor(Integer.parseInt(idProvedor));
                        bean.setZona(Integer.parseInt(idZona));
                        bean.setTicket("" + idIncidente);
                        bean.setStatus(1);
                        logger.info(ticketCuadrillaBI.inserta(bean)); /// Se encuentra error del cual lla cuadrilla sale en blanco , no se asigna la cuadrilla.
                        resp = 1;
                    } else {
                        resp = 0;
                    }
                }

                envoltorioJsonObj = new JsonObject();

                if (!rechazaProveeedor) {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("RES", resp);
                    envoltorioJsonObj.add("MODIFICADO", insJsonObj);
                } else {
                    JsonObject insJsonObj = new JsonObject();
                    insJsonObj.addProperty("RES", resp);
                    envoltorioJsonObj.add("MODIFICADO", insJsonObj);
                }

            }

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/postCierreTecnicoRemoto.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postCierreTecnicoRemoto", method = RequestMethod.POST)
    public @ResponseBody
    String postCierreTecnicoRemoto(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                ////logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                ////logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("postCierreTecnicoRemoto MANTENIMIENTO JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("postCierreTecnicoRemoto MANTENIMIENTO JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            logger.info("postCloseTecnicoRemoto Colose técnico remoto MANTENIMIENTO USUARIO: " + usuario);
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";

            String nomAdjunto = "";
            String adjunto = "";
            String nomAdjunto2 = "";
            String adjunto2 = "";

            String solicitante = "";
            String aprobador = "";
            String clienteAvisado = "";
            String OT = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                //logger.info("postCierreTecnicoRemoto Cierre técnico remoto CONTENIDO: " + rec);
                ////logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
                nomAdjunto2 = rec.getString("nomAdjunto2");
                adjunto2 = rec.getString("adjunto2");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
                clienteAvisado = rec.getString("clienteAvisado");
                OT = rec.getString("OT");
            }

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada"))) {
                            resp = 0;
                            logger.info("SI procede");
                        } else {
                            resp = 1;
                            logger.info("NO procede");
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
                logger.info("postCloseTecnicoRemoto - NO procede close n2");

            } else {
                logger.info("postCloseTecnicoRemoto - SI procede close en espera de idFolio ");

                if (idFolio != null && !idFolio.equals("")) {
                    actIncidente = serviciosRemedyBI.cierreTecnicoRemoto(Integer.parseInt(idFolio), justificacion, nomAdjunto, adjunto, nomAdjunto2, adjunto2, solicitante, aprobador, clienteAvisado, OT);
                    logger.info("postCloseTecnicoRemoto - SI procede close con idFolio: " + idFolio);
                    logger.info("Respuesta postCloseTecnicoRemoto: " + actIncidente);
                }
            }

            logger.info("postCloseTecnicoRemoto valor de resp: " + resp);
            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();
            logger.info("postCloseTecnicoRemoto valor de Obj " + resp);

        } catch (Exception e) {
            logger.info("AP postCloseTecnicoRemoto" + e);
            logger.info(e);
            e.printStackTrace();
            logger.info("postCloseTecnicoRemoto respuesta {}");
            return "{}";
        }

        if (res != null) {
            logger.info("postCloseTecnicoRemoto respuesta " + res.toString());
            return res.toString();
        } else {
            logger.info("postCloseTecnicoRemoto respuesta {}");
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/postCierreTecnicoSitio.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postCierreTecnicoSitio", method = RequestMethod.POST)
    public @ResponseBody
    String postCierreTecnicoSitio(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            // boolean autoriza = false;
            String justificacion = "";
            String idFolio = "";

            String nomAdjunto = "";
            String adjunto = "";
            String nomAdjunto2 = "";
            String adjunto2 = "";

            String latitud = "";
            String longitud = "";

            String solicitante = "";
            String aprobador = "";

            String clienteAvisado = "";
            String OT = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ////logger.info("getAutorizaMonto: " + rec.toString());

                idFolio = rec.getString("folio");
                justificacion = rec.getString("comentarios");
                nomAdjunto = rec.getString("nomAdjunto");
                adjunto = rec.getString("adjunto");
                nomAdjunto2 = rec.getString("nomAdjunto2");
                adjunto2 = rec.getString("adjunto2");
                latitud = rec.getString("latitud");
                longitud = rec.getString("longitud");
                solicitante = rec.getString("solicitante");
                aprobador = rec.getString("aprobador");
                clienteAvisado = rec.getString("clienteAvisado");
                OT = rec.getString("OT");
            }

            //Validacion para no avanzar si las fotos pesan mas de 2.0 mb y no impactar monitoreo
            if (adjunto.getBytes().length > 2000000) {
                logger.info("Usuario = " + usuario);
                logger.info("folio =  " + idFolio);
                logger.info("solicitante = " + solicitante);
                logger.info("Adjunto 1 size = " + adjunto.length());
                return "{}";
            }

            if (adjunto2.getBytes().length > 2000000) {
                logger.info("Usuario = " + usuario);
                logger.info("folio =  " + idFolio);
                logger.info("solicitante = " + solicitante);
                logger.info("Adjunto 2 size = " + adjunto2.length());
                return "{}";
            }

            boolean actIncidente = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {
                if (idFolio != null && !idFolio.equals("")) {
                    actIncidente = serviciosRemedyBI.cierreTecnicoSitio(Integer.parseInt(idFolio), justificacion, nomAdjunto, adjunto, nomAdjunto2, adjunto2, latitud, longitud, solicitante, aprobador, clienteAvisado, OT);
                }
            }

            if (resp != 2) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!actIncidente) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getIniciaAtencionProveedor.json?idUsuario=<?>&idIncidente=<?>&fecha=<?>&latitud=<?>&longitud=<?>
    // @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIniciaAtencionProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String getIniciaAtencionProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            int idIncidente = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            //String justificacion = urides.split("&")[2].split("=")[1];
            String fecha = urides.split("&")[2].split("=")[1];
            String latitud = urides.split("&")[3].split("=")[1];
            String longitud = urides.split("&")[4].split("=")[1];

            boolean rechazaProveeedor = false;

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(idIncidente);
            int resp = 0;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {

                        if (aux.getEstado().trim().toLowerCase().equals("en proceso")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor") || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada") || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            if (resp == 1) {
                resp = 2;
            } else {

                rechazaProveeedor = serviciosRemedyBI.inicioAtencion(idIncidente, fecha, latitud, longitud);

            }

            if (resp != 2) {
                if (rechazaProveeedor) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            if (!rechazaProveeedor) {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            } else {
                JsonObject insJsonObj = new JsonObject();
                insJsonObj.addProperty("RES", resp);
                envoltorioJsonObj.add("MODIFICADO", insJsonObj);
            }
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/loginProveedorCuadrilla.json?idUsiuario=<?>&idProveedor=<?>&zona=<?>&idCuadrilla=<?>&password=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/loginProveedorCuadrilla", method = RequestMethod.GET)
    public @ResponseBody
    String loginProveedorCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idProveedor = urides.split("&")[1].split("=")[1];
            String zona = urides.split("&")[2].split("=")[1];
            String idCuadrilla = urides.split("&")[3].split("=")[1];
            String passwordAPP = urides.split("&")[4].split("=")[1];
            System.out.println("idProveedor " + idProveedor);
            System.out.println("zona " + zona);
            System.out.println("idCuadrilla " + idCuadrilla);
            System.out.println("passwordAPP " + passwordAPP);
            if (idProveedor.matches("\\d+(\\.\\d+)?") && zona.matches("\\d+(\\.\\d+)?") && idCuadrilla.matches("\\d+(\\.\\d+)?")) {

                List<CuadrillaDTO> lista = cuadrillaBI.obtienepass(Integer.parseInt(idCuadrilla), Integer.parseInt(idProveedor), Integer.parseInt(zona));

                String passwordBD = "";
                for (CuadrillaDTO aux : lista) {
                    passwordBD = aux.getPassw();
                }

                int resp = 0;

                if (!passwordBD.equals("")) {
                    if (passwordAPP.trim().equals(passwordBD.trim())) {
                        resp = 1;
                    }

                }

                JsonObject envoltorioJsonObj = new JsonObject();
                if (resp == 1) {

                    for (CuadrillaDTO aux : lista) {

                        envoltorioJsonObj.addProperty("EXITO", resp);
                        envoltorioJsonObj.addProperty("ID_PROVEEDOR", aux.getIdProveedor());
                        envoltorioJsonObj.addProperty("ID_CUADRILLA", aux.getIdCuadrilla());
                        envoltorioJsonObj.addProperty("ZONA", aux.getZona());
                        envoltorioJsonObj.addProperty("LIDER", aux.getLiderCuad());
                        envoltorioJsonObj.addProperty("CORREO", aux.getCorreo());
                        envoltorioJsonObj.addProperty("SEGUNDO", aux.getSegundo());
                        List<ProveedoresDTO> lista2 = proveedoresBI.obtieneDatos(aux.getIdProveedor());

                        for (ProveedoresDTO aux2 : lista2) {
                            envoltorioJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                            envoltorioJsonObj.addProperty("RAZON_SOCIAL", aux2.getRazonSocial());
                            envoltorioJsonObj.addProperty("NOMBRE_CORTO", aux2.getNombreCorto());
                            envoltorioJsonObj.addProperty("MENU", aux2.getMenu());
                            envoltorioJsonObj.addProperty("STATUS", aux2.getStatus());
                        }
                    }

                } else {
                    String x = null;
                    envoltorioJsonObj.addProperty("EXITO", resp);
                    //envoltorioJsonObj.addProperty("ID_PROVEEDOR", x);
                    envoltorioJsonObj.addProperty("ID_CUADRILLA", x);
                    envoltorioJsonObj.addProperty("ZONA", x);
                    envoltorioJsonObj.addProperty("LIDER", x);
                    envoltorioJsonObj.addProperty("CORREO", x);
                    envoltorioJsonObj.addProperty("SEGUNDO", x);
                    //envoltorioJsonObj.addProperty("ID_PROVEEDOR", x);
                    envoltorioJsonObj.addProperty("RAZON_SOCIAL", x);
                    envoltorioJsonObj.addProperty("NOMBRE_CORTO", x);
                    envoltorioJsonObj.addProperty("MENU", x);
                    envoltorioJsonObj.addProperty("STATUS", x);
                }

                ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();
            } else {
                return "{}";
            }
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getIncidentesCuadrilla.json?idCoordinador=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIncidentesCuadrilla", method = RequestMethod.GET)
    public @ResponseBody
    String getIncidentesCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String proveedor = urides.split("&")[1].split("=")[1];
            String zona = urides.split("&")[2].split("=")[1];
            String cuadrilla = urides.split("&")[3].split("=")[1];

            List<TicketCuadrillaDTO> lista = ticketCuadrillaBI.obtieneCuadrillaTK(Integer.parseInt(cuadrilla), Integer.parseInt(proveedor), Integer.parseInt(zona));

            String[] tickets = new String[lista.size()];
            int i = 0;
            for (TicketCuadrillaDTO aux : lista) {
                tickets[i] = aux.getTicket();
                i++;
            }

            List<ProveedoresDTO> lista2 = proveedoresBI.obtieneDatos(Integer.parseInt(proveedor));
            String razonSocial = "";
            for (ProveedoresDTO aux2 : lista2) {
                razonSocial = aux2.getRazonSocial();
            }

            // int idFolio=0;
            ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.consultaFoliosProveedor(razonSocial.trim(), "3");
            ArrayOfInfoIncidente arrIncidentesCerrados = serviciosRemedyBI.consultaFoliosProveedor(razonSocial.trim(), "4");

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();

            ArrayList<String> supervisores = new ArrayList<String>();

            if (arrIncidentes != null) {
                InfoIncidente[] arregloInc = arrIncidentes.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        boolean flag = false;
                        for (String auxS : tickets) {
                            String idIncidentS = "" + aux.getIdIncidencia();
                            if (idIncidentS.compareTo(auxS) == 0) {
                                flag = true;
                                break;
                            }
                        }

                        if (flag == true) {
                            //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
                            if (aux.getEstado().contains("Recibido por atender")) {
                                RPA.add(aux);
                            }
                            if (aux.getEstado().contains("En Proceso")) {
                                EP.add(aux);
                            }
                            if (aux.getEstado().contains("En Recepción")) {
                                EPC.add(aux);
                            }
                            if (aux.getEstado().contains("Atendido")) {
                                Atendidos.add(aux);
                            }
                            if (aux.getEstado().contains("Pendiente")) {
                                PPA.add(aux);
                            }
                            if (supervisores.isEmpty()) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    supervisores.add(aux.getIdCorpSupervisor().trim());
                                }
                            } else {
                                boolean flagSup = false;
                                String supervisor = aux.getIdCorpSupervisor();
                                if (supervisor != null) {
                                    for (String sup : supervisores) {
                                        if (sup.equals(aux.getIdCorpSupervisor().trim())) {
                                            flagSup = true;
                                            break;
                                        }
                                    }
                                    if (flagSup == false) {
                                        supervisores.add(aux.getIdCorpSupervisor().trim());
                                    }
                                }
                            }
                        }

                    }
                }
            }

            if (arrIncidentesCerrados != null) {
                InfoIncidente[] arregloInc = arrIncidentesCerrados.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {
                        boolean flag = false;
                        for (String auxS : tickets) {
                            String idIncidentS = "" + aux.getIdIncidencia();
                            if (idIncidentS.compareTo(auxS) == 0) {
                                flag = true;
                                break;
                            }
                        }

                        if (flag == true) {
                            //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
                            if (aux.getEstado().contains("Recibido por atender")) {
                                RPA.add(aux);
                            }
                            if (aux.getEstado().contains("En Proceso")) {
                                EP.add(aux);
                            }
                            if (aux.getEstado().contains("En Recepción")) {
                                EPC.add(aux);
                            }
                            if (aux.getEstado().contains("Atendido")) {
                                Atendidos.add(aux);
                            }
                            if (aux.getEstado().contains("Pendiente")) {
                                PPA.add(aux);
                            }
                            if (supervisores.isEmpty()) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    supervisores.add(aux.getIdCorpSupervisor().trim());
                                }
                            } else {
                                boolean flagSup = false;
                                String supervisor = aux.getIdCorpSupervisor();
                                if (supervisor != null) {
                                    for (String sup : supervisores) {
                                        if (sup.equals(aux.getIdCorpSupervisor().trim())) {
                                            flagSup = true;
                                            break;
                                        }
                                    }
                                    if (flagSup == false) {
                                        supervisores.add(aux.getIdCorpSupervisor().trim());
                                    }
                                }
                            }
                        }

                    }
                }
            }

            // -------------------------------------------------------------------------------
            JsonArray supervisoresJsonArray = new JsonArray();
            if (!supervisores.isEmpty() && supervisores != null) {
                for (String sup : supervisores) {
                    JsonObject incJsonObj = new JsonObject();
                    if (!sup.matches("[^0-9]*")) {

                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(sup));
                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getNombre());
                                supervisoresJsonArray.add(incJsonObj);
                            }
                        }
                    } else {
                        System.out.println("No es un número");
                        incJsonObj.addProperty("SUPERVISOR", "JUAN PEREZ");
                        supervisoresJsonArray.add(incJsonObj);
                    }
                }
            }

            // --------------------------------------------------------------------------------
            JsonArray AtendidosJsonArray = new JsonArray();
            if (!Atendidos.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : Atendidos) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int j = 0; j < aprHilos.size(); j++) {
                    aprHilos.get(j).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                //logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int j = 0; j < aprHilos.size(); j++) {
                        if (aprHilos.get(j).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(j).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int j = 0; j < aprHilos.size(); j++) {
                    AprobacionesHashMap.put("" + aprHilos.get(j).getIdFolio(), aprHilos.get(j).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------

                AtendidosJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPCJsonArray = new JsonArray();
            if (!EPC.isEmpty() && RPA != null) {
                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EPC) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int j = 0; j < aprHilos.size(); j++) {
                    aprHilos.get(j).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int j = 0; j < aprHilos.size(); j++) {
                        if (aprHilos.get(j).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(j).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int j = 0; j < aprHilos.size(); j++) {
                    AprobacionesHashMap.put("" + aprHilos.get(j).getIdFolio(), aprHilos.get(j).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                EPCJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);
            }

            JsonArray EPJsonArray = new JsonArray();
            if (!EP.isEmpty() && RPA != null) {

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : EP) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int j = 0; j < aprHilos.size(); j++) {
                    aprHilos.get(j).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int j = 0; j < aprHilos.size(); j++) {
                        if (aprHilos.get(j).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(j).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int j = 0; j < aprHilos.size(); j++) {
                    AprobacionesHashMap.put("" + aprHilos.get(j).getIdFolio(), aprHilos.get(j).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                EPJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray RPAJsonArray = new JsonArray();
            if (!RPA.isEmpty() && RPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : RPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int j = 0; j < aprHilos.size(); j++) {
                    aprHilos.get(j).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int j = 0; j < aprHilos.size(); j++) {
                        if (aprHilos.get(j).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(j).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int j = 0; j < aprHilos.size(); j++) {
                    AprobacionesHashMap.put("" + aprHilos.get(j).getIdFolio(), aprHilos.get(j).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                RPAJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);

            }

            JsonArray PPAJsonArray = new JsonArray();
            if (!PPA.isEmpty() && PPA != null) {
                // ArrayList<InfoIncidente> ordenar=RPA;

                ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
                ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

                for (InfoIncidente aux : PPA) {
                    if (aux.getPrioridad().getValue().contains("Normal")) {
                        normal.add(aux);
                    }
                    if (aux.getPrioridad().getValue().contains("Critica")) {
                        Critica.add(aux);
                    }
                }
                //--------------Ejecuta hilos aprobaciones--------------------
                ArrayList<AprobacionesRemedyHilos> aprHilos = new ArrayList<AprobacionesRemedyHilos>();

                if (Critica != null && !Critica.isEmpty()) {
                    for (InfoIncidente aux : Critica) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                if (normal != null && !normal.isEmpty()) {
                    for (InfoIncidente aux : normal) {
                        aprHilos.add(new AprobacionesRemedyHilos(aux.getIdIncidencia(), 1, 2));
                    }
                }

                for (int j = 0; j < aprHilos.size(); j++) {
                    aprHilos.get(j).run();
                }

                boolean terminaron = false;

                long startTime1 = System.currentTimeMillis();
                logger.info("Entro a verificar hilos");
                if (aprHilos == null || aprHilos.isEmpty()) {
                    terminaron = true;
                }
                while (!terminaron) {
                    for (int j = 0; j < aprHilos.size(); j++) {
                        if (aprHilos.get(j).getRespuesta() != null) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                        if (!aprHilos.get(j).isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }
                }

                HashMap<String, Aprobacion> AprobacionesHashMap = new HashMap<String, Aprobacion>();
                for (int j = 0; j < aprHilos.size(); j++) {
                    AprobacionesHashMap.put("" + aprHilos.get(j).getIdFolio(), aprHilos.get(j).getRespuesta());
                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);
                //-------------------------------------------------------------
                PPAJsonArray = arrOrdenadoProveedor(Critica, normal, AprobacionesHashMap);

            }

            if (arrIncidentes == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", RPAJsonArray);
                envoltorioJsonObj.add("EN_PROCESO", EPJsonArray);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", EPCJsonArray);
                envoltorioJsonObj.add("ATENDIDOS", AtendidosJsonArray);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", PPAJsonArray);
                envoltorioJsonObj.add("SUPERVISORES", supervisoresJsonArray);
            }
            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/migestion/servicios/getInfCierreProveedor.json?idUsuario=<?>&idFolio=<?>&opcion=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getInfCierreProveedor", method = RequestMethod.GET)
    public @ResponseBody
    String getInfCierreProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idFolio = urides.split("&")[1].split("=")[1];

            int opc = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            // int numSucursal=Integer.parseInt(ceco.substring(2, 6));
            JsonObject envoltorioJsonObj = new JsonObject();
            String imagen = serviciosRemedyBI.ConsultaAdjunto(Integer.parseInt(idFolio));
            envoltorioJsonObj.addProperty("IMAGEN", imagen);

            ArrayOfInfoIncidente auxIncidente = serviciosRemedyBI.ConsultaDetalleFolio(Integer.parseInt(idFolio));
            int resp = 0;
            boolean flagAcepta = false;
            if (auxIncidente != null) {
                InfoIncidente[] arregloInc = auxIncidente.getInfoIncidente();
                if (arregloInc != null) {
                    for (InfoIncidente aux : arregloInc) {

                        if (aux.getAutorizacionCliente().trim().toLowerCase().equals("aceptado")) {
                            flagAcepta = true;
                        }

                        //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
                        String cord = null;
                        if (aux.getLatitudInicio() != null) {
                            envoltorioJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
                        } else {
                            envoltorioJsonObj.addProperty("LATITUD_INICIO", cord);
                        }

                        if (aux.getLongitudInicio() != null) {
                            envoltorioJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
                        } else {
                            envoltorioJsonObj.addProperty("LONGITUD_INICIO", cord);
                        }

                        if (aux.getLatitudCierre() != null) {
                            envoltorioJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
                        } else {
                            envoltorioJsonObj.addProperty("LATITUD_FIN", cord);
                        }

                        if (aux.getLongitudCierre() != null) {
                            envoltorioJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
                        } else {
                            envoltorioJsonObj.addProperty("LONGITUD_FIN", cord);
                        }

                        //----------------------------------------------------------------------------------
                        Date date = aux.getFechaCierre().getTime();
                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String date1 = format1.format(date);
                        envoltorioJsonObj.addProperty("FECHA_CIERRE_TECNICO", "" + date1);
                        envoltorioJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());

                        envoltorioJsonObj.addProperty("ACEPTA_CLIENTE", flagAcepta);

                        int cal = 0;
                        _Calificacion auxEval = aux.getEvalCliente();
                        if (auxEval != null) {
                            if (auxEval.getValue().contains("NA")) {
                                cal = 0;
                            }
                            if (auxEval.getValue().contains("Item1")) {
                                cal = 1;
                            }
                            if (auxEval.getValue().contains("Item2")) {
                                cal = 2;
                            }
                            if (auxEval.getValue().contains("Item3")) {
                                cal = 3;
                            }
                            if (auxEval.getValue().contains("Item4")) {
                                cal = 4;
                            }
                            if (auxEval.getValue().contains("Item5")) {
                                cal = 5;
                            }
                        }

                        envoltorioJsonObj.addProperty("CALIFICACION", cal);
                        resp = 1;
                    }

                }
            }

            if (opc == 1) {
                //------------------------Aprobacion Cierre Tecnico----------------------------------------
                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(Integer.parseInt(idFolio), 4);
                if (aprobacion != null) {
                    if (aprobacion.getA1_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO1_CIERRE", arch1);

                    } else {
                        // insJsonObj.addProperty("ARCHIVO_1",aprobacion.getA1_Base().toString());
                        InputStream in = aprobacion.getA1_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO1_CIERRE", encoded);
                    }
                    if (aprobacion.getA2_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO2_CIERRE", arch1);
                    } else {
                        // insJsonObj.addProperty("ARCHIVO_2",aprobacion.getA2_Base().toString());
                        InputStream in = aprobacion.getA2_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO2_CIERRE", encoded);
                    }
                    if (aprobacion.getEstadoAprobacion() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_CIERRE", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_CIERRE", aprobacion.getEstadoAprobacion());
                    }

                    if (aprobacion.getJustAprobador() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE", aprobacion.getJustAprobador());
                    }

                    if (aprobacion.getJustSolicitante() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_PROVEEDOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_PROVEEDOR", aprobacion.getJustSolicitante());
                    }

                    if (aprobacion.getSolicitadoPara() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("CLIENTE", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("CLIENTE", aprobacion.getSolicitadoPara());
                    }

                    if (aprobacion.getSolicitadoPor() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("PROVEEDOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("PROVEEDOR", aprobacion.getSolicitadoPor());
                    }

                    Date date = aprobacion.getFechaModificacion().getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date1 = format1.format(date);
                    envoltorioJsonObj.addProperty("FECHA_CIERRE", "" + date1);

                    String arch1 = null;
                    envoltorioJsonObj.addProperty("ARCHIVO1_CLIENTE", arch1);
                    envoltorioJsonObj.addProperty("ARCHIVO2_CLIENTE", arch1);
                    envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", arch1);
                    envoltorioJsonObj.addProperty("JUSTIFICACION_SUPERVISOR", arch1);
                    envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE", arch1);
                    envoltorioJsonObj.addProperty("SUPERVISOR", arch1);
                    envoltorioJsonObj.addProperty("CLIENTE2", arch1);
                }
            } else if (opc == 2) {
                //------------------------Aprobacion rechazo cliente
                Aprobacion aprobacionSpervisor = serviciosRemedyBI.consultarAprobaciones(Integer.parseInt(idFolio), 6);
                if (aprobacionSpervisor != null) {
                    if (aprobacionSpervisor.getA1_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO1_CLIENTE", arch1);

                    } else {
                        // insJsonObj.addProperty("ARCHIVO_1",aprobacion.getA1_Base().toString());
                        InputStream in = aprobacionSpervisor.getA1_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO1_CLIENTE", encoded);
                    }
                    if (aprobacionSpervisor.getA2_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO2_CLIENTE", arch1);
                    } else {
                        // insJsonObj.addProperty("ARCHIVO_2",aprobacion.getA2_Base().toString());
                        InputStream in = aprobacionSpervisor.getA2_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO2_CLIENTE", encoded);
                    }
                    if (aprobacionSpervisor.getEstadoAprobacion() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 0);
                    } else {
                        if (aprobacionSpervisor.getEstadoAprobacion().trim().toLowerCase().equals("aceptado")) {
                            envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 1);
                        } else if (aprobacionSpervisor.getEstadoAprobacion().trim().toLowerCase().equals("rechazado")) {
                            envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 2);
                        } else {
                            envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 0);
                        }
                    }
                    if (aprobacionSpervisor.getJustAprobador() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_SUPERVISOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_SUPERVISOR", aprobacionSpervisor.getJustAprobador());
                    }

                    if (aprobacionSpervisor.getJustSolicitante() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE2", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE2", aprobacionSpervisor.getJustSolicitante());
                    }

                    if (aprobacionSpervisor.getSolicitadoPara() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("SUPERVISOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("SUPERVISOR", aprobacionSpervisor.getSolicitadoPara());
                    }

                    if (aprobacionSpervisor.getSolicitadoPor() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("CLIENTE2", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("CLIENTE2", aprobacionSpervisor.getSolicitadoPor());
                    }

                    Date date = aprobacionSpervisor.getFechaModificacion().getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date1 = format1.format(date);
                    envoltorioJsonObj.addProperty("FECHA_ACEPTA_CLIENTE", "" + date1);

                    String arch1 = null;
                    envoltorioJsonObj.addProperty("ARCHIVO1_CIERRE", arch1);
                    envoltorioJsonObj.addProperty("ARCHIVO2_CIERRE", arch1);
                    envoltorioJsonObj.addProperty("ESTADO_APROBACION_CIERRE", arch1);
                    envoltorioJsonObj.addProperty("JUSTIFICACION_APROBADOR", arch1);
                    envoltorioJsonObj.addProperty("JUSTIFICACION_SOLICITANTE", arch1);
                    envoltorioJsonObj.addProperty("CLIENTE", arch1);
                    envoltorioJsonObj.addProperty("PROVEEDOR", arch1);
                    envoltorioJsonObj.addProperty("FECHA_CIERRE", arch1);
                }
            } else {
                //------------------------Aprobacion Cierre Tecnico
                Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(Integer.parseInt(idFolio), 4);
                if (aprobacion != null) {
                    if (aprobacion.getA1_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO1_CIERRE", arch1);

                    } else {
                        // insJsonObj.addProperty("ARCHIVO_1",aprobacion.getA1_Base().toString());
                        InputStream in = aprobacion.getA1_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO1_CIERRE", encoded);
                    }
                    if (aprobacion.getA2_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO2_CIERRE", arch1);
                    } else {
                        // insJsonObj.addProperty("ARCHIVO_2",aprobacion.getA2_Base().toString());
                        InputStream in = aprobacion.getA2_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO2_CIERRE", encoded);
                    }
                    if (aprobacion.getEstadoAprobacion() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_CIERRE", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_CIERRE", aprobacion.getEstadoAprobacion());
                    }

                    if (aprobacion.getJustAprobador() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE", aprobacion.getJustAprobador());
                    }

                    if (aprobacion.getJustSolicitante() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_PROVEEDOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_PROVEEDOR", aprobacion.getJustSolicitante());
                    }

                    if (aprobacion.getSolicitadoPara() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("CLIENTE", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("CLIENTE", aprobacion.getSolicitadoPara());
                    }

                    if (aprobacion.getSolicitadoPor() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("PROVEEDOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("PROVEEDOR", aprobacion.getSolicitadoPor());
                    }

                    Date date = aprobacion.getFechaModificacion().getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date1 = format1.format(date);
                    envoltorioJsonObj.addProperty("FECHA_CIERRE", "" + date1);

                }
                //------------------------Aprobacion rechazo cliente
                Aprobacion aprobacionSpervisor = serviciosRemedyBI.consultarAprobaciones(Integer.parseInt(idFolio), 6);
                if (aprobacionSpervisor != null) {
                    if (aprobacionSpervisor.getA1_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO1_CLIENTE", arch1);

                    } else {
                        // insJsonObj.addProperty("ARCHIVO_1",aprobacion.getA1_Base().toString());
                        InputStream in = aprobacionSpervisor.getA1_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO1_CLIENTE", encoded);
                    }
                    if (aprobacionSpervisor.getA2_Base() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ARCHIVO2_CLIENTE", arch1);
                    } else {
                        // insJsonObj.addProperty("ARCHIVO_2",aprobacion.getA2_Base().toString());
                        InputStream in = aprobacionSpervisor.getA2_Base().getInputStream();
                        byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                        Base64.Encoder codec = Base64.getEncoder();
                        // String encoded = Base64.encodeBase64String(byteArray);
                        String encoded = new String(codec.encode(byteArray), "UTF-8");
                        envoltorioJsonObj.addProperty("ARCHIVO2_CLIENTE", encoded);
                    }
                    if (aprobacionSpervisor.getEstadoAprobacion() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 0);
                    } else {
                        if (aprobacionSpervisor.getEstadoAprobacion().trim().toLowerCase().equals("aceptado")) {
                            envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 1);
                        } else if (aprobacionSpervisor.getEstadoAprobacion().trim().toLowerCase().equals("rechazado")) {
                            envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 2);
                        } else {
                            envoltorioJsonObj.addProperty("ESTADO_APROBACION_CLIENTE", 0);
                        }
                    }
                    if (aprobacionSpervisor.getJustAprobador() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_SUPERVISOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_SUPERVISOR", aprobacionSpervisor.getJustAprobador());
                    }

                    if (aprobacionSpervisor.getJustSolicitante() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE2", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_CLIENTE2", aprobacionSpervisor.getJustSolicitante());
                    }

                    if (aprobacionSpervisor.getSolicitadoPara() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("SUPERVISOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("SUPERVISOR", aprobacionSpervisor.getSolicitadoPara());
                    }

                    if (aprobacionSpervisor.getSolicitadoPor() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("CLIENTE2", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("CLIENTE2", aprobacionSpervisor.getSolicitadoPor());
                    }

                    Date date = aprobacionSpervisor.getFechaModificacion().getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date1 = format1.format(date);
                    envoltorioJsonObj.addProperty("FECHA_ACEPTA_CLIENTE", "" + date1);
                }

                //--------------------------------Obtiene aprobacion de autorizacion de monto-------------------------
                //------------------------Aprobacion rechazo cliente
                Aprobacion aprobacionMontoI = serviciosRemedyBI.consultarAprobaciones(Integer.parseInt(idFolio), 5);
                if (aprobacionSpervisor != null) {

                    if (aprobacionMontoI.getEstadoAprobacion() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_MONTO", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("ESTADO_APROBACION_MONTO", aprobacionMontoI.getEstadoAprobacion());

                    }
                    if (aprobacionMontoI.getJustAprobador() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_MONTO_COORDINADOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_MONTO_COORDINADOR", aprobacionMontoI.getJustAprobador());
                    }

                    if (aprobacionMontoI.getJustSolicitante() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("JUSTIFICACION_MONTO_SUPERVISOR", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("JUSTIFICACION_MONTO_SUPERVISOR", aprobacionMontoI.getJustSolicitante());
                    }

                    if (aprobacionMontoI.getSolicitadoPara() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("COORDINADOR_MONTO", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("COORDINADOR_MONTO", aprobacionMontoI.getSolicitadoPara());
                    }

                    if (aprobacionMontoI.getSolicitadoPor() == null) {
                        String arch1 = null;
                        envoltorioJsonObj.addProperty("SUPERVISOR_MONTO", arch1);
                    } else {
                        envoltorioJsonObj.addProperty("SUPERVISOR_MONTO", aprobacionMontoI.getSolicitadoPor());
                    }

                    Date date = aprobacionMontoI.getFechaModificacion().getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date1 = format1.format(date);
                    envoltorioJsonObj.addProperty("FECHA_MONTO_NO_AUTORIZADO", "" + date1);
                }

            }

            //envoltorioJsonObj.add("APROBACION", insJsonObj);
            ////logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    /*
     * ****************************** METODOS PARA EL REPLICADO DE IMAGENES
     * ******************************
     */
    // TRUE CUANDO ESTOY EN checklist
    public boolean verificaServidor(String ipActual) {
        boolean resp = false;
        String[] ipFrq = {"10.53.33.74", "10.53.33.75", "10.53.33.76", "10.53.33.77"};

        for (String data : ipFrq) {
            if (data.contains(ipActual)) {
                return true;
            }
        }
        return resp;
    }

    public String codificarBase64(byte[] cadena) throws UnsupportedEncodingException {
        return new String(Base64.getEncoder().encode(cadena), "UTF-8");
    }

    // http://localhost:8080/migestion/servicios/getArchivoBase64.json?idUsuario=<?>&idEvidencia=<?>&nomArchivo=<?>&ruta=<?>&bandera=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getArchivoBase64", method = RequestMethod.GET)
    public @ResponseBody
    String getArchivoBase64(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idEvidencia = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String nomArchivo = urides.split("&")[2].split("=")[1];
            String ruta = urides.split("&")[3].split("=")[1];
            String bandera = urides.split("&")[4].split("=")[1];

            String rutafotoA = ruta;
            if (rutafotoA != null) {
                if (!rutafotoA.contains("franquicia")) {
                    rutafotoA = null;
                }
            } else {
                rutafotoA = null;
            }

            String encodedA = null;
            if (rutafotoA != null) {

                try {
                    Path pdfPath = Paths.get(rutafotoA);
                    byte[] pdf = Files.readAllBytes(pdfPath);
                    encodedA = new String(Base64.getEncoder().encode(pdf), "UTF-8");
                } catch (Exception e) {
                    logger.info("No existe la imagen en la ruta: " + rutafotoA);
                    rutafotoA = null;
                }

            }

            JsonObject envoltorioJsonObj = new JsonObject();

            envoltorioJsonObj.addProperty("ARCHIVO", encodedA);
            envoltorioJsonObj.addProperty("NOM_ARCHIVO", nomArchivo);

            //logger.info("JSON Archivo: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // --------------------------------------------------------------- ----
    // *
    // Terminan Servicios Carpeta Maestra
    // *
    // ---------------------------------------------------------------------
    // http://localhost:8080/migestion/servicios/validaFirmaAzteca.json?id=<?>&usuarioAdmin=1&firma=<?>&key=1
    @SuppressWarnings({"static-access", "unchecked"})
    @RequestMapping(value = "/getUrlSIE", method = RequestMethod.GET)
    public @ResponseBody
    String getUrlSIE(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String json = "";
        Gson g = new GsonBuilder().serializeNulls().create();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];

            json = siePortalBI.getUrlsJson(idUsuario);

        } catch (Exception e) {
            logger.info("Ocurrio algo ");
            logger.info(e);
            e.printStackTrace();
            json = "";

        }

        return json;
    }

    // Servicios Captacion
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCaptacionSaldosSemana", method = RequestMethod.GET)

    public @ResponseBody
    String getCaptacionSaldosSemana(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        JsonObject xml = null;

        JsonObject jsonGeneral = new JsonObject();

        JsonArray detalles = new JsonArray();

        JsonArray indicadores = new JsonArray();

        JsonObject principal = new JsonObject();

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();

            StrCipher cifraIOS = new StrCipher();

            String uri = request.getQueryString();

            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {

                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));

            } else if (idLlave == 666) {

                urides = (cifra.decryptParams(uri));

            }

            String fecha = urides.split("&")[1].split("=")[1];

            String geografia = urides.split("&")[2].split("=")[1].replace("%20", " ");

            String nivel = urides.split("&")[3].split("=")[1];

            ExtraccionFinancieraThreadBI saldoPlazo = new ExtraccionFinancieraThreadBI(4, fecha, geografia, null);

            ExtraccionFinancieraThreadBI saldoVista = new ExtraccionFinancieraThreadBI(5, fecha, geografia, nivel);

            long inicio = System.currentTimeMillis();

            logger.info("INICIA EJECUCION DE HILOS " + new Date());

            saldoPlazo.start();

            saldoVista.start();

            boolean termino = false;

            // Se ejecuta while para saber cuando
            while (saldoPlazo.isAlive() || saldoVista.isAlive()) {

                /* Espera hasta que los hilos hayan terminado */
            }

            long acabo = System.currentTimeMillis();

            long totaltiempo = (acabo - inicio) / 1000;

            logger.info("TERMINARON LOS HILOS EN " + totaltiempo + " SEGUNDOS");

            double compromiso = 0.0;

            double miAvance = 0.0;

            double nosFalta = 0.0;

            double porcentaje = 0.0;

            JsonObject indicador = null;

            if (saldoPlazo.getRespuesta() != null) {

                JsonArray totales = saldoPlazo.getRespuesta().get("saldoCaptacionPlazo").getAsJsonObject()
                        .get("totales").getAsJsonArray();

                // Sacar posiciones 2(Compromiso) y 3(Real)
                if (totales != null) {

                    indicador = new JsonObject();

                    compromiso = Double.parseDouble(
                            totales.get(2).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    miAvance = Double.parseDouble(
                            totales.get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    nosFalta = compromiso - miAvance;

                    porcentaje = (100.0 * miAvance) / compromiso;

                    indicador.addProperty("compromiso", Math.round(compromiso));

                    indicador.addProperty("miAvance", Math.round(miAvance));

                    indicador.addProperty("nosFalta", Math.round(nosFalta));

                    indicador.addProperty("porcentaje", Math.round(porcentaje));

                }

                principal.add("saldoPlazo", indicador);

            }

            compromiso = 0.0;

            miAvance = 0.0;

            nosFalta = 0.0;

            porcentaje = 0.0;

            if (saldoVista.getRespuesta() != null) {

                JsonArray totales = saldoVista.getRespuesta().get("saldoCaptacionVista").getAsJsonObject()
                        .get("totales").getAsJsonArray();

                // Sacar posiciones 3(Compromiso) y 4(Real)
                if (totales != null) {

                    indicador = new JsonObject();

                    compromiso = Double.parseDouble(
                            totales.get(2).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    miAvance = Double.parseDouble(
                            totales.get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    nosFalta = compromiso - miAvance;

                    porcentaje = (100.0 * miAvance) / compromiso;

                    indicador.addProperty("compromiso", Math.round(compromiso));

                    indicador.addProperty("miAvance", Math.round(miAvance));

                    indicador.addProperty("nosFalta", Math.round(nosFalta));

                    indicador.addProperty("porcentaje", Math.round(porcentaje));

                }

                principal.add("saldoVista", indicador);

            }

        } catch (Exception e) {

            logger.info("Ocurrio algo getCaptacionSaldosSemana " + e.getMessage());

            e.printStackTrace();

            String json = "{" + "saldoPlazo: {" + "compromiso: 0," + "miAvance: 0," + "nosFalta: 0," + "porcentaje: 0"
                    + "}," + "saldoVista: {" + "compromiso: 0," + "miAvance: 0," + "nosFalta: 0," + "porcentaje: 0"
                    + "}" + "}";

            return json;

        }

        return principal.toString();

    }

    //http://localhost:8080/migestion/servicios/getCaptacionHijos.json?idUsuario=<?>&fecha=<?>&geografia=<?>&nivel=<?>&negocio=<?>
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCaptacionHijos", method = RequestMethod.GET)
    public @ResponseBody
    String getCaptacionHijos(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        JsonArray listaResult = new JsonArray();

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {

                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));

            } else if (idLlave == 666) {

                urides = (cifra.decryptParams(uri));

            }

            String fecha = urides.split("&")[1].split("=")[1];
            String geografia = urides.split("&")[2].split("=")[1].replace("%20", " ");
            String nivel = urides.split("&")[3].split("=")[1];
            String negocio = urides.split("&")[4].split("=")[1];

            List<CaptacionThread> arrayThreads = new ArrayList<CaptacionThread>();

            String ceco = geografia.substring(0, 6);

            List<CecoDTO> lista = cecoBi.buscaCecosSuperior(ceco, negocio);

            for (CecoDTO cecoDTO : lista) {

                String idCeco = cecoDTO.getIdCeco();
                String nomCeco = cecoDTO.getDescCeco();

                logger.info(cecoDTO.getIdCeco());
                logger.info(cecoDTO.getDescCeco());

                if (cecoDTO.getDescCeco().endsWith(" ")) {
                    logger.info("TIENE ESPACIO");
                    logger.info(cecoDTO.getDescCeco());
                    nomCeco = (cecoDTO.getDescCeco().substring(0, cecoDTO.getDescCeco().length() - 1));
                }

                String geografiaHijo = idCeco + " - " + nomCeco;

                /*String nivelHijo = "";

                 switch (nivel) {
                 case "TR":
                 nivelHijo = "Z";
                 break;
                 case "Z":
                 nivelHijo = "R";
                 break;
                 case "R":
                 nivelHijo = "T";
                 break;
                 default:
                 return new JsonArray().toString();
                 }*/
                CaptacionThread hilo = new CaptacionThread(fecha, geografiaHijo, nivel);

                arrayThreads.add(hilo);
                hilo.start();
            }
            /*
             * Ciclo que servira para verificar si todo los hilos ya terminaron
             */
            boolean terminaron = false;
            List<CaptacionThread> aElminar = new ArrayList<CaptacionThread>();
            while (!terminaron) {
                Iterator<CaptacionThread> it = arrayThreads.iterator();
                int cont = 0;

                // System.out.println("Contador :"+cont);
                if (cont == arrayThreads.size()) {
                    terminaron = true;
                }
                while (it.hasNext()) {
                    CaptacionThread hilo = it.next();
                    if (hilo.getRespuestaHilo() != null) {

                        listaResult.add(hilo.getRespuestaHilo());
                        aElminar.add(hilo);
                        cont++;
                    }
                }
                for (CaptacionThread eliminarHilo : aElminar) {
                    arrayThreads.remove(eliminarHilo);
                }
                aElminar.clear();
            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
        }

        return listaResult.toString();

    }

    //http://localhost:8080/migestion/servicios/getBandejaTickets.json?idUsuario=<?>&idUsuarioBandeja=<?>&tipoUsuario=<?>
    /*@SuppressWarnings("static-access")
    @RequestMapping(value = "/getBandejaTickets", method = RequestMethod.GET)
    public @ResponseBody
    String getBandejaTickets(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            if (urides.split("&").length == 2) {

                String idUsuario = urides.split("&")[0].split("=")[1];
                String tipoUsuario = urides.split("&")[1].split("=")[1];

                res = ticketManrenimientoBI.getBandejaTickets(idUsuario, Integer.parseInt(tipoUsuario));
            } else {
                String idUsuario = urides.split("&")[0].split("=")[1];
                //String razonSocial = urides.split("&")[1].split("=")[1];
                String idProveedor = urides.split("&")[1].split("=")[1];
                String tipoUsuario = urides.split("&")[2].split("=")[1];


                //Implementa buscar el idProveedor
                List<ProveedoresDTO> proveedoresArray = proveedoresBI.obtieneDatos(Integer.parseInt(idProveedor));
                ProveedoresDTO proveedorDTO= null;

                if(proveedoresArray != null && proveedoresArray.size() > 0) {

                    proveedorDTO =  proveedoresArray.get(0);
                    res = ticketManrenimientoBI.getBandejaTickets(proveedorDTO.getRazonSocial(), Integer.parseInt(tipoUsuario));

                }else {
                    res = "{}";
                }


                //res = ticketManrenimientoBI.getBandejaTickets(razonSocial, Integer.parseInt(tipoUsuario));
            }

        } catch (Exception e) {
            res = "{}";
        }

        return res;

    }*/

 /*//http://localhost:8080/migestion/servicios/getBandejaCuadrillaNew.json?idUsuario=<?>&idUsuarioBandeja=<?>&cuadrilla=<?>&zona=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBandejaCuadrillaNew", method = RequestMethod.GET)
    public @ResponseBody
    String getBandejaCuadrillaNew(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        int idLlave=0;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = urides.split("&")[0].split("=")[1];
            String proveedor = urides.split("&")[1].split("=")[1];
            String cuadrilla = urides.split("&")[2].split("=")[1];
            String zona = urides.split("&")[3].split("=")[1];

            res = ticketManrenimientoBI.getBandejaCuadrilla(proveedor, Integer.parseInt(cuadrilla), Integer.parseInt(zona));

        } catch (Exception e) {
            res = "{}";
        }

        return UtilGTN.encrypResult(idLlave, res);

    }*/
    //http://localhost:8080/migestion/servicios/getCaptaciones.json?idUsuario=<?>&fecha=<?>&geografia=<?>&nivel=<?>&tipoCaptacion=<?>&tipoSaldo=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCaptaciones", method = RequestMethod.GET)
    public @ResponseBody
    String getCaptaciones(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        String res = null;

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String fecha = urides.split("&")[1].split("=")[1];
            String geografia = urides.split("&")[2].split("=")[1].replaceAll("%20", " ");
            String nivel = urides.split("&")[3].split("=")[1];
            String tipoCaptacion = urides.split("&")[4].split("=")[1];
            String tipoSaldo = urides.split("&")[5].split("=")[1];

            res = indicadoresBI.getCaptaciones(fecha, geografia, nivel, tipoSaldo, Integer.parseInt(tipoCaptacion)).toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            res = "{}";
        }

        return res;

    }

    // http://localhost:8080/migestion/servicios/getIndicadoresCaptacionThreads.json?idUsuario=<?>&fecha=<?>&geografia=<?>&nivel=<?>&tipo=<?>
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getIndicadoresCaptacionThreads", method = RequestMethod.GET)
    public @ResponseBody
    String getIndicadoresCaptacionThreads(HttpServletRequest request, HttpServletResponse response,
            Model model) throws KeyException, GeneralSecurityException, IOException {

        JsonObject xml = null;
        DatosUsuarioDTO datosUsuarioDTO = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        JsonObject jsonGeneral = new JsonObject();
        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();

        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String fecha = urides.split("&")[1].split("=")[1];
            String geografia = urides.split("&")[2].split("=")[1].replace("%20", " ");
            String nivel = urides.split("&")[3].split("=")[1];

            String tipo = urides.split("&").length == 5 ? urides.split("&")[4].split("=")[1] : "0";

            if (tipo.equals("0")) {

                //Se impleto para terminar de momento con los errores en monitoreo, mientras se trabaja con la reingenieria de SIE
                ParametroBI parametro = (ParametroBI) GTNAppContextProvider.getApplicationContext().getBean("parametroBI");
                List<ParametroDTO> parametros = parametro.obtieneParametros("consumeSIEPilar4CyC");
                String valor = parametros.get(0).getValor();

                if (valor.equals("0")) {
                    return null;
                }

                ExtraccionCaptacionThreadBI captacionVistaSF = new ExtraccionCaptacionThreadBI(1, fecha, geografia,
                        nivel);
                ExtraccionCaptacionThreadBI captacionVistaCN = new ExtraccionCaptacionThreadBI(2, fecha, geografia,
                        nivel);
                ExtraccionCaptacionThreadBI captacionVistaNA = new ExtraccionCaptacionThreadBI(3, fecha, geografia,
                        nivel);
                ExtraccionCaptacionThreadBI captacionVistaMA = new ExtraccionCaptacionThreadBI(4, fecha, geografia,
                        nivel);

                ExtraccionCaptacionThreadBI captacionPlazoSF = new ExtraccionCaptacionThreadBI(5, fecha, geografia,
                        nivel);
                ExtraccionCaptacionThreadBI captacionPlazoCN = new ExtraccionCaptacionThreadBI(6, fecha, geografia,
                        nivel);
                ExtraccionCaptacionThreadBI captacionPlazoNA = new ExtraccionCaptacionThreadBI(7, fecha, geografia,
                        nivel);
                ExtraccionCaptacionThreadBI captacionPlazoMA = new ExtraccionCaptacionThreadBI(8, fecha, geografia,
                        nivel);

                ExtraccionCaptacionThreadBI transferenciasDEX = new ExtraccionCaptacionThreadBI(9, fecha, geografia,
                        nivel);

                long inicio = System.currentTimeMillis();
                logger.info("INICIA EJECUCION DE HILOS " + new Date());

                captacionVistaSF.start();
                captacionVistaCN.start();
                captacionVistaNA.start();
                captacionVistaMA.start();

                captacionPlazoSF.start();
                captacionPlazoCN.start();
                captacionPlazoNA.start();
                captacionPlazoMA.start();

                transferenciasDEX.start();

                boolean termino = false;

                // Se ejecuta while para saber cuando
                while (captacionVistaSF.isAlive() || captacionVistaCN.isAlive() || captacionVistaNA.isAlive()
                        || captacionVistaMA.isAlive() || captacionPlazoSF.isAlive() || captacionPlazoCN.isAlive()
                        || captacionPlazoNA.isAlive() || captacionPlazoMA.isAlive() || transferenciasDEX.isAlive()) {

                    /* Espera hasta que los hilos hayan terminado */
                }

                long acabo = System.currentTimeMillis();
                long totaltiempo = (acabo - inicio) / 1000;

                logger.info("TERMINARON TODOS LOS HILOS EN " + totaltiempo + " SEGUNDOS");

                if (captacionVistaSF.getRespuesta() != null) {

                    JsonArray totales = captacionVistaSF.getRespuesta().getAsJsonArray("captacionVista_SF").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "1");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    // jsonObject.addProperty("idService", "Consumo");
                    jsonObject.addProperty("idService", "1");
                    jsonObject.addProperty("codigo", "VSF");
                    jsonObject.add("detalle",
                            captacionVistaSF.getRespuesta().get("captacionVista_SF").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (captacionVistaCN.getRespuesta() != null) {

                    JsonArray totales = captacionVistaCN.getRespuesta().getAsJsonArray("captacionVista_CN").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "2");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("idService", "2");
                    jsonObject.addProperty("codigo", "VCN");
                    jsonObject.add("detalle",
                            captacionVistaCN.getRespuesta().get("captacionVista_CN").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (captacionVistaNA.getRespuesta() != null) {

                    JsonArray totales = captacionVistaNA.getRespuesta().getAsJsonArray("captacionVista_NA").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "3");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("idService", "3");
                    jsonObject.addProperty("codigo", "VNA");
                    jsonObject.add("detalle",
                            captacionVistaNA.getRespuesta().get("captacionVista_NA").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (captacionVistaMA.getRespuesta() != null) {

                    JsonArray totales = captacionVistaMA.getRespuesta().getAsJsonArray("captacionVista_MA").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "4");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("idService", "4");
                    jsonObject.addProperty("codigo", "VMA");
                    jsonObject.add("detalle",
                            captacionVistaMA.getRespuesta().get("captacionVista_MA").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (captacionPlazoSF.getRespuesta() != null) {

                    JsonArray totales = captacionPlazoSF.getRespuesta().getAsJsonArray("captacionPlazo_SF").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "5");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    // jsonObject.addProperty("idService", "Consumo");
                    jsonObject.addProperty("idService", "5");
                    jsonObject.addProperty("codigo", "PSF");
                    jsonObject.add("detalle",
                            captacionPlazoSF.getRespuesta().get("captacionPlazo_SF").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (captacionPlazoCN.getRespuesta() != null) {

                    JsonArray totales = captacionPlazoCN.getRespuesta().getAsJsonArray("captacionPlazo_CN").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "6");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("idService", "6");
                    jsonObject.addProperty("codigo", "PCN");
                    jsonObject.add("detalle",
                            captacionPlazoCN.getRespuesta().get("captacionPlazo_CN").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (captacionPlazoNA.getRespuesta() != null) {

                    JsonArray totales = captacionPlazoNA.getRespuesta().getAsJsonArray("captacionPlazo_NA").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "7");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("idService", "7");
                    jsonObject.addProperty("codigo", "PNA");
                    jsonObject.add("detalle",
                            captacionPlazoNA.getRespuesta().get("captacionPlazo_NA").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (captacionPlazoMA.getRespuesta() != null) {

                    JsonArray totales = captacionPlazoMA.getRespuesta().getAsJsonArray("captacionPlazo_MA").get(7)
                            .getAsJsonArray();

                    String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Consumo");
                    total.addProperty("idService", "8");

                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("idService", "8");
                    jsonObject.addProperty("codigo", "PMA");
                    jsonObject.add("detalle",
                            captacionPlazoMA.getRespuesta().get("captacionPlazo_MA").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                if (transferenciasDEX.getRespuesta() != null) {

                    JsonArray totales = transferenciasDEX.getRespuesta().get("transferenciasDEX").getAsJsonArray()
                            .get(7).getAsJsonArray();

                    String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                    String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                    JsonObject total = new JsonObject();
                    // total.addProperty("idService", "Dinero Express");
                    total.addProperty("idService", "9");

                    // total.addProperty("indicador", color.equals("rojo") ? "-" +
                    // indicador : indicador);
                    // total.addProperty("color", color);
                    total.addProperty("indicador", indicador);
                    total.addProperty("color", color);

                    indicadores.add(total);
                    JsonObject jsonObject = new JsonObject();
                    // jsonObject.addProperty("idService", "Dinero Express");
                    jsonObject.addProperty("idService", "9");
                    jsonObject.addProperty("codigo", "DEX");
                    jsonObject.add("detalle",
                            transferenciasDEX.getRespuesta().get("transferenciasDEX").getAsJsonArray());

                    detalles.add(jsonObject);

                }

                jsonGeneral.add("indicadores", indicadores);
                jsonGeneral.add("detalles", detalles);

                return jsonGeneral.toString();

            } else {

                //Se impleto para terminar de momento con los errores en monitoreo, mientras se trabaja con la reingenieria de SIE
                ParametroBI parametro = (ParametroBI) GTNAppContextProvider.getApplicationContext().getBean("parametroBI");
                List<ParametroDTO> parametros = parametro.obtieneParametros("consumeSIEPilar4CyC");
                String valor = parametros.get(0).getValor();
                Boolean ejecutaServicios = true;
                if (valor.equals("0")) {
                    int idService = Integer.parseInt(tipo);
                    GTNConstantes.rubrosNegocio rubro = GTNConstantes.rubrosNegocio.getType(idService);
                    switch (rubro) {
                        case captacionSaldoFinalVista:
                            ejecutaServicios = true;
                            break;
                        case captacionNetaVista:
                            ejecutaServicios = true;
                            break;
                        case captacionSaldoFinalPlazo:
                            ejecutaServicios = true;
                            break;
                        case captacionNetaPlazo:
                            ejecutaServicios = true;
                        default:
                            ejecutaServicios = false;
                    }

                }

                if (!ejecutaServicios) {
                    return null;
                }

                // Invoca nuevo servicio
                IndicadoresNegocioCapBI indicadorNegocio = new IndicadoresNegocioCapBI();
                System.out.println("tipo: " + tipo);
                JsonObject respuestaIndicador = indicadorNegocio.getResultadoIndicador(Integer.parseInt(tipo), fecha, geografia, nivel);

                String responseJson = respuestaIndicador.toString();
                return responseJson;

            }

        } catch (Exception e) {
            logger.info("Ocurrio algo en el servicio getIndicadores() ");
            logger.info(e);
            e.printStackTrace();
            return null;
        }

    }

    /*

    // http://localhost:8080/migestion/servicios/getCaptacionByIndicador.json?idUsuario=<?>&fecha=<?>&geografia=<?>&nivel=<?>&negocio=<?>&idIndicador=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCaptacionByIndicador", method = RequestMethod.GET)
    public @ResponseBody
    String getCaptacionByIndicador(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        String responseJson = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = urides.split("&")[0].split("=")[1];
            String fecha = urides.split("&")[1].split("=")[1];
            String geografia = urides.split("&")[2].split("=")[1].replace("%20", " ");
            String nivel = urides.split("&")[3].split("=")[1];
            String negocio = urides.split("&")[4].split("=")[1];
            String indicador = urides.split("&")[5].split("=")[1];

            //Debido a que es CyC el negocio debe ser 96 y para captación 41
            int indicadorInt = Integer.parseInt(indicador);
            if (indicadorInt < 25) {
                negocio = "96";
            }

            /*
             Debido a que se incluyeron los indicadores de captación con los de CyC,
             los id´s de captación se enumeran a partir del 25,
             sin embargo en apps con versiones anteriores consideran numeración del 1 al 9,
             es por eso que se considera el siguiente switch
             * */
 /*     if (Integer.parseInt(indicador) < 10) {
                switch (Integer.parseInt(indicador)) {
                    case 1:
                        indicador = rubrosNegocio.captacionSaldoFinalVista.getValue() + "";
                        break;
                    case 2:
                        indicador = rubrosNegocio.captacionNetaVista.getValue() + "";
                        break;
                    case 3:
                        indicador = rubrosNegocio.numAperturasClientesNuevosVista.getValue() + "";
                        break;
                    case 4:
                        indicador = rubrosNegocio.montAperturasClientesNuevosVista.getValue() + "";
                        break;
                    case 5:
                        indicador = rubrosNegocio.captacionSaldoFinalPlazo.getValue() + "";
                        break;
                    case 6:
                        indicador = rubrosNegocio.captacionNetaPlazo.getValue() + "";
                        break;
                    case 7:
                        indicador = rubrosNegocio.numAperturasClientesNuevosPlazo.getValue() + "";
                        break;
                    case 8:
                        indicador = rubrosNegocio.montAperturasClientesNuevosPlazo.getValue() + "";
                        break;
                    case 9:
                        indicador = rubrosNegocio.transferenciasDEX.getValue() + "";
                        break;
                    default:
                        break;
                }
            }
            //JsonObject respuestaIndicador2 = indicadoresTotalesCaptacionBI.getJsonFinal(fecha, geografia, nivel, negocio, Integer.parseInt(indicador));
            IndicadoresNegocioCapBI indicadorNegocio = new IndicadoresNegocioCapBI();
            JsonObject respuestaIndicador = indicadorNegocio.getTotalesPorIndicador(Integer.parseInt(indicador), fecha, geografia, nivel, negocio);
            responseJson = respuestaIndicador.toString();
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
        }
        return responseJson;

    }*/
    // http://localhost:8080/migestion/servicios/getTablaSemanalCaptacion.json?idUsuario=<?>&fecha=<?>&geografia=<?>&nivel=<?>&negocio=<?>&idIndicador=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTablaSemanalCaptacion", method = RequestMethod.GET)
    public @ResponseBody
    String getTablaSemanalCaptacion(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        String responseJson = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String fecha = urides.split("&")[1].split("=")[1];
            String geografia = urides.split("&")[2].split("=")[1].replace("%20", " ");
            String nivel = urides.split("&")[3].split("=")[1];
            String negocio = urides.split("&")[4].split("=")[1];
            String indicador = urides.split("&")[5].split("=")[1];

            //Debido a que es CyC el negocio debe ser 96 y para captación 41
            int indicadorInt = Integer.parseInt(indicador);
            if (indicadorInt < 25) {
                negocio = "96";
            }

            /*
             Debido a que se incluyeron los indicadores de captación con los de CyC,
             los id´s de captación se enumeran a partir del 25,
             sin embargo en apps con versiones anteriores consideran numeración del 1 al 9,
             es por eso que se considera el siguiente switch
             * */
            if (Integer.parseInt(indicador) < 10) {

                switch (Integer.parseInt(indicador)) {
                    case 1:
                        indicador = rubrosNegocio.captacionSaldoFinalVista.getValue() + "";
                        break;
                    case 2:
                        indicador = rubrosNegocio.captacionNetaVista.getValue() + "";
                        break;

                    case 3:
                        indicador = rubrosNegocio.numAperturasClientesNuevosVista.getValue() + "";
                        break;

                    case 4:
                        indicador = rubrosNegocio.montAperturasClientesNuevosVista.getValue() + "";
                        break;

                    case 5:
                        indicador = rubrosNegocio.captacionSaldoFinalPlazo.getValue() + "";
                        break;

                    case 6:
                        indicador = rubrosNegocio.captacionNetaPlazo.getValue() + "";
                        break;

                    case 7:
                        indicador = rubrosNegocio.numAperturasClientesNuevosPlazo.getValue() + "";
                        break;

                    case 8:
                        indicador = rubrosNegocio.montAperturasClientesNuevosPlazo.getValue() + "";
                        break;

                    case 9:
                        indicador = rubrosNegocio.transferenciasDEX.getValue() + "";
                        break;

                    default:
                        break;
                }
            }
            //res = indicadoresTotalesCaptacionBI.getTablaSemana(fecha, geografia, nivel, negocio, Integer.parseInt(indicador));
            IndicadoresNegocioCapBI indicadorNegocio = new IndicadoresNegocioCapBI();
            JsonObject respuestaIndicador = indicadorNegocio.getResultadoIndicador(Integer.parseInt(indicador), fecha, geografia, nivel);
            responseJson = respuestaIndicador.toString();
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
        }
        return responseJson;
    }

    // http://localhost:8080/migestion/servicios/getDetalleIncidenteClick.json?idUsuario=<?>&idTicket=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleIncidenteClick", method = RequestMethod.GET)
    public @ResponseBody
    String getDetalleIncidenteClick(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        JsonObject respuesta = new JsonObject();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idCoordinador = urides.split("&")[1].split("=")[1];

            int idFolio = Integer.parseInt(idCoordinador);

            ArrayOfInfoIncidente arrIncidentes = serviciosRemedyBI.ConsultaDetalleFolio(idFolio);

            if (arrIncidentes.getInfoIncidente() != null && arrIncidentes.getInfoIncidente().length > 0) {

                AprobacionesRemedyHilos hiloAprobacion = new AprobacionesRemedyHilos(idFolio, 1, 2);
                hiloAprobacion.start();

                while (hiloAprobacion.isAlive()) {

                }

                hiloAprobacion.getRespuesta();

                respuesta = arrOrdenado(arrIncidentes.getInfoIncidente()[0], hiloAprobacion.getRespuesta());

            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{}";
        }

        return respuesta.toString();
    }

    public JsonObject arrOrdenado(InfoIncidente incidente, Aprobacion AprobacionesHashMap) {

        JsonArray arrJsonArray = new JsonArray();
        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();
        HashMap<String, String> EmpleadosRemedyHashMap = serviciosRemedyBI.consultaUsuariosRemedy();

        InfoIncidente aux = incidente;

        JsonObject incJsonObj = new JsonObject();

        aux.getAutorizaCambioMonto();
        incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
        incJsonObj.addProperty("ESTADO", aux.getEstado());
        incJsonObj.addProperty("FALLA", aux.getFalla());
        incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
        incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());
        incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
        incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
        incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
        incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());
        String idCliente = aux.getIdCorpCliente();
        if (idCliente != null) {
            if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                if (!datos_cliente.isEmpty() && datos_cliente != null) {
                    for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                        incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                        incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                        EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                    }
                } else {
                    incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                    incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                }
            } else {
                DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
            }
        } else {
            incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
            incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
        }
        String idCord = aux.getIDCorpCoordinador();
        if (idCord != null) {
            if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
            } else {
                incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
            }
        } else {
            incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
        }
        if (idCord != null) {
            if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                    for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                        incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                        EmpleadosHashMap.put(idCord.trim(), emp_aux);
                    }
                } else {
                    incJsonObj.addProperty("COORDINADOR", "* Sin información");
                }
            } else {
                DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
            }

        } else {
            incJsonObj.addProperty("COORDINADOR", "* Sin información");
        }
        String idSup = aux.getIdCorpSupervisor();
        if (idSup != null) {
            if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
            } else {
                incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
            }
        } else {
            incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
        }
        if (idSup != null) {
            if (!idSup.matches("[^0-9]*")) {

                if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                    List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                    if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                            incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                            EmpleadosHashMap.put(idSup.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                        incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                    incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                    incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                }

            } else {
                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
            }
        } else {
            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
        }

        incJsonObj.addProperty("NOTAS", aux.getNotas());
        incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
        incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

        if (aux.getProveedor() != null) {
            if (!lista.isEmpty()) {
                for (ProveedoresDTO aux3 : lista) {
                    if (aux3.getStatus().contains("Activo")) {
                        if (aux.getProveedor().toLowerCase().trim()
                                .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                            incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                            incJsonObj.addProperty("MENU", aux3.getMenu());
                            incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                            incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                            break;
                        }

                    }
                }
            }
        } else {
            String nomCorto = null;
            incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
            incJsonObj.addProperty("MENU", nomCorto);
            incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
            incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
        }

        incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
        incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
        incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
        incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
        incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

        incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
        incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
        Date date = aux.getFechaCierre().getTime();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
        date = aux.getFechaCreacion().getTime();
        date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_CREACION", "" + date1);
        date = aux.getFechaModificacion().getTime();
        date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
        date = aux.getFechaProgramada().getTime();
        date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
        incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
        incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());
        String autCleinte = aux.getAutorizacionCliente();
        if (autCleinte != null && !autCleinte.equals("")) {
            if (autCleinte.contains("Rechazado")) {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
            }
        } else {
            incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
        }

        if (aux.getTipoAtencion() != null) {
            incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
        } else {
            String x = null;
            incJsonObj.addProperty("TIPO_ATENCION", x);
        }
        if (aux.getOrigen() != null) {
            incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
        } else {
            String x = null;
            incJsonObj.addProperty("ORIGEN", x);
        }
        //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
        String cord = null;
        if (aux.getLatitudInicio() != null) {
            incJsonObj.addProperty("LATITUD_INICIO", aux.getLatitudInicio());
        } else {
            incJsonObj.addProperty("LATITUD_INICIO", cord);
        }

        if (aux.getLongitudInicio() != null) {
            incJsonObj.addProperty("LONGITUD_INICIO", aux.getLongitudInicio());
        } else {
            incJsonObj.addProperty("LONGITUD_INICIO", cord);
        }

        if (aux.getLatitudCierre() != null) {
            incJsonObj.addProperty("LATITUD_FIN", aux.getLatitudCierre());
        } else {
            incJsonObj.addProperty("LATITUD_FIN", cord);
        }

        if (aux.getLongitudCierre() != null) {
            incJsonObj.addProperty("LONGITUD_FIN", aux.getLongitudCierre());
        } else {
            incJsonObj.addProperty("LONGITUD_FIN", cord);
        }

        // ----------------------------------------------------------------------------------
        date = aux.getFechaInicioAtencion().getTime();
        date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

        int idIncidente = aux.getIdIncidencia();

        List<TicketCuadrillaDTO> lista2 = ticketCuadrillaBI.obtieneDatos(idIncidente);
        int idProvedor = 0;
        int idCuadrilla = 0;
        int idZona = 0;
        for (TicketCuadrillaDTO aux2 : lista2) {
            if (aux2.getStatus() != 0) {
                idProvedor = aux2.getIdProveedor();
                idCuadrilla = aux2.getIdCuadrilla();
                idZona = aux2.getZona();
            }

        }
        if (idProvedor != 0) {
            List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
            for (CuadrillaDTO aux2 : lista3) {
                if (aux2.getZona() == idZona) {
                    if (aux2.getIdCuadrilla() == idCuadrilla) {
                        incJsonObj.addProperty("ID_PROVEEDOR", aux2.getIdProveedor());
                        incJsonObj.addProperty("ID_CUADRILLA", aux2.getIdCuadrilla());
                        incJsonObj.addProperty("LIDER", aux2.getLiderCuad());
                        incJsonObj.addProperty("CORREO", aux2.getCorreo());
                        incJsonObj.addProperty("SEGUNDO", aux2.getSegundo());
                        incJsonObj.addProperty("ZONA", aux2.getZona());
                    }
                }
            }
        } else {
            String auxiliar = null;
            // incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
            incJsonObj.addProperty("ID_CUADRILLA", auxiliar);
            incJsonObj.addProperty("LIDER", auxiliar);
            incJsonObj.addProperty("CORREO", auxiliar);
            incJsonObj.addProperty("SEGUNDO", auxiliar);
            incJsonObj.addProperty("ZONA", auxiliar);
        }

        if (aux.getMotivoEstado().trim().toLowerCase().equals("asignado a proveedor")
                || aux.getMotivoEstado().trim().toLowerCase().equals("duración no autorizada")
                || aux.getMotivoEstado().trim().toLowerCase().equals("cotización no autorizada")) {
            Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente, 3);

            String estadoApr = aprobacion.getEstadoAprobacion();
            boolean flag1 = false, flag2 = false;
            if (estadoApr != null) {
                String porvAprob = aprobacion.getSolicitadoPor();
                if (porvAprob.contains("" + idProvedor)) {
                    incJsonObj.addProperty("BANDERA_CMONTO", 1);
                    flag1 = true;
                } else {
                    incJsonObj.addProperty("BANDERA_CMONTO", 0);
                }
            } else {
                incJsonObj.addProperty("BANDERA_CMONTO", 0);
            }

            Aprobacion aprobacion2 = serviciosRemedyBI.consultarAprobaciones(idIncidente, 1);
            estadoApr = aprobacion2.getEstadoAprobacion();
            if (estadoApr != null) {
                String porvAprob = aprobacion2.getSolicitadoPor();
                if (porvAprob.contains("" + idProvedor)) {
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 1);
                    flag1 = true;
                } else {
                    incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                }
            } else {
                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
            }

            if (flag1 == true && flag2 == true) {
                incJsonObj.addProperty("BANDERAS", 1);
            } else {
                incJsonObj.addProperty("BANDERAS", 0);
            }
        } else {
            incJsonObj.addProperty("BANDERA_CMONTO", 0);
            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
            incJsonObj.addProperty("BANDERAS", 0);
        }

        // ----------------------- Consulta Traking --------------------
        ArrayOfTracking arrTraking = serviciosRemedyBI.ConsultaBitacora(aux.getIdIncidencia());

        JsonArray incidenciasJsonArray = new JsonArray();

        ArrayList<Date> RechazoCliente = new ArrayList<Date>();
        ArrayList<Date> inicioAtencion = new ArrayList<Date>();
        ArrayList<Date> cierreTecnico = new ArrayList<Date>();
        ArrayList<Date> cancelados = new ArrayList<Date>();
        ArrayList<Date> atencionGarantia = new ArrayList<Date>();
        ArrayList<Date> cierreGarantia = new ArrayList<Date>();
        ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

        if (arrTraking != null) {
            Tracking[] traking = arrTraking.getTracking();
            if (traking != null) {
                for (Tracking aux2 : traking) {
                    String detalle = aux2.getDetalle();
                    String arrAux[] = detalle.split("\\|\\|");
                    String estado = detalle.split("\\|\\|")[1];
                    String motivoEstado = detalle.split("\\|\\|")[3];
                    Date date2 = aux2.getFechaEnvio().getTime();
                    if (estado.toLowerCase().trim().equals("en recepción")
                            && motivoEstado.trim().toLowerCase().equals("rechazado por cliente")) {
                        RechazoCliente.add(date2);
                    }
                    if (estado.toLowerCase().trim().equals("en proceso")
                            && motivoEstado.trim().toLowerCase().equals("asignado a proveedor")) {
                        inicioAtencion.add(date2);
                    }
                    if (estado.toLowerCase().trim().equals("en recepción")
                            && motivoEstado.trim().toLowerCase().equals("cierre técnico")) {
                        cierreTecnico.add(date2);
                    }

                    if (estado.toLowerCase().trim().equals("atendido")
                            && (motivoEstado.trim().toLowerCase().equals("cancelado duplicado")
                            || motivoEstado.trim().toLowerCase().equals("cancelado no aplica")
                            || motivoEstado.trim().toLowerCase().equals("cancelado correctivo menor"))) {
                        cancelados.add(date2);
                    }

                    if (estado.toLowerCase().trim().equals("en proceso")
                            && motivoEstado.trim().toLowerCase().equals("en curso garantía")) {
                        atencionGarantia.add(date2);
                    }

                    if (estado.toLowerCase().trim().equals("atendido")
                            && motivoEstado.trim().toLowerCase().equals("cerrado garantía")) {
                        cierreGarantia.add(date2);
                    }

                    if (estado.toLowerCase().trim().equals("recibido por atender")
                            && motivoEstado.trim().toLowerCase().equals("rechazado proveedor")) {
                        rechazoProveedor.add(date2);
                    }
                }
            }
        }

        int posRC = 0, posRP = 0;

        String cadAux = null;
        if (RechazoCliente != null && !RechazoCliente.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < RechazoCliente.size(); i++) {
                if (RechazoCliente.get(pos2).compareTo(RechazoCliente.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = RechazoCliente.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", date3);
            posRC = pos2;
        } else {

            incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", cadAux);
        }

        if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < inicioAtencion.size(); i++) {
                if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = inicioAtencion.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_ATENCION", date3);
        } else {
            incJsonObj.addProperty("FECHA_ATENCION", cadAux);
        }

        if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cierreTecnico.size(); i++) {
                if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cierreTecnico.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO", date3);
            incJsonObj.addProperty("FECHA_CIERRE", date3);
        } else {
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO", cadAux);
            incJsonObj.addProperty("FECHA_CIERRE", cadAux);
        }

        if (cancelados != null && !cancelados.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cancelados.size(); i++) {
                if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cancelados.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_CANCELACION", date3);
        } else {
            incJsonObj.addProperty("FECHA_CANCELACION", cadAux);
        }
        //
        if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < atencionGarantia.size(); i++) {
                if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = atencionGarantia.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", date3);
        } else {
            incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", cadAux);
        }

        if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cierreGarantia.size(); i++) {
                if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cierreGarantia.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", date3);
        } else {
            incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", cadAux);
        }

        if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < rechazoProveedor.size(); i++) {
                if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = rechazoProveedor.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date3 = format2.format(date2);
            incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", date3);
            posRP = pos2;
        } else {
            incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", cadAux);
        }

        // BANDERA_RECHAZO_CIERRE_PREVIO
        if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && RechazoCliente != null
                && !RechazoCliente.isEmpty()) {
            if (RechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", true);
            } else {
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
            }
        } else {
            incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
        }

        // ------------------------------------------------------------------
        int cal = 0;
        _Calificacion auxEval = aux.getEvalCliente();
        if (auxEval != null) {
            if (auxEval.getValue().contains("NA")) {
                cal = 0;
            }
            if (auxEval.getValue().contains("Item1")) {
                cal = 1;
            }
            if (auxEval.getValue().contains("Item2")) {
                cal = 2;
            }
            if (auxEval.getValue().contains("Item3")) {
                cal = 3;
            }
            if (auxEval.getValue().contains("Item4")) {
                cal = 4;
            }
            if (auxEval.getValue().contains("Item5")) {
                cal = 5;
            }
        }

        incJsonObj.addProperty("CALIFICACION", cal);

        incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());

        date = aux.getFechaCierreAdmin().getTime();
        date1 = format1.format(date);
        incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);

        incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

        // ------------------------------ COMENTARIOS PARA PROVEEDOR
        // --------------------------
        // if(aux.getAutorizacionProveedor().toLowerCase().contains(""))
        /*
		 * Aprobacion aprobacion = serviciosRemedyBI.consultarAprobaciones(idIncidente,
		 * 2); if(aprobacion!=null) { incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR",
		 * aprobacion.getJustSolicitante());
		 * incJsonObj.addProperty("AUTORIZACION_PROVEEDOR",
		 * aux.getAutorizacionProveedor()); date =
		 * aprobacion.getFechaModificacion().getTime(); date1 = format1.format(date);
		 * incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
		 * incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());
		 * }
         */
        if (AprobacionesHashMap != null) {
            Aprobacion aprobacion = AprobacionesHashMap;

            incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", aprobacion.getJustSolicitante());
            incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", aux.getAutorizacionProveedor());
            date = aprobacion.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "" + date1);
            incJsonObj.addProperty("DECLINAR_PROVEEDOR", aprobacion.getJustAprobador());

            Date dateI = aux.getFechaInicioAtencion().getTime();

            if (date.compareTo(dateI) >= 0) {
                incJsonObj.addProperty("LATITUD_INICIO", cord);
                incJsonObj.addProperty("LONGITUD_INICIO", cord);
            }
        } else {
            String auxApr = null;
            incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", auxApr);
            incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", auxApr);
            incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", auxApr);
            incJsonObj.addProperty("DECLINAR_PROVEEDOR", auxApr);
        }

        // -----------------------------------------------------------------------------------
        // ----------------------------FECHA
        // CANCELACION----------------------------------------
        /*
		 * if( aux.getEstado().trim().toLowerCase().equals("atendido") &&
		 * (aux.getMotivoEstado().trim().toLowerCase().equals("cancelado duplicado")||
		 * aux.getMotivoEstado().trim().toLowerCase().equals("cancelado no aplica")||aux
		 * .getMotivoEstado().trim().toLowerCase().equals("cancelado correctivo menor"))
		 * ) { date = aux.getFechaModificacion().getTime(); date1 =
		 * format1.format(date); incJsonObj.addProperty("FECHA_CANCELACION", "" +
		 * date1); }
         */
        // -------------------------------------------------------------------------------------
        incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
        incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
        incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());

        return incJsonObj;

    }

    // http://localhost:8080/migestion/servicios/getHorasPico.json?idSucursal=<?>&anio=<?>&semana=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getHorasPico", method = RequestMethod.GET)
    @ResponseBody
    public String getHorasPico(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        Map<String, String> params = UtilRequestParameters.getParameterFromQueryString(request.getQueryString());
        String idSucursal = params.get("idSucursal");
        String anio = params.get("anio");
        String semana = params.get("semana");
        List<HorasPicoDTO> horasPico = horasPicoBI.getHorasPico(idSucursal, anio, semana);
        if (horasPico != null) {
            String jsonOut = gson.toJson(horasPico);
            return jsonOut;
        }
        return "[]";
    }

    // http://localhost:8080/migestion/servicios/setDistribucionHorasPico.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/setDistribucionHorasPico", method = RequestMethod.GET)
    @ResponseBody
    public String setDistribucionHorasPico(HttpServletRequest request, HttpServletResponse response, Model model)
            throws Exception {
        boolean success = horasPicoBI.setDistribucionHorasPico();
        return "{\"success\" : " + success + "}";
    }

    // http://localhost:8080/migestion/servicios/getIndicadoresIPNCliente.json?idCeco=<?>&top=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIndicadoresIPNCliente", method = RequestMethod.GET)
    @ResponseBody
    public String getIndicadoresIPNCliente(HttpServletRequest request, HttpServletResponse response, Model model)
            throws Exception {
        Map<String, String> params = UtilRequestParameters.getParameterFromQueryString(request.getQueryString());
        String idCeco = params.get("idCeco");
        String top = params.get("top");
        List<IndicadorIPNClienteDTO> indicadoresIPNCliente = indicadoresIPNBI.getIndicadoresIPNCliente(idCeco,
                new Integer(top));
        if (indicadoresIPNCliente != null) {
            String jsonOut = gson.toJson(indicadoresIPNCliente);
            return jsonOut;
        }
        return "[]";
    }

    // http://localhost:8080/migestion/servicios/getIndicadoresIPNEquipo.json?idCeco=<?>&top=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIndicadoresIPNEquipo", method = RequestMethod.GET)
    @ResponseBody
    public String getIndicadoresIPNEquipo(HttpServletRequest request, HttpServletResponse response, Model model)
            throws Exception {
        Map<String, String> params = UtilRequestParameters.getParameterFromQueryString(request.getQueryString());
        String idCeco = params.get("idCeco");
        String top = params.get("top");
        List<IndicadorIPNEquipoDTO> indicadoresIPNEquipo = indicadoresIPNBI.getIndicadoresIPNEquipo(idCeco,
                new Integer(top));
        if (indicadoresIPNEquipo != null) {
            String jsonOut = gson.toJson(indicadoresIPNEquipo);
            return jsonOut;
        }
        return "[]";
    }

    // http://localhost:8080/migestion/servicios/finalizarComunicado.json?Usuario=<?>&ceco=<?>&projectId=<?>&surveyType=<?>&surveyData=<?>
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/finalizarComunicado", method = RequestMethod.GET)
    public ModelAndView finalizarComunicado(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException, KeyException, GeneralSecurityException {

        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        String usuario = (urides.split("&")[0]).split("=")[1];
        String ceco = (urides.split("&")[1]).split("=")[1];
        String projectId = (urides.split("&")[2]).split("=")[1];
        String sCd = (urides.split("&")[3]).split("=")[1]; // C ó F
        HttpURLConnection conn = null;
        JsonObject jsonObject = null;

        try {
            String parametros = "&userId=" + usuario + "&unitId=" + ceco + "&projectId=" + projectId + "&statusCd="
                    + sCd;
            conn = (HttpURLConnection) new URL(
                    GTNConstantes.getURLProdServicioRest() + GTNConstantes.MTD_STATUS + parametros).openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            jsonObject = (JsonObject) new JsonParser().parse(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            if (jsonObject.has("CdMsj")) {
                if (jsonObject.get("CdMsj").getAsInt() == 0) {
                    mv.addObject("endTask", "si");
                } else {
                    mv.addObject("endTask", "no");
                }
            } else {
                mv.addObject("endTask", "no");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (jsonObject != null) {
                jsonObject = null;
            }
        }

        return mv;

    }

    // http://localhost:8080/migestion/servicios/getCecosById.jsp?idCeco=<?>&idCecoP=<?>&activo=<?>&negocio=<?>&canal=<?>&pais=<?>&nombreCC=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCecosById", method = RequestMethod.GET)
    @ResponseBody
    public String getCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            Map<String, String> params = UtilRequestParameters.getParameterFromQueryString(request.getQueryString());
            String idCeco = params.get("idCeco");
            String idCecoP = params.get("idCecoP");
            String activo = params.get("activo");
            String negocio = params.get("negocio");
            String canal = params.get("canal");
            String pais = params.get("pais");
            String nombreCC = params.get("nombreCC");

            System.out.println(
                    idCeco + " " + idCecoP + " " + activo + " " + negocio + " " + canal + " " + pais + " " + nombreCC);
            List<CecoDTO> res = cecoBI.buscaCecos(idCeco, idCecoP, activo, negocio, canal, pais, nombreCC);
            if (res != null) {
                String jsonOut = gson.toJson(res);
                System.out.println("jsonOut " + jsonOut);
                return jsonOut;
            }
            return "[]";

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return null;
        }

    }

    // http://localhost:8080/migestion/servicios/postGuardaArchivo.json?idUsuario=<?>&ruta=<?>&nomArchivo=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postGuardaArchivo", method = RequestMethod.POST)
    public @ResponseBody
    String postGuardaArchivo(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        String res = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;

        boolean respuesta = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, GTNConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];

            int idSuc = 0;

            String rutaEntrada = (urides.split("&")[1]).split("=")[1];
            String nomArchivo = (urides.split("&")[2]).split("=")[1];
            String archivo = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);

                archivo = rec.getString("archivo");
            }

            File r = null;

            String rootPath = File.listRoots()[0].getAbsolutePath();
            Calendar cal = Calendar.getInstance();

            String rutaFotoA = "";
            String rutaFotoD = "";

            // DEFINIR RUTA
            String rutaImagen = "";
            // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
            // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
            // NUEVO
            // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist

            rutaImagen = rutaEntrada;

            String ruta = rootPath + rutaImagen;

            // System.out.println(cal.getTime().toString());
            // System.out.println(File.listRoots()[0].getAbsolutePath());
            r = new File(ruta);

            // logger.info("RUTA: " + ruta);
            if (r.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            FileOutputStream fos = null;

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] imgDecodificada = decoder.decode(archivo);

            Date fechaActual = new Date();
            System.out.println(fechaActual);
            System.out.println("---------------------------------------------");

            String rutaTotal = ruta + nomArchivo;

            String rutalimpia = rutaTotal;

            FileOutputStream fileOutput = new FileOutputStream(rutalimpia);

            try {
                respuesta = true;
                BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);

                // se escribe el archivo decodificado
                bufferOutput.write(imgDecodificada);
                bufferOutput.close();

                rutaFotoA = rutalimpia;

            } catch (Exception e) {
                logger.info(e);
                e.printStackTrace();
                respuesta = false;
            } finally {

                fileOutput.close();

            }

            JsonObject envoltorioJsonObj = new JsonObject();

            envoltorioJsonObj.addProperty("ARCHIVO", respuesta);

            // logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{\"ARCHIVO\": false}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{\"ARCHIVO\": false}";
        }
    }

}
