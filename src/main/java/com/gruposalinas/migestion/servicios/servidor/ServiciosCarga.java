package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.ProductividadBI;
import com.gruposalinas.migestion.business.ef.AsesoresDigitalesBI;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cargasService")
public class ServiciosCarga {

    //private Logger logger = LogManager.getLogger(ServiciosCarga.class);
    @Autowired
    ProductividadBI productividadBI;

    @Autowired
    AsesoresDigitalesBI asesoresDigitalesBI;

    @RequestMapping(value = "/cargaProductividad", method = RequestMethod.GET)
    public @ResponseBody
    String cargaProductividad(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String fecha = request.getParameter("fecha");
        productividadBI.extraccion(fecha, 2);
        return "Se ejecuto la carga de Productividad";
    }

    @RequestMapping(value = "/cargaInfoAsesoresDigitales", method = RequestMethod.GET)
    public @ResponseBody
    String cargaInfoAsesoresDigitales(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        asesoresDigitalesBI.cargaInformacion();

        return "Se ejecuto el proceso de carga informacion para notificar a los Asesores Digitales";
    }

    @RequestMapping(value = "/enviaNotificacionesAsesores", method = RequestMethod.GET)
    public @ResponseBody
    String enviaNotificacionesAsesores(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        asesoresDigitalesBI.enviaNotifacionesCompromiso();

        return "Se ejecuto el proceso de carga informacion para notificar a los Asesores Digitales";
    }

    @RequestMapping(value = "/updateAvances", method = RequestMethod.GET)
    public @ResponseBody
    String updateAvances(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        asesoresDigitalesBI.actulizaAvances();

        return "Se ejecuto el proceso de actualizacion informacion para notificar a los Asesores Digitales";
    }

    @RequestMapping(value = "/enviaNotificacionesAvances", method = RequestMethod.GET)
    public @ResponseBody
    String enviaNotificacionesAvances(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        asesoresDigitalesBI.enviaNotifacionesAvance();

        return "Se ejecuto el proceso de carga informacion para notificar a los Asesores Digitales";
    }

}
