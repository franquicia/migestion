package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.NotificacionesCiscoSparkBI;
import com.gruposalinas.migestion.domain.TokenUserCiscoSparkDTO;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.StrCipher;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/notificacionesCiscoSparkService")

public class ServicioNotificacionesCiscoSpark {

    @Autowired
    NotificacionesCiscoSparkBI notificacionesCiscoSparkBI;

    @Autowired
    ServletContext context;

    private static Logger logger = LogManager.getLogger(ServicioNotificacionesCiscoSpark.class);

    //http://10.51.210.216:8080/migestion/notificacionesCiscoSparkService/notificacionDirecta.json?idUsuario=189871
    @RequestMapping(value = "/notificacionDirecta", method = RequestMethod.GET)
    public @ResponseBody
    String notificacionDirecta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;

        logger.info("NOTIFICACION DIRECTA SPARK");

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];

            this.enviaNotificacion("6955e8f9c2fd17ef7587c9dce6d20e529fb070ca777d2a74caf3f2576169a484", "1", "1", "1", "1", "1");
            this.enviaNotificacion("46e510413f3dcade6358b4f442a870e25b5f9e25146929138a5b5257d6d781b9", "1", "1", "1", "1", "1");

            logger.info("NOTIFICACION 2 SANDBOX DESTINATION  !!!");

            this.enviaNotificacion2();

            logger.info("NOTIFICACION 3 APPLE DESTINATION TRUE !!!");

            this.enviaNotificacion3();

            res = true;

        } catch (Exception e) {
            //e.printStackTrace();
            //logger.info("Ocurrio un algo ");
            res = false;

        }

        logger.info("NOTIFICACION DIRECTA SPARK: " + res);

        return "{\"response\": \"" + res + "\"}";
    }

    //http://localhost:8080/migestion/notificacionesCiscoSparkService/registraDispositivoCisco.json?idUsuario=<?>&tokenApple=<?>&email=<?>&personId=<?>&tipo=<?>
    @RequestMapping(value = "/registraDispositivoCisco", method = RequestMethod.GET)
    public @ResponseBody
    String registraDispositivoCisco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;

        logger.info("NOTIFICACION SPARK EJECUTANDO SERVICIO REGISTRA DISPOSITIVO");

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String tokenApple = urides.split("&")[1].split("=")[1];
            String email = urides.split("&")[2].split("=")[1];
            String personId = urides.split("&")[3].split("=")[1];
            String tipo = urides.split("&")[4].split("=")[1];

            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            String fecha = date;

            res = notificacionesCiscoSparkBI.registraDispositivo(tokenApple, email, personId, tipo, fecha);

        } catch (Exception e) {
            //e.printStackTrace();
            //logger.info("Ocurrio un algo ");
            res = false;

        }

        logger.info("NOTIFICACION SPARK FIN EJECUTANDO SERVICIO REGISTRA DISPOSITIVO RES: " + res);

        return "{\"response\": \"" + res + "\"}";
    }

    //http://localhost:8080/migestion/notificacionesCiscoSparkService/realizaLlamada.json?idUsuario=<?>&tokenApple=<?>&email=<?>&personId=<?>&tipo=<?>&sucursal=<?>&empleado=<?>
    @RequestMapping(value = "/realizaLlamada", method = RequestMethod.GET)
    public @ResponseBody
    String realizaLlamada(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        logger.info("NOTIFICACION SPARK SERVICIO REALIZA LLAMADA");

        List<TokenUserCiscoSparkDTO> res = null;
        String data = "";
        Boolean result = false;

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String tokenApple = urides.split("&")[1].split("=")[1];
            String email = urides.split("&")[2].split("=")[1];
            String personId = urides.split("&")[3].split("=")[1];
            String tipo = urides.split("&")[4].split("=")[1];
            String sucursal = urides.split("&")[5].split("=")[1];
            String empleado = urides.split("&")[6].split("=")[1];

            logger.info("NOTIFICACION SPARK LLAMANDO USUARIO QUE LLAMA" + email);

            res = notificacionesCiscoSparkBI.buscaDispositivos(email);

            logger.info("DISPOSITIVOS ENCONTRADOS " + res.size());

            Iterator<TokenUserCiscoSparkDTO> it = res.iterator();
            while (it.hasNext()) {

                TokenUserCiscoSparkDTO obj = it.next();

                String tkn = obj.getToken();

                logger.info("NOTIFICACION SPARK LLAMANDO...." + tkn);

                data += "\"" + tkn + "\"" + ",";

                this.enviaNotificacion(tkn, email, personId, tipo, sucursal, empleado);

            }

            result = true;

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("NOTIFICACION SPARK ALGO OCURRIO SERVICIO REALIZA LLAMADA");
            result = false;
            res = null;

        }
        data = this.replaceLast(",", "", data);

        logger.info("NOTIFICACION SPARK REALIZA LLAMADA CORRECTO : " + result);
        return "{\"response\": \"" + result + "\"}";

    }

    //http://localhost:8080/migestion/notificacionesCiscoSparkService/eliminaRegistro.json?idUsuario=<?>&tokenApple=<?>
    @RequestMapping(value = "/eliminaRegistro", method = RequestMethod.GET)
    public @ResponseBody
    String eliminaRegistro(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;
        logger.info("NOTIFICACION SPARK BAJA DISPOSITIVO: ");

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String tokenApple = urides.split("&")[1].split("=")[1];

            res = notificacionesCiscoSparkBI.eliminaRegistro(tokenApple);

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("NOTIFICACION SPARK BAJA DISPOSITIVO ALGO OCURRIO : ");
            res = false;

        }

        logger.info("NOTIFICACION SPARK BAJA DISPOSITIVO CORRECTA : " + res);
        return "{\"response\": \"" + res + "\"}";
    }

    public static String replaceLast(String find, String replace, String string) {
        int lastIndex = string.lastIndexOf(find);

        if (lastIndex == -1) {
            return string;
        }

        String beginString = string.substring(0, lastIndex);
        String endString = string.substring(lastIndex + find.length());

        return beginString + replace + endString;
    }

    public static void main(String[] args) throws UnrecoverableKeyException, KeyManagementException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {

        //new ServicioNotificacionesCiscoSpark().enviaNotificacion("6955e8f9c2fd17ef7587c9dce6d20e529fb070ca777d2a74caf3f2576169a484");
        //new ServicioNotificacionesCiscoSpark().enviaNotificacion2("6955e8f9c2fd17ef7587c9dce6d20e529fb070ca777d2a74caf3f2576169a484");
        //new ServicioNotificacionesCiscoSpark().bibliotecaAPNSHTTP2();
    }

    private void enviaNotificacion(String token, String email, String personId, String tipo, String sucursal, String empleado) {

        //METODO 2
        try {
            logger.info("enviaNotificacion() withAppleDestination");

            String ruta = context.getRealPath("/files/voipGR.p12");

            logger.info("Antes requestproxy");

            logger.info("TOKEN A LLAMAR enviaNotificacion()  --- >" + token);

            Proxy requestproxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 80));
            ApnsService service = APNS.newService()
                    .withCert(ruta, "miguel261291angel")
                    .withAppleDestination(false)
                    .withProxy(requestproxy)
                    .build();

            logger.info("Antes payload");

            String payload = APNS.newPayload().alertBody("Llamando").customField("email", email).customField("personId", personId).customField("tipo", tipo)
                    .customField("sucursal", sucursal).customField("empleado", empleado).build();

            logger.info("Antes de push withAppleDestination");

            service.push(token, payload);

            logger.info("Despues de push 1 withAppleDestination");

        } catch (Exception e) {

            //logger.info("Algo ocurrio al enviarNotificacion()  withAppleDestination!!!!!");
            //logger.info(e.getMessage());
            //logger.info(e.getStackTrace());
        }

    }

    private void enviaNotificacion2() {

        //METODO 2
        logger.info("enviaNotificacion() withSandBoxDestination");
        try {

            String ruta = context.getRealPath("/files/voipGR.p12");

            logger.info("Antes requestproxy withSandBoxDestination");

            Proxy requestproxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 80));
            ApnsService service = APNS.newService()
                    .withCert(ruta, "miguel261291angel")
                    .withSandboxDestination()
                    .withProxy(requestproxy)
                    .build();

            logger.info("Antes payload withSandBoxDestination");

            String payload = APNS.newPayload().alertBody("Notificacion Enviada").customField("email", "amorales@elektra.com.mx").customField("personId", "987654").customField("tipo", "1")
                    .customField("sucursal", "99995").customField("empleado", "189871").build();

            logger.info("Antes de push withSandBoxDestination");

            String token = "6955e8f9c2fd17ef7587c9dce6d20e529fb070ca777d2a74caf3f2576169a484";
            service.push(token, payload);

            logger.info("Despues de push 1  withSandBoxDestination");

            String token1 = "46e510413f3dcade6358b4f442a870e25b5f9e25146929138a5b5257d6d781b9";
            service.push(token1, payload);

            logger.info("Despues de push 2  withSandBoxDestination");

        } catch (Exception e) {

            //logger.info("Algo ocurrio en enviaNotificacion() withSandBoxDestination!!!!!! ");
            //logger.info(e.getMessage());
            //logger.info(e.getLocalizedMessage());
        }

    }

    private void enviaNotificacion3() {

        //METODO 2
        logger.info("enviaNotificacion() withAppleDestination true");
        try {

            String ruta = context.getRealPath("/files/voipGR.p12");

            logger.info("Antes requestproxy withAppleDestination true");

            Proxy requestproxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 80));
            ApnsService service = APNS.newService()
                    .withCert(ruta, "miguel261291angel")
                    .withAppleDestination(true)
                    .withProxy(requestproxy)
                    .build();

            logger.info("Antes payload withAppleDestination true");

            String payload = APNS.newPayload().alertBody("Notificacion Enviada").customField("email", "amorales@elektra.com.mx").customField("personId", "987654").customField("tipo", "1")
                    .customField("sucursal", "99995").customField("empleado", "189871").build();

            logger.info("Antes de push withAppleDestination true");

            String token = "6955e8f9c2fd17ef7587c9dce6d20e529fb070ca777d2a74caf3f2576169a484";
            service.push(token, payload);

            logger.info("Despues de push 1  withSandBoxDestination");

            String token1 = "46e510413f3dcade6358b4f442a870e25b5f9e25146929138a5b5257d6d781b9";
            service.push(token1, payload);

            logger.info("Despues de push 2  withAppleDestination true");

        } catch (Exception e) {

            //logger.info("Algo ocurrio en enviaNotificacion() withAppleDestination true!!!!!! ");
            //logger.info(e.getMessage());
            //logger.info(e.getLocalizedMessage());
        }

    }

}
