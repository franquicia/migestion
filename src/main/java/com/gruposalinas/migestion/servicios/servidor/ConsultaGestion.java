package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.AgendaBI;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.business.GeografiaBI;
import com.gruposalinas.migestion.business.MovilInfoBI;
import com.gruposalinas.migestion.business.PerfilBI;
import com.gruposalinas.migestion.business.PerfilUsuarioBI;
import com.gruposalinas.migestion.business.PeriodoBI;
import com.gruposalinas.migestion.business.PeriodoReporteBI;
import com.gruposalinas.migestion.business.ReporteBI;
import com.gruposalinas.migestion.business.TareaActBI;
import com.gruposalinas.migestion.business.Usuario_ABI;
import com.gruposalinas.migestion.business.ef.AsesoresDigitalesBI;
import com.gruposalinas.migestion.domain.AgendaDTO;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.GeografiaDTO;
import com.gruposalinas.migestion.domain.MovilInfoDTO;
import com.gruposalinas.migestion.domain.MovilinfReporteDTO;
import com.gruposalinas.migestion.domain.NotificaAfDTO;
import com.gruposalinas.migestion.domain.PerfilDTO;
import com.gruposalinas.migestion.domain.PerfilUsuarioDTO;
import com.gruposalinas.migestion.domain.PeriodoDTO;
import com.gruposalinas.migestion.domain.PeriodoReporteDTO;
import com.gruposalinas.migestion.domain.ReporteDTO;
import com.gruposalinas.migestion.domain.ReporteMensualMovilDTO;
import com.gruposalinas.migestion.domain.TareaActDTO;
import com.gruposalinas.migestion.domain.TokensAsesoresDTO;
import com.gruposalinas.migestion.domain.Usuario_ADTO;
import com.gruposalinas.migestion.resources.GTNAuthInterceptor;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/consultaGestionService")

public class ConsultaGestion {

    @Autowired
    CecoBI cecoBI;

    @Autowired
    TareaActBI tareaBI;

    @Autowired
    PeriodoBI periodoBI;

    @Autowired
    Usuario_ABI usuarioBI;

    @Autowired
    PeriodoReporteBI periodoRepBI;

    @Autowired
    ReporteBI reporteBI;

    @Autowired
    GeografiaBI geoBI;

    @Autowired
    AgendaBI agendaBI;

    @Autowired
    PerfilBI perfilbi;

    @Autowired
    PerfilUsuarioBI perfilusuariobi;

    @Autowired
    MovilInfoBI movilInfoBI;

    @Autowired
    AsesoresDigitalesBI asesoresDigitalesBI;


    private static final Logger logger = LogManager.getLogger(GTNAuthInterceptor.class);

    // http://localhost:8080/migestion/consultaGestionService/getCecos.json?idCeco=<?>&idCecoP=<?>&activo=<?>&negocio=<?>&canal=<?>&pais=<?>&nombreCC=<?>
    @RequestMapping(value = "/getCecos", method = RequestMethod.GET)
    public ModelAndView getCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");
            String idCecoP = request.getParameter("idCecoP");
            String activo = request.getParameter("activo");
            String negocio = request.getParameter("negocio");
            String canal = request.getParameter("canal");
            String pais = request.getParameter("pais");
            String nombreCC = request.getParameter("nombreCC");

            System.out.println(
                    idCeco + " " + idCecoP + " " + activo + " " + negocio + " " + canal + " " + pais + " " + nombreCC);
            List<CecoDTO> res = cecoBI.buscaCecos(idCeco, idCecoP, activo, negocio, canal, pais, nombreCC);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA CECOS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/migestion/consultaGestionService/getTarea.json?idTarea=<?>&status=<?>&estatusVac=<?>&idUsuario=<?>&tipoTarea=<?>
    @RequestMapping(value = "/getTarea", method = RequestMethod.GET)
    public ModelAndView getTarea(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idTarea = request.getParameter("idTarea");
            String status = request.getParameter("status");
            String estatusVac = request.getParameter("estatusVac");
            String idUsuario = request.getParameter("idUsuario");
            String tipoTarea = request.getParameter("tipoTarea");

            List<TareaActDTO> res = tareaBI.obtieneTareaCom(idTarea, status, estatusVac, idUsuario, tipoTarea);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA TAREAS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getPeriodo.json?idPeriodo=<?>&descripcion=<?>
    @RequestMapping(value = "/getPeriodo", method = RequestMethod.GET)
    public ModelAndView getPeriodo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPeriodo = request.getParameter("idPeriodo");
            String descripcion = request.getParameter("descripcion");

            List<PeriodoDTO> res = periodoBI.obtienePeriodo(idPeriodo, descripcion);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PERIODO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getUsuario.json?idUsuario=<?>
    @RequestMapping(value = "/getUsuario", method = RequestMethod.GET)
    public ModelAndView getUsuario(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idUsuario = request.getParameter("idUsuario");

            List<Usuario_ADTO> res = usuarioBI.obtieneUsuario(Integer.parseInt(idUsuario));

            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
            mv.addObject("USUARIO", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getPeriodoReporte.json?idPeriodoR=<?>&idPeriodo=<?>&idReporte=<?>
    @RequestMapping(value = "/getPeriodoReporte", method = RequestMethod.GET)
    public ModelAndView getPeriodoReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPeriodoR = request.getParameter("idPeriodoR");
            String idPeriodo = request.getParameter("idPeriodo");
            String idReporte = request.getParameter("idReporte");

            List<PeriodoReporteDTO> res = periodoRepBI.obtienePeriodoReporte(idPeriodoR, idPeriodo, idReporte);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PERIODOR");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getReporte.json?idReporte=<?>&nombre=<?>
    @RequestMapping(value = "/getReporte", method = RequestMethod.GET)
    public ModelAndView getReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idReporte = request.getParameter("idReporte");
            String nombre = request.getParameter("nombre");

            List<ReporteDTO> res = reporteBI.obtieneReporte(idReporte, nombre);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA REPORTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getGeografia.json?idCeco=<?>&idRegion=<?>&idZona=<?>&idTerritorio=<?>
    @RequestMapping(value = "/getGeografia", method = RequestMethod.GET)
    public ModelAndView getGeografia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");
            String idRegion = request.getParameter("idRegion");
            String idZona = request.getParameter("idZona");
            String idTerritorio = request.getParameter("idTerritorio");

            List<GeografiaDTO> res = geoBI.obtieneGeografia(idCeco, idRegion, idZona, idTerritorio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA GEOGRAFIA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getAgenda.json?idUsuario=<?>&idAgenda=<?>
    @RequestMapping(value = "/getAgenda", method = RequestMethod.GET)
    public ModelAndView getAgenda(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idAgenda = request.getParameter("idAgenda");

            List<AgendaDTO> res = agendaBI.obtieneNotas(idAgenda, idUsuario);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA AGENDA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getPerfiles.json
    @RequestMapping(value = "/getPerfiles", method = RequestMethod.GET)
    public ModelAndView getPerfiles(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<PerfilDTO> lista = perfilbi.obtienePerfil();
            //logger.info("Entro a perfiles");
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PERFILES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getPerfilUsuarios.json?idUsuario=<?>&idPerfil=<?>
    @RequestMapping(value = "/getPerfilUsuarios", method = RequestMethod.GET)
    public ModelAndView getPerfilUsuarios(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            List<PerfilUsuarioDTO> res = perfilusuariobi.obtienePerfiles(idUsuario, idPerfil);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "PERFILES USUARIO");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    //MOVER AQUI SOLO MUESTRA LA LISTA QUE YA RECIBI
    // http://localhost:8080/migestion/consultaGestionService/getMovilInfo.json?idUsuario=<?>
    @RequestMapping(value = "/getMovilInfo", method = RequestMethod.GET)
    public ModelAndView getMovilInfo(HttpServletRequest request, HttpServletResponse response) {

        try {
            String params = request.getQueryString();

            if (params != null) {
                String idUsuario = request.getParameter("idUsuario");
                MovilInfoDTO movil = new MovilInfoDTO();
                movil.setIdUsuario(Integer.parseInt(idUsuario));

                List<MovilInfoDTO> lista = movilInfoBI.obtieneInfoMovil(movil);

                ModelAndView mv = new ModelAndView("muestraServicios");
                mv.addObject("tipo", "LISTA MOVIL");
                mv.addObject("res", lista);

                return mv;
            } else {
                logger.info("No hay Usuario: ");
                return null;
            }
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/consultaGestionService/getReporteMensualMovilInf.json?mes=<?>&anio=<?>
    @RequestMapping(value = "/getReporteMensualMovilInf", method = RequestMethod.GET)
    public ModelAndView getReporteMensualMovilInf(HttpServletRequest request, HttpServletResponse response) {
        try {

            String mes = request.getParameter("mes");
            String anio = request.getParameter("anio");

            logger.info("REPORTE MENSUAL- MES: " + mes + " - ANIO: " + anio);

            List<MovilinfReporteDTO> res = movilInfoBI.getReporteMensual(mes, anio);
            logger.info("REPORTE: " + res);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "REPORTE_MOVIL_INF_MENSUAL");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/migestion/consultaGestionService/getReporteMovilInfDetalle.json?mes=<?>&anio=<?>&idUsuario=<?>
    @RequestMapping(value = "/getReporteMovilInfDetalle", method = RequestMethod.GET)
    public ModelAndView getReporteMovilInfDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            String mes = request.getParameter("mes");
            String anio = request.getParameter("anio");
            String idUsuario = request.getParameter("idUsuario");

            logger.info("REPORTE MENSUAL DETALLE- MES: " + mes + " - ANIO: " + anio + " - ID_USUARIO: " + idUsuario);

            List<MovilInfoDTO> res = movilInfoBI.getDetalleUsuario(mes, anio, Integer.parseInt(idUsuario));
            logger.info("REPORTE DETALLE: " + res);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "REPORTE_MOVIL_INF_DETALLE");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/migestion/consultaGestionService/getReporteMensualCeco.json?mes=<?>&anio=<?>&idCeco=<?>&negocio=<?>&pais=<?>
    @RequestMapping(value = "/getReporteMensualCeco", method = RequestMethod.GET)
    public ModelAndView getReporteMensualCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            String mes = request.getParameter("mes");
            String anio = request.getParameter("anio");
            String idCeco = request.getParameter("idCeco");
            String negocio = request.getParameter("negocio");
            String pais = request.getParameter("pais");

            List<ReporteMensualMovilDTO> res = movilInfoBI.getReporteMensualCeco(Integer.parseInt(pais), Integer.parseInt(negocio), Integer.parseInt(idCeco), mes, anio);
            logger.info("REPORTE MENSUAL CECO: " + res);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "REPORTE_MENSUAL_CECO");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/migestion/consultaGestionService/getConsultaCecosHijo.json?idCeco=<?>&negocio=<?>&pais=<?>
    @RequestMapping(value = "/getConsultaCecosHijo", method = RequestMethod.GET)
    public ModelAndView getConsultaCecosHijo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCeco = request.getParameter("idCeco");
            String negocio = request.getParameter("negocio");
            String pais = request.getParameter("pais");

            List<CecoDTO> res = movilInfoBI.getCecosHijos(Integer.parseInt(pais), Integer.parseInt(negocio), idCeco);
            logger.info("REPORTE MENSUAL CECO: " + res);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CECOS_HIJOS");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/migestion/consultaGestionService/getReporteSemanalCeco.json?fechaInicio=<?>&fechaFin=<?>&idCeco=<?>&negocio=<?>&pais=<?>
    @RequestMapping(value = "/getReporteSemanalCeco", method = RequestMethod.GET)
    public ModelAndView getReporteSemanalCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            String idCeco = request.getParameter("idCeco");
            String negocio = request.getParameter("negocio");
            String pais = request.getParameter("pais");
            logger.info("FECHA_INI: " + fechaInicio);
            logger.info("FECHA_FIN: " + fechaFin);
            logger.info("CECO: " + idCeco);
            logger.info("NEGOCIO: " + negocio);
            logger.info("PAIS: " + pais);

            List<ReporteMensualMovilDTO> res = movilInfoBI.getReporteSemanalCeco(Integer.parseInt(pais), Integer.parseInt(negocio), Integer.parseInt(idCeco), fechaInicio, fechaFin);
            logger.info("REPORTE SEMANAL CECO: " + res);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "REPORTE_MENSUAL_CECO");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/migestion/consultaGestionService/consultaGetanotificaAf.json?idCeco=<?>
    @RequestMapping(value = "/consultaGetanotificaAf", method = RequestMethod.GET)
    public ModelAndView consultaGetanotificaAf(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCeco = request.getParameter("idCeco");

            List<NotificaAfDTO> respuesta = asesoresDigitalesBI.consulta(idCeco);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GETANOTIFICA_AF");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/migestion/consultaGestionService/consultaTokenNotifica.json
    @RequestMapping(value = "/consultaTokenNotifica", method = RequestMethod.GET)
    public ModelAndView consultaTokenNotifica(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<TokensAsesoresDTO> tokens = asesoresDigitalesBI.consultaTokenAenviar();

            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
            mv.addObject("TOKENS", tokens);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }


    // http://localhost:8080/migestion/consultaGestionService/getPerfilUsuariosNue.json?idUsuario=<?>&idPerfil=<?>
    @RequestMapping(value = "/getPerfilUsuariosNue", method = RequestMethod.GET)
    public ModelAndView getPerfilUsuariosNue(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            List<PerfilUsuarioDTO> res = perfilusuariobi.obtienePerfilesNue(idUsuario, idPerfil);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "PERFILESUSU");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

}
