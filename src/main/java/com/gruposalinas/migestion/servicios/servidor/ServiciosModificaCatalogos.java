package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.ActivoFijoBI;
import com.gruposalinas.migestion.business.ArchivoUrlBI;
import com.gruposalinas.migestion.business.AreaApoyoBI;
import com.gruposalinas.migestion.business.BitacoraMantenimientoBI;
import com.gruposalinas.migestion.business.CartaAsignacionAFBI;
import com.gruposalinas.migestion.business.CatalogoMinuciasBI;
import com.gruposalinas.migestion.business.CatalogoVistaBI;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.business.CedulaBI;
import com.gruposalinas.migestion.business.ContingenciaBI;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.DepuraActFijoBI;
import com.gruposalinas.migestion.business.EstandPuestoBI;
import com.gruposalinas.migestion.business.ExternoPuestoBI;
import com.gruposalinas.migestion.business.FilasBI;
import com.gruposalinas.migestion.business.FormatoMetodo7sBI;
import com.gruposalinas.migestion.business.FotoPlantillaBI;
import com.gruposalinas.migestion.business.FotosAntesDespuesBI;
import com.gruposalinas.migestion.business.GeografiaBI;
import com.gruposalinas.migestion.business.GuiaDHLBI;
import com.gruposalinas.migestion.business.IncidenciasBI;
import com.gruposalinas.migestion.business.InfoVistaBI;
import com.gruposalinas.migestion.business.OperacionBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.business.PerfilBI;
import com.gruposalinas.migestion.business.PeriodoBI;
import com.gruposalinas.migestion.business.PeriodoReporteBI;
import com.gruposalinas.migestion.business.ProductoBI;
import com.gruposalinas.migestion.business.ProgLimpiezaBI;
import com.gruposalinas.migestion.business.ProveedorLoginBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.PuestoBI;
import com.gruposalinas.migestion.business.ReporteBI;
import com.gruposalinas.migestion.business.TareaActBI;
import com.gruposalinas.migestion.business.TelSucursalBI;
import com.gruposalinas.migestion.business.TicketCuadrillaBI;
import com.gruposalinas.migestion.business.UsuarioExternoBI;
import com.gruposalinas.migestion.business.Usuario_ABI;
import com.gruposalinas.migestion.business.ef.AsesoresDigitalesBI;
import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import com.gruposalinas.migestion.domain.ArchivosUrlDTO;
import com.gruposalinas.migestion.domain.AreaApoyoDTO;
import com.gruposalinas.migestion.domain.BitacoraMntoDTO;
import com.gruposalinas.migestion.domain.CartaAsignacionAFDTO;
import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;
import com.gruposalinas.migestion.domain.CatalogoVistaDTO;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.CedulaDTO;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.domain.DepuraActFijoDTO;
import com.gruposalinas.migestion.domain.EstandPuestoDTO;
import com.gruposalinas.migestion.domain.ExternoDTO;
import com.gruposalinas.migestion.domain.ExternoPuestoDTO;
import com.gruposalinas.migestion.domain.FilasDTO;
import com.gruposalinas.migestion.domain.FormatoMetodo7sDTO;
import com.gruposalinas.migestion.domain.FotoPlantillaDTO;
import com.gruposalinas.migestion.domain.FotosAntesDespuesDTO;
import com.gruposalinas.migestion.domain.GeografiaDTO;
import com.gruposalinas.migestion.domain.GuiaDHLDTO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.domain.InfoVistaDTO;
import com.gruposalinas.migestion.domain.NotificaAfDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.domain.PerfilDTO;
import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;
import com.gruposalinas.migestion.domain.ProveedorLoginDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.PuestoDTO;
import com.gruposalinas.migestion.domain.TareaActDTO;
import com.gruposalinas.migestion.domain.TelSucursalDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.domain.TipificacionDTO;
import com.gruposalinas.migestion.domain.Usuario_ADTO;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/catalogosService")
public class ServiciosModificaCatalogos {

    @Autowired
    ProductoBI productoBi;

    @Autowired
    OperacionBI operacionBI;

    @Autowired
    ParametroBI parametroBI;

    @Autowired
    CecoBI cecoBI;

    @Autowired
    TareaActBI tareaBI;

    @Autowired
    PeriodoBI periodoBI;

    @Autowired
    Usuario_ABI usuarioabi;

    @Autowired
    PeriodoReporteBI periodoReporteBI;

    @Autowired
    ReporteBI reporteBI;

    @Autowired
    GeografiaBI geoBI;

    @Autowired
    PerfilBI perfilbi;

    @Autowired
    AsesoresDigitalesBI asesoresBi;

    @Autowired
    CatalogoVistaBI catalogoVistaBI;

    @Autowired
    InfoVistaBI infoVistaBI;

    @Autowired
    FilasBI FilaBI;

    @Autowired
    ContingenciaBI contingenciaBI;

    @Autowired
    UsuarioExternoBI usuarioexternoBI;

    @Autowired
    TelSucursalBI telsucursalBI;

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    ExternoPuestoBI externoPuestoBI;

    @Autowired
    FormatoMetodo7sBI formatoMetodo7sBI;

    @Autowired
    CedulaBI cedulaBI;

    @Autowired
    EstandPuestoBI estandPuestoBI;

    @Autowired
    ProgLimpiezaBI progLimpiezaBI;

    @Autowired
    ArchivoUrlBI archivoUrlBI;

    @Autowired
    BitacoraMantenimientoBI bitacoraMantenimientoBI;

    @Autowired
    ProveedorLoginBI proveedorLoginBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    TicketCuadrillaBI ticketCuadrillaBI;

    @Autowired
    FotoPlantillaBI fotoPlantillaBI;

    @Autowired
    CatalogoMinuciasBI catalogoMinuciasBI;

    @Autowired
    FotosAntesDespuesBI fotosAntesDespuesBI;

    @Autowired
    DepuraActFijoBI depuraActFijoBI;

    @Autowired
    ActivoFijoBI activoFijoBI;

    @Autowired
    AreaApoyoBI areaApoyoBI;

    @Autowired
    GuiaDHLBI guiaDHLBI;

    @Autowired
    CartaAsignacionAFBI cartaAsignacionAFBI;

    @Autowired
    PuestoBI puestoBI;

    private static final Logger logger = LogManager.getLogger(ServiciosModificaCatalogos.class);

    // http://localhost:8080/migestion/catalogosService/modificaProducto.json?id=<?>&idPadre=<?>&desc=<?>
    @RequestMapping(value = "/modificaProducto", method = RequestMethod.GET)
    public @ResponseBody
    boolean modificaProducto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;
        try {
            TipificacionDTO tipificacionDTO = new TipificacionDTO();
            tipificacionDTO.setIdOpeProd(Integer.valueOf(request.getParameter("id")));
            tipificacionDTO.setDesc(request.getParameter("desc"));
            tipificacionDTO.setPadre(Integer.valueOf(request.getParameter("idPadre")));
            res = productoBi.actualiza(tipificacionDTO);
        } catch (Exception e) {
            //logger.info("Ocurrio algo: ");
            return false;
        }

        return res;
    }

    // http://localhost:8080/migestion/catalogosService/modificaOperacion.json?id=<?>&idPadre=<?>&desc=<?>
    @RequestMapping(value = "/modificaOperacion", method = RequestMethod.GET)
    public @ResponseBody
    boolean modificaOperacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;
        try {
            TipificacionDTO tipificacionDTO = new TipificacionDTO();
            tipificacionDTO.setIdOpeProd(Integer.valueOf(request.getParameter("id")));
            tipificacionDTO.setDesc(request.getParameter("desc"));
            tipificacionDTO.setPadre(Integer.valueOf(request.getParameter("idPadre")));
            res = operacionBI.actualiza(tipificacionDTO);
        } catch (Exception e) {
            //logger.info("Ocurrio algo: ");
            return false;
        }

        return res;
    }

    // http://localhost:8080/migestion/catalogosService/updateParametro.json?setActivo=<?>&setClave=<?>&setValor=<?>
    @RequestMapping(value = "/updateParametro", method = RequestMethod.GET)
    public ModelAndView updateParametro(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("setActivo");
        String clave = request.getParameter("setClave");
        String valor = request.getParameter("setValor");

        ParametroDTO parametro = new ParametroDTO();
        parametro.setActivo(Integer.parseInt(activo));
        parametro.setClave(clave);
        parametro.setValor(valor);
        boolean res = parametroBI.actualizaParametro(parametro);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "PARAMETRO MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/migestion/catalogosService/updateCeco.json?idCeco=<?>&activo=<?>&calle=<?>&ciudad=<?>&cp=<?>&descCeco=<?>&faxContacto=<?>&idCanal=<?>&idCecoSuperior=<?>&idEstado=<?>&idNegocio=<?>&idNivel=<?>&idPais=<?>&nombreContacto=<?>&puestoContacto=<?>&telefonoContacto=<?>&fecha=<?>&usuarioMod=<?>
    @RequestMapping(value = "/updateCeco", method = RequestMethod.GET)
    public ModelAndView updateCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("activo");
        String calle = new String(request.getParameter("calle").getBytes("ISO-8859-1"), "UTF-8");
        String ciudad = new String(request.getParameter("ciudad").getBytes("ISO-8859-1"), "UTF-8");
        String cp = new String(request.getParameter("cp").getBytes("ISO-8859-1"), "UTF-8");
        String idCeco = request.getParameter("idCeco");
        String descCeco = new String(request.getParameter("descCeco").getBytes("ISO-8859-1"), "UTF-8");
        String faxContacto = new String(request.getParameter("faxContacto").getBytes("ISO-8859-1"), "UTF-8");
        String idCanal = request.getParameter("idCanal");
        String idCecoSuperior = request.getParameter("idCecoSuperior");
        String idEstado = request.getParameter("idEstado");
        String idNegocio = request.getParameter("idNegocio");
        String idNivel = request.getParameter("idNivel");
        String idPais = request.getParameter("idPais");
        String nombreContacto = new String(request.getParameter("nombreContacto").getBytes("ISO-8859-1"), "UTF-8");
        String puestoContacto = new String(request.getParameter("puestoContacto").getBytes("ISO-8859-1"), "UTF-8");
        String telefonoContacto = new String(request.getParameter("telefonoContacto").getBytes("ISO-8859-1"), "UTF-8");
        String fecha = request.getParameter("fecha");
        String usuarioMod = request.getParameter("usuarioMod");

        CecoDTO ceco = new CecoDTO();
        ceco.setActivo(Integer.parseInt(activo));
        ceco.setCalle(calle);
        ceco.setCiudad(ciudad);
        ceco.setCp(cp);
        ceco.setDescCeco(descCeco);
        ceco.setFaxContacto(faxContacto);
        ceco.setIdCanal(Integer.parseInt(idCanal));
        ceco.setIdCeco(idCeco);
        ceco.setIdCecoSuperior(Integer.parseInt(idCecoSuperior));
        ceco.setIdEstado(Integer.parseInt(idEstado));
        ceco.setIdNegocio(Integer.parseInt(idNegocio));
        ceco.setIdNivel(Integer.parseInt(idNivel));
        ceco.setIdPais(Integer.parseInt(idPais));
        ceco.setNombreContacto(nombreContacto);
        ceco.setPuestoContacto(puestoContacto);
        ceco.setTelefonoContacto(telefonoContacto);
        ceco.setFechaModifico(fecha);
        ceco.setUsuarioModifico(usuarioMod);
        boolean res = cecoBI.actualizaCeco(ceco);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "CECO ACTUALIZADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/migestion/catalogosService/updateTarea.json?nombre=<?>&idTarea=<?>&estatus=<?>&statusVac=<?>&idUsuario=<?>&tipoTarea=<?>&commit=<?>&pkTarea=<?>
    @RequestMapping(value = "/updateTarea", method = RequestMethod.GET)
    public ModelAndView updateTarea(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String nombre = request.getParameter("nombre");
            String idTarea = request.getParameter("idTarea");
            String estatus = request.getParameter("estatus");
            String statusVac = request.getParameter("statusVac");
            String idUsuario = request.getParameter("idUsuario");
            String tipoTarea = request.getParameter("tipoTarea");
            String commit = request.getParameter("commit");
            String pkTarea = request.getParameter("pkTarea");

            TareaActDTO tareaDTO = new TareaActDTO();
            tareaDTO.setNombreTarea(nombre);
            tareaDTO.setIdTarea(Integer.parseInt(idTarea));
            tareaDTO.setEstatus(Integer.parseInt(estatus));
            tareaDTO.setEstatusVac(Integer.parseInt(statusVac));
            tareaDTO.setIdUsuario(Integer.parseInt(idUsuario));
            tareaDTO.setTipoTarea(tipoTarea);
            tareaDTO.setCommit(Integer.parseInt(commit));
            tareaDTO.setIdPkTarea(Integer.parseInt(pkTarea));

            boolean respuesta = tareaBI.actualizaTareas(tareaDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TAREA ACTUALIZADA CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updatePeriodo.json?descripcion=<?>&commit=<?>&idPeriodo=<?>
    @RequestMapping(value = "/updatePeriodo", method = RequestMethod.GET)
    public ModelAndView updatePeriodo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPeriodo = request.getParameter("idPeriodo");
            String commit = request.getParameter("commit");
            String descripcion = request.getParameter("descripcion");

            boolean respuesta = periodoBI.actualizaPeriodo(idPeriodo, descripcion, Integer.parseInt(commit));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERIODO ACTUALIZADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updateUsuario.json?idUsuario=<?>&idPuesto=<?>&idCeco=<?>&nombre=<?>&activo=<?>&fecha=<?>
    @RequestMapping(value = "/updateUsuario", method = RequestMethod.GET)
    public ModelAndView updateUsuario(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPuesto = request.getParameter("idPuesto");
            String idCeco = request.getParameter("idCeco");
            String nombre = request.getParameter("nombre");
            String activo = request.getParameter("activo");
            String fecha = request.getParameter("fecha");

            Usuario_ADTO usuario = new Usuario_ADTO();
            usuario.setIdUsuario(Integer.parseInt(idUsuario));
            usuario.setIdPuesto(Integer.parseInt(idPuesto));
            usuario.setIdCeco(idCeco);
            usuario.setNombre(nombre);
            usuario.setActivo(Integer.parseInt(activo));
            usuario.setFecha(fecha);

            boolean respuesta = usuarioabi.actualizaUsuario(usuario);

            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
            mv.addObject(respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updatePeriodoReporte.json?idPeriodo=<?>&idReporte=<?>&idPerRep=<?>&horario=<?>&commit=<?>
    @RequestMapping(value = "/updatePeriodoReporte", method = RequestMethod.GET)
    public ModelAndView updatePeriodoReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPeriodo = request.getParameter("idPeriodo");
            String idReporte = request.getParameter("idReporte");
            String horario = request.getParameter("horario");
            String idPerRep = request.getParameter("idPerRep");
            String commit = request.getParameter("commit");

            boolean respuesta = periodoReporteBI.actualizaPeriodoReporte(Integer.parseInt(idPeriodo), Integer.parseInt(idReporte), horario, Integer.parseInt(idPerRep), Integer.parseInt(commit));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERIODO REPORTE ACTUALIZADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            ////logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updateReporte.json?idReporte=<?>&nombre=<?>&commit=<?>
    @RequestMapping(value = "/updateReporte", method = RequestMethod.GET)
    public ModelAndView updateReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idReporte = request.getParameter("idReporte");
            String nombre = request.getParameter("nombre");
            String commit = request.getParameter("commit");

            boolean respuesta = reporteBI.actualizaReporte(Integer.parseInt(idReporte), nombre, Integer.parseInt(commit));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "REPORTE ACTUALIZADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updateGeografia.json?idCeco=<?>&idRegion=<?>&nombreR=<?>&idZona=<?>&nombreZ=<?>&idTerritorio=<?>&nombreT=<?>&commit=<?>
    @RequestMapping(value = "/updateGeografia", method = RequestMethod.GET)
    public ModelAndView updateGeografia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");
            String idRegion = request.getParameter("idRegion");
            String nombreR = request.getParameter("nombreR");
            String idZona = request.getParameter("idZona");
            String nombreZ = request.getParameter("nombreZ");
            String idTerritorio = request.getParameter("idTerritorio");
            String nombreT = request.getParameter("nombreT");
            String commit = request.getParameter("commit");

            GeografiaDTO geoDTO = new GeografiaDTO();

            geoDTO.setIdCeco(idCeco);
            geoDTO.setIdRegion(Integer.parseInt(idRegion));
            geoDTO.setNombreRegion(nombreR);
            geoDTO.setIdZona(Integer.parseInt(idZona));
            geoDTO.setNombreZona(nombreZ);
            geoDTO.setIdTerritorio(Integer.parseInt(idTerritorio));
            geoDTO.setNombreTerritorio(nombreT);
            geoDTO.setCommit(Integer.parseInt(commit));

            boolean respuesta = geoBI.actualizaGeografia(geoDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "GEOGRAFIA ACTUALIZADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updatePerfil.json?idPerfil=<?>&setDescripcion=<?>
    @RequestMapping(value = "/updatePerfil", method = RequestMethod.GET)
    public ModelAndView updatePerfil(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String idP = request.getParameter("idPerfil");
        String descripcion = new String(request.getParameter("setDescripcion").getBytes("ISO-8859-1"), "UTF-8");

        PerfilDTO perfil = new PerfilDTO();
        perfil.setDescripcion(descripcion);
        perfil.setIdPerfil(Integer.parseInt(idP));
        boolean res = perfilbi.actualizaPerfil(perfil);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ID DE PERFIL ACTUALIZADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }


    // http://localhost:8080/migestion/catalogosService/updateNotificaAf.json?id=<?>&ceco=<?>&compromisoP=<?>&avanceP=<?>&compromisoA=<?>&avanceA=<?>&fecha=<?>
    @RequestMapping(value = "/updateNotificaAf", method = RequestMethod.GET)
    public ModelAndView updateNotificaAf(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String id = request.getParameter("id");
        String ceco = request.getParameter("ceco");
        String compromisoP = request.getParameter("compromisoP");
        String avanceP = request.getParameter("avanceP");
        String compromisoA = request.getParameter("compromisoA");
        String avanceA = request.getParameter("avanceA");
        String fecha = request.getParameter("fecha");

        NotificaAfDTO objActualizar = new NotificaAfDTO();
        objActualizar.setIdNotifica(Integer.parseInt(id));
        objActualizar.setCeco(ceco);
        objActualizar.setCompromisoP(compromisoP);
        objActualizar.setAvanceP(avanceP);
        objActualizar.setCompromisoA(compromisoA);
        objActualizar.setAvanceA(avanceA);
        objActualizar.setFecha(fecha);

        boolean res = asesoresBi.actualiza(objActualizar);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "Registro de Tabla GETANOTIFICA_AF actualizado ");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/migestion/catalogosService/updateCatalogoVista.json?idCatVista=<?>&descripcion=<?>&moduloPadre=<?>
    @RequestMapping(value = "/updateCatalogoVista", method = RequestMethod.GET)
    public ModelAndView updateCatalogoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int idCatVista = Integer.parseInt(request.getParameter("idCatVista"));
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
            int moduloPadre = Integer.parseInt(request.getParameter("moduloPadre"));

            CatalogoVistaDTO catVista = new CatalogoVistaDTO();
            catVista.setIdCatVista(idCatVista);
            catVista.setDescripcion(descripcion);
            catVista.setIdModPadre(moduloPadre);

            boolean res = catalogoVistaBI.actualizaCatalogoVista(catVista);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Vista Modificada: ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            ////logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updateInfoVista.json?idInfoVista=<?>&idCatVista=<?>&idUsuario=<?>&plataforma=<?>&fecha=<?>
    // http://10.51.209.7:8080/migestion/catalogosService/updateInfoVista.json?idInfoVista=98&idCatVista=2&idUsuario=189870&plataforma=1&fecha=2018-08-13 17:23:39:461
    @RequestMapping(value = "/updateInfoVista", method = RequestMethod.GET)
    public ModelAndView updateInfoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int idInfoVista = Integer.parseInt(request.getParameter("idInfoVista"));
            int idCatVista = Integer.parseInt(request.getParameter("idCatVista"));
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            int plataforma = Integer.parseInt(request.getParameter("plataforma"));
            String fecha = new String(request.getParameter("fecha").getBytes("ISO-8859-1"), "UTF-8");

            InfoVistaDTO infoVista = new InfoVistaDTO();
            infoVista.setIdInfoVista(idInfoVista);
            infoVista.setIdCatVista(idCatVista);
            infoVista.setIdUsuario(idUsuario);
            infoVista.setPlataforma(plataforma);
            infoVista.setFecha(fecha);

            boolean res = infoVistaBI.actualizaInfoVista(infoVista);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Info Visita modificada: ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updateFilas.json?idFila=<?>&numEconomico=<?>&idgrupo=<?>
    @RequestMapping(value = "/updateFilas", method = RequestMethod.GET)
    public ModelAndView updateFilas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idFila = request.getParameter("idFila");
            String numEconomico = request.getParameter("numEconomico");
            String idGrupo = request.getParameter("idGrupo");

            FilasDTO filas = new FilasDTO();
            filas.setIdFila(Integer.parseInt(idFila));
            filas.setNumEconomico(Integer.parseInt(numEconomico));;
            filas.setIdGrupo(Integer.parseInt(idGrupo));

            boolean res = FilaBI.actualiza(filas);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Filas Modificadas ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/CambiaPuesto.json?idCeco=<?>
    @RequestMapping(value = "/CambiaPuesto", method = RequestMethod.GET)
    public ModelAndView CambiaPuesto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");

            boolean res = contingenciaBI.CambiaPuesto(Integer.parseInt(idCeco));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Modifica Puestos");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updateEmpleadoExterno.json?numEmpleado=<?>&nombre=<?>&aPaterno=<?>&aMaterno=<?>&direccion=<?>&numCasa=<?>&numCelular=<?>&RFCEmpleado=<?>&cencosNum=<?>&puesto=<?>&fechaIngreso=<?>&fechaBaja=<?>&funcion=<?>&usuarioID=<?>&empSituacion=<?>&honorarios=<?>&email=<?>&numEmergencia=<?>&perEmergencia=<?>&numExt=<?>&sueldo=<?>&horarioIn=<?>&horarioFin=<?>&semestre=<?>&actividades=<?>&liberacion=<?>&elaboracion=<?>&llave=<?>&red=<?>&lotus=<?>&credencial=<?>&folioDataSec=<?>&pais=<?>&nombreCompleto=<?>&asistencia=<?>&curp=<?>&
    @RequestMapping(value = "/updateEmpleadoExterno", method = RequestMethod.GET)
    public ModelAndView updateEmpleadoExterno(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int numEmpleado = 0;
            if (request.getParameter("numEmpleado") != null) {
                if (!request.getParameter("numEmpleado").equals("")) {
                    numEmpleado = Integer.parseInt(request.getParameter("numEmpleado"));
                }
            }
            String nombre = request.getParameter("nombre");
            String aPaterno = request.getParameter("aPaterno");
            String aMaterno = request.getParameter("aMaterno");
            String direccion = request.getParameter("direccion");
            int numCasa = 0;
            if (request.getParameter("numCasa") != null) {
                if (!request.getParameter("numCasa").equals("")) {
                    numCasa = Integer.parseInt(request.getParameter("numCasa"));
                }
            }
            int numCelular = 0;
            if (request.getParameter("numCelular") != null) {
                if (!request.getParameter("numCelular").equals("")) {
                    numCelular = Integer.parseInt(request.getParameter("numCelular"));
                }
            }
            String rFCEmpleado = request.getParameter("rFCEmpleado");
            String cencosNum = request.getParameter("cencosNum");
            int puesto = 0;
            if (request.getParameter("puesto") != null) {
                if (!request.getParameter("puesto").equals("")) {
                    puesto = Integer.parseInt(request.getParameter("puesto"));
                }
            }
            String fechaIngreso = request.getParameter("fechaIngreso");
            String fechaBaja = request.getParameter("fechaBaja");
            String funcion = request.getParameter("funcion");
            int usuarioID = 0;
            if (request.getParameter("usuarioID") != null) {
                if (!request.getParameter("usuarioID").equals("")) {
                    usuarioID = Integer.parseInt(request.getParameter("usuarioID"));
                }
            }
            int empSituacion = 0;
            if (request.getParameter("empSituacion") != null) {
                if (!request.getParameter("empSituacion").equals("")) {
                    empSituacion = Integer.parseInt(request.getParameter("empSituacion"));
                }
            }
            int honorarios = 0;
            if (request.getParameter("honorarios") != null) {
                if (!request.getParameter("honorarios").equals("")) {
                    honorarios = Integer.parseInt(request.getParameter("honorarios"));
                }
            }
            String email = request.getParameter("email");
            int numEmergencia = 0;
            if (request.getParameter("numEmergencia") != null) {
                if (!request.getParameter("numEmergencia").equals("")) {
                    numEmergencia = Integer.parseInt(request.getParameter("numEmergencia"));
                }
            }
            String perEmergencia = request.getParameter("perEmergencia");
            int numExt = 0;
            if (request.getParameter("numExt") != null) {
                if (!request.getParameter("numExt").equals("")) {
                    numExt = Integer.parseInt(request.getParameter("numExt"));
                }
            }
            String sueldo = "0";
            if (request.getParameter("sueldo") != null) {
                if (!request.getParameter("sueldo").equals("")) {
                    sueldo = request.getParameter("sueldo");
                }
            }
            String horarioIn = request.getParameter("horarioIn");
            String horarioFin = request.getParameter("horarioFin");
            int semestre = 0;
            if (request.getParameter("semestre") != null) {
                if (!request.getParameter("semestre").equals("")) {
                    semestre = Integer.parseInt(request.getParameter("semestre"));
                }
            }
            String actividades = request.getParameter("actividades");
            String liberacion = request.getParameter("liberacion");
            String elaboracion = request.getParameter("elaboracion");
            int llave = 0;
            if (request.getParameter("llave") != null) {
                if (!request.getParameter("llave").equals("")) {
                    llave = Integer.parseInt(request.getParameter("llave"));
                }
            }
            int red = 0;
            if (request.getParameter("red") != null) {
                if (!request.getParameter("red").equals("")) {
                    red = Integer.parseInt(request.getParameter("red"));
                }
            }
            int lotus = 0;
            if (request.getParameter("lotus") != null) {
                if (!request.getParameter("lotus").equals("")) {
                    lotus = Integer.parseInt(request.getParameter("lotus"));
                }
            }
            int credencial = 0;
            if (request.getParameter("credencial") != null) {
                if (!request.getParameter("credencial").equals("")) {
                    credencial = Integer.parseInt(request.getParameter("credencial"));
                }
            }
            int folioDataSec = 0;
            if (request.getParameter("folioDataSec") != null) {
                if (!request.getParameter("folioDataSec").equals("")) {
                    folioDataSec = Integer.parseInt(request.getParameter("folioDataSec"));
                }
            }
            String pais = request.getParameter("pais");
            String nombreCompleto = request.getParameter("nombreCompleto");
            String asistencia = request.getParameter("asistencia");
            String curp = request.getParameter("curp");
            ExternoDTO externo = new ExternoDTO();

            externo.setNumEmpleado(numEmpleado);
            externo.setNombre(nombre);
            externo.setaPaterno(aPaterno);
            externo.setaMaterno(aMaterno);
            externo.setDireccion(direccion);
            externo.setNumCasa(numCasa);
            externo.setNumCelular(numCelular);
            externo.setRFCEmpleado(rFCEmpleado);
            externo.setCencosNum(cencosNum);
            externo.setPuesto(puesto);
            externo.setFechaIngreso(fechaIngreso);
            externo.setFechaBaja(fechaBaja);
            externo.setFuncion(funcion);
            externo.setUsuarioID(usuarioID);
            externo.setEmpSituacion(empSituacion);
            externo.setHonorarios(honorarios);
            externo.setEmail(email);
            externo.setNumEmergencia(numEmergencia);
            externo.setPerEmergencia(perEmergencia);
            externo.setNumExt(numExt);
            externo.setSueldo(sueldo);
            externo.setHorarioIn(horarioIn);
            externo.setHorarioFin(horarioFin);
            externo.setSemestre(semestre);
            externo.setActividades(actividades);
            externo.setLiberacion(liberacion);
            externo.setElaboracion(elaboracion);
            externo.setLlave(llave);
            externo.setRed(red);
            externo.setLotus(lotus);
            externo.setCredencial(credencial);
            externo.setFolioDataSec(folioDataSec);
            externo.setPais(pais);
            externo.setNombreCompleto(nombreCompleto);
            externo.setAsistencia(asistencia);
            externo.setCurp(curp);

            boolean resp = usuarioexternoBI.actualizaUsuario(externo);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Usuario Actualizado ");
            mv.addObject("res", resp);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/updateTelefonosucursal.json?idFila=<?>&numEconomico=<?>&idgrupo=<?>
    @RequestMapping(value = "/updateTelefonosucursal", method = RequestMethod.GET)
    public ModelAndView updateTelefonosucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idTelefono = Integer.parseInt(request.getParameter("idTelefono"));
            String idCeco = request.getParameter("idCeco");
            String telefono = request.getParameter("telefono");
            String proveedor = request.getParameter("proveedor");
            String estatus = request.getParameter("estatus");

            TelSucursalDTO tel = new TelSucursalDTO();

            tel.setIdTelefono(idTelefono);
            tel.setIdCeco(idCeco);
            tel.setTelefono(telefono);
            tel.setProveedor(proveedor);
            tel.setEstatus(estatus);

            boolean res = telsucursalBI.actualizatelefono(tel);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Filas Modificadas ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/migestion/catalogosService/updateCatalogoIncidencia.json?idTipo=<?>&servicio=<?>&ubicacion=<?>&incidencia=<?>&plantilla=<?>&status=<?>
    @RequestMapping(value = "/updateCatalogoIncidencia", method = RequestMethod.GET)
    public ModelAndView updateCatalogoIncidencia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idTipo = Integer.parseInt(request.getParameter("idTipo"));
            String servicio = request.getParameter("servicio");
            String ubicacion = request.getParameter("ubicacion");
            String incidencia = request.getParameter("incidencia");
            String plantilla = request.getParameter("plantilla");
            String status = request.getParameter("status");

            IncidenciasDTO incid = new IncidenciasDTO();

            incid.setIdTipo(idTipo);
            incid.setServicio(servicio);
            incid.setUbicacion(ubicacion);
            incid.setIncidencia(incidencia);
            incid.setPlantilla(plantilla);
            incid.setStatus(status);

            boolean res = incidenciasBI.actualiza(incid);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Filas Modificadas");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/updateCatalogoProveedores.json?menu=<?>&idProveedor=<?>&nombreCorto=<?>&razonSocial=<?>&status=<?>
    @RequestMapping(value = "/updateCatalogoProveedores", method = RequestMethod.GET)
    public ModelAndView updateCatalogoProveedores(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String menu = request.getParameter("menu");
            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            String nombreCorto = new String(request.getParameter("nombreCorto").getBytes("ISO-8859-1"), "UTF-8");
            String razonSocial = new String(request.getParameter("razonSocial").getBytes("ISO-8859-1"), "UTF-8");
            String status = request.getParameter("status");

            ProveedoresDTO proveedores = new ProveedoresDTO();

            proveedores.setMenu(menu);
            proveedores.setIdProveedor(idProveedor);
            proveedores.setNombreCorto(nombreCorto);
            proveedores.setRazonSocial(razonSocial);
            proveedores.setStatus(status);

            boolean res = proveedoresBI.actualiza(proveedores);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo En catalogo de proveedores");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/updatePuestoEmpleadoExterno.json?numEmpleado=<?>&puesto=<?>
    @RequestMapping(value = "/updatePuestoEmpleadoExterno", method = RequestMethod.GET)
    public ModelAndView updatePuestoEmpleadoExterno(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int numEmpleado = Integer.parseInt(request.getParameter("numEmpleado"));
            int puesto = Integer.parseInt(request.getParameter("puesto"));

            ExternoPuestoDTO externoPuesto = new ExternoPuestoDTO();

            externoPuesto.setNumEmpleado(numEmpleado);
            externoPuesto.setPuesto(puesto);

            boolean res = externoPuestoBI.actualizaPuestoExterno(externoPuesto);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo el puesto de usuarios Externos");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

//localhost:8080/migestion/catalogosService/updatePuestoEmpleadoExte.json?numEmpleado=<?>&puesto=<?>
    @RequestMapping(value = "/updatePuestoEmpleadoExte", method = RequestMethod.GET)
    public ModelAndView updatePuestoEmpleadoExte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int numEmpleado = Integer.parseInt(request.getParameter("numEmpleado"));
            int puesto = Integer.parseInt(request.getParameter("puesto"));

            ExternoPuestoDTO externoPuesto = new ExternoPuestoDTO();

            externoPuesto.setNumEmpleado(numEmpleado);
            externoPuesto.setPuesto(puesto);

            boolean res = externoPuestoBI.actualizaPuesto(numEmpleado, puesto);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo el puesto de usuarios Externos");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

//localhost:8080/migestion/catalogosService/updateFormatoMedicionMetod.json?idTab=<?>&idusuario=<?>&idSucursal=<?>&preg1=<?>&preg2=<?>&preg3=<?>&puesto=<?>
    @RequestMapping(value = "/updateFormatoMedicionMetod", method = RequestMethod.GET)
    public ModelAndView updateFormatoMedicionMetod(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idTabla = Integer.parseInt(request.getParameter("idtab"));
            int idusuario = Integer.parseInt(request.getParameter("idusuario"));
            int idSucursal = Integer.parseInt(request.getParameter("idSucursal"));
            int preg1 = Integer.parseInt(request.getParameter("preg1"));
            int preg2 = Integer.parseInt(request.getParameter("preg2"));
            int preg3 = Integer.parseInt(request.getParameter("preg3"));
            String puesto = request.getParameter("puesto");

            FormatoMetodo7sDTO formato = new FormatoMetodo7sDTO();

            formato.setIdtab(idTabla);
            formato.setIdusuario(idusuario);
            formato.setIdSucursal(idSucursal);
            formato.setPuesto(puesto);
            formato.setPreg1(preg1);
            formato.setPreg2(preg2);
            formato.setPreg3(preg3);

            boolean res = formatoMetodo7sBI.actualiza(formato);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo La tabla de Formatos 7s");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

//				 http://localhost:8080/migestion/catalogosService/updateCedula.json?idusuario=<?>&idSucursal=<?>&fecha=<?>&rutafoto=<?>&rutaFirmaAcepta=<?>&rutaFirmaJefe=<?>&tipoSangre=<?>&idSeguroSoc=<?>&alergia=<?>&descalergia=<?>&enfermedad=<?>&descEnfermedad=<?>&tratamiento=<?>&descTratamiento=<?>&descTratamiento=<?>&contactoEmerg=<?>&telContacto=<?>&telSocio=<?>
    @RequestMapping(value = "/updateCedula", method = RequestMethod.GET)
    public ModelAndView updateCedula(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idUsuario = Integer.parseInt(request.getParameter("idusuario"));
            int idSuc = Integer.parseInt(request.getParameter("idSucursal"));
            String fecha = request.getParameter("fecha");
            String rutafoto = request.getParameter("rutafoto");
            String rutaFirmaAcepta = request.getParameter("rutaFirmaAcepta");
            String rutaFirmaJefe = request.getParameter("rutaFirmaJefe");
            String tipoSangre = request.getParameter("tipoSangre");
            String idSeguroSoc = request.getParameter("idSeguroSoc");
            int alergia = Integer.parseInt(request.getParameter("alergia"));
            String descalergia = request.getParameter("descalergia");
            int enfermedad = Integer.parseInt(request.getParameter("enfermedad"));
            String descEnfermedad = request.getParameter("descEnfermedad");
            int tratamiento = Integer.parseInt(request.getParameter("tratamiento"));
            String descTratamiento = request.getParameter("descTratamiento");
            String telContacto = request.getParameter("telContacto");
            String contactoEmerg = request.getParameter("contactoEmerg");
            String telSocio = request.getParameter("telSocio");

            CedulaDTO ced = new CedulaDTO();

            ced.setIdUsuario(idUsuario);
            ced.setIdSuc(idSuc);
            ced.setFecha(fecha);
            ced.setRutafoto(rutafoto);
            ced.setRutaFirmaAcepta(rutaFirmaAcepta);
            ced.setRutaFirmaJefe(rutaFirmaJefe);
            ced.setTipoSangre(tipoSangre);
            ced.setIdSeguroSoc(idSeguroSoc);
            ced.setAlergia(alergia);
            ced.setDescalergia(descalergia);
            ced.setEnfermedad(enfermedad);
            ced.setDescEnfermedad(descEnfermedad);
            ced.setTratamiento(tratamiento);
            ced.setDescTratamiento(descTratamiento);
            ced.setContactoEmerg(contactoEmerg);
            ced.setTelContacto(telContacto);
            ced.setTelSocio(telSocio);

            boolean res = cedulaBI.actualiza(ced);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Modifico En Tabla Cedula ");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/updateEstandPuesto.json?idPuesto=<?>idUsuario=<?>&ceco=<?>&bandera=<?>
    @RequestMapping(value = "/updateEstandPuesto", method = RequestMethod.GET)
    public ModelAndView updateEstandPuesto(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idPuesto = Integer.parseInt(request.getParameter("idPuesto"));
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            String ceco = request.getParameter("ceco");
            int bandera = Integer.parseInt(request.getParameter("bandera"));

            EstandPuestoDTO ep = new EstandPuestoDTO();

            ep.setIdPuesto(idPuesto);
            ep.setIdUsuario(idUsuario);
            ep.setCeco(ceco);
            ep.setBandera(bandera);

            boolean res = estandPuestoBI.actualiza(ep);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo En Tabla Estandarizacion por puesto ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }
    //localhost:8080/migestion/catalogosService/updateProgLimpieza.json?idProg=<?>&idUsuario=<?>

    @RequestMapping(value = "/updateProgLimpieza", method = RequestMethod.GET)
    public ModelAndView updateProgLimpieza(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idProg = Integer.parseInt(request.getParameter("idProg"));
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            String ceco = request.getParameter("ceco");

            ProgLimpiezaDTO pl = new ProgLimpiezaDTO();
            pl.setIdProg(idProg);
            pl.setIdUsuario(idUsuario);
            pl.setCeco(ceco);

            boolean res = progLimpiezaBI.actualiza(pl);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo En Tabla Programa de limpieza ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/updateArchivoUrl.json?idArch=<?>&nombre=<?>&ruta=<?>&version=<?>
    @RequestMapping(value = "/updateArchivoUrl", method = RequestMethod.GET)
    public ModelAndView updateArchivoUrl(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idArch = Integer.parseInt(request.getParameter("idArch"));
            String nombre = request.getParameter("nombre");
            String ruta = request.getParameter("ruta");
            String version = request.getParameter("version");

            ArchivosUrlDTO au = new ArchivosUrlDTO();

            au.setIdArch(idArch);
            au.setNombre(nombre);
            au.setRuta(ruta);
            au.setVersion(version);

            boolean res = archivoUrlBI.actualiza(au);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo En Tabla Archivo URLS");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    @RequestMapping(value = "/modificaBitacoraMnto", method = RequestMethod.GET)
    public @ResponseBody
    int modificaBitacoraMnto(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {
        int res = 0;
        try {
            BitacoraMntoDTO bitacora = new BitacoraMntoDTO();
            bitacora.setIdAmbito(Integer.parseInt(request.getParameter("fiambito_id")));
            bitacora.setIdUsuario(Integer.parseInt(request.getParameter("fiid usuario_id")));
            bitacora.setIdCeco(request.getParameter("fcid_ceco"));
            bitacora.setIdFolio(request.getParameter("fiid_folio"));
            bitacora.setFechaVisita(request.getParameter("fechaVisita"));
            bitacora.setNombreProvee(request.getParameter("nombreProveedor"));
            bitacora.setTipoMantento(request.getParameter("tipoMantenimiento"));
            bitacora.setFechaSolicitud(request.getParameter("fechaSolicit"));
            bitacora.setHoraEntrada(request.getParameter("horaEntrada"));
            bitacora.setHoraSalida(request.getParameter("horaSalida"));
            bitacora.setNumPersonas(request.getParameter("numPersonas"));
            bitacora.setDetalle(request.getParameter("detalle"));
            bitacora.setRutaProveedor(request.getParameter("rutaPrestador"));
            bitacora.setRutaGerente(request.getParameter("rutaGerente"));

            res = bitacoraMantenimientoBI.actualizaBitacoraMnto(bitacora);
        } catch (Exception e) {
            //logger.info("Ocurrio algo al actualizar la información de la bitacora de mantenimiento:"+e);
        }
        return res;
    }

    //localhost:8080/migestion/catalogosService/updateLoginProveedor.json?idProveedor=<?>&passw=<?>
    @RequestMapping(value = "/updateLoginProveedor", method = RequestMethod.GET)
    public ModelAndView updateLoginProveedor(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            String passw = request.getParameter("passw");

            ProveedorLoginDTO log = new ProveedorLoginDTO();

            log.setIdProveedor(idProveedor);
            log.setPassw(passw);

            boolean res = proveedorLoginBI.actualiza(log);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo En Tabla Login Proveedores ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }
    //localhost:8080/migestion/catalogosService/updateCuadrilla.json?idCuadrilla=<?>idProveedor=<?>&zona=<?>&liderCuad=<?>&correo=<?>&segundo=<?>&passw=<?>

    @RequestMapping(value = "/updateCuadrilla", method = RequestMethod.GET)
    public ModelAndView updateCuadrilla(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCuadrilla = Integer.parseInt(request.getParameter("idCuadrilla"));
            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            int zona = Integer.parseInt(request.getParameter("zona"));
            String liderCuad = request.getParameter("liderCuad");
            String correo = request.getParameter("correo");
            String segundo = request.getParameter("segundo");
            String passw = request.getParameter("passw");

								CuadrillaDTO cua=new CuadrillaDTO();

								cua.setIdCuadrilla(idCuadrilla);
								cua.setIdProveedor(idProveedor);
								cua.setZona(zona);
							    cua.setLiderCuad(liderCuad);
							    cua.setCorreo(correo);
							    cua.setSegundo(segundo);
							    cua.setPassw(passw);


								boolean res = cuadrillaBI.actualiza(cua);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Actualizo En Tabla Cuadrilla ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateTicketCuadrilla.json?idCuadrilla=<?>idProveedor=<?>&zona=<?>&liderCuad=<?>&correo=<?>&segundo=<?>
						@RequestMapping(value="/updateTicketCuadrilla", method = RequestMethod.GET)
						public ModelAndView updateTicketCuadrilla(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								int idtkCuadri=Integer.parseInt(request.getParameter("idtkCuadri"));
								int idCuadrilla=Integer.parseInt(request.getParameter("idCuadrilla"));
								int idProveedor=Integer.parseInt(request.getParameter("idProveedor"));
								String ticket=request.getParameter("ticket");
								int  zona=Integer.parseInt(request.getParameter("zona"));
								String liderCua=request.getParameter("liderCuad");
								int status=Integer.parseInt(request.getParameter("status"));

								TicketCuadrillaDTO tcua=new TicketCuadrillaDTO();

								tcua.setIdtkCuadri(idtkCuadri);
								tcua.setIdCuadrilla(idCuadrilla);
								tcua.setIdProveedor(idProveedor);
								tcua.setTicket(ticket);
								tcua.setZona(zona);
							    tcua.setLiderCua(liderCua);
							    tcua.setStatus(status);

								boolean res = ticketCuadrillaBI.actualiza(tcua);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Actualizo En Tabla tk Cuadrilla ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateFotoPlantilla.json?ceco=<?>usuario=<?>&ruta=<?>
						@RequestMapping(value="/updateFotoPlantilla", method = RequestMethod.GET)
						public ModelAndView updateFotoPlantilla(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								String ceco=request.getParameter("ceco");
								int usuario=Integer.parseInt(request.getParameter("usuario"));
								String ruta=request.getParameter("ruta");

								FotoPlantillaDTO fotop=new FotoPlantillaDTO();

								fotop.setCeco(ceco);
								fotop.setUsuario(usuario);
								fotop.setRuta(ruta);

								boolean res = fotoPlantillaBI.actualiza(fotop);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Inserto En Tabla Foto Plantilla ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updatebitacoCmnt.json?idUsuario=<?>&idCeco=<?>&idFolio=<?>&fechaVisita=<?>&nombreProvee=<?>&tipoMantento=<?>&fechaSolicitud=<?>&horaEntrada=<?>&horaSalida=<?>&numPersonas=<?>&detalle=<?>&rutaProveedor=<?>&rutaGerente=<?>
						@RequestMapping(value = "/updatebitacoCmnt", method = RequestMethod.GET)
						public @ResponseBody ModelAndView updatebitacoCmnt(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException {
																int respuesta = 0;
																try {
																	BitacoraMntoDTO bitacora = new BitacoraMntoDTO();
																	bitacora.setIdAmbito(Integer.parseInt(request.getParameter("fiambito_id")));
																	bitacora.setIdUsuario(Integer.parseInt(request.getParameter("idUsuario;")));
																	bitacora.setIdCeco(request.getParameter("idCeco;"));
																	bitacora.setIdFolio(request.getParameter("idFolio"));
																	bitacora.setFechaVisita(request.getParameter("fechaVisita"));
																	bitacora.setNombreProvee(request.getParameter("nombreProvee"));
																	bitacora.setTipoMantento(request.getParameter("tipoMantento"));
																	bitacora.setFechaSolicitud(request.getParameter("fechaSolicitud"));
																	bitacora.setHoraEntrada(request.getParameter("horaEntrada"));
																	bitacora.setHoraSalida(request.getParameter("horaSalida"));
																	bitacora.setNumPersonas(request.getParameter("numPersonas"));
																	bitacora.setDetalle(request.getParameter("detalle"));
																	bitacora.setRutaProveedor(request.getParameter("rutaProveedor"));
																	bitacora.setRutaGerente(request.getParameter("rutaGerente"));

																	respuesta = bitacoraMantenimientoBI.actualizaBitacoraMnto(bitacora);


																	ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
																	mv.addObject("tipo", "Inserto En Tabla Bitacora Mtto ");
																	mv.addObject("res",respuesta);
																	return mv;

																} catch (Exception e) {
																	//logger.info(e);
																}
																return null;
															}

						//localhost:8080/migestion/catalogosService/updateMinucia.json?idUsu=<?>ceco=<?>&lugarEchos=<?>&fecha=<?>&direccion=<?>&vobo=<?>&destruye=<?>&testigo1=<?>&testigo2=<?>
						@RequestMapping(value="/updateMinucia", method = RequestMethod.GET)
						public ModelAndView updateMinucia(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								int idMinucia=Integer.parseInt(request.getParameter("idMinucia"));
								int idUsu=Integer.parseInt(request.getParameter("idUsu"));
								String ceco=request.getParameter("ceco");
								String lugarEchos=request.getParameter("lugarEchos");
								String fecha=request.getParameter("fecha");
								String direccion=request.getParameter("direccion");
								String vobo=request.getParameter("vobo");
								String destruye=request.getParameter("destruye");
								String testigo1=request.getParameter("testigo1");
								String testigo2=request.getParameter("testigo2");

								CatalogoMinuciasDTO cam=new CatalogoMinuciasDTO();

								cam.setIdMinucia(idMinucia);
								cam.setIdUsu(idUsu);
								cam.setCeco(ceco);
								cam.setLugarEchos(lugarEchos);
								cam.setFecha(fecha);
								cam.setDireccion(direccion);
								cam.setVobo(vobo);
								cam.setDestruye(destruye);
								cam.setTestigo1(testigo1);
								cam.setTestigo2(testigo2);


								boolean res = catalogoMinuciasBI.actualiza(cam);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Actualizo En Tabla Minucias ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateFotoAntDesp.json?idUsu=<?>ceco=<?>&lugarEchos=<?>&fecha=<?>&direccion=<?>&vobo=<?>&destruye=<?>&testigo1=<?>&testigo2=<?>
						@RequestMapping(value="/updateFotoAntDesp", method = RequestMethod.GET)
						public ModelAndView updateFotoAntDesp(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								int idEvento=Integer.parseInt(request.getParameter("idEvento"));
								String idCeco=request.getParameter("idCeco");
								String lugarAntes=request.getParameter("lugarAntes");
								String lugarDespues=request.getParameter("lugarDespues");
								String idEvidantes=request.getParameter("idEvidantes");
								String idEviddespues=request.getParameter("idEviddespues");
								String fechaAntes=request.getParameter("fechaAntes");
								String fechaDespues=request.getParameter("fechaDespues");

								FotosAntesDespuesDTO fa=new FotosAntesDespuesDTO();

								fa.setIdEvento(idEvento);
								fa.setIdCeco(idCeco);
								fa.setLugarAntes(lugarAntes);
								fa.setLugarDespues(lugarDespues);
								fa.setIdEvidantes(idEvidantes);
								fa.setIdEviddespues(idEviddespues);
								fa.setFechaAntes(fechaAntes);
								fa.setFechaDespues(fechaDespues);

								int res = fotosAntesDespuesBI.actualizaFotoAntesDesp(fa);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Actualizo En Tabla Foto Antes y despues ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateDepActFijo.json?idActivoFijo=<?>idusuario=<?>&ceco=<?>&ubicacion=<?>&involucrados=<?>&razonsoc=<?>&domicilio=<?>&responsable=<?>&fecha=<?>
						@RequestMapping(value="/updateDepActFijo", method = RequestMethod.GET)
						public ModelAndView updateDepActFijo(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								int idActivoFijo=Integer.parseInt(request.getParameter("idActivoFijo"));
								int idusuario=Integer.parseInt(request.getParameter("idusuario"));
								String ceco=request.getParameter("ceco");
							    String  ubicacion=request.getParameter("ubicacion");
								String involucrados=request.getParameter("involucrados");
								String razonsoc=request.getParameter("razonsoc");
								String rfc=request.getParameter("rfc");
								String domicilio=request.getParameter("domicilio");
								String responsable=request.getParameter("responsable");
								String testigo=request.getParameter("testigo");
								String fecha=request.getParameter("fecha");
								int precio=Integer.parseInt(request.getParameter("precio"));

								DepuraActFijoDTO dep=new DepuraActFijoDTO();

								dep.setIdusuario(idusuario);
								dep.setCeco(ceco);
								dep.setUbicacion(ubicacion);
								dep.setInvolucrados(involucrados);
								dep.setRazonsoc(razonsoc);
								dep.setRfc(rfc);
								dep.setDomicilio(domicilio);
								dep.setResponsable(responsable);
								dep.setTestigo(testigo);
								dep.setFecha(fecha);
								dep.setPrecio(precio);


								boolean res = depuraActFijoBI.actualiza(dep);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Se inserto en la tabla Depura Act Fijo ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateActFijo.json?depActivoFijo=<?>placa=<?>&ceco=<?>&descripcion=<?>&marca=<?>&serie=<?>
						@RequestMapping(value="/updateActFijo", method = RequestMethod.GET)
						public ModelAndView updateActFijo(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								int depActivoFijo=Integer.parseInt(request.getParameter("depActivoFijo"));
								String placa=request.getParameter("placa");
								String ceco=request.getParameter("ceco");
							    String  descripcion=request.getParameter("descripcion");
								String marca=request.getParameter("marca");
								String serie=request.getParameter("serie");

								ActivoFijoDTO af=new ActivoFijoDTO();

								af.setDepActivoFijo(depActivoFijo);
								af.setPlaca(placa);
								af.setCeco(ceco);
								af.setDescripcion(descripcion);
								af.setMarca(marca);
								af.setSerie(serie);

								int res = activoFijoBI.actualizaActivoFijo(af);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Se inserto en la tabla  Act Fijo ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateGuia.json?idGuia=<?>&idUsuario=<?>ceco=<?>&zona=<?>&status=<?>
						@RequestMapping(value="/updateGuia", method = RequestMethod.GET)
						public ModelAndView updateGuia(HttpServletRequest request, HttpServletResponse response)
						{
							try{
								int  idGuia=Integer.parseInt(request.getParameter("idGuia"));
								String ceco=request.getParameter("ceco");
								int  idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
								int  status=Integer.parseInt(request.getParameter("status"));

								GuiaDHLDTO guia=new GuiaDHLDTO();

								guia.setIdGuia(idGuia);
								guia.setIdUsuario(idUsuario);
								guia.setCeco(ceco);
								guia.setStatus(status);


								boolean res = guiaDHLBI.actualiza(guia);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Alta En Tabla GuiaDHL ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateGuiaEvidencia.json?idGuia=<?>&idEvid=<?>ceco=<?>&nombreArch=<?>&ruta=<?>&descr=<?>
						@RequestMapping(value="/updateGuiaEvidencia", method = RequestMethod.GET)
						public ModelAndView updateGuiaEvidencia(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								int  idGuia=Integer.parseInt(request.getParameter("idGuia"));
								String ceco=request.getParameter("ceco");
								int  idEvid=Integer.parseInt(request.getParameter("idEvid"));
								String  nombreArch=request.getParameter("nombreArch");
								String  ruta=request.getParameter("ruta");
								String descr=request.getParameter("descr");

								GuiaDHLDTO guia=new GuiaDHLDTO();

								guia.setIdGuia(idGuia);
								guia.setCeco(ceco);
								guia.setIdEvid(idEvid);
								guia.setNombreArch(nombreArch);
								guia.setRuta(ruta);
								guia.setDescr(descr);

								boolean res = guiaDHLBI.actualizaEvid(guia);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "modifico En Tabla GuiaDHL evidencia");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}


						//localhost:8080/migestion/catalogosService/updateAreaApoyoEvid.json?idEvid=<?>&idArea<?>&ceco=<?>nombreArc=<?>&ruta=<?>descr=<?>
						@RequestMapping(value="/updateAreaApoyoEvid", method = RequestMethod.GET)
						public ModelAndView updateAreaApoyoEvid(HttpServletRequest request, HttpServletResponse response)
						{
							try{

								int  idEvid=Integer.parseInt(request.getParameter("idEvid"));
								int idArea=Integer.parseInt(request.getParameter("idArea"));
								String ceco=request.getParameter("ceco");
								String nombreArc=request.getParameter("nombreArc");
								String ruta=request.getParameter("ruta");
								String descr=request.getParameter("descr");

								AreaApoyoDTO ape=new AreaApoyoDTO();

								ape.setIdEvid(idEvid);
								ape.setIdArea(idArea);
								ape.setCeco(ceco);
								ape.setNombreArc(nombreArc);
								ape.setRuta(ruta);
								ape.setDescr(descr);


								boolean res = areaApoyoBI.actualizaEvi(ape);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "modifico En Tabla Evidencia Area de apoyo ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateMinuciaEvi.json?idevi=<?>&idMinucia=<?>observac=<?>&ruta=<?>&nombrearc=<?>
						@RequestMapping(value="/updateMinuciaEvi", method = RequestMethod.GET)
						public ModelAndView updateMinuciaEvi(HttpServletRequest request, HttpServletResponse response)
						{
							try{


								int idevi=Integer.parseInt(request.getParameter("idevi"));
								int idMinucia=Integer.parseInt(request.getParameter("idMinucia"));
								String observac=request.getParameter("observac");
								String ruta=request.getParameter("ruta");
								String nombrearc=request.getParameter("nombrearc");

								CatalogoMinuciasDTO cam=new CatalogoMinuciasDTO();

								cam.setIdevi(idevi);
								cam.setIdMinucia(idMinucia);
								cam.setObservac(observac);
								cam.setRuta(ruta);
								cam.setNombrearc(nombrearc);

								boolean res = catalogoMinuciasBI.actualizaEvi(cam);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Actualizo En Tabla Minucias ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						//localhost:8080/migestion/catalogosService/updateCartaAsig.json?idCarta=<?>&idUsuario<?>&ceco=<?>nombreArc=<?>&ruta=<?>&observ=<?>&status=<?>
						@RequestMapping(value="/updateCartaAsig", method = RequestMethod.GET)
						public ModelAndView updateCartaAsig(HttpServletRequest request, HttpServletResponse response)
						{
							try{
								int idCarta=Integer.parseInt(request.getParameter("idCarta"));
								int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
								String ceco=request.getParameter("ceco");
								String nombreArc=request.getParameter("nombreArc");
								String ruta=request.getParameter("ruta");
								String observ=request.getParameter("observ");
								int status=Integer.parseInt(request.getParameter("status"));

								CartaAsignacionAFDTO ca=new CartaAsignacionAFDTO();

								ca.setIdCarta(idCarta);
								ca.setIdUsuario(idUsuario);
								ca.setCeco(ceco);
								ca.setNombreArc(nombreArc);
								ca.setRuta(ruta);
								ca.setObserv(observ);
								ca.setStatus(status);


								boolean res = cartaAsignacionAFBI.actualiza(ca);
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Actualizo En Tabla Carta Asignacion ");
								mv.addObject("res",res);
								return mv;

							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}

						// localhost:8080/migestion/catalogosService/updateEstandPuestoMueble.json?idMueble=<?>idPuesto=<?>&ceco=<?>&mueble1caj1=<?>&mueble1caj2=<?>&mueble1caj3=<?>&mueble2caj1=<?>&mueble2caj2=<?>&mueble2caj6=<?>&bandera=<?>
						@RequestMapping(value = "/updateEstandPuestoMueble", method = RequestMethod.GET)
						public ModelAndView updateEstandPuestoMueble(HttpServletRequest request, HttpServletResponse response) {
							try {
								int idMueble=Integer.parseInt(request.getParameter("idMueble"));
								int idPuesto=Integer.parseInt(request.getParameter("idPuesto"));
								String ceco = request.getParameter("ceco");
								String mueble1caj1 = request.getParameter("mueble1caj1");
								String mueble1caj2 = request.getParameter("mueble1caj2");
								String mueble1caj3 = request.getParameter("mueble1caj3");
								String mueble2caj1 = request.getParameter("mueble2caj1");
								String mueble2caj2 = request.getParameter("mueble2caj2");
								String  mueble2caj6 = request.getParameter("mueble2caj6");
								int bandera=Integer.parseInt(request.getParameter("bandera"));


            EstandPuestoDTO ep = new EstandPuestoDTO();

            ep.setIdMueble(idMueble);
            ep.setIdPuesto(idPuesto);
            ep.setCeco(ceco);
            ep.setMueble1caj1(mueble1caj1);
            ep.setMueble1caj2(mueble1caj2);
            ep.setMueble1caj3(mueble1caj3);
            ep.setMueble2caj1(mueble2caj1);
            ep.setMueble2caj2(mueble2caj2);
            ep.setMueble2caj6(mueble2caj6);
            ep.setBandera(bandera);

            boolean res = estandPuestoBI.actualizaMueble(ep);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo En Tabla mueble ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/updateProgLimpiezaArea.json?idArea=<?>&idProg=<?>&ceco=<?>&area=<?>&articulo=<?>&frecuencia=<?>&horario=<?>&responsable=<?>&supervisor=<?>
    @RequestMapping(value = "/updateProgLimpiezaArea", method = RequestMethod.GET)
    public ModelAndView updateProgLimpiezaArea(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idArea = Integer.parseInt(request.getParameter("idArea"));
            int idProg = Integer.parseInt(request.getParameter("idProg"));
            String ceco = request.getParameter("ceco");
            String area = request.getParameter("area");
            String articulo = request.getParameter("articulo");
            String frecuencia = request.getParameter("frecuencia");
            String horario = request.getParameter("horario");
            String responsable = request.getParameter("responsable");
            String supervisor = request.getParameter("supervisor");

            ProgLimpiezaDTO pl = new ProgLimpiezaDTO();
            pl.setIdArea(idArea);
            pl.setIdProg(idProg);
            pl.setCeco(ceco);
            pl.setArea(area);
            pl.setArticulo(articulo);
            pl.setFrecuencia(frecuencia);
            pl.setHorario(horario);
            pl.setResponsable(responsable);
            pl.setSupervisor(supervisor);

            boolean res = progLimpiezaBI.actualizaPro(pl);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizo En Tabla Programa de limpieza ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/updateDepActFijoEvi.json?idEvi=<?>&idDepAct=<?>&ceco=<?>&nombreArc=<?>&ruta=<?>&desc=<?>
    @RequestMapping(value = "/updateDepActFijoEvi", method = RequestMethod.GET)
    public ModelAndView updateDepActFijoEvi(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idDepAct = Integer.parseInt(request.getParameter("idDepAct"));
            int idEvi = Integer.parseInt(request.getParameter("idEvi"));
            String ceco = request.getParameter("ceco");
            String nombreArc = request.getParameter("nombreArc");
            String ruta = request.getParameter("ruta");
            String desc = request.getParameter("desc");

            DepuraActFijoDTO dep = new DepuraActFijoDTO();

            dep.setIdDepAct(idDepAct);
            dep.setIdEvi(idEvi);
            dep.setCeco(ceco);
            dep.setNombreArc(nombreArc);
            dep.setRuta(ruta);
            dep.setDesc(desc);

            boolean res = depuraActFijoBI.actualizaEvi(dep);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se Actualizo en la tabla Evi Depura Act Fijo ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/updatePuesto.json?idPuesto=<?>&descripcion=<?>
    @RequestMapping(value = "/updatePuesto", method = RequestMethod.GET)
    public ModelAndView updatePuesto(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idPuesto = Integer.parseInt(request.getParameter("idPuesto"));
            String descripcion = request.getParameter("descripcion");

            PuestoDTO pue = new PuestoDTO();
            pue.setIdPuesto(idPuesto);
            pue.setDescripcion(descripcion);

            boolean res = puestoBI.actualiza(pue);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se actualizo en la tabla puesto ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

}
