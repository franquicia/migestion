package com.gruposalinas.migestion.servicios.servidor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/soporte")
public class ServicioVideo {

    private static Logger logger = LogManager.getLogger(ServicioVideo.class);

    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100; // 100MB

    //http://localhost:8080/franquicia/soporte/uploadVideo.htm
    @RequestMapping(value = "/uploadVideo", method = RequestMethod.GET)
    public ModelAndView getLoadVideo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        return new ModelAndView("indexVideo", "command", new multiFileVideo());
    }

    //http://localhost:8080/franquicia/soporte/uploadVideo.htm
    @RequestMapping(value = "/uploadVideo", method = RequestMethod.POST)
    public ModelAndView postLoadVideo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "videoFile", required = true) MultipartFile videoFile)
            throws IOException {

        ModelAndView mv = new ModelAndView("indexVideo", "command", new multiFileVideo());

        File r = null;
        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta;

        ruta = rootPath + "/Sociounico/franquicia/videos/" + Calendar.getInstance().get(Calendar.YEAR) + "/";

        //logger.info("ruta - " + ruta);
        r = new File(ruta);

        if (r.mkdirs()) {
            //logger.info("El directorio ha sido creado");
        } else {
            //logger.info("El directorio no fue creado por que ya existe");
        }

        if (videoFile.getContentType().toString().equalsIgnoreCase("video/mp4")
                || //MP4
                videoFile.getContentType().toString().equalsIgnoreCase("video/x-flv")
                || //FLV
                videoFile.getContentType().toString().equalsIgnoreCase("video/quicktime")
                || //MOV
                videoFile.getContentType().toString().equalsIgnoreCase("video/x-msvideo")
                || //AVI
                videoFile.getContentType().toString().equalsIgnoreCase("video/x-ms-wmv")) {		//WMV

            if (videoFile.getSize() < MAX_FILE_SIZE) {
                BufferedOutputStream bufferOutput = null;
                try {
                    FileOutputStream fileOutput = new FileOutputStream(ruta + videoFile.getOriginalFilename());

                    bufferOutput = new BufferedOutputStream(fileOutput);
                    bufferOutput.write((byte[]) videoFile.getBytes());
                    bufferOutput.close();

                    request.setAttribute("estado", "si");
                    //logger.info("Archivo " + videoFile.getOriginalFilename() + " fue subido exitosamente!");
                } catch (Exception e) {
                    //logger.info("ERROR: " + e.getMessage());
                } finally {
                    try {
                        if (bufferOutput != null) {
                            bufferOutput.close();
                        }
                    } catch (Exception e) {

                    }
                }
            } else {
                request.setAttribute("estado", "no-size");
                //logger.info("El archivo " + videoFile.getOriginalFilename() + " es mayor a 100MB");
            }

        } else {
            request.setAttribute("estado", "no-compatible");
            //logger.info("El archivo " + videoFile.getOriginalFilename() + " no contiene un formato de video válido.");
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/listVideos.htm
    @RequestMapping(value = "/listVideos", method = RequestMethod.GET)
    public ModelAndView getListVideos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("indexVideo", "command", new multiFileVideo());

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + "/Sociounico/franquicia/videos/" + Calendar.getInstance().get(Calendar.YEAR) + "/";
        //String ruta2 = "/Sociounico/franquicia/videos/" + Calendar.getInstance().get(Calendar.YEAR) + "/";

        File directory = new File(ruta);

        if (directory.exists()) {
            //logger.info("El directorio existe - " + ruta);

            mv.addObject("listaVideos", directory.list());
            mv.addObject("lista", "si");
        } else {
            //logger.info("El directorio no existe");
            mv.addObject("lista", "vacia");
        }
        return mv;
    }

    //http://localhost:8080/franquicia/soporte/deleteVideo.htm
    @RequestMapping(value = "/deleteVideo", method = RequestMethod.POST)
    public ModelAndView postDeleteVideo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("indexVideo", "command", new multiFileVideo());

        List<String> listaVideosEliminar = new ArrayList<String>();

        for (String video : request.getParameterValues("listaVideosEliminar")) {
            listaVideosEliminar.add(video);
        }

        List<String> auxList = new ArrayList<String>();
        List<String> fullList = new ArrayList<String>();

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + "/Sociounico/franquicia/videos/" + Calendar.getInstance().get(Calendar.YEAR) + "/";
        //String ruta2 = "/Sociounico/franquicia/videos/" + Calendar.getInstance().get(Calendar.YEAR) + "/";

        File directory = new File(ruta);

        for (String video : directory.list()) {
            fullList.add(video);
        }

        if (listaVideosEliminar.size() > 0) {
            if (directory.exists()) {
                File deleteVideo;

                for (String video : listaVideosEliminar) {
                    deleteVideo = new File(ruta + video);

                    if (deleteVideo.delete()) {
                        //logger.info("El video " + video + " fue eliminado");
                        auxList.add(video);
                    } else {
                        //logger.info("El archivo " + video + " no fue eliminado");
                    }
                }

                deleteVideo = null;

                if (auxList.isEmpty()) {
                    mv.addObject("listaVideos", fullList);
                    mv.addObject("lista", "si");
                } else {
                    fullList.removeAll(auxList);
                    mv.addObject("listaVideos", fullList);
                    mv.addObject("lista", "si");
                }
            } else {
                //logger.info("El directorio no existe, no se pueden eliminar los vídeos");
                mv.addObject("listaVideos", fullList);
            }
        } else {
            //logger.info("La lista se encuentra vacía, no se puede eliminar ningún vídeo");
            mv.addObject("lista", "vacia");
        }

        return mv;

    }
}

class multiFileVideo {

    private MultipartFile video;

    public MultipartFile getVideo() {
        return video;
    }

    public void setVideo(MultipartFile video) {
        this.video = video;
    }

}
