package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.TablasPasoBI;
import com.gruposalinas.migestion.domain.TablasPasoDTO;
import com.gruposalinas.migestion.resources.GTNAuthInterceptor;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/consultaPasoService")

public class ConsultaPaso {

    @Autowired
    TablasPasoBI tablaPasoBI;

    private static final Logger logger = LogManager.getLogger(GTNAuthInterceptor.class);

    // http://localhost:8080/gestion/consultaPasoService/getCecosPasoByParams.json?ceco=<?>&cecoPadre=<?>&desc=<?>
    @RequestMapping(value = "/getCecosPasoByParams", method = RequestMethod.GET)
    public ModelAndView getCecosPasoByParams(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String ceco = request.getParameter("ceco");
            String cecoPadre = request.getParameter("cecoPadre");
            String descripcion = request.getParameter("desc");

            List<TablasPasoDTO> res = tablaPasoBI.obtieneCecos(ceco, cecoPadre, descripcion);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CECO PASO PARAMS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/gestion/consultaPasoService/getCargaUsuariosPasoByParams.json?idUsu=<?>&idPuesto=<?>&idCeco=<?>&idCecoSup=<?>
    @RequestMapping(value = "/getCargaUsuariosPasoByParams", method = RequestMethod.GET)
    public ModelAndView getCargaUsuariosPasoByParams(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsu = request.getParameter("idUsu");
            String idPuesto = request.getParameter("idPuesto");
            String idCeco = request.getParameter("idCeco");
            String idCecoSup = request.getParameter("idCecoSup");

            List<TablasPasoDTO> res = tablaPasoBI.obtieneUsuarios(idUsu, idPuesto, idCeco, idCecoSup);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "USUARIOS PASO PARAMS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/consultaPasoService/getSucursalPaso.json?idSuc=<?>
    @RequestMapping(value = "/getSucursalPaso", method = RequestMethod.GET)
    public ModelAndView getSucursalPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSuc = request.getParameter("idSuc");

            List<TablasPasoDTO> lista = tablaPasoBI.obtieneSucursales(idSuc);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "SUCURSAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

}
