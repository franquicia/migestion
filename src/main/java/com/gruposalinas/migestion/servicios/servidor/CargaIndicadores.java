/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.IndicadoresIPNBI;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author kramireza
 */
@Controller
@RequestMapping("/indicadores")
public class CargaIndicadores {

    @Autowired
    IndicadoresIPNBI indicadoresIPNBI;

    private static Logger logger = LogManager.getLogger(CargaPuestosExterno.class);

    //http://localhost:8080/migestion/indicadores/documentoIndicadores.htm
    @RequestMapping(value = "/documentoIndicadores", method = RequestMethod.GET)
    public ModelAndView getFileIndicadores(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView mv = new ModelAndView("indicadoresIPN", "command", new FileParameters());
        mv.addObject("fileAction", "subirArchivo");
        return mv;
    }

    //http://localhost:8080/migestion/indicadores/getIndicadoresIPNEquipo.htm
    @RequestMapping(value = "/getDataIndicadoresIPN", method = RequestMethod.POST)
    public ModelAndView getDataIndicadoresIPNEquipo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false) MultipartFile uploadedFile,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction)
            throws IOException {
        //System.out.println("1");
        ModelAndView mv = new ModelAndView("indicadoresIPN", "command", new com.gruposalinas.migestion.domain.FileParameters());

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRoute;
        BufferedReader br = null;
        //System.out.println("2");

        if (uploadedFile != null) {
            // System.out.println("3");
            InputStream is = uploadedFile.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            int respuesta = indicadoresIPNBI.getDataIndicadorIPN(br);
            if (respuesta > 0) {
                mv.addObject("ArchivoSubido", "si");
            }

        }

        return mv;
    }

}
