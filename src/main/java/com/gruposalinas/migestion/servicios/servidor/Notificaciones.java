package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.StrCipher;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/notificaciones")
public class Notificaciones {

private static Logger logger = LogManager.getLogger(Servicios.class);
    // http://localhost:8080/migestion/notificaciones/creaNotificacion.json?idTipoApp=<?>&nombre=<?>&mensaje=<?>&prioridad=<?>
    // http://localhost:8080/migestion/notificaciones/creaNotificacion.json?idTipoApp=7&nombre=TEST&mensaje=hola mundo&prioridad=1
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/creaNotificacion", method = RequestMethod.GET)
    public @ResponseBody
    String getDataSession(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String data = "";

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            String idUsuario = "189870"; //urides.split("&")[0].split("=")[1];
            logger.info("idUsuario " + idUsuario);
            data += idUsuario + "\n";

            String idTipoApp = urides.split("&")[0].split("=")[1];
            logger.info("idTipoApp " + idTipoApp);
            data += idTipoApp + "\n";

            String nombre = urides.split("&")[1].split("=")[1];
            logger.info("nombre " + nombre);
            data += nombre + "\n";

            String mensaje = urides.split("&")[2].split("=")[1];
            logger.info("mensaje " + mensaje);
            data += mensaje + "\n";

            String prioridad = urides.split("&")[3].split("=")[1];
            logger.info("prioridad " + prioridad);
            data += prioridad + "\n";

            data += "\n";
        } catch (Exception e) {
            logger.info(e);
        }
        return data;
    }
}
