package com.gruposalinas.migestion.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.ActivoFijoBI;
import com.gruposalinas.migestion.business.ArchivoUrlBI;
import com.gruposalinas.migestion.business.AreaApoyoBI;
import com.gruposalinas.migestion.business.BitacoraMantenimientoBI;
import com.gruposalinas.migestion.business.CartaAsignacionAFBI;
import com.gruposalinas.migestion.business.CatalogoMinuciasBI;
import com.gruposalinas.migestion.business.CatalogoVistaBI;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.business.CedulaBI;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.DatosUsuarioBI;
import com.gruposalinas.migestion.business.DepuraActFijoBI;
import com.gruposalinas.migestion.business.EstandPuestoBI;
import com.gruposalinas.migestion.business.FilasBI;
import com.gruposalinas.migestion.business.FormatoMetodo7sBI;
import com.gruposalinas.migestion.business.FotoPlantillaBI;
import com.gruposalinas.migestion.business.FotosAntesDespuesBI;
import com.gruposalinas.migestion.business.GeografiaBI;
import com.gruposalinas.migestion.business.GuiaDHLBI;
import com.gruposalinas.migestion.business.IncidenciasBI;
import com.gruposalinas.migestion.business.InfoVistaBI;
import com.gruposalinas.migestion.business.OperacionBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.business.PerfilBI;
import com.gruposalinas.migestion.business.PerfilUsuarioBI;
import com.gruposalinas.migestion.business.PeriodoBI;
import com.gruposalinas.migestion.business.PeriodoReporteBI;
import com.gruposalinas.migestion.business.ProductoBI;
import com.gruposalinas.migestion.business.ProgLimpiezaBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.PuestoBI;
import com.gruposalinas.migestion.business.ReporteBI;
import com.gruposalinas.migestion.business.SesionFirmaBI;
import com.gruposalinas.migestion.business.TareaActBI;
import com.gruposalinas.migestion.business.TelSucursalBI;
import com.gruposalinas.migestion.business.TicketCuadrillaBI;
import com.gruposalinas.migestion.business.UsuarioExternoBI;
import com.gruposalinas.migestion.business.Usuario_ABI;
import com.gruposalinas.migestion.business.ef.AsesoresDigitalesBI;
import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import com.gruposalinas.migestion.domain.ArchivosUrlDTO;
import com.gruposalinas.migestion.domain.AreaApoyoDTO;
import com.gruposalinas.migestion.domain.BitacoraMntoDTO;
import com.gruposalinas.migestion.domain.CartaAsignacionAFDTO;
import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;
import com.gruposalinas.migestion.domain.CatalogoVistaDTO;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.CedulaDTO;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.domain.DatosUsuarioDTO;
import com.gruposalinas.migestion.domain.DepuraActFijoDTO;
import com.gruposalinas.migestion.domain.EstandPuestoDTO;
import com.gruposalinas.migestion.domain.ExternoDTO;
import com.gruposalinas.migestion.domain.FilasDTO;
import com.gruposalinas.migestion.domain.FormatoMetodo7sDTO;
import com.gruposalinas.migestion.domain.FotoPlantillaDTO;
import com.gruposalinas.migestion.domain.FotosAntesDespuesDTO;
import com.gruposalinas.migestion.domain.FranquiciaCalDTO;
import com.gruposalinas.migestion.domain.FranquiciaLigasDTO;
import com.gruposalinas.migestion.domain.GeografiaDTO;
import com.gruposalinas.migestion.domain.GuiaDHLDTO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.domain.InfoVistaDTO;
import com.gruposalinas.migestion.domain.NotificaAfDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.domain.PerfilDTO;
import com.gruposalinas.migestion.domain.PerfilUsuarioDTO;
import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.PuestoDTO;
import com.gruposalinas.migestion.domain.TareaActDTO;
import com.gruposalinas.migestion.domain.TelSucursalDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.domain.TipificacionDTO;
import com.gruposalinas.migestion.domain.Usuario_ADTO;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import sun.misc.BASE64Decoder;

@Controller
@RequestMapping("/catalogosService")
public class ServiciosAltaCatalogos {

    @Autowired
    Usuario_ABI usuarioabi;

    @Autowired
    SesionFirmaBI sesionFirmaBI;

    @Autowired
    DatosUsuarioBI datosUsuarioBI;

    @Autowired
    ProductoBI productoBI;

    @Autowired
    OperacionBI operacionBI;

    @Autowired
    ParametroBI parametroBI;

    @Autowired
    CecoBI cecoBI;

    @Autowired
    TareaActBI tareaBI;

    @Autowired
    PeriodoBI periodoBI;

    @Autowired
    PeriodoReporteBI periodoReporteBI;

    @Autowired
    ReporteBI reporteBI;

    @Autowired
    GeografiaBI geoBI;

    @Autowired
    PerfilBI perfilbi;

    @Autowired
    PerfilUsuarioBI perfilusuariobi;

    @Autowired
    AsesoresDigitalesBI asesoresDigitalesBI;

    @Autowired
    CatalogoVistaBI catalogoVistaBI;

    @Autowired
    InfoVistaBI infoVistaBI;

    

    @Autowired
    FilasBI FilaBI;

    @Autowired
    UsuarioExternoBI usuarioexternoBI;

    @Autowired
    TelSucursalBI telsucursalBI;

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    FormatoMetodo7sBI formatoMetodo7sBI;

    @Autowired
    CedulaBI cedulaBI;

    @Autowired
    EstandPuestoBI estandPuestoBI;

    @Autowired
    ProgLimpiezaBI progLimpiezaBI;

    @Autowired
    ArchivoUrlBI archivoUrlBI;

    @Autowired
    BitacoraMantenimientoBI bitacoraMantenimientoBI;

    @Autowired
    FotoPlantillaBI fotoPlantillaBI;

    @Autowired
    CatalogoMinuciasBI catalogoMinuciasBI;

    @Autowired

    FotosAntesDespuesBI fotosAntesDespuesBI;

    @Autowired
    TicketCuadrillaBI ticketCuadrillaBI;

    @Autowired
    DepuraActFijoBI depuraActFijoBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    ActivoFijoBI activoFijoBI;

    @Autowired
    AreaApoyoBI areaApoyoBI;

    @Autowired
    GuiaDHLBI guiaDHLBI;

    @Autowired
    CartaAsignacionAFBI cartaAsignacionAFBI;

    @Autowired
    PuestoBI puestoBI;

    private static final Logger logger = LogManager.getLogger(ServiciosAltaCatalogos.class);

    // http://localhost:8080/migestion/catalogosService/altaUsuarioA.json?activo=<?>&fecha=<?>&idCeco=<?>&idPuesto=<?>&idUsuario=<?>&nombre=<?>
    @RequestMapping(value = "/altaUsuarioA", method = RequestMethod.GET)
    public @ResponseBody
    boolean altaUsuarioA(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean usuA = false;
        try {
            String activo = request.getParameter("activo");
            String fecha = request.getParameter("fecha");
            String idCeco = request.getParameter("idCeco");
            String idPuesto = request.getParameter("idPuesto");
            String idUsuario = request.getParameter("idUsuario");
            String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");

            Usuario_ADTO usuarioA = new Usuario_ADTO();
            usuarioA.setActivo(Integer.parseInt(activo));
            usuarioA.setFecha(fecha);
            usuarioA.setIdCeco(idCeco);
            usuarioA.setIdPuesto(Integer.parseInt(idPuesto));
            usuarioA.setIdUsuario(Integer.parseInt(idUsuario));
            usuarioA.setNombre(nombre);

            usuA = usuarioabi.insertaUsuario(usuarioA);
            /*
			 * ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			 * mv.addObject("tipo", "FRTA Usuario CREADO"); mv.addObject("res", usuA);
			 * return mv;
             */
        } catch (Exception e) {
            //logger.info(e);
            return false;
        }

        return usuA;
    }

    // http://localhost:8080/migestion/catalogosService/createSession.json?idUsuario=<?>&firma=<?>&key=<?>
    @RequestMapping(value = "/createSession", method = RequestMethod.GET)
    public @ResponseBody
    String loginRestFirma(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        try {

            String idUsuario = request.getParameter("idUsuario");
            String firma = request.getParameter("firma");
            String key = request.getParameter("key");

            //logger.info("Usuario: " + idUsuario);
            //logger.info("Firma: " + firma);
            //logger.info("key: " + key);

            /* Usuarios */
            ticketSession = sesionFirmaBI.createSession(idUsuario, firma, key);

            if (!ticketSession.equals("ERROR")) {

                datosUsuarioDTO = new DatosUsuarioDTO();
                // datosUsuarioDTO = datosUsuarioBI.buscaUsuario(idUsuario);
                Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario);
                datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
                datosUsuarioDTO.setTicket(ticketSession);

            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.info("No se pudo iniciar sesión");
        }

        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + "}";

        return json;
    }

    // http://localhost:8080/migestion/catalogosService/altaProducto.json?producto=<?>&idPadre=<?>
    @RequestMapping(value = "/altaProducto", method = RequestMethod.GET)
    public @ResponseBody
    int altaProducto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        int idCreado = 0;

        String producto = request.getParameter("producto");
        String idPadre = request.getParameter("idPadre");

        TipificacionDTO tipificacionDTO = new TipificacionDTO();
        tipificacionDTO.setDesc(producto);
        tipificacionDTO.setPadre(Integer.valueOf(idPadre));

        idCreado = productoBI.inserta(tipificacionDTO);

        return idCreado;

    }

    // http://localhost:8080/migestion/catalogosService/altaOperacion.json?operacion=<?>&idPadre=<?>
    @RequestMapping(value = "/altaOperacion", method = RequestMethod.GET)
    public @ResponseBody
    int altaOperacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        int idCreado = 0;

        String operacion = request.getParameter("operacion");
        String idPadre = request.getParameter("idPadre");

        TipificacionDTO tipificacionDTO = new TipificacionDTO();
        tipificacionDTO.setDesc(operacion);
        tipificacionDTO.setPadre(Integer.valueOf(idPadre));

        idCreado = operacionBI.inserta(tipificacionDTO);

        return idCreado;

    }

    // http://localhost:8080/migestion/catalogosService/altaParametro.json?setActivo=<?>&setClave=<?>&setValor=<?>
    // http://localhost:8080/migestion/catalogosService/altaParametro.json?setActivo=1&setClave=correoBuzon&setValor=franquiciabaz@bancoazteca.com.mx
    @RequestMapping(value = "/altaParametro", method = RequestMethod.GET)
    public ModelAndView altaParametro(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("setActivo");
            String clave = request.getParameter("setClave");
            String valor = request.getParameter("setValor");

            ParametroDTO parametro = new ParametroDTO();
            parametro.setActivo(Integer.parseInt(activo));
            parametro.setClave(clave);
            parametro.setValor(valor);
            boolean param = parametroBI.insertaPrametros(parametro);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PARAMETRO CREADO CORRECTAMENTE");
            mv.addObject("res", param);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaCeco.json?activo=<?>&calle=<?>&ciudad=<?>&cp=<?>&idCeco=<?>&descCeco=<?>&faxContacto=<?>&idCanal=<?>&idCecoSuperior=<?>&idEstado=<?>&idNegocio=<?>&idNivel=<?>&idPais=<?>&nombreContacto=<?>&puestoContacto=<?>&telefonoContacto=<?>&fecha=<?>&usuarioMod=<?>
    @RequestMapping(value = "/altaCeco", method = RequestMethod.GET)
    public ModelAndView altaCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("activo");
            String calle = new String(request.getParameter("calle").getBytes("ISO-8859-1"), "UTF-8");
            String ciudad = new String(request.getParameter("ciudad").getBytes("ISO-8859-1"), "UTF-8");
            String cp = new String(request.getParameter("cp").getBytes("ISO-8859-1"), "UTF-8");
            String idCeco = request.getParameter("idCeco");
            String descCeco = new String(request.getParameter("descCeco").getBytes("ISO-8859-1"), "UTF-8");
            String faxContacto = new String(request.getParameter("faxContacto").getBytes("ISO-8859-1"), "UTF-8");
            String idCanal = request.getParameter("idCanal");
            String idCecoSuperior = request.getParameter("idCecoSuperior");
            String idEstado = request.getParameter("idEstado");
            String idNegocio = request.getParameter("idNegocio");
            String idNivel = request.getParameter("idNivel");
            String idPais = request.getParameter("idPais");
            String nombreContacto = new String(request.getParameter("nombreContacto").getBytes("ISO-8859-1"), "UTF-8");
            String puestoContacto = new String(request.getParameter("puestoContacto").getBytes("ISO-8859-1"), "UTF-8");
            String telefonoContacto = new String(request.getParameter("telefonoContacto").getBytes("ISO-8859-1"),
                    "UTF-8");
            String fecha = request.getParameter("fecha");
            String usuarioMod = request.getParameter("usuarioMod");

            CecoDTO ceco = new CecoDTO();
            ceco.setActivo(Integer.parseInt(activo));
            ceco.setCalle(calle);
            ceco.setCiudad(ciudad);
            ceco.setCp(cp);
            ceco.setDescCeco(descCeco);
            ceco.setFaxContacto(faxContacto);
            ceco.setIdCanal(Integer.parseInt(idCanal));
            ceco.setIdCeco(idCeco);
            ceco.setIdCecoSuperior(Integer.parseInt(idCecoSuperior));
            ceco.setIdEstado(Integer.parseInt(idEstado));
            ceco.setIdNegocio(Integer.parseInt(idNegocio));
            ceco.setIdNivel(Integer.parseInt(idNivel));
            ceco.setIdPais(Integer.parseInt(idPais));
            ceco.setNombreContacto(nombreContacto);
            ceco.setPuestoContacto(puestoContacto);
            ceco.setTelefonoContacto(telefonoContacto);
            ceco.setFechaModifico(fecha);
            ceco.setUsuarioModifico(usuarioMod);
            boolean cecoId = cecoBI.insertaCeco(ceco);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");

            mv.addObject("tipo", "CECO CREADO");
            mv.addObject("res", cecoId);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaTarea.json?nombre=<?>&idTarea=<?>&estatus=<?>&statusVac=<?>&idUsuario=<?>&tipoTarea=<?>&commit=<?>&pkTarea=<?>
    @RequestMapping(value = "/altaTarea", method = RequestMethod.GET)
    public ModelAndView altaTarea(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String nombre = request.getParameter("nombre");
            String idTarea = request.getParameter("idTarea");
            String estatus = request.getParameter("estatus");
            String statusVac = request.getParameter("statusVac");
            String idUsuario = request.getParameter("idUsuario");
            String tipoTarea = request.getParameter("tipoTarea");
            String commit = request.getParameter("commit");
            String pkTarea = request.getParameter("pkTarea");

            TareaActDTO tareaDTO = new TareaActDTO();
            tareaDTO.setNombreTarea(nombre);
            tareaDTO.setIdTarea(Integer.parseInt(idTarea));
            tareaDTO.setEstatus(Integer.parseInt(estatus));
            tareaDTO.setEstatusVac(Integer.parseInt(statusVac));
            tareaDTO.setIdUsuario(Integer.parseInt(idUsuario));
            tareaDTO.setTipoTarea(tipoTarea);
            tareaDTO.setCommit(Integer.parseInt(commit));
            tareaDTO.setIdPkTarea(Integer.parseInt(pkTarea));

            boolean respuesta = tareaBI.insertaTareas(tareaDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TAREA CREADA CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaPeriodo.json?idPeriodo=<?>&descripcion=<?>&commit=<?>
    @RequestMapping(value = "/altaPeriodo", method = RequestMethod.GET)
    public ModelAndView altaPeriodo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPeriodo = request.getParameter("idPeriodo");
            String descripcion = request.getParameter("descripcion");
            String commit = request.getParameter("commit");

            System.out.println(idPeriodo + descripcion + commit);
            boolean respuesta = periodoBI.insertaPeriodo(idPeriodo, descripcion, Integer.parseInt(commit));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERIODO CREADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaPeriodoReporte.json?idPeriodo=<?>&idReporte=<?>&horario=<?>&idPerRep=<?>&commit=<?>
    @RequestMapping(value = "/altaPeriodoReporte", method = RequestMethod.GET)
    public ModelAndView altaPeriodoReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPeriodo = request.getParameter("idPeriodo");
            String idReporte = request.getParameter("idReporte");
            String horario = request.getParameter("horario");
            String idPerRep = request.getParameter("idPerRep");
            String commit = request.getParameter("commit");

            boolean respuesta = periodoReporteBI.insertaPeriodoReporte(Integer.parseInt(idPeriodo),
                    Integer.parseInt(idReporte), horario, Integer.parseInt(idPerRep), Integer.parseInt(commit));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERIODO REPORTE CREADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaReporte.json?idReporte=<?>&nombre=<?>&commit=<?>
    @RequestMapping(value = "/altaReporte", method = RequestMethod.GET)
    public ModelAndView altaReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String nombre = request.getParameter("nombre");
            String idReporte = request.getParameter("idReporte");
            String commit = request.getParameter("commit");

            boolean respuesta = reporteBI.insertaReporte(Integer.parseInt(idReporte), nombre, Integer.parseInt(commit));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "REPORTE CREADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaGeografia.json?idCeco=<?>&idRegion=<?>&nombreR=<?>&idZona=<?>&nombreZ=<?>&idTerritorio=<?>&nombreT=<?>&commit=<?>
    @RequestMapping(value = "/altaGeografia", method = RequestMethod.GET)
    public ModelAndView altaGeografia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");
            String idRegion = request.getParameter("idRegion");
            String nombreR = request.getParameter("nombreR");
            String idZona = request.getParameter("idZona");
            String nombreZ = request.getParameter("nombreZ");
            String idTerritorio = request.getParameter("idTerritorio");
            String nombreT = request.getParameter("nombreT");
            String commit = request.getParameter("commit");

            GeografiaDTO geoDTO = new GeografiaDTO();

            geoDTO.setIdCeco(idCeco);
            geoDTO.setIdRegion(Integer.parseInt(idRegion));
            geoDTO.setNombreRegion(nombreR);
            geoDTO.setIdZona(Integer.parseInt(idZona));
            geoDTO.setNombreZona(nombreZ);
            geoDTO.setIdTerritorio(Integer.parseInt(idTerritorio));
            geoDTO.setNombreTerritorio(nombreT);
            geoDTO.setCommit(Integer.parseInt(commit));

            boolean respuesta = geoBI.insertaGeografia(geoDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "GEOGRAFIA CREADO CORRECTAMENTE");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaPerfil.json?setDescripcion=<?>
    @RequestMapping(value = "/altaPerfil", method = RequestMethod.GET)
    public ModelAndView altaPerfil(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String descripcion = new String(request.getParameter("setDescripcion").getBytes("ISO-8859-1"), "UTF-8");

            PerfilDTO perfil = new PerfilDTO();
            perfil.setDescripcion(descripcion);
            int idPerfil = perfilbi.insertaPerfil(perfil);
            perfil.setIdPerfil(idPerfil);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ID DE PERFIL CREADO");
            mv.addObject("res", true);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaPerfilNuevo.json?setDescripcion=<?>&idPerfil=<?>
    @RequestMapping(value = "/altaPerfilNuevo", method = RequestMethod.GET)
    public ModelAndView altaPerfilNuevo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String descripcion = new String(request.getParameter("setDescripcion").getBytes("ISO-8859-1"), "UTF-8");
            String idPerfil = request.getParameter("idPerfil");
            PerfilDTO perfil = new PerfilDTO();
            perfil.setDescripcion(descripcion);
            perfil.setIdPerfil(Integer.parseInt(idPerfil));
            int idPerfilN = perfilbi.insertaPerfilN(perfil);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ID DE PERFIL CREADO");
            mv.addObject("res", true);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaPerfilUsuario.json?idUsuario=<?>&idPerfil=<?>
    @RequestMapping(value = "/altaPerfilUsuario", method = RequestMethod.GET)
    public ModelAndView altaPerfilUsuario(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
            perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));
            perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));

            boolean res = perfilusuariobi.insertaPerfilUsuario(perfilUsuarioDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERFIL ASIGNADO : ");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }


    // http://localhost:8080/migestion/catalogosService/altaNotificaAf.json?ceco=<?>&compromisoP=<?>&avanceP=<?>&compromisoA=<?>&avanceA=<?>&fecha=<?>
    @RequestMapping(value = "/altaNotificaAf", method = RequestMethod.GET)
    public ModelAndView altaNotificaAf(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String ceco = request.getParameter("ceco");
        String compromisoP = request.getParameter("compromisoP");
        String avanceP = request.getParameter("avanceP");
        String compromisoA = request.getParameter("compromisoA");
        String avanceA = request.getParameter("avanceA");
        String fecha = request.getParameter("fecha");

        NotificaAfDTO objInsertar = new NotificaAfDTO();
        objInsertar.setCeco(ceco);
        objInsertar.setCompromisoP(compromisoP);
        objInsertar.setAvanceP(avanceP);
        objInsertar.setCompromisoA(compromisoA);
        objInsertar.setAvanceA(avanceA);
        objInsertar.setFecha(fecha);

        boolean res = asesoresDigitalesBI.inserta(objInsertar);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "Registro de Tabla GETANOTIFICA_AF insertado ");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/migestion/catalogosService/altaCatalogoVista.json?descripcion=<?>&moduloPadre=<?>
    @RequestMapping(value = "/altaCatalogoVista", method = RequestMethod.GET)
    public ModelAndView altaCatalogoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
            int moduloPadre = Integer.parseInt(request.getParameter("moduloPadre"));

            CatalogoVistaDTO catVista = new CatalogoVistaDTO();
            catVista.setDescripcion(descripcion);
            catVista.setIdModPadre(moduloPadre);

            boolean res = catalogoVistaBI.insertaCatalogoVista(catVista);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Vista creada: ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaInfoVista.json?idCatVista=<?>&idUsuario=<?>&plataforma=<?>&fecha=<?>
    // http://10.51.209.7:8080/migestion/catalogosService/altaInfoVista.json?idCatVista=2&idUsuario=189870&plataforma=1&fecha=2018-08-13
    // 17:23:39:461
    @RequestMapping(value = "/altaInfoVista", method = RequestMethod.GET)
    public ModelAndView altaInfoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            logger.info("Entro al metodo alta info Vista");

            int idCatVista = Integer.parseInt(request.getParameter("idCatVista"));
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            int plataforma = Integer.parseInt(request.getParameter("plataforma"));
            String fecha = request.getParameter("fecha");

            System.out.println("Entro!!! " + idCatVista + ", " + idUsuario + ", " + plataforma + ", " + fecha + ", ");

            InfoVistaDTO infoVista = new InfoVistaDTO();
            infoVista.setIdCatVista(idCatVista);
            infoVista.setIdUsuario(idUsuario);
            infoVista.setPlataforma(plataforma);
            infoVista.setFecha(fecha);

            boolean res = infoVistaBI.insertaInfoVista(infoVista);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Vissta registrada: ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e.getMessage());
            return null;
        }
    }


    // http://localhost:8080/migestion/catalogosService/altaFila.json?numEconomico=<?>&idGrupo=<?>
    @RequestMapping(value = "/altaFila", method = RequestMethod.GET)
    public ModelAndView altaFila(HttpServletRequest request, HttpServletResponse response, FilasDTO FilasDTO) {
        try {

            String numEconomico = request.getParameter("numEconomico");
            String idGrupo = request.getParameter("idGrupo");

            FilasDTO filas = new FilasDTO();

            filas.setNumEconomico(Integer.parseInt(numEconomico));
            ;
            filas.setIdGrupo(Integer.parseInt(idGrupo));

            int res = FilaBI.inserta(filas);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "DATOS INSERTADOS FILA");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }


    // http://localhost:8080/migestion/catalogosService/altaEmpleadoExterno.json?numEmpleado=<?>&nombre=<?>&aPaterno=<?>&aMaterno=<?>&direccion=<?>&numCasa=<?>&numCelular=<?>&RFCEmpleado=<?>&cencosNum=<?>&puesto=<?>&fechaIngreso=<?>&fechaBaja=<?>&funcion=<?>&usuarioID=<?>&empSituacion=<?>&honorarios=<?>&email=<?>&numEmergencia=<?>&perEmergencia=<?>&numExt=<?>&sueldo=<?>&horarioIn=<?>&horarioFin=<?>&semestre=<?>&actividades=<?>&liberacion=<?>&elaboracion=<?>&llave=<?>&red=<?>&lotus=<?>&credencial=<?>&folioDataSec=<?>&pais=<?>&nombreCompleto=<?>&asistencia=<?>&curp=<?>&
    @RequestMapping(value = "/altaEmpleadoExterno", method = RequestMethod.GET)
    public ModelAndView altaEmpleadoExterno(HttpServletRequest request, HttpServletResponse response) {
        try {
            int numEmpleado = 0;
            if (request.getParameter("numEmpleado") != null) {
                if (!request.getParameter("numEmpleado").equals("")) {
                    numEmpleado = Integer.parseInt(request.getParameter("numEmpleado"));
                }
            }
            String nombre = request.getParameter("nombre");
            String aPaterno = request.getParameter("aPaterno");
            String aMaterno = request.getParameter("aMaterno");
            String direccion = request.getParameter("direccion");
            int numCasa = 0;
            if (request.getParameter("numCasa") != null) {
                if (!request.getParameter("numCasa").equals("") && request.getParameter("numCasa").length() <= 8) {
                    numCasa = Integer.parseInt(request.getParameter("numCasa"));
                }
            }
            int numCelular = 0;
            if (request.getParameter("numCelular") != null) {
                if (!request.getParameter("numCelular").equals("") && request.getParameter("numCelular").length() <= 8) {
                    numCelular = Integer.parseInt(request.getParameter("numCelular"));
                }
            }
            String rFCEmpleado = request.getParameter("rFCEmpleado");
            String cencosNum = request.getParameter("cencosNum");
            int puesto = 0;
            if (request.getParameter("puesto") != null) {
                if (!request.getParameter("puesto").equals("")) {
                    puesto = Integer.parseInt(request.getParameter("puesto"));
                }
            }
            String fechaIngreso = request.getParameter("fechaIngreso");
            String fechaBaja = request.getParameter("fechaBaja");
            String funcion = request.getParameter("funcion");
            int usuarioID = 0;
            if (request.getParameter("usuarioID") != null) {
                if (!request.getParameter("usuarioID").equals("")) {
                    usuarioID = Integer.parseInt(request.getParameter("usuarioID"));
                }
            }
            int empSituacion = 0;
            if (request.getParameter("empSituacion") != null) {
                if (!request.getParameter("empSituacion").equals("")) {
                    empSituacion = Integer.parseInt(request.getParameter("empSituacion"));
                }
            }
            int honorarios = 0;
            if (request.getParameter("honorarios") != null) {
                if (!request.getParameter("honorarios").equals("")) {
                    honorarios = Integer.parseInt(request.getParameter("honorarios"));
                }
            }
            String email = request.getParameter("email");
            int numEmergencia = 0;
            if (request.getParameter("numEmergencia") != null) {
                if (!request.getParameter("numEmergencia").equals("")
                        && request.getParameter("numEmergencia").length() <= 8) {
                    numEmergencia = Integer.parseInt(request.getParameter("numEmergencia"));
                }
            }
            String perEmergencia = request.getParameter("perEmergencia");
            int numExt = 0;
            if (request.getParameter("numExt") != null) {
                if (!request.getParameter("numExt").equals("")) {
                    numExt = Integer.parseInt(request.getParameter("numExt"));
                }
            }
            String sueldo = "0";
            if (request.getParameter("sueldo") != null) {
                if (!request.getParameter("sueldo").equals("")) {
                    sueldo = request.getParameter("sueldo");
                }
            }
            String horarioIn = request.getParameter("horarioIn");
            String horarioFin = request.getParameter("horarioFin");
            int semestre = 0;
            if (request.getParameter("semestre") != null) {
                if (!request.getParameter("semestre").equals("")) {
                    semestre = Integer.parseInt(request.getParameter("semestre"));
                }
            }
            String actividades = request.getParameter("actividades");
            String liberacion = request.getParameter("liberacion");
            String elaboracion = request.getParameter("elaboracion");
            int llave = 0;
            if (request.getParameter("llave") != null) {
                if (!request.getParameter("llave").equals("")) {
                    llave = Integer.parseInt(request.getParameter("llave"));
                }
            }
            int red = 0;
            if (request.getParameter("red") != null) {
                if (!request.getParameter("red").equals("")) {
                    red = Integer.parseInt(request.getParameter("red"));
                }
            }
            int lotus = 0;
            if (request.getParameter("lotus") != null) {
                if (!request.getParameter("lotus").equals("")) {
                    lotus = Integer.parseInt(request.getParameter("lotus"));
                }
            }
            int credencial = 0;
            if (request.getParameter("credencial") != null) {
                if (!request.getParameter("credencial").equals("")) {
                    credencial = Integer.parseInt(request.getParameter("credencial"));
                }
            }
            int folioDataSec = 0;
            if (request.getParameter("folioDataSec") != null) {
                if (!request.getParameter("folioDataSec").equals("")) {
                    folioDataSec = Integer.parseInt(request.getParameter("folioDataSec"));
                }
            }
            String pais = request.getParameter("pais");
            String nombreCompleto = request.getParameter("nombreCompleto");
            String asistencia = request.getParameter("asistencia");
            String curp = request.getParameter("curp");

            ExternoDTO externo = new ExternoDTO();

            externo.setNumEmpleado(numEmpleado);
            externo.setNombre(nombre);
            externo.setaPaterno(aPaterno);
            externo.setaMaterno(aMaterno);
            externo.setDireccion(direccion);
            externo.setNumCasa(numCasa);
            externo.setNumCelular(numCelular);
            externo.setRFCEmpleado(rFCEmpleado);
            externo.setCencosNum(cencosNum);
            externo.setPuesto(puesto);
            externo.setFechaIngreso(fechaIngreso);
            externo.setFechaBaja(fechaBaja);
            externo.setFuncion(funcion);
            externo.setUsuarioID(usuarioID);
            externo.setEmpSituacion(empSituacion);
            externo.setHonorarios(honorarios);
            externo.setEmail(email);
            externo.setNumEmergencia(numEmergencia);
            externo.setPerEmergencia(perEmergencia);
            externo.setNumExt(numExt);
            externo.setSueldo(sueldo);
            externo.setHorarioIn(horarioIn);
            externo.setHorarioFin(horarioFin);
            externo.setSemestre(semestre);
            externo.setActividades(actividades);
            externo.setLiberacion(liberacion);
            externo.setElaboracion(elaboracion);
            externo.setLlave(llave);
            externo.setRed(red);
            externo.setLotus(lotus);
            externo.setCredencial(credencial);
            externo.setFolioDataSec(folioDataSec);
            externo.setPais(pais);
            externo.setNombreCompleto(nombreCompleto);
            externo.setAsistencia(asistencia);
            externo.setCurp(curp);

            boolean resp = usuarioexternoBI.insertaUsuario(externo);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Empleado externo Insertado");
            mv.addObject("res", resp);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaTelefonoSucursal.json?idTelefono=<?>&idCeco=<?>&telefono=<?>&proveedor=<?>&estatus=<?>
    @RequestMapping(value = "/altaTelefonoSucursal", method = RequestMethod.GET)
    public ModelAndView altaTelefonoSucursal(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idTelefono = Integer.parseInt(request.getParameter("idTelefono"));
            String idCeco = request.getParameter("idCeco");
            String telefono = request.getParameter("telefono");
            String proveedor = request.getParameter("proveedor");
            String estatus = request.getParameter("estatus");

            TelSucursalDTO tel = new TelSucursalDTO();

            tel.setIdTelefono(idTelefono);
            tel.setIdCeco(idCeco);
            tel.setTelefono(telefono);
            tel.setProveedor(proveedor);
            tel.setEstatus(estatus);

            boolean res = telsucursalBI.insertatelefono(tel);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserto Telefono de sucursal");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaCatalogoIncidencias.json?idTipo=<?>&servicio=<?>&ubicacion=<?>&incidencia=<?>&plantilla=<?>&status=<?>
    @RequestMapping(value = "/altaCatalogoIncidencias", method = RequestMethod.GET)
    public ModelAndView altaCatalogoIncidencias(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idTipo = Integer.parseInt(request.getParameter("idTipo"));
            String servicio = request.getParameter("servicio");
            String ubicacion = request.getParameter("ubicacion");
            String incidencia = request.getParameter("incidencia");
            String plantilla = request.getParameter("plantilla");
            String status = request.getParameter("status");

            IncidenciasDTO incid = new IncidenciasDTO();

            incid.setIdTipo(idTipo);
            incid.setServicio(servicio);
            incid.setUbicacion(ubicacion);
            incid.setIncidencia(incidencia);
            incid.setPlantilla(plantilla);
            incid.setStatus(status);

            int res = incidenciasBI.inserta(incid);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserto En catalogo de Incidencias");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/altaCatalogoProveedores.json?menu=<?>&idProveedor=<?>&nombreCorto=<?>&razonSocial=<?>&status=<?>
    @RequestMapping(value = "/altaCatalogoProveedores", method = RequestMethod.GET)
    public ModelAndView altaCatalogoProveedores(HttpServletRequest request, HttpServletResponse response) {
        try {

            String menu = request.getParameter("menu");
            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            String nombreCorto = new String(request.getParameter("nombreCorto").getBytes("ISO-8859-1"), "UTF-8");
            String razonSocial = new String(request.getParameter("razonSocial").getBytes("ISO-8859-1"), "UTF-8");
            String status = request.getParameter("status");

            ProveedoresDTO proveedores = new ProveedoresDTO();

            proveedores.setMenu(menu);
            proveedores.setIdProveedor(idProveedor);
            proveedores.setNombreCorto(nombreCorto);
            proveedores.setRazonSocial(razonSocial);
            proveedores.setStatus(status);

            int res = proveedoresBI.inserta(proveedores);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserto En catalogo de Incidencias");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }



    // localhost:8080/migestion/catalogosService/altaArchivoUrl.json?nombre=<?>&ruta=<?>&version=<?>
    @RequestMapping(value = "/altaArchivoUrl", method = RequestMethod.GET)
    public ModelAndView altaArchivoUrl(HttpServletRequest request, HttpServletResponse response) {
        try {

            String nombre = request.getParameter("nombre");
            String ruta = request.getParameter("ruta");
            String version = request.getParameter("version");

            ArchivosUrlDTO au = new ArchivosUrlDTO();

            au.setNombre(nombre);
            au.setRuta(ruta);
            au.setVersion(version);

            int res = archivoUrlBI.inserta(au);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserto En Tabla Archivos Url ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }



						
						//localhost:8080/migestion/catalogosService/AltaTicketCuadrilla.json?idCuadrilla=<?>idProveedor=<?>&zona=<?>&liderCuad=<?>&correo=<?>&segundo=<?>
						@RequestMapping(value="/AltaTicketCuadrilla", method = RequestMethod.GET)
						public ModelAndView AltaTicketCuadrilla(HttpServletRequest request, HttpServletResponse response)
						{
							try{
								
								
								int idCuadrilla=Integer.parseInt(request.getParameter("idCuadrilla"));
								int idProveedor=Integer.parseInt(request.getParameter("idProveedor"));
								String ticket=request.getParameter("ticket");
								int  zona=Integer.parseInt(request.getParameter("zona"));
								String liderCua=request.getParameter("liderCuad");
								int status=Integer.parseInt(request.getParameter("status"));		
								
								TicketCuadrillaDTO tcua=new TicketCuadrillaDTO();
																		
							
								tcua.setIdCuadrilla(idCuadrilla);
								tcua.setIdProveedor(idProveedor);
								tcua.setTicket(ticket);
								tcua.setZona(zona);
							    tcua.setLiderCua(liderCua);
							    tcua.setStatus(status);
							    
								int res = ticketCuadrillaBI.inserta(tcua); 
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Actualizo En Tabla tk Cuadrilla ");
								mv.addObject("res",res);
								return mv;
								
							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}
	

				
	
						
						





						
						//localhost:8080/migestion/catalogosService/altaCuadrilla.json?idCuadrilla=<?>idProveedor=<?>&zona=<?>&liderCuad=<?>&correo=<?>&segundo=<?>&passw=<?>
						@RequestMapping(value="/altaCuadrilla", method = RequestMethod.GET)
						public ModelAndView altaCuadrilla(HttpServletRequest request, HttpServletResponse response)
						{
							try{
								
								int idCuadrilla=Integer.parseInt(request.getParameter("idCuadrilla"));
								int idProveedor=Integer.parseInt(request.getParameter("idProveedor"));
								int  zona=Integer.parseInt(request.getParameter("zona"));
								String liderCuad=request.getParameter("liderCuad");
								String correo=request.getParameter("correo");
								String segundo=request.getParameter("segundo");
								String passw=request.getParameter("passw");

								CuadrillaDTO cua=new CuadrillaDTO();
																		
								cua.setIdCuadrilla(idCuadrilla);
								cua.setIdProveedor(idProveedor);
								cua.setZona(zona);
							    cua.setLiderCuad(liderCuad);
							    cua.setCorreo(correo);
							    cua.setSegundo(segundo);
							    cua.setPassw(passw);
								
								
								int res = cuadrillaBI.inserta(cua); 
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Alta En Tabla Cuadrilla ");
								mv.addObject("res",res);
								return mv;
								
							}catch(Exception e)
							{
								//logger.info(e);
								return null;
							}
						}
						



						// localhost:8080/migestion/catalogosService/altaEstandPuestoMueble.json?idPuesto=<?>&ceco=<?>&mueble1caj1=<?>&mueble1caj2=<?>&mueble1caj3=<?>&mueble2caj1=<?>&mueble2caj2=<?>&mueble2caj6=<?>&bandera=<?>
						@RequestMapping(value = "/altaEstandPuestoMueble", method = RequestMethod.GET)
						public ModelAndView altaEstandPuestoMueble(HttpServletRequest request, HttpServletResponse response) {
							try {

            int idPuesto = Integer.parseInt(request.getParameter("idPuesto"));
            String ceco = request.getParameter("ceco");
            String mueble1caj1 = request.getParameter("mueble1caj1");
            String mueble1caj2 = request.getParameter("mueble1caj2");
            String mueble1caj3 = request.getParameter("mueble1caj3");
            String mueble2caj1 = request.getParameter("mueble2caj1");
            String mueble2caj2 = request.getParameter("mueble2caj2");
            String mueble2caj6 = request.getParameter("mueble2caj6");
            int bandera = Integer.parseInt(request.getParameter("bandera"));

            EstandPuestoDTO ep = new EstandPuestoDTO();

            ep.setIdPuesto(idPuesto);
            ep.setCeco(ceco);
            ep.setMueble1caj1(mueble1caj1);
            ep.setMueble1caj2(mueble1caj2);
            ep.setMueble1caj3(mueble1caj3);
            ep.setMueble2caj1(mueble2caj1);
            ep.setMueble2caj2(mueble2caj2);
            ep.setMueble2caj6(mueble2caj6);
            ep.setBandera(bandera);

            int res = estandPuestoBI.insertaMueble(ep);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserto En Tabla mueble ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // localhost:8080/migestion/catalogosService/altaProgLimpiezaArea.json?idProg=<?>&ceco=<?>&area=<?>&articulo=<?>&frecuencia=<?>&horario=<?>&responsable=<?>&supervisor=<?>
    @RequestMapping(value = "/altaProgLimpiezaArea", method = RequestMethod.GET)
    public ModelAndView altaProgLimpiezaArea(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idProg = Integer.parseInt(request.getParameter("idProg"));
            String ceco = request.getParameter("ceco");
            String area = request.getParameter("area");
            String articulo = request.getParameter("articulo");
            String frecuencia = request.getParameter("frecuencia");
            String horario = request.getParameter("horario");
            String responsable = request.getParameter("responsable");
            String supervisor = request.getParameter("supervisor");

            ProgLimpiezaDTO pl = new ProgLimpiezaDTO();

            pl.setIdProg(idProg);
            pl.setCeco(ceco);
            pl.setArea(area);
            pl.setArticulo(articulo);
            pl.setFrecuencia(frecuencia);
            pl.setHorario(horario);
            pl.setResponsable(responsable);
            pl.setSupervisor(supervisor);

            int res = progLimpiezaBI.insertaPro(pl);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserto En Tabla Programa de limpieza ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //localhost:8080/migestion/catalogosService/AltaDepActFijoEvi.json?idDepAct=<?>&ceco=<?>&nombreArc=<?>&ruta=<?>&desc=<?>
    @RequestMapping(value = "/AltaDepActFijoEvi", method = RequestMethod.GET)
    public ModelAndView AltaDepActFijoEvi(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idDepAct = Integer.parseInt(request.getParameter("idDepAct"));
            String ceco = request.getParameter("ceco");
            String nombreArc = request.getParameter("nombreArc");
            String ruta = request.getParameter("ruta");
            String desc = request.getParameter("desc");

            DepuraActFijoDTO dep = new DepuraActFijoDTO();

            dep.setIdDepAct(idDepAct);
            dep.setCeco(ceco);
            dep.setNombreArc(nombreArc);
            dep.setRuta(ruta);
            dep.setDesc(desc);

            int res = depuraActFijoBI.insertaEvi(dep);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se inserto en la tabla Evi Depura Act Fijo ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/migestion/catalogosService/AltaPuesto.json?idPuesto=<?>&descripcion=<?>
    @RequestMapping(value = "/AltaPuesto", method = RequestMethod.GET)
    public ModelAndView AltaPuesto(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idPuesto = Integer.parseInt(request.getParameter("idPuesto"));
            String descripcion = request.getParameter("descripcion");

            PuestoDTO pue = new PuestoDTO();
            pue.setIdPuesto(idPuesto);
            pue.setDescripcion(descripcion);

            int res = puestoBI.inserta(pue);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se inserto en la tabla Evi Depura Act Fijo ");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/migestion/catalogosService/postGuardaArchivos.json
    @RequestMapping(value = "/postGuardaArchivos", method = RequestMethod.POST)
    public @ResponseBody
    String postGuardaArchivos(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response) {
        String json = "";

        String res = null;

        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();

        String contenido = "";

        boolean respuesta = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {

            contenido = provider;

            String rutaEntrada = "";
            String nomArchivo = "";
            String archivo = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                rutaEntrada = rec.getString("ruta");
                nomArchivo = rec.getString("nomArchivo");
                archivo = rec.getString("archivo");
            }

            File r = null;

            String rootPath = File.listRoots()[0].getAbsolutePath();
            Calendar cal = Calendar.getInstance();

            String rutaFotoA = "";
            String rutaFotoD = "";

            // DEFINIR RUTA
            String rutaImagen = "";
            // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
            // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
            // NUEVO
            // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist

            rutaImagen = rutaEntrada;

            String ruta = rootPath + rutaImagen;

            // System.out.println(cal.getTime().toString());
            // System.out.println(File.listRoots()[0].getAbsolutePath());
            r = new File(ruta);

            //logger.info("RUTA: " + ruta);
            if (r.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            FileOutputStream fos = null;

            BASE64Decoder decoder = new BASE64Decoder();
            byte[] imgDecodificada = decoder.decodeBuffer(archivo);

            Date fechaActual = new Date();
            System.out.println(fechaActual);
            System.out.println("---------------------------------------------");

            String rutaTotal = ruta + nomArchivo;

            String rutalimpia = rutaTotal;

            FileOutputStream fileOutput = new FileOutputStream(rutalimpia);

            try {
                respuesta = true;
                BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);

                // se escribe el archivo decodificado
                bufferOutput.write(imgDecodificada);
                bufferOutput.close();

                rutaFotoA = rutalimpia;

            } catch (Exception e) {
                logger.info(e);
                respuesta = false;
            } finally {

                fileOutput.close();

            }

            JsonObject envoltorioJsonObj = new JsonObject();

            envoltorioJsonObj.addProperty("ARCHIVO", respuesta);

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return "{\"ARCHIVO\": false}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{\"ARCHIVO\": false}";
        }
    }
    // http://localhost:8080/migestion/catalogosService/altaPerfilUsuarioNvo.json?idUsuario=<?>&idPerfil=<?>&idActor=<?>&bandNvoEsq=<?>&tipoProyecto=

    @RequestMapping(value = "/altaPerfilUsuarioNvo", method = RequestMethod.GET)
    public ModelAndView altaPerfilUsuarioNvo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idActor = null;
            idActor = request.getParameter("idActor");
            String bandNvoEsq = null;
            bandNvoEsq = request.getParameter("bandNvoEsq");
            String tipoProyecto = null;
            tipoProyecto = request.getParameter("tipoProyecto");

            PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
            perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));
            perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));
            perfilUsuarioDTO.setIdActor(idActor);
            perfilUsuarioDTO.setBandNvoEsq(bandNvoEsq);
            perfilUsuarioDTO.setTipoProyecto(tipoProyecto);

            boolean res = perfilusuariobi.insertaPerfilUsuarioNvo(perfilUsuarioDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERFIL ASIGNADO : ");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

}
