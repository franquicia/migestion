package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.ActivoFijoBI;
import com.gruposalinas.migestion.business.ArchivoUrlBI;
import com.gruposalinas.migestion.business.AreaApoyoBI;
import com.gruposalinas.migestion.business.BitacoraMantenimientoBI;
import com.gruposalinas.migestion.business.CartaAsignacionAFBI;
import com.gruposalinas.migestion.business.CatalogoMinuciasBI;
import com.gruposalinas.migestion.business.CatalogoVistaBI;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.business.CedulaBI;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.DepuraActFijoBI;
import com.gruposalinas.migestion.business.EstandPuestoBI;
import com.gruposalinas.migestion.business.FilasBI;
import com.gruposalinas.migestion.business.FormatoMetodo7sBI;
import com.gruposalinas.migestion.business.FotoPlantillaBI;
import com.gruposalinas.migestion.business.FotosAntesDespuesBI;
import com.gruposalinas.migestion.business.GeografiaBI;
import com.gruposalinas.migestion.business.GuiaDHLBI;
import com.gruposalinas.migestion.business.IncidenciasBI;
import com.gruposalinas.migestion.business.InfoVistaBI;
import com.gruposalinas.migestion.business.OperacionBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.business.PerfilBI;
import com.gruposalinas.migestion.business.PerfilUsuarioBI;
import com.gruposalinas.migestion.business.PeriodoBI;
import com.gruposalinas.migestion.business.PeriodoReporteBI;
import com.gruposalinas.migestion.business.ProductoBI;
import com.gruposalinas.migestion.business.ProgLimpiezaBI;
import com.gruposalinas.migestion.business.ProveedorLoginBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.PuestoBI;
import com.gruposalinas.migestion.business.ReporteBI;
import com.gruposalinas.migestion.business.TareaActBI;
import com.gruposalinas.migestion.business.TelSucursalBI;
import com.gruposalinas.migestion.business.TicketCuadrillaBI;

import com.gruposalinas.migestion.business.UsuarioExternoBI;
import com.gruposalinas.migestion.business.Usuario_ABI;
import com.gruposalinas.migestion.business.ef.AsesoresDigitalesBI;
import com.gruposalinas.migestion.domain.PerfilUsuarioDTO;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/catalogosService")
public class ServiciosEliminaCatalogos {

    @Autowired
    Usuario_ABI usuarioabi;

    @Autowired
    ProductoBI productoBi;

    @Autowired
    OperacionBI operacionBI;

    @Autowired
    ParametroBI parametroBI;

    @Autowired
    CecoBI cecoBI;

    @Autowired
    TareaActBI tareaBI;

    @Autowired
    PeriodoBI periodoBI;

    @Autowired
    PeriodoReporteBI periodoReporteBI;

    @Autowired
    ReporteBI reporteBI;

    @Autowired
    GeografiaBI geoBI;

    @Autowired
    PerfilBI perfilbi;

    @Autowired
    PerfilUsuarioBI perfilusuariobi;


    @Autowired
    AsesoresDigitalesBI asesoresDigitalesBI;

    @Autowired
    CatalogoVistaBI catalogoVistaBI;

    @Autowired
    InfoVistaBI infoVistaBI;

    @Autowired
    FilasBI FilaBI;


    @Autowired
    UsuarioExternoBI usuarioexternoBI;

    @Autowired
    TelSucursalBI telsucursalBI;

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    FormatoMetodo7sBI formatoMetodo7sBI;

    @Autowired
    CedulaBI cedulaBI;

    @Autowired
    EstandPuestoBI estandPuestoBI;

    @Autowired
    ProgLimpiezaBI progLimpiezaBI;

    @Autowired
    ArchivoUrlBI archivoUrlBI;

    @Autowired
    ProveedorLoginBI proveedorLoginBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    TicketCuadrillaBI ticketCuadrillaBI;

    @Autowired
    FotoPlantillaBI fotoPlantillaBI;

    @Autowired
    BitacoraMantenimientoBI bitacoraMantenimientoBI;

    @Autowired
    CatalogoMinuciasBI catalogoMinuciasBI;

    @Autowired
    DepuraActFijoBI depuraActFijoBI;

    @Autowired
    ActivoFijoBI activoFijoBI;

    @Autowired
    FotosAntesDespuesBI fotosAntesDespuesBI;

    @Autowired
    AreaApoyoBI areaApoyoBI;

    @Autowired
    GuiaDHLBI guiaDHLBI;

    @Autowired
    CartaAsignacionAFBI cartaAsignacionAFBI;

    @Autowired
    PuestoBI puestoBI;

    private static final Logger logger = LogManager.getLogger(ServiciosEliminaCatalogos.class);

    // http://localhost:8080/migestion/catalogosService/eliminaUsuarioA.json?idUsuario=<?>
    @RequestMapping(value = "/eliminaUsuarioA", method = RequestMethod.GET)
    public @ResponseBody
    boolean eliminaUsuarioA(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;
        try {
            String idUsuario = request.getParameter("idUsuario");
            res = usuarioabi.eliminaUsuario(Integer.parseInt(idUsuario));
            /*
			 * ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			 * mv.addObject("tipo", "FRTA Usuario ELIMINADO CORRECTAMENTE");
			 * mv.addObject("res", res); return mv;
             */
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return false;
        }
        return res;
    }

    // http://localhost:8080/migestion/catalogosService/eliminaProducto.json?idProducto=<?>
    @RequestMapping(value = "/eliminaProducto", method = RequestMethod.GET)
    public @ResponseBody
    boolean eliminaProducto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;
        try {
            String idProducto = request.getParameter("idProducto");
            res = productoBi.elimina(Integer.valueOf(idProducto));
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return false;
        }
        return res;
    }

    // http://localhost:8080/migestion/catalogosService/eliminaOperacion.json?idOperacion=<?>
    @RequestMapping(value = "/eliminaOperacion", method = RequestMethod.GET)
    public @ResponseBody
    boolean eliminaOperacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = false;
        try {
            String idOperacion = request.getParameter("idOperacion");
            res = operacionBI.elimina(Integer.valueOf(idOperacion));
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return false;
        }
        return res;
    }

    // http://localhost:8080/migestion/catalogosService/eliminaParametro.json?&setClave=<?>
    @RequestMapping(value = "/eliminaParametro", method = RequestMethod.GET)
    public ModelAndView eliminaParametro(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String clave = request.getParameter("setClave");
            boolean res = parametroBI.eliminaParametro(clave);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PARAMETRO ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaCeco.json?idCeco=<?>
    @RequestMapping(value = "/eliminaCeco", method = RequestMethod.GET)
    public ModelAndView eliminaCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");

            boolean res = cecoBI.eliminaCeco(Integer.parseInt(idCeco));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CECO BORRADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaTarea.json?&idPkTarea=<?>
    @RequestMapping(value = "/eliminaTarea", method = RequestMethod.GET)
    public ModelAndView eliminaTarea(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPkTarea = request.getParameter("idPkTarea");

            boolean res = tareaBI.eliminaTareas(idPkTarea);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TAREA ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaPeriodo.json?&idPeriodo=<?>
    @RequestMapping(value = "/eliminaPeriodo", method = RequestMethod.GET)
    public ModelAndView eliminaPeriodo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPeriodo = request.getParameter("idPeriodo");

            boolean res = periodoBI.eliminaPeriodo(idPeriodo);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERIODO ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaPeriodoReporte.json?&idPerRep=<?>
    @RequestMapping(value = "/eliminaPeriodoReporte", method = RequestMethod.GET)
    public ModelAndView eliminaPeriodoReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPerRep = request.getParameter("idPerRep");

            boolean res = periodoReporteBI.eliminaPeriodoReporte(idPerRep);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERIODO REPORTE ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaReporte.json?&idReporte=<?>
    @RequestMapping(value = "/eliminaReporte", method = RequestMethod.GET)
    public ModelAndView eliminaReporte(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idReporte = request.getParameter("idReporte");

            boolean res = reporteBI.eliminaReporte(idReporte);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "REPORTE ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaGeografia.json?&idCeco=<?>
    @RequestMapping(value = "/eliminaGeografia", method = RequestMethod.GET)
    public ModelAndView eliminaGeografia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");

            boolean res = geoBI.eliminaGeografia(idCeco);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "GEOGRAFIA ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaPerfil.json?idPerfil=<?>
    @RequestMapping(value = "/eliminaPerfil", method = RequestMethod.GET)
    public ModelAndView eliminaPerfil(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idP = request.getParameter("idPerfil");

            boolean res = perfilbi.eliminaPerfil(Integer.parseInt(idP));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ID DE PERFIL BORRADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/migestion/catalogosService/eliminaPerfilUsuario.json?idUsuario=<?>&idPerfil=<?>

    @RequestMapping(value = "/eliminaPerfilUsuario", method = RequestMethod.GET)
    public ModelAndView eliminaPerfilUsuario(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
            perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));
            perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));

            boolean res = perfilusuariobi.eliminaPerfilUsaurio(perfilUsuarioDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Perfil eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaNotificaAf.json?id=<?>
    @RequestMapping(value = "/eliminaNotificaAf", method = RequestMethod.GET)
    public ModelAndView eliminaNotificaAf(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String id = request.getParameter("id");

            boolean res = asesoresDigitalesBI.elimina(Integer.parseInt(id));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se elimino el registro en la tabla GETANOTIFICA_AF ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/truncaNotificaAf.json
    @RequestMapping(value = "/truncaNotificaAf", method = RequestMethod.GET)
    public ModelAndView truncaNotificaAf(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = asesoresDigitalesBI.trunca();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se trunco la tabla GETANOTIFICA_AF: ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaCatalogoVista.json?idCatVista=<?>
    @RequestMapping(value = "/eliminaCatalogoVista", method = RequestMethod.GET)
    public ModelAndView eliminaCatalogoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int idCatVista = Integer.parseInt(request.getParameter("idCatVista"));

            boolean res = catalogoVistaBI.eliminaCatalogoVistal(idCatVista);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Vista Eliminada: ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaInfoVista.json?idInfoVista=<?>
    @RequestMapping(value = "/eliminaInfoVista", method = RequestMethod.GET)
    public ModelAndView eliminaInfoVista(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int idInfoVista = Integer.parseInt(request.getParameter("idInfoVista"));

            boolean res = infoVistaBI.eliminaInfoVistal(idInfoVista);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Info Visita Eliminada: ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaFila.json?idFila=<?>
    @RequestMapping(value = "/eliminaFila", method = RequestMethod.GET)
    public ModelAndView eliminaFila(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idFila = request.getParameter("idFila");

            boolean res = FilaBI.elimina(idFila);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Fila Eliminada ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaEmpleadoExterno.json?idUsuario=<?>

    @RequestMapping(value = "/eliminaEmpleadoExterno", method = RequestMethod.GET)
    public ModelAndView eliminaEmpleadoExterno(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));

            boolean res = usuarioexternoBI.eliminaUsuario(idUsuario);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Usuario Eliminado " + idUsuario);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/depuraUsuarioExterno.json?
    @RequestMapping(value = "/depuraUsuarioExterno", method = RequestMethod.GET)
    public ModelAndView depuraUsuarioExterno(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        try {

            boolean res = usuarioexternoBI.depuraUsuarioExterno();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SE DEPURO LA TABLA DE USUARIOS EXTERNO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaTelefonoSucursal.json?idTelefono=<?>
    @RequestMapping(value = "/eliminaTelefonoSucursal", method = RequestMethod.GET)
    public ModelAndView eliminaTelefonoSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int idTelefono = Integer.parseInt(request.getParameter("idTelefono"));

            boolean res = telsucursalBI.eliminatelefono(idTelefono);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Telefono Eliminado " + idTelefono);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/depuraTelefonosSucursal.json?
    @RequestMapping(value = "/depuraTelefonosSucursal", method = RequestMethod.GET)
    public ModelAndView depuraTelefonosSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        try {

            boolean res = telsucursalBI.depuraTelefonos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SE DEPURO LA TABLA DE TELEFONOS SUCURSAL");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaIncidenciaCatalogo.json?idTipo=<?>
    @RequestMapping(value = "/eliminaIncidenciaCatalogo", method = RequestMethod.GET)
    public ModelAndView eliminaIncidenciaCatalogo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idTipo = request.getParameter("idTipo");

            boolean res = incidenciasBI.elimina(idTipo);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Incidencia Eliminada " + idTipo);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/depuraCatalogoIncidencias.json?
    @RequestMapping(value = "/depuraCatalogoIncidencias", method = RequestMethod.GET)
    public ModelAndView depuraCatalogoIncidencias(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        try {

            boolean res = incidenciasBI.depura();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SE DEPURO LA TABLA DE CATALOGOS DE INCIDENCIAS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaCatalogoProveedor.json?idProveedor=<?>
    @RequestMapping(value = "/eliminaCatalogoProveedor", method = RequestMethod.GET)
    public ModelAndView eliminaCatalogoProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idProveedor = request.getParameter("idProveedor");

            boolean res = proveedoresBI.elimina(idProveedor);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Proveedor Eliminado " + idProveedor);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/depuraCatalogoProveedores.json?
    @RequestMapping(value = "/depuraCatalogoProveedores", method = RequestMethod.GET)
    public ModelAndView depuraCatalogoProveedores(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        try {

            boolean res = proveedoresBI.depura();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SE DEPURO LA TABLA DE CATALOGOS DE  PROVEEDORES");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio algo: " + e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaFormatoMedicionMetod.json?idtab=<?>&idusuario=<?>
    @RequestMapping(value = "/eliminaFormatoMedicionMetod", method = RequestMethod.GET)
    public ModelAndView eliminaFormatoMedicionMetod(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int idtab = Integer.parseInt(request.getParameter("idtab"));
            String idusuario = request.getParameter("idusuario");

            boolean res = formatoMetodo7sBI.elimina(idtab, idusuario);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Usuario eliminado " + idusuario);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaCedula.json?idusuario=<?>
    @RequestMapping(value = "/eliminaCedula", method = RequestMethod.GET)
    public ModelAndView eliminaCedula(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idusuario = request.getParameter("idusuario");

            boolean res = cedulaBI.elimina(idusuario);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Usuario eliminado de la tabla Cedula " + idusuario);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaEstandPuesto.json?idPuesto=<?>
    @RequestMapping(value = "/eliminaEstandPuesto", method = RequestMethod.GET)
    public ModelAndView eliminaEstandPuesto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idPuesto = Integer.parseInt(request.getParameter("idPuesto"));

            boolean res = estandPuestoBI.elimina(idPuesto);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Ceco eliminado de la tabla Estandarizacion por puesto id tabla: " + idPuesto);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaProgLimpieza.json?idProg=<?>
    @RequestMapping(value = "/eliminaProgLimpieza", method = RequestMethod.GET)
    public ModelAndView eliminaProgLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idProg = Integer.parseInt(request.getParameter("idProg"));

            boolean res = progLimpiezaBI.elimina(idProg);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Ceco eliminado de la tabla Estandarizacion por puesto  idtabla" + idProg);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaArchivoUrl.json?idArch=<?>
    @RequestMapping(value = "/eliminaArchivoUrl", method = RequestMethod.GET)
    public ModelAndView eliminaArchivoUrl(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idArch = request.getParameter("idArch");

            boolean res = archivoUrlBI.elimina(idArch);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Archivo Url" + idArch);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaLoginProveedor.json?idProveedor=<?>
    @RequestMapping(value = "/eliminaLoginProveedor", method = RequestMethod.GET)
    public ModelAndView eliminaLoginProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idProveedor = request.getParameter("idProveedor");

            boolean res = proveedorLoginBI.elimina(idProveedor);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Archivo Url" + idProveedor);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaCuadrilla.json?idCuadrilla=<?>
    @RequestMapping(value = "/eliminaCuadrilla", method = RequestMethod.GET)
    public ModelAndView eliminaCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idCuadrilla = request.getParameter("idCuadrilla");

            boolean res = cuadrillaBI.elimina(idCuadrilla);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Archivo Url" + idCuadrilla);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaTkCuadrilla.json?ticket=<?>
    @RequestMapping(value = "/eliminaTkCuadrilla", method = RequestMethod.GET)
    public ModelAndView eliminaTkCuadrilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String ticket = request.getParameter("ticket");

            boolean res = ticketCuadrillaBI.elimina(ticket);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Archivo Url" + ticket);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaFotoPlantilla.json?ceco=<?>
    @RequestMapping(value = "/eliminaFotoPlantilla", method = RequestMethod.GET)
    public ModelAndView eliminaFotoPlantilla(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String ceco = request.getParameter("ceco");

            boolean res = fotoPlantillaBI.elimina(ceco);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Foto Plantilla" + ceco);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaBitaMtto.json?idBitacora=<?>
    @RequestMapping(value = "/eliminaBitaMtto", method = RequestMethod.GET)
    public ModelAndView eliminaBitaMtto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idBitacora = Integer.parseInt(request.getParameter("idBitacora"));

            int res = bitacoraMantenimientoBI.eliminaBitacoraMnto(idBitacora);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Bitacora Plantilla" + idBitacora);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaMinucia.json?idMinucia=<?>
    @RequestMapping(value = "/eliminaMinucia", method = RequestMethod.GET)
    public ModelAndView eliminaMinucia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idMinucia = Integer.parseInt(request.getParameter("idMinucia"));

            boolean res = catalogoMinuciasBI.elimina(idMinucia);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Bitacora Plantilla" + idMinucia);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaFotoAntDesp.json?idEvento=<?>
    @RequestMapping(value = "/eliminaFotoAntDesp", method = RequestMethod.GET)
    public ModelAndView eliminaFotoAntDesp(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idEvento = Integer.parseInt(request.getParameter("idEvento"));

            int res = fotosAntesDespuesBI.eliminaFotoAntesDesp(idEvento);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Bitacora Plantilla" + idEvento);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaDepuraActFijo.json?idDepAct=<?>
    @RequestMapping(value = "/eliminaDepuraActFijo", method = RequestMethod.GET)
    public ModelAndView eliminaDepuraActFijo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idDepAct = Integer.parseInt(request.getParameter("idDepAct"));

            boolean res = depuraActFijoBI.elimina(idDepAct);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Depura activo Fijo" + idDepAct);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaActFijo.json?idActivoFijo=<?>
    @RequestMapping(value = "/eliminaActFijo", method = RequestMethod.GET)
    public ModelAndView eliminaActFijo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int idActivoFijo = Integer.parseInt(request.getParameter("idActivoFijo"));

            int res = activoFijoBI.eliminaActivoFijo(idActivoFijo);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla activo Fijo" + idActivoFijo);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaGuiaDHL.json?idGuia=<?>
    @RequestMapping(value = "/eliminaGuiaDHL", method = RequestMethod.GET)
    public ModelAndView eliminaGuiaDHL(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idGuia = request.getParameter("idGuia");

            boolean res = guiaDHLBI.elimina(idGuia);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Depura Guia DHL" + idGuia);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaGuiaDHLEvidencia.json?idEvid=<?>
    @RequestMapping(value = "/eliminaGuiaDHLEvidencia", method = RequestMethod.GET)
    public ModelAndView eliminaGuiaDHLEvidencia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idEvid = request.getParameter("idEvid");

            boolean res = guiaDHLBI.eliminaEvi(idEvid);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Depura Guia DHL" + idEvid);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaAreaApoyojson.json?idArea=<?>
    @RequestMapping(value = "/eliminaAreaApoyojson", method = RequestMethod.GET)
    public ModelAndView eliminaAreaApoyojson(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idArea = request.getParameter("idArea");

            boolean res = areaApoyoBI.elimina(idArea);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla area Apoyo" + idArea);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaAreaApoyoEvid.json?idEvid=<?>
    @RequestMapping(value = "/eliminaAreaApoyoEvid", method = RequestMethod.GET)
    public ModelAndView eliminaAreaApoyoEvid(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idEvid = request.getParameter("idEvid");

            boolean res = areaApoyoBI.eliminaEvi(idEvid);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Evidencia Area de apoyo " + idEvid);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaCartaAsignac.json?idCarta=<?>
    @RequestMapping(value = "/eliminaCartaAsignac", method = RequestMethod.GET)
    public ModelAndView eliminaCartaAsignac(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idCarta = request.getParameter("idCarta");

            boolean res = cartaAsignacionAFBI.elimina(idCarta);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Carta Asignacion " + idCarta);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaPuesto.json?idPuesto=<?>
    @RequestMapping(value = "/eliminaPuesto", method = RequestMethod.GET)
    public ModelAndView eliminaPuesto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String idPuesto = request.getParameter("idPuesto");

            boolean res = puestoBI.elimina(idPuesto);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "id eliminado de la tabla Puesto " + idPuesto);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/catalogosService/eliminaCuadrillaProveedor.json?cuadrilla=<?>&proveedor=<?>&zona=<?>
    @RequestMapping(value = "/eliminaCuadrillaProveedor", method = RequestMethod.GET)
    public ModelAndView eliminaCuadrillaProveedor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = false;

            String cuadrilla = request.getParameter("cuadrilla");
            String proveedor = request.getParameter("proveedor");
            String zona = request.getParameter("zona");

            if (cuadrilla != null && !cuadrilla.equals("")
                    && proveedor != null && !proveedor.equals("")
                    && zona != null && !zona.equals("")) {

                res = cuadrillaBI.eliminaCuadrillaProveedor(Integer.parseInt(cuadrilla), Integer.parseInt(proveedor), Integer.parseInt(zona));
            } else {

            }

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Cuadrilla eliminada " + proveedor + " id cuadrilla " + cuadrilla + " zona " + zona);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

}
