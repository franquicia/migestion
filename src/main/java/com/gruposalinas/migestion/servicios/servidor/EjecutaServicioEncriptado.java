package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.TokenBI;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import com.gruposalinas.migestion.util.UtilDate;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/ejecutaServiciosEncriptados")
public class EjecutaServicioEncriptado {

    private static final Logger logger = LogManager.getLogger(EjecutaServicioEncriptado.class);

    @Autowired
    TokenBI tokenbi;

    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getInfoTiempoRealThreads.json?idUsuario=191312&ceco=202297&nivel=Z&geografia=202297
    // - ZONA LERMA PONIENTE&negocio=82
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/createIncident.json?idUsuario=191312
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/createSession.json?idUsuario=178621&firma=123456&key=1
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getValidSession.json?idUsuario=178621&ticket=Edlav8-ZU9qUM8yM-LL3Tix7
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/closeSession.json?idUsuario=178621
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getDataSession.json?idUsuario=178621&ticket=sdas&refer=ejemplo
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getDatosRH.json?id=147247&strCC=237117&strPeriodo=20180201
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getFiltroDatosRH.json?id=189870&strCC=236737&strPeriodo=20170801&strOpcion=Plantilla
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getDetalleGarantiaRH.json?id=881948&strCC=488267&strPeriodo=20171011
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getConsumoColocacion.json?idUsuario=191312&fecha=20170803&geografia=236737
    // - TERRITORIAL CENTRO&nivel=TR
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getPersonalesColocacion.json?idUsuario=191312&fecha=20170809&geografia=236737
    // - TERRITORIAL CENTRO&nivel=TR
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getTAZColocacion.json?idUsuario=191312&fecha=20170809&geografia=236737
    // - TERRITORIAL CENTRO&nivel=TR
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getIndicadores.json?idUsuario=191312&fecha=20170813&geografia=236737
    // - TERRITORIAL CENTRO&nivel=TR
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getIndicadoresThreads.json?idUsuario=191312&fecha=20170813&geografia=236737
    // - TERRITORIAL CENTRO&nivel=TR
    // http://10.51.210.237:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/enviaCorreoBuzon.json?idUsuario=191312
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/enviaCorreoBuzon.json?idUsuario=191312
    // migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getTipificaciones.json?id=191312
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=notificaciones/creaNotificacion.json?idTipoApp=7&nombre=TEST&mensaje=hola
    // mundo&prioridad=1
    /// http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCecosCercanos.json?idUsuario=191312&ceco=232822&latitud=19.3093259&longitud=-99.1870433&valida_dist=0
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getDetalleGarantia8Sem.json?id=189870&strCC=486318&strPeriodo=20180625
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaUsuario.json?usuario=189870&ip=19.51.210.239&token=567374&ntc=1
    // http://10.51.210.239:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaFirmaAzteca.json?id=189870&usuarioAdmin=189870&firma=600167&key=1
    // http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/geTipoIncidencias.json?id=189870
    // http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getTelefonoSucursal.json?id=189870&ceco=480100
    // Mantenimiento
    // http://10.51.218.82:8080/migestion/cargaServices/cargaProveedores.json
    // http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=serviciosMantenimiento/getProveedoresXML.json?id=189870
    // http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getIncidentesSupervisor.json?id=189870&idSupervisor=10951291
    // http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getIncidentesSupervisorXML.json?id=189870
    // http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getDetallePlantillaRH.json?id=189870&strCC=482213&strPeriodo=20190809
    // http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getBandejaTicketsLimpieza.json?id=189870&idUsuarioBandeja=189870&tipoUsuario=189870
    //http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getProveedores.json?id=189870
    //http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getProveedoresLimpieza.json?id=189870
    //http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCuadrillasLimpieza.json?id=189870&id=
    //http://10.51.218.82:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getBandejaTicketsLimpieza.json?idUsuario=971858&tipoUsuario=2
    @RequestMapping(value = "/ejecutaServicio", method = RequestMethod.GET)
    public String ejecutaServicio(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException, GeneralSecurityException {
        logger.info("Entro a ejecutar servicio");

        UtilDate fec = new UtilDate();
        @SuppressWarnings("static-access")
        String fecha = fec.getSysDate("ddMMyyyyHHmmss");

        String service = new String(request.getQueryString().getBytes("ISO-8859-1"), "UTF-8");
        String rutaService = service.split("\\?")[0].replace("service=", "");
        String params = service.replaceAll(rutaService, "").replace("service=", "").replace("?", "");
        String enc = new UtilCryptoGS().encryptParams(params);
        String token = new UtilCryptoGS().encryptParams("ip=" + GTNConstantes.getIpOrigen() + "&fecha=" + fecha
                + "&idapl=666&uri=http://+" + GTNConstantes.getIpOrigen() + "/migestion/" + rutaService)
                .replace("\n", "");
        String tokenCifrado = tokenbi.getToken(token);
        String exec = (rutaService + "?" + enc + "&token=" + tokenCifrado).replace("\n", "");

        //logger.info("exc " + exec);
        return "redirect:/" + exec;
    }

}
