package com.gruposalinas.migestion.servicios.servidor;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.migestion.business.CargaBI;
import com.gruposalinas.migestion.business.ConvertImageExifBI;
import com.gruposalinas.migestion.business.CuadrillaBI;
import com.gruposalinas.migestion.business.ProveedorLoginBI;
import com.gruposalinas.migestion.business.ProveedoresBI;
import com.gruposalinas.migestion.business.ServiciosRemedyBI;
import com.gruposalinas.migestion.business.ServiciosRemedyLimpiezaBI;
import com.gruposalinas.migestion.business.SocioUnicoBI;
import com.gruposalinas.migestion.business.TelSucursalBI;
import com.gruposalinas.migestion.business.TruncTableTokenBI;
import com.gruposalinas.migestion.domain.ProveedorLoginDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.resources.GTNAuthInterceptor;

@Controller
@RequestMapping("/cargaServices")
public class CargaService {

    @Autowired
    CargaBI cargaBI;

    @Autowired
    ServiciosRemedyBI serviciosRemedyBI;

    @Autowired
    ServiciosRemedyLimpiezaBI serviciosRemedyLimpiezaBI;

    @Autowired
    SocioUnicoBI socioUnicoBI;

    @Autowired
    TelSucursalBI telSucursalBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    ProveedorLoginBI proveedorLoginBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    TruncTableTokenBI truncTableTokenBI;

    @Autowired
    ConvertImageExifBI convertImage;

    private static final Logger logger = LogManager.getLogger(GTNAuthInterceptor.class);

    // http://localhost:8080/gestion/cargaServices/cargaGeografia.json
    @RequestMapping(value = "/cargaGeografia", method = RequestMethod.GET)
    public ModelAndView cargaGeografia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaGeografia();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA GEOGRAFIA CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaUsuariosPaso.json
    @RequestMapping(value = "/cargaUsuariosPaso", method = RequestMethod.GET)
    public ModelAndView cargaUsuariosPaso(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaUsuariosPaso();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA USUARIOS PASO CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaCecosPaso.json
    @RequestMapping(value = "/cargaCecosPaso", method = RequestMethod.GET)
    public ModelAndView cargaCecosPaso(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaCecosPaso();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS PASO CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaSucursalesPaso.json
    @RequestMapping(value = "/cargaSucursalesPaso", method = RequestMethod.GET)
    public ModelAndView cargaSucursalesPaso(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaSucursalesPaso();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA SUCURSALES PASO CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaPuestos.json
    @RequestMapping(value = "/cargaPuestos", method = RequestMethod.GET)
    public ModelAndView cargaPuestos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaPuestos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA PUESTOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaUsuarios.json
    @RequestMapping(value = "/cargaUsuarios", method = RequestMethod.GET)
    public ModelAndView cargaUsuarios(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaUsuarios();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA USUARIOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaUsuariosFRQ.json
    @RequestMapping(value = "/cargaUsuariosFRQ", method = RequestMethod.GET)
    public ModelAndView cargaUsuariosFRQ(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaUsuariosFRQ();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA USUARIOS CORRECTA DESDE FRANQUICIA A GESTION");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaCecos.json
    @RequestMapping(value = "/cargaCecos", method = RequestMethod.GET)
    public ModelAndView cargaCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaCecos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaSucursales.json
    @RequestMapping(value = "/cargaSucursales", method = RequestMethod.GET)
    public ModelAndView cargaSucursales(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaSucursales();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA SUCURSALES CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaTareas.json
    @RequestMapping(value = "/cargaTareas", method = RequestMethod.GET)
    public ModelAndView cargaTareas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaTareas();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA TAREAS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/gestion/cargaServices/cargaLimpiaCecos.json
    @RequestMapping(value = "/cargaLimpiaCecos", method = RequestMethod.GET)
    public ModelAndView cargaLimpiaCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaLimpiaCecos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "DEPURACION ESPACIOS CECOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaCecosGuadalajara.json
    @RequestMapping(value = "/cargaCecosGuadalajara", method = RequestMethod.GET)
    public ModelAndView cargaCecosGuadalajara(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaCecosGuadalajara();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS GUADALAJARA CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/getFallasRemedy.json
    @RequestMapping(value = "/getFallasRemedy", method = RequestMethod.GET)
    public ModelAndView getFallasRemedy(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = serviciosRemedyBI.cargafallas();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargo la tabla de fallas");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/getFallasRemedyLimpieza.json
    @RequestMapping(value = "/getFallasRemedyLimpieza", method = RequestMethod.GET)
    public ModelAndView getFallasRemedyLimpieza(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = serviciosRemedyLimpiezaBI.cargafallas();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargo la tabla de fallas");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaTelefonos.json
    @RequestMapping(value = "/cargaTelefonos", method = RequestMethod.GET)
    public ModelAndView cargaTelefonos(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = serviciosRemedyBI.cargaTelefonos();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargo la tabla de telefonos");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaProveedores.json
    @RequestMapping(value = "/cargaProveedores", method = RequestMethod.GET)
    public ModelAndView cargaProveedores(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = serviciosRemedyBI.ConsultaProveedor();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargo la tabla de Proveedores");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaProveedoresLimpieza.json
    @RequestMapping(value = "/cargaProveedoresLimpieza", method = RequestMethod.GET)
    public ModelAndView cargaProveedoresLimpieza(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = serviciosRemedyLimpiezaBI.ConsultaProveedorLimpieza();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargo la tabla de Proveedores Limpieza");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaUsuariosExternosProductiva.json
    @RequestMapping(value = "/cargaUsuariosExternosProductiva", method = RequestMethod.GET)
    public ModelAndView cargaUsuariosExternosProductiva(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cargaBI.cargaUsuariosExternos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA USUARIOS CORRECTA DESDE TABLA DE PASO A PRODUCTIVA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaUsuariosSU.json
    @RequestMapping(value = "/cargaUsuariosSU", method = RequestMethod.GET)
    public ModelAndView cargaUsuariosSU(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = serviciosRemedyBI.cargaUsuariosExternos();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargo la tabla de Usuarios Externos");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaPerfilesMtto.json
    @RequestMapping(value = "/cargaPerfilesMtto", method = RequestMethod.GET)
    public ModelAndView cargaPerfilesMtto(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = cargaBI.cargaPerfilesMantenimiento();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargaron Los perfiles a los usuarios de Mantenimiento");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaPassProveedores.json
    @RequestMapping(value = "/cargaPassProveedores", method = RequestMethod.GET)
    public ModelAndView cargaPassProveedores(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = false;
            int resp = 0;
            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();

            for (ProveedoresDTO aux : lista) {
                List<ProveedorLoginDTO> lista2 = proveedorLoginBI.obtieneDatos(aux.getIdProveedor());
                if (lista2.isEmpty() || lista2 == null) {
                    ProveedorLoginDTO bean = new ProveedorLoginDTO();
                    bean.setIdProveedor(aux.getIdProveedor());
                    bean.setPassw("" + aux.getIdProveedor());
                    resp = proveedorLoginBI.inserta(bean);
                }
            }

            if (resp != 0) {
                res = true;
            }
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargaron Los perfiles a los usuarios de Mantenimiento");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaPassProveedoresLimpieza.json
    @RequestMapping(value = "/cargaPassProveedoresLimpieza", method = RequestMethod.GET)
    public ModelAndView cargaPassProveedoresLimpieza(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = false;
            int resp = 0;
            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();

            for (ProveedoresDTO aux : lista) {
                List<ProveedorLoginDTO> lista2 = proveedorLoginBI.obtieneDatos(aux.getIdProveedor());
                if (lista2.isEmpty() || lista2 == null) {
                    ProveedorLoginDTO bean = new ProveedorLoginDTO();
                    bean.setIdProveedor(aux.getIdProveedor());
                    bean.setPassw("" + aux.getIdProveedor());
                    resp = proveedorLoginBI.inserta(bean);
                }
            }

            if (resp != 0) {
                res = true;
            }
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargaron Los perfiles a los usuarios de Limpieza");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/getRestablecePass.json?idProveedor=<?>
    @RequestMapping(value = "/getRestablecePass", method = RequestMethod.GET)
    public ModelAndView getRestablecePass(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            boolean res = false;
            int resp = 0;

            List<ProveedorLoginDTO> lista2 = proveedorLoginBI.obtieneDatos(idProveedor);
            if (!lista2.isEmpty() || lista2 == null) {
                ProveedorLoginDTO bean = new ProveedorLoginDTO();
                bean.setIdProveedor(idProveedor);
                bean.setPassw("" + idProveedor);
                res = proveedorLoginBI.actualiza(bean);
            }

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargaron Los perfiles a los usuarios de Mantenimiento");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaPasswordCuadrillas.json
    @RequestMapping(value = "/cargaPasswordCuadrillas", method = RequestMethod.GET)
    public ModelAndView cargaPasswordCuadrillas(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = cuadrillaBI.cargapass();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargaron Los passwords de las cuadrillas");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/migestion/cargaServices/cargaUsuariosMnto.json
    @RequestMapping(value = "/cargaUsuariosMnto", method = RequestMethod.GET)
    public ModelAndView cargaUsuariosMnto(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = serviciosRemedyBI.cargaUsuariosRemedy();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Se cargaron los usuarios de mantenimiento");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://10.51.219.179:8080/migestion/cargaServices/truncateTableToken.json
    @RequestMapping(value = "/truncateTableToken", method = RequestMethod.GET)
    public ModelAndView truncateTableToken(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = truncTableTokenBI.truncTable();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TRUNCATE TABLE GESTION.GETATOKEN ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://10.51.219.179:8080/migestion/cargaServices/convertImageExif.json?path=<?>
    @RequestMapping(value = "/convertImageExif", method = RequestMethod.GET)
    public ModelAndView convertImageExif(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String path = request.getParameter("path");

            boolean res = convertImage.startConvert(path);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "MEDATA_IMAGEN");
            mv.addObject("res", res);
            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

}
