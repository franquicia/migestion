package com.gruposalinas.migestion.servicios.servidor;

import com.gruposalinas.migestion.business.ExternoPuestoBI;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/central")

public class CargaPuestosExterno {

    @Autowired
    ExternoPuestoBI externoPuestoBI;

    private static Logger logger = LogManager.getLogger(CargaPuestosExterno.class);
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100;//100MB

    //http://localhost:8080/migestion/central/documentoPuestoExterno.htm
    @RequestMapping(value = "/documentoPuestoExterno", method = RequestMethod.GET)
    public ModelAndView getFileCenter(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView mv = new ModelAndView("empleadoExterno", "command", new FileParameters());

        String fileRoute = "/franquicia/imagenes";
        String idusu = "" + request.getSession().getAttribute("idusuario");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRoute;

        File route = new File(ruta);

        String[] lista = route.list();

        if (new File(fileRoute).isDirectory()) {
            logger.info("El directorio" + fileRoute + "si existe");
            mv.addObject("empleadoExterno", new File(fileRoute).list());
            for (String arch : lista) {
                if (arch.contains(idusu)) {
                    logger.info("Si se encontro");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NombreArchivo", arch);
                    break;
                } else {
                    mv.addObject("OKARCH", "NO");
                    logger.info("No se encontro");
                }
            }

        } else {
            logger.info("El directorio no existe");
            mv.addObject("directorio", "noExiste");
        }
        mv.addObject("fileAction", "subirArchivo");
        return mv;
    }

    //http://localhost:8080/migestion/central/documentoPuestoExterno.htm
    @RequestMapping(value = "/documentoPuestoExterno", method = RequestMethod.POST)
    public ModelAndView postFileCenter(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction)
            throws IOException {

        ModelAndView mv = new ModelAndView("empleadoExterno", "command", new FileParameters());
        //System.out.println("Aki");
        fileRoute = "/franquicia/imagenes";
        String idusu = "" + request.getSession().getAttribute("idusuario");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRoute;

        File route = new File(ruta);

        String[] lista = route.list();

        if (new File(fileRoute).isDirectory()) {
            logger.info("El directorio" + fileRoute + "si existe");
            mv.addObject("listaArchivos", new File(fileRoute).list());
            for (String arch : lista) {
                if (arch.contains(idusu)) {
                    logger.info("Si se encontro");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NombreArchivo", arch);
                    break;
                } else {
                    mv.addObject("OKARCH", "NO");
                    logger.info("No se encontro archivo");
                }
            }
        } else {
            mv.addObject("directorio", "noExiste");
            logger.info("No existe");
        }

        mv.addObject("fileAction", "subirArchivo");
        return mv;

    }

    //http://localhost:8080/migestion/central/uploadExpedFiles.htm
    @RequestMapping(value = "/uploadExpedFiles", method = RequestMethod.POST)
    public ModelAndView postUploadFiles(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false) MultipartFile uploadedFile,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction)
            throws IOException {
        ModelAndView mv = new ModelAndView("empleadoExterno", "command", new FileParameters());

        String idusu = "" + request.getSession().getAttribute("idusuario");
        fileRoute = "/franquicia/imagenes";
        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRoute;

        File route = new File(ruta);

        if (route.exists()) {
            if (uploadedFile.getSize() < MAX_FILE_SIZE) {
                //if(checkContentType(uploadedFile.getContentType())) {
                FileOutputStream fileOutput = null;
                BufferedOutputStream bufferOutput = null;
                try {
                    InputStream is = uploadedFile.getInputStream();
                    BufferedReader br;
                    br = new BufferedReader(new InputStreamReader(is));

                    String line = "";

                    int superOK = 0;
                    int superNO = 0;
                    int coordOK = 0;
                    int coordNO = 0;
                    while ((line = br.readLine()) != null) {
                        logger.info("linea: " + line);
                        String[] col = line.split(",");
                        if (line.split(",")[1].toLowerCase().contains("supervisor")) {
                            boolean res = externoPuestoBI.actualizaPuesto(Integer.parseInt(line.split(",")[3]), 99999998);
                            if (res) {
                                superOK++;
                            } else {
                                superNO++;
                            }
                        }
                        if (line.split(",")[1].toLowerCase().contains("coordinador")) {
                            boolean res = externoPuestoBI.actualizaPuesto(Integer.parseInt(line.split(",")[3]), 99999999);
                            if (res) {
                                coordOK++;
                            } else {
                                coordNO++;
                            }
                        }
                    }
                    br.close();
                    is.close();

                    File uploadedFile2 = new File(ruta + uploadedFile.getOriginalFilename());
                    String cad = ruta + idusu + uploadedFile.getOriginalFilename();
                    logger.info("Archivo" + uploadedFile.getOriginalFilename() + "Fue subido Exitosamente");
                    mv.addObject("ArchivoSubido", "si");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NombreArchivo" + idusu + ".csv");

                } catch (Exception e) {
                    mv.addObject("ArchivoSubido", "no");
                    //e.printStackTrace();
                    logger.info("El archivo no se subió");
                }//catch
                finally {
                    try {
                        if (fileOutput != null) {
                            fileOutput.close();
                        }
                        if (bufferOutput != null) {
                            bufferOutput.close();
                        }
                    } catch (Exception e)//catch
                    {
                        logger.info("No cerro la conexión");
                        //e.printStackTrace();
                    }
                }//finally

                /*}else {
					mv.addObject("archivoSubido","no-compatible");
					logger.info("El archivo"+uploadedFile.getOriginalFilename()+" tiene una extension incompatible");
				}*/
            } else {
                mv.addObject("archivoSubido", "size");
                logger.info("El archivo" + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
            }
        } else {
            mv.addObject("archivoSubido", "no");
            logger.info("El directorio" + uploadedFile.getOriginalFilename() + " no existe");
        }
        mv.addObject("fileAction", fileAction);
        mv.addObject("fileRoute", fileRoute);
        return mv;

    }

    boolean checkContentType(String contentType) {

        String[] contentTypeList = {"audio/aac", "video/x-msvideo", "application/octet-stream", "text/css", "application/msword", "application/epub+zip",
            "image/gif", "text/html", "image/x-icon", "application/java-archive", "image/jpeg", "application/javascript", "application/json", "video/mpeg",
            "audio/mpeg", "video/mp4", "image/png", "application/pdf", "application/vnd.ms-powerpoint", "application/x-rar-compressed", "application/rtf",
            "audio/x-wav", "application/xhtml+xml", "application/vnd.ms-excel", "application/xml", "application/zip",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "audio/mp4", "video/quicktime",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "video/x-ms-wmv",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            null};

        for (String string : contentTypeList) {
            if (string.equals(contentType)) {
                return true;
            }
        }

        return false;
    }
}

class FileParameters {

    private MultipartFile video;
    private String fileAction;
    private String fileRoute;
    private String newFileName;
    private String oldFileName;

    public MultipartFile getVideo() {
        return video;
    }

    public void setVideo(MultipartFile video) {
        this.video = video;
    }

    public String getFileAction() {
        return fileAction;
    }

    public void setFileAction(String fileAction) {
        this.fileAction = fileAction;
    }

    public String getFileRoute() {
        return fileRoute;
    }

    public void setFileRoute(String fileRoute) {
        this.fileRoute = fileRoute;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getOldFileName() {
        return oldFileName;
    }

    public void setOldFileName(String oldFileName) {
        this.oldFileName = oldFileName;
    }
}
