package com.gruposalinas.migestion.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.domain.DatosFiltroRHDTO;
import com.gruposalinas.migestion.domain.DatosRHDTO;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import mx.com.gsalinas.encryption.AES;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hornetq.utils.json.JSONException;

public class MiGestionPruebas {

    private static Logger logger = LogManager.getLogger(MiGestionPruebas.class);


	public static void main(String[] args) {
		//System.out.println("datos: "+getDetalleCubrimiento("236066", "20170326"));
		//System.out.println("Fin");
	}




	public static String getDetalleAsesoresGarantia (String strCC, String strPeriodo) {

		String llave = "1Nd1Rh17";
		String usuario = "USR_FRANQUICIAS";
		String password = "fr4nqu1c1as";
		String usuarioCifrado = "";
		String passwordCifrado = "";	
		String salida = null;



		try {
			usuarioCifrado = AES.code(llave, usuario);
			passwordCifrado = AES.code(llave, password);
		} catch (Exception e) {
			//logger.info("Algo Ocurrio en cifrado: " + e.getMessage());
		}

		BufferedReader rd = null;
		OutputStreamWriter ou = null;
		InputStreamReader inputStream = null;

		try {

			URL url = new URL(GTNConstantes.getURLdetalleGarantiaRH());
			//URL url = new URL("http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getDetalleAsesoresGarantia");
			//logger.info("url : " +url);

			String data = URLEncoder.encode("strUsuario", "UTF-8") + "=" + URLEncoder.encode(usuarioCifrado, "UTF-8");
			data += "&" + URLEncoder.encode("strContrasenia", "UTF-8") + "=" + URLEncoder.encode(passwordCifrado, "UTF-8");
			data += "&" + URLEncoder.encode("strCC", "UTF-8") + "=" + URLEncoder.encode(strCC, "UTF-8");
			data += "&" + URLEncoder.encode("strPeriodo", "UTF-8") + "=" + URLEncoder.encode(strPeriodo, "UTF-8");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			//logger.info("http : " +conn);
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setUseCaches(false);

			//OutputStreamWriter ou = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
			ou = new OutputStreamWriter(conn.getOutputStream());
			ou.write(data);
			ou.flush();

			StringBuffer res = new StringBuffer();
			//BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream
			inputStream = new InputStreamReader(conn.getInputStream());
			rd = new BufferedReader(inputStream);
			String line = "";

			while ((line = rd.readLine()) != null) {
				res.append(line);
			}
			//ou.close();
			//rd.close();
			if(res.length()<=2){
				salida = null;
				//return null;
			}else{
				//return res.toString();
				salida = res.toString();
			}
		} catch (Exception e) {
			//logger.info("Algo ocurrio en conexion: " + e.getMessage());
			//return null;
			salida = null;
		}finally {
			try{

				if(rd != null)
					rd.close();
			}catch(Exception e){
				//logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
			
			}
			
			try{
				
				if(ou != null)
					ou.close();				
			}catch(Exception e){
				//logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
			}
			
			try{
				
				if(inputStream != null)
					inputStream.close();

			}catch(Exception e){
				//logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
			}
		}
		return salida;
	}

	public static JsonArray getReagrupaListaZA(JsonArray listaDatosRHDTO) throws JSONException, org.codehaus.jettison.json.JSONException{		

		List<DatosFiltroRHDTO> listDatosRHDTO = new ArrayList<DatosFiltroRHDTO>();     

		if (listaDatosRHDTO!= null) { 
			int len = listaDatosRHDTO.size();
			for (int i=0; i<len; i++) { 
				String datos = listaDatosRHDTO.get(i).getAsJsonObject().toString();
				JSONObject o =  new JSONObject(datos);
				listDatosRHDTO.add(new DatosFiltroRHDTO(o.getInt("idCeco"), o.getString("nomCeco"), o.getString("opcion"), o.getInt("valorOpcion"), o.getInt("valorTendencia")));
			} 
		} 
		boolean flag = false;
		int l =1;
		int sumaAct;
		int sumaAnt;
		DatosFiltroRHDTO act;
		DatosFiltroRHDTO ant;
		while(l < listDatosRHDTO.size()){

			sumaAnt = listDatosRHDTO.get(l-1).getValorOpcion();
			sumaAct = listDatosRHDTO.get(l).getValorOpcion();

			if(sumaAnt < sumaAct){
				ant=listDatosRHDTO.get(l-1);
				act=listDatosRHDTO.get(l);
				listDatosRHDTO.set(l - 1, act);
				listDatosRHDTO.set(l, ant);
				flag=true;
			}

			if (flag && (l + 1) == listDatosRHDTO.size()) {
				l = 0;
				flag = false;
			}
			l++;
		}
		Gson gson = new Gson();
		String json = gson.toJson(listDatosRHDTO);
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonArrayFinal = (JsonArray) jsonParser.parse(json);    
		return jsonArrayFinal;
	}



}
