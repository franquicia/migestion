package com.gruposalinas.migestion.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class UtilXML {

    public static StringBuilder getResponse(String url, String xmlRequest) {

        StringBuilder xmlString = new StringBuilder();

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection con = null;

        try {
            URL urlConnection = new URL(url);
            con = (HttpURLConnection) urlConnection.openConnection();

            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            con.setRequestProperty("Content-Length", Integer.toString(xmlRequest.length()));
            con.setRequestProperty("Connection", "Keep-Alive");
            con.connect();

            System.out.println("Peticion al servicio: " + xmlRequest);

            OutputStream outputStream = con.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xmlRequest, 0, xmlRequest.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputStream = con.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputStream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }

            String line;
            while ((line = rd.readLine()) != null) {
                xmlString.append(line);
            }

            inputStream.close();
            inputStream = null;

        } catch (Exception e) {
            xmlString = null;

            e.printStackTrace();
        }

        if (xmlString != null) {
            System.out.println("Respuesta del servicio SOA: \n" + xmlString.toString());
        } else {
            System.out.println("Respuesta del servicio SOA: \n");
        }

        return xmlString;
    }

    public static String getTemplates() {

        String url = "http://10.54.28.194/wsGestionIncFranquiciasApp/Service1.svc?wsdl";

        String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<tem:Templates/>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        StringBuilder strBuilder = UtilXML.getResponse(url, xmlRequest);

        return "";
    }

    public static void main(String[] args) {
        getTemplates();
    }

}
