package com.gruposalinas.migestion.util;

import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.resources.GTNResourceLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

public class UtilResource {

    public static Resource getResourceFromInternet(String paginaEstatica) {
        ApplicationContext appContext = GTNAppContextProvider.getApplicationContext();
        GTNResourceLoader resourceLoader = (GTNResourceLoader) appContext.getBean("GTNResourceLoader");
        //Resource resource = resourceLoader.getResourceLoader().getResource("http://ipdelserver/static/prueba.html");
        Resource resource = resourceLoader.getResourceLoader().getResource(GTNConstantes.getURLServer() + "/migestion/static/" + paginaEstatica);
        return resource;
    }
}
