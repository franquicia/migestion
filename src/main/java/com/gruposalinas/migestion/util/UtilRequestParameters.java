/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.util;

import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author cescobarh
 */
public class UtilRequestParameters {

    private final static UtilCryptoGS cifra = new UtilCryptoGS();

    private final static Logger logger = LogManager.getLogger(UtilRequestParameters.class);

    public static Map<String, String> getParameterFromQueryString(String queryString) throws Exception {
        try {
            Map<String, String> query_pairs = new HashMap<String, String>();
            String[] pairs = queryString.split("&");
            for (String pair : pairs) {
                try {
                    int idx = pair.indexOf("=");
                    if (idx > -1) {
                        query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
                    }
                } catch (UnsupportedEncodingException ex) {
                    logger.warn("Error", ex);
                }
            }
            String uriAp = query_pairs.get("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            String uri = pairs[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (StrCipher.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            pairs = urides.split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                if (idx > -1) {
                    String indexName = URLDecoder.decode(pair.substring(0, idx), "UTF-8");
                    String value = URLDecoder.decode(pair.substring(idx + 1), "UTF-8");
                    query_pairs.put(indexName, value.isEmpty() ? null : value);
                }
            }
            return query_pairs;

        } catch (NumberFormatException | GeneralSecurityException | IOException ex) {
            throw new Exception(ex);
        }
    }
}
