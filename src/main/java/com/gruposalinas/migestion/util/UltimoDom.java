package com.gruposalinas.migestion.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UltimoDom {

    Date fechaDate = null;
    String Domingo = null;

    public String domingo() {

        /*Definimos el formato deseado para la fecha*/
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");

        /*Obtnemos la fecha actual*/
        java.util.Date fecha = new Date();

        try {
            fechaDate = formato.parse(formato.format(fecha));
        } catch (ParseException ex) {
            System.out.println(ex);
        }

        /*A la fecha actual le restamo tres dias*/
        Calendar calendar3 = Calendar.getInstance();
        calendar3.setTime(fechaDate);
        calendar3.add(Calendar.DAY_OF_YEAR, -3);

        Domingo = (formato.format(calendar3.getTime()));
        Domingo = Domingo.replace("/", "");

        return Domingo;
    }

}
