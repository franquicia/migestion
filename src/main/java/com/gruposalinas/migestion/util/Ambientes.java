/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.util;

import com.gruposalinas.migestion.resources.GTNConstantes;

/**
 *
 * @author cescobarh
 */
public enum Ambientes {

    AMBIENTE,
    DESARROLLO,
    CALIDAD,
    PRODUCTIVO;
    final String ipHost = GTNConstantes.getIPLocal();

    public Ambientes value() {
        if (ipHost.endsWith(".74") || ipHost.endsWith(".75") || ipHost.endsWith(".76") || ipHost.endsWith(".77")) {
            return PRODUCTIVO;
        } else if (ipHost.endsWith(".47")) {
            return CALIDAD;
        } else {
            return DESARROLLO;
        }
    }

}
