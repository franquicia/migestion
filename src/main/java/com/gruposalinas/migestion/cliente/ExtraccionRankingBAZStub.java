/**
 * ExtraccionRankingBAZStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.gruposalinas.migestion.cliente;

import com.gruposalinas.migestion.resources.GTNConstantes;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/*
 *  ExtraccionRankingBAZStub java implementation
 */
public class ExtraccionRankingBAZStub extends org.apache.axis2.client.Stub {

    private static int counter = 0;
    protected org.apache.axis2.description.AxisOperation[] _operations;

    //hashmaps to keep the fault mapping
    @SuppressWarnings("rawtypes")
    private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
    @SuppressWarnings("rawtypes")
    private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
    @SuppressWarnings("rawtypes")
    private java.util.HashMap faultMessageMap = new java.util.HashMap();
    private javax.xml.namespace.QName[] opNameArray = null;

    /**
     * Constructor that takes in a configContext
     */
    public ExtraccionRankingBAZStub(
            org.apache.axis2.context.ConfigurationContext configurationContext,
            java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(configurationContext, targetEndpoint, false);
    }

    /**
     * Constructor that takes in a configContext and useseperate listner
     */
    public ExtraccionRankingBAZStub(
            org.apache.axis2.context.ConfigurationContext configurationContext,
            java.lang.String targetEndpoint, boolean useSeparateListener)
            throws org.apache.axis2.AxisFault {
        //To populate AxisService
        populateAxisService();
        populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,
                _service);

        _serviceClient.getOptions()
                .setTo(new org.apache.axis2.addressing.EndpointReference(
                        targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

        //Set the soap version
        _serviceClient.getOptions()
                .setSoapVersionURI(org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
    }

    /**
     * Default Constructor
     */
    public ExtraccionRankingBAZStub(
            org.apache.axis2.context.ConfigurationContext configurationContext)
            throws org.apache.axis2.AxisFault {
        this(configurationContext,
                //"http://10.54.24.81:7082/WebServiceExtraccionRankingBAZ/ExtraccionRankingBAZ.asmx"
                GTNConstantes.getURLProductividad()
        );

    }

    /**
     * Default Constructor
     */
    public ExtraccionRankingBAZStub() throws org.apache.axis2.AxisFault {
        this(
                //"http://10.54.24.81:7082/WebServiceExtraccionRankingBAZ/ExtraccionRankingBAZ.asmx"
                GTNConstantes.getURLProductividad()
        );
    }

    /**
     * Constructor taking the target endpoint
     */
    public ExtraccionRankingBAZStub(java.lang.String targetEndpoint)
            throws org.apache.axis2.AxisFault {
        this(null, targetEndpoint);
    }

    private static synchronized java.lang.String getUniqueSuffix() {
        // reset the counter if it is greater than 99999
        if (counter > 99999) {
            counter = 0;
        }

        counter = counter + 1;

        return java.lang.Long.toString(java.lang.System.currentTimeMillis())
                + "_" + counter;
    }

    private void populateAxisService() throws org.apache.axis2.AxisFault {
        //creating the Service with a unique name
        _service = new org.apache.axis2.description.AxisService(
                "ExtraccionRankingBAZ" + getUniqueSuffix());
        addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[1];

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://extraccionsie.com/", "extraerDatos"));
        _service.addOperation(__operation);

        _operations[0] = __operation;
    }

    //populates the faults
    private void populateFaults() {
    }

    /**
     * Auto generated method signature
     *
     * @see com.gruposalinas.migestion.cliente.ExtraccionRankingBAZ#extraerDatos
     * @param extraerDatos
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatosResponse extraerDatos(
            com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatos extraerDatos)
            throws java.rmi.RemoteException {
        org.apache.axis2.context.MessageContext _messageContext = null;

        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
            _operationClient.getOptions()
                    .setAction("http://extraccionsie.com/ExtraerDatos");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

            addPropertyToOperationClient(_operationClient,
                    org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                    "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                    .getSoapVersionURI()),
                    extraerDatos,
                    optimizeContent(
                            new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "extraerDatos")),
                    new javax.xml.namespace.QName("http://extraccionsie.com/",
                            "ExtraerDatos"));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

            java.lang.Object object = fromOM(_returnEnv.getBody()
                    .getFirstElement(),
                    com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatosResponse.class);

            return (com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatosResponse) object;
        } catch (org.apache.axis2.AxisFault f) {
            org.apache.axiom.om.OMElement faultElt = f.getDetail();

            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(
                        new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(), "ExtraerDatos"))) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(), "ExtraerDatos"));
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                        java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(), "ExtraerDatos"));
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                                messageClass);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                new java.lang.Class[]{messageClass});
                        m.invoke(ex, new java.lang.Object[]{messageObject});

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            if (_messageContext.getTransportOut() != null) {
                _messageContext.getTransportOut().getSender()
                        .cleanup(_messageContext);
            }
        }
    }

    private boolean optimizeContent(javax.xml.namespace.QName opName) {
        if (opNameArray == null) {
            return false;
        }

        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;
            }
        }

        return false;
    }

    @SuppressWarnings("unused")
    private org.apache.axiom.om.OMElement toOM(
            com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatos param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatos.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    @SuppressWarnings("unused")
    private org.apache.axiom.om.OMElement toOM(
            com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatosResponse param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatosResponse.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
            org.apache.axiom.soap.SOAPFactory factory,
            com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatos param,
            boolean optimizeContent, javax.xml.namespace.QName elementQName)
            throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope.getBody()
                    .addChild(param.getOMElement(
                            com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatos.MY_QNAME,
                            factory));

            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    /* methods to provide back word compatibility */
    /**
     * get the default envelope
     */
    @SuppressWarnings("unused")
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
            org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    @SuppressWarnings("rawtypes")
    private java.lang.Object fromOM(org.apache.axiom.om.OMElement param,
            java.lang.Class type) throws org.apache.axis2.AxisFault {
        try {
            if (com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatos.class.equals(
                    type)) {
                return com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatos.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatosResponse.class.equals(
                    type)) {
                return com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.ExtraerDatosResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }
        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

        return null;
    }

    //http://10.54.24.81:7082/WebServiceExtraccionRankingBAZ/ExtraccionRankingBAZ.asmx
    @SuppressWarnings("serial")
    public static class ExtraerDatos implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://extraccionsie.com/",
                "ExtraerDatos", "ns1");

        /**
         * field for Nivel
         */
        protected java.lang.String localNivel;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localNivelTracker = false;

        /**
         * field for Fecha
         */
        protected int localFecha;

        /**
         * field for TipoGeografia
         */
        protected int localTipoGeografia;

        /**
         * field for CentroCostos
         */
        protected int localCentroCostos;

        /**
         * field for RowInicio
         */
        protected int localRowInicio;

        /**
         * field for RowFin
         */
        protected int localRowFin;

        public boolean isNivelSpecified() {
            return localNivelTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getNivel() {
            return localNivel;
        }

        /**
         * Auto generated setter method
         *
         * @param param Nivel
         */
        public void setNivel(java.lang.String param) {
            localNivelTracker = param != null;

            this.localNivel = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getFecha() {
            return localFecha;
        }

        /**
         * Auto generated setter method
         *
         * @param param Fecha
         */
        public void setFecha(int param) {
            this.localFecha = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getTipoGeografia() {
            return localTipoGeografia;
        }

        /**
         * Auto generated setter method
         *
         * @param param TipoGeografia
         */
        public void setTipoGeografia(int param) {
            this.localTipoGeografia = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getCentroCostos() {
            return localCentroCostos;
        }

        /**
         * Auto generated setter method
         *
         * @param param CentroCostos
         */
        public void setCentroCostos(int param) {
            this.localCentroCostos = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getRowInicio() {
            return localRowInicio;
        }

        /**
         * Auto generated setter method
         *
         * @param param RowInicio
         */
        public void setRowInicio(int param) {
            this.localRowInicio = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getRowFin() {
            return localRowFin;
        }

        /**
         * Auto generated setter method
         *
         * @param param RowFin
         */
        public void setRowFin(int param) {
            this.localRowFin = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                    xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://extraccionsie.com/");

                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":ExtraerDatos", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "ExtraerDatos", xmlWriter);
                }
            }

            if (localNivelTracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "nivel", xmlWriter);

                if (localNivel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "nivel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localNivel);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "fecha", xmlWriter);

            if (localFecha == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "fecha cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localFecha));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "tipoGeografia", xmlWriter);

            if (localTipoGeografia == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "tipoGeografia cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localTipoGeografia));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "centroCostos", xmlWriter);

            if (localCentroCostos == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "centroCostos cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localCentroCostos));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "rowInicio", xmlWriter);

            if (localRowInicio == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "rowInicio cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localRowInicio));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "rowFin", xmlWriter);

            if (localRowFin == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "rowFin cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localRowFin));
            }

            xmlWriter.writeEndElement();

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace.equals("http://extraccionsie.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
                java.lang.String namespace, java.lang.String localPart,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                        attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                        namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                        attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */
        @SuppressWarnings("unused")
        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        @SuppressWarnings("unused")
        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                            qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            @SuppressWarnings("unused")
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            @SuppressWarnings({"rawtypes", "unused"})
            public static ExtraerDatos parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                ExtraerDatos object = new ExtraerDatos();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                            if (!"ExtraerDatos".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                        .getNamespaceURI(nsPrefix);

                                return (ExtraerDatos) ExtensionMapper.getTypeObject(nsUri,
                                        type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "nivel").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "nivel").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "nivel" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setNivel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "fecha").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "fecha").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "fecha" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setFecha(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "tipoGeografia").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "tipoGeografia").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "tipoGeografia"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTipoGeografia(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "centroCostos").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "centroCostos").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "centroCostos"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCentroCostos(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "rowInicio").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "rowInicio").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "rowInicio"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRowInicio(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "rowFin").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "rowFin").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "rowFin"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRowFin(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class

        @Override
        public XMLStreamReader getPullParser(QName arg0) throws XMLStreamException {
            // TODO Auto-generated method stub
            return null;
        }
    }

    public static class ExtensionMapper {

        public static java.lang.Object getTypeObject(
                java.lang.String namespaceURI, java.lang.String typeName,
                javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            if ("http://extraccionsie.com/".equals(namespaceURI)
                    && "XML".equals(typeName)) {
                return XML.Factory.parse(reader);
            }

            if ("http://extraccionsie.com/".equals(namespaceURI)
                    && "ArrayOfXML".equals(typeName)) {
                return ArrayOfXML.Factory.parse(reader);
            }

            throw new org.apache.axis2.databinding.ADBException(
                    "Unsupported type " + namespaceURI + " " + typeName);
        }
    }

    @SuppressWarnings("serial")
    public static class ExtraerDatosResponse implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://extraccionsie.com/",
                "ExtraerDatosResponse", "ns1");

        /**
         * field for ExtraerDatosResult
         */
        protected ArrayOfXML localExtraerDatosResult;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localExtraerDatosResultTracker = false;

        public boolean isExtraerDatosResultSpecified() {
            return localExtraerDatosResultTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return ArrayOfXML
         */
        public ArrayOfXML getExtraerDatosResult() {
            return localExtraerDatosResult;
        }

        /**
         * Auto generated setter method
         *
         * @param param ExtraerDatosResult
         */
        public void setExtraerDatosResult(ArrayOfXML param) {
            localExtraerDatosResultTracker = param != null;

            this.localExtraerDatosResult = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                    xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://extraccionsie.com/");

                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":ExtraerDatosResponse", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "ExtraerDatosResponse", xmlWriter);
                }
            }

            if (localExtraerDatosResultTracker) {
                if (localExtraerDatosResult == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                            "ExtraerDatosResult cannot be null!!");
                }

                localExtraerDatosResult.serialize(new javax.xml.namespace.QName(
                        "http://extraccionsie.com/", "ExtraerDatosResult"),
                        xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace.equals("http://extraccionsie.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
                java.lang.String namespace, java.lang.String localPart,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                        attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                        namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                        attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */
        @SuppressWarnings("unused")
        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        @SuppressWarnings("unused")
        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                            qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            @SuppressWarnings("unused")
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            @SuppressWarnings({"unused", "rawtypes"})
            public static ExtraerDatosResponse parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                ExtraerDatosResponse object = new ExtraerDatosResponse();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                            if (!"ExtraerDatosResponse".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                        .getNamespaceURI(nsPrefix);

                                return (ExtraerDatosResponse) ExtensionMapper.getTypeObject(nsUri,
                                        type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/",
                                    "ExtraerDatosResult").equals(reader.getName()))
                            || new javax.xml.namespace.QName("",
                                    "ExtraerDatosResult").equals(reader.getName())) {
                        object.setExtraerDatosResult(ArrayOfXML.Factory.parse(
                                reader));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class

        @Override
        public XMLStreamReader getPullParser(QName arg0) throws XMLStreamException {
            // TODO Auto-generated method stub
            return null;
        }
    }

    @SuppressWarnings("serial")
    public static class XML implements org.apache.axis2.databinding.ADBBean {

        /* This type was generated from the piece of schema that had
           name = XML
           Namespace URI = http://extraccionsie.com/
           Namespace Prefix = ns1
         */
        /**
         * field for Nivel
         */
        protected java.lang.String localNivel;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localNivelTracker = false;

        /**
         * field for Fecha
         */
        protected java.lang.String localFecha;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localFechaTracker = false;

        /**
         * field for TipoGeografia
         */
        protected java.lang.String localTipoGeografia;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localTipoGeografiaTracker = false;

        /**
         * field for POSICION
         */
        protected java.lang.String localPOSICION;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPOSICIONTracker = false;

        /**
         * field for Geografia
         */
        protected java.lang.String localGeografia;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localGeografiaTracker = false;

        /**
         * field for Contribucion
         */
        protected double localContribucion;

        /**
         * field for POS_CONT
         */
        protected double localPOS_CONT;

        /**
         * field for PRESTAMOS
         */
        protected double localPRESTAMOS;

        /**
         * field for POS_PRES
         */
        protected double localPOS_PRES;

        /**
         * field for CAPTACION
         */
        protected double localCAPTACION;

        /**
         * field for POS_CAPT
         */
        protected double localPOS_CAPT;

        /**
         * field for SIPA
         */
        protected double localSIPA;

        /**
         * field for POS_SIPA
         */
        protected double localPOS_SIPA;

        /**
         * field for SEGUROS
         */
        protected double localSEGUROS;

        /**
         * field for POS_SEGU
         */
        protected double localPOS_SEGU;

        /**
         * field for GENTE
         */
        protected double localGENTE;

        /**
         * field for POS_GENT
         */
        protected double localPOS_GENT;

        /**
         * field for AFORE
         */
        protected double localAFORE;

        /**
         * field for POS_AFOR
         */
        protected double localPOS_AFOR;

        /**
         * field for TOTAL
         */
        protected double localTOTAL;

        /**
         * field for ID_CC
         */
        protected int localID_CC;

        /**
         * field for CC
         */
        protected int localCC;

        /**
         * field for EMPLEADO
         */
        protected int localEMPLEADO;

        /**
         * field for NOMBRE
         */
        protected java.lang.String localNOMBRE;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localNOMBRETracker = false;

        /**
         * field for DESDE
         */
        protected java.lang.String localDESDE;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localDESDETracker = false;

        /**
         * field for T_REG
         */
        protected double localT_REG;

        public boolean isNivelSpecified() {
            return localNivelTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getNivel() {
            return localNivel;
        }

        /**
         * Auto generated setter method
         *
         * @param param Nivel
         */
        public void setNivel(java.lang.String param) {
            localNivelTracker = param != null;

            this.localNivel = param;
        }

        public boolean isFechaSpecified() {
            return localFechaTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getFecha() {
            return localFecha;
        }

        /**
         * Auto generated setter method
         *
         * @param param Fecha
         */
        public void setFecha(java.lang.String param) {
            localFechaTracker = param != null;

            this.localFecha = param;
        }

        public boolean isTipoGeografiaSpecified() {
            return localTipoGeografiaTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getTipoGeografia() {
            return localTipoGeografia;
        }

        /**
         * Auto generated setter method
         *
         * @param param TipoGeografia
         */
        public void setTipoGeografia(java.lang.String param) {
            localTipoGeografiaTracker = param != null;

            this.localTipoGeografia = param;
        }

        public boolean isPOSICIONSpecified() {
            return localPOSICIONTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getPOSICION() {
            return localPOSICION;
        }

        /**
         * Auto generated setter method
         *
         * @param param POSICION
         */
        public void setPOSICION(java.lang.String param) {
            localPOSICIONTracker = param != null;

            this.localPOSICION = param;
        }

        public boolean isGeografiaSpecified() {
            return localGeografiaTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getGeografia() {
            return localGeografia;
        }

        /**
         * Auto generated setter method
         *
         * @param param Geografia
         */
        public void setGeografia(java.lang.String param) {
            localGeografiaTracker = param != null;

            this.localGeografia = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getContribucion() {
            return localContribucion;
        }

        /**
         * Auto generated setter method
         *
         * @param param Contribucion
         */
        public void setContribucion(double param) {
            this.localContribucion = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPOS_CONT() {
            return localPOS_CONT;
        }

        /**
         * Auto generated setter method
         *
         * @param param POS_CONT
         */
        public void setPOS_CONT(double param) {
            this.localPOS_CONT = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPRESTAMOS() {
            return localPRESTAMOS;
        }

        /**
         * Auto generated setter method
         *
         * @param param PRESTAMOS
         */
        public void setPRESTAMOS(double param) {
            this.localPRESTAMOS = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPOS_PRES() {
            return localPOS_PRES;
        }

        /**
         * Auto generated setter method
         *
         * @param param POS_PRES
         */
        public void setPOS_PRES(double param) {
            this.localPOS_PRES = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getCAPTACION() {
            return localCAPTACION;
        }

        /**
         * Auto generated setter method
         *
         * @param param CAPTACION
         */
        public void setCAPTACION(double param) {
            this.localCAPTACION = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPOS_CAPT() {
            return localPOS_CAPT;
        }

        /**
         * Auto generated setter method
         *
         * @param param POS_CAPT
         */
        public void setPOS_CAPT(double param) {
            this.localPOS_CAPT = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getSIPA() {
            return localSIPA;
        }

        /**
         * Auto generated setter method
         *
         * @param param SIPA
         */
        public void setSIPA(double param) {
            this.localSIPA = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPOS_SIPA() {
            return localPOS_SIPA;
        }

        /**
         * Auto generated setter method
         *
         * @param param POS_SIPA
         */
        public void setPOS_SIPA(double param) {
            this.localPOS_SIPA = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getSEGUROS() {
            return localSEGUROS;
        }

        /**
         * Auto generated setter method
         *
         * @param param SEGUROS
         */
        public void setSEGUROS(double param) {
            this.localSEGUROS = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPOS_SEGU() {
            return localPOS_SEGU;
        }

        /**
         * Auto generated setter method
         *
         * @param param POS_SEGU
         */
        public void setPOS_SEGU(double param) {
            this.localPOS_SEGU = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getGENTE() {
            return localGENTE;
        }

        /**
         * Auto generated setter method
         *
         * @param param GENTE
         */
        public void setGENTE(double param) {
            this.localGENTE = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPOS_GENT() {
            return localPOS_GENT;
        }

        /**
         * Auto generated setter method
         *
         * @param param POS_GENT
         */
        public void setPOS_GENT(double param) {
            this.localPOS_GENT = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getAFORE() {
            return localAFORE;
        }

        /**
         * Auto generated setter method
         *
         * @param param AFORE
         */
        public void setAFORE(double param) {
            this.localAFORE = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getPOS_AFOR() {
            return localPOS_AFOR;
        }

        /**
         * Auto generated setter method
         *
         * @param param POS_AFOR
         */
        public void setPOS_AFOR(double param) {
            this.localPOS_AFOR = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getTOTAL() {
            return localTOTAL;
        }

        /**
         * Auto generated setter method
         *
         * @param param TOTAL
         */
        public void setTOTAL(double param) {
            this.localTOTAL = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getID_CC() {
            return localID_CC;
        }

        /**
         * Auto generated setter method
         *
         * @param param ID_CC
         */
        public void setID_CC(int param) {
            this.localID_CC = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getCC() {
            return localCC;
        }

        /**
         * Auto generated setter method
         *
         * @param param CC
         */
        public void setCC(int param) {
            this.localCC = param;
        }

        /**
         * Auto generated getter method
         *
         * @return int
         */
        public int getEMPLEADO() {
            return localEMPLEADO;
        }

        /**
         * Auto generated setter method
         *
         * @param param EMPLEADO
         */
        public void setEMPLEADO(int param) {
            this.localEMPLEADO = param;
        }

        public boolean isNOMBRESpecified() {
            return localNOMBRETracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getNOMBRE() {
            return localNOMBRE;
        }

        /**
         * Auto generated setter method
         *
         * @param param NOMBRE
         */
        public void setNOMBRE(java.lang.String param) {
            localNOMBRETracker = param != null;

            this.localNOMBRE = param;
        }

        public boolean isDESDESpecified() {
            return localDESDETracker;
        }

        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getDESDE() {
            return localDESDE;
        }

        /**
         * Auto generated setter method
         *
         * @param param DESDE
         */
        public void setDESDE(java.lang.String param) {
            localDESDETracker = param != null;

            this.localDESDE = param;
        }

        /**
         * Auto generated getter method
         *
         * @return double
         */
        public double getT_REG() {
            return localT_REG;
        }

        /**
         * Auto generated setter method
         *
         * @param param T_REG
         */
        public void setT_REG(double param) {
            this.localT_REG = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                    xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://extraccionsie.com/");

                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":XML", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "XML", xmlWriter);
                }
            }

            if (localNivelTracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "Nivel", xmlWriter);

                if (localNivel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "Nivel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localNivel);
                }

                xmlWriter.writeEndElement();
            }

            if (localFechaTracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "Fecha", xmlWriter);

                if (localFecha == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "Fecha cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localFecha);
                }

                xmlWriter.writeEndElement();
            }

            if (localTipoGeografiaTracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "TipoGeografia", xmlWriter);

                if (localTipoGeografia == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "TipoGeografia cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localTipoGeografia);
                }

                xmlWriter.writeEndElement();
            }

            if (localPOSICIONTracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "POSICION", xmlWriter);

                if (localPOSICION == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "POSICION cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPOSICION);
                }

                xmlWriter.writeEndElement();
            }

            if (localGeografiaTracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "Geografia", xmlWriter);

                if (localGeografia == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "Geografia cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localGeografia);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "Contribucion", xmlWriter);

            if (java.lang.Double.isNaN(localContribucion)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "Contribucion cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localContribucion));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "POS_CONT", xmlWriter);

            if (java.lang.Double.isNaN(localPOS_CONT)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "POS_CONT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPOS_CONT));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "PRESTAMOS", xmlWriter);

            if (java.lang.Double.isNaN(localPRESTAMOS)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "PRESTAMOS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPRESTAMOS));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "POS_PRES", xmlWriter);

            if (java.lang.Double.isNaN(localPOS_PRES)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "POS_PRES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPOS_PRES));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "CAPTACION", xmlWriter);

            if (java.lang.Double.isNaN(localCAPTACION)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "CAPTACION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localCAPTACION));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "POS_CAPT", xmlWriter);

            if (java.lang.Double.isNaN(localPOS_CAPT)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "POS_CAPT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPOS_CAPT));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "SIPA", xmlWriter);

            if (java.lang.Double.isNaN(localSIPA)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "SIPA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localSIPA));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "POS_SIPA", xmlWriter);

            if (java.lang.Double.isNaN(localPOS_SIPA)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "POS_SIPA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPOS_SIPA));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "SEGUROS", xmlWriter);

            if (java.lang.Double.isNaN(localSEGUROS)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "SEGUROS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localSEGUROS));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "POS_SEGU", xmlWriter);

            if (java.lang.Double.isNaN(localPOS_SEGU)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "POS_SEGU cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPOS_SEGU));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "GENTE", xmlWriter);

            if (java.lang.Double.isNaN(localGENTE)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "GENTE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localGENTE));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "POS_GENT", xmlWriter);

            if (java.lang.Double.isNaN(localPOS_GENT)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "POS_GENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPOS_GENT));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "AFORE", xmlWriter);

            if (java.lang.Double.isNaN(localAFORE)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "AFORE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localAFORE));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "POS_AFOR", xmlWriter);

            if (java.lang.Double.isNaN(localPOS_AFOR)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "POS_AFOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPOS_AFOR));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "TOTAL", xmlWriter);

            if (java.lang.Double.isNaN(localTOTAL)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "TOTAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localTOTAL));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "ID_CC", xmlWriter);

            if (localID_CC == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "ID_CC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localID_CC));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "CC", xmlWriter);

            if (localCC == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "CC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localCC));
            }

            xmlWriter.writeEndElement();

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "EMPLEADO", xmlWriter);

            if (localEMPLEADO == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                        "EMPLEADO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localEMPLEADO));
            }

            xmlWriter.writeEndElement();

            if (localNOMBRETracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "NOMBRE", xmlWriter);

                if (localNOMBRE == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "NOMBRE cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localNOMBRE);
                }

                xmlWriter.writeEndElement();
            }

            if (localDESDETracker) {
                namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "DESDE", xmlWriter);

                if (localDESDE == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                            "DESDE cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDESDE);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://extraccionsie.com/";
            writeStartElement(null, namespace, "T_REG", xmlWriter);

            if (java.lang.Double.isNaN(localT_REG)) {
                throw new org.apache.axis2.databinding.ADBException(
                        "T_REG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localT_REG));
            }

            xmlWriter.writeEndElement();

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace.equals("http://extraccionsie.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
                java.lang.String namespace, java.lang.String localPart,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                        attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                        namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                        attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */
        @SuppressWarnings("unused")
        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        @SuppressWarnings("unused")
        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                            qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            @SuppressWarnings("unused")
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            @SuppressWarnings({"unused", "rawtypes"})
            public static XML parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                XML object = new XML();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                            if (!"XML".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                        .getNamespaceURI(nsPrefix);

                                return (XML) ExtensionMapper.getTypeObject(nsUri,
                                        type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "Nivel").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "Nivel").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "Nivel" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setNivel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "Fecha").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "Fecha").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "Fecha" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setFecha(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "TipoGeografia").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "TipoGeografia").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "TipoGeografia"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTipoGeografia(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POSICION").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POSICION").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POSICION"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOSICION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "Geografia").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "Geografia").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "Geografia"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setGeografia(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "Contribucion").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "Contribucion").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "Contribucion"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setContribucion(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POS_CONT").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POS_CONT").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POS_CONT"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOS_CONT(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "PRESTAMOS").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "PRESTAMOS").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "PRESTAMOS"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPRESTAMOS(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POS_PRES").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POS_PRES").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POS_PRES"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOS_PRES(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "CAPTACION").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "CAPTACION").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "CAPTACION"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCAPTACION(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POS_CAPT").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POS_CAPT").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POS_CAPT"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOS_CAPT(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "SIPA").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "SIPA").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "SIPA" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setSIPA(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POS_SIPA").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POS_SIPA").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POS_SIPA"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOS_SIPA(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "SEGUROS").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "SEGUROS").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "SEGUROS"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setSEGUROS(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POS_SEGU").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POS_SEGU").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POS_SEGU"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOS_SEGU(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "GENTE").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "GENTE").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "GENTE" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setGENTE(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POS_GENT").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POS_GENT").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POS_GENT"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOS_GENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "AFORE").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "AFORE").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "AFORE" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAFORE(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "POS_AFOR").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "POS_AFOR").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "POS_AFOR"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPOS_AFOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "TOTAL").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "TOTAL").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "TOTAL" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTOTAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "ID_CC").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "ID_CC").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "ID_CC" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setID_CC(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "CC").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "CC").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "CC" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCC(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "EMPLEADO").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "EMPLEADO").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "EMPLEADO"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setEMPLEADO(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "NOMBRE").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "NOMBRE").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "NOMBRE"
                                    + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setNOMBRE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "DESDE").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "DESDE").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "DESDE" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDESDE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "T_REG").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "T_REG").equals(
                                    reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue)
                                || "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                    "The element: " + "T_REG" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setT_REG(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(
                                content));

                        reader.next();
                    } // End of if for expected property start element
                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class

        @Override
        public XMLStreamReader getPullParser(QName arg0) throws XMLStreamException {
            // TODO Auto-generated method stub
            return null;
        }
    }

    @SuppressWarnings("serial")
    public static class ArrayOfXMLE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://extraccionsie.com/",
                "ArrayOfXML", "ns1");

        /**
         * field for ArrayOfXML
         */
        protected ArrayOfXML localArrayOfXML;

        /**
         * Auto generated getter method
         *
         * @return ArrayOfXML
         */
        public ArrayOfXML getArrayOfXML() {
            return localArrayOfXML;
        }

        /**
         * Auto generated setter method
         *
         * @param param ArrayOfXML
         */
        public void setArrayOfXML(ArrayOfXML param) {
            this.localArrayOfXML = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                    this, MY_QNAME));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localArrayOfXML == null) {
                java.lang.String namespace = "http://extraccionsie.com/";
                writeStartElement(null, namespace, "ArrayOfXML", xmlWriter);

                // write the nil attribute
                writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "nil", "1",
                        xmlWriter);
                xmlWriter.writeEndElement();
            } else {
                localArrayOfXML.serialize(MY_QNAME, xmlWriter);
            }
        }

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace.equals("http://extraccionsie.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
                java.lang.String namespace, java.lang.String localPart,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                        attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                        namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                        attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */
        @SuppressWarnings("unused")
        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        @SuppressWarnings("unused")
        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                            qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            @SuppressWarnings("unused")
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            @SuppressWarnings({"rawtypes", "unused"})
            public static ArrayOfXMLE parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                ArrayOfXMLE object = new ArrayOfXMLE();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    currentQName = reader.getName();

                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue)
                            || "1".equals(nillableValue)) {
                        // Skip the element and report the null value.  It cannot have subelements.
                        while (!reader.isEndElement()) {
                            reader.next();
                        }

                        return object;
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if ((reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://extraccionsie.com/",
                                            "ArrayOfXML").equals(reader.getName()))
                                    || new javax.xml.namespace.QName("",
                                            "ArrayOfXML").equals(reader.getName())) {
                                nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                        "nil");

                                if ("true".equals(nillableValue)
                                        || "1".equals(nillableValue)) {
                                    object.setArrayOfXML(null);
                                    reader.next();
                                } else {
                                    object.setArrayOfXML(ArrayOfXML.Factory.parse(
                                            reader));
                                }
                            } // End of if for expected property start element
                            else {
                                // 3 - A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                        "Unexpected subelement "
                                        + reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class

        @Override
        public XMLStreamReader getPullParser(QName arg0) throws XMLStreamException {
            // TODO Auto-generated method stub
            return null;
        }
    }

    @SuppressWarnings("serial")
    public static class ArrayOfXML implements org.apache.axis2.databinding.ADBBean {

        /* This type was generated from the piece of schema that had
           name = ArrayOfXML
           Namespace URI = http://extraccionsie.com/
           Namespace Prefix = ns1
         */
        /**
         * field for XML This was an Array!
         */
        protected XML[] localXML;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localXMLTracker = false;

        public boolean isXMLSpecified() {
            return localXMLTracker;
        }

        /**
         * Auto generated getter method
         *
         * @return XML[]
         */
        public XML[] getXML() {
            return localXML;
        }

        /**
         * validate the array for XML
         */
        protected void validateXML(XML[] param) {
        }

        /**
         * Auto generated setter method
         *
         * @param param XML
         */
        public void setXML(XML[] param) {
            validateXML(param);

            localXMLTracker = param != null;

            this.localXML = param;
        }

        /**
         * Auto generated add method for the array for convenience
         *
         * @param param XML
         */
        @SuppressWarnings({"unchecked", "rawtypes"})
        public void addXML(XML param) {
            if (localXML == null) {
                localXML = new XML[]{};
            }

            //update the setting tracker
            localXMLTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localXML);
            list.add(param);
            this.localXML = (XML[]) list.toArray(new XML[list.size()]);
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory)
                throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                    this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                    xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://extraccionsie.com/");

                if ((namespacePrefix != null)
                        && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":ArrayOfXML", xmlWriter);
                } else {
                    writeAttribute("xsi",
                            "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "ArrayOfXML", xmlWriter);
                }
            }

            if (localXMLTracker) {
                if (localXML != null) {
                    for (int i = 0; i < localXML.length; i++) {
                        if (localXML[i] != null) {
                            localXML[i].serialize(new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "XML"),
                                    xmlWriter);
                        } else {
                            // we don't have to do any thing since minOccures is zero
                        }
                    }
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                            "XML cannot be null!!");
                }
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
                java.lang.String namespace) {
            if (namespace.equals("http://extraccionsie.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
                java.lang.String namespace, java.lang.String localPart,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
                java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                        attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeAttribute(java.lang.String namespace,
                java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                        namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        @SuppressWarnings("unused")
        private void writeQNameAttribute(java.lang.String namespace,
                java.lang.String attName, javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                        attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */
        @SuppressWarnings("unused")
        private void writeQName(javax.xml.namespace.QName qname,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        @SuppressWarnings("unused")
        private void writeQNames(javax.xml.namespace.QName[] qnames,
                javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                            qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
                javax.xml.stream.XMLStreamWriter xmlWriter,
                java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * Factory class that keeps the parse method
         */
        public static class Factory {

            @SuppressWarnings("unused")
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object Precondition: If this object
             * is an element, the current or next start element starts this
             * object and any intervening reader events are ignorable If this
             * object is not an element, it is a complex type and the reader is
             * at the event just after the outer start element Postcondition: If
             * this object is an element, the reader is positioned at its end
             * element If this object is a complex type, the reader is
             * positioned at the end element of its outer element
             */
            @SuppressWarnings({"unused", "rawtypes", "unchecked"})
            public static ArrayOfXML parse(
                    javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                ArrayOfXML object = new ArrayOfXML();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance",
                            "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                            if (!"ArrayOfXML".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                        .getNamespaceURI(nsPrefix);

                                return (ArrayOfXML) ExtensionMapper.getTypeObject(nsUri,
                                        type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if ((reader.isStartElement()
                            && new javax.xml.namespace.QName(
                                    "http://extraccionsie.com/", "XML").equals(
                                    reader.getName()))
                            || new javax.xml.namespace.QName("", "XML").equals(
                                    reader.getName())) {
                        // Process the array and step past its final element's end.
                        list1.add(XML.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;

                        while (!loopDone1) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement()) {
                                reader.next();
                            }

                            // Step out of this element
                            reader.next();

                            // Step to next element event.
                            while (!reader.isStartElement()
                                    && !reader.isEndElement()) {
                                reader.next();
                            }

                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            } else {
                                if (new javax.xml.namespace.QName(
                                        "http://extraccionsie.com/", "XML").equals(
                                        reader.getName())) {
                                    list1.add(XML.Factory.parse(reader));
                                } else {
                                    loopDone1 = true;
                                }
                            }
                        }

                        // call the converter utility  to convert and set the array
                        object.setXML((XML[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                XML.class, list1));
                    } // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) {
                        reader.next();
                    }

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                                "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class

        @Override
        public XMLStreamReader getPullParser(QName arg0) throws XMLStreamException {
            // TODO Auto-generated method stub
            return null;
        }
    }
}
