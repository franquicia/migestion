package com.gruposalinas.migestion.cliente;

import com.gruposalinas.migestion.util.UtilDate;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class ConsumoRemedyXML {

    private static final Logger logger = LogManager.getLogger(ConsumoRemedyXML.class);

    /**
     * ****************************** 1 - 7
     * **************************************
     */
    /**
     * ****************************** 8 - 14
     * **************************************
     */
    // 11
    public static String getProveedores() {

        String estatus = "-";

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        try {

            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 8080));

            UtilDate date = new UtilDate();
            @SuppressWarnings("static-access")
            String fecha = date.getSysDate("yyyy-MM-dd");

            URL url = new URL("http://10.54.21.38/Arsys/WCFMantenimiento/Service.svc" + "?WSDL");
            rc = (HttpURLConnection) url.openConnection(proxy);

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:elek=\"http://Elektra.com\">\n"
                    + "   <soapenv:Header/>\n" + "   <soapenv:Body>\n" + "      <tem:ConsultaProveedores>\n"
                    + "         <!--Optional:-->\n" + "         <tem:authentication>\n"
                    + "            <elek:Password>demoWsM</elek:Password>\n"
                    + "            <elek:UserName>demoWsM</elek:UserName>\n" + "         </tem:authentication>\n"
                    + "         <!--Optional:-->\n" + "         <tem:fecha>" + fecha + "</tem:fecha>\n"
                    + "      </tem:ConsultaProveedores>\n" + "   </soapenv:Body>\n" + "</soapenv:Envelope>\n" + "";

            //logger.info("PETICION SOA: " + xml);
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.setRequestProperty("SOAPAction", "http://tempuri.org/IService/ConsultaProveedores");
            rc.connect();

            try {
                wr = new OutputStreamWriter(rc.getOutputStream());
                wr.write(xml, 0, xml.length());
                wr.flush();
            } catch (Exception e) {
                wr.close();
                wr = null;
            }

            try {
                rd = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            } catch (Exception e) {
                //logger.info("Servicio ERROR " + e.toString());
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            estatus = sb.toString();

            SAXBuilder builder = new SAXBuilder();
            InputStream stream = new ByteArrayInputStream(estatus.getBytes("UTF-8"));
            Document document = (Document) builder.build(stream);

            Element rootNode = document.getRootElement();
            //System.out.println("Root element :" + rootNode.getName());

            Element listBody = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            //System.out.println(listBody.getContentSize());

            Element listResponse = listBody.getChild("ConsultaProveedoresResponse", Namespace.getNamespace("http://tempuri.org/"));
            //System.out.println(listResponse.getContentSize());

            Element listResult = listResponse.getChild("ConsultaProveedoresResult", Namespace.getNamespace("http://tempuri.org/"));
            //System.out.println(listResult.getContentSize());

            Element listResultProv = listResult.getChild("Proveedores", Namespace.getNamespace("http://Elektra.com"));
            //System.out.println(listResultProv.getContentSize());

            @SuppressWarnings("unchecked")
            List<Element> result = listResultProv.getChildren("Proveedor", Namespace.getNamespace("http://Elektra.com"));

            //System.out.println(result.size());
            for (int i = 0; i < result.size(); i++) {

                Element node = (Element) result.get(i);

                Element e1 = node.getChild("FormatoMenu", Namespace.getNamespace("http://Elektra.com"));
                Element e2 = node.getChild("NoProveedor", Namespace.getNamespace("http://Elektra.com"));
                Element e3 = node.getChild("NombreCorto", Namespace.getNamespace("http://Elektra.com"));
                Element e4 = node.getChild("RazonSocial", Namespace.getNamespace("http://Elektra.com"));
                Element e5 = node.getChild("Status", Namespace.getNamespace("http://Elektra.com"));

                /*   System.out.println("**************************************");
				   System.out.println(e1.getValue());
				   System.out.println(e2.getValue());
				   System.out.println(e3.getValue());
				   System.out.println(e4.getValue());
				   System.out.println(e5.getValue());
				   System.out.println("************************************** \n");*/
            }

        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
            e.printStackTrace();
            estatus = e.getMessage();
        }

        try {
            if (wr != null) {
                wr.close();
            }
        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
        }

        try {
            if (rd != null) {
                rd.close();
            }
        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
        }

        try {
            if (rc != null) {
                rc.disconnect();
            }
        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
        }

        return estatus;
    }

    /**
     * ****************************** 15 - 21
     * **************************************
     */
    /**
     * ****************************** 22 - 28
     * **************************************
     */
    /**
     * ****************************** 29 - 35
     * **************************************
     */
    public static boolean actualizaCuadrillas(String idCuadrilla, String alterno, String pwd, String correo, String jefe, int noProveedor, String telefono, String zona) {

        boolean respuesta = false;
        String estatus = "";

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        try {

            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 8080));

            UtilDate date = new UtilDate();
            @SuppressWarnings("static-access")
            String fecha = date.getSysDate("yyyy-MM-dd");

            URL url = new URL("http://10.54.21.38/Arsys/WCFMantenimiento/Service.svc" + "?WSDL");
            rc = (HttpURLConnection) url.openConnection(proxy);

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:elek=\"http://Elektra.com\">\n"
                    + "   <soapenv:Header/>\n" + "   <soapenv:Body>\n" + "      <tem:ConsultaProveedores>\n"
                    + "         <!--Optional:-->\n" + "         <tem:authentication>\n"
                    + "            <elek:Password>demoWsM</elek:Password>\n"
                    + "            <elek:UserName>demoWsM</elek:UserName>\n" + "         </tem:authentication>\n"
                    + "         <!--Optional:-->\n" + "         <tem:fecha>" + fecha + "</tem:fecha>\n"
                    + "      </tem:ConsultaProveedores>\n" + "   </soapenv:Body>\n" + "</soapenv:Envelope>\n" + "";

            //logger.info("PETICION SOA: " + xml);
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.setRequestProperty("SOAPAction", "http://tempuri.org/IService/ConsultaProveedores");
            rc.connect();

            try {
                wr = new OutputStreamWriter(rc.getOutputStream());
                wr.write(xml, 0, xml.length());
                wr.flush();
            } catch (Exception e) {
                wr.close();
                wr = null;
            }

            try {
                rd = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            } catch (Exception e) {
                //logger.info("Servicio ERROR " + e.toString());
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            estatus = sb.toString();

            SAXBuilder builder = new SAXBuilder();
            InputStream stream = new ByteArrayInputStream(estatus.getBytes("UTF-8"));
            Document document = (Document) builder.build(stream);

            Element rootNode = document.getRootElement();
            //System.out.println("Root element :" + rootNode.getName());

            Element listBody = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            //System.out.println(listBody.getContentSize());

            Element listResponse = listBody.getChild("ConsultaProveedoresResponse", Namespace.getNamespace("http://tempuri.org/"));
            //System.out.println(listResponse.getContentSize());

            Element listResult = listResponse.getChild("ConsultaProveedoresResult", Namespace.getNamespace("http://tempuri.org/"));
            //System.out.println(listResult.getContentSize());

            Element listResultProv = listResult.getChild("Proveedores", Namespace.getNamespace("http://Elektra.com"));
            //System.out.println(listResultProv.getContentSize());

            @SuppressWarnings("unchecked")
            List<Element> result = listResultProv.getChildren("Proveedor", Namespace.getNamespace("http://Elektra.com"));

            //System.out.println(result.size());
            for (int i = 0; i < result.size(); i++) {

                Element node = (Element) result.get(i);

                Element e1 = node.getChild("FormatoMenu", Namespace.getNamespace("http://Elektra.com"));
                Element e2 = node.getChild("NoProveedor", Namespace.getNamespace("http://Elektra.com"));
                Element e3 = node.getChild("NombreCorto", Namespace.getNamespace("http://Elektra.com"));
                Element e4 = node.getChild("RazonSocial", Namespace.getNamespace("http://Elektra.com"));
                Element e5 = node.getChild("Status", Namespace.getNamespace("http://Elektra.com"));

                /* System.out.println("**************************************");
				   System.out.println(e1.getValue());
				   System.out.println(e2.getValue());
				   System.out.println(e3.getValue());
				   System.out.println(e4.getValue());
				   System.out.println(e5.getValue());
				   System.out.println("************************************** \n");*/
            }

        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
            e.printStackTrace();
            respuesta = false;
        }

        try {
            if (wr != null) {
                wr.close();
            }
        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
        }

        try {
            if (rd != null) {
                rd.close();
            }
        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
        }

        try {
            if (rc != null) {
                rc.disconnect();
            }
        } catch (Exception e) {
            //logger.info("Servicio ERROR " + e.toString());
        }

        return respuesta;

    }

    /**
     * ****************************** 36 - 43
     * **************************************
     */
}
