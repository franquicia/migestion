package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.TablasPasoImpl;
import com.gruposalinas.migestion.domain.TablasPasoDTO;
import com.gruposalinas.migestion.domain.TareaActDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TablasPasoBI {

    private static Logger logger = LogManager.getLogger(TablasPasoBI.class);

    private List<TablasPasoDTO> listaUsuarios;
    private List<TablasPasoDTO> listaCecos;
    private List<TablasPasoDTO> listaSucursales;
    private List<TareaActDTO> listaTareas;

    @Autowired
    TablasPasoImpl tablasPasoDAO;

    public List<TablasPasoDTO> obtieneUsuarios(String idUsuario, String idPuesto, String idCeco, String idCecoSup) {

        try {
            listaUsuarios = tablasPasoDAO.obtieneUsuarios(idUsuario, idPuesto, idCeco, idCecoSup);
        } catch (Exception e) {
            logger.info("No fue posible obtener los usuarios de paso");

        }

        return listaUsuarios;
    }

    public List<TablasPasoDTO> obtieneSucursales(String numSucursal) {

        try {
            System.out.println(numSucursal);
            listaSucursales = tablasPasoDAO.obtieneSucursales(numSucursal);
        } catch (Exception e) {
            logger.info("No fue posible obtener las sucursales de paso");

        }

        return listaSucursales;
    }

    public List<TablasPasoDTO> obtieneCecos(String idCeco, String idCecoSup, String descripcion) {

        try {
            listaCecos = tablasPasoDAO.obtieneCecos(idCeco, idCecoSup, descripcion);
        } catch (Exception e) {
            logger.info("No fue posible obtener los cecos de paso");

        }

        return listaCecos;
    }

    public List<TareaActDTO> obtieneTareas(String idTarea, String idUsuario, String estatus, String statusVac) {

        try {
            listaTareas = tablasPasoDAO.obtieneTareas(idTarea, idUsuario, estatus, statusVac);
        } catch (Exception e) {
            logger.info("No fue posible obtener los cecos de paso");

        }

        return listaTareas;
    }

}
