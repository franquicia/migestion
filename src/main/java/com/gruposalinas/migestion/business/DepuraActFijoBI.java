package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.DepuraActFijoDAO;
import com.gruposalinas.migestion.domain.DepuraActFijoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class DepuraActFijoBI {

    private static Logger logger = LogManager.getLogger(DepuraActFijoBI.class);

    private List<DepuraActFijoDTO> listafila;

    @Autowired
    DepuraActFijoDAO depuraActFijoDAO;



    public boolean elimina(int idDepAct) {
        boolean respuesta = false;

        try {
            respuesta = depuraActFijoDAO.elimina(idDepAct);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }


    public List<DepuraActFijoDTO> obtieneInfo() {

        try {
            listafila = depuraActFijoDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Filas");

        }

        return listafila;
    }

    public boolean actualiza(DepuraActFijoDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = depuraActFijoDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

    // evi
    public int insertaEvi(DepuraActFijoDTO bean) {
        int respuesta = 0;

        try {
            respuesta = depuraActFijoDAO.insertaEvi(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public List<DepuraActFijoDTO> obtieneDatosEvi(String ceco) {

        try {
            listafila = depuraActFijoDAO.obtieneDatosEvi(ceco);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<DepuraActFijoDTO> obtieneInfoEvi() {

        try {
            listafila = depuraActFijoDAO.obtieneInfoEvi();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Evidencia act fijo");

        }

        return listafila;
    }

    public boolean actualizaEvi(DepuraActFijoDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = depuraActFijoDAO.actualizaEvi(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
