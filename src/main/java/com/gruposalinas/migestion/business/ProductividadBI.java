package com.gruposalinas.migestion.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub;
import com.gruposalinas.migestion.cliente.ExtraccionRankingBAZStub.XML;
import com.gruposalinas.migestion.dao.ProductividadDAO;
import com.gruposalinas.migestion.domain.ClasificacionCecoDTO;
import com.gruposalinas.migestion.domain.ProductividadDTO;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductividadBI {

    @Autowired
    ProductividadDAO productividadDAO;

    private Logger logger = LogManager.getLogger(ProductividadBI.class);

    public void extraccion(String fechaDescarga, int origenEjecucion) {

        //String ultDomQ;
        boolean fechaEliminada = true;
        String fechaEnviar = "";

        fechaEnviar = fechaDescarga;

        /* origenEjecucion = 1 -> El proceso se ejecuta por medio del Scheduler
         * origenEjecucion = 2 -> El proceso se ejecuta manualemente
         */
        try {
            /*Obtenemos la semana de la fecha actual*/
            SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
            java.util.Date fecha = new Date();

            String fechaP = formato.format(fecha);
            Date fechaDate = null;
            fechaDate = formato.parse(fechaP);

            Calendar calendar = Calendar.getInstance();
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            calendar.setMinimalDaysInFirstWeek(4);
            calendar.setTime(fechaDate);
            int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

            /*Valida el origen de donde se esta ejecutando el proceso*/
            if (origenEjecucion == 2) {

                /*Valida si existen datos de la fecha recibida*/
                if (productividadDAO.totalRegistros(fechaEnviar) > 0) {
                    fechaEliminada = productividadDAO.eliminaFecha(fechaDescarga);
                }
            }

            if (fechaEliminada) {
                if (numberWeekOfYear == 14 || numberWeekOfYear == 27 || numberWeekOfYear == 40 || numberWeekOfYear == 1) {

                    logger.info("La semana es:" + numberWeekOfYear);

                    /*Obtenemos el ultimo domigo del trimestre*/
                    //UltimoDom procesoDom = new UltimoDom();
                    //String UltDomQ = procesoDom.domingo();
                    System.out.println("Inicia descarga de geografia tipo 1");
                    extraeDatos(fechaEnviar, 1);

                    //System.out.println("Inicia descarga de geografia tipo 2");
                    //extraeDatos(fechaEnviar, 2);
                }
            }

        } catch (Exception e) {
            System.out.println("Occurio un problema al realizar las descargas" + e);
        }
        System.out.println("La descargas de productivida terminaron");
    }

    protected void extraeDatos(String fechaEnviar, int tipoGeo) {

        XML[] productZonas;
        XML[] productRegiones;
        XML[] productGerencias;

        /*Consumimos el servicio para extraer el ranking de productividad nacional*/
        ExtraccionRankingBAZStub productividad = null;
        ExtraccionRankingBAZStub.ExtraerDatos request = null;
        ExtraccionRankingBAZStub.ExtraerDatosResponse response = null;

        try {

            /*Consumimos el servicio para extraer el ranking de productividad nacional*/
            productividad = new ExtraccionRankingBAZStub();
            request = new ExtraccionRankingBAZStub.ExtraerDatos();
            response = new ExtraccionRankingBAZStub.ExtraerDatosResponse();

            request.setNivel("Z");
            request.setFecha(Integer.valueOf(fechaEnviar));
            request.setTipoGeografia(tipoGeo);
            request.setCentroCostos(0);
            request.setRowInicio(1);
            request.setRowFin(1);

            response = productividad.extraerDatos(request);
            productZonas = response.getExtraerDatosResult().getXML();
            logger.info("Se obtuvieron Zonas");

            /*Consumimos el servicio para extraer el ranking de productividad de todas la regiones*/
            productividad = new ExtraccionRankingBAZStub();
            request = new ExtraccionRankingBAZStub.ExtraerDatos();
            response = new ExtraccionRankingBAZStub.ExtraerDatosResponse();

            request.setNivel("R");
            request.setFecha(Integer.valueOf(fechaEnviar));
            request.setTipoGeografia(tipoGeo);
            request.setCentroCostos(0);
            request.setRowInicio(1);
            request.setRowFin(1);

            response = productividad.extraerDatos(request);
            productRegiones = response.getExtraerDatosResult().getXML();
            logger.info("Se obtuvieron Regiones");

            productGerencias = extraeGerencias(fechaEnviar, tipoGeo);

            envioDatos(productZonas, "0");
            envioDatos(productRegiones, "0");
            envioDatos(productGerencias, "0");
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Ocurrio un error al ejecutar la extraccion de datos ");

        }

    }

    protected void envioDatos(XML[] products, String cc_padre) {

        try {

            for (XML product : products) {

                ProductividadDTO datos = null;
                datos = new ProductividadDTO();
                datos.setNivel(product.getNivel());
                datos.setFecha(product.getFecha());
                datos.setTipo_geografia(product.getTipoGeografia());
                datos.setPosicion(Integer.valueOf(product.getPOSICION()));
                datos.setContribucion(product.getContribucion());
                datos.setPos_contribucion((int) product.getPOS_CONT());
                datos.setContribucion(product.getPRESTAMOS());
                datos.setPos_contribucion((int) product.getPOS_PRES());
                datos.setCaptacion(product.getCAPTACION());
                datos.setPos_captacion((int) product.getPOS_CAPT());
                datos.setSipa(product.getSIPA());
                datos.setPos_sipa((int) product.getPOS_SIPA());
                datos.setSeguros(product.getSEGUROS());
                datos.setPos_seguros((int) product.getPOS_SEGU());
                datos.setGente(product.getGENTE());
                datos.setPos_gente((int) product.getPOS_GENT());
                datos.setAfore(product.getAFORE());
                datos.setPos_afore((int) product.getPOS_AFOR());
                datos.setTotal(product.getTOTAL());
                datos.setId_cc(product.getID_CC());
                datos.setCc_padre(cc_padre);
                datos.setCc(String.valueOf(product.getCC()));
                datos.setEmpleado(product.getEMPLEADO());
                datos.setDesde(product.getDESDE());
                datos.setTreg((int) product.getT_REG());
                datos.setPeriodo("2015");

                productividadDAO.cargaProductividad(datos);
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un problema al realizar la descarga de la geografia " + e);
        }

    }

    protected XML[] extraeGerencias(String fechaEnviar, int tipoGeo) {

        XML[] productGerenciasReturn = null;

        int totalG = 0;
        int rango = 200;
        int inicio = 1;
        int fin = 0;
        ExtraccionRankingBAZStub productividad = null;
        ExtraccionRankingBAZStub.ExtraerDatos request = null;
        ExtraccionRankingBAZStub.ExtraerDatosResponse response = null;
        try {
            XML[] productGerenciasAux;

            productividad = new ExtraccionRankingBAZStub();
            request = new ExtraccionRankingBAZStub.ExtraerDatos();
            response = new ExtraccionRankingBAZStub.ExtraerDatosResponse();

            /*Se llama al service para obtener el total del sucursales*/
            request.setNivel("G");
            request.setFecha(Integer.valueOf(fechaEnviar));
            request.setTipoGeografia(tipoGeo);
            request.setCentroCostos(0);
            request.setRowInicio(1);
            request.setRowFin(1);

            response = productividad.extraerDatos(request);
            productGerenciasAux = response.getExtraerDatosResult().getXML();

            int total = (int) productGerenciasAux[0].getT_REG();
            productGerenciasReturn = new XML[total];

            /**/
            while (total > rango) {

                total = total - rango;
                //System.out.println(inicio+"-"+(fin+=total%rango)+"------->Resta:"+ (total - total%rango));
                productividad = new ExtraccionRankingBAZStub();
                request = new ExtraccionRankingBAZStub.ExtraerDatos();
                response = new ExtraccionRankingBAZStub.ExtraerDatosResponse();

                request.setNivel("G");
                request.setFecha(Integer.valueOf(fechaEnviar));
                request.setTipoGeografia(tipoGeo);
                request.setCentroCostos(0);
                request.setRowInicio(inicio);
                request.setRowFin(fin += rango);

                response = productividad.extraerDatos(request);
                productGerenciasAux = response.getExtraerDatosResult().getXML();

                logger.info("Se obtiene del rango " + inicio + "-" + fin);

                for (int i = 0; i < productGerenciasAux.length; i++) {
                    productGerenciasReturn[totalG] = productGerenciasAux[i];
                    totalG++;
                }

                inicio += rango;

                if (total < rango) {

                    //System.out.println(inicio+"-"+(fin+=total%rango)+"------->Resta:"+ (total - total%rango));
                    productividad = new ExtraccionRankingBAZStub();
                    request = new ExtraccionRankingBAZStub.ExtraerDatos();
                    response = new ExtraccionRankingBAZStub.ExtraerDatosResponse();

                    request.setNivel("G");
                    request.setFecha(Integer.valueOf(fechaEnviar));
                    request.setTipoGeografia(tipoGeo);
                    request.setCentroCostos(0);
                    request.setRowInicio(inicio);
                    request.setRowFin(fin += total % rango);

                    response = productividad.extraerDatos(request);
                    productGerenciasAux = response.getExtraerDatosResult().getXML();

                    logger.info("Se obtiene del rango " + inicio + "-" + fin);

                    for (int i = 0; i < productGerenciasAux.length; i++) {
                        productGerenciasReturn[totalG] = productGerenciasAux[i];
                        totalG++;
                    }

                    total = total - rango;

                }

            }

            if (total < rango && total > 0) {
                //System.out.println(inicio+"-"+(fin+=total%rango)+"------->Resta:"+ (total - total%rango));
                productividad = new ExtraccionRankingBAZStub();
                request = new ExtraccionRankingBAZStub.ExtraerDatos();
                response = new ExtraccionRankingBAZStub.ExtraerDatosResponse();

                request.setNivel("G");
                request.setFecha(Integer.valueOf(fechaEnviar));
                request.setTipoGeografia(tipoGeo);
                request.setCentroCostos(0);
                request.setRowInicio(inicio);
                request.setRowFin(fin += total % rango);

                response = productividad.extraerDatos(request);
                productGerenciasAux = response.getExtraerDatosResult().getXML();

                logger.info("Se obtiene del rango " + inicio + "-" + fin);

                for (int i = 0; i < productGerenciasAux.length; i++) {
                    productGerenciasReturn[totalG] = productGerenciasAux[i];
                    totalG++;
                }
                //
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

        return productGerenciasReturn;

    }

    public int getClasificacion(String ceco) {

        int clasificacion = 0;

        try {
            clasificacion = productividadDAO.getClasificacion(ceco);
        } catch (Exception e) {

            System.out.println("Ocurrio un problema al obtener la clasificacion" + e);
        }

        return clasificacion;
    }

    @SuppressWarnings("unused")
    public String gecCecosClasificacion(String ceco, int tipo) {

        //String cecoPrincipal = ceco;
        List<ClasificacionCecoDTO> clasificaciones = null;
        JsonObject principal = new JsonObject();
        try {
            clasificaciones = productividadDAO.gecCecosClasificacion(ceco, tipo);
            int cont = 0;
            //String cc_Padre = "";

            JsonObject principal_nivel2 = null;
            JsonArray hijosNivel2 = null;
            JsonArray hijosNivel1 = null;
            int n2 = 0;
            int n3 = 0;

            if (clasificaciones != null) {

                while (cont < clasificaciones.size()) {
                    //Compara si el registro es del ceco el cual usuario solicito
                    if (ceco.equals(clasificaciones.get(cont).getCeco())) {
                        principal.addProperty("ceco", clasificaciones.get(cont).getCeco());
                        principal.addProperty("clasificacion", clasificaciones.get(cont).getClasificacion());
                        principal.addProperty("nivel", clasificaciones.get(cont).getNivel());

                        hijosNivel1 = new JsonArray();
                        cont++;

                    } else {
                        String segundoNivel_cc = "";
                        int clasificacion = 0;
                        String nivel = "";
                        while (cont < clasificaciones.size()) {
                            //Valida que el ceco actual sea dependiente del ceco solicitado
                            if (clasificaciones.get(cont).getCecoPadre().equals(ceco)) {

                                if (n2 == 1) {
                                    //hijosNivel1.add(hijosNivel2);
                                    principal_nivel2 = new JsonObject();
                                    principal_nivel2.addProperty("ceco", segundoNivel_cc);
                                    principal_nivel2.addProperty("clasificacion", clasificacion);
                                    principal_nivel2.addProperty("nivel", nivel);
                                    principal_nivel2.add("ceco_hijos", hijosNivel2);
                                    hijosNivel1.add(principal_nivel2);
                                    n2 = 0;
                                }

                                hijosNivel2 = new JsonArray();

                                segundoNivel_cc = clasificaciones.get(cont).getCeco();
                                clasificacion = clasificaciones.get(cont).getClasificacion();
                                nivel = clasificaciones.get(cont).getNivel();
                                n2 = 1;
                            }

                            if (clasificaciones.get(cont).getCecoPadre().equals(segundoNivel_cc)) {

                                JsonObject nivel3 = new JsonObject();
                                nivel3.addProperty("ceco", clasificaciones.get(cont).getCeco());
                                nivel3.addProperty("clasificacion", clasificaciones.get(cont).getClasificacion());
                                nivel3.addProperty("nivel", clasificaciones.get(cont).getNivel());
                                hijosNivel2.add(nivel3);
                                n3++;
                            }
                            cont++;

                            if (cont == clasificaciones.size()) {
                                if (n2 == 1) {
                                    principal_nivel2 = new JsonObject();
                                    principal_nivel2.addProperty("ceco", segundoNivel_cc);
                                    principal_nivel2.addProperty("clasificacion", clasificacion);
                                    principal_nivel2.addProperty("nivel", nivel);
                                    principal_nivel2.add("ceco_hijos", hijosNivel2);
                                    hijosNivel1.add(principal_nivel2);
                                    n2 = 0;
                                }
                            }

                        }

                    }
                }
                principal.add("cecos_hijo", hijosNivel1);
                System.out.println(principal.toString());
            }

        } catch (Exception e) {

            System.out.println("Ocurrio un problema al obtener la clasificacion" + e);
        }

        return principal.toString();
    }

}
