package com.gruposalinas.migestion.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.dao.ParametroDAO;
import com.gruposalinas.migestion.domain.ParametroDTO;

public class ParametroBI {

    private static Logger logger = LogManager.getLogger(ParametroBI.class);

    @Autowired
    ParametroDAO parametroDAO;

    List<ParametroDTO> listaParametros = null;
    List<ParametroDTO> listaParametro = null;

    public List<ParametroDTO> obtieneParametro() {
        try {
            listaParametros = parametroDAO.obtieneParametro();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Parametros: " + e);
        }

        return listaParametros;
    }

    public List<ParametroDTO> obtieneParametros(String clave) {
        try {
            listaParametro = parametroDAO.obtieneParametro(clave);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Parametro: " + e);
        }

        return listaParametro;
    }

    public boolean insertaPrametros(ParametroDTO parametro) {
        boolean respuesta = false;
        try {
            respuesta = parametroDAO.insertaParametro(parametro);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Parametro: " + e);
        }

        return respuesta;
    }

    public boolean actualizaParametro(ParametroDTO parametro) {
        boolean respuesta = false;
        try {
            respuesta = parametroDAO.actualizaParametro(parametro);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Parametro: " + e);
        }

        return respuesta;
    }

    public boolean eliminaParametro(String clave) {
        boolean respuesta = false;
        try {
            respuesta = parametroDAO.eliminaParametros(clave);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Parametro: " + e);
        }

        return respuesta;
    }
}
