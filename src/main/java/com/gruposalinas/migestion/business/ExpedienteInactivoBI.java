package com.gruposalinas.migestion.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.dao.ExpedienteInactivoDAO;
import com.gruposalinas.migestion.domain.ExpedienteInactivoDTO;

public class ExpedienteInactivoBI {

    private static Logger logger = LogManager.getLogger(ExpedienteInactivoBI.class);

    @Autowired
    ExpedienteInactivoDAO expedienteInactivoDAO;

    List<ExpedienteInactivoDTO> listaExpedientes = null;
    List<ExpedienteInactivoDTO> listaResponsables = null;

    public List<ExpedienteInactivoDTO> buscaExpediente(String idExpediente, String idEstatus, String idCeco, String idUsuario, String fechaI, String fechaF) {

        try {
            listaExpedientes = expedienteInactivoDAO.buscaExpediente(idExpediente, idEstatus, idCeco, idUsuario, fechaI, fechaF);
        } catch (Exception e) {
            logger.info("No fue posible consultar el Expediente");
        }

        return listaExpedientes;
    }

    public List<ExpedienteInactivoDTO> buscaResponsable(String idExpediente, String idEstatus, String idUsuario) {

        try {
            listaResponsables = expedienteInactivoDAO.buscaResponsable(idExpediente, idEstatus, idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible consultar los Responsables");
        }

        return listaResponsables;
    }



    public boolean actualizaEstatus(int estatus, int idExpediente) {

        boolean respuesta = false;

        try {
            respuesta = expedienteInactivoDAO.actualizaEstatus(estatus, idExpediente);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el estatus");
        }

        return respuesta;
    }

    public boolean eliminaExpediente(int idExpediente) {

        boolean respuesta = false;

        try {
            respuesta = expedienteInactivoDAO.eliminaExpediente(idExpediente);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el Expediente");
        }

        return respuesta;
    }

    public boolean eliminaResponsables(int idExpediente) {

        boolean respuesta = false;

        try {
            respuesta = expedienteInactivoDAO.eliminaResponsables(idExpediente);
        } catch (Exception e) {
            logger.info("No fue posible eliminar a los Responsables");
        }

        return respuesta;
    }

}
