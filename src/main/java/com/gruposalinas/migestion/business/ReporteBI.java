package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ReporteDAOImpl;
import com.gruposalinas.migestion.domain.ReporteDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ReporteBI {

    private static Logger logger = LogManager.getLogger(ReporteBI.class);

    private List<ReporteDTO> listaReporte;

    @Autowired
    ReporteDAOImpl reporteDAO;

    public List<ReporteDTO> obtieneReporte(String idReporte, String nombre) {

        try {
            listaReporte = reporteDAO.obtieneReporte(idReporte, nombre);
        } catch (Exception e) {
            logger.info("No fue posible obtener el reporte");

        }

        return listaReporte;
    }

    public boolean insertaReporte(int idReporte, String nombre, int commit) {
        boolean respuesta = false;

        try {
            respuesta = reporteDAO.insertaReporte(idReporte, nombre, commit);
        } catch (Exception e) {
            logger.info("No fue posible insertar el reporte");

        }

        return respuesta;
    }

    public boolean actualizaReporte(int idReporte, String nombre, int commit) {
        boolean respuesta = false;

        try {
            respuesta = reporteDAO.actualizaReporte(idReporte, nombre, commit);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el reporte");

        }

        return respuesta;
    }

    public boolean eliminaReporte(String idReporte) {
        boolean respuesta = false;

        try {
            respuesta = reporteDAO.eliminaReporte(idReporte);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el reporte");

        }

        return respuesta;
    }
}
