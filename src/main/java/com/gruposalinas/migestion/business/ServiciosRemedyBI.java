package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.cliente.RemedyStub;
import com.gruposalinas.migestion.cliente.RemedyStub.ActualizarCuadrillas;
import com.gruposalinas.migestion.cliente.RemedyStub.ActualizarIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.Aprobacion;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfGrupoFalla;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfInfoAdjunto;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfInfoIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfProveedor;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfTracking;
import com.gruposalinas.migestion.cliente.RemedyStub.Authentication;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultaFallas;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultaProveedores;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultarAdjunto;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultarAprobaciones;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultarAprobacionesResponse;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultarBitacoraIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultarIncidentes;
import com.gruposalinas.migestion.cliente.RemedyStub.CrearCuadrilla;
import com.gruposalinas.migestion.cliente.RemedyStub.CrearIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.Cuadrilla;
import com.gruposalinas.migestion.cliente.RemedyStub.DTConsultarIncidentes;
import com.gruposalinas.migestion.cliente.RemedyStub.Filtro;
import com.gruposalinas.migestion.cliente.RemedyStub.GrupoFalla;
import com.gruposalinas.migestion.cliente.RemedyStub.Incidente;
import com.gruposalinas.migestion.cliente.RemedyStub.InfoAdjunto;
import com.gruposalinas.migestion.cliente.RemedyStub.ListaPersonal;
import com.gruposalinas.migestion.cliente.RemedyStub.Proveedor;
import com.gruposalinas.migestion.cliente.RemedyStub.Registro;
import com.gruposalinas.migestion.cliente.RemedyStub.RsCAdjuntos;
import com.gruposalinas.migestion.cliente.RemedyStub.RsCAprobacion;
import com.gruposalinas.migestion.cliente.RemedyStub.RsCFallas;
import com.gruposalinas.migestion.cliente.RemedyStub.RsCIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.RsCProveedor;
import com.gruposalinas.migestion.cliente.RemedyStub.RsRIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.RsRespond;
import com.gruposalinas.migestion.cliente.RemedyStub.RsTracking;
import com.gruposalinas.migestion.cliente.RemedyStub.RsUIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub._Accion;
import com.gruposalinas.migestion.cliente.RemedyStub._Calificacion;
import com.gruposalinas.migestion.cliente.RemedyStub._MotivoEstado;
import com.gruposalinas.migestion.cliente.RemedyStub._Origen;
import com.gruposalinas.migestion.cliente.RemedyStub._Tipo;
import com.gruposalinas.migestion.cliente.RemedyStub._TipoAtencion;
import com.gruposalinas.migestion.cliente.RemedyStub._TipoCierre;
import com.gruposalinas.migestion.domain.ExternoDTO;
import com.gruposalinas.migestion.domain.FolioRemedyDTO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.TelSucursalDTO;
import com.gruposalinas.migestion.domain.Usuario_ADTO;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import javax.activation.DataHandler;
import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiciosRemedyBI {

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    SocioUnicoBI socioUnicoBI;

    @Autowired
    TelSucursalBI telSucursalBI;

    @Autowired
    UsuarioExternoBI usuarioExternoBI;

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    Usuario_ABI usuarioBI;

    @Autowired
    Usuario_ABI usuarioabi;

    @Autowired
    LogBI logbi;

    @Autowired
    PeticionesRemedyBI peticionesRemedyBI;

    @Autowired
    ParametroBI parametroBi;

    private Logger logger = LogManager.getLogger(ServiciosRemedyBI.class);

    public void imprimefallas() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("imprimefallas()" + " MATENIMIENTO");
        }

        try {
            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            ConsultaFallas ConsultaFallasRequest = new ConsultaFallas();
            ConsultaFallasRequest.setFecha(today);
            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            ConsultaFallasRequest.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultaFallas request = new RemedyStub.ConsultaFallas();
            RemedyStub.ConsultaFallasResponse response = new RemedyStub.ConsultaFallasResponse();

            response = consulta.consultaFallas(ConsultaFallasRequest);

            RsCFallas adjuntoResult = response.getConsultaFallasResult();

            if (adjuntoResult.getExito()) {
                logger.info("Encontro las fallas");

                ArrayOfGrupoFalla x = adjuntoResult.getFallas();
                GrupoFalla[] aux = x.getGrupoFalla();

                for (GrupoFalla aux2 : aux) {
                    logger.info("" + aux2.getId() + ", " + aux2.getN1_Incidencia() + ", " + aux2.getN2_Falla() + ", "
                            + aux2.getN3_TipoFalla());
                }

            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

        }
    }

    public boolean cargafallas() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cargafallas()+ \" MATENIMIENTO\"");
        }

        boolean salida = false;
        try {
            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            ConsultaFallas ConsultaFallasRequest = new ConsultaFallas();
            ConsultaFallasRequest.setFecha(today);
            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            ConsultaFallasRequest.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultaFallas request = new RemedyStub.ConsultaFallas();
            RemedyStub.ConsultaFallasResponse response = new RemedyStub.ConsultaFallasResponse();

            response = consulta.consultaFallas(ConsultaFallasRequest);

            RsCFallas adjuntoResult = response.getConsultaFallasResult();

            if (adjuntoResult.getExito()) {
                logger.info("Encontro las fallas");

                ArrayOfGrupoFalla x = adjuntoResult.getFallas();
                if (x.getGrupoFalla() != null) {
                    salida = true;

                    GrupoFalla[] aux = x.getGrupoFalla();
                    if (x != null) {
                        logger.info("Depurando tabla fallas: " + incidenciasBI.depura());
                        for (GrupoFalla aux2 : aux) {
                            logger.info("" + aux2.getId() + ", " + aux2.getN1_Incidencia() + ", " + aux2.getN2_Falla()
                                    + ", " + aux2.getN3_TipoFalla());
                            IncidenciasDTO incid = new IncidenciasDTO();

                            incid.setIdTipo(Integer.parseInt(aux2.getId()));
                            incid.setServicio(aux2.getN1_Incidencia());
                            incid.setUbicacion(aux2.getN2_Falla());
                            incid.setIncidencia(aux2.getN3_TipoFalla());
                            incid.setPlantilla(aux2.getPlantillaAsociada());
                            incid.setStatus(aux2.getStatus());
                            int res = incidenciasBI.inserta(incid);
                        }
                    }
                }

            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return salida;
        }

        return salida;
    }

    public int levantaFolio(FolioRemedyDTO bean) {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("levantaFolio()" + " MATENIMIENTO");
        }

        int idFolio = 0;
        try {

            CrearIncidente crearIncitenteRequest = new CrearIncidente();
            Registro regRequest = new Registro();
            regRequest.setCC(bean.getCeco());
            regRequest.setComentarios(bean.getComentario());
            regRequest.setCorreo(bean.getCorreo());
            regRequest.setFalla(bean.getUbicacion());
            regRequest.setIncidencia(bean.getServicio());
            regRequest.setTipoFalla(bean.getIncidencia());
            regRequest.setNoEmpleado(bean.getNumEmpleado());
            regRequest.setNombre(bean.getNombre());
            regRequest.setNoSucursal(bean.getNoSucursal());
            regRequest.setPuesto(bean.getPuesto());
            regRequest.setSucursal(bean.getNomSucursal());
            regRequest.setTelSucursal(bean.getTelSucursal());
            regRequest.setTelefono(bean.getTelUsrAlterno());
            regRequest.setPuestoAlterno(bean.getPuestoAlterno());
            regRequest.setUsrAlterno(bean.getNomUsrAlterno());
            if (bean.getNomAdjunto() != null) {
                regRequest.setAdjunto1Nombre(bean.getNomAdjunto());
            } else {
                regRequest.setAdjunto1Nombre("");
            }

            if (bean.getAdjunto() != null) {

                Base64.Decoder decoder = Base64.getDecoder();
                byte[] aux2 = decoder.decode(bean.getAdjunto());

                ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                DataHandler data = new DataHandler(rawData);

                regRequest.setAdjunto1Base(data);

            } else {
                regRequest.setAdjunto1Base(null);
            }

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            crearIncitenteRequest.setAuthentication(autenticatiosRemedy);
            crearIncitenteRequest.setIncidente(regRequest);

            RemedyStub consulta = new RemedyStub();
            logger.info("MENSAJE: ");
            RemedyStub.CrearIncidenteResponse response = new RemedyStub.CrearIncidenteResponse();

            response = consulta.crearIncidente(crearIncitenteRequest);

            RsRIncidente crearIncidenteResult = response.getCrearIncidenteResult();

            //logger.info("MENSAJE: " + crearIncidenteResult.getMensaje());
            logbi.insertaerror(0, crearIncidenteResult.getMensaje(), "levantaFolio()");

            if (crearIncidenteResult.getExito()) {
                int folio = crearIncidenteResult.getNoIncidente();
                if (folio != 0) {
                    idFolio = folio;
                }
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return 0;
        }

        return idFolio;
    }

    public String levantaFolioXML() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("levantaFolioXML()" + " MATENIMIENTO");
        }

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();
        String strService = "leeeeviio";
        String cadena = "";
        String cadenaXml = "";

        String img = "iVBORw0KGgoAAAANSUhEUgAAAFMAAACACAYAAAB3GFWBAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NUVCRjdENzkwODE2MTFFNzhFQkNGNjY3MTMxNTAzOTMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NUVCRjdEN0EwODE2MTFFNzhFQkNGNjY3MTMxNTAzOTMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1RUJGN0Q3NzA4MTYxMUU3OEVCQ0Y2NjcxMzE1MDM5MyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1RUJGN0Q3ODA4MTYxMUU3OEVCQ0Y2NjcxMzE1MDM5MyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Po++uB0AAAnhSURBVHja7J0LUFTnFcfPvewuD4ElKC8fiEEjGqsyGKEgQYSq+KxGTGsao+Bj1CTapDaOmarRoTXOpI6NNVNfaHy0RmJEo9FgVQIiOMWq1SKoAYIgEHGXN8vu3tvzgZlh7L3ERmf3u805zpllljPD3p/fd/7n3Pt93woDQYRHLAB9MvpQdG90O7oMZN2ZJ/p93SNvzkVPRQ9Eb0Zve/i+QLy6NSP6va4wV6EvQS9DL0IXidFjm6XryJyFvhT9Ono7gfxhxqD5or+OXkognxxmEnpvdBOBfHKYwx+KDYF8CjCNXVSb7AlhUg1JMPmESUYwCSbBJCOYBJNgkhFMgkkwCSYZwSSYBJOMYBJMgkkwyQgmwSSYZCqm+3+4CBcAoREkWx2Ajf0cBIJBB4Jgd/CTV82PTFe8hlsgtbQCSGMCQ4wjPf173AG5+QFIVj0IIsF8TNPj578BUsPo3sHe2fn5Y8/cK008W1OWuHfzllENOErrwW516AzxBYGtEg5Cb9FYfhK+BqnVHa+hsKRkQv+wwV74tqDT68URUZG+Xk0WOHwxtwKvz9UxEwSaNTky2TLmVpCkdnzJPHDopz6B/v8FbObiBf098CLbMI6mebejUhbLAcwrpyWHjZszu49STFtLC5vjkiMvUHMwmVqXoMAMdTc+szHj4Ci1uL1pm24jTJsBRJFgqpgFpy2WPJY9nx6OFPU6xc9/8djJ6rTD+4uCO3dB0MhUK4PKABpeHhIe/ELSz/yVYlrrG6wzp0/NwxFs8HDgqNQUzC7q7frhl5+PVotbNmXGpWqQmsNA7GGjol05TzaBZMfCvPGznR9H+fXt7a4Ud2TLR6XpuWdvh4LgY0Hxod5cwSQcYHcR5LrXUkdOSH21n1LMzbwCU/KKpQVYbHqyVpJudKh0ObdANk8aPDxo7Z6d4Yp5srHJFh8TfZ4NxWAQ3exOWg0t8j69yzBPovAY9p75IlotbvmUmQWYJ5uGgOjljOnNPUyhowySJcyTzZ9t3xPVSyVPZm7dXrbjq6zbA0AwWp0IkmuYuo4ySK5fmjjpuaSFrwUrxVSXlrfMemNxAbaNngYH3yHSDExWBhXjtO0vGrw2Z2aolkHzEybmYfljG+DEPMk1TDa9G0Cy4Xxt//TEyRiDh7uLUtz2d9+7ear05t3BOL0tTp7e3MJ0AVmoAqjfuGDZiIiJCX6KLWVLq33V79fdwCTqJYHAzT4mrmBiGSTcBLkxOqBfwDs7tg5Xi3v758mXTABt/UF05WlHmMjT9H6A05v9fODMadUy6Isde7/5c9aJkhAQvOyc7a7jBiZT7xoA028mzRgYMmyIt1KM6W5V24xF8/JxevdwRfXmbZ8iFzDZ3aAikBqi+oQEvXfoY8Uux9pmkZJGR59HsbGHgOhu53DPp9Nhsi6nHKQ2/CBCZva5Fz08PXVqebLgXnk1djneVk7UmzuYNpDlJoCmw5u3RfqHhngoxZzava/iw9PHizFP+to4Bel0mGx63wHZnBITHzpzxZIBSjEPqqrbZqbOzXfDPOnGYZ7kAqauowySmgNA6LH1xNFItbiUxKSL2J9bQjFP2jjfG+8UmKwMagTJzp7lHDl6PNrd6K1Xiktfs6E4s+jKN4OcdLNXEzBZl1OJXc66V+YPi54+OVAp5s7lq/UpG9YU9gTwFjVy4JfDYX7X5YR7+fVau393uFrcy/HjL7BB7A+i3q6Roy8cDtOEXQ4rgw6d/VK1y9nw6oIrhQ2198Owy7Fq6AwRh8OsBmjZsuK3Pxk0aqSP0u9vXrlqXrN/V5EfTm87RzcxuINpBUlGFdclLZjXTy1mfeqSayw0AFz0WjvWxqEwBfyH5Y3dVFVtUYuZs/z1EKZRJrDbBILZXW0psGcL4roVv76mFjNl7pzg1S/NCUO1NzPVJ5jd2HNYpJ/499XKtXNTLqvFpGUciHgxODQQVb/eVUOrThz+QZmoBINgXL8v/V/Ht+0sU4vLzMuJM4LgWoRdkl4jJ8s6HCYTFQ8QXDwBPKctW5h3q6DQrBTn0yfI7dypM3HsWVAtSO0uGgDqlCnEeuz+ILqxv58QE33eYm5UXHsePmGc3760TZF12H2yZ+gCwVQ21ms/D6JXhb29aXbs2By1uF+tXjloxfTkIWUgm3gXJKcmdwaU3cQ4dv1yxYaUxf9Ui9t89JPRscHPci9IHHwwQe6LKXJN+vZrn3+0q1wt6lheLveC5HSYTJA8QewQpKlLF1y4feny9wmSpRwkC49AuZgyXQRJmBafkCNbbbKaIB3duj22BaD1PkhW3hSem/zD8id7WFbUYjbPix+fqxY3fdnCkN3r//DCtwDNdpC5at+52qGGZORnANzOV5RW+Ta26SMnJCpuAgiPG9OrIP1g7aX6urogEDw4uN/J3w41+WH/HghgXP7H9wuz/5ZRpRb7l9PHIw0ABrYJlZf8yV2ZwUaZD4g6/K/2SPhlck5lUXGTUlxw2GCvM58ciW0HaGPLanjIn1zWbEyQBnau2pAmRsact7e3Kz5Mi02eEbR15eqIGoB69vxdIJjdC9L1xjpTakLSBbW4ZZvShs6PGx96BzsknZOvh+vbW2z1BtvTszf37Nd/envVDbW4XVknYob7+Pdk65Wc2SFxDfNxBUnQ64RT+blxmDfF2yhIOiflT+5vvD4qSFVFJYqCFDR4kOff/5oRi+mh1eQkQdLEXewugmRPio7NVuuQ4n7xUu8tb73DBMnUBrLkaKCaeSTwUJCM18y1plkRUdlqcW9+sPH536UsGlEOcoPk4FpeU2d0sA7JFzukC7VV9/rYBLeIcWN7KcXFT58aVHzqnDmnsryuJwiGH2UH9DiCJKAg+aEgLUpbV/iPk1m1arHvfvD+MPbaDpJM07yb0dkLRD3mQ3385PFfPaiobFWK8+0d6OqNMY5chqjJA0/Y+iO2OR9l3TI5aoxi/szLPFlTj62mmwO3AWr2kCgmSGEgGPOryu6/kjA+p8Fsbv/udxeysmreeOvNqz4A7o5cjqjps+DYM/hBAD4Hz2bdLQkIaYyIjfFtbW62ZeRnV7O5/SyWU45cJCtg/bYNX9k6yW+1CJSNO1ZP3gXJ0tT5vZlCHxyRXiC6ODBfsn1LNZo/pVB+WNQHgshKIEPXQp86IA0bwSSYBJNgkhFMgkkwyQgmwSSYBJOMYBJMgklGMAkmwSSYZASTYBJMMoJJMAnmjx2mQBieHkzmMqF4OjAboXMZMdlTgFmMzr6zUSIcTw7zJLAj0ju/vI2m+xPCrERPB3YYVqcR0B9obOsKey1ED0GPQ2dnZFhJ5f8n69i60nWx63J0thVkNnQuGm2AjjWjZI9h7CxQK1uG/egvItAnoveFzjXvAk397zWmN/f/I8AAJuVS68ZUMBIAAAAASUVORK5CYII=";

        try {
            URL url = new URL(GTNConstantes.getURLRemedy() + "?wsdl");
            rc = (HttpURLConnection) url.openConnection();
            rc.setRequestMethod("GET");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:elek=\"http://Elektra.com\">"
                    + " <soapenv:Header/>" + "<soapenv:Body>" + "<tem:CrearIncidente>\n" + "        <!--Optional:-->\n"
                    + "         <tem:authentication>\n" + "            <elek:Password>demoWsM</elek:Password>\n"
                    + "            <elek:UserName>demoWsM</elek:UserName>\n" + "         </tem:authentication>"
                    + "<!--Optional:-->\n" + "         <tem:incidente>\n" + "            <!--Optional:-->\n"
                    + "            <elek:Adjunto1Base></elek:Adjunto1Base>\n" + "            <!--Optional:-->\n"
                    + "            <elek:Adjunto1Nombre></elek:Adjunto1Nombre>\n"
                    + "            <elek:CC>480100</elek:CC>\n"
                    + "            <elek:Comentarios>Folio de prueba con java</elek:Comentarios>\n"
                    + "            <elek:Correo>G0100@elektra.com.mx</elek:Correo>\n"
                    + "            <elek:Falla>BUNKER</elek:Falla>\n"
                    + "            <elek:Incidencia>CLAVES DE SEGURIDAD</elek:Incidencia>\n"
                    + "            <elek:NoEmpleado>331952</elek:NoEmpleado>\n"
                    + "            <elek:NoSucursal>100</elek:NoSucursal>\n"
                    + "            <elek:Nombre>Carlos Aguilar Leyva</elek:Nombre>\n"
                    + "            <elek:Puesto>Gerente</elek:Puesto>\n"
                    + "            <elek:PuestoAlterno>Subgerente</elek:PuestoAlterno>\n"
                    + "            <elek:Sucursal>MEGA LA LUNA</elek:Sucursal>\n"
                    + "            <elek:TelSucursal>5512345678</elek:TelSucursal>\n"
                    + "            <elek:Telefono>5587654321</elek:Telefono>\n"
                    + "            <elek:TipoFalla>APERTURAS POR VIOLACION</elek:TipoFalla>\n"
                    + "            <elek:UsrAlterno>Javier</elek:UsrAlterno>\n" + "         </tem:incidente>\n"
                    + "      </tem:CrearIncidente>" + "</soapenv:Body>" + "</soapenv:Envelope>";

            logger.info("PETICION SOA: " + xml);

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info(" Ocurrio algo en obtieneXmlResult: " + e.getMessage());
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }
        return cadenaXml;
    }

    public ArrayOfInfoIncidente ConsultaFoliosSucursal(int numSucursal, String opcEst) {
        ArrayOfInfoIncidente arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaFoliosSucursal()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            logger.info("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setNoSucursal(numSucursal);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente" + e);

            return arrIncidentes;
        }

        return arrIncidentes;

    }

    //Nuevo metodo en donde se buscara por ticket y/o sucursal
    public ArrayOfInfoIncidente consultaPaginadoSucursal(int numSucursal, int ticket, int opcEst, int pagina) {

        ArrayOfInfoIncidente arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaPaginadoSucursal()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            logger.info("Today is " + today.getTime());

            DTConsultarIncidentes consultarIncitenteRequest = new DTConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setNoSucursal(numSucursal);

            if (ticket != 0) {
                nFiltro.setIdIncidente(ticket);
            }
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);
            consultarIncitenteRequest.setPagina(pagina);
            consultarIncitenteRequest.setTamanio(100);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.DTConsultarIncidentesResponse response = new RemedyStub.DTConsultarIncidentesResponse();

            response = consulta.dTConsultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getDTConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente" + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public boolean cargaTelefonos() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cargaTelefonos()" + " MATENIMIENTO");
        }

        boolean bandera = false;
        try {

            String json = socioUnicoBI.obtieneTelefonosSucursales();

            logger.info("Salida telefonos: \n" + json);
            if (json != null) {
                JSONArray jsonArr = new JSONArray(json);
                if (jsonArr != null) {
                    logger.info("Depurando tabla Telefonos" + telSucursalBI.depuraTelefonos());
                    for (int i = 0; i < jsonArr.length(); ++i) {
                        JSONObject rec = jsonArr.getJSONObject(i);
                        // logger.info("Salida telefonos: \n"+rec.getInt("idTelSucursal")+",
                        // "+rec.getString("numEco")+", "+rec.getString("telefono")+",
                        // "+rec.getString("proveedor"));
                        TelSucursalDTO telSucBEAN = new TelSucursalDTO();
                        telSucBEAN.setIdTelefono(rec.getInt("idTelSucursal"));
                        telSucBEAN.setIdCeco(rec.getString("numEco"));
                        telSucBEAN.setTelefono(rec.getString("telefono"));
                        telSucBEAN.setProveedor(rec.getString("proveedor"));
                        telSucBEAN.setEstatus(1 + "");

                        logger.info("Inserta Telefono" + telSucursalBI.insertatelefono(telSucBEAN));
                    }
                    bandera = true;
                }

            }

        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return bandera;
        }
        return bandera;
    }

    public boolean cargaUsuariosExternos() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cargaUsuariosExternos()" + " MATENIMIENTO");
        }

        boolean bandera = false;
        try {

            String json = socioUnicoBI.obtieneEmpleadosExternos();

            logger.info("Salida telefonos: \n" + json);
            if (json != null) {
                JSONArray jsonArr = new JSONArray(json);
                if (jsonArr != null) {
                    logger.info("Depurando tabla Telefonos" + usuarioExternoBI.depuraUsuarioExterno());
                    for (int i = 0; i < jsonArr.length(); ++i) {
                        JSONObject rec = jsonArr.getJSONObject(i);
                        logger.info("Salida Empleados externos: \n" + rec);

                        // logger.info("Salida telefonos: \n"+rec.getInt("idTelSucursal")+",
                        // "+rec.getString("numEco")+", "+rec.getString("telefono")+",
                        // "+rec.getString("proveedor"));
                        ExternoDTO usuario = new ExternoDTO();
                        usuario.setUsuarioID(rec.getInt("idEmpleado"));
                        ExternoDTO externo = new ExternoDTO();

                        externo.setNumEmpleado(rec.getInt("idEmpleado"));
                        if (rec.has("nombre") && !rec.isNull("nombre")) {
                            externo.setNombre(rec.getString("nombre"));
                        }

                        if (rec.has("apPaterno") && !rec.isNull("apPaterno")) {
                            externo.setaPaterno(rec.getString("apPaterno"));
                        }
                        if (rec.has("apMaterno") && !rec.isNull("apMaterno")) {
                            externo.setaMaterno(rec.getString("apMaterno"));
                        }
                        if (rec.has("direccion") && !rec.isNull("direccion")) {
                            externo.setDireccion(rec.getString("direccion"));
                        }

                        if (rec.has("numCasa") && !rec.isNull("numCasa")) {
                            String numCel = rec.getString("numCasa");
                            if (numCel.length() <= 9 && !numCel.contains(" ")) {
                                externo.setNumCasa(Integer.parseInt(rec.getString("numCasa")));
                            }
                        } else {
                            externo.setNumCasa(0);
                        }

                        if (rec.has("numCelular") && !rec.isNull("numCelular")) {
                            String numCel = rec.getString("numCelular");
                            if (numCel.length() <= 9 && !numCel.contains(" ")) {
                                externo.setNumCelular(Integer.parseInt(rec.getString("numCelular")));
                            }
                        } else {
                            externo.setNumCelular(0);
                        }

                        if (rec.has("rfcEmpleado") && !rec.isNull("rfcEmpleado")) {
                            externo.setRFCEmpleado(rec.getString("rfcEmpleado"));
                        }
                        if (rec.has("cencosNum") && !rec.isNull("cencosNum")) {

                            if (rec.getString("cencosNum").length() == 4) {
                                externo.setCencosNum("48" + rec.getString("cencosNum"));
                            } else if (rec.getString("cencosNum").length() == 3) {
                                externo.setCencosNum("480" + rec.getString("cencosNum"));
                            } else if (rec.getString("cencosNum").length() == 2) {
                                externo.setCencosNum("4800" + rec.getString("cencosNum"));
                            } else if (rec.getString("cencosNum").length() == 1) {
                                externo.setCencosNum("48000" + rec.getString("cencosNum"));
                            } else {
                                externo.setCencosNum(rec.getString("cencosNum"));
                            }

                        }
                        if (rec.has("puesto") && !rec.isNull("puesto")) {
                            externo.setPuesto(Integer.parseInt(rec.getString("puesto")));
                        } else {
                            externo.setPuesto(0);
                        }
                        String fechaI = "";
                        if (rec.has("fechaIngreso") && !rec.isNull("fechaIngreso")) {
                            fechaI = rec.getString("fechaIngreso");
                        }

                        externo.setFechaIngreso(
                                fechaI.substring(0, 3) + fechaI.substring(5, 6) + fechaI.substring(8, 9));
                        String fechaB = "";
                        if (rec.has("fechaBaja") && !rec.isNull("fechaBaja")) {
                            fechaB = rec.getString("fechaBaja");
                        }

                        externo.setFechaBaja(fechaB.substring(0, 3) + fechaB.substring(5, 6) + fechaB.substring(8, 9));

                        if (rec.has("funcion") && !rec.isNull("funcion")) {
                            externo.setFuncion(rec.getString("funcion"));
                        }

                        if (rec.has("idUsuario") && !rec.isNull("idUsuario")) {
                            externo.setUsuarioID(rec.getInt("idUsuario"));
                        }

                        if (rec.has("empleadoSituacion") && !rec.isNull("empleadoSituacion")) {
                            externo.setEmpSituacion(rec.getInt("empleadoSituacion"));
                        }
                        if (rec.has("honorarios") && !rec.isNull("honorarios")) {
                            externo.setHonorarios(rec.getInt("honorarios"));
                        }

                        if (rec.has("email") && !rec.isNull("email")) {
                            externo.setEmail(rec.getString("email"));
                        }

                        if (rec.has("numGerencia") && !rec.isNull("numGerencia")) {
                            String numCel = rec.getString("numGerencia");
                            if (numCel.length() <= 9 && !numCel.contains(" ")) {
                                externo.setNumEmergencia(Integer.parseInt(rec.getString("numGerencia")));
                            }
                        }

                        if (rec.has("personaEmergencia") && !rec.isNull("personaEmergencia")) {
                            externo.setPerEmergencia(rec.getString("personaEmergencia"));
                        }

                        if (rec.has("numExtension") && !rec.isNull("numExtension")) {
                            externo.setNumExt(Integer.parseInt(rec.getString("numExtension")));
                        }

                        if (rec.has("sueldo") && !rec.isNull("sueldo")) {
                            externo.setSueldo(rec.getString("sueldo"));
                        }

                        if (rec.has("horarioIni") && !rec.isNull("horarioIni")) {
                            externo.setHorarioIn(rec.getString("horarioIni"));
                        }

                        if (rec.has("horarioFin") && !rec.isNull("horarioFin")) {
                            externo.setHorarioFin(rec.getString("horarioFin"));
                        }

                        if (rec.has("semestre") && !rec.isNull("semestre")) {
                            externo.setSemestre(rec.getInt("semestre"));
                        }

                        if (rec.has("actividades") && !rec.isNull("actividades")) {
                            externo.setActividades(rec.getString("actividades"));
                        }
                        // externo.setLiberacion(rec.getString("fechaBaja"));
                        // externo.setElaboracion(rec.getString("fechaBaja"));
                        if (rec.has("llave") && !rec.isNull("llave")) {
                            externo.setLlave(rec.getInt("llave"));
                        }

                        if (rec.has("red") && !rec.isNull("red")) {
                            externo.setRed(rec.getInt("red"));
                        }

                        if (rec.has("lotus") && !rec.isNull("lotus")) {
                            externo.setLotus(rec.getInt("lotus"));
                        }

                        if (rec.has("credencial") && !rec.isNull("credencial")) {
                            externo.setCredencial(rec.getInt("credencial"));
                        }

                        if (rec.has("folioDatasec") && !rec.isNull("folioDatasec")) {
                            externo.setFolioDataSec(rec.getInt("folioDatasec"));
                        }

                        if (rec.has("pais") && !rec.isNull("pais")) {
                            externo.setPais(rec.getString("pais"));
                        }

                        if (rec.has("nomCompleto") && !rec.isNull("nomCompleto")) {
                            externo.setNombreCompleto(rec.getString("nomCompleto"));
                        }

                        if (rec.has("asistencia") && !rec.isNull("asistencia")) {
                            externo.setAsistencia(rec.getString("asistencia"));
                        }

                        if (rec.has("curp") && !rec.isNull("curp")) {
                            externo.setCurp(rec.getString("curp"));
                        }

                        logger.info("Inserta Usuario: " + usuarioExternoBI.insertaUsuario(externo));

                        /*
                         * TelSucursalDTO telSucBEAN = new TelSucursalDTO();
                         * telSucBEAN.setIdTelefono(rec.getInt("idTelSucursal"));
                         * telSucBEAN.setIdCeco(rec.getString("numEco"));
                         * telSucBEAN.setTelefono(rec.getString("telefono"));
                         * telSucBEAN.setProveedor(rec.getString("proveedor"));
                         * telSucBEAN.setEstatus(1+"");
                         *
                         * logger.info("Inserta Telefono"+telSucursalBI.insertatelefono(telSucBEAN));
                         */
                    }
                    bandera = true;
                }

            }

        } catch (Exception e) {
            logger.info("Ocurrio Algo: " + e);
            return bandera;
        }
        return bandera;
    }

    public boolean ConsultaProveedor() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaProveedor()" + " MATENIMIENTO");
        }

        boolean salida = false;
        try {
            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            //System.out.println("Today is " + today.getTime());

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            ConsultaProveedores ConsultaProveedoresRequest = new ConsultaProveedores();
            ConsultaProveedoresRequest.setFecha(today);
            ConsultaProveedoresRequest.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultaProveedores request = new RemedyStub.ConsultaProveedores();
            RemedyStub.ConsultaProveedoresResponse response = new RemedyStub.ConsultaProveedoresResponse();

            response = consulta.consultaProveedores(ConsultaProveedoresRequest);

            RsCProveedor adjuntoResult = response.getConsultaProveedoresResult();

            if (adjuntoResult.getExito()) {
                logger.info("Encontro las fallas");

                ArrayOfProveedor x = adjuntoResult.getProveedores();
                if (x.getProveedor() != null) {
                    salida = true;

                    Proveedor[] aux = x.getProveedor();
                    if (aux != null) {
                        logger.info("Depura tabla proovedores: " + proveedoresBI.depura());
                        for (Proveedor aux2 : aux) {
                            logger.info("" + aux2.getFormatoMenu() + ", " + aux2.getNoProveedor() + ", "
                                    + aux2.getNombreCorto() + aux2.getRazonSocial() + ", " + aux2.getStatus());
                            ProveedoresDTO proveedores = new ProveedoresDTO();

                            proveedores.setMenu(aux2.getFormatoMenu());
                            proveedores.setIdProveedor(aux2.getNoProveedor());
                            proveedores.setNombreCorto(aux2.getNombreCorto());
                            proveedores.setRazonSocial(aux2.getRazonSocial());
                            proveedores.setStatus(aux2.getStatus());

                            int res = proveedoresBI.inserta(proveedores);

                        }
                    }
                }

            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente" + e);

            return salida;
        }

        return salida;
    }

    public String ConsultaAdjunto(int idFolio) {
        String res = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaAdjunto()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            //System.out.println("Today is " + today.getTime());

            ConsultarAdjunto consultarAdjuntoRequest = new ConsultarAdjunto();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            consultarAdjuntoRequest.setAuthentication(autenticatiosRemedy);
            consultarAdjuntoRequest.setNoIncidente(idFolio);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarAdjuntoResponse response = new RemedyStub.ConsultarAdjuntoResponse();

            response = consulta.consultarAdjunto(consultarAdjuntoRequest);

            RsCAdjuntos adjuntoR = response.getConsultarAdjuntoResult();
            //logger.info("Mensaje adjunto: " + adjuntoR.getMensaje());
            logbi.insertaerror(0, adjuntoR.getMensaje(), "ConsultaAdjunto()");
            if (adjuntoR.getExito()) {
                ArrayOfInfoAdjunto arrAdjunto = adjuntoR.getAdjuntos();
                if (arrAdjunto != null) {
                    InfoAdjunto[] aux = arrAdjunto.getInfoAdjunto();
                    if (aux != null) {
                        for (InfoAdjunto x : aux) {
                            DataHandler dataAUX = x.getA1_Base();

                            InputStream in = dataAUX.getInputStream();
                            byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                            Base64.Encoder codec = Base64.getEncoder();
                            // String encoded = Base64.encodeBase64String(byteArray);
                            String encoded = new String(codec.encode(byteArray), "UTF-8");
                            res = encoded;
                        }
                    } else {
                        res = null;
                    }
                }
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente" + e);

            return res;
        }

        return res;
    }

    public ArrayOfTracking ConsultaBitacora(int idFolio) {
        ArrayOfTracking res = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaBitacora()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            //System.out.println("Today is " + today.getTime());

            ConsultarBitacoraIncidente consultarBitacoraRequest = new ConsultarBitacoraIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            consultarBitacoraRequest.setAuthentication(autenticatiosRemedy);
            consultarBitacoraRequest.setNoIncidente(idFolio);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarBitacoraIncidenteResponse response = new RemedyStub.ConsultarBitacoraIncidenteResponse();

            response = consulta.consultarBitacoraIncidente(consultarBitacoraRequest);

            RsTracking traking = response.getConsultarBitacoraIncidenteResult();

            //logger.info("Mensaje adjunto: " + traking.getMensaje());
            logbi.insertaerror(0, traking.getMensaje(), "ConsultaBitacora()");
            if (traking.getExito()) {
                ArrayOfTracking arrTraking = traking.getBitacora();
                res = arrTraking;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return null;
        }

        return res;
    }

    public ArrayOfInfoIncidente ConsultaFoliosSupervidor(int numSupervisor, String opcEst) {
        ArrayOfInfoIncidente arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaFoliosSupervidor()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setSupervisorAsignado(numSupervisor);
            nFiltro.setCoordinadorAsignado(0);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al consultar los folios del supervisor " + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public boolean AutorizaCoordinador(int idIncidente, String justificacion, String monto, String AprSolicitante,
            String AprAprobador) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("AutorizaCoordinador()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.SolicitarAutorizacion);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            // incide.setAdjunto1Base("");
            incide.setAdjunto1Nombre("");
            // incide.setAdjunto2Base("");
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setMontoEstimado(new BigDecimal(monto));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            incide.setAprAprobador(AprAprobador);
            incide.setAprSolicitante(AprSolicitante);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AutorizaCoordinador()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean cierreSinOT(int idIncidente, String resolucion, String montivoEstado) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreSinOT()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.CierreSinOT);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            // incide.setAdjunto1Base("");
            incide.setAdjunto1Nombre("");
            // incide.setAdjunto2Base("");
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion("");
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            if (montivoEstado.contains("duplicado")) {
                incide.setMotivoEstado(motivo.Duplicado);
            }
            if (montivoEstado.contains("NA")) {
                incide.setMotivoEstado(motivo.NoAplica);
            }
            if (montivoEstado.contains("correctivo")) {
                incide.setMotivoEstado(motivo.CorrectivoMenor);
            }
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion(resolucion);
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "CierreSinOT()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean asignarProveedor(int idIncidente, String proveedor, String fecha, String monto, String origen, String AprSolicitante, String AprAprobador, String mensaje, String tipoAten) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("asignarProveedor()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AsignarProveedor);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            // incide.setAdjunto1Base("");
            incide.setAdjunto1Nombre("");
            // incide.setAdjunto2Base("");
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada(fecha);
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(mensaje);
            incide.setMontoEstimado(new BigDecimal(monto));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor(proveedor);
            incide.setResolucion("");

            _Origen TA = null;

            if (origen.contains("Correctivo")) {
                incide.setOrigen(TA.Correctivo);
            }
            if (origen.contains("Preventivo")) {
                incide.setOrigen(TA.Preventivo);
            }
            if (origen.contains("Proyectos")) {
                incide.setOrigen(TA.ProyectosEspeciales);
            }
            if (origen.contains("Mantenimiento")) {
                incide.setOrigen(TA.MantenimientoMayor);
            }
            if (origen.contains("Servicio")) {
                incide.setOrigen(TA.Serviciocomplementario);
            }
            if (origen.contains("Emergencia")) {
                incide.setOrigen(TA.EmergenciaContingencia);
            }
            if (origen.contains("Garantias")) {
                incide.setOrigen(TA.Garantias);
            }
            if (origen.contains("Remodelaciones")) {
                incide.setOrigen(TA.Remodelaciones);
            }
            if (origen.contains("Construcciones")) {
                incide.setOrigen(TA.Construcciones);
            }

            _TipoAtencion Aten = null;
            if (tipoAten.contains("0")) {
                incide.setAtencion(Aten.Directo);
            } else {
                incide.setAtencion(Aten.Programable);
            }

            incide.setAprAprobador(AprAprobador);
            incide.setAprSolicitante(AprSolicitante);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AsignarProveedor()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public HashMap<String, Object> consultaFoliosPaginado(String[] params) {
        ArrayOfInfoIncidente arrIncidentes = null;

        HashMap<String, Object> mapResult = new HashMap<>();

        int incidentesExistentes = 0;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaFoliosPaginado()" + " MATENIMIENTO");
        }

        try {

            int numSucursal = 0;

            //Si taremos como filtro la sucursal scaremos el numero económico
            if (params[0] != null && !params[0].equals("")) {

                String cecoS = params[0] + "";
                if (cecoS.length() == 6) {
                    numSucursal = Integer.parseInt(cecoS.substring(2, cecoS.length()));
                } else if (cecoS.length() == 5) {
                    numSucursal = Integer.parseInt(cecoS.substring(1, cecoS.length()));
                } else {
                    numSucursal = Integer.parseInt(cecoS);
                }

            }

            DTConsultarIncidentes consultarIncitenteRequest = new DTConsultarIncidentes();

            Filtro nFiltro = new Filtro();

            nFiltro.setNoSucursal(numSucursal);
            nFiltro.setIdIncidente(params[1] != null && !params[1].equals("") ? Integer.parseInt(params[1]) : 0);
            //nFiltro.setIdIncidente(13000720);

            //Buscamos el proveedor  por medio del id recibido, para obtener la razon social
            ProveedoresDTO proveedor = null;
            if (params[2] != null && !params[2].equals("")) {
                List<ProveedoresDTO> proveedoresArray = proveedoresBI.obtieneDatos(Integer.parseInt(params[2]));

                if (proveedoresArray != null && proveedoresArray.size() > 0) {
                    proveedor = proveedoresArray.get(0);
                }

            }

            nFiltro.setProveedor(proveedor != null ? proveedor.getRazonSocial() : "");
            nFiltro.setSupervisorAsignado(params[3] != null && !params[3].equals("") ? Integer.parseInt(params[3]) : 0);
            nFiltro.setCoordinadorAsignado(params[4] != null && !params[4].equals("") ? Integer.parseInt(params[4]) : 0);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(0);
            nFiltro.setIdCuadrilla(params[6] != null && !params[6].equals("") ? params[6] : "");
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);
            consultarIncitenteRequest.setTamanio(100); // Parametrizar el tamanio
            consultarIncitenteRequest.setPagina(params[5] != null && !params[5].equals("") ? Integer.parseInt(params[5]) : 1);
            consultarIncitenteRequest.setOrden("FechaCreacion_DESC"); // Parametrizar el ordenamiento

            RemedyStub consulta = new RemedyStub();
            RemedyStub.DTConsultarIncidentesResponse response = new RemedyStub.DTConsultarIncidentesResponse();

            response = consulta.dTConsultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getDTConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
                incidentesExistentes = incidentes.getTotal();

            }

            /**
             * Se consultara nuevamente con ceco en caso de que la primera vez
             * no se encontrara nada con numero economico
             */
            if (arrIncidentes != null && incidentesExistentes == 0
                    && (params[0] != null && !params[0].equals(""))) {

                nFiltro.setNoSucursal(Integer.parseInt(params[0]));

                response = consulta.dTConsultarIncidentes(consultarIncitenteRequest);

                RsCIncidente incidentesCeco = response.getDTConsultarIncidentesResult();

                if (incidentesCeco.getExito()) {
                    arrIncidentes = incidentesCeco.getIncidentes();
                    incidentesExistentes = incidentesCeco.getTotal();
                }

            }

            mapResult.put("numTotalIncidentes", incidentesExistentes);
            mapResult.put("arrayIncidente", arrIncidentes);

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            mapResult.put("arrayIncidente", arrIncidentes);
            mapResult.put("numTotalIncidentes", 0);

            return mapResult;
        }

        return mapResult;
    }

    public ArrayOfInfoIncidente ConsultaFoliosCoordinador(int numCorrdinador, String opcEst) {
        ArrayOfInfoIncidente arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaFoliosCoordinador()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setSupervisorAsignado(0);
            nFiltro.setCoordinadorAsignado(numCorrdinador);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public boolean autorizaProveedor(int idIncidente, String justificacion, boolean autoriza, String idCuadrilla) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("autorizaProveedor()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AutorizaProveedor);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            // incide.setAdjunto1Base("");
            incide.setAdjunto1Nombre("");
            // incide.setAdjunto2Base("");
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(autoriza);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            if (autoriza == false) {
                incide.setJustificacion(justificacion);
            } else {
                incide.setJustificacion("");
            }
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            if (autoriza == true) {
                incide.setNombreTecnico(justificacion);
            } else {
                incide.setNombreTecnico("");
            }
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            incide.setIdCuadrilla(idCuadrilla);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AutorizaProveedor()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean autorizaCambioMonto(int idIncidente, boolean autoriza, String justificacion, String monto, String nomArchivo, String adjuntoBase) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("autorizaCambioMonto()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AutorizaCambioMonto);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(autoriza);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);

            incide.setMontoEstimado(new BigDecimal(monto));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            incide.setAprAprobador("");
            incide.setAprSolicitante("");

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AutorizaCambioMonto()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean autorizaCoordinador(int idIncidente, boolean autoriza, String justificacion, String nomArchivo, String adjuntoBase, String monto) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("autorizaCoordinador()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AutorizaCoordinador);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            // incide.setAdjunto1Base(adjuntoBase);
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(autoriza);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);

            incide.setMontoEstimado(new BigDecimal(monto));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);
            incide.setAprAprobador("");
            incide.setAprSolicitante("");

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AutorizaCoordinador()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean posibleGarantia(int idIncidente, String justificacion) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("posibleGarantia()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.PosibleGarantia);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();

            incide.setAdjunto1Nombre("");
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(true);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            actIncitenteRequest.setIncidente(incide);
            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("Mensaje: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "PosibleGarantia()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }
        } catch (Exception e) {

            logger.info("Ocurrio algo al mandar a posible garantia " + e);

            return false;

        }
        return result;
    }

    public boolean solicitaAplazamiento(int idIncidente, String solicitante, String aprobador, String justificacion, String nomArchivo, String adjuntoBase, int dias) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("solicitaAplazamiento()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.SolicitarAplazamiento);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            incide.setAprSolicitante(solicitante);
            incide.setAprAprobador(aprobador);
            incide.setDiasAplazamiento(dias);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "SolicitaAplazamiento()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean autorizaAplazamiento(int idIncidente, boolean autoriza, String justificacion, String nomArchivo, String adjuntoBase, int dias) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("autorizaAplazamiento()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AutorizarAplazamiento);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(autoriza);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);

            incide.setJustificacion(justificacion);

            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);

            incide.setNombreTecnico(justificacion);

            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);
            incide.setDiasAplazamiento(dias);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AutorizaAplazamiento()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public Aprobacion consultarAprobaciones(int idIncidente, int opcion) {
        Aprobacion result = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultarAprobaciones()" + " MATENIMIENTO");
        }

        try {

            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(autenticatiosRemedy);
            _Tipo tipo = null;

            if (opcion == 1) {
                aprobacionesRequest.setTipo(tipo.Aplazamiento);
            } else if (opcion == 2) {
                aprobacionesRequest.setTipo(tipo.AsignacionProveedor);
            } else if (opcion == 3) {
                aprobacionesRequest.setTipo(tipo.CambioMonto);
            } else if (opcion == 4) {
                aprobacionesRequest.setTipo(tipo.ConfirmacionCliente);
            } else if (opcion == 5) {
                aprobacionesRequest.setTipo(tipo.MontoInicial);
            } else if (opcion == 6) {
                aprobacionesRequest.setTipo(tipo.RevisionSupervisor);
            }

            RemedyStub consulta = new RemedyStub();

            ConsultarAprobacionesResponse response = new RemedyStub.ConsultarAprobacionesResponse();

            response = consulta.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            //logger.info("MENSAJE aprobaciones: " + aprobacionesResult.getMensaje());
            logbi.insertaerror(0, aprobacionesResult.getMensaje(), "ConsultarAprobaciones()");
            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return null;
        }

        return result;
    }

    public boolean solicitaCambioMonto(int idIncidente, String solicitante, String aprobador, String justificacion, String monto, String nomArchivo, String adjuntoBase) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("solicitaCambioMonto()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.SolicitaCambioMonto);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }

            incide.setAprSolicitante(solicitante);
            incide.setAprAprobador(aprobador);

            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setMontoEstimado(new BigDecimal(monto));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "SolicitaCambioMonto()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public ArrayOfInfoIncidente ConsultaDetalleFolio(int idFolio) {
        ArrayOfInfoIncidente arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaDetalleFolio()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setSupervisorAsignado(0);
            nFiltro.setCoordinadorAsignado(0);
            nFiltro.setIdIncidente(idFolio);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public ArrayOfInfoIncidente consultaFoliosProveedor(String proveedor, String opcEst) {
        ArrayOfInfoIncidente arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaFoliosProveedor()" + " MATENIMIENTO");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            //System.out.println("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setSupervisorAsignado(0);
            nFiltro.setCoordinadorAsignado(0);
            nFiltro.setProveedor(proveedor);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public boolean cierreTecnicoRemoto(int idIncidente, String justificacion, String nomArchivo, String adjuntoBase, String nomArchivo2, String adjuntoBase2, String solicitante, String aprobador, String clienteAvisado, String OT) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreTecnicoRemoto()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.Cierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }

            if (adjuntoBase2 != null) {
                if (adjuntoBase2.trim().toLowerCase().equals("null") || adjuntoBase2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase2);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto2Base(data);
                }

            }
            if (nomArchivo2 != null) {
                if (nomArchivo2.trim().toLowerCase().equals("null") || nomArchivo2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Nombre("");
                } else {
                    incide.setAdjunto2Nombre(nomArchivo2);

                }
            } else {
                incide.setAdjunto2Nombre("");
            }
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.Remoto);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion("");
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion(justificacion);
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            incide.setAprSolicitante(solicitante);
            incide.setAprAprobador(aprobador);
            incide.setDiasAplazamiento(0);
            incide.setClienteAvisado(clienteAvisado);
            incide.setOT(OT);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "CierreTecnicoRemoto()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo en cierre remoto del incidente " + e);

            return false;
        }

        return result;
    }

    public boolean cierreTecnicoSitio(int idIncidente, String justificacion,
            String nomArchivo, String adjuntoBase, String nomArchivo2, String adjuntoBase2, String latitud, String longitud, String solicitante, String aprobador, String clienteAvisado, String OT) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreTecnicoSitio()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.Cierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }

            if (adjuntoBase2 != null) {
                if (adjuntoBase2.trim().toLowerCase().equals("null") || adjuntoBase2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase2);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto2Base(data);
                }

            }
            if (nomArchivo2 != null) {
                if (nomArchivo2.trim().toLowerCase().equals("null") || nomArchivo2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Nombre("");
                } else {
                    incide.setAdjunto2Nombre(nomArchivo2);

                }
            } else {
                incide.setAdjunto2Nombre("");
            }
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.EnSitio);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion("");
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion(justificacion);
            _Origen TA = null;
            incide.setOrigen(TA.NA);
            incide.setLatitud(latitud);
            incide.setLongitud(longitud);

            incide.setAprSolicitante(solicitante);
            incide.setAprAprobador(aprobador);
            incide.setDiasAplazamiento(0);
            incide.setClienteAvisado(clienteAvisado);
            incide.setOT(OT);

            /*if(incide != null) {
            	logger.info("OBJETO_REQUEST_MANTENIMIENTO: "+ incide.toString() );

            	logger.info("(INICIO_CIERRE_TECNICO) nomArchivo: "+ nomArchivo +"adjuntoBase: "+ adjuntoBase
            			+ "nomArchivo2: "+nomArchivo2 + " adjuntoBase2: "+adjuntoBase2 +" latitud: " +latitud
            			+ " longitud: " +longitud + "solicitante: " + solicitante +" aprobador: "+aprobador
            			+ "clienteAvisado"+ clienteAvisado + "OT: "+OT);

            	logger.info("Bytes adjuntoBase size = " +  adjuntoBase.getBytes().length);
            	logger.info("Bytes adjuntoBase2 size = " +  adjuntoBase2.getBytes().length);
            }*/
            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje() + "  // " + response);
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "cierreTecnicoSitio()");
            if (actIncidenteResult.getExito()) {
                result = true;
            } else {

                /*logger.info("(INICIO_CIERRE_TECNICO) nomArchivo: "+ nomArchivo +"adjuntoBase: "+ adjuntoBase
            			+ "nomArchivo2: "+nomArchivo2 + " adjuntoBase2: "+adjuntoBase2 +" latitud: " +latitud
            			+ " longitud: " +longitud + "solicitante: " + solicitante +" aprobador: "+aprobador
            			+ "clienteAvisado"+ clienteAvisado + "OT: "+OT);

            	logger.info("Bytes adjuntoBase size = " +  adjuntoBase.getBytes().length);
            	logger.info("Bytes adjuntoBase2 size = " +  adjuntoBase2.getBytes().length);*/
            }

        } catch (Exception e) {

            /*logger.info("Ocurrio algo en cierre remoto del incidente " + e);

        	logger.info("(INICIO_CIERRE_TECNICO) nomArchivo: "+ nomArchivo +"adjuntoBase: "+ adjuntoBase
        			+ "nomArchivo2: "+nomArchivo2 + " adjuntoBase2: "+adjuntoBase2 +" latitud: " +latitud
        			+ " longitud: " +longitud + "solicitante: " + solicitante +" aprobador: "+aprobador
        			+ "clienteAvisado"+ clienteAvisado + "OT: "+OT);

        	logger.info("Bytes adjuntoBase size = " +  adjuntoBase.getBytes().length);
        	logger.info("Bytes adjuntoBase2 size = " +  adjuntoBase2.getBytes().length);*/
            return false;
        }

        return result;
    }

    public boolean inicioAtencion(int idIncidente, String fecha, String latitud, String longitud) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("inicioAtencion()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AtencionProveedor);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();

            incide.setAdjunto1Base(null);
            incide.setAdjunto1Nombre("");
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion(fecha);
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion("");
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);
            incide.setLatitud(latitud);
            incide.setLongitud(longitud);

            incide.setAprSolicitante("");
            incide.setAprAprobador("");
            incide.setDiasAplazamiento(0);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "inicioAtencion()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo iniciar la atencion de proveedor " + e);

            return false;
        }

        return result;
    }

    public boolean aceptaCliente(int idIncidente, int calificacion) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("aceptaCliente()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AceptaCliente);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();

            incide.setAdjunto1Base(null);
            incide.setAdjunto1Nombre("");
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(true);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion("");
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            _Calificacion calAux = null;

            if (calificacion == 1) {
                incide.setCalificacion(calAux.Item1);
            }
            if (calificacion == 2) {
                incide.setCalificacion(calAux.Item2);
            }
            if (calificacion == 3) {
                incide.setCalificacion(calAux.Item3);
            }
            if (calificacion == 4) {
                incide.setCalificacion(calAux.Item4);
            }
            if (calificacion == 5) {
                incide.setCalificacion(calAux.Item5);
            }

            incide.setAprSolicitante("");
            incide.setAprAprobador("");
            incide.setDiasAplazamiento(0);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AceptaCliente()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo acptar el trabajo cliente " + e);

            return false;
        }

        return result;
    }

    public boolean rechazaCliente(int idIncidente, String justificacion, String nomArchivo, String adjuntoBase, String solicitante, String aprobador) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("rechazaCliente()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AceptaCliente);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(false);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setCorrecto(false);
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);

            incide.setJustificacion(justificacion);

            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);

            incide.setNombreTecnico(justificacion);

            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);
            incide.setDiasAplazamiento(0);
            incide.setAprSolicitante(solicitante);
            incide.setAprAprobador(aprobador);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "RechazaCliente()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean cierreSupervisor(int idIncidente, int correcto, String justificacion) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreSupervisor()" + " MATENIMIENTO");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AutorizaCierra);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();

            incide.setAdjunto1Base(null);
            incide.setAdjunto1Nombre("");
            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");
            incide.setAutoriza(true);
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            if (correcto == 1) {
                incide.setCorrecto(false);
            }
            if (correcto == 0) {
                incide.setCorrecto(true);
            }
            incide.setFechaAtencion("");
            incide.setFechaEstimada("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setMontoEstimado(new BigDecimal("0"));
            _MotivoEstado motivo = null;
            incide.setMotivoEstado(motivo.NA);
            incide.setNombreTecnico("");
            incide.setPosibleGarantia(false);
            incide.setProveedor("");
            incide.setResolucion("");
            _Origen TA = null;
            incide.setOrigen(TA.NA);

            incide.setAprSolicitante("");
            incide.setAprAprobador("");
            incide.setDiasAplazamiento(0);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "CierreSupervisor()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo acptar el trabajo cliente " + e);

            return false;
        }

        return result;
    }

    public boolean actualizaCuadrillas(String idCuadrilla, String alterno, String pwd, String correo, String jefe, int noProveedor, String telefono, String zona) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("actualizaCuadrillas()" + " MATENIMIENTO");
        }

        try {
            Cuadrilla cuadrilla = new Cuadrilla();

            cuadrilla.setIdCuadrilla(idCuadrilla);
            cuadrilla.setAlterno(alterno);
            cuadrilla.setSegundoACargo(alterno);
            cuadrilla.setContrasenia(pwd);
            cuadrilla.setCorreo(correo);
            cuadrilla.setJefe(jefe);
            cuadrilla.setNoProveedor(noProveedor);
            cuadrilla.setTelefono(telefono);
            String zonaS = "";
            if (zona.contains("0")) {
                zonaS = "NORTE";
            }
            if (zona.contains("1")) {
                zonaS = "CENTRO";
            }
            if (zona.contains("2")) {
                zonaS = "SUR";
            }
            cuadrilla.setZona(zonaS);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            ActualizarCuadrillas actCuadrilla = new ActualizarCuadrillas();
            actCuadrilla.setAuthentication(autenticatiosRemedy);
            actCuadrilla.setCuadrilla(cuadrilla);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarCuadrillasResponse response = new RemedyStub.ActualizarCuadrillasResponse();

            response = consulta.actualizarCuadrillas(actCuadrilla);

            RsRespond resultCuadrilla = response.getActualizarCuadrillasResult();

            //logger.info("MENSAJE Actualizar cuadrilla: " + resultCuadrilla.getMensaje());
            logbi.insertaerror(0, resultCuadrilla.getMensaje(), "ActualizaCuadrillas()");

            if (resultCuadrilla.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al incertar cuadrilla " + e);

            return false;
        }

        return result;
    }

    public boolean cargaUsuariosRemedy() {
        boolean salida = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cargaUsuariosRemedy()" + " MATENIMIENTO");
        }

        try {
            Calendar today = Calendar.getInstance();
            //today.add(Calendar.DATE, -1);
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            ListaPersonal listaPersonal = new ListaPersonal();
            listaPersonal.setFecha(today);
            listaPersonal.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ListaPersonal request = new RemedyStub.ListaPersonal();
            RemedyStub.ListaPersonalResponse response = new RemedyStub.ListaPersonalResponse();

            response = consulta.listaPersonal(listaPersonal);

            RemedyStub.RsCPersonal listaPersonalResult = response.getListaPersonalResult();

            if (listaPersonalResult.getExito()) {
                logger.info("Encontro Personal");

                RemedyStub.ArrayOfPersonal x = listaPersonalResult.getOPersonal();
                if (x.getPersonal() != null) {
                    salida = true;

                    RemedyStub.Personal[] aux = x.getPersonal();
                    if (aux != null) {

                        for (RemedyStub.Personal aux2 : aux) {
                            logger.info("" + aux2.getIdCorporativo() + ", " + aux2.getNombre());
                            List<Usuario_ADTO> res = usuarioBI.obtieneUsuario(Integer.parseInt(aux2.getIdCorporativo().trim()));
                            if (res == null || res.isEmpty()) {
                                Usuario_ADTO usuarioA = new Usuario_ADTO();
                                int activo = 0;
                                if (aux2.getPerfil().trim().toLowerCase().equals("activado")) {
                                    activo = 1;
                                }
                                usuarioA.setActivo(activo);
                                usuarioA.setFecha("19990101");
                                usuarioA.setIdCeco("230001");
                                int puesto = 99999999;
                                if (aux2.getPuesto().toLowerCase().trim().equals("supervisor mantenimiento")) {
                                    puesto = 99999998;
                                }
                                usuarioA.setIdPuesto(puesto);
                                usuarioA.setIdUsuario(Integer.parseInt(aux2.getIdCorporativo().trim()));
                                usuarioA.setNombre(aux2.getNombre());

                                logger.info("Inserto el personal de REMEDY: " + usuarioabi.insertaUsuario(usuarioA));
                            } else {
                                Usuario_ADTO usuarioA = new Usuario_ADTO();
                                int activo = 0;
                                if (aux2.getPerfil().trim().toLowerCase().equals("activado")) {
                                    activo = 1;
                                }
                                usuarioA.setActivo(activo);
                                usuarioA.setFecha("19990101");
                                usuarioA.setIdCeco("230001");
                                int puesto = 99999999;
                                if (aux2.getPuesto().toLowerCase().trim().equals("supervisor mantenimiento")) {
                                    puesto = 99999998;
                                }
                                usuarioA.setIdPuesto(puesto);
                                usuarioA.setIdUsuario(Integer.parseInt(aux2.getIdCorporativo().trim()));
                                usuarioA.setNombre(aux2.getNombre());

                                logger.info("Inserto el personal de REMEDY: " + usuarioabi.actualizaUsuario(usuarioA));
                            }

                        }
                    }
                }

            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al consultar el personal de REMEDY " + e);

            return salida;
        }

        return salida;
    }

    public boolean crearCuadrillas(String idCuadrilla, String alterno, String pwd, String correo, String jefe, int noProveedor, String telefono, String zona) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("crearCuadrillas()" + " MATENIMIENTO");
        }

        try {

            Cuadrilla cuadrilla = new Cuadrilla();

            cuadrilla.setIdCuadrilla(idCuadrilla);
            cuadrilla.setAlterno(alterno);
            cuadrilla.setSegundoACargo(alterno);
            cuadrilla.setContrasenia(pwd);
            cuadrilla.setCorreo(correo);
            cuadrilla.setJefe(jefe);
            cuadrilla.setNoProveedor(noProveedor);
            cuadrilla.setTelefono(telefono);
            String zonaS = "";
            if (zona.contains("0")) {
                zonaS = "NORTE";
            }
            if (zona.contains("1")) {
                zonaS = "CENTRO";
            }
            if (zona.contains("2")) {
                zonaS = "SUR";
            }
            cuadrilla.setZona(zonaS);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            CrearCuadrilla crearCuadrilla = new CrearCuadrilla();
            crearCuadrilla.setAuthentication(autenticatiosRemedy);
            crearCuadrilla.setCuadrilla(cuadrilla);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.CrearCuadrillaResponse response = new RemedyStub.CrearCuadrillaResponse();

            response = consulta.crearCuadrilla(crearCuadrilla);

            RsRespond resultCuadrilla = response.getCrearCuadrillaResult();

            //logger.info("MENSAJE Actualizar cuadrilla: " + resultCuadrilla.getMensaje());
            logbi.insertaerror(0, resultCuadrilla.getMensaje(), "CrearCuadrillas()");

            if (resultCuadrilla.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al insertar cuadrilla " + e);

            return false;
        }

        return result;
    }

    public HashMap<String, String> consultaUsuariosRemedy() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaUsuariosRemedy()" + " MATENIMIENTO");
        }

        HashMap<String, String> EmpleadosHashMap = new HashMap<String, String>();

        boolean salida = false;
        try {
            Calendar today = Calendar.getInstance();
            //today.add(Calendar.DATE, -1);
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            ListaPersonal listaPersonal = new ListaPersonal();
            listaPersonal.setFecha(today);
            listaPersonal.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ListaPersonal request = new RemedyStub.ListaPersonal();
            RemedyStub.ListaPersonalResponse response = new RemedyStub.ListaPersonalResponse();

            response = consulta.listaPersonal(listaPersonal);

            RemedyStub.RsCPersonal listaPersonalResult = response.getListaPersonalResult();

            if (listaPersonalResult.getExito()) {
                logger.info("Encontro Personal");

                RemedyStub.ArrayOfPersonal x = listaPersonalResult.getOPersonal();
                if (x.getPersonal() != null) {
                    salida = true;

                    RemedyStub.Personal[] aux = x.getPersonal();
                    if (aux != null) {

                        for (RemedyStub.Personal aux2 : aux) {
                            logger.info("" + aux2.getIdCorporativo() + ", " + aux2.getNombre());
                            EmpleadosHashMap.put(aux2.getIdCorporativo().trim(), aux2.getTelefono());

                        }
                    }
                }

            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al consultar el personal de REMEDY " + e);

            return EmpleadosHashMap;
        }

        return EmpleadosHashMap;
    }

    public boolean getPermisoPeticionesBD() {

        /*boolean respuesta = false;

    	try {
    		List<ParametroDTO > listaParam = parametroBi.obtieneParametros("ejecutaInsertBD");

    		if(listaParam != null && listaParam.size() > 0 ) {

    			ParametroDTO parametro = listaParam.get(0);

    			if(parametro != null && parametro.getValor().equals("1") ) {
    				respuesta = true;
    			}else {
    				respuesta =  false;
    			}

    		}

    		return respuesta;

    	}catch(Exception e) {
    		return false;
    	}*/
        return false;

    }

}
