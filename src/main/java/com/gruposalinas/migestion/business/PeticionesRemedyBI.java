package com.gruposalinas.migestion.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.dao.PeticionesRemedyDAO;
import com.gruposalinas.migestion.domain.PeticionDTO;

public class PeticionesRemedyBI {

    private static Logger logger = LogManager.getLogger(PeticionesRemedyBI.class);

    @Autowired
    PeticionesRemedyDAO peticionesRemedyDAO;

    public ArrayList< PeticionDTO> consultaPeticiones(String fechaInicio, String fechaFin) {

        ArrayList< PeticionDTO> listaPeticiones = null;

        try {

            if (validaFormatoFecha(fechaInicio) && validaFormatoFecha(fechaFin)) {
                listaPeticiones = peticionesRemedyDAO.consultaPeticiones(fechaInicio, fechaFin);

            }

        } catch (Exception e) {
            logger.info("No fue posible obtener las peticiones ");

            return null;
        }

        return listaPeticiones;

    }

    private boolean validaFormatoFecha(String fecha) {

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        try {

            sdf.parse(fecha);

            return true;

        } catch (ParseException e) {

            return false;

        }

    }

    public boolean insertaPeticion(String origen) {

        try {

            return peticionesRemedyDAO.insertaPeticion(origen);

        } catch (Exception e) {
            logger.info("No fue posible insertar la peticion ");
            return false;
        }

    }

    public int eliminaPeticiones(String fechaInicio, String fechaFin) {

        try {

            return peticionesRemedyDAO.eliminaPeticiones(fechaInicio, fechaFin);

        } catch (Exception e) {
            logger.info("No fue posible eliminar las peticiones ");

            return -2;
        }

    }

}
