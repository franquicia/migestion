package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.FormatoMetodo7sDAO;
import com.gruposalinas.migestion.domain.FormatoMetodo7sDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class FormatoMetodo7sBI {

    private static Logger logger = LogManager.getLogger(FormatoMetodo7sBI.class);

    private List<FormatoMetodo7sDTO> listafila;

    @Autowired
    FormatoMetodo7sDAO formatoMetodo7sDAO;

    public boolean elimina(int idtab, String idusuario) {
        boolean respuesta = false;

        try {
            respuesta = formatoMetodo7sDAO.elimina(idtab, idusuario);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }


    public List<FormatoMetodo7sDTO> obtieneInfo() {

        try {
            listafila = formatoMetodo7sDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Formato 7s ");

        }

        return listafila;
    }


    public boolean actualiza(FormatoMetodo7sDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = formatoMetodo7sDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
