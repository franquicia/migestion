package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.IncidentesDAOImpl;
import com.gruposalinas.migestion.domain.IncidentesDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class IncidentesAdmBI {

    private static Logger logger = LogManager.getLogger(IncidentesAdmBI.class);

    private List<IncidentesDTO> listaIncicentes;

    @Autowired
    IncidentesDAOImpl incidentesDAO;

    public List<IncidentesDTO> obtieneIncidentes(String idIncidente, String idUsuario) {

        try {
            listaIncicentes = incidentesDAO.obtieneIncidentes(idIncidente, idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener el registro de la Tarea");

        }

        return listaIncicentes;
    }

    public boolean insertaIncidentes(String idIncidente, int idUsuario, int commit) {
        boolean respuesta = false;

        try {
            respuesta = incidentesDAO.insertaIncidentes(idIncidente, idUsuario, commit);
        } catch (Exception e) {
            logger.info("No fue posible insertar el incidente");

        }

        return respuesta;
    }

    public boolean actualizaIncidente(String idIncidente, int idUsuario, int commit) {
        boolean respuesta = false;

        try {
            respuesta = incidentesDAO.actualizaIncidente(idIncidente, idUsuario, commit);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el registro el incidente");

        }

        return respuesta;
    }

    public boolean eliminaIncidente(String idIncidente) {
        boolean respuesta = false;

        try {
            respuesta = incidentesDAO.eliminaIncidente(idIncidente);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el incidente");

        }

        return respuesta;
    }

}
