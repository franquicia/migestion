package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.BitacoraMantenimientoDAO;
import com.gruposalinas.migestion.domain.BitacoraMntoDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class BitacoraMantenimientoBI {

    private static Logger logger = LogManager.getLogger(AgendaBI.class);

    @Autowired
    BitacoraMantenimientoDAO bitacoraMantenimientoDAO;


    public int actualizaBitacoraMnto(BitacoraMntoDTO bean) {
        int respuesta = 0;
        try {
            respuesta = bitacoraMantenimientoDAO.actualizaBitacoraMnto(bean);
        } catch (Exception e) {
            logger.info("BitacoraMantenimientoBI||actualizaBitacoraMnto||No fue posible actualizar la bitacora de mantenimiento:" + e.getMessage());
        }
        return respuesta;
    }

    public int eliminaBitacoraMnto(Integer idBitacora) {
        int respuesta = 0;
        try {
            respuesta = bitacoraMantenimientoDAO.eliminaBitacoraMnto(idBitacora);
        } catch (Exception e) {
            logger.info("BitacoraMantenimientoBI||eliminaBitacoraMnto||No fue posible eliminar la bitacora de mantenimiento:" + e.getMessage());
        }
        return respuesta;
    }

    public List<BitacoraMntoDTO> consultaBitacoraMntoIds(int idAmbito) {
        List<BitacoraMntoDTO> respuesta = new ArrayList<BitacoraMntoDTO>();
        try {
            respuesta = bitacoraMantenimientoDAO.consultaBitacoraMntoIds(idAmbito);
        } catch (Exception e) {
            logger.info("BitacoraMantenimientoBI||consultaBitacoraMntoIds||No fue posible consultar la tabla de bitacora de mantenimiento:" + e.getMessage());
        }
        return respuesta;
    }
}
