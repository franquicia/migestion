package com.gruposalinas.migestion.business;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.NullHostNameVerifier;
import com.gruposalinas.migestion.util.NullX509TrustManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SesionFirmaBI {

    private Logger logger = LogManager.getLogger(SesionFirmaBI.class);

    public String createSession(String idUsuario, String token, String key) {

        String ticket = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(GTNConstantes.getURLFirma() + "createSessionService");

            //OutputStream os = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "idUsuario=" + idUsuario + "&password=" + token + "&key=" + key;

            logger.info("Parametros Url: " + urlParameters);

            conn = (HttpURLConnection) url.openConnection();
            logger.info("Despues de abrir conexion ");
            logger.info("conn:  " + conn);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(res.toString()).getAsJsonObject();

            String code = jsonObject.get("codigo").getAsString();

            if (!code.equals("200")) {
                ticket = "ERROR";
                String status = jsonObject.get("status").getAsString();
                logger.info("Codigo: " + code + " Status : " + status + " usuario " + idUsuario);
            } else {
                ticket = jsonObject.get("ticket").getAsString();
            }

            /**/
        } catch (Exception e) {
            e.printStackTrace();
            ticket = "ERROR";
        } finally {
            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null) {
                    conn.disconnect();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ticket;
    }

    public boolean checkSession(String ticket) {

        //String isSession = "";
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(GTNConstantes.getURLFirma() + "checkSessionExistence");

            //OutputStream os = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "idSession=" + ticket;

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            System.out.println(res.toString());

            boolean isSessionExist = Boolean.valueOf(res.toString());

            if (isSessionExist) {
                return true;
            } else {
                return false;
            }

            /**/
        } catch (Exception e) {

            e.printStackTrace();
            return false;
        } finally {
            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null) {
                    conn.disconnect();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public boolean closeSession(String noEmpleado) {

        //String isSession = "";
        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(GTNConstantes.getURLFirma() + "rompeSesionPorNoEmpleado");

            //OutputStream os = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "noEmpleado=" + noEmpleado;

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            System.out.println(res.toString());

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(res.toString()).getAsJsonObject();

            boolean status = jsonObject.get("ok").getAsBoolean();

            if (status) {
                return true;
            } else {
                return false;
            }

            /**/
        } catch (Exception e) {

            e.printStackTrace();
            return false;
        } finally {
            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public JsonObject getDataSession(String ticket, String refer) {

        JsonObject respuesta = null;
        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(GTNConstantes.getURLFirma() + "getSessionJSONService");

            //OutputStream os = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "ticket=" + ticket + "&refer=" + refer;

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            System.out.println(res.toString());

            JsonParser parser = new JsonParser();
            respuesta = parser.parse(res.toString()).getAsJsonObject();

            /**/
        } catch (Exception e) {

            e.printStackTrace();
            return new JsonObject();
        } finally {
            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return respuesta;
    }

    public static boolean wsTokenUsuario(String urlStr, String cadena) throws IOException {
        UtilServicios utilServ = new UtilServicios();
        boolean valido = false;
        BufferedReader read = null;
        String line = "";
        read = utilServ.consumeWebService(urlStr, cadena);

        try {
            while ((line = read.readLine()) != null) {
                if (line.contains("<GetDataResult>0</GetDataResult>")) {
                    valido = true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            read.close();
        }
        return valido;
    }

}
