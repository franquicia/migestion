package com.gruposalinas.migestion.business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.dao.NotificacionesDAO;
import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;

public class NotificacionesBI {

    @Autowired
    NotificacionesDAO notificacionesDAO;

    static String apiKey = "AIzaSyCN-msi853al5iJrm0ihXErHXj9k2SYOzI";
    private final String FCM_SERVICE = "https://fcm.googleapis.com/fcm/send";

    private static Logger logger = LogManager.getLogger(NotificacionesBI.class);

    @SuppressWarnings("unused")
    public boolean enviarNotificacionJSON(String token, String titulo, String mensaje) {

        int totSucces = 0;
        int totFail = 0;

        String json = "";

        json = "{ " + "\"notification\":{ " + "\"sound\": \"OverrideSound\" " + "\"title\":\"" + titulo + "\", "
                + "\"text\":\"" + mensaje + "\" " + "}, " + "\"registration_ids\":[\"" + token + "\"] " + "}";
        System.out.println("JSON: " + json);

        HttpURLConnection conn = null;
        OutputStream os = null;
        BufferedReader rd = null;
        try {
            URL url = new URL(FCM_SERVICE);


            /*ÇSystem.setProperty("java.net.useSystemProxies", "true");
             System.setProperty("http.proxyHost", "10.50.8.20");
             System.setProperty("http.proxyPort", "8080");
             // System.setProperty("http.proxyUser", "B196228");
             // System.setProperty("http.proxyPassword", "Bancoazteca2345");
             System.setProperty("http.proxySet", "true");

             System.setProperty("https.proxyHost", "10.50.8.20");
             System.setProperty("https.proxyPort", "8080");
             // System.setProperty("https.proxyUser", "B196228");
             // System.setProperty("https.proxyPassword", "Bancoazteca2345");
             System.setProperty("https.proxySet", "true");*/
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);
            // conn.setRequestProperty("Content-Type", "text/html");

            os = conn.getOutputStream(); //Cerrar Stream
            os.write(json.getBytes("UTF-8"));

            System.out.println("CODIGO: " + conn.getResponseCode());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            // rd=new BufferedInputStream(conn.getInputStream());

            StringBuffer response = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
            System.out.println(response.toString());

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();

            totSucces = jsonObject.get("success").getAsInt();
            totFail = jsonObject.get("failure").getAsInt();

        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            System.out.println("ERROR: " + e);
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {

            }

            try {
                if (os != null) {
                    os.close();
                }

            } catch (Exception e) {

            }
        }

        if (totSucces == 1) {
            return true;
        } else {
            return false;
        }

    }

    public void enviaNotificacionesCeco(String ceco, String puesto, String mensaje, String title) throws Exception {

        List<UsuarioInfoTokenDTO> tokens = null;

        tokens = notificacionesDAO.consultaTokens(ceco, puesto);

        logger.info("Total a enviar :" + tokens.size());
        System.out.println("Total a enviar :" + tokens.size());

        if (tokens != null || tokens.size() > 0) {

            for (UsuarioInfoTokenDTO usuarioInfoTokenDTO : tokens) {
                enviarNotificacionJSON(usuarioInfoTokenDTO.getToken(), title, mensaje);
            }

        }

    }

    public JsonObject enviarNotificacionJSONSinProxy(String token, String titulo, String mensaje) {

        String apiKey = "AIzaSyCN-msi853al5iJrm0ihXErHXj9k2SYOzI";
        String FCM_SERVICE = "https://fcm.googleapis.com/fcm/send";
        String json = "";
        JsonObject jsonObject = null;

        json = "{ " + "\"notification\":{ " + "\"sound\": \"OverrideSound\" " + ",\"title\":\"" + titulo + "\", "
                + "\"text\":\"" + mensaje + "\" " + "}, " + "\"registration_ids\":[\"" + token + "\"] " + "}";
        System.out.println("JSON: " + json);

        try {
            URL url = new URL(FCM_SERVICE);
            HttpURLConnection conn = null;
            OutputStream os = null;
            BufferedReader rd = null;

            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 8080));

            conn = (HttpURLConnection) url.openConnection(proxy);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);
            // conn.setRequestProperty("Content-Type", "text/html");

            os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));

            System.out.println("CODIGO: " + conn.getResponseCode());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            // rd=new BufferedInputStream(conn.getInputStream());

            StringBuffer response = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                response.append(line);
            }

            System.out.println(response.toString());

            JsonParser parser = new JsonParser();
            jsonObject = parser.parse(response.toString()).getAsJsonObject();

            int totSucces = jsonObject.get("success").getAsInt();
            int totFail = jsonObject.get("failure").getAsInt();

            System.out.println("Success: " + totSucces);
            System.out.println("Success: " + totFail);

        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());

            System.out.println("ERROR: " + e);

        }

        return jsonObject;

    }

    public static void main(String[] args) {

        NotificacionesBI n = new NotificacionesBI();
        n.enviarNotificacionJSONSinProxy("dctT86fAe9k:APA91bHHu5xGwvgowK06JMgt1P9u12OCfuklMvr5bp57p8Y0xvPWhFwyNYBdSBi0KrrFsXMgt6sS1rd5SpGcJoqx3hxug5TD8NGWLNj86-9DugE61M8pBjr2wHpA-G6jW4srrnYaPLlm", "Test", "Hola Mundo");
    }

}
