package com.gruposalinas.migestion.business;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Formatter;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CajerosAutomaticosBI {

    //desarrollo
    /*public static final String HOST_CAJEROS="10.51.193.108";
     public static final String INFO_CAJEROS="http://10.51.193.108:8082/WSCentralATM/web/CentralATM/infoCajeros";
     public static final String DETALLE_CAJEROS="http://10.51.193.108:8082/WSCentralATM/web/CentralATM/detalleCajero";*/
    //produccion
    public static final String HOST_CAJEROS = "10.63.14.133";
    public static final String INFO_CAJEROS = "http://10.63.14.133:28080/WSCentralATM/web/CentralATM/infoCajeros";
    public static final String DETALLE_CAJEROS = "http://10.63.14.133:28080/WSCentralATM/web/CentralATM/detalleCajero";

    private static Logger logger = LogManager.getLogger(CajerosAutomaticosBI.class);

    class GeneratorToken {
        //private static final String KEY = "T0k3NG3n3R4t0R";
        //private static final String KEY;

        public static final String ERROR_TOKEN = "TKN_ERROR";

        public GeneratorToken() {
        }

        public String generateToken(String data) {
            String token;
            try {
                byte[] keyBytes = "T0k3NG3n3R4t0R".getBytes("UTF-8");
                MessageDigest sha = MessageDigest.getInstance("SHA-1");
                keyBytes = sha.digest(keyBytes);
                keyBytes = Arrays.copyOf(keyBytes, 16);
                SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

                //Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
                //cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                cipher.init(1, secretKeySpec);

                byte[] bytes = cipher.doFinal(data.getBytes("UTF-8"));

                Formatter formatter = new Formatter();
                for (byte b : bytes) {
                    formatter.format("%02x", new Object[]{Byte.valueOf(b)});
                }

                token = formatter.toString();
            } catch (UnsupportedEncodingException e) {
                token = "TKN_ERROR: " + e.getMessage();
            } catch (InvalidKeyException e) {
                token = "TKN_ERROR: " + e.getMessage();
            } catch (NoSuchAlgorithmException e) {
                token = "TKN_ERROR: " + e.getMessage();
            } catch (BadPaddingException e) {
                token = "TKN_ERROR: " + e.getMessage();
            } catch (IllegalBlockSizeException e) {
                token = "TKN_ERROR: " + e.getMessage();
            } catch (NoSuchPaddingException e) {
                token = "TKN_ERROR: " + e.getMessage();
            } catch (Exception e) {
                token = "TKN_ERROR: " + e.getMessage();
            }

            return token;
        }
    }

    static class Cajero {

        private String idCajero;
        private String tipoCajero;
        private String clasificacion;
        private int estatusFalla;
        private int estatusTransaccion;
        private int realTransaccion;
        private int planTransaccion;

        public Cajero() {

        }

        public String getIdCajero() {
            return idCajero;
        }

        public void setIdCajero(String idCajero) {
            this.idCajero = idCajero;
        }

        public String getTipoCajero() {
            return tipoCajero;
        }

        public void setTipoCajero(String tipoCajero) {
            this.tipoCajero = tipoCajero;
        }

        public String getClasificacion() {
            return clasificacion;
        }

        public void setClasificacion(String clasificacion) {
            this.clasificacion = clasificacion;
        }

        public int getEstatusFalla() {
            return estatusFalla;
        }

        public void setEstatusFalla(int estatusFalla) {
            this.estatusFalla = estatusFalla;
        }

        public int getEstatusTransaccion() {
            return estatusTransaccion;
        }

        public void setEstatusTransaccion(int estatusTransaccion) {
            this.estatusTransaccion = estatusTransaccion;
        }

        public int getRealTransaccion() {
            return realTransaccion;
        }

        public void setRealTransaccion(int realTransaccion) {
            this.realTransaccion = realTransaccion;
        }

        public int getPlanTransaccion() {
            return planTransaccion;
        }

        public void setPlanTransaccion(int planTransaccion) {
            this.planTransaccion = planTransaccion;
        }

        @Override
        public String toString() {
            return "Cajero [idCajero=" + idCajero + ", tipoCajero=" + tipoCajero + ", clasificacion=" + clasificacion
                    + ", estatusFalla=" + estatusFalla + ", estatusTransaccion=" + estatusTransaccion
                    + ", realTransaccion=" + realTransaccion + ", planTransaccion=" + planTransaccion + "]";
        }

    }

}
