package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.InfoVistaDAO;
import com.gruposalinas.migestion.domain.InfoVistaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class InfoVistaBI {

    private static Logger logger = LogManager.getLogger(InfoVistaBI.class);

    @Autowired
    InfoVistaDAO InfoVistaDAO;

    List<InfoVistaDTO> listaInfoVista = null;

    public List<InfoVistaDTO> buscaInfoVista(int idCatVista) {

        try {
            listaInfoVista = InfoVistaDAO.buscaInfoVista(idCatVista);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Perfil");

        }

        return listaInfoVista;
    }

    public List<InfoVistaDTO> buscaInfoVistaParam(String idInfoVista, String idCatVista, String idUsuario, String plataforma, String fecha) {

        try {
            listaInfoVista = InfoVistaDAO.buscaInfoVistaParam(idInfoVista, idCatVista, idUsuario, plataforma, fecha);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Perfil");

        }

        return listaInfoVista;
    }

    public boolean insertaInfoVista(InfoVistaDTO vista) {

        boolean respuesta = false;

        try {
            respuesta = InfoVistaDAO.insertaInfoVista(vista);
        } catch (Exception e) {
            logger.info("No fue posible insertar la vista");

        }

        return respuesta;
    }

    public boolean actualizaInfoVista(InfoVistaDTO vista) {

        boolean respuesta = false;

        try {
            respuesta = InfoVistaDAO.actualizaInfoVista(vista);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la vista");

        }

        return respuesta;
    }

    public boolean eliminaInfoVistal(int idCatVista) {

        boolean respuesta = false;

        try {
            respuesta = InfoVistaDAO.eliminaInfoVista(idCatVista);
        } catch (Exception e) {
            logger.info("No fue posible eliminar la vista");

        }

        return respuesta;
    }
}
