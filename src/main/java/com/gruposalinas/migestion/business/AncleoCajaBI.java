package com.gruposalinas.migestion.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.domain.AncleoCajaAperturaCierreDTO;
import com.gruposalinas.migestion.domain.AncleoCajaSeguimientoDTO;
import com.gruposalinas.migestion.domain.DetalleAncleoCajaAperturaCierreDTO;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.NullHostNameVerifier;
import com.gruposalinas.migestion.util.NullX509TrustManager;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AncleoCajaBI {

    @Autowired
    ArqueoBI arqueoBI;

    private Logger logger = LogManager.getLogger(AncleoCajaBI.class);

    /*::::::::::::::MÉTODOS DE ANCLEO DE CAJA SEGUIMIENTO::::::::::::::::::::::::*/
    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaSeguimientoDTO> arqueoGeneralSeguimientoPorcentaje(int pais, int nivelCeco, int numCeco, String puesto) {

        String response = null;
        //Guardo Todos Los Objetos Que Tiene mi Petición
        ArrayList<AncleoCajaSeguimientoDTO> arrayDetalle = new ArrayList<AncleoCajaSeguimientoDTO>();
        //Guardo el Porcentaje del Seguimiento
        ArrayList<AncleoCajaSeguimientoDTO> arrayGlobal = new ArrayList<AncleoCajaSeguimientoDTO>();

        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        try {

            //			logger.info(puesto);
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosGeneral/seguimiento");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + ",\"puesto\":\"" + puesto + "\"}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); //Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();
                int totalSursalesGlobal = 0;
                int totalSucursalesGlobalConArqueo = 0;
                int totalSucursalesGlobalSinArqueo = 0;
                int conDiferenciaGlobal = 0;
                int sinDiferenciaGlobal = 0;

                int total1ArqDif = 0;
                int total1ArqSinDif = 0;
                int total2ArqDif = 0;
                int total2ArqSinDif = 0;
                int total3ArqDif = 0;
                int total3ArqSinDif = 0;
                double porcentajeGlobal = 0;
                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    je.toString();

                    AncleoCajaSeguimientoDTO obj = new AncleoCajaSeguimientoDTO();
                    obj.setIdPais(je.getAsJsonObject().get("idPais").getAsInt());
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsString());
                    obj.setTotalSucursales(je.getAsJsonObject().get("totalSucursales").getAsInt());
                    obj.setTotalSucSinArqueo(je.getAsJsonObject().get("totalSucSinArqueo").getAsInt());
                    obj.setTotalSucConArqueo(je.getAsJsonObject().get("totalSucConArqueo").getAsInt());
                    obj.setSucConUnArqueo(je.getAsJsonObject().get("sucConUnArqueo").getAsInt());
                    obj.setTotalArqueosConUnArqueo(je.getAsJsonObject().get("totalArqueosConUnArqueo").getAsInt());
                    obj.setArqueosConUnArqueoSinDif(je.getAsJsonObject().get("arqueosConUnArqueoSinDif").getAsInt());
                    obj.setArqueosConUnArqueoConDif(je.getAsJsonObject().get("arqueosConUnArqueoConDif").getAsInt());
                    obj.setSucConDosArqueos(je.getAsJsonObject().get("sucConDosArqueos").getAsInt());
                    obj.setTotalArqueosConDosArqueos(je.getAsJsonObject().get("totalArqueosConDosArqueos").getAsInt());
                    obj.setArqueosConDosArqueosSinDif(je.getAsJsonObject().get("arqueosConDosArqueosSinDif").getAsInt());
                    obj.setArqueosConDosArqueosConDif(je.getAsJsonObject().get("arqueosConDosArqueosConDif").getAsInt());
                    obj.setSucConMasDeDosArqueos(je.getAsJsonObject().get("sucConMasDeDosArqueos").getAsInt());
                    obj.setTotalArqueosConMasDeDos(je.getAsJsonObject().get("totalArqueosConMasDeDos").getAsInt());
                    obj.setArqueosConMasDeDosSinDif(je.getAsJsonObject().get("arqueosConMasDeDosSinDif").getAsInt());
                    obj.setArqueosConMasDeDosConDif(je.getAsJsonObject().get("arqueosConMasDeDosConDif").getAsInt());
                    /*Se agrega el porcentaje al objeto*/
 /*Se agrega el porcentaje al objeto*/
                    int porcentaje = (obj.getTotalSucConArqueo() * 100) / obj.getTotalSucursales();
                    obj.setPorcentaje(porcentaje + "");
                    arrayDetalle.add(obj);

                    totalSursalesGlobal += obj.getTotalSucursales();
                    totalSucursalesGlobalConArqueo += obj.getTotalSucConArqueo();
                    totalSucursalesGlobalSinArqueo += obj.getTotalSucSinArqueo();

                    total1ArqDif = obj.getArqueosConUnArqueoConDif();
                    total1ArqSinDif = obj.getArqueosConUnArqueoSinDif();
                    total2ArqDif = obj.getArqueosConDosArqueosConDif();
                    total2ArqSinDif = obj.getArqueosConDosArqueosSinDif();
                    total3ArqDif = obj.getArqueosConMasDeDosConDif();
                    total3ArqSinDif = obj.getArqueosConMasDeDosSinDif();

                    int conDiferenciaLocal = total1ArqDif + total2ArqDif + total3ArqDif;
                    int sinDiferenciaLocal = total1ArqSinDif + total2ArqSinDif + total3ArqSinDif;

                    conDiferenciaGlobal += conDiferenciaLocal;
                    sinDiferenciaGlobal += sinDiferenciaLocal;

                    if (conDiferenciaLocal > 0) {
                        obj.setColor("rojo");
                    } else {
                        obj.setColor("verde");
                    }

                }
                logger.info("TOTAL DE SUCURSALES GLOBAL CON ARQUEO --- " + totalSucursalesGlobalConArqueo);
                logger.info("TOTAL DE SUCURSALES GLOBAL --- " + totalSursalesGlobal);
                porcentajeGlobal = ((totalSucursalesGlobalConArqueo * 100) / totalSursalesGlobal);

                AncleoCajaSeguimientoDTO objGlobal = new AncleoCajaSeguimientoDTO();
                objGlobal.setIdPais(arrayDetalle.get(0).getIdPais());
                objGlobal.setIdTerritorio(arrayDetalle.get(0).getIdTerritorio());
                objGlobal.setIdZona(arrayDetalle.get(0).getIdZona());
                objGlobal.setIdSucursal(arrayDetalle.get(0).getIdSucursal());
                //NOMBRE
                objGlobal.setNombreCeco(arqueoBI.obtieneNombre(numCeco + "").get(0).getNombreCeco());
                objGlobal.setNombreCeco("Arqueo Caja de Seguimiento");

                objGlobal.setPorcentaje(porcentajeGlobal + "");

                if (conDiferenciaGlobal > 0) {
                    objGlobal.setColor("rojo");
                } else {
                    objGlobal.setColor("verde");
                }

                arrayGlobal.add(objGlobal);

            }

            /**/
        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

        }

        return arrayGlobal;
    }

    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaSeguimientoDTO> arqueoGeneralSeguimientoPorcentajeDesglose(int pais, int nivelCeco, int numCeco, String puesto) {

        String response = null;
        //Guardo Todos Los Objetos Que Tiene mi Petición
        ArrayList<AncleoCajaSeguimientoDTO> arrayDetalle = new ArrayList<AncleoCajaSeguimientoDTO>();
        //Guardo el Porcentaje del Seguimiento
        ArrayList<AncleoCajaSeguimientoDTO> arrayGlobal = new ArrayList<AncleoCajaSeguimientoDTO>();
        ArrayList<AncleoCajaSeguimientoDTO> verde = new ArrayList<AncleoCajaSeguimientoDTO>();
        ArrayList<AncleoCajaSeguimientoDTO> rojo = new ArrayList<AncleoCajaSeguimientoDTO>();

        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        OutputStream os = null;
        try {

            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosGeneral/seguimiento");

            //OutputStream os = null;
            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + ",\"puesto\":\"" + puesto + "\"}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();
                int totalSursalesGlobal = 0;
                int totalSucursalesGlobalConArqueo = 0;
                int totalSucursalesGlobalSinArqueo = 0;
                double porcentajeGlobal = 0;

                int conDiferenciaGlobal = 0;
                int sinDiferenciaGlobal = 0;

                int total1ArqDif = 0;
                int total1ArqSinDif = 0;
                int total2ArqDif = 0;
                int total2ArqSinDif = 0;
                int total3ArqDif = 0;
                int total3ArqSinDif = 0;

                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    AncleoCajaSeguimientoDTO obj = new AncleoCajaSeguimientoDTO();
                    obj.setIdPais(je.getAsJsonObject().get("idPais").getAsInt());
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsString());
                    obj.setTotalSucursales(je.getAsJsonObject().get("totalSucursales").getAsInt());
                    obj.setTotalSucSinArqueo(je.getAsJsonObject().get("totalSucSinArqueo").getAsInt());
                    obj.setTotalSucConArqueo(je.getAsJsonObject().get("totalSucConArqueo").getAsInt());
                    obj.setSucConUnArqueo(je.getAsJsonObject().get("sucConUnArqueo").getAsInt());
                    obj.setTotalArqueosConUnArqueo(je.getAsJsonObject().get("totalArqueosConUnArqueo").getAsInt());
                    obj.setArqueosConUnArqueoSinDif(je.getAsJsonObject().get("arqueosConUnArqueoSinDif").getAsInt());
                    obj.setArqueosConUnArqueoConDif(je.getAsJsonObject().get("arqueosConUnArqueoConDif").getAsInt());
                    obj.setSucConDosArqueos(je.getAsJsonObject().get("sucConDosArqueos").getAsInt());
                    obj.setTotalArqueosConDosArqueos(je.getAsJsonObject().get("totalArqueosConDosArqueos").getAsInt());
                    obj.setArqueosConDosArqueosSinDif(je.getAsJsonObject().get("arqueosConDosArqueosSinDif").getAsInt());
                    obj.setArqueosConDosArqueosConDif(je.getAsJsonObject().get("arqueosConDosArqueosConDif").getAsInt());
                    obj.setSucConMasDeDosArqueos(je.getAsJsonObject().get("sucConMasDeDosArqueos").getAsInt());
                    obj.setTotalArqueosConMasDeDos(je.getAsJsonObject().get("totalArqueosConMasDeDos").getAsInt());
                    obj.setArqueosConMasDeDosSinDif(je.getAsJsonObject().get("arqueosConMasDeDosSinDif").getAsInt());
                    obj.setArqueosConMasDeDosConDif(je.getAsJsonObject().get("arqueosConMasDeDosConDif").getAsInt());
                    //AGREGAR EL NOMBRE DEL CECO Y EL ID CECO
                    if (nivelCeco == 0) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdTerritorio()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdTerritorio());
                    }
                    if (nivelCeco == 1) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdZona()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdZona());
                    }
                    if (nivelCeco == 2) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdRegion()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdRegion());
                    }
                    if (nivelCeco == 3) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdSucursal()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdSucursal());
                    }

                    /*Se agrega el porcentaje al objeto*/
                    int porcentaje = (obj.getTotalSucConArqueo() * 100) / obj.getTotalSucursales();
                    obj.setPorcentaje(porcentaje + "");
                    arrayDetalle.add(obj);

                    total1ArqDif = obj.getArqueosConUnArqueoConDif();
                    total1ArqSinDif = obj.getArqueosConUnArqueoSinDif();
                    total2ArqDif = obj.getArqueosConDosArqueosConDif();
                    total2ArqSinDif = obj.getArqueosConDosArqueosSinDif();
                    total3ArqDif = obj.getArqueosConMasDeDosConDif();
                    total3ArqSinDif = obj.getArqueosConMasDeDosSinDif();

                    int conDiferenciaLocal = total1ArqDif + total2ArqDif + total3ArqDif;
                    int sinDiferenciaLocal = total1ArqSinDif + total2ArqSinDif + total3ArqSinDif;

                    conDiferenciaGlobal += conDiferenciaLocal;
                    sinDiferenciaGlobal += sinDiferenciaLocal;

                    if (conDiferenciaLocal > 0) {
                        obj.setColor("rojo");
                    } else {
                        obj.setColor("verde");
                    }

                    totalSursalesGlobal += obj.getTotalSucursales();
                    totalSucursalesGlobalConArqueo += obj.getTotalSucConArqueo();
                    totalSucursalesGlobalSinArqueo += obj.getTotalSucSinArqueo();

                }

                porcentajeGlobal = ((totalSucursalesGlobalConArqueo * 100) / totalSursalesGlobal);

                AncleoCajaSeguimientoDTO objGlobal = new AncleoCajaSeguimientoDTO();
                objGlobal.setIdPais(arrayDetalle.get(0).getIdPais());
                objGlobal.setIdTerritorio(arrayDetalle.get(0).getIdTerritorio());
                objGlobal.setIdZona(arrayDetalle.get(0).getIdZona());
                objGlobal.setIdSucursal(arrayDetalle.get(0).getIdSucursal());
                objGlobal.setNombreCeco("Pendiente");
                objGlobal.setPorcentaje(porcentajeGlobal + "");

                if (conDiferenciaGlobal > 0) {
                    objGlobal.setColor("rojo");
                } else {
                    objGlobal.setColor("verde");
                }

                arrayGlobal.add(objGlobal);

            }

            /**/
        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {

            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

        }

        verde = ordenaAncleoCajaSegColor(arrayDetalle, "verde");
        verde = ordenaAncleoCajaSeg(verde.size(), verde);

        rojo = ordenaAncleoCajaSegColor(arrayDetalle, "rojo");
        rojo = ordenaAncleoCajaSeg(rojo.size(), rojo);

        //ordenaAncleoCajaApCiColor
        //return arrayDetalle;
        return agregaAncleoCajaSegColor(rojo, verde);

    }

    /*::::::::::::::FIN MÉTODOS DE ANCLEO DE CAJA SEGUIMIENTO::::::::::::::::::::::::*/

 /*::::::::::::::MÉTODOS DE ANCLEO DE CAJA APERTURA::::::::::::::::::::::::*/
    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaAperturaCierreDTO> arqueoGeneralAperturaPorcentaje(int pais, int nivelCeco, int numCeco) {

        String response = null;
        //Guardo Todos Los Objetos Que Tiene mi Petición
        ArrayList<AncleoCajaAperturaCierreDTO> arrayDetalle = new ArrayList<AncleoCajaAperturaCierreDTO>();
        //Guardo el Porcentaje del Seguimiento
        ArrayList<AncleoCajaAperturaCierreDTO> arrayGlobal = new ArrayList<AncleoCajaAperturaCierreDTO>();

        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosGeneral/aperturaCierre");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + "}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); //Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();
                int totalSursalesGlobal = 0;
                int totalSucursalesGlobalConArqueo = 0;
                int totalSucursalesGlobalSinArqueo = 0;
                int conteoDiferencia = 0;
                double porcentajeGlobal = 0;
                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    AncleoCajaAperturaCierreDTO obj = new AncleoCajaAperturaCierreDTO();
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsString());
                    obj.setTotalSucursales(je.getAsJsonObject().get("totalSucursales").getAsInt());
                    obj.setSucConArqueoApertura(je.getAsJsonObject().get("sucConArqueoApertura").getAsInt());
                    obj.setSucSinArqueoApertura(je.getAsJsonObject().get("sucSinArqueoApertura").getAsInt());
                    obj.setSucConArqueoAperturaSinDif(je.getAsJsonObject().get("sucConArqueoAperturaSinDif").getAsInt());
                    obj.setSucConArqueoAperturaConDif(je.getAsJsonObject().get("sucConArqueoAperturaConDif").getAsInt());
                    obj.setSucConArqueoCierre(je.getAsJsonObject().get("sucConArqueoCierre").getAsInt());
                    obj.setSucSinArqueoCierre(je.getAsJsonObject().get("sucSinArqueoCierre").getAsInt());
                    obj.setSucConArqueoCierreSinDif(je.getAsJsonObject().get("sucConArqueoCierreSinDif").getAsInt());
                    obj.setSucConArqueoCierreConDif(je.getAsJsonObject().get("sucConArqueoCierreConDif").getAsInt());

                    if (obj.getSucConArqueoAperturaConDif() > 0) {
                        obj.setColor("rojo");
                        conteoDiferencia++;
                    } else {
                        obj.setColor("verde");
                    }

                    //Se agrega el porcentaje al objeto
                    int porcentaje = (obj.getSucConArqueoApertura() * 100) / obj.getTotalSucursales();
                    obj.setPorcentaje(porcentaje + "");
                    arrayDetalle.add(obj);

                    totalSursalesGlobal += obj.getTotalSucursales();
                    totalSucursalesGlobalConArqueo += obj.getSucConArqueoApertura();
                    totalSucursalesGlobalSinArqueo += obj.getSucSinArqueoApertura();

                }

                porcentajeGlobal = ((totalSucursalesGlobalConArqueo * 100) / totalSursalesGlobal);

                AncleoCajaAperturaCierreDTO objGlobal = new AncleoCajaAperturaCierreDTO();
                objGlobal.setIdTerritorio(arrayDetalle.get(0).getIdTerritorio());
                objGlobal.setIdZona(arrayDetalle.get(0).getIdZona());
                objGlobal.setIdSucursal(arrayDetalle.get(0).getIdSucursal());

                //NOMBRE
                //objGlobal.setNombreCeco(arqueoBI.obtieneNombre(numCeco+"").get(0).getNombreCeco());
                objGlobal.setNombreCeco("Arqueo Caja de Apertura");

                objGlobal.setPorcentaje(porcentajeGlobal + "");
                if (conteoDiferencia > 0) {
                    objGlobal.setColor("rojo");
                } else {
                    objGlobal.setColor("verde");
                }

                arrayGlobal.add(objGlobal);

            }

        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
        }

        return arrayGlobal;
    }

    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaAperturaCierreDTO> arqueoGeneralAperturaDesglose(int pais, int nivelCeco, int numCeco) {

        String response = null;
        //Guardo Todos Los Objetos Que Tiene mi Petición
        ArrayList<AncleoCajaAperturaCierreDTO> arrayDetalle = new ArrayList<AncleoCajaAperturaCierreDTO>();
        //Guardo el Porcentaje del Seguimiento
        ArrayList<AncleoCajaAperturaCierreDTO> arrayGlobal = new ArrayList<AncleoCajaAperturaCierreDTO>();
        ArrayList<AncleoCajaAperturaCierreDTO> rojo = new ArrayList<AncleoCajaAperturaCierreDTO>();
        ArrayList<AncleoCajaAperturaCierreDTO> verde = new ArrayList<AncleoCajaAperturaCierreDTO>();

        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        try {
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosGeneral/aperturaCierre");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + "}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); //Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();
                int totalSursalesGlobal = 0;
                int totalSucursalesGlobalConArqueo = 0;
                int totalSucursalesGlobalSinArqueo = 0;
                double porcentajeGlobal = 0;
                int conteoDiferencia = 0;
                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    AncleoCajaAperturaCierreDTO obj = new AncleoCajaAperturaCierreDTO();
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsString());
                    obj.setTotalSucursales(je.getAsJsonObject().get("totalSucursales").getAsInt());
                    obj.setSucConArqueoApertura(je.getAsJsonObject().get("sucConArqueoApertura").getAsInt());
                    obj.setSucSinArqueoApertura(je.getAsJsonObject().get("sucSinArqueoApertura").getAsInt());
                    obj.setSucConArqueoAperturaSinDif(je.getAsJsonObject().get("sucConArqueoAperturaSinDif").getAsInt());
                    obj.setSucConArqueoAperturaConDif(je.getAsJsonObject().get("sucConArqueoAperturaConDif").getAsInt());
                    obj.setSucConArqueoCierre(je.getAsJsonObject().get("sucConArqueoCierre").getAsInt());
                    obj.setSucSinArqueoCierre(je.getAsJsonObject().get("sucSinArqueoCierre").getAsInt());
                    obj.setSucConArqueoCierreSinDif(je.getAsJsonObject().get("sucConArqueoCierreSinDif").getAsInt());
                    obj.setSucConArqueoCierreConDif(je.getAsJsonObject().get("sucConArqueoCierreConDif").getAsInt());

                    //AGREGAR EL NOMBRE DEL CECO Y EL ID CECO
                    if (nivelCeco == 0) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdTerritorio()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdTerritorio());
                    }
                    if (nivelCeco == 1) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdZona()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdZona());
                    }
                    if (nivelCeco == 2) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdRegion()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdRegion());
                    }
                    if (nivelCeco == 3) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdSucursal()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdSucursal());
                    }

                    if (obj.getSucConArqueoAperturaConDif() > 0) {
                        obj.setColor("rojo");
                        conteoDiferencia++;
                    } else {
                        obj.setColor("verde");
                    }

                    //Se agrega el porcentaje al objeto
                    int porcentaje = (obj.getSucConArqueoApertura() * 100) / obj.getTotalSucursales();
                    obj.setPorcentaje(porcentaje + "");

                    arrayDetalle.add(obj);

                    totalSursalesGlobal += obj.getTotalSucursales();
                    totalSucursalesGlobalConArqueo += obj.getSucConArqueoApertura();
                    totalSucursalesGlobalSinArqueo += obj.getSucSinArqueoApertura();

                }

                porcentajeGlobal = ((totalSucursalesGlobalConArqueo * 100) / totalSursalesGlobal);

                AncleoCajaAperturaCierreDTO objGlobal = new AncleoCajaAperturaCierreDTO();
                objGlobal.setIdTerritorio(arrayDetalle.get(0).getIdTerritorio());
                objGlobal.setIdZona(arrayDetalle.get(0).getIdZona());
                objGlobal.setIdSucursal(arrayDetalle.get(0).getIdSucursal());
                objGlobal.setNombreCeco("Pendiente");
                objGlobal.setPorcentaje(porcentajeGlobal + "");

                if (conteoDiferencia > 0) {
                    objGlobal.setColor("rojo");
                } else {
                    objGlobal.setColor("verde");
                }

                arrayGlobal.add(objGlobal);

            }

        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
        }
        logger.info("SIZE DENTRO DE MI BI DE LO QUE RETORNO************* " + arrayDetalle.size());

        verde = ordenaAncleoCajaApCiColor(arrayDetalle, "verde");
        verde = ordenaAncleoCajaApCi(verde.size(), verde);

        rojo = ordenaAncleoCajaApCiColor(arrayDetalle, "rojo");
        rojo = ordenaAncleoCajaApCi(rojo.size(), rojo);

        //ordenaAncleoCajaApCiColor
        //return arrayDetalle;
        return agregaAncleoCajaApCiColor(rojo, verde);
    }

    /*::::::::::::::FIN MÉTODOS DE ANCLEO DE CAJA APERTURA::::::::::::::::::::::::*/

 /*::::::::::::::MÉTODOS DE ANCLEO DE CAJA CIERRE::::::::::::::::::::::::*/
    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaAperturaCierreDTO> arqueoGeneralCierrePorcentaje(int pais, int nivelCeco, int numCeco) {

        String response = null;
        //Guardo Todos Los Objetos Que Tiene mi Petición
        ArrayList<AncleoCajaAperturaCierreDTO> arrayDetalle = new ArrayList<AncleoCajaAperturaCierreDTO>();
        //Guardo el Porcentaje del Seguimiento
        ArrayList<AncleoCajaAperturaCierreDTO> arrayGlobal = new ArrayList<AncleoCajaAperturaCierreDTO>();

        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosGeneral/aperturaCierre");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + "}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); //Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();
                int totalSursalesGlobal = 0;
                int totalSucursalesGlobalConArqueo = 0;
                int totalSucursalesGlobalSinArqueo = 0;
                double porcentajeGlobal = 0;
                int conteoDiferencia = 0;
                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    AncleoCajaAperturaCierreDTO obj = new AncleoCajaAperturaCierreDTO();
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsString());
                    obj.setTotalSucursales(je.getAsJsonObject().get("totalSucursales").getAsInt());
                    obj.setSucConArqueoApertura(je.getAsJsonObject().get("sucConArqueoApertura").getAsInt());
                    obj.setSucSinArqueoApertura(je.getAsJsonObject().get("sucSinArqueoApertura").getAsInt());
                    obj.setSucConArqueoAperturaSinDif(je.getAsJsonObject().get("sucConArqueoAperturaSinDif").getAsInt());
                    obj.setSucConArqueoAperturaConDif(je.getAsJsonObject().get("sucConArqueoAperturaConDif").getAsInt());
                    obj.setSucConArqueoCierre(je.getAsJsonObject().get("sucConArqueoCierre").getAsInt());
                    obj.setSucSinArqueoCierre(je.getAsJsonObject().get("sucSinArqueoCierre").getAsInt());
                    obj.setSucConArqueoCierreSinDif(je.getAsJsonObject().get("sucConArqueoCierreSinDif").getAsInt());
                    obj.setSucConArqueoCierreConDif(je.getAsJsonObject().get("sucConArqueoCierreConDif").getAsInt());

                    if (obj.getSucConArqueoCierreConDif() > 0) {
                        obj.setColor("rojo");
                        conteoDiferencia++;
                    } else {
                        obj.setColor("verde");
                    }

                    //Se agrega el porcentaje al objeto
                    int porcentaje = (obj.getSucConArqueoCierre() * 100) / obj.getTotalSucursales();
                    obj.setPorcentaje(porcentaje + "");
                    arrayDetalle.add(obj);
                    totalSursalesGlobal += obj.getTotalSucursales();
                    totalSucursalesGlobalConArqueo += obj.getSucConArqueoCierre();
                    totalSucursalesGlobalSinArqueo += obj.getSucSinArqueoCierre();

                }

                porcentajeGlobal = ((totalSucursalesGlobalConArqueo * 100) / totalSursalesGlobal);

                logger.info(totalSucursalesGlobalConArqueo + " *****" + totalSucursalesGlobalSinArqueo);

                AncleoCajaAperturaCierreDTO objGlobal = new AncleoCajaAperturaCierreDTO();
                objGlobal.setIdTerritorio(arrayDetalle.get(0).getIdTerritorio());
                objGlobal.setIdZona(arrayDetalle.get(0).getIdZona());
                objGlobal.setIdSucursal(arrayDetalle.get(0).getIdSucursal());

                //NOMBRE
                //objGlobal.setNombreCeco(arqueoBI.obtieneNombre(numCeco+"").get(0).getNombreCeco());
                objGlobal.setNombreCeco("Arqueo Caja de Cierre");

                objGlobal.setPorcentaje(porcentajeGlobal + "");
                if (conteoDiferencia > 0) {
                    objGlobal.setColor("rojo");
                } else {
                    objGlobal.setColor("verde");
                }

                arrayGlobal.add(objGlobal);

            }

        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
        }
        return arrayGlobal;
    }

    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaAperturaCierreDTO> arqueoGeneralCierreDesglose(int pais, int nivelCeco, int numCeco) {

        String response = null;
        //Guardo Todos Los Objetos Que Tiene mi Petición
        ArrayList<AncleoCajaAperturaCierreDTO> arrayDetalle = new ArrayList<AncleoCajaAperturaCierreDTO>();
        //Guardo el Porcentaje del Seguimiento
        ArrayList<AncleoCajaAperturaCierreDTO> arrayGlobal = new ArrayList<AncleoCajaAperturaCierreDTO>();
        ArrayList<AncleoCajaAperturaCierreDTO> rojo = new ArrayList<AncleoCajaAperturaCierreDTO>();
        ArrayList<AncleoCajaAperturaCierreDTO> verde = new ArrayList<AncleoCajaAperturaCierreDTO>();

        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosGeneral/aperturaCierre");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + "}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream()); //Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();
                int totalSursalesGlobal = 0;
                int totalSucursalesGlobalConArqueo = 0;
                int totalSucursalesGlobalSinArqueo = 0;
                double porcentajeGlobal = 0;
                int conteoDiferencia = 0;
                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    AncleoCajaAperturaCierreDTO obj = new AncleoCajaAperturaCierreDTO();
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsString());
                    obj.setTotalSucursales(je.getAsJsonObject().get("totalSucursales").getAsInt());
                    obj.setSucConArqueoApertura(je.getAsJsonObject().get("sucConArqueoApertura").getAsInt());
                    obj.setSucSinArqueoApertura(je.getAsJsonObject().get("sucSinArqueoApertura").getAsInt());
                    obj.setSucConArqueoAperturaSinDif(je.getAsJsonObject().get("sucConArqueoAperturaSinDif").getAsInt());
                    obj.setSucConArqueoAperturaConDif(je.getAsJsonObject().get("sucConArqueoAperturaConDif").getAsInt());
                    obj.setSucConArqueoCierre(je.getAsJsonObject().get("sucConArqueoCierre").getAsInt());
                    obj.setSucSinArqueoCierre(je.getAsJsonObject().get("sucSinArqueoCierre").getAsInt());
                    obj.setSucConArqueoCierreSinDif(je.getAsJsonObject().get("sucConArqueoCierreSinDif").getAsInt());
                    obj.setSucConArqueoCierreConDif(je.getAsJsonObject().get("sucConArqueoCierreConDif").getAsInt());

                    //AGREGAR EL NOMBRE DEL CECO Y EL ID CECO
                    if (nivelCeco == 0) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdTerritorio()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdTerritorio());
                    }
                    if (nivelCeco == 1) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdZona()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdZona());
                    }
                    if (nivelCeco == 2) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdRegion()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdRegion());
                    }
                    if (nivelCeco == 3) {
                        obj.setNombreCeco(arqueoBI.obtieneNombre(obj.getIdSucursal()).get(0).getNombreCeco());
                        obj.setCecoGlobal(obj.getIdSucursal());
                    }

                    if (obj.getSucConArqueoCierreConDif() > 0) {
                        obj.setColor("rojo");
                        conteoDiferencia++;
                    } else {
                        obj.setColor("verde");
                    }

                    //Se agrega el porcentaje al objeto
                    int porcentaje = (obj.getSucConArqueoApertura() * 100) / obj.getTotalSucursales();
                    obj.setPorcentaje(porcentaje + "");
                    arrayDetalle.add(obj);

                    totalSursalesGlobal += obj.getTotalSucursales();
                    totalSucursalesGlobalConArqueo += obj.getSucConArqueoApertura();
                    totalSucursalesGlobalSinArqueo += obj.getSucSinArqueoApertura();

                }

                porcentajeGlobal = ((totalSucursalesGlobalConArqueo * 100) / totalSursalesGlobal);
                AncleoCajaAperturaCierreDTO objGlobal = new AncleoCajaAperturaCierreDTO();
                objGlobal.setIdTerritorio(arrayDetalle.get(0).getIdTerritorio());
                objGlobal.setIdZona(arrayDetalle.get(0).getIdZona());
                objGlobal.setIdSucursal(arrayDetalle.get(0).getIdSucursal());
                objGlobal.setNombreCeco("Pendiente");
                objGlobal.setPorcentaje(porcentajeGlobal + "");
                if (objGlobal.getSucConArqueoCierreConDif() > 0) {
                    objGlobal.setColor("rojo");
                    conteoDiferencia++;
                } else {
                    objGlobal.setColor("verde");
                }

                arrayGlobal.add(objGlobal);

            }

        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
        }
        verde = ordenaAncleoCajaApCiColor(arrayDetalle, "verde");
        verde = ordenaAncleoCajaApCi(verde.size(), verde);

        rojo = ordenaAncleoCajaApCiColor(arrayDetalle, "rojo");
        rojo = ordenaAncleoCajaApCi(rojo.size(), rojo);

        //ordenaAncleoCajaApCiColor
        //return arrayDetalle;
        return agregaAncleoCajaApCiColor(rojo, verde);
    }

    /*::::::::::::::FIN MÉTODOS DE ANCLEO DE CAJA CIERRE::::::::::::::::::::::::*/

 /*::::::::::::::MÉTODOS DE DETALLE ANCLEO DE CAJA APERTURA::::::::::::::::::::::::*/
    @SuppressWarnings("unused")
    public ArrayList<DetalleAncleoCajaAperturaCierreDTO> arqueoAperturaDetalle(int pais, int nivelCeco, int numCeco) {

        String response = null;
        //Guardo el Porcentaje del Seguimiento
        ArrayList<DetalleAncleoCajaAperturaCierreDTO> arrayGlobal = new ArrayList<DetalleAncleoCajaAperturaCierreDTO>();
        //Guardo el Porcentaje del Seguimiento A Retornar
        ArrayList<DetalleAncleoCajaAperturaCierreDTO> arrayRetorno = new ArrayList<DetalleAncleoCajaAperturaCierreDTO>();

        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosDetalle/aperturaCierre");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + "}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();

                double contMxn = 0;
                double contUsd = 0;
                double contOlp = 0;
                double contCad = 0;
                double contEur = 0;

                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    DetalleAncleoCajaAperturaCierreDTO obj = new DetalleAncleoCajaAperturaCierreDTO();

                    obj.setFechaArqueo(je.getAsJsonObject().get("fechaArqueo").getAsString());
                    obj.setIdPais(je.getAsJsonObject().get("idPais").getAsInt());
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdCanal(je.getAsJsonObject().get("idCanal").getAsInt());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsInt());
                    obj.setTipoArqueo(je.getAsJsonObject().get("tipoArqueo").getAsInt());
                    obj.setDescTipoArqueo(je.getAsJsonObject().get("descTipoArqueo").getAsString());
                    obj.setIdDivisa(je.getAsJsonObject().get("idDivisa").getAsInt());
                    obj.setDescDivisa(je.getAsJsonObject().get("descDivisa").getAsString());
                    obj.setImporteSistema(je.getAsJsonObject().get("importeSistema").getAsDouble());
                    obj.setImporteArqueado(je.getAsJsonObject().get("importeArqueado").getAsDouble());
                    obj.setDiferenciaSaldos(je.getAsJsonObject().get("diferenciaSaldos").getAsDouble());
                    obj.setEmpValida(je.getAsJsonObject().get("empValida").getAsString());
                    obj.setPuestoValida(je.getAsJsonObject().get("puestoValida").getAsInt());
                    obj.setEmpAutoriza(je.getAsJsonObject().get("empAutoriza").getAsInt());
                    obj.setPuestoAutoriza(je.getAsJsonObject().get("puestoAutoriza").getAsInt());

                    arrayGlobal.add(obj);

                    if (obj.getDescTipoArqueo().equals("Apertura")) {
                        //if (obj.getDescTipoArqueo().equals("Apertura")){

                        switch (obj.getIdDivisa()) {

                            //MXN
                            case (1):
                                contMxn += obj.getDiferenciaSaldos();
                                break;
                            //USD
                            case (2):
                                contUsd += obj.getDiferenciaSaldos();
                                break;
                            //OLP
                            case (3):
                                contOlp += obj.getDiferenciaSaldos();
                                break;
                            //CAD
                            case (5):
                                contCad += obj.getDiferenciaSaldos();
                                break;
                            //EUR
                            case (7):
                                contEur += obj.getDiferenciaSaldos();
                                break;

                        }

                    }
                }

                DetalleAncleoCajaAperturaCierreDTO objMxn = new DetalleAncleoCajaAperturaCierreDTO();
                objMxn.setDiferenciaSaldos(contMxn);
                objMxn.setDescDivisa("MXN");
                arrayRetorno.add(objMxn);

                DetalleAncleoCajaAperturaCierreDTO objUsd = new DetalleAncleoCajaAperturaCierreDTO();
                objUsd.setDiferenciaSaldos(contUsd);
                objUsd.setDescDivisa("USD");
                arrayRetorno.add(objUsd);

                DetalleAncleoCajaAperturaCierreDTO objOlp = new DetalleAncleoCajaAperturaCierreDTO();
                objOlp.setDiferenciaSaldos(contOlp);
                objOlp.setDescDivisa("OLP");
                arrayRetorno.add(objOlp);

                DetalleAncleoCajaAperturaCierreDTO objCad = new DetalleAncleoCajaAperturaCierreDTO();
                objCad.setDiferenciaSaldos(contCad);
                objCad.setDescDivisa("CAD");
                arrayRetorno.add(objCad);

                DetalleAncleoCajaAperturaCierreDTO objEur = new DetalleAncleoCajaAperturaCierreDTO();
                objEur.setDiferenciaSaldos(contEur);
                objEur.setDescDivisa("EUR");
                arrayRetorno.add(objEur);
            }

        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
        }
        return arrayRetorno;
    }

    /*::::::::::::::FIN MÉTODOS DE DETALLE ANCLEO DE CAJA APERTURA::::::::::::::::::::::::*/

 /*::::::::::::::MÉTODOS DE DETALLE ANCLEO DE CAJA APERTURA::::::::::::::::::::::::*/
    @SuppressWarnings("unused")
    public ArrayList<DetalleAncleoCajaAperturaCierreDTO> arqueoCierreDetalle(int pais, int nivelCeco, int numCeco) {

        String response = null;
        //Guardo el Porcentaje del Seguimiento
        ArrayList<DetalleAncleoCajaAperturaCierreDTO> arrayGlobal = new ArrayList<DetalleAncleoCajaAperturaCierreDTO>();
        //Guardo el Porcentaje del Seguimiento A Retornar
        ArrayList<DetalleAncleoCajaAperturaCierreDTO> arrayRetorno = new ArrayList<DetalleAncleoCajaAperturaCierreDTO>();

        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosDetalle/aperturaCierre");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + "}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();

                double contMxn = 0;
                double contUsd = 0;
                double contOlp = 0;
                double contCad = 0;
                double contEur = 0;

                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    DetalleAncleoCajaAperturaCierreDTO obj = new DetalleAncleoCajaAperturaCierreDTO();

                    obj.setFechaArqueo(je.getAsJsonObject().get("fechaArqueo").getAsString());
                    obj.setIdPais(je.getAsJsonObject().get("idPais").getAsInt());
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdCanal(je.getAsJsonObject().get("idCanal").getAsInt());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsInt());
                    obj.setTipoArqueo(je.getAsJsonObject().get("tipoArqueo").getAsInt());
                    obj.setDescTipoArqueo(je.getAsJsonObject().get("descTipoArqueo").getAsString());
                    obj.setIdDivisa(je.getAsJsonObject().get("idDivisa").getAsInt());
                    obj.setDescDivisa(je.getAsJsonObject().get("descDivisa").getAsString());
                    obj.setImporteSistema(je.getAsJsonObject().get("importeSistema").getAsDouble());
                    obj.setImporteArqueado(je.getAsJsonObject().get("importeArqueado").getAsDouble());
                    obj.setDiferenciaSaldos(je.getAsJsonObject().get("diferenciaSaldos").getAsDouble());
                    obj.setEmpValida(je.getAsJsonObject().get("empValida").getAsString());
                    obj.setPuestoValida(je.getAsJsonObject().get("puestoValida").getAsInt());
                    obj.setEmpAutoriza(je.getAsJsonObject().get("empAutoriza").getAsInt());
                    obj.setPuestoAutoriza(je.getAsJsonObject().get("puestoAutoriza").getAsInt());

                    arrayGlobal.add(obj);

                    if (obj.getDescTipoArqueo().equals("Cierre")) {

                        switch (obj.getIdDivisa()) {

                            //MXN
                            case (1):
                                contMxn += obj.getDiferenciaSaldos();
                                break;
                            //USD
                            case (2):
                                contUsd += obj.getDiferenciaSaldos();
                                break;
                            //OLP
                            case (3):
                                contOlp += obj.getDiferenciaSaldos();
                                break;
                            //CAD
                            case (5):
                                contCad += obj.getDiferenciaSaldos();
                                break;
                            //EUR
                            case (7):
                                contEur += obj.getDiferenciaSaldos();
                                break;

                        }

                    }

                }

                DetalleAncleoCajaAperturaCierreDTO objMxn = new DetalleAncleoCajaAperturaCierreDTO();
                objMxn.setDiferenciaSaldos(contMxn);
                objMxn.setDescDivisa("MXN");
                arrayRetorno.add(objMxn);

                DetalleAncleoCajaAperturaCierreDTO objUsd = new DetalleAncleoCajaAperturaCierreDTO();
                objUsd.setDiferenciaSaldos(contUsd);
                objUsd.setDescDivisa("USD");
                arrayRetorno.add(objUsd);

                DetalleAncleoCajaAperturaCierreDTO objOlp = new DetalleAncleoCajaAperturaCierreDTO();
                objOlp.setDiferenciaSaldos(contOlp);
                objOlp.setDescDivisa("OLP");
                arrayRetorno.add(objOlp);

                DetalleAncleoCajaAperturaCierreDTO objCad = new DetalleAncleoCajaAperturaCierreDTO();
                objCad.setDiferenciaSaldos(contCad);
                objCad.setDescDivisa("CAD");
                arrayRetorno.add(objCad);

                DetalleAncleoCajaAperturaCierreDTO objEur = new DetalleAncleoCajaAperturaCierreDTO();
                objEur.setDiferenciaSaldos(contEur);
                objEur.setDescDivisa("EUR");
                arrayRetorno.add(objEur);
            }

        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
        }
        logger.info("ARRAY RETORNO" + arrayRetorno.size());

        return arrayRetorno;
    }

    /*::::::::::::::FIN MÉTODOS DE DETALLE ANCLEO DE CAJA APERTURA::::::::::::::::::::::::*/

 /*::::::::::::::MÉTODOS DE DETALLE SEGUIMIENTO ANCLEO::::::::::::::::::::::::*/
    @SuppressWarnings("unused")
    public ArrayList<DetalleAncleoCajaAperturaCierreDTO> arqueoSeguimientoDetalle(int pais, int nivelCeco, int numCeco, String puesto) {

        String response = null;
        //Guardo el Porcentaje del Seguimiento
        ArrayList<DetalleAncleoCajaAperturaCierreDTO> arrayGlobal = new ArrayList<DetalleAncleoCajaAperturaCierreDTO>();
        //Guardo el Porcentaje del Seguimiento A Retornar
        ArrayList<DetalleAncleoCajaAperturaCierreDTO> arrayRetorno = new ArrayList<DetalleAncleoCajaAperturaCierreDTO>();

        BufferedReader rd = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            URL url = new URL(GTNConstantes.getURLArqueo() + "AlarmasMonitoreo/services/rest/arqueosDetalle/seguimiento");

            //BufferedReader rd = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext cont = SSLContext.getInstance("TLS");
            cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());

            String urlParameters = "{\"idPais\":" + pais + ",\"nivelCeco\":" + nivelCeco + ",\"numCeco\":" + numCeco + ",\"puesto\":\"" + puesto + "\"}";

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setUseCaches(false);

            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            outputStreamWriter.write(urlParameters);
            outputStreamWriter.flush();

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream

            StringBuffer res = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }

            String jsonObject = (res.toString());
            logger.info(jsonObject);

            if (jsonObject.equals("[]")) {
                response = "ERROR";
                logger.info(jsonObject);
            } else {

                response = jsonObject;
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(res.toString()).getAsJsonArray();

                double contMxn = 0;
                double contUsd = 0;
                double contOlp = 0;
                double contCad = 0;
                double contEur = 0;

                for (int i = 0; i < array.size(); i++) {
                    JsonElement je = array.get(i);
                    DetalleAncleoCajaAperturaCierreDTO obj = new DetalleAncleoCajaAperturaCierreDTO();

                    obj.setFechaArqueo(je.getAsJsonObject().get("fechaArqueo").getAsString());
                    obj.setIdPais(je.getAsJsonObject().get("idPais").getAsInt());
                    obj.setIdTerritorio(je.getAsJsonObject().get("idTerritorio").getAsString());
                    obj.setIdZona(je.getAsJsonObject().get("idZona").getAsString());
                    obj.setIdRegion(je.getAsJsonObject().get("idRegion").getAsString());
                    obj.setIdCanal(je.getAsJsonObject().get("idCanal").getAsInt());
                    obj.setIdSucursal(je.getAsJsonObject().get("idSucursal").getAsInt());
                    obj.setTipoArqueo(je.getAsJsonObject().get("tipoArqueo").getAsInt());
                    obj.setDescTipoArqueo(je.getAsJsonObject().get("descTipoArqueo").getAsString());
                    obj.setIdDivisa(je.getAsJsonObject().get("idDivisa").getAsInt());
                    obj.setDescDivisa(je.getAsJsonObject().get("descDivisa").getAsString());
                    obj.setImporteSistema(je.getAsJsonObject().get("importeSistema").getAsDouble());
                    obj.setImporteArqueado(je.getAsJsonObject().get("importeArqueado").getAsDouble());
                    obj.setDiferenciaSaldos(je.getAsJsonObject().get("diferenciaSaldos").getAsDouble());
                    obj.setEmpValida(je.getAsJsonObject().get("empValida").getAsString());
                    obj.setPuestoValida(je.getAsJsonObject().get("puestoValida").getAsInt());
                    obj.setEmpAutoriza(je.getAsJsonObject().get("empAutoriza").getAsInt());
                    obj.setPuestoAutoriza(je.getAsJsonObject().get("puestoAutoriza").getAsInt());

                    arrayGlobal.add(obj);

                    if (obj.getDescTipoArqueo().equals("Seguimiento")) {

                        switch (obj.getIdDivisa()) {

                            //MXN
                            case (1):
                                contMxn += obj.getDiferenciaSaldos();
                                break;
                            //USD
                            case (2):
                                contUsd += obj.getDiferenciaSaldos();
                                break;
                            //OLP
                            case (3):
                                contOlp += obj.getDiferenciaSaldos();
                                break;
                            //CAD
                            case (5):
                                contCad += obj.getDiferenciaSaldos();
                                break;
                            //EUR
                            case (7):
                                contEur += obj.getDiferenciaSaldos();
                                break;

                        }

                    }

                }

                DetalleAncleoCajaAperturaCierreDTO objMxn = new DetalleAncleoCajaAperturaCierreDTO();
                objMxn.setDiferenciaSaldos(contMxn);
                objMxn.setDescDivisa("MXN");
                arrayRetorno.add(objMxn);

                DetalleAncleoCajaAperturaCierreDTO objUsd = new DetalleAncleoCajaAperturaCierreDTO();
                objUsd.setDiferenciaSaldos(contUsd);
                objUsd.setDescDivisa("USD");
                arrayRetorno.add(objUsd);

                DetalleAncleoCajaAperturaCierreDTO objOlp = new DetalleAncleoCajaAperturaCierreDTO();
                objOlp.setDiferenciaSaldos(contOlp);
                objOlp.setDescDivisa("OLP");
                arrayRetorno.add(objOlp);

                DetalleAncleoCajaAperturaCierreDTO objCad = new DetalleAncleoCajaAperturaCierreDTO();
                objCad.setDiferenciaSaldos(contCad);
                objCad.setDescDivisa("CAD");
                arrayRetorno.add(objCad);

                DetalleAncleoCajaAperturaCierreDTO objEur = new DetalleAncleoCajaAperturaCierreDTO();
                objEur.setDiferenciaSaldos(contEur);
                objEur.setDescDivisa("EUR");
                arrayRetorno.add(objEur);
            }

        } catch (Exception e) {
            logger.info("FALLO EN ANCLEO CAJA BI");
            logger.info(e.getMessage());
            response = "ERROR";
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }

            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un error al cerrar los Streams ");
                logger.info(e.getMessage());
            }
        }

        logger.info("ARRAY RETORNO" + arrayRetorno.size());

        return arrayRetorno;
    }

    //METODOS PARA ORDENAR
    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaSeguimientoDTO> ordenaAncleoCajaSeg(int tam, ArrayList<AncleoCajaSeguimientoDTO> lista) {

        boolean flag = false;
        int i = 1;
        double porcentAnt = 0.0;
        double porcentAct = 0.0;
        AncleoCajaSeguimientoDTO act = null;
        AncleoCajaSeguimientoDTO ant = null;
        ArrayList<AncleoCajaSeguimientoDTO> retorno = new ArrayList<AncleoCajaSeguimientoDTO>();

        while (i < tam) {
            porcentAnt = Double.parseDouble(lista.get(i - 1).getPorcentaje());
            porcentAct = Double.parseDouble(lista.get(i).getPorcentaje());

            if (porcentAnt > porcentAct) {
                ant = lista.get(i - 1);
                act = lista.get(i);
                lista.set(i - 1, act);
                lista.set(i, ant);
                flag = true;
            }
            if (flag && (i + 1) == tam) {
                i = 0;
                flag = false;
            }
            i++;
        }
        return lista;
    }

    @SuppressWarnings("unused")
    public ArrayList<AncleoCajaAperturaCierreDTO> ordenaAncleoCajaApCi(int tam, ArrayList<AncleoCajaAperturaCierreDTO> lista) {

        boolean flag = false;
        int i = 1;
        double porcentAnt = 0.0;
        double porcentAct = 0.0;
        AncleoCajaAperturaCierreDTO act = null;
        AncleoCajaAperturaCierreDTO ant = null;
        ArrayList<AncleoCajaAperturaCierreDTO> retorno = new ArrayList<AncleoCajaAperturaCierreDTO>();

        while (i < tam) {
            porcentAnt = Double.parseDouble(lista.get(i - 1).getPorcentaje());
            porcentAct = Double.parseDouble(lista.get(i).getPorcentaje());

            if (porcentAnt > porcentAct) {
                ant = lista.get(i - 1);
                act = lista.get(i);
                lista.set(i - 1, act);
                lista.set(i, ant);
                flag = true;
            }
            if (flag && (i + 1) == tam) {
                i = 0;
                flag = false;
            }
            i++;
        }
        return lista;
    }

    public ArrayList<AncleoCajaSeguimientoDTO> ordenaAncleoCajaSegColor(ArrayList<AncleoCajaSeguimientoDTO> lista, String color) {

        ArrayList<AncleoCajaSeguimientoDTO> retorno = new ArrayList<AncleoCajaSeguimientoDTO>();

        Iterator<AncleoCajaSeguimientoDTO> it = lista.iterator();

        while (it.hasNext()) {
            AncleoCajaSeguimientoDTO obj = (AncleoCajaSeguimientoDTO) it.next();
            if (obj.getColor().equals(color)) {
                retorno.add(obj);
            }
        }
        return retorno;
    }

    public ArrayList<AncleoCajaAperturaCierreDTO> ordenaAncleoCajaApCiColor(ArrayList<AncleoCajaAperturaCierreDTO> lista, String color) {

        ArrayList<AncleoCajaAperturaCierreDTO> retorno = new ArrayList<AncleoCajaAperturaCierreDTO>();

        Iterator<AncleoCajaAperturaCierreDTO> it = lista.iterator();

        while (it.hasNext()) {
            AncleoCajaAperturaCierreDTO obj = (AncleoCajaAperturaCierreDTO) it.next();
            if (obj.getColor().equals(color)) {
                retorno.add(obj);
            }
        }
        return retorno;
    }

    public ArrayList<AncleoCajaSeguimientoDTO> agregaAncleoCajaSegColor(ArrayList<AncleoCajaSeguimientoDTO> lista1, ArrayList<AncleoCajaSeguimientoDTO> lista2) {

        Iterator<AncleoCajaSeguimientoDTO> it = lista2.iterator();

        while (it.hasNext()) {
            AncleoCajaSeguimientoDTO obj = (AncleoCajaSeguimientoDTO) it.next();
            lista1.add(obj);
        }
        return lista1;
    }

    public ArrayList<AncleoCajaAperturaCierreDTO> agregaAncleoCajaApCiColor(ArrayList<AncleoCajaAperturaCierreDTO> lista1, ArrayList<AncleoCajaAperturaCierreDTO> lista2) {

        Iterator<AncleoCajaAperturaCierreDTO> it = lista2.iterator();

        while (it.hasNext()) {
            AncleoCajaAperturaCierreDTO obj = (AncleoCajaAperturaCierreDTO) it.next();
            lista1.add(obj);
        }
        return lista1;
    }

    //FIN METODOS PAR ORDENAR AncleoCajaAperturaCierreDTO ,
    /*::::::::::::::FIN MÉTODOS DE DETALLE SEGUIMIENTO ANCLEO::::::::::::::::::::::::*/
    @SuppressWarnings("rawtypes")
    public static void main(String[] args) {
        AncleoCajaBI ac = new AncleoCajaBI();
        //	public ArrayList <AncleoCajaDTO> arqueoGeneralSeguimientoPorcentaje(int pais, int nivelCeco, int numCeco)
        // 	public ArrayList <AncleoCajaDTO> arqueoGeneralSeguimientoPorcentajeDesglose(int pais, int nivelCeco, int numCeco)
        // 	public ArrayList <AncleoCajaDTO> arqueoGeneralAperturaPorcentaje(int pais, int nivelCeco, int numCeco)
        // 	public ArrayList <AncleoCajaDTO> arqueoGeneralAperturaDesglose(int pais, int nivelCeco, int numCeco)
        //	public ArrayList <AncleoCajaDTO> arqueoGeneralCierrePorcentaje(int pais, int nivelCeco, int numCeco)
        // 	public ArrayList <AncleoCajaDTO> arqueoGeneralCierreDesglose(int pais, int nivelCeco, int numCeco)

        /*
         ArrayList<AncleoCajaSeguimientoDTO> res = ac.arqueoGeneralSeguimientoPorcentaje(1,  3 , 236121);
         Iterator it = res.iterator();
         while (it.hasNext()){
         AncleoCajaSeguimientoDTO ob = (AncleoCajaSeguimientoDTO) it.next();
         logger.info("*********INICIO DESGLOSE*********");
         logger.info("TERRITORIO --------"+ob.getIdTerritorio());
         logger.info("ZONA --------"+ob.getIdZona());
         logger.info("REGION --------"+ob.getIdRegion());
         logger.info("SUCURSAL --------"+ob.getIdSucursal());
         logger.info("NOMBRECECO --------"+ob.getNombreCeco());
         logger.info("PORCENTAJE --------"+ob.getPorcentaje());
         logger.info("COLOR --------"+ob.getColor());
         logger.info("***********FIN DESGLOSE*******\n");

         AncleoCajaSeguimientoDTO
         AncleoCajaAperturaCierreDTO
         }

         ArrayList<AncleoCajaAperturaCierreDTO> res = ac.arqueoGeneralAperturaPorcentaje(1, 1, 236736);
         Iterator it = res.iterator();
         while (it.hasNext()){
         AncleoCajaAperturaCierreDTO ob = (AncleoCajaAperturaCierreDTO) it.next();
         logger.info("*********INICIO DESGLOSE*********");
         logger.info("TERRITORIO --------"+ob.getIdTerritorio());
         logger.info("ZONA --------"+ob.getIdZona());
         logger.info("REGION --------"+ob.getIdRegion());
         logger.info("SUCURSAL --------"+ob.getIdSucursal());
         logger.info("NOMBRECECO --------"+ob.getNombreCeco());
         logger.info("PORCENTAJE --------"+ob.getPorcentaje());
         logger.info("COLOR --------"+ob.getColor());
         logger.info("***********FIN DESGLOSE*******\n");

         AncleoCajaSeguimientoDTO
         AncleoCajaAperturaCierreDTO
         DetalleAncleoCajaAperturaCierreDTO

         public ArrayList <AncleoCajaAperturaCierreDTO> arqueoGeneralAperturaPorcentaje(int pais, int nivelCeco, int numCeco){

         }


         */
        //	 ArrayList<AncleoCajaSeguimientoDTO> res = ac.arqueoGeneralSeguimientoPorcentaje(1,  3 , 236121);
        ArrayList<AncleoCajaSeguimientoDTO> res = ac.arqueoGeneralSeguimientoPorcentaje(1, 3, 236080, "R");
        Iterator it = res.iterator();
        while (it.hasNext()) {
            AncleoCajaSeguimientoDTO ob = (AncleoCajaSeguimientoDTO) it.next();
            System.out.println("*********INICIO DESGLOSE*********");
            System.out.println("TERRITORIO --------" + ob.getIdTerritorio());
            System.out.println("ZONA --------" + ob.getIdZona());
            System.out.println("REGION --------" + ob.getIdRegion());
            System.out.println("SUCURSAL --------" + ob.getIdSucursal());
            System.out.println("NOMBRECECO --------" + ob.getNombreCeco());
            System.out.println("PORCENTAJE --------" + ob.getPorcentaje());
            System.out.println("COLOR --------" + ob.getColor());
            System.out.println("***********FIN DESGLOSE*******\n");
        }
    }
}
