package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ParametroDAO;
import com.gruposalinas.migestion.dao.UsuarioExternoDAO;
import com.gruposalinas.migestion.domain.ExternoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class UsuarioExternoBI {

    private static Logger logger = LogManager.getLogger(UsuarioExternoBI.class);

    @Autowired
    UsuarioExternoDAO usuarioExternoDAO;

    @Autowired
    ParametroDAO parametroDAO;

    List<ExternoDTO> listaUsuarios = null;
    List<ExternoDTO> listaUsuario = null;

    public List<ExternoDTO> obtieneUsuario() {

        try {
            listaUsuarios = usuarioExternoDAO.obtieneUsuarioExterno();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Usuario");

        }

        return listaUsuarios;
    }

    public List<ExternoDTO> obtieneUsuario(int idUsuario) {

        try {
            listaUsuario = usuarioExternoDAO.obtieneUsuarioExterno(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Usuario");

        }

        return listaUsuario;
    }

    public boolean insertaUsuario(ExternoDTO usuario) {

        boolean respuesta = false;

        try {
            respuesta = usuarioExternoDAO.insertaUsuarioExterno(usuario);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Usuario");

        }

        return respuesta;
    }

    public boolean actualizaUsuario(ExternoDTO usuario) {

        boolean respuesta = false;

        try {
            respuesta = usuarioExternoDAO.actualizaUsuarioExterno(usuario);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Usuario");

        }

        return respuesta;
    }

    public boolean eliminaUsuario(int idUsuario) {

        boolean respuesta = false;

        try {
            respuesta = usuarioExternoDAO.eliminaUsuarioExterno(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Usuario");

        }

        return respuesta;
    }

    public boolean depuraUsuarioExterno() {
        boolean respuesta = false;

        try {
            respuesta = usuarioExternoDAO.depuraUsuarioExterno();
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

}
