package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.EstandPuestoDAO;
import com.gruposalinas.migestion.domain.EstandPuestoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class EstandPuestoBI {

    private static Logger logger = LogManager.getLogger(EstandPuestoBI.class);

    private List<EstandPuestoDTO> listafila;
    private List<EstandPuestoDTO> listaplantilla;

    @Autowired
    EstandPuestoDAO estandPuestoDAO;

    public boolean elimina(int idPuesto) {
        boolean respuesta = false;

        try {
            respuesta = estandPuestoDAO.elimina(idPuesto);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<EstandPuestoDTO> obtieneDatos(String ceco) {

        try {
            listafila = estandPuestoDAO.obtieneDatos(ceco);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<EstandPuestoDTO> obtieneInfo() {

        try {
            listafila = estandPuestoDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Estandarizacion por puesto");

        }

        return listafila;
    }

    public boolean actualiza(EstandPuestoDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = estandPuestoDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

    //mueble
    public int insertaMueble(EstandPuestoDTO bean) {
        int respuesta = 0;

        try {
            respuesta = estandPuestoDAO.insertaMueble(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public List<EstandPuestoDTO> obtieneDatosMueble(String idPuesto) {

        try {
            listafila = estandPuestoDAO.obtieneDatosMueble(idPuesto);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<EstandPuestoDTO> obtieneInfoMueble() {

        try {
            listafila = estandPuestoDAO.obtieneInfoMueble();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Estandarizacion por puesto");

        }

        return listafila;
    }

//plantilla
    public List<EstandPuestoDTO> obtieneDatosPlantillaMueble(String ceco) {

        try {
            listaplantilla = estandPuestoDAO.obtieneDatosPlantillaMueble(ceco);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listaplantilla;
    }

    public boolean actualizaMueble(EstandPuestoDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = estandPuestoDAO.actualizaMueble(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
