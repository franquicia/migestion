package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.GeografiaDAOImpl;
import com.gruposalinas.migestion.domain.GeografiaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class GeografiaBI {

    private static Logger logger = LogManager.getLogger(GeografiaBI.class);

    private List<GeografiaDTO> listaGeografia;

    @Autowired
    GeografiaDAOImpl geoDAO;

    public List<GeografiaDTO> obtieneGeografia(String idCeco, String idRegion, String idZona, String idTerritorio) {

        try {
            listaGeografia = geoDAO.obtieneGeografia(idCeco, idRegion, idZona, idTerritorio);
        } catch (Exception e) {
            logger.info("No fue posible obtener la Geografia");

        }

        return listaGeografia;
    }

    public boolean insertaGeografia(GeografiaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = geoDAO.insertaGeografia(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar la Geografia");

        }

        return respuesta;
    }

    public boolean actualizaGeografia(GeografiaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = geoDAO.actualizaGeografia(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la Geografia");

        }

        return respuesta;
    }

    public boolean eliminaGeografia(String idCeco) {
        boolean respuesta = false;

        try {
            respuesta = geoDAO.eliminaGeografia(idCeco);
        } catch (Exception e) {
            logger.info("No fue posible eliminar la Geografia");

        }

        return respuesta;
    }

}
