package com.gruposalinas.migestion.business;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.dao.ReporteChecklistDAO;
import com.gruposalinas.migestion.domain.ReporteChecklistDTO;
 

public class ReporteChecklistBI {

    private static Logger logger = LogManager.getLogger(ReporteChecklistBI.class);

    Map<String, Object> listaCheck = null;
    private List<ReporteChecklistDTO> listaChecklist;

    @Autowired
    ReporteChecklistDAO reporteCheckDAO;

    public Map<String, Object> obtieneTotalCheck(int idUsuario, int idReporte, int idCheck) {

        try {
            listaCheck = reporteCheckDAO.obtieneTotalCheck(idUsuario, idReporte, idCheck);
        } catch (Exception e) {
            logger.info("No fue posible consultar el total de tareas realizadas");
             
        }

        return listaCheck;
    }

    public Map<String, Object> obtieneDetalleCheck(int idCeco, int idReporte) {

        try {
            listaCheck = reporteCheckDAO.obtieneDetalleCheck(idCeco, idReporte);
        } catch (Exception e) {
            logger.info("No fue posible consultar el detalle de tareas realizadas");
             
        }

        return listaCheck;
    }

    public Map<String, Object> obtieneSucursalesCheck(int idUsuario, int idReporte, String idCeco, int idPais, int idCanal, int bandera, int idCheck) {

        try {
            listaCheck = reporteCheckDAO.obtieneSucursalesCheck(idUsuario, idReporte, idCeco, idPais, idCanal, bandera, idCheck);
        } catch (Exception e) {
            logger.info("No fue posible obtener el registro de la Tarea");
             
        }

        return listaCheck;
    }

    public List<ReporteChecklistDTO> obtieneListaCheck(int idUsuario) {

        try {
            listaChecklist = reporteCheckDAO.obtieneListaCheck(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener los checklist");
             
        }

        return listaChecklist;
    }

    public List<ReporteChecklistDTO> obtieneFechaTermino(int idChecklist, int idUsuario, int idCeco) {

        try {
            listaChecklist = reporteCheckDAO.obtieneFechaTermino(idChecklist, idUsuario, idCeco);
        } catch (Exception e) {
            logger.info("No fue posible obtener la fecha termino");
             
        }

        return listaChecklist;
    }


}
