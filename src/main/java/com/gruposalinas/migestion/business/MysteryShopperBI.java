package com.gruposalinas.migestion.business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

public class MysteryShopperBI {
    //private static Logger logger = LogManager.getLogger(BlocBI.class);

    public String getVideos(int idCeco) {
        int mesActual;
        int anioActual;
        Date fecha = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        mesActual = c.get(Calendar.MONTH) + 1;
        anioActual = c.get(Calendar.YEAR);

        int mesAux = mesActual;
        int anioAux = anioActual;
        String json = "{\"listaVideos\":[";
        for (int i = 0; i < 12; i++) {

            //System.out.println("MES SEL: " + mesAux);
            //System.out.println("A�O SEL: " + anioAux);
            json += "{";
            json += "\"mes\":" + mesAux + ",";
            json += "\"anio\":" + anioAux + ",";
            json += "\"urlVideo\":" + "[";
            System.out.println((Integer.parseInt((idCeco + "").substring(2))));
            System.out.println("CECO: " + idCeco);

            ArrayList<String> listaTmp = getUrlVideo(Integer.parseInt((idCeco + "").substring(2)) + "", mesAux, anioAux);
            for (int j = 0; j < listaTmp.size(); j++) {
                if (j == listaTmp.size() - 1) {
                    json += "\"" + listaTmp.get(j) + "\"";
                } else {
                    json += "\"" + listaTmp.get(j) + "\",";
                }
            }
            json += "]";
            if (i == 11) {
                json += "}";

            } else {
                json += "},";

            }

            //System.out.println("LISTA VIDEOS: " + getUrlVideo((idCeco + "").substring(3), mesAux, anioAux));
            mesAux--;
            if (mesAux == 0) {
                mesAux = 12;
                anioAux--;
            }
        }
        json += "] }";

        System.out.println("JSON:\n" + json);

        return json;
    }

    @SuppressWarnings("unused")
    public ArrayList<String> getUrlVideo(String suc, int mes, int anio) {

        ArrayList<String> listaVideo = new ArrayList<String>();

        String json = "{" + "\"noTienda\":" + suc + ", " + "\"mes\":" + (mes < 10 ? "0" + mes : mes) + ", " + "\"ano\":"
                + anio + "}";

        System.out.println("JSON:\n" + json);
        URL url;
        HttpURLConnection con;
        OutputStream os = null;
        BufferedReader rd = null;
        int statusCode;

        try {

            /*System.setProperty("java.net.useSystemProxies", "true");
             System.setProperty("http.proxyHost", "10.50.8.20");
             System.setProperty("http.proxyPort", "8080");
             // System.setProperty("http.proxyUser", "B196228");
             // System.setProperty("http.proxyPassword", "Bancoazteca2345");
             System.setProperty("http.proxySet", "true");

             System.setProperty("https.proxyHost", "10.50.8.20");
             System.setProperty("https.proxyPort", "8080");
             // System.setProperty("https.proxyUser", "B196228");
             // System.setProperty("https.proxyPassword", "Bancoazteca2345");
             System.setProperty("https.proxySet", "true");*/
            url = new URL("http://34.197.112.187/Tienda/webresources/generic/getDataTienda");
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            // con.setRequestProperty("Content-Type", "text/html");
            // con.setReadTimeout(60000);

            con.setRequestProperty("Content-Type", "application/json");
            os = con.getOutputStream();//Cerrar Stream
            os.write(json.getBytes());

            rd = new BufferedReader(new InputStreamReader(con.getInputStream()));//Cerrar Stream
            String line;
            String aux = null;
            while ((line = rd.readLine()) != null) {
                //System.out.println(line);
                aux = line;
            }
            JSONObject j = new JSONObject(aux);

            if (aux != null) {//Fortify NULL
                if (aux.contains("\"url\"")) {
                    String listaUrl = j.getString("url");
                    listaUrl = listaUrl.replaceAll("\\[", "\"");
                    listaUrl = listaUrl.replaceAll("\\]", "\",");

                    //System.out.println("[" + listaUrl.substring(0, listaUrl.length()-1) + "]");
                    JSONArray urlArray = new JSONArray("[" + listaUrl.substring(0, listaUrl.length() - 1) + "]");

                    for (int i = 0; i < urlArray.length(); i++) {
                        listaVideo.add(urlArray.getString(i));
                    }

                }
            }

        } catch (Exception e) {
            System.out.println("ERROR= " + e);
            // e.printStackTrace();
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {

            }
            try {
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {

            }
        }

        return listaVideo;
    }
}
