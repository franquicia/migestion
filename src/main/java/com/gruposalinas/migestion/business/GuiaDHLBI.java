package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.GuiaDHLDAO;
import com.gruposalinas.migestion.domain.GuiaDHLDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class GuiaDHLBI {

    private static Logger logger = LogManager.getLogger(GuiaDHLBI.class);

    private List<GuiaDHLDTO> listafila;

    @Autowired
    GuiaDHLDAO guiaDHLDAO;

    public boolean elimina(String idGuia) {
        boolean respuesta = false;

        try {
            respuesta = guiaDHLDAO.elimina(idGuia);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public boolean eliminaEvi(String idEvid) {
        boolean respuesta = false;

        try {
            respuesta = guiaDHLDAO.eliminaEvi(idEvid);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }


    public List<GuiaDHLDTO> obtieneInfo() {

        try {
            listafila = guiaDHLDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla guias");

        }

        return listafila;
    }

    public boolean actualiza(GuiaDHLDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = guiaDHLDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

    public boolean actualizaEvid(GuiaDHLDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = guiaDHLDAO.actualizaEvid(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
