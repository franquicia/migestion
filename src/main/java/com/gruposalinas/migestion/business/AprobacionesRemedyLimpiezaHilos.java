package com.gruposalinas.migestion.business;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.migestion.clientelimpieza.RemedyStub;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Aprobacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Authentication;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarAprobaciones;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarAprobacionesResponse;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsCAprobacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub._Accion;

public class AprobacionesRemedyLimpiezaHilos extends Thread {

    private Logger logger = LogManager.getLogger(AprobacionesRemedyLimpiezaHilos.class);

    private int idFolio;
    private int opcion;
    private boolean bandera;
    private Aprobacion respuesta;

    public Aprobacion getRespuesta() {
        return this.respuesta;
    }

    public int getIdFolio() {
        return this.idFolio;
    }

    public AprobacionesRemedyLimpiezaHilos(int idFolio, int opcion) {
        this.idFolio = idFolio;
        this.opcion = opcion;
    }

    @Override
    public void run() {

        consultarAprobaciones(this.idFolio, this.opcion);

    }

    public void consultarAprobaciones(int idIncidente, int opcion) {
        Aprobacion result = null;
        try {

            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(autenticatiosRemedy);

            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcion);

            aprobacionesRequest.setTipo(auxB);

            RemedyStub consulta = new RemedyStub();

            ConsultarAprobacionesResponse response = new RemedyStub.ConsultarAprobacionesResponse();

            response = consulta.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            //logger.info("MENSAJE aprobaciones: " + aprobacionesResult.getMensaje());
            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            logger.info("AprobacionesRemedyHilos||consultarAprobaciones||Ocurrio algo al consultar la aprobacion el incidente: " + e.getMessage());
            this.respuesta = null;
        }

        this.respuesta = result;
    }
}
