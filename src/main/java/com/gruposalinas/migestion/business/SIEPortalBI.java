package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.util.Ambientes;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class SIEPortalBI {

    private final String URL_DESARROLLO = "http://10.54.24.210:7082/WSFenix/Login/portalSIE/";
    private final String URL_PRODUCCION = "http://10.54.24.118:7082/WSFenix/Login/portalSIE/";
    private final String URL_PRODUCCION_FRQ = "200.38.122.186";
    private final String URL_DESARROLLO_FRQ = "10.53.33.82";

    private String getUrlWS() {
        String urlWs = null;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            //url produccion
            urlWs = URL_PRODUCCION;
        } else {
            //Desarrollo
            urlWs = URL_DESARROLLO;
        }

        return urlWs;
    }

    private String getUrlFrq() {
        String urlWs = null;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            //url produccion
            urlWs = URL_PRODUCCION_FRQ;
        } else {
            //Desarrollo
            urlWs = URL_DESARROLLO_FRQ;
        }

        return urlWs;
    }

    private String obtenerUrl(String idUsuario) {

        String urlSIE = null;

        try {
            URL url = new URL(getUrlWS() + idUsuario);
            HttpURLConnection conn = null;
            BufferedReader rd = null;

            //Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 8080));
            //if(GTNConstantes.PRODUCCION) {
            //en caso de que el war sea productivo se ejecutan las notificaciones con proxy
            //	conn = (HttpURLConnection) url.openConnection(proxy);
            //}else {
            //en caso de que el war no sea productivo se ejecutan las notificaciones sin proxy
            conn = (HttpURLConnection) url.openConnection();

            //}
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            // conn.setRequestProperty("Content-Type", "text/html");

            System.out.println("CODIGO: " + conn.getResponseCode());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            // rd=new BufferedInputStream(conn.getInputStream());

            StringBuffer response = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
            //System.out.println(response.toString());

            StringReader stringReader = new StringReader(response.toString());
            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            urlSIE = rootNode.getText();

            //System.out.println(urlTmp);
        } catch (Exception e) {
            e.printStackTrace();
            urlSIE = null;
        }

        return urlSIE;

    }

    public String getUrlsJson(String idUsuario) {

        String urlGeneral = obtenerUrl(idUsuario);
        String urls
                = //url para el portal de banco
                "{"
                + "  \"urlSIE\": ["
                + "    {"
                + "      \"url\": \"" + urlGeneral + "&RolPortal=460&trpsie=2" + "\","
                + "      \"tipo\": 0"
                + "    },"
                + //url para el portal de elektra
                "    {"
                + "      \"url\": \"" + urlGeneral + "&RolPortal=450&trpsie=2" + "\","
                + "      \"tipo\": 1"
                + "    }"
                + "  ]"
                + "}";

        urls = urls.replaceAll("10.54.24.119", getUrlFrq());

        return urls;

    }

    /*public static void main(String args[]) {
     SIEPortalBI sie=new SIEPortalBI();

     System.out.println(sie.getUrlsJson("196228"));

     }*/
}
