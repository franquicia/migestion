package com.gruposalinas.migestion.business;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.cliente.Service1Stub;
import com.gruposalinas.migestion.cliente.Service1Stub.Adjuntos;
import com.gruposalinas.migestion.cliente.Service1Stub.EmployeeBySucursal;
import com.gruposalinas.migestion.cliente.Service1Stub.Incident;
import com.gruposalinas.migestion.dao.OperacionDAO;
import com.gruposalinas.migestion.dao.ProductoDAO;
import com.gruposalinas.migestion.domain.TipificacionDTO;
import java.util.List;
import javax.activation.DataHandler;
import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import sun.misc.BASE64Decoder;

public class IncidentesBI {

    private Logger logger = LogManager.getLogger(IncidentesBI.class);

    @Autowired
    private ProductoDAO productoDAO;

    @Autowired
    private OperacionDAO operacionDAO;

    public JsonObject creaIncidente(String json) {

        JsonObject respuesta = new JsonObject();

        try {

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(json).getAsJsonObject();

            JsonObject parameters = jsonObject.getAsJsonObject("parametros");

            //String idIncid_aux = parameters.get("idIncidente").getAsString();
            if (parameters.get("idIncidente").getAsString().compareTo("") == 0) {
                String sucursal = parameters.getAsJsonObject("EmployeeBySucursal").get("sucursal").getAsString();
                String empleado = parameters.getAsJsonObject("EmployeeBySucursal").get("empleado").getAsString();
                String descripcion = parameters.get("descripcion").getAsString();
                String resumen = parameters.get("resumen").getAsString();
                String N1Operacion = parameters.get("N1Operacion").getAsString();
                String N2Operacion = parameters.get("N2Operacion").getAsString();
                String N3Operacion = parameters.get("N3Operacion").getAsString();
                String N1Producto = parameters.get("N1Producto").getAsString();
                String N2Producto = parameters.get("N2Producto").getAsString();
                String N3Producto = parameters.get("N3Producto").getAsString();

                boolean existeAdjunto = parameters.get("existeAdjunto").getAsBoolean();

                String nombreArchivo = "";
                String archivobytes = "";
                int numBytes = 0;

                if (existeAdjunto) {
                    nombreArchivo = parameters.get("nombreArchivo").getAsString();
                    archivobytes = parameters.get("archivoBytes").getAsString();
                    numBytes = parameters.get("numBytes").getAsInt();
                }
                BASE64Decoder decoder = new BASE64Decoder();

                byte[] aux2 = decoder.decodeBuffer(archivobytes);

                /*
                 Base64 base64 = new Base64();

                 byte[] aux3=base64.decode(archivobytes);

                 //byte[] aux3=javax.xml.bind.DatatypeConverter.parseBase64Binary(archivobytes);

                 //logger.info("Base64: "+archivobytes);
                 FileOutputStream fileOutput = new FileOutputStream("E:\\"+nombreArchivo);
                 BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);
                 // se escribe el archivo decodificado
                 bufferOutput.write(aux3);
                 bufferOutput.close();
                 */
                //byte[] objAsBytes = convertToBytes(archivo_aux);
                //logger.info("Archivo bytes: "+Arrays.toString(aux2));
                //logger.info("Arreglo bytes: "+b.toString());
                EmployeeBySucursal employeeBySucursal = new EmployeeBySucursal();
                employeeBySucursal.setSucursal(sucursal);
                employeeBySucursal.setCorporateID(empleado);

                Incident incidenteRequest = new Incident();
                incidenteRequest.setEmployeeBySucursal(employeeBySucursal);
                incidenteRequest.setDescripcion(descripcion);
                incidenteRequest.setResumen(resumen);
                incidenteRequest.setN1Operacion(N1Operacion);
                incidenteRequest.setN2Operacion(N2Operacion);
                incidenteRequest.setN3Operacion(N3Operacion);
                incidenteRequest.setN1Producto(N1Producto);
                incidenteRequest.setN2Producto(N2Producto);
                incidenteRequest.setN3Producto(N3Producto);

                Service1Stub incidente = new Service1Stub();
                Service1Stub.CreateIncident request = new Service1Stub.CreateIncident();

                Service1Stub.CreateIncidentResponse response = new Service1Stub.CreateIncidentResponse();
                Service1Stub.CreateIncidentResult resultIncident = new Service1Stub.CreateIncidentResult();
                Service1Stub.Result result = new Service1Stub.Result();
                request.setParameter(incidenteRequest);

                response = incidente.createIncident(request);
                resultIncident = response.getCreateIncidentResult();
                result = resultIncident.getResult();

                if (result.getSuccessfulOperation()) {

                    Incident incidentRespuesta = resultIncident.getIncident();

                    respuesta.addProperty("result", true);
                    respuesta.addProperty("idIncidente", incidentRespuesta.getIncidentID());

                    if (existeAdjunto) {
                        if (adjuntos(incidentRespuesta.getIncidentID(), nombreArchivo, aux2, numBytes)) {
                            respuesta.addProperty("resultAdjunto", true);
                        } else {
                            respuesta.addProperty("resultAdjunto", false);
                        }
                    } else {
                        respuesta.addProperty("resultAdjunto", true);
                    }
                    EmployeeBySucursal empleadoSoporte = incidentRespuesta.getEmployeeBySucursal();

                    JsonElement jelement = new JsonParser().parse(new Gson().toJson(empleadoSoporte));
                    JsonObject jsonEmpleadoSoporte = jelement.getAsJsonObject();
                    respuesta.add("empleadoAsignado", jsonEmpleadoSoporte);

                } else {
                    respuesta.addProperty("result", false);
                    respuesta.addProperty("resultAdjunto", false);
                    respuesta.addProperty("idIncidente", (String) null);

                }
            } else {
                String idIncid_aux = parameters.get("idIncidente").getAsString();
                String nombreArchivo = parameters.get("nombreArchivo").getAsString();
                String archivobytes = parameters.get("archivoBytes").getAsString();
                int numBytes = parameters.get("numBytes").getAsInt();
                //JsonObject jsonEmpleadoSoporte =parameters.get("empleadoAsignado").getAsJsonObject() ;
                //respuesta.add("empleadoAsignado", jsonEmpleadoSoporte);

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] aux2 = decoder.decodeBuffer(parameters.get("archivoBytes").getAsString());

                //byte[] b = archivobytes.getBytes(Charset.forName("UTF-8"));
                respuesta.addProperty("result", true);
                respuesta.addProperty("idIncidente", idIncid_aux);
                if (adjuntos(idIncid_aux, nombreArchivo, aux2, numBytes)) {
                    respuesta.addProperty("resultAdjunto", true);
                } else {
                    respuesta.addProperty("resultAdjunto", false);
                }

            }

        } catch (Exception e) {

            respuesta.addProperty("result", false);
            respuesta.addProperty("idIncidente", (String) null);

            logger.info("Ocurrio algo crear el incidente");

        }

        logger.info("Respuesta Create Incident ..... " + respuesta.toString());
        return respuesta;

    }

    public boolean adjuntos(String idIncidente, String nombreArchivo, byte[] archivobytes, int numBytes) {

        boolean respuesta = false;

        try {
            logger.info("Paso");
            ByteArrayDataSource rawData = new ByteArrayDataSource(archivobytes);
            DataHandler data = new DataHandler(rawData);

            //idIncidente="INC000006303244";
            Adjuntos adjuntoRequest = new Adjuntos();
            adjuntoRequest.setNoIncidente(idIncidente);
            adjuntoRequest.setNombreArchivo(nombreArchivo);
            adjuntoRequest.setArchivobytes(data);
            adjuntoRequest.setNumBytesRead(numBytes);

            Service1Stub incidente = new Service1Stub();
            Service1Stub.Adjuntos request = new Service1Stub.Adjuntos();
            Service1Stub.AdjuntosResponse response = new Service1Stub.AdjuntosResponse();
            Service1Stub.Result result = new Service1Stub.Result();

            response = incidente.adjuntos(adjuntoRequest);
            String adjuntoResult = response.getAdjuntosResult();
            //String adjuntoResult=null;

            if (adjuntoResult == null) {
                respuesta = false;
                logger.info("Respuesta false'" + adjuntoResult + "'");
            } else {
                respuesta = true;
                logger.info("Respuesta true '" + adjuntoResult + "'");
            }

        } catch (Exception e) {

            respuesta = false;

            logger.info("Ocurrio algo al adjuntar archivo al incidente");

        }

        logger.info("Respuesta Adjuntos ..... " + respuesta);
        return respuesta;

    }

    public JsonObject getTipificacion() {

        List<TipificacionDTO> lista = null;
        JsonObject tipificaciones = new JsonObject();

        Gson gson = new Gson();
        JsonArray jsonAuxiliar = null;
        String catalogo = "";
        JsonParser parser = new JsonParser();
        try {
            lista = operacionDAO.consulta();
            catalogo = gson.toJson(lista);
            jsonAuxiliar = parser.parse(catalogo).getAsJsonArray();

            tipificaciones.add("operaciones", jsonAuxiliar);

            lista = productoDAO.consulta();
            catalogo = gson.toJson(lista);
            jsonAuxiliar = parser.parse(catalogo).getAsJsonArray();

            tipificaciones.add("productos", jsonAuxiliar);

            logger.info(tipificaciones.toString());
        } catch (Exception e) {

            logger.info("Ocurrio un problema " + e);

        }

        return tipificaciones;

    }

    public String getDirectores() {

        String url = "";

        return "";
    }
}
