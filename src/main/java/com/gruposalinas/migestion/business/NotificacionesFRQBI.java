package com.gruposalinas.migestion.business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.dao.ObtieneTokenDAO;
import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import com.gruposalinas.migestion.util.Ambientes;

public class NotificacionesFRQBI {

    @Autowired
    private ObtieneTokenDAO obtieneTokenDAO;
    @Autowired
    private ContingenciaBI contingenciaBI;

    public int enviarNotificaciones(int ceco, String titulo, String mensaje) {
        int contadorNotificaciones = 0;
        try {

            try {
                //Se cambian los puestos
                contingenciaBI.CambiaPuesto(ceco);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            List<UsuarioInfoTokenDTO> listaUsr = obtieneTokenDAO.consultaUsuarios(ceco);

            if (listaUsr != null) {
                for (UsuarioInfoTokenDTO usr : listaUsr) {

                    List<UsuarioInfoTokenDTO> tokensUsr = obtieneTokenDAO.consultaTokens(usr.getIdUsuario());
                    if (tokensUsr != null) {
                        for (UsuarioInfoTokenDTO tokenUsr : tokensUsr) {
                            String tokenTmp = tokenUsr.getToken();
                            if (tokenTmp != null) {
                                if (!tokenTmp.equalsIgnoreCase("null") && !tokenTmp.equalsIgnoreCase("-")) {

                                    boolean envio = enviarNotificacion(tokenUsr.getToken(), titulo, mensaje);
                                    if (envio) {
                                        contadorNotificaciones++;
                                    }

                                }
                            }
                        }

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            contadorNotificaciones = 0;
        }

        return contadorNotificaciones;

    }

    public boolean enviarNotificacion(String token, String titulo, String mensaje) {

        String apiKey = "AIzaSyCN-msi853al5iJrm0ihXErHXj9k2SYOzI";
        String FCM_SERVICE = "https://fcm.googleapis.com/fcm/send";
        String json = "";
        JsonObject jsonObject = null;
        boolean envio = false;

        json = "{ " + "\"notification\":{ " + "\"sound\": \"OverrideSound\" " + ",\"title\":\"" + titulo + "\", "
                + "\"text\":\"" + mensaje + "\" " + "}, " + "\"registration_ids\":[\"" + token + "\"] " + "}";
        System.out.println("JSON: " + json);

        try {
            URL url = new URL(FCM_SERVICE);
            HttpURLConnection conn = null;
            OutputStream os = null;
            BufferedReader rd = null;

            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 8080));

            if (Ambientes.AMBIENTE.value().equals(Ambientes.DESARROLLO)) {
                //en caso de que el war sea productivo se ejecutan las notificaciones con proxy
                conn = (HttpURLConnection) url.openConnection(proxy);

            } else {
                //en caso de que el war no sea productivo se ejecutan las notificaciones sin proxy
                conn = (HttpURLConnection) url.openConnection();

            }
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);
            // conn.setRequestProperty("Content-Type", "text/html");

            os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));

            System.out.println("CODIGO: " + conn.getResponseCode());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            // rd=new BufferedInputStream(conn.getInputStream());

            StringBuffer response = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                response.append(line);
            }

            System.out.println(response.toString());

            JsonParser parser = new JsonParser();
            jsonObject = parser.parse(response.toString()).getAsJsonObject();

            int totSucces = jsonObject.get("success").getAsInt();
            int totFail = jsonObject.get("failure").getAsInt();

            if (totSucces > 0) {
                envio = true;
            }

            System.out.println("Success: " + totSucces);
            System.out.println("Success: " + totFail);

        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());

            System.out.println("ERROR: " + e);

        }

        return envio;

    }

}
