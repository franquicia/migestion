package com.gruposalinas.migestion.business;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.io.output.ByteArrayOutputStream;

import javaxt.io.Image;

public class ConvertImageExifBI {

    public boolean startConvert(String path) {

        File entrada = new File(path);

        boolean res = false;

        if (entrada.exists()) {
            res = copyExifOrientation(entrada);
        }

        return res;

    }

    /**
     * This method will copy the Exif "orientation" information to the resulting
     * image, if the original image contains this data too.
     *
     * @param sourceImage The source image.
     * @param result The original result.
     * @return The new result containing the Exif orientation.
     */
    private boolean copyExifOrientation(File sourceImage) {

        boolean respuesta = false;
        try {
            ImageMetadata imageMetadata = Imaging.getMetadata(sourceImage);
            /*if (imageMetadata == null) {
                return result.;
            }*/
            List<? extends ImageMetadata.ImageMetadataItem> metadataItems = imageMetadata
                    .getItems();
            for (ImageMetadata.ImageMetadataItem metadataItem : metadataItems) {
                if (metadataItem instanceof TiffImageMetadata.TiffMetadataItem) {
                    TiffField tiffField = ((TiffImageMetadata.TiffMetadataItem) metadataItem)
                            .getTiffField();
                    if (!tiffField.getTagInfo().equals(TiffTagConstants.TIFF_TAG_ORIENTATION)) {
                        continue;
                    }
                    Object orientationValue = tiffField.getValue();
                    if (orientationValue == null) {
                        break;
                    }
                    TiffOutputSet outputSet = new TiffOutputSet();
                    TiffOutputDirectory outputDirectory = outputSet.getOrCreateRootDirectory();
                    outputDirectory.add(TiffTagConstants.TIFF_TAG_ORIENTATION,
                            ((Number) 1).shortValue());

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

                    Image image = new Image(sourceImage.getAbsolutePath());

                    switch (((Number) orientationValue).shortValue()) {
                        case 1:
                            //desc = "Top, left side (Horizontal / normal)";
                            break;
                        case 2:
                            //desc = "Top, right side (Mirror horizontal)";
                            break;
                        case 3:
                            //desc = "Bottom, right side (Rotate 180)";
                            image.rotate(180);
                            break;
                        case 4:
                            //desc = "Bottom, left side (Mirror vertical)";
                            break;
                        case 5:
                            //desc = "Left side, top (Mirror horizontal and rotate 270 CW)";
                            break;
                        case 6:
                            image.rotate(90);
                            //desc = "Right side, top (Rotate 90 CW)"; b
                            break;
                        case 7:
                            // desc = "Right side, bottom (Mirror horizontal and rotate 90 CW)";
                            break;
                        case 8:
                            image.rotate(270);
                            //desc = "Left side, bottom (Rotate 270 CW)";
                            break;
                    }

                    image.saveAs(sourceImage.getAbsolutePath());

                    new ExifRewriter().updateExifMetadataLossy(sourceImage, outputStream, outputSet);

                    boolean isCreateFile = createImageFile(outputStream.toByteArray(), sourceImage.getAbsolutePath());

                    if (isCreateFile) {
                        respuesta = true;
                    } else {
                        respuesta = false;
                    }

                    break;

                }
            }
        } catch (Exception e) {

            respuesta = false;
        }
        return respuesta;
    }

    /**
     * Creates an image file and save it to the cache directory
     *
     * @param data Image binary data
     * @param filename Destination path of the given file
     * @return If the creation of image file was successful, then returns
     * <code>true</code>, else <code>false</code>
     */
    private boolean createImageFile(byte[] data, String filename) {
        FileOutputStream fos = null;

        boolean respuesta = false;

        try {
            File file = new File(filename);
            if (!file.exists()) {
                new File(file.getAbsolutePath()).mkdirs();
            }
            fos = new FileOutputStream(file);
            fos.write(data);

            respuesta = true;

        } catch (Exception e) {
            //LOGGER.error("Error writing image to file: {} ", filename, e);

            respuesta = false;

        } finally {
            //IOHelper.close(fos);

            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception ex) {

                }
            }

        }
        return respuesta;
    }

}
