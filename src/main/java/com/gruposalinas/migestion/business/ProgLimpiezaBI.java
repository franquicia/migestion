package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ProgLimpiezaDAO;
import com.gruposalinas.migestion.domain.ProgLimpiezaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProgLimpiezaBI {

    private static Logger logger = LogManager.getLogger(ProgLimpiezaBI.class);

    private List<ProgLimpiezaDTO> listafila;

    @Autowired
    ProgLimpiezaDAO progLimpiezaDAO;


    public boolean elimina(int idProg) {
        boolean respuesta = false;

        try {
            respuesta = progLimpiezaDAO.elimina(idProg);
        } catch (Exception e) {
            logger.info("No fue posible eliminar de la tabla Programa de limpieza");

        }

        return respuesta;
    }


    public List<ProgLimpiezaDTO> obtieneInfo() {

        try {
            listafila = progLimpiezaDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Programa de limpieza");

        }

        return listafila;
    }

    public boolean actualiza(ProgLimpiezaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = progLimpiezaDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar  datos de la tabla Programa de limpieza");

        }

        return respuesta;
    }

    public int insertaPro(ProgLimpiezaDTO bean) {
        int respuesta = 0;

        try {
            respuesta = progLimpiezaDAO.insertaPro(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar de la tabla Programa de limpieza");

        }

        return respuesta;
    }

    public List<ProgLimpiezaDTO> obtieneInfoPro() {

        try {
            listafila = progLimpiezaDAO.obtieneInfoPro();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Programa de limpieza");

        }

        return listafila;
    }

    public boolean actualizaPro(ProgLimpiezaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = progLimpiezaDAO.actualizaPro(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar  datos de la tabla Programa de limpieza");

        }

        return respuesta;
    }
}
