/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.IndicadoresIPNDAOImpl;
import com.gruposalinas.migestion.domain.IndicadorIPNClienteDTO;
import com.gruposalinas.migestion.domain.IndicadorIPNEquipoDTO;
import java.io.BufferedReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author cescobarh
 */
public class IndicadoresIPNBI {

    public static final String SEPARATOR = ";";
    public static final String QUOTE = "\"";

    private static final Logger logger = LogManager.getLogger(IndicadoresIPNBI.class);

    @Autowired
    private IndicadoresIPNDAOImpl indicadoresIPNImpl;

    public List<IndicadorIPNClienteDTO> getIndicadoresIPNCliente(String idCeco, int top) {
        try {
            return indicadoresIPNImpl.getIndicadoresIPNCliente(idCeco, top);
        } catch (Exception ex) {
            logger.error("No fue posible consultar idCeco = " + idCeco + ", top = " + top, ex);
            return null;
        }
    }

    public List<IndicadorIPNEquipoDTO> getIndicadoresIPNEquipo(String idCeco, int top) {
        try {
            return indicadoresIPNImpl.getIndicadoresIPNEquipo(idCeco, top);
        } catch (Exception ex) {
            logger.error("No fue posible consultar idCeco = " + idCeco + ", top = " + top, ex);
            return null;
        }
    }

    public int getDataIndicadorIPN(BufferedReader br) {
        //     BufferedReader br = null;
        System.out.println("222");
        try {
            // br =new BufferedReader(new FileReader("e:/Users/kramireza/Documents/LAY_OUT_SUCS_IPN_EQUIPO_CSV.csv"));
            //  br =new BufferedReader(new FileReader("e:/Users/kramireza/Documents/LAY_OUT_IPN_CLIENTE_CSV_Q1_2018.csv"));

            String line = br.readLine();

            List<IndicadorIPNEquipoDTO> records = new ArrayList<IndicadorIPNEquipoDTO>();
            List<IndicadorIPNClienteDTO> datosCliente = new ArrayList<IndicadorIPNClienteDTO>();

            if (line.contains("evaluaciones") && line.contains("NOMBRE DE SUCURSAL")) {
                //CARGA DATOS DE CLIENTE
                while (null != line) {
                    if (!line.contains("ID Territorio")) {
                        List<String> items = Arrays.asList(line.split("\\s*,\\s*"));
                        IndicadorIPNClienteDTO cliente = new IndicadorIPNClienteDTO();
                        cliente.setFiTerritorio((items.get(0).equals("#N/A")) ? 0 : Integer.valueOf(items.get(0)));
                        cliente.setFiZona((items.get(1).equals("#N/A")) ? 0 : Integer.valueOf(items.get(1)));
                        cliente.setFiRegion((items.get(2).equals("#N/A")) ? 0 : Integer.valueOf(items.get(2)));
                        cliente.setFiSucursal(Integer.valueOf(items.get(3)));
                        cliente.setFiNumEconomico(Integer.valueOf(items.get(4)));
                        cliente.setFcSucursal(items.get(5));
                        cliente.setFiNumEval(Integer.valueOf(items.get(6)));
                        cliente.setFiPromotor((items.get(7).equals("")) ? 0 : Integer.valueOf(items.get(7)));
                        cliente.setFiPasivo((items.get(8).equals("")) ? 0 : Integer.valueOf(items.get(8)));
                        cliente.setFiDetractivo((items.get(9).equals("")) ? 0 : Integer.valueOf(items.get(9)));
                        cliente.setFiIPN(new BigDecimal(items.get(10)));
                        cliente.setFiQ(Integer.valueOf(items.get(11)));
                        cliente.setFiAnio(Integer.valueOf(items.get(12)));

                        datosCliente.add(cliente);
                    }
                    line = br.readLine();
                }
                return indicadoresIPNImpl.getDatoIndicadorIPNCliente(datosCliente);

            } else {
                //CARGA DATOS DE EQUIPO
                while (null != line) {
                    if (!line.contains("ID Territorio")) {
                        List<String> items = Arrays.asList(line.split("\\s*,\\s*"));
                        IndicadorIPNEquipoDTO ip = new IndicadorIPNEquipoDTO();
                        ip.setFiTerritorio(Integer.valueOf(items.get(0)));
                        ip.setFiZona(Integer.valueOf(items.get(1)));
                        ip.setFiRegion(Integer.valueOf(items.get(2)));
                        ip.setFiSucursal(Integer.valueOf(items.get(3)));
                        ip.setFiNumEconomico(Integer.valueOf(items.get(4)));
                        ip.setFcSucursal(items.get(5));
                        ip.setFiDetractivo(Integer.valueOf(items.get(6)));
                        ip.setFiPasivo(Integer.valueOf(items.get(7)));
                        ip.setFiPromotor(Integer.valueOf(items.get(8)));
                        ip.setFiTotal(Integer.valueOf(items.get(9)));
                        ip.setFiIPN(new BigDecimal(items.get(10)));
                        ip.setFiQ(Integer.valueOf(items.get(11)));
                        ip.setFiAnio(Integer.valueOf(items.get(12)));
                        records.add(ip);
                    }
                    line = br.readLine();
                }
                return indicadoresIPNImpl.getDatoIndicadorIPNEquipo(records);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("No fue posible consultar", ex);
            return 0;
        }
    }

}
