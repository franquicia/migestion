package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.CatalogoVistaDAO;
import com.gruposalinas.migestion.domain.CatalogoVistaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CatalogoVistaBI {

    private static Logger logger = LogManager.getLogger(CatalogoVistaBI.class);

    @Autowired
    CatalogoVistaDAO catalogoVistaDAO;

    List<CatalogoVistaDTO> listaCatalogoVista = null;

    public List<CatalogoVistaDTO> buscaCatalogoVista(int idCatVista) {

        try {
            listaCatalogoVista = catalogoVistaDAO.buscaCatalogoVista(idCatVista);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Perfil");

        }

        return listaCatalogoVista;
    }

    public List<CatalogoVistaDTO> buscaCatalogoVistaParam(String idCatVista, String descripcion, String moduloPadre) {

        try {
            listaCatalogoVista = catalogoVistaDAO.buscaCatalogoVistaParam(idCatVista, descripcion, moduloPadre);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Perfil");

        }

        return listaCatalogoVista;
    }

    public boolean insertaCatalogoVista(CatalogoVistaDTO vista) {

        boolean respuesta = false;

        try {
            respuesta = catalogoVistaDAO.insertaCatalogoVista(vista);
        } catch (Exception e) {
            logger.info("No fue posible insertar la vista");

        }

        return respuesta;
    }

    public boolean actualizaCatalogoVista(CatalogoVistaDTO vista) {

        boolean respuesta = false;

        try {
            respuesta = catalogoVistaDAO.actualizaCatalogoVista(vista);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la vista");

        }

        return respuesta;
    }

    public boolean eliminaCatalogoVistal(int idCatVista) {

        boolean respuesta = false;

        try {
            respuesta = catalogoVistaDAO.eliminaCatalogoVista(idCatVista);
        } catch (Exception e) {
            logger.info("No fue posible eliminar la vista");

        }

        return respuesta;
    }
}
