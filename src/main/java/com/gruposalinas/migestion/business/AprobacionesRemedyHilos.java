package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.cliente.RemedyStub;
import com.gruposalinas.migestion.cliente.RemedyStub.Aprobacion;
import com.gruposalinas.migestion.cliente.RemedyStub.Authentication;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultarAprobaciones;
import com.gruposalinas.migestion.cliente.RemedyStub.ConsultarAprobacionesResponse;
import com.gruposalinas.migestion.cliente.RemedyStub.RsCAprobacion;
import com.gruposalinas.migestion.cliente.RemedyStub._Accion;
import com.gruposalinas.migestion.cliente.RemedyStub._Tipo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AprobacionesRemedyHilos extends Thread {

    private Logger logger = LogManager.getLogger(AprobacionesRemedyHilos.class);

    private int idFolio;
    private int opcion;
    private int opcApr;
    private boolean bandera;
    private Aprobacion respuesta;

    public Aprobacion getRespuesta() {
        return this.respuesta;
    }

    public int getIdFolio() {
        return this.idFolio;
    }

    public AprobacionesRemedyHilos(int idFolio, int opcion, int opcApr) {
        this.idFolio = idFolio;
        this.opcion = opcion;
        this.opcApr = opcApr;
    }

    @Override
    public void run() {

        if (this.opcion == 1) {
            consultarAprobaciones(this.idFolio, this.opcApr);
        }

    }

    public void consultarAprobaciones(int idIncidente, int opcion) {
        Aprobacion result = null;
        try {

            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(autenticatiosRemedy);
            _Tipo tipo = null;

            if (opcion == 1) {
                aprobacionesRequest.setTipo(tipo.Aplazamiento);
            } else if (opcion == 2) {
                aprobacionesRequest.setTipo(tipo.AsignacionProveedor);
            } else if (opcion == 3) {
                aprobacionesRequest.setTipo(tipo.CambioMonto);
            } else if (opcion == 4) {
                aprobacionesRequest.setTipo(tipo.ConfirmacionCliente);
            } else if (opcion == 5) {
                aprobacionesRequest.setTipo(tipo.MontoInicial);
            } else if (opcion == 6) {
                aprobacionesRequest.setTipo(tipo.RevisionSupervisor);
            }

            RemedyStub consulta = new RemedyStub();

            ConsultarAprobacionesResponse response = new RemedyStub.ConsultarAprobacionesResponse();

            response = consulta.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            //logger.info("MENSAJE aprobaciones: " + aprobacionesResult.getMensaje());
            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            logger.info("AprobacionesRemedyHilos||consultarAprobaciones||Ocurrio algo al crear el incidente: " + e.getMessage());
            this.respuesta = null;
        }

        this.respuesta = result;
    }
}
