package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.OperacionDAO;
import com.gruposalinas.migestion.domain.TipificacionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class OperacionBI {

    @Autowired
    OperacionDAO operacionDAO;

    public List<TipificacionDTO> consulta() {

        List<TipificacionDTO> lista = null;

        try {
            lista = operacionDAO.consulta();
        } catch (Exception e) {

        }

        return lista;

    }

    public List<TipificacionDTO> consultaPadres() {

        List<TipificacionDTO> lista = null;

        try {
            lista = operacionDAO.consultaPadres();
        } catch (Exception e) {

        }

        return lista;

    }

    public int inserta(TipificacionDTO bean) {

        int idCreado = 0;

        try {
            idCreado = operacionDAO.inserta(bean);
        } catch (Exception e) {

        }

        return idCreado;

    }

    public boolean actualiza(TipificacionDTO bean) {

        boolean respuesta = false;

        try {
            respuesta = operacionDAO.actualiza(bean);
        } catch (Exception e) {

        }

        return respuesta;

    }

    public boolean elimina(int idProducto) {
        boolean respuesta = false;

        try {
            respuesta = operacionDAO.elimina(idProducto);
        } catch (Exception e) {

        }

        return respuesta;
    }

    public List<TipificacionDTO> getHijos(int idPadre) {

        List<TipificacionDTO> lista = null;

        try {
            lista = operacionDAO.getHijos(idPadre);
        } catch (Exception e) {

        }

        return lista;

    }

}
