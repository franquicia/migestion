package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.AgendaDAOImpl;
import com.gruposalinas.migestion.domain.AgendaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AgendaBI {

    private static Logger logger = LogManager.getLogger(AgendaBI.class);

    private List<AgendaDTO> listaAgenda;

    @Autowired
    AgendaDAOImpl agendaDAO;

    public List<AgendaDTO> obtieneNotas(String idAgenda, String idUsuario) {

        try {
            listaAgenda = agendaDAO.obtieneNotas(idAgenda, idUsuario);
        } catch (Exception e) {
            logger.info("AgendaBI||obtieneNotas||No fue posible obtener los eventos: " + e.getMessage());
        }

        return listaAgenda;
    }

   
}
