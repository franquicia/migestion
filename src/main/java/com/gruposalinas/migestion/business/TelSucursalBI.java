package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.TelSucursalDAO;
import com.gruposalinas.migestion.domain.TelSucursalDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TelSucursalBI {

    private static Logger logger = LogManager.getLogger(TelSucursalBI.class);

    @Autowired
    TelSucursalDAO telSucursalDAO;

    List<TelSucursalDTO> listaTelesonos = null;
    List<TelSucursalDTO> listaTelefono = null;

    public List<TelSucursalDTO> obtienetelefono() {

        try {
            listaTelesonos = telSucursalDAO.obtieneTodosTelefonos();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Telefonos");

        }

        return listaTelesonos;
    }

    public List<TelSucursalDTO> obtienetelefono(int idCeco) {

        try {
            listaTelefono = telSucursalDAO.obtieneTelefono(idCeco);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla telefono");

        }

        return listaTelefono;
    }

    public boolean insertatelefono(TelSucursalDTO telefono) {

        boolean respuesta = false;

        try {
            respuesta = telSucursalDAO.insertaTelefono(telefono);
        } catch (Exception e) {
            logger.info("No fue posible insertar el telefono");

        }

        return respuesta;
    }

    public boolean actualizatelefono(TelSucursalDTO telefono) {

        boolean respuesta = false;

        try {
            respuesta = telSucursalDAO.actualizaTelefono(telefono);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el telefono");

        }

        return respuesta;
    }

    public boolean eliminatelefono(int idTelefono) {

        boolean respuesta = false;

        try {
            respuesta = telSucursalDAO.eliminaTelefono(idTelefono);
        } catch (Exception e) {
            logger.info("No fue posible borrar el telefono");

        }

        return respuesta;
    }

    public boolean depuraTelefonos() {
        boolean respuesta = false;

        try {
            respuesta = telSucursalDAO.depuraTelefono();
        } catch (Exception e) {
            logger.info("No fue posible depurar la tabla telefonos");

        }

        return respuesta;
    }

}
