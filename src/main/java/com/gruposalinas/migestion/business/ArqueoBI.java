package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ArqueoDAOImpl;
import com.gruposalinas.migestion.domain.ArqueoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ArqueoBI {

    private static Logger logger = LogManager.getLogger(ArqueoBI.class);

    private List<ArqueoDTO> listaArqueo;

    @Autowired
    ArqueoDAOImpl arqueoDAO;

    public List<ArqueoDTO> obtieneNombre(String ceco) {

        try {

            if (ceco.length() == 3) {
                ceco = "0" + ceco;
            }

            listaArqueo = arqueoDAO.consultaCeco(ceco);

        } catch (Exception e) {

            logger.info("ArqueoBI||obtieneNombre||No fue posible obtener el nombre del Ceco:" + e.getMessage());

        }

        return listaArqueo;

    }

}
