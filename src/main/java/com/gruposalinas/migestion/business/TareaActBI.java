package com.gruposalinas.migestion.business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.gruposalinas.migestion.dao.TareaActDAOImpl;
import com.gruposalinas.migestion.domain.TareaActDTO;
import com.gruposalinas.migestion.resources.GTNConstantes;
 

public class TareaActBI {

    private static Logger logger = LogManager.getLogger(TareaActBI.class);

    Map<String, Object> listaTareas = null;
    List<TareaActDTO> listaTarea = null;

    @Autowired
    TareaActDAOImpl tareaActDAO;

    public List<TareaActDTO> obtieneTareaCom(String idTarea, String status, String estatusVac, String idUsuario, String tipoTarea) {

        try {
            listaTarea = tareaActDAO.obtieneTareaCom(idTarea, status, estatusVac, idUsuario, tipoTarea);
        } catch (Exception e) {
            logger.info("No fue posible obtener el registro de la Tarea");
             
        }

        return listaTarea;
    }

    public boolean insertaTareas(TareaActDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = tareaActDAO.insertaTareas(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar la tarea");
             
        }

        return respuesta;
    }

    public boolean actualizaTareas(TareaActDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = tareaActDAO.actualizaTareas(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la tarea");
             
        }

        return respuesta;
    }

    public boolean eliminaTareas(String idTarea) {
        boolean respuesta = false;

        try {
            respuesta = tareaActDAO.eliminaTareas(idTarea);
        } catch (Exception e) {
            logger.info("No fue posible eliminar la tarea");
             
        }

        return respuesta;
    }

    public String getPorcentajeCecoPadre(int idCeco) {

        String json = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        HttpURLConnection con = null;
        BufferedReader in = null;
        try {

            //String urlPath = GTNConstantes.getURLConteoTareas()+"?idCeco=0&negocio=0&op=2";
            String urlPath = GTNConstantes.getURLReflexis() + "ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq?idCeco=" + idCeco + "&negocio=20&op=2";

            URL url = new URL(urlPath);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine = "";
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {

                response.append(inputLine);

            }
            in.close();

            logger.info(response.toString());

            if (!response.toString().equals("[]") && !response.toString().equals("[ ]")) {

                int totalTareas = 0;
                int totalTareasTerminadas = 0;
                double porcentaje = 0d;
                JSONArray jsonArray = new JSONArray(response.toString());
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        totalTareas += Integer.parseInt(obj.getString("FIASIGNADAS"));
                        totalTareasTerminadas += Integer.parseInt(obj.getString("FITERMINADAS"));

                    }
                }
                if (totalTareas > 0) {
                    porcentaje = ((double) totalTareasTerminadas / (double) totalTareas) * 100d;

                }
                DecimalFormat df = new DecimalFormat("###");

                json = "{"
                        + "\"porcentajeCeco\":" + df.format(porcentaje)
                        + "}";

            } else {
                String result = "0";
                json = "{"
                        + "\"porcentajeCeco\":" + result
                        + "}";

            }

        } catch (Exception e) {
            String result = "0";
            json = "{"
                    + "\"porcentajeCeco\":" + result
                    + "}";
        } finally {
            try {
                if (con != null) {
                    con.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return json;

    }

    public String getPorcentajeTareasCeco(int idCeco) {

        String json = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        HttpURLConnection con = null;
        BufferedReader in = null;
        try {

            //String urlPath = GTNConstantes.getURLConteoTareas()+"?idCeco=0&negocio=0&op=2";
            String urlPath = GTNConstantes.getURLReflexis() + "ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq?idCeco=" + idCeco + "&negocio=20&op=3";

            URL url = new URL(urlPath);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine = "";
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {

                response.append(inputLine);

            }
            in.close();

            logger.info(response.toString());

            if (!response.toString().equals("[]") && !response.toString().equals("[ ]")) {

                int totalTareas = 0;
                int totalTareasTerminadas = 0;
                double porcentaje = 0d;
                JSONArray jsonArray = new JSONArray(response.toString());
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        totalTareas += Integer.parseInt(obj.getString("FIASIGNADAS"));
                        totalTareasTerminadas += Integer.parseInt(obj.getString("FITERMINADAS"));
                        porcentaje = Double.parseDouble(obj.getString("FIPORCENTAJE"));

                    }
                }
                /*if(totalTareas>0) {
                 porcentaje=((double)totalTareasTerminadas/(double)totalTareas)*100d;

                 }*/
                DecimalFormat df = new DecimalFormat("###");

                json = "{"
                        + "\"porcentajeCeco\":" + df.format(porcentaje)
                        + "}";

            } else {
                String result = "0";
                json = "{"
                        + "\"porcentajeCeco\":" + result
                        + "}";

            }

        } catch (Exception e) {
            String result = "0";
            json = "{"
                    + "\"porcentajeCeco\":" + result
                    + "}";
        } finally {
            try {
                if (con != null) {
                    con.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return json;

    }

    public String getPorcentajeTareasCecoHijos(int idCeco) {

        String json = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        HttpURLConnection con = null;
        BufferedReader in = null;
        JSONArray resultado = null;
        try {

            //String urlPath = GTNConstantes.getURLConteoTareas()+"?idCeco=0&negocio=0&op=2";
            String urlPath = GTNConstantes.getURLReflexis() + "ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq?idCeco=" + idCeco + "&negocio=20&op=1";

            URL url = new URL(urlPath);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine = "";
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {

                response.append(inputLine);

            }
            in.close();

            logger.info(response.toString());

            if (!response.toString().equals("[]") && !response.toString().equals("[ ]")) {

                JSONArray jsonArray = new JSONArray(response.toString());
                if (jsonArray != null) {
                    resultado = new JSONArray();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        JSONObject objResultado = new JSONObject();
                        objResultado.put("idCeco", obj.getString("FCID_CECO"));
                        objResultado.put("nombreCeco", obj.getString("FCNOMBRE"));
                        objResultado.put("totalTareas", obj.getString("FIASIGNADAS"));
                        objResultado.put("tareasTerminadas", obj.getString("FITERMINADAS"));
                        objResultado.put("porcentaje", obj.getString("FIPORCENTAJE"));
                        objResultado.put("nivel", obj.getString("FINIVEL"));

                        resultado.put(objResultado);

                    }
                }
                json = "{"
                        + "\"listaTareasCeco\":" + resultado.toString()
                        + "}";

            } else {
                String result = null;
                json = "{"
                        + "\"listaTareasCeco\":" + result
                        + "}";

            }

        } catch (Exception e) {
            String result = null;
            json = "{"
                    + "\"listaTareasCeco\":" + result
                    + "}";
        } finally {
            try {
                if (con != null) {
                    con.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return json;

    }

    public String getDetalleTareas(int idCeco) {

        String json = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        HttpURLConnection con = null;
        BufferedReader in = null;
        JSONArray resultado = null;
        Gson g = new Gson();

        try {

            //String urlPath = GTNConstantes.getURLConteoTareas()+"?idCeco=0&negocio=0&op=2";
            String urlPath = GTNConstantes.getURLReflexis() + "ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq?idCeco=" + idCeco + "&negocio=20&op=2";

            URL url = new URL(urlPath);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine = "";
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {

                response.append(inputLine);

            }
            in.close();

            logger.info(response.toString());

            if (!response.toString().equals("[]") && !response.toString().equals("[ ]")) {

                JSONArray jsonArray = new JSONArray(response.toString());
                if (jsonArray != null) {
                    resultado = new JSONArray();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        JSONObject objResultado = new JSONObject();
                        int idUsuario = Integer.parseInt(obj.getString("FIID_USUARIO"));
                        objResultado.put("idUsuario", idUsuario);
                        objResultado.put("nombreUsuario", obj.getString("FINOMBRE"));
                        String puesto = obj.getString("FIID_PUESTO");
                        objResultado.put("idPuesto", obj.getString("FIID_PUESTO"));
                        objResultado.put("descripcionPuesto", obj.getString("FIDESCRIPCION"));
                        objResultado.put("tareasAsignadas", obj.getString("FIASIGNADAS"));
                        objResultado.put("tareasTerminadas", obj.getString("FITERMINADAS"));
                        objResultado.put("porcentaje", obj.getString("FIPORCENTAJE"));
                        int cecoEmp = idCeco;
                        if (puesto.trim().equals("60") || puesto.trim().equals("625") || puesto.trim().equals("1412") || puesto.trim().equals("2028")) {
                            String cecoS = "" + idCeco;
                            cecoS = "39" + cecoS.substring(2, cecoS.length());
                            //System.out.println(cecoS);
                            cecoEmp = Integer.parseInt(cecoS);
                        }
                        if (puesto.trim().equals("1406")) {
                            String cecoS = "" + idCeco;
                            cecoS = "36" + cecoS.substring(2, cecoS.length());
                            //System.out.println(cecoS);
                            cecoEmp = Integer.parseInt(cecoS);
                        }
                        if (puesto.trim().equals("9337")) {
                            String cecoS = "" + idCeco;
                            cecoS = "20" + cecoS.substring(2, cecoS.length());
                            //System.out.println(cecoS);
                            cecoEmp = Integer.parseInt(cecoS);
                        }

                        ArrayList<TareaPendiente> aux = getTareasPendientesUsr(idUsuario, cecoEmp);

                        if (aux != null) {
                            objResultado.put("tareasPendientes", new JSONArray(g.toJson(aux)));
                        } else {
                            objResultado.put("tareasPendientes", new JSONArray());
                        }

                        resultado.put(objResultado);

                    }
                }
                json = "{"
                        + "\"listaDetalleTareas\":" + resultado.toString()
                        + "}";

            } else {
                String result = null;
                json = "{"
                        + "\"listaDetalleTareas\":" + result
                        + "}";

            }

        } catch (Exception e) {
            e.printStackTrace();
            String result = null;
            json = "{"
                    + "\"listaDetalleTareas\":" + result
                    + "}";
        } finally {
            try {
                if (con != null) {
                    con.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return json;

    }

    public ArrayList<TareaPendiente> getTareasPendientesUsr(int idUsuario, int idCeco) {

        OutputStreamWriter outputStreamWriter = null;
        BufferedReader rd = null;
        HttpURLConnection con = null;
        BufferedReader in = null;
        ArrayList<TareaPendiente> resultado = null;
        try {

            //String urlPath = GTNConstantes.getURLConteoTareas()+"?idCeco=0&negocio=0&op=2";
            String urlPath = GTNConstantes.getURLReflexis() + "ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq?idCeco=" + idCeco + "&negocio=20&op=5&usr=" + idUsuario;

            URL url = new URL(urlPath);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine = "";
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {

                response.append(inputLine);

            }
            in.close();

            logger.info(response.toString());

            if (!response.toString().equals("[]") && !response.toString().equals("[ ]")) {

                JSONArray jsonArray = new JSONArray(response.toString());
                if (jsonArray != null) {
                    resultado = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        TareaPendiente tarea = new TareaPendiente();
                        tarea.setDescTarea(jsonArray.getJSONObject(i).getString("FIDET_TAREAS"));
                        resultado.add(tarea);

                    }

                }

            } else {
                resultado = null;

            }

        } catch (Exception e) {
            e.printStackTrace();
            resultado = null;
        } finally {
            try {
                if (con != null) {
                    con.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return resultado;

    }

    class TareaPendiente {

        private int idTarea;
        private String descTarea;

        public int getIdTarea() {
            return idTarea;
        }

        public void setIdTarea(int idTarea) {
            this.idTarea = idTarea;
        }

        public String getDescTarea() {
            return descTarea;
        }

        public void setDescTarea(String descTarea) {
            this.descTarea = descTarea;
        }

        @Override
        public String toString() {
            return "TareaPendiente [idTarea=" + idTarea + ", descTarea=" + descTarea + "]";
        }

    }

    class UsuarioTareas {

        private int idUsuario;
        private String nombre;
        private int idPuesto;
        private String descPuesto;
        private int tareasAsignadas;
        private int tareasTerminadas;
        private double porcentaje;
        private ArrayList<TareaPendiente> tareasPendientes;

        public int getIdUsuario() {
            return idUsuario;
        }

        public void setIdUsuario(int idUsuario) {
            this.idUsuario = idUsuario;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public int getIdPuesto() {
            return idPuesto;
        }

        public void setIdPuesto(int idPuesto) {
            this.idPuesto = idPuesto;
        }

        public String getDescPuesto() {
            return descPuesto;
        }

        public void setDescPuesto(String descPuesto) {
            this.descPuesto = descPuesto;
        }

        public int getTareasAsignadas() {
            return tareasAsignadas;
        }

        public void setTareasAsignadas(int tareasAsignadas) {
            this.tareasAsignadas = tareasAsignadas;
        }

        public int getTareasTerminadas() {
            return tareasTerminadas;
        }

        public void setTareasTerminadas(int tareasTerminadas) {
            this.tareasTerminadas = tareasTerminadas;
        }

        public double getPorcentaje() {
            return porcentaje;
        }

        public void setPorcentaje(double porcentaje) {
            this.porcentaje = porcentaje;
        }

        public ArrayList<TareaPendiente> getTareasPendientes() {
            return tareasPendientes;
        }

        public void setTareasPendientes(ArrayList<TareaPendiente> tareasPendientes) {
            this.tareasPendientes = tareasPendientes;
        }

        @Override
        public String toString() {
            return "UsuarioTareas [idUsuario=" + idUsuario + ", nombre=" + nombre + ", idPuesto=" + idPuesto
                    + ", descPuesto=" + descPuesto + ", tareasAsignadas=" + tareasAsignadas + ", tareasTerminadas="
                    + tareasTerminadas + ", porcentaje=" + porcentaje + ", tareasPendientes=" + tareasPendientes + "]";
        }

    }

    /*public static void main(String args[]) {

     TareaActBI t=new TareaActBI();
     System.out.println(t.getPorcentajeTareasCeco(236279));
     //System.out.println(t.getPorcentajeTareasCeco(488685));
     System.out.println(t.getPorcentajeTareasCecoHijos(236095));
     System.out.println(t.getDetalleTareas(488685));



     }*/
}
