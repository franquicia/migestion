package com.gruposalinas.migestion.business;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.activation.DataHandler;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfProveedor;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Proveedor;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsCProveedor;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Adjunto;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Aprobacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfAdjunto;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfGrupoFalla;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarFallas;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.GrupoFalla;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsCFallas;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarAdjunto;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarAprobaciones;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarAprobacionesResponse;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsCAdjuntos;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsCAprobacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfTracking;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarBitacoraIncidente;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsTracking;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfInformacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub._Accion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ActualizarCuadrillas;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ActualizarIncidente;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Incidente;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsUIncidente;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub._Calificacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub._TipoCierre;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Authentication;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarIncidentes;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarListaAdjuntos;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ConsultarProveedores;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.CrearCuadrilla;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.CrearIncidente;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Cuadrilla;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Filtro;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Informacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Registro;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsCIncidente;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsRIncidente;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.RsRespond;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import com.gruposalinas.migestion.domain.FolioRemedyDTO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.domain.TicketLimpiezaDTO;
import com.gruposalinas.migestion.util.UtilDate;
import com.gruposalinas.migestion.util.UtilString;

import com.google.gson.JsonObject;

import sun.misc.BASE64Decoder;

@SuppressWarnings("restriction")
public class ServiciosRemedyLimpiezaBI {

    private Logger logger = LogManager.getLogger(ServiciosRemedyLimpiezaBI.class);

    @Autowired
    ProveedoresBI proveedoresBI;

    @Autowired
    IncidenciasBI incidenciasBI;

    @Autowired
    TicketCuadrillaLimpiezaBI ticketCuadrillaLimpiezaBI;

    @Autowired
    CuadrillaBI cuadrillaBI;

    @Autowired
    LogBI logbi;

    @Autowired
    PeticionesRemedyBI peticionesRemedyBI;

    @Autowired
    ParametroBI parametroBi;

    /////////////// GERENTE ////////////////
    public int levantaFolio(FolioRemedyDTO bean) {
        int idFolio = 0;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("levantaFolio()" + " LIMPIEZA");
        }

        try {
            CrearIncidente crearIncitenteRequest = new CrearIncidente();
            Registro regRequest = new Registro();
            regRequest.setCC(bean.getCeco());
            regRequest.setComentarios(bean.getComentario());
            regRequest.setCorreo(bean.getCorreo());
            regRequest.setFalla(bean.getUbicacion());
            regRequest.setIncidencia(bean.getServicio());
            regRequest.setTipoFalla(bean.getIncidencia());
            regRequest.setNoEmpleado(bean.getNumEmpleado());
            regRequest.setNombre(bean.getNombre());
            regRequest.setNoSucursal(bean.getNoSucursal());
            regRequest.setPuesto(bean.getPuesto());
            regRequest.setSucursal(bean.getNomSucursal());
            regRequest.setTelSucursal(bean.getTelSucursal());
            regRequest.setTelefono(bean.getTelUsrAlterno());
            regRequest.setPuestoAlterno(bean.getPuestoAlterno());
            regRequest.setUsrAlterno(bean.getNomUsrAlterno());

            if (bean.getNomAdjunto() != null) {
                regRequest.setAdjunto1Nombre(bean.getNomAdjunto());
            } else {
                regRequest.setAdjunto1Nombre("");
            }

            if (bean.getAdjunto() != null) {
                BASE64Decoder decoder = new BASE64Decoder();
                byte[] aux2 = decoder.decodeBuffer(bean.getAdjunto());

                ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                DataHandler data = new DataHandler(rawData);

                regRequest.setAdjunto1Base(data);
            } else {
                regRequest.setAdjunto1Base(null);
            }

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            crearIncitenteRequest.setAuthentication(autenticatiosRemedy);
            crearIncitenteRequest.setIncidente(regRequest);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.CrearIncidenteResponse response = new RemedyStub.CrearIncidenteResponse();
            response = consulta.crearIncidente(crearIncitenteRequest);
            RsRIncidente crearIncidenteResult = response.getCrearIncidenteResult();

            //logger.info("MENSAJE: " + crearIncidenteResult.getMensaje());
            logbi.insertaerror(0, crearIncidenteResult.getMensaje(), "levantaFolio()");
            if (crearIncidenteResult.getExito()) {
                int folio = crearIncidenteResult.getNoIncidente();
                if (folio != 0) {
                    idFolio = folio;
                }
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo al crear el incidente " + e);
            return 0;
        }
        return idFolio;
    }

    public ArrayList<TicketLimpiezaDTO> ConsultaFoliosSucursal(int numSucursal, String opcEst) {
        ArrayOfInformacion arrIncidentes = null;

        ArrayList<TicketLimpiezaDTO> arrayTickets = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaFoliosSucursal()" + " LIMPIEZA");
        }

        try {
            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            logger.info("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setNoSucursal(numSucursal);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(
                    opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {

                arrayTickets = new ArrayList<>();
                arrIncidentes = incidentes.getIncidentes();

                TicketLimpiezaDTO ticketLimpieza = null;
                if (arrIncidentes.getInformacion() != null) {
                    for (int i = 0; i < arrIncidentes.getInformacion().length; i++) {

                        Informacion ticketRemedy = arrIncidentes.getInformacion()[i];
                        ticketLimpieza = new TicketLimpiezaDTO();

                        ticketLimpieza.setIdIncidencia(ticketRemedy.getIdIncidencia());
                        ticketLimpieza.setNoSucursal(ticketRemedy.getNoSucursal());
                        ticketLimpieza.setNombreSucursal(UtilString.validaStrNull(ticketRemedy.getSucursal()));
                        ticketLimpieza.setCentroCostos(UtilString.validaStrNull(ticketRemedy.getCC()));
                        ticketLimpieza.setTelefonoSucursal(UtilString.validaStrNull(ticketRemedy.getTelefonoSucursal()));
                        ticketLimpieza.setIncidencia(UtilString.validaStrNull(ticketRemedy.getIncidencia()));
                        ticketLimpieza.setFalla(UtilString.validaStrNull(ticketRemedy.getFalla()));
                        ticketLimpieza.setTipoFalla(UtilString.validaStrNull(ticketRemedy.getTipoFalla()));
                        ticketLimpieza.setNotas(UtilString.validaStrNull(ticketRemedy.getNotas()));
                        ticketLimpieza.setTicketAnterior(UtilString.validaStrNull(ticketRemedy.getTicketAnterior()));
                        ticketLimpieza.setIdCliente(UtilString.validaStrNull(ticketRemedy.getIdCorpCliente()));
                        ticketLimpieza.setNombreCliente(UtilString.validaStrNull(ticketRemedy.getNombreCliente()));
                        ticketLimpieza.setTelefonoCliente(UtilString.validaStrNull(ticketRemedy.getTelCliente()));
                        ticketLimpieza.setPuestoCliente(UtilString.validaStrNull(ticketRemedy.getPuesto()));
                        ticketLimpieza.setAutorizacionCliente(UtilString.validaStrNull(ticketRemedy.getAutorizacionCliente()));

                        int cal = 0;
                        _Calificacion auxEval = ticketRemedy.getEvalCliente();
                        if (auxEval != null) {

                            if (auxEval.getValue().contains("NA")) {
                                cal = 1;
                            }
                            if (auxEval.getValue().contains("E0a50")) {
                                cal = 2;
                            }
                            if (auxEval.getValue().contains("E51a69")) {
                                cal = 3;
                            }
                            if (auxEval.getValue().contains("E70a89")) {
                                cal = 4;
                            }
                            if (auxEval.getValue().contains("E90a100")) {
                                cal = 5;
                            }
                        }

                        ticketLimpieza.setEvaluacionCliente(cal + "");

                        ticketLimpieza.setUsuarioAlterno(UtilString.validaStrNull(ticketRemedy.getUsrAlterno()));
                        ticketLimpieza.setPuestoAlterno(UtilString.validaStrNull(ticketRemedy.getPuestoAlterno()));
                        ticketLimpieza.setTelefonoContactoAlterno(UtilString.validaStrNull(ticketRemedy.getTelCliente()));

                        ticketLimpieza.setIdSupervisor(UtilString.validaStrNull(ticketRemedy.getIdCorpSupervisor()));
                        ticketLimpieza.setIdCoordinador(UtilString.validaStrNull(ticketRemedy.getIdCorpCoordinador()));
                        ticketLimpieza.setIdProveedor(UtilString.validaStrNull(ticketRemedy.getIdProveedor()));
                        ticketLimpieza.setNoTicketProveedor(UtilString.validaStrNull(ticketRemedy.getNoTicketProveedor()));
                        ticketLimpieza.setAutorizacionProveedor(UtilString.validaStrNull(ticketRemedy.getAutorizacionProveedor()));

                        try {

                            List<ProveedoresDTO> proveedorLimpieza = null;

                            String idProveedor = ticketRemedy.getIdProveedor();
                            if (idProveedor != null && !idProveedor.equals("0")) {
                                proveedorLimpieza = proveedoresBI.obtieneDatos(Integer.parseInt(idProveedor));

                                if (proveedorLimpieza != null && proveedorLimpieza.size() > 0) {
                                    ticketLimpieza.setRazonSocial(proveedorLimpieza.get(0).getRazonSocial());
                                    ticketLimpieza.setMenu(proveedorLimpieza.get(0).getMenu());
                                    ticketLimpieza.setNombreCortoProveedor(proveedorLimpieza.get(0).getNombreCorto());

                                }

                            }

                        } catch (Exception e) {

                        }

                        ticketLimpieza.setProveedor(UtilString.validaStrNull(ticketRemedy.getProveedor()));

                        ticketLimpieza.setTipoCierreProvedor(UtilString.validaStrNull(ticketRemedy.getTipoCierreProveedor()));
                        ticketLimpieza.setAutorizacionPipa(UtilString.validaStrNull(ticketRemedy.getAutorizacionPipa()));
                        ticketLimpieza.setCostoPipa(ticketRemedy.getCostoPipa());
                        ticketLimpieza.setNotificacion(UtilString.validaStrNull(ticketRemedy.getNotificacion()));
                        ticketLimpieza.setMontoEstimado(ticketRemedy.getMontoEstimado());

                        ticketLimpieza.setPrioridad(
                                ticketRemedy.getPrioridad() != null ? UtilString.validaStrNull(ticketRemedy.getPrioridad().getValue()) : null);

                        ticketLimpieza.setTipoAtencion(
                                ticketRemedy.getTipoAtencion() != null ? UtilString.validaStrNull(ticketRemedy.getTipoAtencion().getValue()) : null);

                        ticketLimpieza.setOrigen(
                                ticketRemedy.getOrigen() != null ? UtilString.validaStrNull(ticketRemedy.getOrigen().getValue()) : null);

                        ticketLimpieza.setEstado(UtilString.validaStrNull(ticketRemedy.getEstado()));
                        ticketLimpieza.setMotivoEstado(UtilString.validaStrNull(ticketRemedy.getMotivoEstado()));

                        ticketLimpieza.setFechaCreacion(UtilDate.validaFecha(ticketRemedy.getFechaCreacion()));

                        ticketLimpieza.setFechaModificacion(UtilDate.validaFecha(ticketRemedy.getFechaModificacion()));

                        ticketLimpieza.setFechaInicioAtencion(UtilDate.validaFecha(ticketRemedy.getFechaInicioAtencion()));

                        ticketLimpieza.setFechaUltimoTicket(UtilDate.validaFecha(ticketRemedy.getFechaUltimoTicket()));

                        ticketLimpieza.setFechaEstimada(UtilDate.validaFecha(ticketRemedy.getFechaEstimada()));

                        ticketLimpieza.setFechaCierreAdministrativo(UtilDate.validaFecha(ticketRemedy.getFechaCierreAdministrativo()));

                        int idIncidente = ticketRemedy.getIdIncidencia();

                        List<TicketCuadrillaDTO> lista2 = ticketCuadrillaLimpiezaBI.obtieneDatos(idIncidente);
                        int idProvedor = 0;
                        int idCuadrilla = 0;
                        int idZona = 0;
                        for (TicketCuadrillaDTO aux2 : lista2) {
                            if (aux2.getStatus() != 0) {
                                idProvedor = aux2.getIdProveedor();
                                idCuadrilla = aux2.getIdCuadrilla();
                                idZona = aux2.getZona();
                            }

                        }
                        if (idProvedor != 0) {
                            List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                            for (CuadrillaDTO aux2 : lista3) {
                                if (aux2.getZona() == idZona) {
                                    if (aux2.getIdCuadrilla() == idCuadrilla) {
                                        ticketLimpieza.setIdProveedor(aux2.getIdProveedor() + "");
                                        ticketLimpieza.setIdCuadrilla(aux2.getIdCuadrilla());
                                        ticketLimpieza.setLider(aux2.getLiderCuad());
                                        ticketLimpieza.setCorreo(aux2.getCorreo());
                                        ticketLimpieza.setSegundoMando(aux2.getSegundo());
                                        ticketLimpieza.setIdZona(aux2.getZona());

                                    }
                                }
                            }
                        } else {
                            String auxiliar = null;
                            //incJsonObj.addProperty("ID_PROVEEDOR", auxiliar);
                            ticketLimpieza.setIdProveedor("");
                            ticketLimpieza.setIdCuadrilla(0);
                            ticketLimpieza.setLider("");
                            ticketLimpieza.setCorreo("");
                            ticketLimpieza.setSegundoMando("");
                            ticketLimpieza.setIdZona(0);
                        }

                        Aprobacion aprobacionObjeto = null;

                        //En caso de ser un ticket cancelado.
                        if (ticketRemedy.getEstado().equalsIgnoreCase("atendido") && ticketRemedy.getMotivoEstado().equalsIgnoreCase("cancelado no aplica")) {

                            aprobacionObjeto = consultaAprobacion(ticketRemedy.getIdIncidencia(), 2);

                        } else if (ticketRemedy.getEstado().equalsIgnoreCase("atendido") && ticketRemedy.getMotivoEstado().equalsIgnoreCase("Cancelado correctivo menor")) {
                            aprobacionObjeto = consultaAprobacion(ticketRemedy.getIdIncidencia(), 4);
                        }

                        if (aprobacionObjeto != null) {
                            ticketLimpieza.setResolucion(aprobacionObjeto.getJustificacion());
                        }

                        ticketLimpieza.setClienteAvisado(ticketRemedy.getClienteAvisado());

                        arrayTickets.add(ticketLimpieza);
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo al adjuntar archivo al incidente" + e);
            return arrayTickets;
        }
        return arrayTickets;
    }

    public String ConsultaAdjunto(int idFolio) {
        String res = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaAdjunto()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            //System.out.println("Today is " + today.getTime());

            ConsultarAdjunto consultarAdjuntoRequest = new ConsultarAdjunto();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            consultarAdjuntoRequest.setAuthentication(autenticatiosRemedy);
            consultarAdjuntoRequest.setNoIncidente(idFolio);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarAdjuntoResponse response = new RemedyStub.ConsultarAdjuntoResponse();

            response = consulta.consultarAdjunto(consultarAdjuntoRequest);

            RsCAdjuntos adjuntoR = response.getConsultarAdjuntoResult();
            //logger.info("Mensaje adjunto: " + adjuntoR.getMensaje());
            logbi.insertaerror(0, adjuntoR.getMensaje(), "ConsultaAdjunto()");
            if (adjuntoR.getExito()) {
                ArrayOfAdjunto arrAdjunto = adjuntoR.getAdjuntos();
                if (arrAdjunto != null) {
                    Adjunto[] aux = arrAdjunto.getAdjunto();
                    if (aux != null) {
                        for (Adjunto x : aux) {
                            DataHandler dataAUX = x.getA1_Base();

                            InputStream in = dataAUX.getInputStream();
                            byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                            Base64 codec = new Base64();
                            // String encoded = Base64.encodeBase64String(byteArray);
                            String encoded = new String(Base64.encodeBase64(byteArray), "UTF-8");
                            res = encoded;
                        }
                    } else {
                        res = null;
                    }
                }
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente" + e);

            return res;
        }

        return res;
    }

    public Adjunto[] consultaAdjuntos(int incidente) {

        RsCAdjuntos adjuntos = null;

        Adjunto[] arrayAdjuntos = null;

        Calendar today = Calendar.getInstance();
        today.set(2016, 1, 1);
        //System.out.println("Today is " + today.getTime());

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaAdjuntos()" + " LIMPIEZA");
        }

        try {
            ConsultarListaAdjuntos consultaradjuntosRequest = new ConsultarListaAdjuntos();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            consultaradjuntosRequest.setAuthentication(autenticatiosRemedy);
            consultaradjuntosRequest.setNoIncidente(incidente);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarListaAdjuntosResponse response = new RemedyStub.ConsultarListaAdjuntosResponse();

            response = consulta.consultarListaAdjuntos(consultaradjuntosRequest);

            adjuntos = response.getConsultarListaAdjuntosResult();

            if (adjuntos != null) {

                ArrayOfAdjunto adjuntosLista = adjuntos.getAdjuntos();

                if (adjuntosLista != null && adjuntosLista.getAdjunto() != null && adjuntosLista.getAdjunto().length > 0) {

                    arrayAdjuntos = adjuntosLista.getAdjunto();

                } else {
                    arrayAdjuntos = null;
                }

            }

        } catch (Exception e) {
            return null;
        }

        return arrayAdjuntos;

    }

    public ArrayOfTracking ConsultaBitacora(int idFolio) {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaBitacora()" + " LIMPIEZA");
        }

        ArrayOfTracking res = null;
        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            //System.out.println("Today is " + today.getTime());

            ConsultarBitacoraIncidente consultarBitacoraRequest = new ConsultarBitacoraIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            consultarBitacoraRequest.setAuthentication(autenticatiosRemedy);
            consultarBitacoraRequest.setNoIncidente(idFolio);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarBitacoraIncidenteResponse response = new RemedyStub.ConsultarBitacoraIncidenteResponse();

            response = consulta.consultarBitacoraIncidente(consultarBitacoraRequest);

            RsTracking traking = response.getConsultarBitacoraIncidenteResult();

            //logger.info("Mensaje adjunto: " + traking.getMensaje());
            logbi.insertaerror(0, traking.getMensaje(), "ConsultaBitacora()");
            if (traking.getExito()) {
                ArrayOfTracking arrTraking = traking.getBitacora();
                res = arrTraking;
            }

        } catch (Exception e) {
            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);
            return null;
        }

        return res;
    }

    public boolean aceptaCliente(int idIncidente, int calificacion, String justificacion) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("aceptaCliente()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AceptaCierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();

            incide.setAdjunto1Base(null);
            incide.setAdjunto1Nombre("");

            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setAcepta(true);
            incide.setSupervisor("*Sin informacion");

            _Calificacion calAux = null;

            if (calificacion == 1) {
                incide.setCalificacion(calAux.NA);
            }
            if (calificacion == 2) {
                incide.setCalificacion(calAux.E0a50);
            }
            if (calificacion == 3) {
                incide.setCalificacion(calAux.E51a69);
            }
            if (calificacion == 4) {
                incide.setCalificacion(calAux.E70a89);
            }
            if (calificacion == 5) {
                incide.setCalificacion(calAux.E90a100);
            }

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResulje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AceptaCliente()");

            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo acptar el trabajo cliente " + e);

            return false;
        }

        return result;
    }

    public boolean rechazaCliente(int idIncidente, String justificacion, String nomArchivo, String adjuntoBase, String solicitante, String aprobador) {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("rechazaCliente()" + " LIMPIEZA");
        }

        boolean result = false;
        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AceptaCierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    BASE64Decoder decoder = new BASE64Decoder();
                    byte[] aux2 = decoder.decodeBuffer(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }

            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");

            incide.setAcepta(false);
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setSupervisor("*Sin informacion");

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResulje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "RechazaCliente()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo al crear el incidente " + e);
            return false;
        }

        return result;
    }

    public String getInfoAprobaciones(int incidente, int opcion) {

        JsonObject respuesta = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("getInfoAprobaciones()" + " LIMPIEZA");
        }

        try {

            if (opcion == 1) {

                AprobacionesRemedyLimpiezaHilos cierreTecnico = new AprobacionesRemedyLimpiezaHilos(incidente, 6);
                cierreTecnico.start();

                while (cierreTecnico.isAlive()) {
                    //Se ejecuta mientras este vivo el hilo
                }

                respuesta = new JsonObject();

                if (cierreTecnico.getRespuesta() != null) {
                    respuesta.addProperty("justificacion_cierre", cierreTecnico.getRespuesta().getJustificacion());
                    respuesta.addProperty("archivo_cierre_1", validaNuloBase64(cierreTecnico.getRespuesta().getA1_Base()));
                    respuesta.addProperty("archivo_cierre_2", validaNuloBase64(cierreTecnico.getRespuesta().getA2_Base()));
                    respuesta.addProperty("fecha_cierre", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cierreTecnico.getRespuesta().getFechaModificacion().getTime()));
                    respuesta.addProperty("justificacion_cierre_rechazado", "");
                    respuesta.addProperty("archivo_cierre_rechazado_1", "");
                    respuesta.addProperty("fecha_cierre_rechazo", "0001-01-01 00:00:00");
                    respuesta.addProperty("justificacion_calificacion", "");
                }

            } else if (opcion == 2) {

                AprobacionesRemedyLimpiezaHilos cierreTecnico = new AprobacionesRemedyLimpiezaHilos(incidente, 6);
                cierreTecnico.start();

                AprobacionesRemedyLimpiezaHilos cierreTecnicoRechazado = new AprobacionesRemedyLimpiezaHilos(incidente, 7);
                cierreTecnicoRechazado.start();

                while (cierreTecnico.isAlive() || cierreTecnicoRechazado.isAlive()) {
                    //Se ejecuta mientras este vivov cualquier hilo
                }

                respuesta = new JsonObject();

                if (cierreTecnico.getRespuesta() != null) {
                    respuesta.addProperty("justificacion_cierre", cierreTecnico.getRespuesta().getJustificacion());
                    respuesta.addProperty("archivo_cierre_1", validaNuloBase64(cierreTecnico.getRespuesta().getA1_Base()));
                    respuesta.addProperty("archivo_cierre_2", validaNuloBase64(cierreTecnico.getRespuesta().getA2_Base()));
                    respuesta.addProperty("fecha_cierre", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cierreTecnico.getRespuesta().getFechaModificacion().getTime()));
                }

                if (cierreTecnicoRechazado.getRespuesta() != null) {
                    respuesta.addProperty("justificacion_cierre_rechazado", cierreTecnicoRechazado.getRespuesta().getJustificacion());
                    respuesta.addProperty("archivo_cierre_rechazado_1", validaNuloBase64(cierreTecnicoRechazado.getRespuesta().getA1_Base()));
                    respuesta.addProperty("fecha_cierre_rechazo", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cierreTecnicoRechazado.getRespuesta().getFechaModificacion().getTime()));
                    respuesta.addProperty("justificacion_calificacion", "");
                }

            } else if (opcion == 8) {

                AprobacionesRemedyLimpiezaHilos cierreTecnico = new AprobacionesRemedyLimpiezaHilos(incidente, 6);
                cierreTecnico.start();
                AprobacionesRemedyLimpiezaHilos cierreTecnicoAceptado = new AprobacionesRemedyLimpiezaHilos(incidente, 8);
                cierreTecnicoAceptado.start();

                while (cierreTecnico.isAlive() || cierreTecnicoAceptado.isAlive()) {
                    //Se ejecuta mientras este vivo el hilo
                }

                respuesta = new JsonObject();

                if (cierreTecnico.getRespuesta() != null) {
                    respuesta.addProperty("justificacion_cierre", cierreTecnico.getRespuesta().getJustificacion());
                    respuesta.addProperty("archivo_cierre_1", validaNuloBase64(cierreTecnico.getRespuesta().getA1_Base()));
                    respuesta.addProperty("archivo_cierre_2", validaNuloBase64(cierreTecnico.getRespuesta().getA2_Base()));
                    respuesta.addProperty("fecha_cierre", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cierreTecnico.getRespuesta().getFechaModificacion().getTime()));
                    respuesta.addProperty("justificacion_cierre_rechazado", "");
                    respuesta.addProperty("archivo_cierre_rechazado_1", "");
                    respuesta.addProperty("fecha_cierre_rechazo", "0001-01-01 00:00:00");
                }

                if (cierreTecnicoAceptado.getRespuesta() != null) {
                    respuesta.addProperty("justificacion_calificacion", cierreTecnicoAceptado.getRespuesta().getJustificacion());
                    respuesta.addProperty("fecha_calificacion", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cierreTecnicoAceptado.getRespuesta().getFechaModificacion().getTime()));
                }

            } else {

                respuesta = new JsonObject();

                respuesta.addProperty("justificacion_cierre", "");
                respuesta.addProperty("archivo_cierre_1", "");
                respuesta.addProperty("archivo_cierre_2", "");
                respuesta.addProperty("fecha_cierre", "0001-01-01 00:00:00");
                respuesta.addProperty("justificacion_cierre_rechazado", "");
                respuesta.addProperty("archivo_cierre_rechazado_1", "");
                respuesta.addProperty("fecha_cierre_rechazo", "0001-01-01 00:00:00");
                respuesta.addProperty("justificacion_calificacion", "");

            }

        } catch (Exception e) {

            respuesta = new JsonObject();

        }

        return respuesta.toString();
    }

    /////////////// GERENTE ////////////////
    //////////////// PROVEEDORES ////////////////
    public boolean ConsultaProveedorLimpieza() {

        boolean salida = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaProveedorLimpieza()" + " LIMPIEZA");
        }

        try {
            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            ConsultarProveedores consultaProveedoresRequest = new ConsultarProveedores();
            // consultaProveedoresRequest.setFecha(today);
            consultaProveedoresRequest.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarProveedores request = new RemedyStub.ConsultarProveedores();
            RemedyStub.ConsultarProveedoresResponse response = new RemedyStub.ConsultarProveedoresResponse();

            response = consulta.consultarProveedores(consultaProveedoresRequest);

            RsCProveedor adjuntoResult = response.getConsultarProveedoresResult();

            if (adjuntoResult.getExito()) {
                logger.info("Encontro los Proveedores");

                ArrayOfProveedor x = adjuntoResult.getProveedores();
                if (x.getProveedor() != null) {
                    salida = true;

                    Proveedor[] aux = x.getProveedor();
                    if (aux != null) {

                        for (Proveedor aux2 : aux) {

                            ProveedoresDTO proveedores = new ProveedoresDTO();

                            proveedores.setMenu(aux2.getFormatoMenu());
                            proveedores.setIdProveedor(aux2.getNoProveedor());
                            proveedores.setNombreCorto(aux2.getNombreCorto());
                            proveedores.setRazonSocial(aux2.getRazonSocial());
                            proveedores.setStatus(aux2.getStatus());

                            proveedoresBI.insertaLimpieza(proveedores);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo al adjuntar archivo al incidente" + e);
            return salida;
        }
        return salida;
    }

    public boolean cargafallas() {

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cargafallas()" + " LIMPIEZA");
        }

        boolean salida = false;
        try {
            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            ConsultarFallas ConsultaFallasRequest = new ConsultarFallas();
            ConsultaFallasRequest.setFecha(today);
            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            ConsultaFallasRequest.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarFallas request = new RemedyStub.ConsultarFallas();
            RemedyStub.ConsultarFallasResponse response = new RemedyStub.ConsultarFallasResponse();

            response = consulta.consultarFallas(ConsultaFallasRequest);

            RsCFallas adjuntoResult = response.getConsultarFallasResult();

            if (adjuntoResult.getExito()) {
                logger.info("Encontro las fallas");

                ArrayOfGrupoFalla x = adjuntoResult.getFallas();
                if (x.getGrupoFalla() != null) {
                    salida = true;

                    GrupoFalla[] aux = x.getGrupoFalla();
                    if (x != null) {
                        //logger.info("Depurando tabla fallas: " + incidenciasBI.depura());
                        for (GrupoFalla aux2 : aux) {
                            logger.info("" + aux2.getId() + ", " + aux2.getN1_Incidencia() + ", " + aux2.getN2_Falla()
                                    + ", " + aux2.getN3_TipoFalla());
                            IncidenciasDTO incid = new IncidenciasDTO();

                            incid.setIdTipo(Integer.parseInt(aux2.getId()));
                            incid.setServicio(aux2.getN1_Incidencia());
                            incid.setUbicacion(aux2.getN2_Falla());
                            incid.setIncidencia(aux2.getN3_TipoFalla());
                            incid.setPlantilla(aux2.getPlantillaAsociada());
                            incid.setStatus(aux2.getStatus());
                            int res = incidenciasBI.insertaLimpieza(incid);
                            logger.info("Consultar Fallas Remedy " + res);
                        }
                    }
                }

            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return salida;
        }

        return salida;
    }

    public boolean autorizaProveedor(int idIncidente, String justificacion, String idCuadrilla) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("autorizaProveedor()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.ProveedorOperativo);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            incide.setIdIncidente(idIncidente);
            incide.setAcepta(true);
            incide.setJustificacion("");
            incide.setSupervisor("*Supervisor autorizaProveedor");
            incide.setContacto("*Contacto autorizaProveedor");
            incide.setIdCuadrilla(idCuadrilla);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AutorizaProveedor()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {
            logger.info("Ocurrio algo al crear el incidente " + e);
            return false;
        }

        return result;
    }

    public boolean declinaProveedor(int idIncidente, String justificacion, String idCuadrilla) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("declinaProveedor()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.RechazoProveedor);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            incide.setIdIncidente(idIncidente);
            incide.setAcepta(false);
            incide.setJustificacion(justificacion);
            incide.setProveedor(idCuadrilla);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "DeclinaProveedor()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean cierreTecnicoRemoto(int idIncidente, String justificacion, String nomArchivo, String adjuntoBase, String nomArchivo2, String adjuntoBase2, String solicitante, String aprobador, String clienteAvisado) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreTecnicoRemoto()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.SolicitaCierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    BASE64Decoder decoder = new BASE64Decoder();
                    byte[] aux2 = decoder.decodeBuffer(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }

            if (adjuntoBase2 != null) {
                if (adjuntoBase2.trim().toLowerCase().equals("null") || adjuntoBase2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Base(null);
                } else {
                    BASE64Decoder decoder = new BASE64Decoder();
                    byte[] aux2 = decoder.decodeBuffer(adjuntoBase2);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto2Base(data);
                }

            }
            if (nomArchivo2 != null) {
                if (nomArchivo2.trim().toLowerCase().equals("null") || nomArchivo2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Nombre("");
                } else {
                    incide.setAdjunto2Nombre(nomArchivo2);

                }
            } else {
                incide.setAdjunto2Nombre("");
            }

            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setSupervisor(aprobador);
            incide.setContacto("*Sin Contacto");
            incide.setIdCuadrilla("*Sin Contacto");
            incide.setClienteAvisado(clienteAvisado);

            _TipoCierre tipoCierre = null;
            incide.setCierre(tipoCierre.Remoto);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResulje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "CierreTecnicoRemoto()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {
            logger.info("Ocurrio algo en cierre remoto del incidente " + e);
            return false;
        }
        return result;
    }

    public boolean cierreTecnicoSitio(int idIncidente, String justificacion,
            String nomArchivo, String adjuntoBase, String nomArchivo2, String adjuntoBase2, String solicitante, String aprobador, String clienteAvisado) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreTecnicoSitio()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.SolicitaCierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    BASE64Decoder decoder = new BASE64Decoder();
                    byte[] aux2 = decoder.decodeBuffer(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }

            if (adjuntoBase2 != null) {
                if (adjuntoBase2.trim().toLowerCase().equals("null") || adjuntoBase2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Base(null);
                } else {
                    BASE64Decoder decoder = new BASE64Decoder();
                    byte[] aux2 = decoder.decodeBuffer(adjuntoBase2);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto2Base(data);
                }

            }
            if (nomArchivo2 != null) {
                if (nomArchivo2.trim().toLowerCase().equals("null") || nomArchivo2.trim().toLowerCase().equals("")) {
                    incide.setAdjunto2Nombre("");
                } else {
                    incide.setAdjunto2Nombre(nomArchivo2);

                }
            } else {
                incide.setAdjunto2Nombre("");
            }

            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setSupervisor(aprobador);
            incide.setContacto("*Sin Contacto");
            incide.setIdCuadrilla("*Sin idCuadrilla");
            incide.setClienteAvisado(clienteAvisado);

            _TipoCierre tipoCierre = null;
            incide.setCierre(tipoCierre.EnSitio);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {
            logger.info("Ocurrio algo en cierre en sitio del incidente " + e);
            return false;
        }

        return result;
    }

    public boolean crearCuadrillas(String idCuadrilla, String alterno, String pwd, String correo, String jefe, int noProveedor, String telefono, String zona) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("crearCuadrillas()" + " LIMPIEZA");
        }

        try {

            Cuadrilla cuadrilla = new Cuadrilla();

            cuadrilla.setIdCuadrilla(idCuadrilla);
            cuadrilla.setAlterno(alterno);
            cuadrilla.setContrasenia(pwd);
            cuadrilla.setCorreo(correo);
            cuadrilla.setJefe(jefe);
            cuadrilla.setNoProveedor(noProveedor);
            cuadrilla.setTelefono(telefono);
            String zonaS = "";
            if (zona.contains("0")) {
                zonaS = "NORTE";
            }
            if (zona.contains("1")) {
                zonaS = "CENTRO";
            }
            if (zona.contains("2")) {
                zonaS = "SUR";
            }
            cuadrilla.setZona(zonaS);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            CrearCuadrilla crearCuadrilla = new CrearCuadrilla();
            crearCuadrilla.setAuthentication(autenticatiosRemedy);
            crearCuadrilla.setCuadrilla(cuadrilla);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.CrearCuadrillaResponse response = new RemedyStub.CrearCuadrillaResponse();

            response = consulta.crearCuadrilla(crearCuadrilla);

            RsRespond resultCuadrilla = response.getCrearCuadrillaResult();

            //logger.info("MENSAJE Actualizar cuadrilla: " + resultCuadrilla.getMensaje());
            logbi.insertaerror(0, resultCuadrilla.getMensaje(), "CrearCuadrillas()");

            if (resultCuadrilla.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al insertar cuadrilla " + e);

            return false;
        }

        return result;
    }

    public boolean actualizaCuadrillas(String idCuadrilla, String alterno, String pwd, String correo, String jefe, int noProveedor, String telefono, String zona) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("actualizaCuadrillas()" + " LIMPIEZA");
        }

        try {
            Cuadrilla cuadrilla = new Cuadrilla();

            cuadrilla.setIdCuadrilla(idCuadrilla);
            cuadrilla.setAlterno(alterno);
            cuadrilla.setContrasenia(pwd);
            cuadrilla.setCorreo(correo);
            cuadrilla.setJefe(jefe);
            cuadrilla.setNoProveedor(noProveedor);
            cuadrilla.setTelefono(telefono);
            String zonaS = "";
            if (zona.contains("0")) {
                zonaS = "NORTE";
            }
            if (zona.contains("1")) {
                zonaS = "CENTRO";
            }
            if (zona.contains("2")) {
                zonaS = "SUR";
            }
            cuadrilla.setZona(zonaS);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            ActualizarCuadrillas actCuadrilla = new ActualizarCuadrillas();
            actCuadrilla.setAuthentication(autenticatiosRemedy);
            actCuadrilla.setCuadrilla(cuadrilla);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarCuadrillasResponse response = new RemedyStub.ActualizarCuadrillasResponse();

            response = consulta.actualizarCuadrillas(actCuadrilla);

            RsRespond resultCuadrilla = response.getActualizarCuadrillasResult();

            //logger.info("MENSAJE Actualizar cuadrilla: " + resultCuadrilla.getMensaje());
            logbi.insertaerror(0, resultCuadrilla.getMensaje(), "ActualizaCuadrillas()");

            if (resultCuadrilla.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al incertar cuadrilla " + e);

            return false;
        }

        return result;
    }

    //////////////// PROVEEDORES ////////////////
    //////////////// BANDEJAS ////////////////
    public ArrayOfInformacion consultaFoliosSupervidor(int numSupervisor, String opcEst) {

        ArrayOfInformacion arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaFoliosSupervidor()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            logger.info("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(
                    opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al consultar la bandeja de supervisor " + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public ArrayOfInformacion consultaFoliosProveedor(String proveedor, String opcEst) {
        ArrayOfInformacion arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaFoliosProveedor()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            //System.out.println("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            //nFiltro.setSupervisorAsignado(0);
            //nFiltro.setCoordinadorAsignado(0);
            nFiltro.setProveedor(proveedor);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public ArrayOfInformacion ConsultaDetalleFolio(int idFolio) {
        ArrayOfInformacion arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaDetalleFolio()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            //nFiltro.setSupervisorAsignado(0);
            //nFiltro.setCoordinadorAsignado(0);
            nFiltro.setIdIncidente(idFolio);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al adjuntar archivo al incidente " + e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    //////////////// BANDEJAS ////////////////
    public String validaNuloBase64(DataHandler imagen) {

        String respuesta = "";

        try {

            if (imagen != null) {

                InputStream in = imagen.getInputStream();
                byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                Base64 codec = new Base64();
                // String encoded = Base64.encodeBase64String(byteArray);
                String encoded = new String(Base64.encodeBase64(byteArray), "UTF-8");
                respuesta = encoded;

            } else {

                respuesta = "";

            }

        } catch (Exception e) {
            respuesta = "";
        }

        return respuesta;

    }

    public Aprobacion consultaAprobacion(int idIncidente, int opcion) {

        Aprobacion result = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaAprobacion()" + " LIMPIEZA");
        }

        try {

            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(accesoRemedy);

            org.apache.axis2.databinding.types.UnsignedByte auxZ = new org.apache.axis2.databinding.types.UnsignedByte(opcion);

            aprobacionesRequest.setTipo(auxZ);

            RemedyStub consultaAprobacion = new RemedyStub();

            ConsultarAprobacionesResponse response = new RemedyStub.ConsultarAprobacionesResponse();

            response = consultaAprobacion.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            //logger.info("MENSAJE aprobaciones: " + aprobacionesResult.getMensaje());
            logbi.insertaerror(0, aprobacionesResult.getMensaje(), "ConsultaAprobacion()");

            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            logger.info("AprobacionesRemedyHilos||consultarAprobaciones||Ocurrio algo al consultar la aprobacion el incidente: " + e.getMessage());
            result = null;
        }

        return result;

    }

    public Aprobacion consultarAprobaciones(int idIncidente, int opcion) {
        Aprobacion result = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultarAprobaciones()" + " LIMPIEZA");
        }

        try {
            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(accesoRemedy);

            org.apache.axis2.databinding.types.UnsignedByte auxZ = new org.apache.axis2.databinding.types.UnsignedByte(opcion);

            aprobacionesRequest.setTipo(auxZ);

            RemedyStub consulta = new RemedyStub();

            ConsultarAprobacionesResponse response = new RemedyStub.ConsultarAprobacionesResponse();

            response = consulta.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            //logger.info("MENSAJE aprobaciones: " + aprobacionesResult.getMensaje());
            logbi.insertaerror(0, aprobacionesResult.getMensaje(), "ConsultarAprobaciones()");

            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return null;
        }

        return result;
    }

    public boolean cierreSinOT(int idIncidente, String resolucion, String montivoEstado) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreSinOT()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            if (montivoEstado.equalsIgnoreCase("rechazado proveedor")) {
                actIncitenteRequest.setAccion(action.CancelacionRechazo);
            } else if (montivoEstado.equalsIgnoreCase("por asignar")) {
                actIncitenteRequest.setAccion(action.Cancelacion);
            }

            actIncitenteRequest.setAuthentication(accesoRemedy);

            Incidente incide = new Incidente();
            // incide.setAdjunto1Base("");
            incide.setAdjunto1Nombre("");
            // incide.setAdjunto2Base("");
            incide.setAdjunto2Nombre("");
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(resolucion);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "CierreSinOT()");

            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean asignarProveedor(int idIncidente, String proveedor) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("asignarProveedor()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AsignaProveedor);
            actIncitenteRequest.setAuthentication(accesoRemedy);

            Incidente incide = new Incidente();
            // incide.setAdjunto1Base("");
            incide.setAdjunto1Nombre("");
            // incide.setAdjunto2Base("");
            incide.setAdjunto2Nombre("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion("");

            incide.setProveedor(proveedor);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            //logger.info("MENSAJE: " + actIncidenteResult.getMensaje());
            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AsignarProveedor()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo al crear el incidente " + e);

            return false;
        }

        return result;
    }

    public boolean getPermisoPeticionesBD() {

        boolean respuesta = false;

        try {
            List<ParametroDTO> listaParam = parametroBi.obtieneParametros("ejecutaInsertBD");

            if (listaParam != null && listaParam.size() > 0) {

                ParametroDTO parametro = listaParam.get(0);

                if (parametro != null && parametro.getValor().equals("1")) {
                    respuesta = true;
                } else {
                    respuesta = false;
                }

            }

            return respuesta;

        } catch (Exception e) {
            return false;
        }

    }

}
