package com.gruposalinas.migestion.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.dao.MovilInfReporteDAO;
import com.gruposalinas.migestion.dao.MovilInfoDAO;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.MovilInfoDTO;
import com.gruposalinas.migestion.domain.MovilinfReporteDTO;
import com.gruposalinas.migestion.domain.ReporteMensualMovilDTO;

public class MovilInfoBI {

    private static Logger logger = LogManager.getLogger(MovilInfoBI.class);

    @Autowired
    MovilInfoDAO movilInfoDAO;

    @Autowired
    MovilInfReporteDAO movilInfReporteDAO;

    List<MovilInfoDTO> listaInfo = null;

    public List<MovilInfoDTO> obtieneInfoMovil(MovilInfoDTO bean) {
        try {
            listaInfo = movilInfoDAO.obtieneInfoMovil(bean);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos");

        }
        return listaInfo;
    }

    public boolean insertaInfoMovil(MovilInfoDTO bean) {

        boolean idMovil = false;
        try {
            idMovil = movilInfoDAO.insertaInfoMovil(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar la información del dispositivo" + e);

        }
        return idMovil;
    }

    public boolean actualizaInfoMovil(MovilInfoDTO bean) {

        boolean respuesta = false;

        try {
            respuesta = movilInfoDAO.actualizaInfoMovil(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la tabla");

        }

        return respuesta;
    }

    public boolean eliminaInfoMovil(int idMovil) {
        boolean respuesta = false;

        try {
            respuesta = movilInfoDAO.eliminaInfoMovil(idMovil);
        } catch (Exception e) {
            logger.info("No fue posible borrar la información del dispositivo");

        }
        return respuesta;
    }

    public List<MovilinfReporteDTO> getReporteMensual(String mes, String anio) {
        List<MovilinfReporteDTO> reporte = null;

        try {
            reporte = movilInfReporteDAO.getReporteMensual(mes, anio);

        } catch (Exception e) {
            //e.printStackTrace();
        }

        return reporte;

    }

    public List<MovilInfoDTO> getDetalleUsuario(String mes, String anio, int idUsuario) {
        List<MovilInfoDTO> reporte = null;

        try {
            reporte = movilInfReporteDAO.getDetalleUsuarios(idUsuario, mes, anio);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return reporte;

    }

    public List<ReporteMensualMovilDTO> getReporteMensualCeco(int pais, int negocio, int ceco, String mes, String anio) {
        List<ReporteMensualMovilDTO> reporte = null;

        try {
            reporte = movilInfReporteDAO.getReporteMensualCeco(pais, negocio, ceco, mes, anio);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return reporte;

    }

    public List<CecoDTO> getCecosHijos(int pais, int negocio, String ceco) {
        List<CecoDTO> reporte = null;

        try {
            reporte = movilInfReporteDAO.getCecosHijos(pais, negocio, ceco);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return reporte;

    }

    public List<ReporteMensualMovilDTO> getReporteSemanalCeco(int pais, int negocio, int ceco, String fechaInicio, String fechaFin) {
        List<ReporteMensualMovilDTO> reporte = null;

        try {
            reporte = movilInfReporteDAO.getReporteSemanalCeco(pais, negocio, ceco, fechaInicio, fechaFin);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return reporte;

    }

}
