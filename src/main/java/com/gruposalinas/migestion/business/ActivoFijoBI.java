package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ActivoFijoDAO;
import com.gruposalinas.migestion.domain.ActivoFijoDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ActivoFijoBI {

    private static Logger logger = LogManager.getLogger(AgendaBI.class);

    @Autowired
    ActivoFijoDAO activoFijoDAO;


    public int actualizaActivoFijo(ActivoFijoDTO bean) {
        int respuesta = 0;
        try {
            respuesta = activoFijoDAO.actualizaActivoFijo(bean);
        } catch (Exception e) {
            logger.info("ActivoFijoBI||actualizaActivoFijo||No fue posible actualizar la GETAACTFIJO: " + e.getMessage());
        }
        return respuesta;
    }

    public int eliminaActivoFijo(int idActivoFijo) {
        int respuesta = 0;
        try {
            respuesta = activoFijoDAO.eliminaActivoFijo(idActivoFijo);
        } catch (Exception e) {
            logger.info("ActivoFijoBI||eliminaActivoFijo||No fue posible eliminar en la tabla GETAACTFIJO: " + e.getMessage());
        }
        return respuesta;
    }



    public List<ActivoFijoDTO> consultaActivoFijoAll() {
        List<ActivoFijoDTO> respuesta = new ArrayList<ActivoFijoDTO>();
        try {
            respuesta = activoFijoDAO.consultaActivoFijoAll();
        } catch (Exception e) {
            logger.info("ActivoFijoBI||consultaActivoFijoAll||No fue posible consultar la tabla GETAACTFIJO: " + e.getMessage());
        }
        return respuesta;
    }
}
