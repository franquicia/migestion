package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ArchivosUrlDAO;
import com.gruposalinas.migestion.domain.ArchivosUrlDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ArchivoUrlBI {

    private static Logger logger = LogManager.getLogger(ArchivoUrlBI.class);

    private List<ArchivosUrlDTO> listafila;

    @Autowired
    ArchivosUrlDAO archivosUrlDAO;

    public int inserta(ArchivosUrlDTO bean) {
        int respuesta = 0;

        try {
            respuesta = archivosUrlDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("ArchivoUrlBI||inserta||No fue posible insertar: " + e.getMessage());
        }

        return respuesta;
    }

    public boolean elimina(String idArch) {
        boolean respuesta = false;

        try {
            respuesta = archivosUrlDAO.elimina(idArch);
        } catch (Exception e) {
            logger.info("ArchivoUrlBI||elimina||No fue posible eliminar: " + e.getMessage());
        }

        return respuesta;
    }

    public List<ArchivosUrlDTO> obtieneDatos(int idArch) {

        try {
            listafila = archivosUrlDAO.obtieneDatos(idArch);
        } catch (Exception e) {
            logger.info("ArchivoUrlBI||obtieneDatos||No fue posible obtener los datos: " + e.getMessage());
        }

        return listafila;
    }

    public List<ArchivosUrlDTO> obtieneInfo() {

        try {
            listafila = archivosUrlDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("ArchivoUrlBI||obtieneInfo||No fue posible obtener datos de la tabla Archivos Url: " + e.getMessage());
        }

        return listafila;
    }

    public boolean actualiza(ArchivosUrlDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = archivosUrlDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("ArchivoUrlBI||actualiza||No fue posible actualizar: " + e.getMessage());
        }

        return respuesta;
    }

}
