package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.IncidenciasDAO;
import com.gruposalinas.migestion.domain.IncidenciasDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class IncidenciasBI {

    private static Logger logger = LogManager.getLogger(IncidenciasBI.class);

    private List<IncidenciasDTO> listafila;

    @Autowired
    IncidenciasDAO incidenciasDAO;

    public int inserta(IncidenciasDTO bean) {
        int respuesta = 0;

        try {
            respuesta = incidenciasDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public int insertaLimpieza(IncidenciasDTO bean) {
        int respuesta = 0;

        try {
            respuesta = incidenciasDAO.insertaLimpieza(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public boolean elimina(String idTipo) {
        boolean respuesta = false;

        try {
            respuesta = incidenciasDAO.elimina(idTipo);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public boolean depura() {
        boolean respuesta = false;

        try {
            respuesta = incidenciasDAO.depura();
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<IncidenciasDTO> obtieneDatos(int idTipo) {

        try {
            listafila = incidenciasDAO.obtieneDatos(idTipo);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<IncidenciasDTO> obtieneInfo() {

        try {
            listafila = incidenciasDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Incidencia");

        }

        return listafila;
    }

    public List<IncidenciasDTO> obtieneInfoLimpieza() {

        try {
            listafila = incidenciasDAO.obtieneInfoLimpieza();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Incidencia");

        }

        return listafila;
    }

    public boolean actualiza(IncidenciasDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = incidenciasDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
