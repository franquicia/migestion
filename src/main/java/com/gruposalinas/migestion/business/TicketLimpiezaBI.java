package com.gruposalinas.migestion.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.ArrayOfInformacion;
import com.gruposalinas.migestion.clientelimpieza.RemedyStub.Informacion;
import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TicketLimpiezaBI {

    private static Logger logger = LogManager.getLogger(TicketLimpiezaBI.class);

    private ArrayList<String> supervisores;

    ArrayList<Informacion> RPA;
    ArrayList<Informacion> EP;
    ArrayList<Informacion> EPC;
    ArrayList<Informacion> Atendidos;
    ArrayList<Informacion> PPA;

    private DatosEmpMttoBI datosEmpMttoBI;
    private ServiciosRemedyLimpiezaBI serviciosRemedyLimpiezaBI;
    private ProveedoresBI proveedoresBI;
    private TicketCuadrillaBI ticketCuadrillaBI;

    /**
     *
     * @param usuario Usuario del cual se consultaran los tickets , cuando la
     * opcion es 3 se envia en usuario la razon social.
     * @param tipoUsuario 1= Coordinador, 2=Supervisor 3=Proveedor
     * @return
     */
    public String getBandejaTickets(String usuario, int tipoUsuario) {

        String res = null;
        try {

            ArrayOfInformacion arrInformacions;
            ArrayOfInformacion arrInformacionsCerrados;

            serviciosRemedyLimpiezaBI = (ServiciosRemedyLimpiezaBI) GTNAppContextProvider.getApplicationContext().getBean("serviciosRemedyLimpiezaBI");

            /*if (tipoUsuario == 1) { //Coordinador
                arrInformacions = serviciosRemedyLimpiezaBI.ConsultaFoliosCoordinador(Integer.parseInt(usuario), "1");
                arrInformacionsCerrados = serviciosRemedyLimpiezaBI.ConsultaFoliosCoordinador(Integer.parseInt(usuario), "2");
            } else*/ if (tipoUsuario == 2) {// Supervisor
                arrInformacions = serviciosRemedyLimpiezaBI.consultaFoliosSupervidor(Integer.parseInt(usuario), "1");
                arrInformacionsCerrados = serviciosRemedyLimpiezaBI.consultaFoliosSupervidor(Integer.parseInt(usuario), "2");
            } else {
                arrInformacions = serviciosRemedyLimpiezaBI.consultaFoliosProveedor(usuario.trim(), "1");
                arrInformacionsCerrados = serviciosRemedyLimpiezaBI.consultaFoliosProveedor(usuario.trim(), "2");
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            RPA = new ArrayList<Informacion>();
            EP = new ArrayList<Informacion>();
            EPC = new ArrayList<Informacion>();
            Atendidos = new ArrayList<Informacion>();
            PPA = new ArrayList<Informacion>();

            supervisores = new ArrayList<String>();

            /*
             * Separara incidentes en las bandejas correspondientes;
             */
            separaBandejas(arrInformacions, tipoUsuario);
            separaBandejas(arrInformacionsCerrados, tipoUsuario);

            datosEmpMttoBI = (DatosEmpMttoBI) GTNAppContextProvider.getApplicationContext().getBean("datosEmpMttoBI");

            /*
             * Poner nombres de los supervisores en base al ID
             */
            //JsonArray supervisores = matchSupervisores();
            JsonArray arrayRPA = trabajaBandeja(RPA);
            JsonArray arrayEP = trabajaBandeja(EP);
            JsonArray arrayEPC = trabajaBandeja(EPC);
            JsonArray arrayAtendidos = trabajaBandeja(Atendidos);
            JsonArray arrayPPA = trabajaBandeja(PPA);

            if (arrInformacions == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", arrayRPA == null ? new JsonArray() : arrayRPA);
                envoltorioJsonObj.add("EN_PROCESO", arrayEP == null ? new JsonArray() : arrayEP);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", arrayEPC == null ? new JsonArray() : arrayEPC);
                envoltorioJsonObj.add("ATENDIDOS", arrayAtendidos == null ? new JsonArray() : arrayAtendidos);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", arrayPPA == null ? new JsonArray() : arrayPPA);
                //envoltorioJsonObj.add("SUPERVISORES", supervisores == null ? new JsonArray() : supervisores);

            }

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);

            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }

    }

    public String getBandejaCuadrilla(String proveedor, int idCuadrilla, int zona) {

        String res = null;
        try {

            ArrayOfInformacion arrInformacions;
            ArrayOfInformacion arrInformacionsCerrados;

            proveedoresBI = (ProveedoresBI) GTNAppContextProvider.getApplicationContext().getBean("proveedoresBI");;
            ticketCuadrillaBI = (TicketCuadrillaBI) GTNAppContextProvider.getApplicationContext().getBean("ticketCuadrillaBI");

            List<TicketCuadrillaDTO> lista = ticketCuadrillaBI.obtieneCuadrillaTK(idCuadrilla, Integer.parseInt(proveedor), zona);

            List<ProveedoresDTO> lista2 = proveedoresBI.obtieneDatos(Integer.parseInt(proveedor));
            String razonSocial = "";
            for (ProveedoresDTO aux2 : lista2) {
                //razonSocial = aux2.getRazonSocial();
                razonSocial = aux2.getNombreCorto();
            }

            serviciosRemedyLimpiezaBI = (ServiciosRemedyLimpiezaBI) GTNAppContextProvider.getApplicationContext().getBean("serviciosRemedyLimpiezaBI");
            datosEmpMttoBI = (DatosEmpMttoBI) GTNAppContextProvider.getApplicationContext().getBean("datosEmpMttoBI");

            arrInformacions = serviciosRemedyLimpiezaBI.consultaFoliosProveedor(razonSocial.trim(), "1");
            arrInformacionsCerrados = serviciosRemedyLimpiezaBI.consultaFoliosProveedor(razonSocial.trim(), "2");

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            RPA = new ArrayList<Informacion>();
            EP = new ArrayList<Informacion>();
            EPC = new ArrayList<Informacion>();
            Atendidos = new ArrayList<Informacion>();
            PPA = new ArrayList<Informacion>();

            supervisores = new ArrayList<String>();

            /*
             * Poner nombres de los supervisores en base al ID
             */
            JsonArray supervisores = matchSupervisores();

            if (arrInformacions == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {

                /*
                 * Separa tickets de la cuadrilla del proveedor
                 */
                Informacion[] incidentesAbiertos = separaTicketCuadrilla(arrInformacions, lista);
                Informacion[] incidentesCerrados = separaTicketCuadrilla(arrInformacionsCerrados, lista);

                arrInformacions.setInformacion(incidentesAbiertos);
                arrInformacionsCerrados.setInformacion(incidentesCerrados);

                /*
                 * Separara incidentes en las bandejas correspondientes;
                 */
                separaBandejasTecnico(arrInformacions);
                separaBandejasTecnico(arrInformacionsCerrados);

                JsonArray arrayRPA = trabajaBandeja(RPA);
                JsonArray arrayEP = trabajaBandeja(EP);
                JsonArray arrayEPC = trabajaBandeja(EPC);
                JsonArray arrayAtendidos = trabajaBandeja(Atendidos);
                JsonArray arrayPPA = trabajaBandeja(PPA);

                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", arrayRPA == null ? new JsonArray() : arrayRPA);
                envoltorioJsonObj.add("EN_PROCESO", arrayEP == null ? new JsonArray() : arrayEP);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", arrayEPC == null ? new JsonArray() : arrayEPC);
                envoltorioJsonObj.add("ATENDIDOS", arrayAtendidos == null ? new JsonArray() : arrayAtendidos);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", arrayPPA == null ? new JsonArray() : arrayPPA);
                envoltorioJsonObj.add("SUPERVISORES", supervisores == null ? new JsonArray() : supervisores);
                //envoltorioJsonObj.add("SUPERVISORES", null);
            }

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }

    }

    public Informacion[] separaTicketCuadrilla(ArrayOfInformacion incidentes, List<TicketCuadrillaDTO> ticketsCuadrilla) {

        Informacion[] incidentesArray = incidentes.getInformacion();

        ArrayList<Informacion> incidentesCuadrilla = new ArrayList<>();

        if (ticketsCuadrilla != null && ticketsCuadrilla.size() > 0) {
            //Sepra los ticket del proveddor
            for (TicketCuadrillaDTO ticket : ticketsCuadrilla) {

                if (incidentesArray != null) {
                    for (int i = 0; i < incidentesArray.length; i++) {

                        if (ticket.getTicket().equals(String.valueOf(incidentesArray[i].getIdIncidencia()))) {

                            incidentesCuadrilla.add(incidentesArray[i]);

                            break;
                        }

                    }
                }

            }
        }

        Informacion[] regreso = null;

        if (incidentesCuadrilla != null && incidentesCuadrilla.size() > 0) {
            regreso = incidentesCuadrilla.toArray(new Informacion[incidentesCuadrilla.size()]);
        }

        return regreso;
    }

    public JsonArray matchSupervisores() {

        // -------------------------------------------------------------------------------
        JsonArray supervisoresJsonArray = new JsonArray();
        if (!this.supervisores.isEmpty() && supervisores != null) {
            for (String sup : this.supervisores) {
                JsonObject incJsonObj = new JsonObject();
                if (!sup.matches("[^0-9]*")) {

                    List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(sup));
                    if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                            incJsonObj.addProperty("SUPERVISOR", emp_aux.getNombre());
                            supervisoresJsonArray.add(incJsonObj);
                        }
                    }
                } else {
                    System.out.println("No es un número");
                    incJsonObj.addProperty("SUPERVISOR", "JUAN PEREZ");
                    supervisoresJsonArray.add(incJsonObj);
                }
            }
        }

        return supervisoresJsonArray;

    }

    public void separaBandejas(ArrayOfInformacion bandejaPrincipal, int tipoUsuario) {

        if (bandejaPrincipal != null) {
            Informacion[] arregloInc = bandejaPrincipal.getInformacion();
            if (arregloInc != null) {

                try {

                    for (Informacion aux : arregloInc) {

                        //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()+ ", ");
                        if (aux.getEstado().contains("Recibido por atender")
                                && (aux.getMotivoEstado().toLowerCase().contains("asignado por aceptar")
                                || aux.getMotivoEstado().toLowerCase().contains("revisado")
                                || aux.getMotivoEstado().toLowerCase().contains("por asignar")
                                || aux.getMotivoEstado().toLowerCase().contains("rechazado proveedor"))) {

                            if (tipoUsuario == 3 && (aux.getMotivoEstado().toLowerCase().contains("rechazado proveedor") || aux.getMotivoEstado().toLowerCase().contains("por asignar"))) {
                                RPA.add(aux);

                            } else {
                                RPA.add(aux);
                            }
                        }
                        if (aux.getEstado().contains("En Proceso")) {

                            if (tipoUsuario != 3 && (aux.getMotivoEstado().toLowerCase().contains("asignado a proveedor"))) {
                                EP.add(aux);
                            }

                            if (tipoUsuario == 3 && (aux.getMotivoEstado().toLowerCase().contains("asignado a proveedor"))) {
                                EP.add(aux);
                            }
                            //EPC.add(aux);
                        }
                        if (aux.getEstado().contains("En Recepción")
                                && (aux.getMotivoEstado().toLowerCase().contains("en validacion")
                                || aux.getMotivoEstado().toLowerCase().contains("cierre técnico")
                                || aux.getMotivoEstado().toLowerCase().contains("rechazado por cliente"))) {
                            EPC.add(aux);
                        }
                        if (aux.getEstado().contains("Atendido") && (aux.getMotivoEstado().toLowerCase().contains("atendido")
                                || aux.getMotivoEstado().toLowerCase().contains("cancelado correctivo menor")
                                || aux.getMotivoEstado().toLowerCase().contains("monto no autorizado")
                                || aux.getMotivoEstado().toLowerCase().contains("cancelado no aplica"))) {

                            if (tipoUsuario == 3 && (aux.getMotivoEstado().toLowerCase().contains("cancelado correctivo menor") || aux.getMotivoEstado().toLowerCase().contains("cancelado no aplica"))) {

                            } else {
                                Atendidos.add(aux);
                            }

                        }
                        if (aux.getEstado().contains("Pendiente")) {
                            //PPA.add(aux);
                        }

                        if (tipoUsuario != 3 && aux.getEstado().toLowerCase().contains("pendiente por atender") && aux.getMotivoEstado().toLowerCase().contains("cotización por aprobar")) {
                            PPA.add(aux);
                        }
                        if (supervisores.isEmpty()) {
                            supervisores.add(aux.getIdCorpSupervisor());
                        } else {
                            boolean flagSup = false;
                            for (String sup : supervisores) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    if (sup != null && sup.equals(aux.getIdCorpSupervisor())) {
                                        flagSup = true;
                                        break;
                                    }
                                }
                            }
                            if (flagSup == false) {
                                if (aux.getIdCorpSupervisor() != null) {
                                    supervisores.add(aux.getIdCorpSupervisor());
                                }
                            }
                        }

                    }
                } catch (Exception e) {
                    logger.info("No fue posible separar las bandejas");
                }

            }
        }

    }

    public void separaBandejasTecnico(ArrayOfInformacion bandejaPrincipal) {

        if (bandejaPrincipal != null) {
            Informacion[] arregloInc = bandejaPrincipal.getInformacion();
            if (arregloInc != null) {
                for (Informacion aux : arregloInc) {
                    //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()+ ", ");
                    if (aux.getEstado().contains("Recibido por atender")
                            && (aux.getMotivoEstado().toLowerCase().contains("abierto")
                            || aux.getMotivoEstado().toLowerCase().contains("revisado"))) {
                        RPA.add(aux);
                    }
                    if (aux.getEstado().toLowerCase().contains("en proceso")) {

                        if (aux.getMotivoEstado().toLowerCase().contains("asignado a proveedor")) {
                            EP.add(aux);
                        }
                        //EP.add(aux);
                        //EPC.add(aux);
                    }
                    if (aux.getEstado().contains("En Recepción")
                            && (aux.getMotivoEstado().toLowerCase().contains("en validacion")
                            || aux.getMotivoEstado().toLowerCase().contains("rechazado por cliente"))) {
                        EPC.add(aux);
                    }
                    if (aux.getEstado().contains("Atendido") && aux.getMotivoEstado().toLowerCase().contains("atendido")) {
                        Atendidos.add(aux);
                    }
                    if (aux.getEstado().contains("Pendiente")) {
                        //PPA.add(aux);
                    }
                    if (supervisores.isEmpty()) {
                        supervisores.add(aux.getIdCorpSupervisor());
                    } else {
                        boolean flagSup = false;
                        for (String sup : supervisores) {
                            if (aux.getIdCorpSupervisor() != null) {
                                if (sup != null && sup.equals(aux.getIdCorpSupervisor())) {
                                    flagSup = true;
                                    break;
                                }
                            }
                        }
                        if (flagSup == false) {
                            if (aux.getIdCorpSupervisor() != null) {
                                supervisores.add(aux.getIdCorpSupervisor());
                            }
                        }
                    }

                }
            }
        }

    }

    public JsonArray trabajaBandeja(ArrayList<Informacion> bandeja) {

        JsonArray respuesta = null;

        if (!bandeja.isEmpty() && bandeja != null) {

            /*ArrayList<Informacion> Critica = new ArrayList<Informacion>();
            ArrayList<Informacion> normal = new ArrayList<Informacion>();

            for (Informacion aux : bandeja) {
                if (aux.getPrioridad().getValue().contains("Normal")) {
                    normal.add(aux);
                }
                if (aux.getPrioridad().getValue().contains("Critica")) {
                    Critica.add(aux);
                }
            }*/
            //respuesta = arrOrdenadoSupervisor(Critica, normal);
            respuesta = arrOrdenadoSupervisorJuntos(bandeja);

        }

        return respuesta;

    }

    public JsonArray arrOrdenadoSupervisor(ArrayList<Informacion> critica, ArrayList<Informacion> normal) {

        JsonArray arrJsonArray = new JsonArray();

        armaJson(critica, arrJsonArray);
        armaJson(normal, arrJsonArray);

        return arrJsonArray;
    }

    public JsonArray arrOrdenadoSupervisorJuntos(ArrayList<Informacion> juntos) {

        JsonArray arrJsonArray = new JsonArray();

        armaJson(juntos, arrJsonArray);

        return arrJsonArray;
    }

    public void armaJson(ArrayList<Informacion> listaTickets, JsonArray arrayRespuesta) {

        proveedoresBI = (ProveedoresBI) GTNAppContextProvider.getApplicationContext().getBean("proveedoresBI");;

        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();
        //HashMap<String, String> EmpleadosRemedyHashMap = serviciosRemedyLimpiezaBI.consultaUsuariosRemedy();

        while (!listaTickets.isEmpty()) {
            int pos = 0;

            for (int i = 0; i < listaTickets.size(); i++) {
                if (listaTickets.get(pos).getFechaCreacion().compareTo(listaTickets.get(i).getFechaCreacion()) < 0) {
                    pos = i;
                }
            }

            Informacion aux = listaTickets.get(pos);

            //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
            JsonObject incJsonObj = new JsonObject();

            //aux.getAutorizaCambioMonto();
            incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
            incJsonObj.addProperty("ESTADO", aux.getEstado());
            incJsonObj.addProperty("FALLA", aux.getFalla());
            incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
            incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());

            incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
            incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
            incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
            incJsonObj.addProperty("NUMEMP_COORDINADOR", 0);
            //incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());

            String idCliente = aux.getIdCorpCliente();

            if (idCliente != null) {
                if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                    List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                    if (!datos_cliente.isEmpty() && datos_cliente != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                            incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                            incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                            EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                        incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                    incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                    incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                }
            } else {
                incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
            }
            //String idCord = aux.getIDCorpCoordinador();
            String idCord = null;
            if (idCord != null) {
                /*if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                }*/
            } else {
                incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
            }
            if (idCord != null) {
                /*if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                    List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                    if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                            incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            EmpleadosHashMap.put(idCord.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.addProperty("COORDINADOR", "* Sin información");
                    }
                } else {
                    DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                    incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                }*/

            } else {
                incJsonObj.addProperty("COORDINADOR", "* Sin información");
            }

            //String idSup = aux.getIdCorpSupervisor();
            String idSup = null;

            if (idSup != null) {
                /*if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                }*/
            } else {
                incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
            }
            if (idSup != null) {
                if (!idSup.matches("[^0-9]*")) {

                    if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                        List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                        if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                EmpleadosHashMap.put(idSup.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                            incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                        incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                        incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }
            } else {
                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
            }

            incJsonObj.addProperty("NOTAS", aux.getNotas());
            incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
            incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

            if (aux.getProveedor() != null) {
                if (!lista.isEmpty()) {
                    for (ProveedoresDTO aux3 : lista) {
                        if (aux3.getStatus().contains("Activo")) {
                            if (aux.getProveedor().toLowerCase().trim()
                                    .contains(aux3.getNombreCorto().toLowerCase().trim())) {//cambieRazonSocial
                                incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                incJsonObj.addProperty("MENU", aux3.getMenu());
                                incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                break;
                            }

                        }
                    }
                }
            } else {
                String nomCorto = null;
                incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                incJsonObj.addProperty("MENU", nomCorto);
                incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
            }

            incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
            incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
            incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
            incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
            incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

            incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
            incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
            Date date; //= aux.getFechaCierre().getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date1; //= format1.format(date);
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "0001-01-01 00:00:00");
            date = aux.getFechaCreacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_CREACION", date1);
            date = aux.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.addProperty("FECHA_MODIFICACION", date1);
            //date = aux.getFechaProgramada().getTime();
            //date1 = format1.format(date);
            //incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
            incJsonObj.addProperty("FECHA_PROGRAMADA", "0001-01-01 00:00:00");
            incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
            incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());

            String autCleinte = aux.getAutorizacionCliente();
            if (autCleinte != null && !autCleinte.equals("")) {
                if (autCleinte.contains("Rechazado")) {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                }
            } else {
                incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
            }

            if (aux.getTipoAtencion() != null) {
                incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("TIPO_ATENCION", x);
            }
            if (aux.getOrigen() != null) {
                incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
            } else {
                String x = null;
                incJsonObj.addProperty("ORIGEN", x);
            }

            //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
            incJsonObj.addProperty("LATITUD_FIN", "");
            incJsonObj.addProperty("LONGITUD_FIN", "");

            //incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);
            incJsonObj.addProperty("FECHA_INICIO_ATENCION", "0001-01-01 00:00:00");

            incJsonObj.addProperty("ID_CUADRILLA", 0);
            incJsonObj.addProperty("LIDER", "");
            incJsonObj.addProperty("CORREO", "");
            incJsonObj.addProperty("SEGUNDO", "");
            incJsonObj.addProperty("ZONA", 0);

            incJsonObj.addProperty("BANDERA_CMONTO", 0);
            incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
            incJsonObj.addProperty("BANDERAS", 0);

            incJsonObj.addProperty("FECHA_ATENCION", "0001-01-01 00:00:00");
            incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", "0001-01-01 00:00:00");
            incJsonObj.addProperty("FECHA_CIERRE_TECNICO", "0001-01-01 00:00:00");
            incJsonObj.addProperty("FECHA_CIERRE", "0001-01-01 00:00:00");
            incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", "0001-01-01 00:00:00");
            incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", "0001-01-01 00:00:00");
            incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", "0001-01-01 00:00:00");
            incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);

            if (aux.getEvalCliente() != null) {

                //incJsonObj.addProperty("CALIFICACION", aux.getEvalCliente().getValue());
                int cal = 0;
                if (aux.getEvalCliente().getValue().contains("NA")) {
                    cal = 1;
                }
                if (aux.getEvalCliente().getValue().contains("E0a50")) {
                    cal = 2;
                }
                if (aux.getEvalCliente().getValue().contains("E51a69")) {
                    cal = 3;
                }
                if (aux.getEvalCliente().getValue().contains("E70a89")) {
                    cal = 4;
                }
                if (aux.getEvalCliente().getValue().contains("E90a100")) {
                    cal = 5;
                }

                incJsonObj.addProperty("CALIFICACION", cal);

            } else {
                incJsonObj.addProperty("CALIFICACION", 0);
            }

            incJsonObj.addProperty("MONTO_REAL", 0.0);
            incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "0001-01-01 00:00:00");
            incJsonObj.addProperty("DIAS_APLAZAMIENTO", "0");
            incJsonObj.addProperty("MONTO_REAL", 0.0);
            incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "0001-01-01 00:00:00");
            incJsonObj.addProperty("DIAS_APLAZAMIENTO", 0);

            incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", "");
            incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", "");

            incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "0001-01-01 00:00:00");
            incJsonObj.addProperty("DECLINAR_PROVEEDOR", "");

            incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", "");
            incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", "");

            incJsonObj.addProperty("LATITUD_INICIO", "");
            incJsonObj.addProperty("LONGITUD_INICIO", "");

            incJsonObj.addProperty("FECHA_CANCELACION", "0001-01-01 00:00:00");

            //-------------------------------------------------------------------------------------
            incJsonObj.addProperty("CLIENTE_AVISADO", "");
            incJsonObj.addProperty("RESOLUCION", "");
            incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", 0.0);
            incJsonObj.addProperty("CLIENTE_AVISADO", "");
            incJsonObj.addProperty("RESOLUCION", "");
            incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", 0.0);

            arrayRespuesta.add(incJsonObj);

            listaTickets.remove(pos);
        }

    }

}
