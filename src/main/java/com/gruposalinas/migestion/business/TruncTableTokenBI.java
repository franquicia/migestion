package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.TruncTableTokenDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TruncTableTokenBI {

    private Logger logger = LogManager.getLogger(IncidentesBI.class);

    @Autowired
    TruncTableTokenDAO truncTableTokenDAO;

    public boolean truncTable() {
        boolean respuesta = false;

        try {
            respuesta = truncTableTokenDAO.truncTable();
        } catch (Exception e) {
            logger.info("No fue posible truncar la tabla GESTION.GETATOKEN " + e);

        }

        return respuesta;
    }
}
