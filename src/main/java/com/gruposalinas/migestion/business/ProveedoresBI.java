package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ProveedoresDAO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProveedoresBI {

    private static Logger logger = LogManager.getLogger(ProveedoresBI.class);

    private List<ProveedoresDTO> listafila;

    @Autowired
    ProveedoresDAO proveedoresDAO;

    public int inserta(ProveedoresDTO bean) {
        int respuesta = 0;

        try {
            respuesta = proveedoresDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public int insertaLimpieza(ProveedoresDTO bean) {
        int respuesta = 0;

        try {
            respuesta = proveedoresDAO.insertaLimpieza(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public boolean elimina(String idProveedor) {
        boolean respuesta = false;

        try {
            respuesta = proveedoresDAO.elimina(idProveedor);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public boolean depura() {
        boolean respuesta = false;

        try {
            respuesta = proveedoresDAO.depura();
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<ProveedoresDTO> obtieneDatos(int idProveedor) {

        try {
            listafila = proveedoresDAO.obtieneDatos(idProveedor);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos" + e);

        }

        return listafila;
    }

    public List<ProveedoresDTO> obtieneInfo() {

        try {
            listafila = proveedoresDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Incidencia");

        }

        return listafila;
    }

    public List<ProveedoresDTO> obtieneInfoLimpieza() {

        try {
            listafila = proveedoresDAO.obtieneInfoLimpieza();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Incidencia");

        }

        return listafila;
    }

    public boolean actualiza(ProveedoresDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = proveedoresDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
