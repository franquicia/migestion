package com.gruposalinas.migestion.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.dao.MysteryDAOImpl;
import com.gruposalinas.migestion.domain.MysteryDTO;

public class MysteryBI {

    private static Logger logger = LogManager.getLogger(MysteryBI.class);

    private List<MysteryDTO> listaCecos;

    private List<MysteryDTO> listaNivel;

    @Autowired
    MysteryDAOImpl misteryDAO;

    public List<MysteryDTO> consultaCecos(String idCeco) {

        try {
            listaCecos = misteryDAO.consultaCecos(idCeco);
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }

        return listaCecos;
    }

    public List<MysteryDTO> consultaNivel(String idCeco) {

        try {
            listaNivel = misteryDAO.consultaNivel(idCeco);
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }

        return listaNivel;
    }

}
