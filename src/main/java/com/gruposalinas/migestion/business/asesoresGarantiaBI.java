package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import mx.com.gsalinas.encryption.AES;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class asesoresGarantiaBI extends Thread {

    private static Logger logger = LogManager.getLogger(asesoresGarantiaBI.class);

    private String ceco;
    private String fecha;
    private String respuesta;
    private int opcion;
    private boolean bandera;

    public asesoresGarantiaBI(String ceco, String fecha, int opcion) {
        this.ceco = ceco;
        this.fecha = fecha;
        this.opcion = opcion;
    }

    public String getRespuesta() {
        return this.respuesta;
    }

    @Override
    public void run() {

        if (this.opcion == 1) {
            getDetalleGarantia(ceco, fecha);
            logger.info("Termino");
        }

        if (this.opcion == 2) {
            consulta(ceco, fecha);
            logger.info("Termino");
        }
        //respuesta=MiGestionPruebas.getDetalleGarantia(ceco, fecha);

        //logger.info("Termino");
    }

    public void getDetalleGarantia(String strCC, String strPeriodo) {

        String llave = "1Nd1Rh17";
        String usuario = "USR_FRANQUICIAS";
        String password = "fr4nqu1c1as";
        String usuarioCifrado = "";
        String passwordCifrado = "";
        String salida = null;

        try {
            usuarioCifrado = AES.code(llave, usuario);
            passwordCifrado = AES.code(llave, password);
        } catch (Exception e) {
            logger.info("Algo Ocurrio en cifrado: " + e.getMessage());
        }

        BufferedReader rd = null;
        OutputStreamWriter ou = null;
        InputStreamReader inputStream = null;
        try {

            URL url = new URL(GTNConstantes.getURLdetalleGarantiaRH());
            //URL url = new URL("http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getDetalleGarantia");
            logger.info("url : " + url);

            String data = URLEncoder.encode("strUsuario", "UTF-8") + "=" + URLEncoder.encode(usuarioCifrado, "UTF-8");
            data += "&" + URLEncoder.encode("strContrasenia", "UTF-8") + "=" + URLEncoder.encode(passwordCifrado, "UTF-8");
            data += "&" + URLEncoder.encode("strCC", "UTF-8") + "=" + URLEncoder.encode(strCC, "UTF-8");
            data += "&" + URLEncoder.encode("strPeriodo", "UTF-8") + "=" + URLEncoder.encode(strPeriodo, "UTF-8");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            logger.info("http : " + conn);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            //OutputStreamWriter ou = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            ou = new OutputStreamWriter(conn.getOutputStream());
            ou.write(data);
            ou.flush();

            StringBuffer res = new StringBuffer();
            //BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream
            inputStream = new InputStreamReader(conn.getInputStream());
            rd = new BufferedReader(inputStream);
            String line = "";

            logger.info("rdrdrdrd" + rd);
            while ((line = rd.readLine()) != null) {
                res.append(line);
            }
            logger.info("rdrdrdrd" + res.toString());
            //ou.close();
            //rd.close();
            if (res.length() <= 2) {
                salida = null;
                //return null;
            } else {
                //return res.toString();
                salida = res.toString();
            }
        } catch (Exception e) {
            logger.info("Algo ocurrio en conexion: " + e.getMessage());
            //return null;
            salida = null;
        } finally {
            try {

                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }

            try {
                if (ou != null) {
                    ou.close();
                }
            } catch (Exception e) {
                logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }

            try {
                if (inputStream != null) {
                    inputStream.close();
                }

            } catch (Exception e) {
                logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
        }

        this.respuesta = salida;
        this.bandera = true;
    }

    public void consulta(String strCC, String strPeriodo) {

        String llave = "1Nd1Rh17";
        String usuario = "USR_FRANQUICIAS";
        String password = "fr4nqu1c1as";
        String usuarioCifrado = "";
        String passwordCifrado = "";

        String salida = null;

        try {
            usuarioCifrado = AES.code(llave, usuario);
            passwordCifrado = AES.code(llave, password);
        } catch (Exception e) {
            logger.info("Algo Ocurrio en cifrado: " + e.getMessage());
        }

        OutputStreamWriter ou = null;
        BufferedReader rd = null;

        InputStreamReader inputStream = null;
        try {

            URL url = new URL(GTNConstantes.getURLIndicadoresRH());
            //URL url = new URL("http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getIndicadores");
            logger.info("url : " + url);

            /*
             * HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
             * SSLContext cont = SSLContext.getInstance("TLS");
             * cont.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
             * HttpsURLConnection.setDefaultSSLSocketFactory(cont.getSocketFactory());
             *
             * String urlParameters = "strUsuario="+usuarioCifrado+"&strContrasenia="+passwordCifrado+"&strCC="+strCC+"&strPeriodo="+strPeriodo;
             */
            String data = URLEncoder.encode("strUsuario", "UTF-8") + "=" + URLEncoder.encode(usuarioCifrado, "UTF-8");
            data += "&" + URLEncoder.encode("strContrasenia", "UTF-8") + "=" + URLEncoder.encode(passwordCifrado, "UTF-8");
            data += "&" + URLEncoder.encode("strCC", "UTF-8") + "=" + URLEncoder.encode(strCC, "UTF-8");
            data += "&" + URLEncoder.encode("strPeriodo", "UTF-8") + "=" + URLEncoder.encode(strPeriodo, "UTF-8");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            logger.info("http : " + conn);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            //OutputStreamWriter ou = new OutputStreamWriter(conn.getOutputStream());//Cerrar Stream
            ou = new OutputStreamWriter(conn.getOutputStream());
            ou.write(data);
            ou.flush();

            StringBuffer res = new StringBuffer();
            //BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Cerrar Stream
            inputStream = new InputStreamReader(conn.getInputStream());
            rd = new BufferedReader(inputStream);//Cerrar Stream
            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }
            //ou.close();
            //rd.close();
            if (res.length() <= 2) {
                //return null;
                salida = null;
            } else {
                //return res.toString();
                salida = res.toString();
            }

        } catch (Exception e) {
            salida = null;
            logger.info("Algo ocurrio en conexion: " + e.getMessage());
            //return null;

        } finally {
            //ou.close();
            //rd.close();
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }

            try {
                if (ou != null) {
                    ou.close();
                }
            } catch (Exception e) {
                logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception ex) {
                logger.info("Algo ocurrio al intentar cerrar los Streams " + ex.getMessage());
            }

        }

        this.respuesta = salida;
        this.bandera = true;
    }

}
