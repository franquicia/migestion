package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.CartaAsignacionAFDAO;
import com.gruposalinas.migestion.domain.CartaAsignacionAFDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CartaAsignacionAFBI {

    private static Logger logger = LogManager.getLogger(CartaAsignacionAFBI.class);

    private List<CartaAsignacionAFDTO> listafila;

    @Autowired
    CartaAsignacionAFDAO cartaAsignacionAFDAO;

    public boolean elimina(String idCarta) {
        boolean respuesta = false;

        try {
            respuesta = cartaAsignacionAFDAO.elimina(idCarta);
        } catch (Exception e) {
            logger.info("CartaAsignacionAFBI||elimina||No fue posible eliminar: " + e.getMessage());
        }

        return respuesta;
    }

    public List<CartaAsignacionAFDTO> obtieneInfo() {

        try {
            listafila = cartaAsignacionAFDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("CartaAsignacionAFBI||obtieneInfo||No fue posible obtener datos de la tabla Carta Asignacion: " + e.getMessage());
        }

        return listafila;
    }

    public boolean actualiza(CartaAsignacionAFDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = cartaAsignacionAFDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("CartaAsignacionAFBI||actualiza||No fue posible actualizar: " + e.getMessage());
        }

        return respuesta;
    }

}
