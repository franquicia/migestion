package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.FotosAntesDespuesDAO;
import com.gruposalinas.migestion.domain.FotosAntesDespuesDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class FotosAntesDespuesBI {

    private static Logger logger = LogManager.getLogger(AgendaBI.class);

    @Autowired
    FotosAntesDespuesDAO fotosAntesDespuesDAO;


    public int actualizaFotoAntesDesp(FotosAntesDespuesDTO bean) {
        int respuesta = 0;
        try {
            respuesta = fotosAntesDespuesDAO.actualizaFotoAntesDesp(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la foto");

        }
        return respuesta;
    }

    public int eliminaFotoAntesDesp(int idEvento) {
        int respuesta = 0;
        try {
            respuesta = fotosAntesDespuesDAO.eliminaFotoAntesDesp(idEvento);
        } catch (Exception e) {
            logger.info("No fue posible eliminar la foto");

        }
        return respuesta;
    }



    public List<FotosAntesDespuesDTO> consultaFotoAntesDespUlt() {
        List<FotosAntesDespuesDTO> respuesta = new ArrayList<FotosAntesDespuesDTO>();
        try {
            respuesta = fotosAntesDespuesDAO.consultaFotoAntesDespUlt();
        } catch (Exception e) {
            logger.info("No fue posible consultar la tabla de fotos antes y después.");

        }
        return respuesta;
    }
}
