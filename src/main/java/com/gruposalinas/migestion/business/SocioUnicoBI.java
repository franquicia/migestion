package com.gruposalinas.migestion.business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.migestion.resources.GTNConstantes;

public class SocioUnicoBI {

    private Logger logger = LogManager.getLogger(SocioUnicoBI.class);

    public String datosEmpleado(int numEmpleado) {

        String respuesta = null;
        String urlStr = GTNConstantes.getURLSocioUnico() + "getDatosEmpleado.htm?idEmpleado=" + numEmpleado;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String getSueno(int numEmpleado) {
        String respuesta = null;
        String urlStr = GTNConstantes.getURLSocioUnico() + "getDatoSueno.htm?idEmpleado=" + numEmpleado;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();

            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;
    }

    public String getEmpleadosFestejados(int numEmpleado) {
        String respuesta = null;
        String urlStr = GTNConstantes.getURLSocioUnico() + "getEmpFestejados.htm?idEmpleado=" + numEmpleado;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;
    }

    public String obtieneTelefonosSucursales() {

        String respuesta = null;
        String urlStr = GTNConstantes.getURLSocioUnico2() + "getDatosTelSucursal.htm";
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneEmpleadosExternos() {

        String respuesta = null;
        String urlStr = GTNConstantes.getURLSocioUnico2() + "getDatosEmpExternos.htm";
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

}
