package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.CedulaDAO;
import com.gruposalinas.migestion.domain.CedulaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CedulaBI {

    private static Logger logger = LogManager.getLogger(CedulaBI.class);

    private List<CedulaDTO> listafila;

    @Autowired
    CedulaDAO cedulaDAO;


    public boolean elimina(String idFila) {
        boolean respuesta = false;

        try {
            respuesta = cedulaDAO.elimina(idFila);
        } catch (Exception e) {
            logger.info("CedulaBI||inserta||No fue posible eliminar: "+e.getMessage());
        }

        return respuesta;
    }




    public List<CedulaDTO> obtieneInfo() {

        try {
            listafila = cedulaDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("CedulaBI||obtieneDatos||No fue posible obtener datos de la tabla Cedula: " + e.getMessage());
        }

        return listafila;
    }

    public boolean actualiza(CedulaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = cedulaDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("CedulaBI||obtieneDatos||No fue posible actualizar: " + e.getMessage());
        }

        return respuesta;
    }

}
