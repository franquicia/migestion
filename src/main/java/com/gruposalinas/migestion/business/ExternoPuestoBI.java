package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ExternoPuestoDAO;
import com.gruposalinas.migestion.domain.ExternoPuestoDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ExternoPuestoBI {

    private static Logger logger = LogManager.getLogger(ExternoPuestoBI.class);

    @Autowired
    ExternoPuestoDAO externoPuestoDAO;

    public boolean actualizaPuesto(int numEmpleado, int puesto) {
        boolean respuesta = false;

        try {
            respuesta = externoPuestoDAO.actualizaPuesto(numEmpleado, puesto);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Usuario");

        }

        return respuesta;
    }

    public boolean actualizaPuestoExterno(ExternoPuestoDTO bean) {

        boolean respuesta = false;

        try {
            respuesta = externoPuestoDAO.actualizaPuestoExterno(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Usuario");

        }

        return respuesta;
    }

}
