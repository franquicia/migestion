package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.CatalogoMinuciasDAO;
import com.gruposalinas.migestion.domain.CatalogoMinuciasDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CatalogoMinuciasBI {

    private static Logger logger = LogManager.getLogger(CatalogoMinuciasBI.class);

    private List<CatalogoMinuciasDTO> listafila;

    @Autowired
    CatalogoMinuciasDAO catalogoMinuciasDAO;



    public boolean elimina(int idMinucia) {
        boolean respuesta = false;

        try {
            respuesta = catalogoMinuciasDAO.elimina(idMinucia);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    //parametro
    public List<CatalogoMinuciasDTO> obtieneDatosEvi(String idMinucia) {

        try {
            listafila = catalogoMinuciasDAO.obtieneDatosEvi(idMinucia);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos Minucias");

        }

        return listafila;
    }

    public List<CatalogoMinuciasDTO> obtieneInfo() {

        try {
            listafila = catalogoMinuciasDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Minucias");

        }

        return listafila;
    }

    public boolean actualiza(CatalogoMinuciasDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = catalogoMinuciasDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar Minucias");

        }

        return respuesta;
    }

    public boolean actualizaEvi(CatalogoMinuciasDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = catalogoMinuciasDAO.actualizaEvi(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar Minucias");

        }

        return respuesta;
    }

}
