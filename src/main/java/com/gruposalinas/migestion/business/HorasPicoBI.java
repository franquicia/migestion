/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.HorasPicoDAOImpl;
import com.gruposalinas.migestion.domain.HorasPicoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author cescobarh
 */
public class HorasPicoBI {

    private static final Logger logger = LogManager.getLogger(HorasPicoBI.class);

    @Autowired
    private HorasPicoDAOImpl horasPicoDAOImpl;

    public List<HorasPicoDTO> getHorasPico(String fiSucursal, String fiAnio, String fiSemana) {
        try {
            return horasPicoDAOImpl.getHorasPico(fiSucursal, fiAnio, fiSemana);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("No fue posible consultar");
            return null;
        }

    }

    public List<HorasPicoDTO> getHorasPicoPaso(String fiSucursal, String fiSemana) {
        try {
            return horasPicoDAOImpl.getHorasPicoPaso(fiSucursal, fiSemana);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("No fue posible consultar");
            return null;
        }

    }

    public boolean setDistribucionHorasPico() {
        try {
            return horasPicoDAOImpl.setDistribucionHorasPico();
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("No fue posible realizar la distribucion de horas pico");
            return false;
        }
    }

}
