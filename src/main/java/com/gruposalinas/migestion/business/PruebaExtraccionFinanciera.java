package com.gruposalinas.migestion.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class PruebaExtraccionFinanciera {

    static ArrayList<Integer> numCeldas = new ArrayList<Integer>();

    static String arrayNombres[] = {"V�gente De 0 A 1", "Temprana 2", "Atrasada 1a De 3 A 7", "Tard�a 1b De 8 A 13", "Atrasada 2 A 13", "Vencida 2a De 14 A 25", "Dificil 2b De 26 A 39", "Vencida De 14 A 39", "Saldo Cartera De 0 A 39 Semanas", "Legal 40 a 55", "Cazadores 56 +", "Saldo De Cartera"};

    @SuppressWarnings("unused")
    public static void main(String args[]) {

        SAXBuilder saxBuilder = new SAXBuilder();

        BufferedReader rd = null;
        StringReader srd = null;
        try {
            rd = new BufferedReader(new FileReader("E:\\Users\\b191312\\Desktop\\archivo.xml"));

            String inputLine = null;
            StringBuilder builder = new StringBuilder();

            //Store the contents of the file to the StringBuilder.
            while ((inputLine = rd.readLine()) != null) {
                builder.append(inputLine);
            }

            //Create a new tokenizer based on the StringReader class instance.
            srd = new StringReader(builder.toString());

        } catch (IOException ex) {
            System.err.println("An IOException was caught: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {

            }

        }

        limpiXmlNormalidad(srd);
        //System.out.println(limpiXmlNormalidad(srd).toString());

    }

    @SuppressWarnings("unchecked")
    public static JsonObject limpiXmlNormalidad(StringReader xmlStr) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {
//
//			/* Convierte String a XML */
//			StringReader stringReader = new StringReader(xmlStr);
//
            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(xmlStr);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            System.out.println("Celldata" + rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                System.out.println("Celda " + element.getAttributeValue("CellOrdinal"));
                numCeldas.add(Integer.parseInt(element.getAttributeValue("CellOrdinal")));

                String valor = element.getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            int[] flagsPagos = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};

            jsonRepuestas = armaJsonPagosServicios(valoresCeldas, flagsPagos, numCeldas);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return jsonRepuestas;

    }

    @SuppressWarnings("unused")
    public static JsonObject armaJsonPagosServicios(List<String> valoresCeldas, int[] flagsColumnas, List<Integer> numCeldas) {

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        try {
            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###.#");
            int columnas = flagsColumnas.length;

            System.out.println("Columnas :" + columnas);
            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - columnas);
            int indiceColumnas = 0;

            while (indice < valoresCeldas.size()) {

                if (flagsColumnas[indiceColumnas] == 1) {
                    System.out.println(formateador.format(Double.parseDouble(valoresCeldas.get(indice))));
                    totales.add(Double.parseDouble(valoresCeldas.get(indice)));
                }

                indiceColumnas++;
                indice++;
            }

            /*Obtener Filas*/
            int cont = 0;
            int veces = 0;
            int corte = 1;

            ArrayList<Double> valores = new ArrayList<Double>();

            while (cont < numCeldas.size() && cont != (valoresCeldas.size() - columnas)) {

                JsonObject valorPorcentaje = null;

                if ((corte - 1) == 2) {
                    corte++;
                    System.out.println("Mi contador es :" + cont + " no debo tomar el valor");
                } else {
                    System.out.println("Mi contador es :" + cont + " si debo tomar el valor");
//					System.out.println("Num. Celda: "+numCeldas.get(cont));
//					System.out.println("Corte: "+(corte-1));
                    if (veces != numCeldas.get(cont)) {
                        break;
                    }

                    if (flagsColumnas[corte - 1] == 1) {
                        valores.add(Double.parseDouble(valoresCeldas.get(cont)));
                        System.out.println("$" + formateador.format(Double.parseDouble(valoresCeldas.get(cont))));
                        //System.out.println("$"+Double.parseDouble(valoresCeldas.get(cont)));
                    }

                    if (corte == flagsColumnas.length) {
                        System.out.println("Agrega Fila");
                        corte = 1;
                        veces += 1;
                    } else {
                        corte++;
                        veces++;
                    }

                    cont++;
                }

            }

            // int positionTotales = 0;
            int veces1 = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i < valores.size(); i++) {

                veces1++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "verde";

                if (veces1 > 4) {
                    valor = valores.get(i) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = valores.get(i - pos_vs) / 1000.0;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = valores.get(i) / 1000;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(Math.abs(valor)));
                valorProcentaje.addProperty("porcentaje", Math.abs((int) Math.round(porcentaje)) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces1 == 7) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces1 = 0;
                    pos_vs = 2;
                }
            }
            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.size(); a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 3) {
                    porcentajeTot = (double) totales.get(a) / (double) totales.get(a - pos_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(Math.abs(totales.get(a))));
                jsonTotales.addProperty("porcentaje", "" + Math.abs((int) Math.round(porcentajeTot)) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);

        } catch (Exception e) {
            e.printStackTrace();
            jsonDias = null;
        }

        return jsonDias;

    }

    static protected String getdia(int nuDia) {
        String dia = "";
        if (nuDia == 1) {
            dia = "lunes";
        } else if (nuDia == 2) {
            dia = "martes";
        } else if (nuDia == 3) {
            dia = "miercoles";
        } else if (nuDia == 4) {
            dia = "jueves";
        } else if (nuDia == 5) {
            dia = "viernes";
        } else if (nuDia == 6) {
            dia = "sabado";
        } else if (nuDia == 7) {
            dia = "domingo";
        }
        return dia;

    }
}
