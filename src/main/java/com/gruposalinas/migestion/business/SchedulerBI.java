package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.domain.SchedulerDTO;
import com.gruposalinas.migestion.domain.TareaDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SchedulerBI implements Job {

    @Autowired
    TareasBI tareasBI;

    private static final Logger logger = LogManager.getLogger(SchedulerBI.class);
    private static SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
    private static Map<String, Scheduler> schedArray = new LinkedHashMap<String, Scheduler>();
    private static Map<String, JobDetail> jobDetailArray = new LinkedHashMap<String, JobDetail>();
    private static int aut = 0;

    public void execute(JobExecutionContext cntxt) throws JobExecutionException {

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaToken")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO eliminaToken... ");
            try {
                TokenBI tokenBI = (TokenBI) GTNAppContextProvider.getApplicationContext().getBean("tokenBI");
                logger.info("Token... " + tokenBI.eliminaTokenTodos());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("test")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO test... ");
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaGeografia")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaGeografia... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaGeografia... " + cargaBI.cargaGeografia());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaUsuariosPaso")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaUsuariosPaso... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaUsuariosPaso... " + cargaBI.cargaUsuariosPaso());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecosPaso")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaCecosPaso... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaCecosPaso... " + cargaBI.cargaCecosPaso());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaSucursalesPaso")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaSucursalesPaso... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaSucursalesPaso... " + cargaBI.cargaSucursalesPaso());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaPuestos")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaPuestos... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaPuestos... " + cargaBI.cargaPuestos());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaUsuarios")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaUsuarios... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaUsuarios... " + cargaBI.cargaUsuarios());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecos")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaCecos... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaCecos... " + cargaBI.cargaCecos());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaSucursales")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaSucursales... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaSucursales... " + cargaBI.cargaSucursales());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaTareas")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaTareas... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaTareas... " + cargaBI.cargaTareas());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaTareasDos")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaTareasDos... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaTareasDos... " + cargaBI.cargaTareas());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("productividad")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO productividad... ");
            try {
                ProductividadBI extraccionBI = (ProductividadBI) GTNAppContextProvider.getApplicationContext().getBean("extraccionBI");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                String date = sdf.format(new Date());

                extraccionBI.extraccion(date, 1);

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecosGuadalajara")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaCecosGuadalajara... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaTareasDos... " + cargaBI.cargaCecosGuadalajara());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaUsuariosFRQ")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaUsuariosFRQ... ");
            try {
                CargaBI cargaBI = (CargaBI) GTNAppContextProvider.getApplicationContext().getBean("cargaBI");
                logger.info("Se ejecuto cargaUsuariosFRQ... " + cargaBI.cargaUsuariosFRQ());

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }


        //------------------------------------- Trunca tabla de token ------------------------------------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("truncTableTokenBI")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO para truncar tabla de token... ");
            try {
                TruncTableTokenBI truncTableTokenBI = (TruncTableTokenBI) GTNAppContextProvider.getApplicationContext().getBean("truncTableTokenBI");
                truncTableTokenBI.truncTable();

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }

        //-----------------------------------------------------------------------------------------------------
        //------------------------------------- Trunca tabla de token ------------------------------------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("serviciosRemedyBI")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO para cargar empleados de mantenimiento... ");
            try {
                ServiciosRemedyBI serviciosRemedyBI = (ServiciosRemedyBI) GTNAppContextProvider.getApplicationContext().getBean("serviciosRemedyBI");
                serviciosRemedyBI.cargaUsuariosRemedy();

            } catch (Exception e) {
                logger.info("Algo Ocurrió... " + e);
            }
        }

        //-----------------------------------------------------------------------------------------------------
    }

    public void run() throws SchedulerException, UnknownHostException {
        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        //if (aut == 0 && ip.equals("10.53.29.153")) {
        if (aut == 0 && ip.equals(GTNConstantes.getIpHost())) {
            CronTrigger trigger = null;
            List<TareaDTO> listaTareas = null;
            try {
                listaTareas = tareasBI.obtieneTareas();
                Scheduler sched = null;
                JobDetail jobDetail = new JobDetail();
                Iterator<TareaDTO> it = listaTareas.iterator();
                while (it.hasNext()) {
                    /* INICIA EL SCHEDULER */
                    TareaDTO o = it.next();
                    if (o.getActivo() == 1) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + o.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", o.getCveTarea());
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression("*/10 * * * * ?");
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                        /* MODIFICA EL SCHEDULER CON LOS DATOS DE LA BD */
                        sched.unscheduleJob("trigger" + o.getCveTarea(), "Trigger");
                        sched = schedFact.getScheduler();
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression(o.getStrFechaTarea());
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                    } else if (o.getActivo() == 0) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + o.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", o.getCveTarea());
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression("*/10 * * * * ?");
                        /* MODIFICA EL SCHEDULER CON LOS DATOS DE LA BD */
                        sched.unscheduleJob("trigger" + o.getCveTarea(), "Trigger");
                        sched = schedFact.getScheduler();
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression(o.getStrFechaTarea());
                    }
                    schedArray.put(o.getCveTarea(), sched);
                    jobDetailArray.put(o.getCveTarea(), jobDetail);
                }
            } catch (ParseException e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        aut++;
    }

    // AGREGA UNA TAREA
    public boolean newTask(SchedulerDTO scheduler, HttpServletRequest request)
            throws ParseException, UnknownHostException {

        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        //if (ip.equals("10.53.29.153")) {
        if (ip.equals(GTNConstantes.getIpHost())) {
            try {
                //
                String date = "";
                String h = scheduler.getHora(), m = scheduler.getMinutos(), s = scheduler.getSegundos(),
                        d = scheduler.getDia(), month = scheduler.getMes(), diaSemana = scheduler.getDiaSemana();

                if (request.getParameter("h") != null && request.getParameter("h").equals("on")) {
                    h = "*/" + scheduler.getHora();
                }
                if (request.getParameter("m") != null && request.getParameter("m").equals("on")) {
                    m = "*/" + scheduler.getMinutos();
                }
                if (request.getParameter("s") != null && request.getParameter("s").equals("on")) {
                    s = "*/" + scheduler.getSegundos();
                }
                if (request.getParameter("d") != null && request.getParameter("d").equals("on")) {
                    d = "*/" + scheduler.getDia();
                }
                if (request.getParameter("month") != null && request.getParameter("month").equals("on")) {
                    month = "*/" + scheduler.getMes();
                }
                if (request.getParameter("diaSemana") != null && request.getParameter("diaSemana").equals("on")) {
                    month = "*/" + scheduler.getDiaSemana();
                }
                if (!request.getParameter("diaSemana").equals("?")) {
                    d = "?";
                }

                date = s + " " + m + " " + h + " " + d + " " + month + " " + diaSemana;

                TareaDTO tarea = new TareaDTO();
                tarea.setActivo(Integer.parseInt(request.getParameter("nTareaActiva")));
                tarea.setCveTarea(request.getParameter("nombreTarea"));// nombre
                tarea.setStrFechaTarea(date);

                CronTrigger trigger = null;
                @SuppressWarnings("unused")
                List<TareaDTO> listaTareas = null;
                try {
                    listaTareas = tareasBI.obtieneTareas();
                    Scheduler sched = null;
                    JobDetail jobDetail = new JobDetail();
                    if (tarea.getActivo() == 1) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + tarea.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", tarea.getCveTarea());
                        trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                        trigger.setCronExpression(tarea.getStrFechaTarea());
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                    } else if (tarea.getActivo() == 0) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + tarea.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", tarea.getCveTarea());
                        trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                        trigger.setCronExpression(tarea.getStrFechaTarea());
                    }
                    schedArray.put(tarea.getCveTarea(), sched);
                    jobDetailArray.put(tarea.getCveTarea(), jobDetail);
                } catch (SchedulerException e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
                try {
                    tareasBI.insertaTarea(tarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
                return false;
            }
        }
        return true;
    }

    // MODIFICA UNA TAREA
    public boolean task(SchedulerDTO scheduler, HttpServletRequest request)
            throws ParseException, UnknownHostException {

        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        // if (ip.equals("10.53.29.153") ) {
        if (ip.equals(GTNConstantes.getIpHost())) {
            try {
                String date = "";
                String identifier = scheduler.getId();
                String h = scheduler.getHora(), m = scheduler.getMinutos(), s = scheduler.getSegundos(),
                        d = scheduler.getDia(), month = scheduler.getMes(), diaSemana = scheduler.getDiaSemana();

                if (request.getParameter("h") != null && request.getParameter("h").equals("on")) {
                    h = "*/" + scheduler.getHora();
                }
                if (request.getParameter("m") != null && request.getParameter("m").equals("on")) {
                    m = "*/" + scheduler.getMinutos();
                }
                if (request.getParameter("s") != null && request.getParameter("s").equals("on")) {
                    s = "*/" + scheduler.getSegundos();
                }
                if (request.getParameter("d") != null && request.getParameter("d").equals("on")) {
                    d = "*/" + scheduler.getDia();
                }
                if (request.getParameter("month") != null && request.getParameter("month").equals("on")) {
                    month = "*/" + scheduler.getMes();
                }
                if (request.getParameter("diaSemana") != null && request.getParameter("diaSemana").equals("on")) {
                    month = "*/" + scheduler.getDiaSemana();
                }
                if (!request.getParameter("diaSemana").equals("?")) {
                    d = "?";
                }

                date = s + " " + m + " " + h + " " + d + " " + month + " " + diaSemana;

                TareaDTO tarea = new TareaDTO();
                tarea.setActivo(Integer.parseInt(request.getParameter("act" + identifier)));

                if (Integer.parseInt(request.getParameter("act" + identifier)) == 0) {
                    tarea.setStrFechaTarea(request.getParameter("fecha" + identifier));
                } else {
                    tarea.setStrFechaTarea(date);
                }

                tarea.setCveTarea(request.getParameter("idTarea" + identifier));
                tarea.setIdTarea(Integer.parseInt(scheduler.getId()));

                try {
                    tareasBI.actualizaTarea(tarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió...", e);
                    return false;
                }

                CronTrigger trigger = null;
                List<TareaDTO> listaTareas = null;
                try {
                    listaTareas = tareasBI.obtieneTareas();
                    Iterator<TareaDTO> it = listaTareas.iterator();
                    @SuppressWarnings("unused")
                    Scheduler sched = null;
                    while (it.hasNext()) {
                        TareaDTO o = it.next();
                        if (o.getIdTarea() == tarea.getIdTarea() && o.getActivo() == 1) {
                            schedArray.get(tarea.getCveTarea()).unscheduleJob("trigger" + tarea.getCveTarea(),
                                    "Trigger");
                            sched = schedFact.getScheduler();
                            trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                            trigger.setCronExpression(tarea.getStrFechaTarea());
                            schedArray.get(tarea.getCveTarea()).scheduleJob(jobDetailArray.get(tarea.getCveTarea()),
                                    trigger);
                            schedArray.get(tarea.getCveTarea()).start();
                        } else if (o.getIdTarea() == tarea.getIdTarea() && o.getActivo() == 0) {
                            schedArray.get(tarea.getCveTarea()).unscheduleJob("trigger" + tarea.getCveTarea(),
                                    "Trigger");
                        }
                    }
                } catch (SchedulerException e) {
                    logger.info("Algo Ocurrió...", e);
                    return false;
                }

            } catch (Exception e) {
                logger.info("Algo Ocurrió...", e);
                return false;
            }
        }
        return true;
    }

    // ELIMINA UNA TAREA
    public boolean eliminaTask(int idTarea, String cveTarea) throws ParseException, UnknownHostException {
        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        // if (ip.equals("10.53.29.153") ) {
        if (ip.equals(GTNConstantes.getIpHost())) {
            List<TareaDTO> listaTareas = null;
            try {
                listaTareas = tareasBI.obtieneTareas();
                Iterator<TareaDTO> it = listaTareas.iterator();
                while (it.hasNext()) {
                    TareaDTO o = it.next();
                    if (o.getIdTarea() == idTarea) {
                        schedArray.get(cveTarea).unscheduleJob("trigger" + cveTarea, "Trigger");
                        schedArray.get(cveTarea).deleteJob("Job" + cveTarea, "Ejemplo");
                        schedArray.remove(cveTarea);
                        jobDetailArray.remove(cveTarea);
                    }
                }
                try {
                    tareasBI.eliminaTarea(idTarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
            } catch (SchedulerException e) {
                logger.info("Algo Ocurrió... ", e);
                return false;
            }
        }
        return true;
    }
}
