package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.PuestoDAO;
import com.gruposalinas.migestion.domain.PuestoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PuestoBI {

    private static Logger logger = LogManager.getLogger(PuestoBI.class);

    private List<PuestoDTO> listafila;

    @Autowired
    PuestoDAO puestoDAO;

    public int inserta(PuestoDTO bean) {
        int respuesta = 0;

        try {
            respuesta = puestoDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public boolean elimina(String idPuesto) {
        boolean respuesta = false;

        try {
            respuesta = puestoDAO.elimina(idPuesto);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<PuestoDTO> obtieneDatos(int idPuesto, String descripcion) {

        try {
            listafila = puestoDAO.obtieneDatos(idPuesto, descripcion);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<PuestoDTO> obtieneInfo() {

        try {
            listafila = puestoDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Filas");

        }

        return listafila;
    }

    public boolean actualiza(PuestoDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = puestoDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
