package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.TicketCuadrillaDAO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TicketCuadrillaLimpiezaBI {

    private static Logger logger = LogManager.getLogger(TicketCuadrillaLimpiezaBI.class);

    private List<TicketCuadrillaDTO> listafila;

    @Autowired
    TicketCuadrillaDAO ticketCuadrillaDAO;

    public int inserta(TicketCuadrillaDTO bean) {
        int respuesta = 0;

        try {
            respuesta = ticketCuadrillaDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public boolean elimina(String ticket) {
        boolean respuesta = false;

        try {
            respuesta = ticketCuadrillaDAO.elimina(ticket);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<TicketCuadrillaDTO> obtieneCuadrillaTK(int idCuadrilla, int idProveedor, int zona) {

        try {
            listafila = ticketCuadrillaDAO.obtieneCuadrillaTK(idCuadrilla, idProveedor, zona);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<TicketCuadrillaDTO> obtieneDatos(int idCuadrilla) {

        try {
            listafila = ticketCuadrillaDAO.obtieneDatos(idCuadrilla);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<TicketCuadrillaDTO> obtieneInfo() {

        try {
            listafila = ticketCuadrillaDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Tk Cuadrilla");

        }

        return listafila;
    }

    public boolean actualiza(TicketCuadrillaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = ticketCuadrillaDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
