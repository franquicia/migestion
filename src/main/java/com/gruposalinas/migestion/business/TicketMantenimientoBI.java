package com.gruposalinas.migestion.business;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.cliente.RemedyStub;
import com.gruposalinas.migestion.cliente.RemedyStub.ArrayOfInfoIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.Authentication;
import com.gruposalinas.migestion.cliente.RemedyStub.InfoIncidente;
import com.gruposalinas.migestion.cliente.RemedyStub.ListaPersonal;
import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import com.gruposalinas.migestion.domain.ProveedoresDTO;
import com.gruposalinas.migestion.domain.TicketCuadrillaDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TicketMantenimientoBI {

    private static Logger logger = LogManager.getLogger(TicketMantenimientoBI.class);

    private ArrayList<String> supervisores;

    private DatosEmpMttoBI datosEmpMttoBI;
    private ServiciosRemedyBI serviciosRemedyBI;
    private ProveedoresBI proveedoresBI;
    private TicketCuadrillaBI ticketCuadrillaBI;

    /**
     *
     * @param usuario Usuario del cual se consultaran los tickets , cuando la
     * opcion es 3 se envia en usuario la razon social.
     * @param tipoUsuario 1= Coordinador, 2=Supervisor 3=Proveedor
     * @return
     */
    public String getBandejaTickets(String usuario, int tipoUsuario) {

        String res = null;
        try {

            ArrayOfInfoIncidente arrIncidentes;
            ArrayOfInfoIncidente arrIncidentesCerrados;

            serviciosRemedyBI = (ServiciosRemedyBI) GTNAppContextProvider.getApplicationContext().getBean("ServiciosRemedyBI");

            if (tipoUsuario == 1) { //Coordinador

                int usu = (usuario != null && !usuario.isEmpty() && !usuario.equalsIgnoreCase("") ? Integer.parseInt(usuario.trim()) : 0);

                arrIncidentes = serviciosRemedyBI.ConsultaFoliosCoordinador(usu, "1");
                arrIncidentesCerrados = serviciosRemedyBI.ConsultaFoliosCoordinador(usu, "2");
            } else if (tipoUsuario == 2) {// Supervisor

                int usu = (usuario != null && !usuario.isEmpty() && !usuario.equalsIgnoreCase("") ? Integer.parseInt(usuario.trim()) : 0);

                arrIncidentes = serviciosRemedyBI.ConsultaFoliosSupervidor(usu, "1");
                arrIncidentesCerrados = serviciosRemedyBI.ConsultaFoliosSupervidor(usu, "2");
            } else {
                arrIncidentes = serviciosRemedyBI.consultaFoliosProveedor(usuario.trim() + "", "1");
                arrIncidentesCerrados = serviciosRemedyBI.consultaFoliosProveedor(usuario.trim() + "", "2");
            }

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();

            supervisores = new ArrayList<String>();

            /*
                  * Separara incidentes en las bandejas correspondientes;
             */
            separaBandejas(arrIncidentes, RPA, EP, EPC, Atendidos, PPA);
            separaBandejas(arrIncidentesCerrados, RPA, EP, EPC, Atendidos, PPA);

            datosEmpMttoBI = (DatosEmpMttoBI) GTNAppContextProvider.getApplicationContext().getBean("datosEmpMttoBI");

            /*
                  * Poner nombres de los supervisores en base al ID
             */
            JsonArray supervisores = matchSupervisores();

            List<Integer> ticketsErroneos = new ArrayList<>();

            JsonArray arrayRPA = trabajaBandeja(RPA, ticketsErroneos);
            JsonArray arrayEP = trabajaBandeja(EP, ticketsErroneos);
            JsonArray arrayEPC = trabajaBandeja(EPC, ticketsErroneos);
            JsonArray arrayAtendidos = trabajaBandeja(Atendidos, ticketsErroneos);
            JsonArray arrayPPA = trabajaBandeja(PPA, ticketsErroneos);

            if (arrIncidentes == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", arrayRPA == null ? new JsonArray() : arrayRPA);
                envoltorioJsonObj.add("EN_PROCESO", arrayEP == null ? new JsonArray() : arrayEP);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", arrayEPC == null ? new JsonArray() : arrayEPC);
                envoltorioJsonObj.add("ATENDIDOS", arrayAtendidos == null ? new JsonArray() : arrayAtendidos);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", arrayPPA == null ? new JsonArray() : arrayPPA);
                envoltorioJsonObj.add("SUPERVISORES", supervisores == null ? new JsonArray() : supervisores);

            }

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info("Ocurrio algo en getBandejaTickets");

            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }

    }

    public String getBandejaTicketsPaginado(String... params) {

        ArrayList<InfoIncidente> RPA;
        ArrayList<InfoIncidente> EP;
        ArrayList<InfoIncidente> EPC;
        ArrayList<InfoIncidente> Atendidos;
        ArrayList<InfoIncidente> PPA;

        String res = null;

        JsonArray supervisoresActivos = null;

        HashMap< String, Object> mapResult = null;
        try {

            ArrayOfInfoIncidente arrIncidentes;
            ArrayOfInfoIncidente arrIncidentesCerrados;

            serviciosRemedyBI = (ServiciosRemedyBI) GTNAppContextProvider.getApplicationContext().getBean("ServiciosRemedyBI");

            mapResult = serviciosRemedyBI.consultaFoliosPaginado(params);

            arrIncidentes = (ArrayOfInfoIncidente) mapResult.get("arrayIncidente");

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            RPA = new ArrayList<InfoIncidente>();
            EP = new ArrayList<InfoIncidente>();
            EPC = new ArrayList<InfoIncidente>();
            Atendidos = new ArrayList<InfoIncidente>();
            PPA = new ArrayList<InfoIncidente>();

            //supervisores = new ArrayList<String>();
            supervisoresActivos = getSupervisoresRemedy();

            /*
                  * Separara incidentes en las bandejas correspondientes;
             */
            separaBandejas(arrIncidentes, RPA, EP, EPC, Atendidos, PPA);

            datosEmpMttoBI = (DatosEmpMttoBI) GTNAppContextProvider.getApplicationContext().getBean("datosEmpMttoBI");

            /*
                  * Poner nombres de los supervisores en base al ID
             */
            //JsonArray supervisores = matchSupervisores();
            List<Integer> ticketsErroneos = new ArrayList<>();

            JsonArray arrayRPA = trabajaBandeja(RPA, ticketsErroneos);
            JsonArray arrayEP = trabajaBandeja(EP, ticketsErroneos);
            JsonArray arrayEPC = trabajaBandeja(EPC, ticketsErroneos);
            JsonArray arrayAtendidos = trabajaBandeja(Atendidos, ticketsErroneos);
            JsonArray arrayPPA = trabajaBandeja(PPA, ticketsErroneos);

            if (arrIncidentes == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {
                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", arrayRPA == null ? new JsonArray() : arrayRPA);
                envoltorioJsonObj.add("EN_PROCESO", arrayEP == null ? new JsonArray() : arrayEP);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", arrayEPC == null ? new JsonArray() : arrayEPC);
                envoltorioJsonObj.add("ATENDIDOS", arrayAtendidos == null ? new JsonArray() : arrayAtendidos);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", arrayPPA == null ? new JsonArray() : arrayPPA);
                envoltorioJsonObj.add("SUPERVISORES", supervisoresActivos == null ? new JsonArray() : supervisoresActivos);

                envoltorioJsonObj.addProperty("TOTAL_TICKETS", (Integer) mapResult.get("numTotalIncidentes"));

                JsonElement result = new GsonBuilder().create().toJsonTree(ticketsErroneos);
                envoltorioJsonObj.add("TICKETS_ERRONEOS", result);

            }

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {

            logger.info("Ocurrio algo en getBandejaTickets");

            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }

    }

    public String getBandejaCuadrilla(String proveedor, int idCuadrilla, int zona) {

        String res = null;
        try {

            ArrayOfInfoIncidente arrIncidentes;
            ArrayOfInfoIncidente arrIncidentesCerrados;

            int prov = (proveedor != null && !proveedor.isEmpty() && !proveedor.equalsIgnoreCase("") ? Integer.parseInt(proveedor) : 0);

            if (prov > 0) {

                proveedoresBI = (ProveedoresBI) GTNAppContextProvider.getApplicationContext().getBean("proveedoresBI");;
                ticketCuadrillaBI = (TicketCuadrillaBI) GTNAppContextProvider.getApplicationContext().getBean("ticketCuadrillaBI");

                List<TicketCuadrillaDTO> lista = ticketCuadrillaBI.obtieneCuadrillaTK(idCuadrilla, prov, zona);
                List<ProveedoresDTO> lista2 = proveedoresBI.obtieneDatos(prov);

                if (lista2 != null) {

                    String razonSocial = "";
                    for (ProveedoresDTO aux2 : lista2) {
                        razonSocial = aux2.getRazonSocial();
                    }

                    serviciosRemedyBI = (ServiciosRemedyBI) GTNAppContextProvider.getApplicationContext().getBean("ServiciosRemedyBI");
                    datosEmpMttoBI = (DatosEmpMttoBI) GTNAppContextProvider.getApplicationContext().getBean("datosEmpMttoBI");

                    arrIncidentes = serviciosRemedyBI.consultaFoliosProveedor(razonSocial.trim(), "3");
                    arrIncidentesCerrados = serviciosRemedyBI.consultaFoliosProveedor(razonSocial.trim(), "4");

                    JsonObject envoltorioJsonObj = new JsonObject();

                    // ----------------------Agrupar folios por tipo de
                    // estado-----------------------
                    ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
                    ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
                    ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
                    ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
                    ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();

                    supervisores = new ArrayList<String>();

                    /*
                     * Poner nombres de los supervisores en base al ID
                     */
                    JsonArray supervisores = matchSupervisores();

                    if (arrIncidentes == null) {
                        envoltorioJsonObj.add("FOLIOS", null);
                    } else {

                        /*
                         * Separa tickets de la cuadrilla del proveedor
                         */
                        InfoIncidente[] incidentesAbiertos = separaTicketCuadrilla(arrIncidentes, lista);
                        InfoIncidente[] incidentesCerrados = separaTicketCuadrilla(arrIncidentesCerrados, lista);

                        arrIncidentes.setInfoIncidente(incidentesAbiertos);
                        arrIncidentesCerrados.setInfoIncidente(incidentesCerrados);

                        /*
                         * Separara incidentes en las bandejas correspondientes;
                         */
                        separaBandejas(arrIncidentes, RPA, EP, EPC, Atendidos, PPA);
                        separaBandejas(arrIncidentesCerrados, RPA, EP, EPC, Atendidos, PPA);

                        List<Integer> ticketsErroneos = new ArrayList<>();

                        JsonArray arrayRPA = trabajaBandeja(RPA, ticketsErroneos);
                        JsonArray arrayEP = trabajaBandeja(EP, ticketsErroneos);
                        JsonArray arrayEPC = trabajaBandeja(EPC, ticketsErroneos);
                        JsonArray arrayAtendidos = trabajaBandeja(Atendidos, ticketsErroneos);
                        JsonArray arrayPPA = trabajaBandeja(PPA, ticketsErroneos);

                        envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", arrayRPA == null ? new JsonArray() : arrayRPA);
                        envoltorioJsonObj.add("EN_PROCESO", arrayEP == null ? new JsonArray() : arrayEP);
                        envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", arrayEPC == null ? new JsonArray() : arrayEPC);
                        envoltorioJsonObj.add("ATENDIDOS", arrayAtendidos == null ? new JsonArray() : arrayAtendidos);
                        envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", arrayPPA == null ? new JsonArray() : arrayPPA);

                        //envoltorioJsonObj.add("SUPERVISORES", supervisores == null ? new JsonArray() : supervisores);
                        //envoltorioJsonObj.add("SUPERVISORES", null);
                    }

                    //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                    res = envoltorioJsonObj.toString();
                } else {
                    return "{}";
                }

            } else {
                return "{}";
            }

        } catch (Exception e) {
            logger.info("Ocurrio algo en getBandejaCuadrilla");
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }

    }

    public String getBandejaCuadrillaPaginado(String idCuadrilla, String ticket, String sucursal, int pagina) {

        String[] params = {sucursal, ticket, "", "", "", "" + pagina, "" + idCuadrilla};

        String res = null;
        HashMap< String, Object> mapResult = null;
        try {

            ArrayOfInfoIncidente arrIncidentes;
            ArrayOfInfoIncidente arrIncidentesCerrados;

            serviciosRemedyBI = (ServiciosRemedyBI) GTNAppContextProvider.getApplicationContext().getBean("ServiciosRemedyBI");
            datosEmpMttoBI = (DatosEmpMttoBI) GTNAppContextProvider.getApplicationContext().getBean("datosEmpMttoBI");

            mapResult = serviciosRemedyBI.consultaFoliosPaginado(params);
            arrIncidentes = (ArrayOfInfoIncidente) mapResult.get("arrayIncidente");

            JsonObject envoltorioJsonObj = new JsonObject();

            // ----------------------Agrupar folios por tipo de
            // estado-----------------------
            ArrayList<InfoIncidente> RPA = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EP = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> EPC = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> Atendidos = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> PPA = new ArrayList<InfoIncidente>();

            supervisores = new ArrayList<String>();

            /*
             * Poner nombres de los supervisores en base al ID
             */
            JsonArray supervisores = matchSupervisores();

            if (arrIncidentes == null) {
                envoltorioJsonObj.add("FOLIOS", null);
            } else {

                /*
                 * Separa tickets de la cuadrilla del proveedor
                 */
 /*InfoIncidente[] incidentesAbiertos = separaTicketCuadrilla(arrIncidentes, lista);
                InfoIncidente[] incidentesCerrados = separaTicketCuadrilla(arrIncidentesCerrados, lista);

                arrIncidentes.setInfoIncidente(incidentesAbiertos);
                arrIncidentesCerrados.setInfoIncidente(incidentesCerrados);*/

 /*
                 * Separara incidentes en las bandejas correspondientes;
                 */
                separaBandejas(arrIncidentes, RPA, EP, EPC, Atendidos, PPA);
                //separaBandejas(arrIncidentesCerrados);

                List<Integer> ticketsErroneos = new ArrayList<>();

                JsonArray arrayRPA = trabajaBandeja(RPA, ticketsErroneos);
                JsonArray arrayEP = trabajaBandeja(EP, ticketsErroneos);
                JsonArray arrayEPC = trabajaBandeja(EPC, ticketsErroneos);
                JsonArray arrayAtendidos = trabajaBandeja(Atendidos, ticketsErroneos);
                JsonArray arrayPPA = trabajaBandeja(PPA, ticketsErroneos);

                envoltorioJsonObj.add("RECIBIDO_POR_ATENDER", arrayRPA == null ? new JsonArray() : arrayRPA);
                envoltorioJsonObj.add("EN_PROCESO", arrayEP == null ? new JsonArray() : arrayEP);
                envoltorioJsonObj.add("EN_PROCESO_DE_CIERRE", arrayEPC == null ? new JsonArray() : arrayEPC);
                envoltorioJsonObj.add("ATENDIDOS", arrayAtendidos == null ? new JsonArray() : arrayAtendidos);
                envoltorioJsonObj.add("PENDIENTES_POR_AUTORIZAR", arrayPPA == null ? new JsonArray() : arrayPPA);

                envoltorioJsonObj.addProperty("TOTAL_TICKETS", (Integer) mapResult.get("numTotalIncidentes"));

                JsonElement result = new GsonBuilder().create().toJsonTree(ticketsErroneos);
                envoltorioJsonObj.add("TICKETS_ERRONEOS", result);

                //envoltorioJsonObj.add("SUPERVISORES", supervisores == null ? new JsonArray() : supervisores);
                //envoltorioJsonObj.add("SUPERVISORES", null);
            }

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info("Ocurrio algo en getBandejaCuadrilla");
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }

    }

    public InfoIncidente[] separaTicketCuadrilla(ArrayOfInfoIncidente incidentes, List<TicketCuadrillaDTO> ticketsCuadrilla) {

        InfoIncidente[] incidentesArray = incidentes.getInfoIncidente();

        ArrayList<InfoIncidente> incidentesCuadrilla = new ArrayList<>();

        if (ticketsCuadrilla != null && ticketsCuadrilla.size() > 0) {
            //Sepra los ticket del proveddor
            for (TicketCuadrillaDTO ticket : ticketsCuadrilla) {

                if (incidentesArray != null) {
                    for (int i = 0; i < incidentesArray.length; i++) {

                        if (ticket.getTicket().equals(String.valueOf(incidentesArray[i].getIdIncidencia()))) {

                            incidentesCuadrilla.add(incidentesArray[i]);

                            break;
                        }

                    }
                }

            }

        }

        InfoIncidente[] regreso = null;

        if (incidentesCuadrilla != null && incidentesCuadrilla.size() > 0) {

            regreso = incidentesCuadrilla.toArray(new InfoIncidente[incidentesCuadrilla.size()]);

        }

        return regreso;
    }

    public JsonArray matchSupervisores() {

        // -------------------------------------------------------------------------------
        JsonArray supervisoresJsonArray = new JsonArray();
        if (!this.supervisores.isEmpty() && supervisores != null) {
            for (String sup : this.supervisores) {
                JsonObject incJsonObj = new JsonObject();
                if (!sup.matches("[^0-9]*")) {

                    List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(sup));
                    if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                        for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                            incJsonObj.addProperty("SUPERVISOR", emp_aux.getNombre());
                            supervisoresJsonArray.add(incJsonObj);
                        }
                    }
                } else {
                    System.out.println("No es un número");
                    incJsonObj.addProperty("SUPERVISOR", "JUAN PEREZ");
                    supervisoresJsonArray.add(incJsonObj);
                }
            }
        }

        return supervisoresJsonArray;

    }

    public void separaBandejas(ArrayOfInfoIncidente bandejaPrincipal, ArrayList<InfoIncidente> RPA, ArrayList<InfoIncidente> EP, ArrayList<InfoIncidente> EPC, ArrayList<InfoIncidente> Atendidos, ArrayList<InfoIncidente> PPA) {

        if (bandejaPrincipal != null) {
            InfoIncidente[] arregloInc = bandejaPrincipal.getInfoIncidente();
            if (arregloInc != null) {
                for (InfoIncidente aux : arregloInc) {
                    //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia()+ ", ");
                    if (aux.getEstado().contains("Recibido por atender")) {
                        RPA.add(aux);
                    }
                    if (aux.getEstado().contains("En Proceso")) {
                        EP.add(aux);
                    }
                    if (aux.getEstado().contains("En Recepción")) {
                        EPC.add(aux);
                    }
                    if (aux.getEstado().contains("Atendido")) {
                        Atendidos.add(aux);
                    }
                    if (aux.getEstado().contains("Pendiente")) {
                        PPA.add(aux);
                    }
                    /*if (supervisores.isEmpty()) {
                    	if(aux.getIdCorpSupervisor()!=null)
                    		supervisores.add(aux.getIdCorpSupervisor().trim());
                    } else {
                        boolean flagSup = false;
                        for (String sup : supervisores) {
                            if (aux.getIdCorpSupervisor() != null) {
                                if (sup.equals(aux.getIdCorpSupervisor().trim())) {
                                    flagSup = true;
                                    break;
                                }
                            }
                        }
                        if (flagSup == false) {
                            if (aux.getIdCorpSupervisor() != null) {
                                supervisores.add(aux.getIdCorpSupervisor().trim());
                            }
                        }
                    }*/

                }
            }
        }

    }

    public JsonArray trabajaBandeja(ArrayList<InfoIncidente> bandeja, List<Integer> ticketsError) {

        JsonArray respuesta = null;

        if (!bandeja.isEmpty() && bandeja != null) {

            ArrayList<InfoIncidente> Critica = new ArrayList<InfoIncidente>();
            ArrayList<InfoIncidente> normal = new ArrayList<InfoIncidente>();

            for (InfoIncidente aux : bandeja) {
                if (aux.getPrioridad().getValue().contains("Normal")) {
                    normal.add(aux);
                }
                if (aux.getPrioridad().getValue().contains("Critica")) {
                    Critica.add(aux);
                }
            }

            respuesta = arrOrdenadoSupervisor(Critica, normal, ticketsError);

        }

        return respuesta;

    }

    public JsonArray arrOrdenadoSupervisor(ArrayList<InfoIncidente> critica, ArrayList<InfoIncidente> normal, List<Integer> ticketsError) {

        JsonArray arrJsonArray = new JsonArray();

        armaJson(critica, arrJsonArray, ticketsError);
        armaJson(normal, arrJsonArray, ticketsError);

        return arrJsonArray;
    }

    public void armaJson(ArrayList<InfoIncidente> listaTickets, JsonArray arrayRespuesta, List<Integer> ticketsError) {

        proveedoresBI = (ProveedoresBI) GTNAppContextProvider.getApplicationContext().getBean("proveedoresBI");;

        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();
        HashMap<String, DatosEmpMttoDTO> EmpleadosHashMap = new HashMap<String, DatosEmpMttoDTO>();
        HashMap<String, String> EmpleadosRemedyHashMap = serviciosRemedyBI.consultaUsuariosRemedy();

        while (!listaTickets.isEmpty()) {
            int pos = 0;
            int ticket = 0;
            try {

                for (int i = 0; i < listaTickets.size(); i++) {
                    if (listaTickets.get(pos).getFechaCreacion().compareTo(listaTickets.get(i).getFechaCreacion()) >= 0) {
                        pos = i;
                    }
                }
                InfoIncidente aux = listaTickets.get(pos);

                //logger.info("prueba: " + aux.getEstado() + ", " + aux.getFalla() + ", " + aux.getIdIncidencia() + ", ");
                JsonObject incJsonObj = new JsonObject();

                aux.getAutorizaCambioMonto();
                ticket = aux.getIdIncidencia();
                incJsonObj.addProperty("ID_INCIDENTE", aux.getIdIncidencia());
                incJsonObj.addProperty("ESTADO", aux.getEstado());
                incJsonObj.addProperty("FALLA", aux.getFalla());
                incJsonObj.addProperty("INCIDENCIA", aux.getIncidencia());
                incJsonObj.addProperty("MOTIVO_ESTADO", aux.getMotivoEstado());

                incJsonObj.addProperty("NOMBRE_CLIENTE", aux.getNombreCliente());
                incJsonObj.addProperty("NUMEMP_CLIENTE", aux.getIdCorpCliente());
                incJsonObj.addProperty("NUMEMP_SUPERVISOR", aux.getIdCorpSupervisor());
                incJsonObj.addProperty("NUMEMP_COORDINADOR", aux.getIDCorpCoordinador());

                String idCliente = aux.getIdCorpCliente();

                if (idCliente != null) {
                    if (!EmpleadosHashMap.containsKey(idCliente.trim())) {
                        List<DatosEmpMttoDTO> datos_cliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                        if (!datos_cliente.isEmpty() && datos_cliente != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_cliente) {
                                incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                                incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                                EmpleadosHashMap.put(idCliente.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                            incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCliente.trim());
                        incJsonObj.addProperty("PUESTO_CLIENTE", emp_aux.getDescripcion());
                        incJsonObj.addProperty("TEL_CLIENTE", emp_aux.getTelefono());
                    }
                } else {
                    incJsonObj.addProperty("PUESTO_CLIENTE", "* Sin información");
                    incJsonObj.addProperty("TEL_CLIENTE", "* Sin información");
                }
                String idCord = aux.getIDCorpCoordinador();
                if (idCord != null) {
                    if (!EmpleadosRemedyHashMap.containsKey(idCord.trim())) {
                        incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_COORDINADOR", EmpleadosRemedyHashMap.get(idCord.trim()));
                    }
                } else {
                    incJsonObj.addProperty("TEL_COORDINADOR", "* Sin información");
                }
                if (idCord != null) {
                    if (!EmpleadosHashMap.containsKey(idCord.trim())) {
                        List<DatosEmpMttoDTO> datos_coordinador = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIDCorpCoordinador()));
                        if (!datos_coordinador.isEmpty() && datos_coordinador != null) {
                            for (DatosEmpMttoDTO emp_aux : datos_coordinador) {
                                incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                EmpleadosHashMap.put(idCord.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.addProperty("COORDINADOR", "* Sin información");
                        }
                    } else {
                        DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idCord.trim());
                        incJsonObj.addProperty("COORDINADOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                    }

                } else {
                    incJsonObj.addProperty("COORDINADOR", "* Sin información");
                }
                //String idSup = aux.getIdCorpSupervisor().trim();
                String idSup = aux.getIdCorpSupervisor();

                if (idSup != null) {
                    if (!EmpleadosRemedyHashMap.containsKey(idSup)) {
                        incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                    } else {
                        incJsonObj.addProperty("TEL_SUPERVISOR", EmpleadosRemedyHashMap.get(idSup));
                    }
                } else {
                    incJsonObj.addProperty("TEL_SUPERVISOR", "* Sin información");
                }
                if (idSup != null) {
                    if (!idSup.matches("[^0-9]*")) {

                        if (!EmpleadosHashMap.containsKey(idSup.trim())) {
                            List<DatosEmpMttoDTO> datos_supervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                            if (!datos_supervidor.isEmpty() && datos_supervidor != null) {
                                for (DatosEmpMttoDTO emp_aux : datos_supervidor) {
                                    incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                    incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                                    EmpleadosHashMap.put(idSup.trim(), emp_aux);
                                }
                            } else {
                                incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                                incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                            }
                        } else {
                            DatosEmpMttoDTO emp_aux = EmpleadosHashMap.get(idSup.trim());
                            incJsonObj.addProperty("SUPERVISOR", emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                            incJsonObj.addProperty("NOM_SUPERVISOR", emp_aux.getNombre());
                        }

                    } else {
                        incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                        incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                    }
                } else {
                    incJsonObj.addProperty("SUPERVISOR", "* Sin información");
                    incJsonObj.addProperty("NOM_SUPERVISOR", "* Sin información");
                }

                incJsonObj.addProperty("NOTAS", aux.getNotas());
                incJsonObj.addProperty("NO_TICKET_PROVEEDOR", aux.getNoTicketProveedor());
                incJsonObj.addProperty("PROVEEDOR", aux.getProveedor());

                if (aux.getProveedor() != null) {
                    if (!lista.isEmpty()) {
                        for (ProveedoresDTO aux3 : lista) {
                            if (aux3.getStatus().contains("Activo")) {
                                if (aux.getProveedor().toLowerCase().trim()
                                        .contains(aux3.getRazonSocial().toLowerCase().trim())) {
                                    incJsonObj.addProperty("NOMBRE_CORTO", aux3.getNombreCorto());
                                    incJsonObj.addProperty("MENU", aux3.getMenu());
                                    incJsonObj.addProperty("RAZON_SOCIAL", aux3.getRazonSocial());
                                    incJsonObj.addProperty("ID_PROVEEDOR", aux3.getIdProveedor());
                                    break;
                                }

                            }
                        }
                    }
                } else {
                    String nomCorto = null;
                    incJsonObj.addProperty("NOMBRE_CORTO", nomCorto);
                    incJsonObj.addProperty("MENU", nomCorto);
                    incJsonObj.addProperty("RAZON_SOCIAL", nomCorto);
                    incJsonObj.addProperty("ID_PROVEEDOR", nomCorto);
                }

                incJsonObj.addProperty("PUESTO_ALTERNO", aux.getPuestoAlterno());
                incJsonObj.addProperty("SUCURSAL", aux.getSucursal());
                incJsonObj.addProperty("TIPO_FALLA", aux.getTipoFalla());
                incJsonObj.addProperty("USR_ALTERNO", aux.getUsrAlterno());
                incJsonObj.addProperty("TEL_USR_ALTERNO", aux.getTelCliente());

                incJsonObj.addProperty("MONTO_ESTIMADO", aux.getMontoEstimado());
                incJsonObj.addProperty("NO_SUCURSAL", aux.getNoSucursal());
                Date date = aux.getFechaCierre() != null ? aux.getFechaCierre().getTime() : fechaNula();
                //Date date = aux.getFechaCierre().getTime();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO_2", "" + date1);
                date = aux.getFechaCreacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_CREACION", "" + date1);
                date = aux.getFechaModificacion().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_MODIFICACION", "" + date1);
                date = aux.getFechaProgramada().getTime();
                date1 = format1.format(date);
                incJsonObj.addProperty("FECHA_PROGRAMADA", "" + date1);
                incJsonObj.addProperty("PRIORIDAD", aux.getPrioridad().getValue());
                incJsonObj.addProperty("TIPO_CIERRE", aux.getTipoCierreProveedor());

                String autCleinte = aux.getAutorizacionCliente();
                if (autCleinte != null && !autCleinte.equals("")) {
                    if (autCleinte.contains("Rechazado")) {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 2);
                    } else {
                        incJsonObj.addProperty("AUTORIZADO_CLIENTE", 1);
                    }
                } else {
                    incJsonObj.addProperty("AUTORIZADO_CLIENTE", 0);
                }

                if (aux.getTipoAtencion() != null) {
                    incJsonObj.addProperty("TIPO_ATENCION", aux.getTipoAtencion().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("TIPO_ATENCION", x);
                }
                if (aux.getOrigen() != null) {
                    incJsonObj.addProperty("ORIGEN", aux.getOrigen().getValue().toString());
                } else {
                    String x = null;
                    incJsonObj.addProperty("ORIGEN", x);
                }

                //-----------------Obtiene coordenadas de inicio y fin de ticket -------------------
                incJsonObj.addProperty("LATITUD_FIN", "");
                incJsonObj.addProperty("LONGITUD_FIN", "");

                incJsonObj.addProperty("FECHA_INICIO_ATENCION", "" + date1);

                incJsonObj.addProperty("ID_CUADRILLA", 0);
                incJsonObj.addProperty("LIDER", "");
                incJsonObj.addProperty("CORREO", "");
                incJsonObj.addProperty("SEGUNDO", "");
                incJsonObj.addProperty("ZONA", 0);

                incJsonObj.addProperty("BANDERA_CMONTO", 0);
                incJsonObj.addProperty("BANDERA_APLAZAMIENTO", 0);
                incJsonObj.addProperty("BANDERAS", 0);

                incJsonObj.addProperty("FECHA_ATENCION", "0001-01-01 00:00:00");
                incJsonObj.addProperty("FECHA_RECHAZO_CLIENTE", "0001-01-01 00:00:00");
                incJsonObj.addProperty("FECHA_CIERRE_TECNICO", "0001-01-01 00:00:00");
                incJsonObj.addProperty("FECHA_CIERRE", "0001-01-01 00:00:00");
                incJsonObj.addProperty("FECHA_ATENCION_GARANTIA", "0001-01-01 00:00:00");
                incJsonObj.addProperty("FECHA_CIERRE_GARANTIA", "0001-01-01 00:00:00");
                incJsonObj.addProperty("FECHA_RECHAZO_PROVEEDOR", "0001-01-01 00:00:00");
                incJsonObj.addProperty("BANDERA_RECHAZO_CIERRE_PREVIO", false);
                incJsonObj.addProperty("CALIFICACION", 0);
                incJsonObj.addProperty("MONTO_REAL", aux.getMontoReal());
                incJsonObj.addProperty("FECHA_CIERRE_ADMIN", "" + date1);
                incJsonObj.addProperty("DIAS_APLAZAMIENTO", aux.getDiasAplazamiento());

                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", "");
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", "");

                incJsonObj.addProperty("FECHA_AUTORIZA_PROVEEDOR", "0001-01-01 00:00:00");
                incJsonObj.addProperty("DECLINAR_PROVEEDOR", "");

                incJsonObj.addProperty("MENSAJE_PARA_PROVEEDOR", "");
                incJsonObj.addProperty("AUTORIZACION_PROVEEDOR", "");

                incJsonObj.addProperty("LATITUD_INICIO", "");
                incJsonObj.addProperty("LONGITUD_INICIO", "");

                incJsonObj.addProperty("FECHA_CANCELACION", "0001-01-01 00:00:00");

                //-------------------------------------------------------------------------------------
                incJsonObj.addProperty("CLIENTE_AVISADO", aux.getClienteAvisado());
                incJsonObj.addProperty("RESOLUCION", aux.getResolucion());
                incJsonObj.addProperty("MONTO_ESTIMADO_AUTORIZADO", aux.getMontoEstimadoAutorizado());

                arrayRespuesta.add(incJsonObj);

                listaTickets.remove(pos);

            } catch (Exception e) {
                logger.info("TICKET_ERRONEO : Revisar ticket : " + ticket);

                ticketsError.add(ticket);

                listaTickets.remove(pos);
            }

        }

    }

    public Date fechaNula() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date nullDate = null;
        try {
            nullDate = sdf.parse("00010101");
        } catch (Exception e) {

        }

        return nullDate;

    }

    public JsonArray getSupervisoresRemedy() {

        JsonArray supervisoresActivos = new JsonArray();
        JsonObject usuario;

        try {

            Calendar today = Calendar.getInstance();
            //today.add(Calendar.DATE, -1);
            today.set(2016, 1, 1);
            System.out.println("Today is " + today.getTime());

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("demoWsM");
            autenticatiosRemedy.setUserName("demoWsM");

            ListaPersonal listaPersonal = new ListaPersonal();
            listaPersonal.setFecha(today);
            listaPersonal.setAuthentication(autenticatiosRemedy);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ListaPersonal request = new RemedyStub.ListaPersonal();
            RemedyStub.ListaPersonalResponse response = new RemedyStub.ListaPersonalResponse();

            response = consulta.listaPersonal(listaPersonal);

            RemedyStub.RsCPersonal listaPersonalResult = response.getListaPersonalResult();

            if (listaPersonalResult.getExito()) {

                RemedyStub.ArrayOfPersonal arrayOfPersonal = listaPersonalResult.getOPersonal();
                if (arrayOfPersonal.getPersonal() != null) {

                    RemedyStub.Personal[] personalArray = arrayOfPersonal.getPersonal();
                    if (personalArray != null) {

                        for (RemedyStub.Personal personal : personalArray) {

                            try {

                                if (personal.getPerfil().equalsIgnoreCase("activado") && personal.getPuesto().contains("Supervisor Mantenimiento")) {

                                    usuario = new JsonObject();

                                    usuario.addProperty("ID", personal.getIdCorporativo());
                                    usuario.addProperty("NOMBRE", personal.getNombre());

                                    supervisoresActivos.add(usuario);

                                }

                            } catch (Exception e) {

                            }

                        }

                    }
                }

            }

        } catch (Exception e) {

            supervisoresActivos = new JsonArray();

        }

        return supervisoresActivos;

    }

}
