package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.domain.UsuarioDTO;
import com.gruposalinas.migestion.servicios.servidor.CorreosLotus;
import com.gruposalinas.migestion.util.UtilDate;
import com.gruposalinas.migestion.util.UtilObject;
import com.gruposalinas.migestion.util.UtilResource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;

public class CorreoBI {

    private static final Logger logger = LogManager.getLogger(CorreoBI.class);

    @SuppressWarnings("unused")
    private String mailPersonal = "jsepulveda@elektra.com.mx";
    @SuppressWarnings("unused")
    private String mailContactoAdmin = "sistemas@franquicia.com.mx";
    private List<String> copiados = null;

    public boolean sendMailBuzon(UsuarioDTO user, String asunto, String idCeco, String puesto, String lugar, String telefono, String comentario,
            String copiadosParam) {

        copiados = new ArrayList<String>();
        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");
        String titulo = "";
        titulo = user.getNombre() + " - " + asunto;
        String arr[] = null;

        if (copiadosParam != "") {
            arr = copiadosParam.split(",");
            if (arr.length > 0) {
                for (int i = 0; i < arr.length; i++) {
                    copiados.add(arr[i]);
                }
            }
        }
        logger.info("copiados... " + copiados);

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailBuzon.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Buzón dudas o comentarios");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replaceAll("@Fecha", fechaPendientes);
            strCorreoOwner = strCorreoOwner.replaceAll("@Nombre", user.getNombre());
            strCorreoOwner = strCorreoOwner.replaceAll("@IdCeco", idCeco);
            strCorreoOwner = strCorreoOwner.replaceAll("@Puesto", puesto);
            strCorreoOwner = strCorreoOwner.replaceAll("@NoEmpleado", user.getIdUsuario());
            strCorreoOwner = strCorreoOwner.replaceAll("@Zona", lugar);
            strCorreoOwner = strCorreoOwner.replaceAll("@Telefono", telefono);
            strCorreoOwner = strCorreoOwner.replace("@Comentario", comentario);

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";

            try {
                destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                logger.info("correo destinatario - " + destinatario);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "jsepulveda@elektra.com.mx";
            }

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO
            // VALIDA_USUARIO_LOTUS_CORREO
            logger.info("strCorreoOwner " + strCorreoOwner);

            //UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, null);
        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;
    }
}
