package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.FilasDAO;
import com.gruposalinas.migestion.domain.FilasDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class FilasBI {

    private static Logger logger = LogManager.getLogger(FilasBI.class);

    private List<FilasDTO> listafila;

    @Autowired
    FilasDAO FilasDAO;

    public int inserta(FilasDTO bean) {
        int respuesta = 0;

        try {
            respuesta = FilasDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public boolean elimina(String idFila) {
        boolean respuesta = false;

        try {
            respuesta = FilasDAO.elimina(idFila);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<FilasDTO> obtieneDatos(int idFila) {

        try {
            listafila = FilasDAO.obtieneDatos(idFila);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<FilasDTO> obtieneInfo() {

        try {
            listafila = FilasDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Filas");

        }

        return listafila;
    }

    public boolean actualiza(FilasDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = FilasDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
