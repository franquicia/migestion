package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.FotoPlantillaDAO;
import com.gruposalinas.migestion.domain.FotoPlantillaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class FotoPlantillaBI {

    private static Logger logger = LogManager.getLogger(FotoPlantillaBI.class);

    private List<FotoPlantillaDTO> listafila;

    @Autowired
    FotoPlantillaDAO fotoPlantillaDAO;

    public boolean elimina(String ceco) {
        boolean respuesta = false;

        try {
            respuesta = fotoPlantillaDAO.elimina(ceco);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }


    public List<FotoPlantillaDTO> obtieneInfo() {

        try {
            listafila = fotoPlantillaDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la Foto Plantilla");

        }

        return listafila;
    }

    public boolean actualiza(FotoPlantillaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = fotoPlantillaDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
