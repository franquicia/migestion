package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.PeriodoReporteDAOImpl;
import com.gruposalinas.migestion.domain.PeriodoReporteDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PeriodoReporteBI {

    private static Logger logger = LogManager.getLogger(PeriodoReporteBI.class);

    private List<PeriodoReporteDTO> listaReportePeriodo;

    @Autowired
    PeriodoReporteDAOImpl repPerDAO;

    public List<PeriodoReporteDTO> obtienePeriodoReporte(String idsPerRep, String idPeriodo, String idReporte) {

        try {
            listaReportePeriodo = repPerDAO.obtienePeriodoReporte(idsPerRep, idPeriodo, idReporte);
        } catch (Exception e) {
            logger.info("No fue posible obtener el registro de la Tarea");

        }

        return listaReportePeriodo;
    }

    public boolean insertaPeriodoReporte(int idPeriodo, int idReporte, String horario, int idPerRep, int commit) {
        boolean respuesta = false;

        try {
            respuesta = repPerDAO.insertaPeriodoReporte(idPeriodo, idReporte, horario, idPerRep, commit);
        } catch (Exception e) {
            logger.info("No fue posible insertar el incidente");

        }

        return respuesta;
    }

    public boolean actualizaPeriodoReporte(int idPeriodo, int idReporte, String horario, int idPerRep, int commit) {
        boolean respuesta = false;

        try {
            respuesta = repPerDAO.actualizaPeriodoReporte(idPeriodo, idReporte, horario, idPerRep, commit);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el registro el incidente");

        }

        return respuesta;
    }

    public boolean eliminaPeriodoReporte(String idPerRep) {
        boolean respuesta = false;

        try {
            respuesta = repPerDAO.eliminaPeriodoReporte(idPerRep);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el incidente");

        }

        return respuesta;
    }
}
