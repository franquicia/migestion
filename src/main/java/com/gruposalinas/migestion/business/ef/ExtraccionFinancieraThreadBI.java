package com.gruposalinas.migestion.business.ef;

//import org.jboss.xnio.log.Logger;
import com.google.gson.JsonObject;

public class ExtraccionFinancieraThreadBI extends Thread {

    //ExtraccionFinancieraBI bussines = new ExtraccionFinancieraBI();
    IndicadoresBI bussines = new IndicadoresBI();

    private int idService;
    private String fecha;
    private String geografia;
    private String nivel;
    private boolean termino = false;
    private JsonObject respuesta;

    public boolean isTermino() {
        return termino;
    }

    public void setTermino(boolean termino) {
        this.termino = termino;
    }

    public JsonObject getRespuesta() {
        return respuesta;
    }

    public ExtraccionFinancieraThreadBI(int idService, String fecha, String geografia, String nivel) {

        this.idService = idService;
        this.fecha = fecha;
        this.geografia = geografia;
        this.nivel = nivel;

    }

    @Override
    public void run() {

        switch (idService) {
            
            case 13:
                respuesta = bussines.getActivacionesBazDigital(fecha, geografia);
                break;
            case 14:
                respuesta = bussines.getColocacionBazDigital(fecha, geografia, nivel);
                break;

            default:
                break;
        }

        termino = true;

        //System.out.println("Servicio " + idService + "termino: "+termino);
    }

}
