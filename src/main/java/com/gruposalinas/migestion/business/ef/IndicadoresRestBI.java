package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class IndicadoresRestBI {

    private Logger logger = LogManager.getLogger(IndicadoresRestBI.class);

//	public static void main (String [] args){
//
////		String res = getResponse("","","");
////		System.out.println(res);
////		JsonObject json = clearXml(res);
////
////		json.toString();
//
//		JsonObject ejemplo = new JsonObject();
//		ejemplo.add("ejemplo", null);
//
//		System.out.println(ejemplo.toString());
//
//	}
    public String getResponse(String fecha, String geografia, String nivel, String service) {

        BufferedReader br = null;
        String response = "";
        try {
            String uri = GTNConstantes.getURLExtraccionFinancieraRest() + "/" + service + "/" + fecha + "/" + geografia + "/" + nivel + "/";

//			logger.info("URL : "+uri);
            URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP status code : "
                        + conn.getResponseCode());
            }

            br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;

            String cadena = "";
            while ((output = br.readLine()) != null) {
                cadena += output;
            }

            StringReader strReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(strReader);
            Element rootNode = document.getRootElement();

            response = rootNode.getValue();

            conn.disconnect();

        } catch (Exception e) {
            logger.info("Ap al ejecutar el servicio " + e);

            return null;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }

            } catch (Exception e) {

            }
        }

        return response;
    }

    public JsonObject getPersonales(String fecha, String geografia, String nivel) {

        logger.info("---------Personales------------");

        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            String xmlStr = getResponse(fecha, geografia, nivel, "getColocacionPersonales");
            logger.info(xmlStr);

            json = clearXml(xmlStr);

        } catch (Exception e) {
            logger.info("Ap en getPersonales() " + e);

            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("personalesColocacion", json);
        }

        return respuestaService;

    }

    public Map<String, Object> armaJsonNoAbonadas(String xmlStr) {

        Map<String, Object> mapJson = new HashMap<String, Object>();
        JsonObject nuncaAbonadas = new JsonObject();
        JsonObject pendientesSurtir = new JsonObject();

        try {
            /* Convierte String a XML */

            StringReader str = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(str);
            Element rootNode = document.getRootElement();

            Element data = rootNode.getChild("data");
            Element renglones = data.getChild("renglones");
            Element renglon = renglones.getChild("renglon");
            Element celdas = renglon.getChild("celdas");
            List<Element> listCeldas = celdas.getChildren("celda");

            for (Element element : listCeldas) {

                int celda = Integer.parseInt(element.getAttributeValue("pos"));
                String valor = element.getChild("caption").getValue();
                switch (celda) {
                    case 1:
                        break;
                    case 2:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 3:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 4:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 5:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 6:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 7:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                }

            }

            mapJson.put("pendientesSurtrir", pendientesSurtir);
            mapJson.put("nuncaAbonadas", nuncaAbonadas);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        logger.info("JSON SURTIDAS :" + pendientesSurtir.toString());
        logger.info("JSON NUNCA ABONADAS :" + nuncaAbonadas.toString());

        return mapJson;
    }

    public JsonObject armaJsonContribucion(String xmlStr) {

        JsonObject contribucion = new JsonObject();
        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            Element consulta = rootNode.getChild("Consulta");
            Element table = consulta.getChild("Table");

            DecimalFormat df = new DecimalFormat("###,###,###");

            contribucion.addProperty("posicion", table.getChild("POSICION").getValue());
            contribucion.addProperty("geografia", table.getChild("GEOGRAFIA").getValue());
            contribucion.addProperty("semana1", df.format(Double.parseDouble(table.getChild("SEM_1").getValue())));
            contribucion.addProperty("semana2", df.format(Double.parseDouble(table.getChild("SEM_2").getValue())));
            contribucion.addProperty("crecimiento", table.getChild("CRECIMIENTO").getValue());
            contribucion.addProperty("total", table.getChild("TOTAL").getValue());
            contribucion.addProperty("totalReg", table.getChild("T_REG").getValue());

            int posicion = Integer.parseInt(contribucion.get("posicion").getAsString());
            int total_reg = Integer.parseInt(contribucion.get("totalReg").getAsString());
            String imagen = "";

            int res = (int) ((posicion * 100) / total_reg);

            logger.info("Posicion " + posicion);
            logger.info("total_reg " + total_reg);
            logger.info("res " + (int) res);

            if (res >= 0 && res <= 20) {
                imagen = "oro";
            } else if (res > 20 && res <= 50) {
                imagen = "plata";
            } else if (res > 50 && res <= 80) {
                imagen = "bronce";
            } else if (res > 80 && res <= 100) {
                imagen = "carbon";
            }

            contribucion.addProperty("imagen", imagen);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        return contribucion;
    }

    protected JsonArray limpiXmlNew(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            //logger.info("Columnas :"+columnas);
            //logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
                    //					    logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                    //						logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue());
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
                    //						logger.info("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                //logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //logger.info(arrayFilas.toString());
            //logger.info("Valores celda lista : " +listValoresCeldas.size());
            //logger.info("Valores totales lista : " +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            jsonArray = armaJsonNew(valores, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonNew(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 4) {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                    //logger.info("cantidad: "+ valoresCeldas.get(i));
                    //logger.info("Cantidad vs"+ valoresCeldas.get( i - pos_vs ));
                    pos_vs += 3;
                } else {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

            }

            valor = new JsonObject();
            valor.addProperty("cantidad", valorStr);
            valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
            valor.addProperty("color", color);

            arrayValores.add(valor);

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /*Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 4) {
                    porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                    pos_vsTot += 3;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

            }

            jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
            jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
            jsonTotales.addProperty("color", color);
            arrayValores.add(jsonTotales);

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonObject clearXml(String xlmStr) {

        JsonObject respuesta = new JsonObject();

        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xlmStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            List<Element> allChildren = rootNode.getChildren();


            /*ï¿½NORMAL*/
            String fmtValue = "";
            boolean flagAdd = false;
            String celda = "";

            ArrayList<String> listValores = new ArrayList<String>();

            for (Element element : allChildren) {
                if (element.getName().equals("CellData")) {

                    List<Element> listaDatos = element.getChildren();// Lista
                    // que
                    // guarda
                    // todas
                    // las
                    // etiquets
                    // "Cell"
                    for (Element elementChild : listaDatos) {
                        fmtValue = "";
                        flagAdd = false;
                        celda = elementChild.getAttributeValue("CellOrdinal");
                        List<Element> elementDatos = elementChild.getChildren();// Lista
                        // que
                        // guarda
                        // la
                        // etiquetas
                        // dentro
                        // de
                        // cada
                        // etiqueta
                        // "Cell"
                        for (Element elementDato : elementDatos) {
                            if (elementDato.getName().equals("FmtValue")) {
                                fmtValue = elementDato.getValue();
                            } else if (elementDato.getName().equals("Value")) {
                                List<Attribute> atributos = elementDato.getAttributes();
                                for (Attribute elementAtributo : atributos) { // Revisa
                                    // Atributos
                                    // de
                                    // la
                                    // celda
                                    if (!elementAtributo.getValue().equals("xsd:short")) {
                                        flagAdd = true;
                                    }
                                }
                            }

                        }

                        if (flagAdd) {
                            listValores.add(fmtValue);
                        }

                    }

                }
            }

            respuesta = createResult(listValores);

        } catch (Exception e) {
            logger.info("Ap al ejecutar clearXml() " + e);

            respuesta = null;
        }

        return respuesta;

    }

    @SuppressWarnings("unused")
    protected JsonObject createResult(ArrayList<String> valores) {

        DecimalFormat formateador = new DecimalFormat("###,###.#");
        double[] totales = new double[9];
        int nuInteraciones = 8;

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        int indice = valores.size() - 1;

        try {

            /* Obtiene totales */
            while (nuInteraciones >= 0) {
                double valor = Double.parseDouble(valores.get(indice)) / 1000;
                totales[nuInteraciones] = valor;
                // totales[nuInteraciones] = (int)Math.round(valor);
                indice--;
                nuInteraciones--;
            }

            // int positionTotales = 0;
            int veces = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i <= indice; i++) {

                veces++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "";

                if (veces > 5) {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = Double.parseDouble(valores.get(i - pos_vs)) / 1000;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt= (int)Math.round(valor);
                    porcentaje = (valor / totales[veces - 1]) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", "" + (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces == 9) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces = 0;
                    pos_vs = 2;
                }

            }

            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.length; a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 4) {
                    porcentajeTot = (double) totales[a] / (double) totales[a - pos_vs] * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales[a]));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);
            logger.info(jsonDias.toString());

        } catch (Exception e) {
            logger.info("Ap createResult()" + e);

            jsonDias = null;
        }

        return jsonDias;

    }

    /*Limpia respuesta xml de los rubros Captacion*/
    @SuppressWarnings("unchecked")
    protected JsonObject limpiaXmlCaptacion(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();


            /*-----------------------------------------------Eliminar por pruebas--------------------------------------------------------------------*/
            logger.info("Celldata" + rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                //logger.info("Celda " +element.getAttributeValue("CellOrdinal"));
                numCeldas.add(Integer.parseInt(element.getAttributeValue("CellOrdinal")));

                String valor = element.getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonCaptacion(valoresCeldas, flagsColumnas, numCeldas);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        return jsonRepuestas;

    }

    /*Arma Json de los rubros Captacion*/
    @SuppressWarnings("unused")
    protected JsonObject armaJsonCaptacion(List<String> valoresCeldas, int[] flagsColumnas, ArrayList<Integer> numCeldas) {

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        try {
            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###.#");
            int columnas = flagsColumnas.length;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - columnas);
            int indiceColumnas = 0;

            while (indice < valoresCeldas.size()) {

                if (flagsColumnas[indiceColumnas] == 1) {
                    totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                }

                indiceColumnas++;
                indice++;
            }

            /*Obtener Filas*/
            int cont = 0;
            int veces = 1;
            int corte = 1;

            ArrayList<Double> valores = new ArrayList<Double>();

            while (cont < numCeldas.size() && cont != (valoresCeldas.size() - columnas)) {

                JsonObject valorPorcentaje = null;

                if (veces != numCeldas.get(cont)) {
                    break;
                }

                if (flagsColumnas[corte - 1] == 1) {
                    valores.add(Double.parseDouble(valoresCeldas.get(cont)));
                    //logger.info("$"+formateador.format(Double.parseDouble(valoresCeldas.get(cont))/1000.0));
                    //logger.info("$"+Double.parseDouble(valoresCeldas.get(cont))/1000);
                }

                if (corte == flagsColumnas.length) {
                    //logger.info("Agrega Fila");
                    corte = 1;
                    veces += 2;
                } else {
                    corte++;
                    veces++;
                }

                cont++;

            }

            // int positionTotales = 0;
            int veces1 = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i < valores.size(); i++) {

                veces1++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "verde";

                if (veces1 > 4) {
                    valor = valores.get(i) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = valores.get(i - pos_vs) / 1000.0;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = valores.get(i) / 1000;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces1 == 7) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces1 = 0;
                    pos_vs = 2;
                }
            }
            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.size(); a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 3) {
                    porcentajeTot = (double) totales.get(a) / (double) totales.get(a - pos_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);

        } catch (Exception e) {
            logger.info("Ap " + e);
            jsonDias = null;
        }

        return jsonDias;
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected JsonObject limpiXmlNormalidad(String xmlStr) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            //logger.info("Celldata"+rootNode.getChild("CellData",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                //logger.info("Celda " +element.getAttributeValue("CellOrdinal"));

                String valor = element.getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonNormalidad(valoresCeldas);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        return jsonRepuestas;

    }

    @SuppressWarnings("unused")
    protected JsonObject armaJsonNormalidad(List<String> valoresCeldas) {

        String arrayNombres[] = {
            "Vigente De 0 A 1",
            "Temprana 2",
            "Atrasada 1a De 3 A 7",
            "Tardia 1b De 8 A 13",
            "Atrasada 2 A 13",
            "Vencida 2a De 14 A 25",
            "Dificil 2b De 26 A 39",
            "Vencida De 14 A 39",
            "Saldo Cartera De 0 A 39 Semanas",
            "Legal 40 a 55",
            "Cazadores 56 +",
            "Saldo De Cartera"};

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonFilas = new JsonObject();

        try {

            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###");
            int columnas = 5;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - (columnas * 4));
            int indiceColumnas = 0;

            logger.info("Inicio :" + indice);
            while (indice < valoresCeldas.size() - (columnas * 3)) {

                //logger.info(formateador.format(Double.parseDouble(valoresCeldas.get(indice))/1000.0));
                totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                indiceColumnas++;
                indice++;
            }

            int fin = (valoresCeldas.size() - (columnas * 4));
            int veces = 0;

            JsonObject valorPorcentaje = null;

            int totalFilas = 0;
            for (int i = 0; i < valoresCeldas.size(); i++) {

                veces++;
                valorPorcentaje = new JsonObject();

                double porcentaje = 0.0;
                double valor = 0.0;

                int valorInt = 0;
                String color = "verde";

                valor = Double.parseDouble(valoresCeldas.get(i)) / 1000;
                //logger.info("Valor "+ formateador.format(valor));
                //logger.info("Total "+ formateador.format(totales.get(veces-1)));

                if (veces > 3) {
                    porcentaje = 0.0;
                } else {
                    porcentaje = (valor / (totales.get(veces - 1))) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                //logger.info("Porcentaje "+(int)Math.round(porcentaje)+"%");
                valorPorcentaje.addProperty("cantidad", formateador.format(valor));
                valorPorcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorPorcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorPorcentaje);

                if (veces == 5) {

                    jsonFilas.add(arrayNombres[totalFilas], arrayValorPorcentaje);
                    arrayValorPorcentaje = new JsonArray();

                    veces = 0;

                    totalFilas++;
                }

            }
        } catch (Exception e) {
            jsonFilas = null;
        }

        logger.info(jsonFilas.toString());

        return jsonFilas;

    }

    protected String getdia(int nuDia) {
        String dia = "";
        if (nuDia == 1) {
            dia = "lunes";
        } else if (nuDia == 2) {
            dia = "martes";
        } else if (nuDia == 3) {
            dia = "miercoles";
        } else if (nuDia == 4) {
            dia = "jueves";
        } else if (nuDia == 5) {
            dia = "viernes";
        } else if (nuDia == 6) {
            dia = "sabado";
        } else if (nuDia == 7) {
            dia = "domingo";
        }
        return dia;

    }

    public String getProperty(int valor) {

        String propiedad = "";
        switch (valor) {
            case 1:
                propiedad = "cabeceras";
                break;
            case 2:
                propiedad = "noAbonadas";
                break;
            case 3:
                propiedad = "totalCuentas";
                break;
            case 4:
                propiedad = "pendientesSurtir";
                break;
            case 5:
                propiedad = "autorizadas";
                break;
            case 6:
                propiedad = "clientesNuevos";
                break;
            case 7:
                propiedad = "creditosOtorgados";
                break;
            case 8:
                propiedad = "planClientes";
                break;
            case 9:
                propiedad = "level";
                break;

            default:
                propiedad = "";
                break;
        }

        return propiedad;

    }

}
