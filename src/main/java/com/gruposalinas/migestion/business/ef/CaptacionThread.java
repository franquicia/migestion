package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class CaptacionThread extends Thread {

    private String fecha;
    private String geografia;
    private String nivel;

    private String ceco;
    private String nombreCeco;

    private JsonObject respuestaHilo;

    public String getCeco() {
        return ceco;
    }

    public JsonObject getRespuestaHilo() {
        return respuestaHilo;
    }

    public CaptacionThread(String fecha, String geografia, String nivel) {

        this.fecha = fecha;
        this.geografia = geografia;
        this.nivel = nivel;

    }

    @Override
    public void run() {

        JsonObject principal = new JsonObject();

        try {

            ceco = geografia.substring(0, 6);

            nombreCeco = geografia.substring(9);

            IndicadoresBI indicadorBI = new IndicadoresBI();

            principal.addProperty("ceco", ceco);
            principal.addProperty("nombre_ceco", nombreCeco);

            JsonObject captacionVista = indicadorBI.getSaldoCaptacionVista(fecha, geografia, nivel);
            JsonObject captacionPlazo = indicadorBI.getSaldoCaptacionPlazo(fecha, geografia, nivel);

            double compromiso = 0.0;

            double miAvance = 0.0;

            double nosFalta = 0.0;

            double porcentaje = 0.0;

            JsonObject indicador = null;

            if (captacionPlazo != null) {

                JsonArray totales = captacionPlazo.get("saldoCaptacionPlazo").getAsJsonObject()
                        .get("totales").getAsJsonArray();

                // Sacar posiciones 2(Compromiso) y 3(Real)
                if (totales != null) {

                    indicador = new JsonObject();

                    compromiso = Double.parseDouble(
                            totales.get(2).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    miAvance = Double.parseDouble(
                            totales.get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    nosFalta = compromiso - miAvance;

                    porcentaje = (100.0 * miAvance) / compromiso;

                    indicador.addProperty("compromiso", Math.round(compromiso));

                    indicador.addProperty("miAvance", Math.round(miAvance));

                    indicador.addProperty("nosFalta", Math.round(nosFalta));

                    indicador.addProperty("porcentaje", Math.round(porcentaje));

                }

                principal.add("saldoPlazo", indicador);

            }

            compromiso = 0.0;

            miAvance = 0.0;

            nosFalta = 0.0;

            porcentaje = 0.0;

            if (captacionVista != null) {

                JsonArray totales = captacionVista.get("saldoCaptacionVista").getAsJsonObject()
                        .get("totales").getAsJsonArray();

                // Sacar posiciones 3(Compromiso) y 4(Real)
                if (totales != null) {

                    indicador = new JsonObject();

                    compromiso = Double.parseDouble(
                            totales.get(2).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    miAvance = Double.parseDouble(
                            totales.get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    nosFalta = compromiso - miAvance;

                    porcentaje = (100.0 * miAvance) / compromiso;

                    indicador.addProperty("compromiso", Math.round(compromiso));

                    indicador.addProperty("miAvance", Math.round(miAvance));

                    indicador.addProperty("nosFalta", Math.round(nosFalta));

                    indicador.addProperty("porcentaje", Math.round(porcentaje));

                }

                principal.add("saldoVista", indicador);

            }

            respuestaHilo = principal;

        } catch (Exception e) {

            /*principal = new JsonObject();

             JsonObject indicador = new JsonObject();

             indicador.addProperty("compromiso", 0);

             indicador.addProperty("miAvance", 0);

             indicador.addProperty("nosFalta", 0);

             indicador.addProperty("porcentaje", 0);


             principal.add("saldoPlazo", indicador);
             principal.add("saldoVista", indicador);*/
        }

    }

}
