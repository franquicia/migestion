package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.business.GeografiaBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.domain.GeografiaDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;

public class PorcentajeTiempoRealHijosBI extends Thread {

    private Logger logger = LogManager.getLogger(PorcentajeTiempoRealHijosBI.class);

    private String ceco;
    private String nombreCeco;
    private String fecha;
    private String nivel;
    private String geografia;
    private String negocioClase;
    private JsonObject respuestaHilo;

    @Autowired
    GeografiaBI geografiaNuevoBI;

    public PorcentajeTiempoRealHijosBI(String ceco, String nombreCeco, String nivel, String geo1, String negocio, String fecha) {

        this.ceco = ceco;
        this.nombreCeco = nombreCeco;
        this.fecha = fecha;
        this.nivel = nivel;
        this.geografia = geo1;
        this.negocioClase = "96";//negocio;  Se cambio por el negocio de CyC

        //		System.out.println("DATOS EN EL CONSTRUCTOR *****  CECO ---- "+this.ceco + "NIVEL ---- " + this.nivel + "FECHA ---- " + this.fecha + "GEOGRAFIA ----" + this.geografia+ "NEGOCIO ---- " + this.negocioClase);
    }

    public PorcentajeTiempoRealHijosBI() {

    }

    @Override
    public void run() {

        try {

            IndicadoresTiempoRealBI indicadoresTiempoRealBI = new IndicadoresTiempoRealBI();

            ExtraccionFinancieraBI extraccionFinancieraBI = new ExtraccionFinancieraBI();

            logger.info("EJECUTNANDO HILO CON DATOS *****  CECO ---- " + this.ceco + "NIVEL ---- " + this.nivel + "FECHA ---- " + this.fecha + "GEOGRAFIA ----" + this.geografia + "NEGOCIO ---- " + this.negocioClase);

            String realConsumo = indicadoresTiempoRealBI.getRealTotalColocacion(this.ceco, this.nivel);
            String resPersonales = indicadoresTiempoRealBI.getRealTotal(this.ceco, this.nivel);

            JsonParser parser = new JsonParser();

            JsonObject realConsumoObj = parser.parse(realConsumo).getAsJsonObject();
            JsonObject realPersonalesObj = parser.parse(resPersonales).getAsJsonObject();

            logger.info(realConsumoObj.get("compromisoHoy").getAsString());
            logger.info(realConsumoObj.get("avance").getAsString());

            logger.info(realPersonalesObj.get("compromisoHoy").getAsString());
            logger.info(realPersonalesObj.get("avance").getAsString());

            JsonObject compromisoConsumo = extraccionFinancieraBI.getConsumoColocacionXML(this.fecha, this.geografia, this.nivel);
            JsonObject compromisoPersonales = extraccionFinancieraBI.getConsumoColocacionTiempoRealXML(this.fecha, this.geografia, this.nivel);

            JsonObject dataConsumo = compromisoConsumo.get("consumoColocacion").getAsJsonObject();
            JsonObject dataPersonales = compromisoPersonales.get("consumoColocacion").getAsJsonObject();


            /*xmlPersonales = extraccionFinancieraBI.getConsumoColocacionTiempoRealXML(fecha, geografia, nivel);
             logger.info("PERSONALES SEMANA :)" + xmlPersonales);
             JsonObject data = xmlPersonales.getAsJsonObject("consumoColocacion");
             logger.info("ARRAY DATA" + data.toString());
             */
 /* xmlPersonales == compromisoPersonales*/
            String hoy = getNameToday();
            String[] dias = {"lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"};

            double compromisoHoyPersonales = 0.0f;
            double compromisoDiasAnterioresPersonales = 0.0f;
            double colocadoDiasAnterioresPersonales = 0.0f;

            double compromisoHoyConsumo = 0.0f;
            double compromisoDiasAnterioresConsumo = 0.0f;
            double colocadoDiasAnterioresConsumo = 0.0f;

            try {

                switch (hoy) {

                    case "lunes":
                        // PERSONALES
                        compromisoHoyPersonales = Double.parseDouble(
                                dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        compromisoDiasAnterioresPersonales += 0;
                        colocadoDiasAnterioresPersonales += 0;

                        //CONSUMO
                        compromisoHoyConsumo = Double.parseDouble(
                                dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        compromisoDiasAnterioresConsumo += 0;
                        colocadoDiasAnterioresConsumo += 0;

                        break;
                    case "martes":
                        // PERSONALES
                        compromisoHoyPersonales = Double.parseDouble(
                                dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoHoyConsumo = Double.parseDouble(
                                dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 1; i++) {

                            compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            //CONSUMO
                            compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                        }
                        break;
                    case "miercoles":
                        // PERSONALES
                        compromisoHoyPersonales = Double.parseDouble(
                                dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoHoyConsumo = Double.parseDouble(
                                dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 2; i++) {

                            compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            //CONSUMO
                            compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "jueves":
                        // PERSONALES
                        compromisoHoyPersonales = Double.parseDouble(
                                dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoHoyConsumo = Double.parseDouble(
                                dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 3; i++) {

                            compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            //CONSUMO
                            compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "viernes":
                        // PERSONALES
                        compromisoHoyPersonales = Double.parseDouble(
                                dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoHoyConsumo = Double.parseDouble(
                                dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 4; i++) {

                            compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            //CONSUMO
                            compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "sabado":
                        // PERSONALES
                        compromisoHoyPersonales = Double.parseDouble(
                                dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoHoyConsumo = Double.parseDouble(
                                dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 5; i++) {

                            compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            //CONSUMO
                            compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "domingo":
                        // PERSONALES
                        compromisoHoyPersonales = Double.parseDouble(
                                dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoHoyConsumo = Double.parseDouble(
                                dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 6; i++) {

                            compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            //CONSUMO
                            compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));

                            colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                }

            } catch (Exception e) {
                logger.info(e);
            }

            //Calculo el compromiso semanal de Personales y Calculo el compromiso semanal de Consumo
            double compromisoSemanalPersonales = 0.0;
            double compromisoSemanalConsumo = 0.0;

            for (int i = 0; i < 7; i++) {

                //PERSONALES
                compromisoSemanalPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                        .get("cantidad").getAsString().replace(",", ""));

                //CONSUMO
                compromisoSemanalConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                        .get("cantidad").getAsString().replace(",", ""));

            }

            //Obtengo los compromisos por Hora de Personales y Consumo
            String dataDia = getCompromisosDia(this.ceco);
            JsonObject compDia = parser.parse(dataDia).getAsJsonObject();

            Calendar calendario = Calendar.getInstance();

            int hora = calendario.get(Calendar.HOUR_OF_DAY);

            int compromisoDiaPersonales = 0;
            int compromisoDiaConsumo = 0;

            JsonArray con = compDia.getAsJsonArray("compromisosConsumo");

            for (int i = 0; i < con.size(); i++) {

                System.out.println(con.get(i).getAsJsonObject().get("hora"));

                if (Integer.parseInt(con.get(i).getAsJsonObject().get("hora").getAsString()) <= hora) {

                    System.out.println(con.get(i).getAsJsonObject().get("compromiso").getAsString());

                    compromisoDiaConsumo += Integer.parseInt(con.get(i).getAsJsonObject().get("compromiso").getAsString());

                }

            }

            JsonArray per = compDia.getAsJsonArray("compromisosPersonales");

            for (int i = 0; i < per.size(); i++) {

                System.out.println(per.get(i).getAsJsonObject().get("hora"));

                if (Integer.parseInt(per.get(i).getAsJsonObject().get("hora").getAsString()) <= hora) {

                    System.out.println(per.get(i).getAsJsonObject().get("compromiso").getAsString());

                    compromisoDiaPersonales += Integer.parseInt(per.get(i).getAsJsonObject().get("compromiso").getAsString());

                }

            }

            System.out.println("Personales : - " + compromisoDiaPersonales);
            System.out.println("Consumo : - " + compromisoDiaConsumo);

            //Porcentaje día Personales = (avanceDia * 100)/compromisoDiaPersonales
            double porcentajeDiaPersonales = 0.0;

            if (compromisoDiaPersonales > 0) {

                porcentajeDiaPersonales = ((Math.round((Double.parseDouble(realPersonalesObj.get("avance").getAsString()))) * 100) / Math.round(compromisoDiaPersonales));

            } else {

                porcentajeDiaPersonales = 0;

            }

            //Porcentaje día Consumo= (avanceDia * 100)/compromisoDiaConsumo
            double porcentajeDiaConsumo = 0.0;

            if (compromisoDiaConsumo > 0) {

                porcentajeDiaConsumo = (Math.round((Double.parseDouble(realConsumoObj.get("avance").getAsString())) * 100) / Math.round(compromisoDiaConsumo));

            } else {

                porcentajeDiaConsumo = 0;

            }

            //Porcentaje Semana Personales = (avanceDia * 100)/compromisoDiaPersonales
            double porcentajeSemanaPersonales = 0.0;

            if ((compromisoDiasAnterioresPersonales + compromisoDiaPersonales) > 0) {

                porcentajeSemanaPersonales = ((Math.round((Double.parseDouble(realPersonalesObj.get("avance").getAsString())) + Math.round(colocadoDiasAnterioresPersonales)) * 100) / (Math.round(compromisoDiasAnterioresPersonales) + Math.round(compromisoDiaPersonales)));

            } else {

                porcentajeSemanaPersonales = 0;

            }

            //Porcentaje día Consumo= (avanceDia * 100)/compromisoDiaConsumo
            double porcentajeSemanaConsumo = 0.0;

            if ((compromisoDiasAnterioresConsumo + compromisoDiaConsumo) > 0) {

                porcentajeSemanaConsumo = ((Math.round((Double.parseDouble(realConsumoObj.get("avance").getAsString())) + Math.round(colocadoDiasAnterioresConsumo)) * 100) / (Math.round(compromisoDiasAnterioresConsumo) + Math.round(compromisoDiaConsumo)));

            } else {

                porcentajeSemanaConsumo = 0;

            }

            logger.info("RESPUESTA DE SERVICIO PORCENTAJE TIEMPO REAL HIJOS THREADS");

            logger.info("Geografia: " + this.geografia);
            logger.info("Ceco: " + this.ceco);
            logger.info("Nombre Ceco: " + this.nombreCeco);

            logger.info("**********   PERSONALES   ***********");
            logger.info("**********   DIA   ***********");
            logger.info("Compromiso Dia: " + compromisoHoyPersonales);
            logger.info("Cuanto Deberia llevar este momento: " + compromisoDiaPersonales);
            logger.info("Cuanto llevo colocado: " + realPersonalesObj.get("avance"));
            logger.info("Porcentaje: " + porcentajeDiaPersonales);
            logger.info("Porcentaje Round: " + Math.round(porcentajeDiaPersonales));
            logger.info("Nos faltan: " + (compromisoHoyPersonales - (Double.parseDouble(realPersonalesObj.get("avance").getAsString()))));
            logger.info("**********   DIA   ***********");
            logger.info("**********   SEMANA   ***********");
            logger.info("Compromiso Semana: " + compromisoSemanalPersonales);
            logger.info("Cuanto Deberia llevar este momento: " + (compromisoDiasAnterioresPersonales + (Double.parseDouble(realPersonalesObj.get("avance").getAsString()))));
            logger.info("Cuanto llevo colocado: " + (colocadoDiasAnterioresPersonales + (Double.parseDouble(realPersonalesObj.get("avance").getAsString()))));
            logger.info("Porcentaje: " + porcentajeSemanaPersonales);
            logger.info("Porcentaje Round: " + Math.round(porcentajeSemanaPersonales));
            logger.info("Nos faltan: " + (compromisoSemanalPersonales - ((colocadoDiasAnterioresPersonales) + (Double.parseDouble(realPersonalesObj.get("avance").getAsString())))));
            logger.info("**********   SEMANA   ***********");
            logger.info("**********   PERSONALES   ***********");

            logger.info("**********   CONSUMO   ***********");
            logger.info("**********   DIA   ***********");
            logger.info("Compromiso Dia: " + compromisoHoyConsumo);
            logger.info("Cuanto Deberia llevar este momento: " + compromisoDiaConsumo);
            logger.info("Cuanto llevo colocado: " + realConsumoObj.get("avance"));
            logger.info("Porcentaje: " + porcentajeDiaConsumo);
            logger.info("Porcentaje Round: " + Math.round(porcentajeDiaConsumo));
            logger.info("Nos faltan: " + (compromisoHoyConsumo - (Double.parseDouble(realConsumoObj.get("avance").getAsString()))));
            logger.info("**********   DIA   ***********");
            logger.info("**********   SEMANA   ***********");
            logger.info("Compromiso Semana: " + compromisoSemanalConsumo);
            logger.info("Cuanto Deberia llevar este momento: " + (compromisoDiasAnterioresConsumo + (Double.parseDouble(realConsumoObj.get("avance").getAsString()))));
            logger.info("Cuanto llevo colocado: " + (colocadoDiasAnterioresConsumo + (Double.parseDouble(realConsumoObj.get("avance").getAsString()))));
            logger.info("Porcentaje: " + porcentajeSemanaConsumo);
            logger.info("Porcentaje Round: " + Math.round(porcentajeSemanaConsumo));
            logger.info("Nos faltan: " + (compromisoSemanalConsumo - ((colocadoDiasAnterioresConsumo) + (Double.parseDouble(realConsumoObj.get("avance").getAsString())))));
            logger.info("**********   SEMANA   ***********");
            logger.info("**********   CONSUMO   ***********");

            logger.info("RESPUESTA DE SERVICIO PORCENTAJE TIEMPO REAL HIJOS THREADS");

            this.respuestaHilo = new JsonObject();

            this.respuestaHilo.addProperty("geografia", this.geografia);
            this.respuestaHilo.addProperty("ceco", this.ceco);
            this.respuestaHilo.addProperty("nombreCeco", this.nombreCeco);
            this.respuestaHilo.addProperty("valorDiaPersonales", Math.round(porcentajeDiaPersonales) + "");
            this.respuestaHilo.addProperty("valorDiaConsumo", Math.round(porcentajeDiaConsumo) + "");
            this.respuestaHilo.addProperty("valorSemanaPersonales", Math.round(porcentajeSemanaPersonales) + "");
            this.respuestaHilo.addProperty("valorSemanaConsumo", (Math.round(porcentajeSemanaConsumo)) + "");

            /*
             this.respuestaHilo.addProperty("compromisoSemanalPersonales", compromisoSemanalPersonales);
             this.respuestaHilo.addProperty("compromisoSemanalConsumo", compromisoSemanalConsumo);

             this.respuestaHilo.addProperty("avancePersonalesReal", realPersonalesObj.get("avance").getAsString());
             this.respuestaHilo.addProperty("compromisoHoyPersonales", compromisoHoyPersonales);
             this.respuestaHilo.addProperty("compromisoDiasAnterioresPersonales", compromisoDiasAnterioresPersonales);
             this.respuestaHilo.addProperty("colocadoDiasAnterioresPersonales", colocadoDiasAnterioresPersonales);

             this.respuestaHilo.addProperty("avanceConsumoReal", realConsumoObj.get("avance").getAsString());
             this.respuestaHilo.addProperty("compromisoHoyConsumo", compromisoHoyConsumo);
             this.respuestaHilo.addProperty("compromisoDiasAnterioresConsumo", compromisoDiasAnterioresConsumo);
             this.respuestaHilo.addProperty("colocadoDiasAnterioresConsumo", colocadoDiasAnterioresConsumo);
             */
        } catch (Exception e) {

            //   logger.info("Algo Ocurrio InfoTiempoRealBI -- " + e);
            this.respuestaHilo = new JsonObject();

            this.respuestaHilo.addProperty("geografia", this.geografia);
            this.respuestaHilo.addProperty("ceco", this.ceco);
            this.respuestaHilo.addProperty("nombreCeco", this.nombreCeco);

            this.respuestaHilo.addProperty("valorDiaPersonales", "-1000");
            this.respuestaHilo.addProperty("valorDiaConsumo", "-1000");

            this.respuestaHilo.addProperty("valorSemanaPersonales", "-1000");
            this.respuestaHilo.addProperty("valorSemanaConsumo", "-1000");
        }

    }

    /*
     * Metodo que obtiene los indicadores de cada hijo
     */
    public JsonArray getTiempoRealHijos(String ceco, String nivel, String geo1, String negocio, String fecha) {

        this.negocioClase = negocio;

        JsonArray data = null;

        List<CecoDTO> lista = obtieneHijos(ceco);
        JsonArray listaResult = new JsonArray();

        List<PorcentajeTiempoRealHijosBI> arrayThreads = new ArrayList<PorcentajeTiempoRealHijosBI>();

        try {

            for (CecoDTO cecoDTO : lista) {
                String geografia = cecoDTO.getIdCeco() + " - " + cecoDTO.getDescCeco();
                logger.info("ESTA ES UNA GEOGRAFIA PARA CONSUMIR ---- " + geografia);
                //EL NIVEL LLEGA DE PARAMETRO, EL NEGOCIO PARAMETRO (81 - 41), LA FECHA LLEGA DIRECTA
                PorcentajeTiempoRealHijosBI hilo = new PorcentajeTiempoRealHijosBI(cecoDTO.getIdCeco(), cecoDTO.getDescCeco(), nivel, geografia, negocio, fecha);
                arrayThreads.add(hilo);
                hilo.start();
            }

            /*
             * Ciclo que servira para verificar si todo los hilos ya terminaron
             */
            boolean terminaron = false;
            List<PorcentajeTiempoRealHijosBI> aElminar = new ArrayList<PorcentajeTiempoRealHijosBI>();
            while (!terminaron) {
                Iterator<PorcentajeTiempoRealHijosBI> it = arrayThreads.iterator();
                int cont = 0;

                // System.out.println("Contador :"+cont);
                if (cont == arrayThreads.size()) {
                    terminaron = true;
                }
                while (it.hasNext()) {
                    PorcentajeTiempoRealHijosBI hilo = it.next();
                    if (hilo.respuestaHilo != null) {
                        listaResult.add(hilo.respuestaHilo);
                        System.out.println("Agregue CECO " + hilo.ceco);
                        aElminar.add(hilo);
                        cont++;
                    }
                }
                for (PorcentajeTiempoRealHijosBI eliminarHilo : aElminar) {
                    arrayThreads.remove(eliminarHilo);
                }
                aElminar.clear();
            }

            /*
             * Envia ArrayObject con los indicadores de cada hijo para generar
             * tabla de totales
             */
        } catch (Exception e) {
            logger.info("Ap en getIndicadoresHijos() ");
            logger.info("Exception para Ceco (" + ceco + ") " + e);

            return new JsonArray();
        }

        // System.out.println("Lista Respuesta " +listaHijos);
        return listaResult;

    }

    public List<CecoDTO> obtieneHijos(String ceco) {

        List<CecoDTO> listaCecos = new ArrayList<>();

        //		System.out.println("Negocio Clase :" + negocioClase);
        CecoBI cecoBi = (CecoBI) GTNAppContextProvider.getApplicationContext().getBean("cecoBI");
        listaCecos = cecoBi.buscaCecosSuperior(ceco, "" + negocioClase);

        System.out.println("****** TAMA�O LISTA *****:" + listaCecos.size());

        return listaCecos;
    }

    private String ejecutaServiciosTiempoReal() {
        String res = "";

        return res;

    }

    private String getNameToday() {

        String hoy = "";

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {

            case Calendar.MONDAY:
                hoy = "lunes";
                break;
            case Calendar.TUESDAY:
                hoy = "martes";
                break;
            case Calendar.WEDNESDAY:
                hoy = "miercoles";
                break;
            case Calendar.THURSDAY:
                hoy = "jueves";
                break;
            case Calendar.FRIDAY:
                hoy = "viernes";
                break;
            case Calendar.SATURDAY:
                hoy = "sabado";
                break;
            case Calendar.SUNDAY:
                hoy = "domingo";
                break;
        }

        return hoy;

    }

    public String getCompromisosDia(String ceco) {

        List<GeografiaDTO> geografiaList = null;

        //Obtener toda la geografia del ceco
        //String cadenaGeografia = "11046.236737.236276.236080.2752";
        String cadenaGeografia = "11046";
        String jsonString = "";

        ParametroBI parametro = (ParametroBI) GTNAppContextProvider.getApplicationContext().getBean("parametroBI");

        List<ParametroDTO> parametros = parametro.obtieneParametros("PREFIJO_REAL");
        cadenaGeografia = parametros.get(0).getValor();

        GeografiaBI geografiaNuevoBI = (GeografiaBI) GTNAppContextProvider.getApplicationContext().getBean("GeografiaBI");
        geografiaList = geografiaNuevoBI.obtieneGeografia(ceco, null, null, null);

        if (geografiaList != null) {
            if (geografiaList.size() > 0) {
                GeografiaDTO geo = geografiaList.get(0);

                if (geo.getIdTerritorio() != 0) {
                    cadenaGeografia += "." + geo.getIdTerritorio();
                }

                if (geo.getIdZona() != 0) {
                    cadenaGeografia += "." + geo.getIdZona();
                }

                if (geo.getIdRegion() != 0) {
                    cadenaGeografia += "." + geo.getIdRegion();
                }

                if (Integer.parseInt(geo.getIdCeco()) != geo.getIdRegion() && geo.getIdRegion() != 0) {
                    cadenaGeografia += "." + geo.getIdCeco().substring(2);
                }

            }
        }

        System.out.println("Cadena geografia " + cadenaGeografia);

        String jsonRespuesta = "";

        try {

            String consumo = "\"compromisosConsumo\":" + obtieneResultado("ColocacionConsumo", cadenaGeografia);
            String personales = "\"compromisosPersonales\":" + obtieneResultado("ColocacionPersonales", cadenaGeografia);

            jsonRespuesta = "{" + consumo + "," + personales + "}";

        } catch (Exception e) {
            return "{\"compromisosConsumo\":[], \"compromisosPersonales\":[]}";
        }

        return jsonRespuesta;

    }

    private String getJsonPorhora(String xml) {

        JsonArray jsonRespuesta = new JsonArray();
        String respuesta = "";

        try {

            StringReader stringReader = new StringReader(xml);
            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = builder.build(stringReader);

            //Se obtiene la raiz
            Element rootNode = document.getRootElement();
            Element consulta = rootNode.getChild("Consulta");
            List<Element> table = consulta.getChildren("Table");

            JsonObject hora = null;
            for (int i = 0; i < table.size(); i++) {

                if (i != (table.size() - 1)) {
                    Element elemento = table.get(i);

                    hora = new JsonObject();
                    hora.addProperty("hora", elemento.getChild("HORA").getValue());
                    hora.addProperty("compromiso", elemento.getChild("OBJETIVO").getValue());

                    jsonRespuesta.add(hora);
                }
            }

            String array = jsonRespuesta.toString();
            System.out.println("Json: " + array);

        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }

        return jsonRespuesta.toString();
    }

    private String obtieneResultado(String service, String strGeografia) {

        String jsonString = "";
        //llama servicio SOA
        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;
        StringBuilder strBuild = new StringBuilder();

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml
                    = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<siew:TR" + service + "PorHora>"
                    + "<siew:geografia>" + strGeografia + "</siew:geografia>"
                    + "</siew:TR" + service + "PorHora>"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();
            } finally {
                wr.close();
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();

            String cadena = strBuild.toString().trim();
            //logger.info(cadena);

            if (cadena.contains("No se encontro informacion con los parametros especificados")) {
                //Regresar null
            } else {
                StringReader stringReader = new StringReader(cadena);
                try {

                    SAXBuilder builder = new SAXBuilder();
                    Document document = builder.build(stringReader);

                    Element rootNode = document.getRootElement();
                    Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
                    Element response = body.getChild("TR" + service + "PorHoraResponse", Namespace.getNamespace("http://siewebservices/"));
                    Element result = response.getChild("TR" + service + "PorHoraResult", Namespace.getNamespace("http://siewebservices/"));

                    //
                    String resultado = result.getValue();

                    jsonString = getJsonPorhora(resultado);

                } catch (Exception e) {
                    //return "{\"compromisoDia\":[]}";
                    return "[]";
                }
            }

        } catch (Exception e) {
            //return "{\"compromisoDia\":[]}";
            return "[]";
        }

        return jsonString;

    }

}
