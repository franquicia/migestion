package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.GeografiaBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.domain.GeografiaDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;

public class IndicadoresTiempoRealBI {

    Logger logger = LogManager.getLogger(IndicadoresTiempoRealBI.class);

    @Autowired
    GeografiaBI geografiaBI;

    public static void main(String args[]) {

        String json = "";
        String respuesta = "";
//		respuesta = getRealConsumo("480100", "T").toString();
//
//		respuesta = getRealPersonales("480100", "T").toString();
//
//		respuesta = getRealTotal("480100", "T").toString();

        System.out.println(json.toString());
    }

    
    //SE UTILIZA PARA OBTENER LA COLOCACION DE CREDITO
    public String getRealTotal(String ceco, String nivel) {

        String xmlResult = null;
        String geografia = ceco;
        //IMP_ACT
        int avanceReal = 0;
        int uni_act = 0;
        int var_vta = 0;
        int var_uni = 0;

        //CAMBIÓ EL METODO Y YA NO SE REQUIERE IR POR LA GEOGRAFIA
        geografia = getGeografia(ceco);

        //		System.out.println("Geografia : "+geografia );
        //		System.out.println("Nivel : "+nivel );
        /*if (geografia.length() == 6){
         geografia = geografia.substring(2, 6);
         System.out.println("Geografia Nuevo : "+geografia );
         }*/
        //String geografia ="110460.236737.236276.3531";
        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<siew:TiempoRealColocacionPersonales>"
                + "<siew:geografia>" + geografia + "</siew:geografia>"
                + "<siew:nivel>" + nivel + "</siew:nivel>"
                + "</siew:TiempoRealColocacionPersonales>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        String response = getResponseService(request);

        //RESPONSE MAL
        if (response != null && (!response.contains("No se encontro informacion con los parametros especificados"))) {

            xmlResult = getXMLServiceResult("TiempoRealColocacionPersonales", response);

            // RESULTADO NUEVO TIEMPO REAL COLOCACION
            //logger.info("************ INICIO TIEMPO REAL COLOCACION *****************");
            //logger.info(xmlResult);
            //logger.info("************ FIN TIEMPO REAL COLOCACION *****************");
            // Inserta implementacion para obtenmer los datos del XML
            //logger.info("TiempoRealColocacionPersonales: ceco: " + ceco + ", geografia: " + geografia + ", nivel : " + nivel + " result =  " + xmlResult);
            StringReader stringReader = new StringReader(xmlResult);
            SAXBuilder builder = new SAXBuilder();
            Document document = null;
            try {
                document = builder.build(stringReader);

                Element rootNode = document.getRootElement();
                //logger.info("INICIO OBTENEMOS DATOS DEL XML");
                //logger.info(rootNode);
                Element result = rootNode.getChild("Consulta");

                List<Element> listElements = result.getChildren("Table");

                Iterator it = listElements.iterator();

                while (it.hasNext()) {
                    Element obj = (Element) it.next();

                    if (obj.getChild("DES").getValue().equals("TOTAL")) {
                        //logger.info("******TOTALES******");
                        //logger.info("VALOR DE MI IMP_ACT" + obj.getChild("IMP_ACT").getValue());
                        avanceReal = Integer.parseInt(obj.getChild("IMP_ACT").getValue());
                        uni_act = Integer.parseInt(obj.getChild("UNI_ACT").getValue());
                        var_vta = Integer.parseInt(obj.getChild("VAR_VTA").getValue());
                        var_uni = Integer.parseInt(obj.getChild("VAR_UNI").getValue());
                        //logger.info("******TOTALES******");
                    }
                }

                //logger.info("FIN OBTENEMOS DATOS DEL XML" + avanceReal);
            } catch (JDOMException e) {
                // TODO Auto-generated catch block
                logger.info("Catch 1");
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                logger.info("Catch 2");
                e.printStackTrace();
            } catch (Exception e) {

                // EL CECO NO EXISTE EN SIE, LO CACHAMOS DE ESTA MANERA PARA
                // CONTROLAR
                avanceReal = -1000;
                uni_act = -1000;
                var_vta = -1000;
                var_uni = -1000;

            }

        } else {

            logger.info("Algo ocurrio al obtener el XML en IndicadoresTiempoReal.getRealTotal() - ");
            logger.info("Con los datos - IndicadoresTiempoRealBI _ getRealTotal() - ceco" + ceco + " - nivel - " + nivel);

            avanceReal = -1000;
            uni_act = -1000;
            var_vta = -1000;
            var_uni = -1000;

        }

        /*
         * indicadoresTiempoRealBI
         linea 229
         *
         * */
 /*Este codigo sera duro ya que aun no se tiene bien los datos*/
        JsonObject respuesta = new JsonObject();

        respuesta.addProperty("compromisoHoy", 0);
        respuesta.addProperty("acumAlDia", 0);
        respuesta.addProperty("avance", avanceReal);
        respuesta.addProperty("unidades", 0);
        respuesta.addProperty("realDinero", 0);
        respuesta.addProperty("diferencia", 0);
        respuesta.addProperty("uni_act", uni_act);
        respuesta.addProperty("var_vta", var_vta);
        respuesta.addProperty("var_uni", var_uni);

        System.out.println(respuesta.toString());
        return respuesta.toString();
    }

    //SE UTILIZA PARA OBTENER LA COLOCACION DE CREDITO
    public String getRealTotalColocacion(String ceco, String nivel) {

        String xmlResult = null;
        String geografia = ceco;
        //IMP_ACT
        int avanceReal = 0;
        int uni_act = 0;
        int var_vta = 0;
        int var_uni = 0;

        geografia = getGeografia(ceco);

        //CAMBIÓ EL METODO Y YA NO SE REQUIERE IR POR LA GEOGRAFIA
        //getGeografia(ceco);
        //			System.out.println("Geografia : "+geografia );
        //			System.out.println("Nivel : "+nivel );

        /*if (geografia.length() == 6){
         geografia = geografia.substring(2, 6);
         System.out.println("Geografia Nuevo : "+geografia );
         }*/
        //String geografia ="110460.236737.236276.3531";
        //String geografia ="110460.236737.236276.3531";
        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<siew:TiempoRealColocacionConsumo>"
                + "<siew:geografia>" + geografia + "</siew:geografia>"
                + "<siew:nivel>" + nivel + "</siew:nivel>"
                + "</siew:TiempoRealColocacionConsumo>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        //	System.out.println("mi XML");
//			System.out.println(request);
        String response = getResponseService(request);

        if (response != null && (!response.contains("No se encontro informacion con los parametros especificados"))) {

            xmlResult = getXMLServiceResult("TiempoRealColocacionConsumo", response);

            //RESULTADO NUEVO TIEMPO REAL COLOCACION
            //logger.info("************ COLOCACION *****************");
            //logger.info(xmlResult);
            //logger.info("************ COLOCACION *****************");
            //Inserta implementacion para obtenmer los datos del XML
            //logger.info("TiempoRealColocacionConsumo: ceco: " + ceco + ", geografia: " + geografia + ", nivel : " + nivel + " result =  " + xmlResult);
            StringReader stringReader = new StringReader(xmlResult);
            SAXBuilder builder = new SAXBuilder();
            Document document = null;
            try {
                document = builder.build(stringReader);

                Element rootNode = document.getRootElement();
                logger.info("INICIO OBTENEMOS DATOS DEL XML");
                //logger.info("rootNode... " + rootNode);
                Element result = rootNode.getChild("Consulta");

                List<Element> listElements = result.getChildren("Table");

                //logger.info("listElements... " + listElements.toString());
                Iterator it = listElements.iterator();

                while (it.hasNext()) {
                    Element obj = (Element) it.next();

                    if (obj.getChild("DES").getValue().equals("TOTAL")) {
                        // logger.info("******TOTALES******");

                        avanceReal = Integer.parseInt(obj.getChild("IMP_ACT").getValue());
                        //logger.info("VALOR DE MI IMP_ACT " + avanceReal);

                        uni_act = Integer.parseInt(obj.getChild("UNI_ACT").getValue());
                        //logger.info("VALOR DE MI UNI_ACT "+ uni_act);

                        var_vta = Integer.parseInt(obj.getChild("VAR_VTA").getValue());
                        //logger.info("VALOR DE MI VAR_VTA "+ var_vta);

                        var_uni = Integer.parseInt(obj.getChild("VAR_UNI").getValue());
                        //logger.info("VALOR DE MI VAR_UNI "+ var_uni);

                        //logger.info("******TOTALES******");
                    }
                }

                //logger.info("FIN OBTENEMOS DATOS DEL XML: " + avanceReal);
            } catch (JDOMException e) {
                // TODO Auto-generated catch block
                logger.info("Catch 1");
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                logger.info("Catch 2");
                e.printStackTrace();
            } catch (Exception e) {

                //EL CECO NO EXISTE EN SIE, LO CACHAMOS DE ESTA MANERA PARA CONTROLAR
                avanceReal = -1000;
                uni_act = -1000;
                var_vta = -1000;
                var_uni = -1000;

            }
        } else {

            logger.info("Algo ocurrio al obtener el XML en IndicadoresTiempoReal.getRealTotalColocacion() - ");
            logger.info("Con los datos - IndicadoresTiempoRealBI _ getRealTotalColocacion() - ceco" + ceco + " - nivel - " + nivel);

            avanceReal = -1000;
            uni_act = -1000;
            var_vta = -1000;
            var_uni = -1000;

        }

        /*
         * indicadoresTiempoRealBI
         linea 229
         *
         * */
 /*Este codigo sera duro ya que aun no se tiene bien los datos*/
        JsonObject respuesta = new JsonObject();

        respuesta.addProperty("compromisoHoy", 0);
        respuesta.addProperty("acumAlDia", 0);
        respuesta.addProperty("avance", avanceReal);
        respuesta.addProperty("unidades", 0);
        respuesta.addProperty("realDinero", 0);
        respuesta.addProperty("diferencia", 0);
        respuesta.addProperty("uni_act", uni_act);
        respuesta.addProperty("var_vta", var_vta);
        respuesta.addProperty("var_uni", var_uni);

        //System.out.println(respuesta.toString());
        return respuesta.toString();
    }

    public String getRealTotalCredito(String ceco, String nivel) {

        String xmlResult = null;
        String geografia = ceco;
        //IMP_ACT
        int avanceReal = 0;
        int uni_act = 0;
        int var_vta = 0;
        int var_uni = 0;

        //CAMBIÓ EL METODO Y YA NO SE REQUIERE IR POR LA GEOGRAFIA
        //getGeografia(ceco);
        System.out.println("Geografia : " + geografia);
        System.out.println("Nivel : " + nivel);
        /*if (geografia.length() == 6){
         geografia = geografia.substring(2, 6);
         System.out.println("Geografia Nuevo : "+geografia );
         }*/

        //String geografia ="110460.236737.236276.3531";
        String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<TiempoRealColocacionCredito>"
                + "<siew:geografia>" + geografia + "</siew:geografia>"
                + "<siew:nivel>" + nivel + "</siew:nivel>"
                + "</TiempoRealColocacionCredito>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        String response = getResponseService(request);

        if (response != null && (!response.contains("No se encontro informacion con los parametros especificados"))) {

            xmlResult = getXMLServiceResult("TiempoRealColocacionCredito", response);

            //RESULTADO NUEVO TIEMPO REAL COLOCACION
            logger.info("************ COLOCACION *****************");
            logger.info(xmlResult);
            logger.info("************ COLOCACION *****************");
            //Inserta implementacion para obtenmer los datos del XML

            StringReader stringReader = new StringReader(xmlResult);
            SAXBuilder builder = new SAXBuilder();
            Document document = null;
            try {
                document = builder.build(stringReader);

                Element rootNode = document.getRootElement();
                logger.info("INICIO OBTENEMOS DATOS DEL XML");
                logger.info(rootNode);
                Element result = rootNode.getChild("Consulta");

                List<Element> listElements = result.getChildren("Table");

                Iterator it = listElements.iterator();

                while (it.hasNext()) {
                    Element obj = (Element) it.next();

                    if (obj.getChild("DES").getValue().equals("TOTAL")) {
                        logger.info("******TOTALES******");
                        logger.info("VALOR DE MI IMP_ACT" + obj.getChild("IMP_ACT").getValue());
                        avanceReal = Integer.parseInt(obj.getChild("IMP_ACT").getValue());
                        uni_act = Integer.parseInt(obj.getChild("UNI_ACT").getValue());
                        var_vta = Integer.parseInt(obj.getChild("VAR_VTA").getValue());
                        var_uni = Integer.parseInt(obj.getChild("VAR_UNI").getValue());
                        logger.info("******TOTALES******");
                    }
                }

                logger.info("FIN OBTENEMOS DATOS DEL XML" + avanceReal);

            } catch (JDOMException e) {
                // TODO Auto-generated catch block
                logger.info("Catch 1");
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                logger.info("Catch 2");
                e.printStackTrace();
            }
        } else {

            logger.info("Algo ocurrio al obtener el XML en IndicadoresTiempoReal.getRealTotalCredito() - ");
            logger.info("Con los datos - IndicadoresTiempoRealBI _ getRealTotalCredito() - ceco" + ceco + " - nivel - " + nivel);

            avanceReal = 0;
            uni_act = 0;
            var_vta = 0;
            var_uni = 0;

        }

        /*
         * indicadoresTiempoRealBI
         linea 229
         *
         * */
 /*Este codigo sera duro ya que aun no se tiene bien los datos*/
        JsonObject respuesta = new JsonObject();

        respuesta.addProperty("compromisoHoy", 0);
        respuesta.addProperty("acumAlDia", 0);
        respuesta.addProperty("avance", avanceReal);
        respuesta.addProperty("unidades", 0);
        respuesta.addProperty("realDinero", 0);
        respuesta.addProperty("diferencia", 0);
        respuesta.addProperty("uni_act", uni_act);
        respuesta.addProperty("var_vta", var_vta);
        respuesta.addProperty("var_uni", var_uni);

        //System.out.println(respuesta.toString());
        return respuesta.toString();
    }

    public String getResponseService(String xml) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        String result = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            //rd.close();
            //rd = null;
            inputSream = null;

            result = strBuild.toString().trim();

        } catch (Exception e) {
            logger.info("Ap ");
            result = null;
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return result;

    }

    public String getXMLServiceResult(String service, String resultXML) {

        String cadenaXml = "";

        try {
            StringReader stringReader = new StringReader(resultXML);

            //System.out.println("getXMLServiceResult");
            //System.out.println(stringReader);
            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(service + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(service + "Result", Namespace.getNamespace("http://siewebservices/"));

            Element rootNode2 = document.getRootElement();

            Element body2 = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response2 = body.getChild(service + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result2 = response.getChild(service + "Result", Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap ");
            cadenaXml = null;
        }

        return cadenaXml;

    }

    public String getGeografia(String ceco) {

        GeografiaBI geografiaBi2 = (GeografiaBI) GTNAppContextProvider.getApplicationContext().getBean("GeografiaBI");

        boolean territorio = false;
        boolean zona = false;
        boolean region = false;
        String geografia = "11046";
        //String geografia = "6528.";

        List<GeografiaDTO> listaGeografia;

        /*GeografiaDTO geografiaDTO = new GeografiaDTO();
         geografiaDTO.setIdRegion(236399);
         geografiaDTO.setIdZona(232822);
         geografiaDTO.setIdTerritorio(236737);
         geografiaDTO.setIdCeco(ceco);
         listaGeografia = new ArrayList<GeografiaDTO>();
         listaGeografia.add(geografiaDTO);*/
        try {

            ParametroBI parametro = (ParametroBI) GTNAppContextProvider.getApplicationContext().getBean("parametroBI");

            List<ParametroDTO> parametros = parametro.obtieneParametros("PREFIJO_REAL");
            geografia = parametros.get(0).getValor();

            listaGeografia = geografiaBi2.obtieneGeografia(ceco, "", "", "");

            if (listaGeografia != null) {
                if (listaGeografia.size() > 0) {

                    if (listaGeografia.get(0).getIdTerritorio() != 0) {
                        territorio = true;
                        geografia += "." + listaGeografia.get(0).getIdTerritorio();
                    }

                    if (listaGeografia.get(0).getIdZona() != 0) {
                        zona = true;
                        geografia += "." + listaGeografia.get(0).getIdZona();
                    }

                    if (listaGeografia.get(0).getIdRegion() != 0) {
                        region = true;
                        geografia += "." + listaGeografia.get(0).getIdRegion();
                    }

                    if ((Integer.parseInt(ceco) != listaGeografia.get(0).getIdTerritorio()) && (Integer.parseInt(ceco) != listaGeografia.get(0).getIdZona()) && (Integer.parseInt(ceco) != listaGeografia.get(0).getIdRegion())) {
                        geografia += "." + ceco.substring(2);
                    }

                }
            }

        } catch (Exception e) {
            logger.info("Ap en getGeografia() ");
            geografia = null;
        }
        return geografia;
    }

}
