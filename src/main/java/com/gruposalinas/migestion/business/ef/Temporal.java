package com.gruposalinas.migestion.business.ef;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.resources.GTNConstantes;

public class Temporal {

    private Logger logger = LogManager.getLogger(Temporal.class);

    public JsonObject getActivacionesBazDigital(String fecha) {
        logger.info("---------ActivacionesBazDigital------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();
        try {
            String xmlStr = obtieneXmlResult(fecha, "ActivacionesBazDigital");
            logger.info(xmlStr);
            jsonArray = limpiaXMLActivaciones(xmlStr);
            json = jsonDiasSemana(jsonArray);
            eliminaObjetos(json);
        } catch (Exception e) {
            logger.info("Ap getActivacionesBazDigital:" + e.getMessage());
            return null;
        }
        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("activacionesBazDigital", json);
        }

        return respuestaService;
    }

    public JsonObject getColocacionBazDigital(String fecha) {
        logger.info("---------PersonalesColocacionBAZDigital------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();
        try {
            String xmlStr = obtieneXmlResult(fecha, "PersonalesColocacionBAZDigital");
            logger.info(xmlStr);
            jsonArray = limpiaXMLColocacion(xmlStr);
            json = jsonDiasSemana(jsonArray);
            eliminaObjetos(json);
        } catch (Exception e) {
            logger.info("Ap getColocacionBAZDigital:" + e.getMessage());
            return null;
        }
        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("colocacionBAZDigital", json);
        }

        return respuestaService;
    }

    public String obtieneXmlResult(String fecha, String service) {
        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();
        String strService = service;
        String cadena = "";
        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");
            rc = (HttpURLConnection) url.openConnection();
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>" + "<soapenv:Body>" + "<siew:" + strService + ">" + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "</siew:" + strService + ">" + "</soapenv:Body>" + "</soapenv:Envelope>";

            logger.info("PETICION SOA: " + xml);

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info(" Ap en obtieneXmlResult: " + e.getMessage());
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }
        return cadenaXml;
    }

    protected JsonArray limpiaXMLActivaciones(String xmlStr) {
        JsonArray jsonArray = new JsonArray();
        try {
            StringReader stringReader = new StringReader(xmlStr);
            SAXBuilder builder = new SAXBuilder();
            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();
            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();
            List<Element> axes0 = axes.getChildren();

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;
            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            int totalCeldas = columnas * rowsCount;
            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;
            int veces = 0;
            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
            List<BigDecimal> listValoresCeldas = new ArrayList<BigDecimal>();
            List<BigDecimal> listaValoresTotales = new ArrayList<BigDecimal>();

            BigDecimal valorDouble = new BigDecimal(0.00);
            BigDecimal divisor = new BigDecimal(1000);
            int numFilas = 1;

            while (cont < totalCeldas) {
                veces++;

                if (celda1 == celda2) {
                    valorDouble = new BigDecimal(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()).divide(divisor, 8, RoundingMode.HALF_UP);
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {
                    cont++;
                    celda1 = cont;
                    valorDouble = new BigDecimal(0.00);
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                if (veces == columnas) {
                    logger.info("------------FILA-------------" + numFilas);
                    int posicionReal;

                    if (numFilas == 1 || numFilas == rowsCount) {
                        posicionReal = 4;
                    } else {
                        posicionReal = listValoresCeldas.size() - 1;
                    }

                    BigDecimal valorReal = new BigDecimal(0.00);

                    if (numFilas == rowsCount) {
                        valorReal = listaValoresTotales.get(posicionReal);
                    } else {
                        valorReal = listValoresCeldas.get(posicionReal);
                    }

                    for (int i = 1; i <= 3; i++) {
                        BigDecimal valorVs = new BigDecimal(0.00);

                        if (numFilas == rowsCount) {
                            valorVs = listaValoresTotales.get(posicionReal - i);
                        } else {
                            valorVs = listValoresCeldas.get(posicionReal - i);
                        }

                        BigDecimal valorCalculado = valorVs.subtract(valorReal);

                        logger.info("Valor real " + valorReal.toString());
                        logger.info("Valor vs: " + valorVs.toString());
                        logger.info("Valor Calculado: " + valorVs.toString());

                        if (numFilas == rowsCount) {
                            listaValoresTotales.add(valorCalculado);
                        } else {
                            listValoresCeldas.add(valorCalculado);
                        }
                    }
                    veces = 0;
                    numFilas++;
                }
            }
            int[] flags = {0, 1, 1, 1, 1, 1, 1, 1};
            jsonArray = armaJsonArrayActivaciones(listValoresCeldas, listaValoresTotales, flags, columnas);
        } catch (Exception e) {
            logger.info("Ap en limpiaXMLActivaciones:" + e.getMessage());
            return null;
        }
        return jsonArray;
    }

    protected JsonArray limpiaXMLColocacion(String xmlStr) {
        JsonArray jsonArray = new JsonArray();
        try {
            StringReader stringReader = new StringReader(xmlStr);
            SAXBuilder builder = new SAXBuilder();
            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();
            List<Element> axes0 = axes.getChildren();
            int columnas = 0;
            int rowsCount = 0;
            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            int totalCeldas = columnas * rowsCount;
            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;
            int veces = 0;
            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
            List<BigDecimal> listValoresCeldas = new ArrayList<BigDecimal>();
            List<BigDecimal> listaValoresTotales = new ArrayList<BigDecimal>();

            BigDecimal valorDouble = new BigDecimal(0.00);
            BigDecimal divisor = new BigDecimal(1000);

            int numFilas = 1;

            while (cont < totalCeldas) {

                veces++;

                if (celda1 == celda2) {
                    valorDouble = new BigDecimal(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()).divide(divisor, 8, RoundingMode.HALF_UP);
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {
                    cont++;
                    celda1 = cont;
                    valorDouble = new BigDecimal(0.00);
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                if (veces == columnas) {
                    logger.info("------------FILA-------------" + numFilas);

                    int posicionReal;

                    if (numFilas == 1 || numFilas == rowsCount) {
                        posicionReal = 5;
                    } else {
                        posicionReal = listValoresCeldas.size() - 2;
                    }

                    BigDecimal valorReal;

                    if (numFilas == rowsCount) {
                        valorReal = listaValoresTotales.get(posicionReal);
                    } else {
                        valorReal = listValoresCeldas.get(posicionReal);
                    }

                    for (int i = 1; i <= 4; i++) {
                        BigDecimal valorVs;

                        if (numFilas == rowsCount) {
                            valorVs = listaValoresTotales.get(posicionReal - i);
                        } else {
                            valorVs = listValoresCeldas.get(posicionReal - i);
                        }

                        BigDecimal valorCalculado = valorVs.subtract(valorReal);

                        logger.info("Valor real " + valorReal.toString());
                        logger.info("Valor vs: " + valorVs.toString());
                        logger.info("Valor Calculado: " + valorVs.toString());

                        if (numFilas == rowsCount) {
                            listaValoresTotales.add(valorCalculado);
                        } else {
                            listValoresCeldas.add(valorCalculado);
                        }
                    }
                    veces = 0;
                    numFilas++;
                }
            }
            int[] flags = {0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1};
            jsonArray = armaJsonArrayColocacion(listValoresCeldas, listaValoresTotales, flags, 11);
        } catch (Exception e) {
            //logger.info("Ap en limpiaXMLColocacion:" + e.getMessage());
            return null;
        }
        return jsonArray;
    }

    protected JsonArray armaJsonArrayActivaciones(List<BigDecimal> valoresCeldas, List<BigDecimal> valoresTotales, int[] flagsColumnas, int columnas) {
        JsonArray arrayFilas = new JsonArray();
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;
        int veces = 0;
        int pos_vs = 2;

        for (int i = 0; i < valoresCeldas.size(); i++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonActivacion(flagsColumnas, valoresCeldas, valoresTotales, pos_vs, i, veces, true);
                pos_vs = veces > 5 ? pos_vs + 2 : pos_vs;
                arrayValores.add(valor);
            }
            if (veces == columnas) {
                logger.info("------------FILA-------------");
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                veces = 0;
                pos_vs = 2;
            }
        }
        pos_vs = 2;
        veces = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonActivacion(flagsColumnas, valoresTotales, null, pos_vs, a, veces, false);
                arrayValores.add(valor);
            }
        }
        arrayFilas.add(arrayValores);
        return arrayFilas;
    }

    protected JsonArray armaJsonArrayColocacion(List<BigDecimal> valoresCeldas, List<BigDecimal> valoresTotales, int[] flagsColumnas, int columnas) {
        int veces = 0;
        int pos_vs = 3;
        JsonArray arrayFilas = new JsonArray();
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        for (int i = 0; i < valoresCeldas.size(); i++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonColocacion(flagsColumnas, valoresCeldas, valoresTotales, pos_vs, i, veces, true);
                pos_vs = veces > 7 ? pos_vs + 2 : pos_vs;
                arrayValores.add(valor);
            }
            if (veces == columnas) {
                logger.info("------------FILA-------------");
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                veces = 0;
                pos_vs = 3;
            }
        }
        pos_vs = 3;
        veces = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonColocacion(flagsColumnas, valoresTotales, null, pos_vs, a, veces, false);
                pos_vs = veces > 7 ? pos_vs + 2 : pos_vs;
                arrayValores.add(valor);
            }
        }
        arrayFilas.add(arrayValores);

        return arrayFilas;
    }

    private JsonObject mapearObjetoToJsonActivacion(int[] flagsColumnas, List<BigDecimal> listaInicio, List<BigDecimal> valoresTotales, int pos_vs, int i, int veces, boolean tipoLista) {
        BigDecimal porcentaje = new BigDecimal(0.00);
        JsonObject valor = new JsonObject();
        BigDecimal porCiento = new BigDecimal(100.00);
        BigDecimal divisor = new BigDecimal(0);

        if (veces > 5) {
            divisor = listaInicio.get(i - pos_vs);
            if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                porcentaje = listaInicio.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                logger.info("cantidad: " + listaInicio.get(i).toString());
                logger.info("Cantidad vs" + divisor.toString());
            }
        } else {
            if (tipoLista) {
                divisor = valoresTotales.get(veces - 1);
                if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                    porcentaje = listaInicio.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                    logger.info("cantidad: " + listaInicio.get(i).toString());
                    logger.info("Cantidad vs" + divisor.toString());
                }
            } else {
                porcentaje = new BigDecimal(100.00);
            }
        }

        valor = crearObjetoJson(listaInicio.get(i), porcentaje);
        return valor;
    }

    private JsonObject mapearObjetoToJsonColocacion(int[] flagsColumnas, List<BigDecimal> valoresCeldas, List<BigDecimal> valoresTotales, int pos_vs, int i, int veces, boolean tipoLista) {
        JsonObject valor = null;
        BigDecimal divisor = new BigDecimal(0.00);
        BigDecimal porcentaje = new BigDecimal(0.00);
        BigDecimal porCiento = new BigDecimal(100.00);

        if (veces > 7) {
            divisor = valoresCeldas.get(i - pos_vs);
            if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                porcentaje = valoresCeldas.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                logger.info("cantidad: " + valoresCeldas.get(i).toString());
                logger.info("Cantidad vs" + divisor.toString());
            }
        } else {
            if (tipoLista) {
                divisor = valoresTotales.get(veces - 1);
                if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                    porcentaje = valoresCeldas.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                    logger.info("cantidad: " + valoresCeldas.get(i).toString());
                    logger.info("Cantidad vs" + divisor.toString());
                }
            } else {
                porcentaje = new BigDecimal(100.00);
            }
        }

        valor = crearObjetoJson(valoresCeldas.get(i), porcentaje);

        return valor;
    }

    private JsonObject crearObjetoJson(BigDecimal monto, BigDecimal porcentaje) {
        JsonObject valor = new JsonObject();
        DecimalFormat formateador = new DecimalFormat("###,###.##");

        String color = porcentaje.compareTo(BigDecimal.ZERO) > 0 ? "verde" : "rojo";
        String valorStr = formateador.format(monto.setScale(4, RoundingMode.HALF_UP));

        valor.addProperty("cantidad", valorStr);
        valor.addProperty("porcentaje", porcentaje.setScale(2, RoundingMode.HALF_UP).intValue() + "%");
        valor.addProperty("color", color);

        return valor;
    }

    //Este metodo solo se copio para fines de prueba
    public static JsonObject jsonDiasSemana(JsonArray array) {
        JsonObject respuesta = new JsonObject();
        for (int i = 1; i <= array.size(); i++) {
            respuesta.add(getdia(i), array.get(i - 1).getAsJsonArray());
        }
        return respuesta;
    }

    //Este metodo solo se copio para fines de prueba
    public static JsonObject eliminaObjetos(JsonObject json) {
        JsonObject jsonLocal = json;
        return jsonLocal;
    }

    //Este metodo solo se copio para fines de prueba
    protected static String getdia(int nuDia) {
        String dia = "";
        if (nuDia == 1) {
            dia = "lunes";
        } else if (nuDia == 2) {
            dia = "martes";
        } else if (nuDia == 3) {
            dia = "miercoles";
        } else if (nuDia == 4) {
            dia = "jueves";
        } else if (nuDia == 5) {
            dia = "viernes";
        } else if (nuDia == 6) {
            dia = "sabado";
        } else if (nuDia == 7) {
            dia = "domingo";
        } else if (nuDia == 8) {
            dia = "totales";
        }
        return dia;
    }
}
