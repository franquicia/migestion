package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.NotificacionesBI;
import com.gruposalinas.migestion.dao.AsesoresDigitalesDAO;
import com.gruposalinas.migestion.domain.CecoComboDTO;
import com.gruposalinas.migestion.domain.InfoAsesoresDigitalesDTO;
import com.gruposalinas.migestion.domain.NotificaAfDTO;
import com.gruposalinas.migestion.domain.TokensAsesoresDTO;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AsesoresDigitalesBI {

    @Autowired
    AsesoresDigitalesDAO asesoresDigitalesDAO;

    private Logger logger = LogManager.getLogger(AsesoresDigitalesBI.class);

    public void cargaInformacion() {

        List<CecoComboDTO> cecosSF = null;

        IndicadoresBI indicadoresBI = new IndicadoresBI();
        JsonObject activaciones = null;
        JsonObject personales = null;

        InfoAsesoresDigitalesDTO infoAsesoresDigitales;

        String fecha = calculaFecha();

        //fecha= "20180722"; /*Fecha prueba*/
        try {
            //Obtiene Cecos de Sector Financiero Mexico
            cecosSF = asesoresDigitalesDAO.getCecos();

            if (cecosSF != null) {
                if (cecosSF.size() > 0) {

                    //Por cada ceco que exista en la lista se ira a obtener la informacion correspondiente en SIE
                    for (CecoComboDTO cecoComboDTO : cecosSF) {
                        String geografia = "";
                        activaciones = null;
                        personales = null;
                        try {
                            geografia = cecoComboDTO.getCeco() + " - " + cecoComboDTO.getNombre();
                            logger.info("Geografia a consultar: " + geografia);

                            infoAsesoresDigitales = new InfoAsesoresDigitalesDTO();

                            activaciones = indicadoresBI.getActivacionesBazDigital(fecha, geografia);
                            personales = indicadoresBI.getColocacionBazDigital(fecha, geografia, "T");

                            if (activaciones != null && personales != null) {
                                if (!activaciones.getAsJsonObject("activacionesBazDigital").isJsonNull()) {

                                    JsonArray totalesActivaciones = activaciones.get("activacionesBazDigital").getAsJsonObject().get("totales").getAsJsonArray();

                                    String cantidadCompromiso = totalesActivaciones.get(2).getAsJsonObject().get("cantidad").getAsString();

                                    infoAsesoresDigitales.setCompromiso_activaciones(cantidadCompromiso);

                                }

                                if (!personales.getAsJsonObject("colocacionBAZDigital").isJsonNull()) {

                                    JsonArray totalesPersonales = personales.get("colocacionBAZDigital").getAsJsonObject().get("totales").getAsJsonArray();

                                    String cantidadCompromiso = totalesPersonales.get(3).getAsJsonObject().get("cantidad").getAsString();

                                    infoAsesoresDigitales.setCompromiso_personales(cantidadCompromiso);
                                }

                                infoAsesoresDigitales.setCeco(cecoComboDTO.getCeco());
                                infoAsesoresDigitales.setFecha(fecha);
                                asesoresDigitalesDAO.insertInfoSIE(infoAsesoresDigitales);

                            }

                        } catch (Exception e) {
                            logger.info("Ap al procesar el ceco " + geografia);
                        }
                    }//Termina for que recorre la lista de Cecos
                }//Fin del IF que valida que la lista sea mayor a 0
            }//Fin del IF que valida que la lista no sea NULL

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void enviaNotifacionesCompromiso() {

        List<TokensAsesoresDTO> tokens = null;

        NotificacionesBI notificacionesBI = new NotificacionesBI();

        String mensaje = "Hola %s, recuerda que esta semana tu compromiso es de:\n"
                + "$%s de colocación de préstamos personales y %s activaciones\n"
                + "Vamos con todo! Buen lunes!";

        try {
            tokens = asesoresDigitalesDAO.getCompromisosAvance();

            if (tokens != null) {

                for (TokensAsesoresDTO token : tokens) {
                    String mensajeNotif = String.format(mensaje, token.getNombreUsuario(), token.getCompromisoPerso(), token.getCompromisoActi());
                    notificacionesBI.enviarNotificacionJSONSinProxy(token.getToken(), "Tu Compromiso Semanal", mensajeNotif);
                }

            }

        } catch (Exception e) {
            logger.info("Ap al enviar las notificaciones");
        }

    }

    public List<TokensAsesoresDTO> consultaTokenAenviar() {

        List<TokensAsesoresDTO> tokens = null;

        try {
            tokens = asesoresDigitalesDAO.getCompromisosAvance();

        } catch (Exception e) {
            logger.info("Ap al enviar las notificaciones");
        }

        return tokens;

    }

    public void enviaNotifacionesAvance() {

        List<TokensAsesoresDTO> tokens = null;

        NotificacionesBI notificacionesBI = new NotificacionesBI();

        String mensaje = "Hola %s hasta el momento llevas:\n"
                + "$%s de colocación  de préstamos personales y  %s activaciones\n"
                + "Tu puedes!\n"
                + "Ánimo, solo te falta:\n"
                + "$%s de colocación  de préstamos personales y  %s activaciones\n"
                + "Si se puede!\n"
                + "";

        try {
            tokens = asesoresDigitalesDAO.getCompromisosAvance();

            if (tokens != null) {

                for (TokensAsesoresDTO token : tokens) {

                    Double compromisoP = Double.parseDouble(token.getCompromisoPerso());
                    Double avanceP = Double.parseDouble(token.getAvancePerso());
                    Double quedanP = avanceP - compromisoP;
                    if (quedanP > 0) {
                        quedanP = 0.0;
                    }

                    Double compromisoA = Double.parseDouble(token.getCompromisoActi());
                    Double avanceA = Double.parseDouble(token.getAvanceActi());
                    Double quedanA = avanceA - compromisoA;
                    if (quedanA > 0) {
                        quedanA = 0.0;
                    }

                    String mensajeNotif = String.format(mensaje,
                            token.getNombreUsuario(),
                            avanceP,
                            avanceA,
                            Math.abs(quedanP),
                            Math.abs(quedanA));
                    notificacionesBI.enviarNotificacionJSONSinProxy(token.getToken(), "Tu Avance Semanal", mensajeNotif);
                }

            }

        } catch (Exception e) {
            logger.info("Ap al enviar las notificaciones");
        }

    }

    public void actulizaAvances() {

        List<NotificaAfDTO> cecosToUpdate = null;

        IndicadoresBI indicadoresBI = new IndicadoresBI();
        JsonObject activaciones = null;
        JsonObject personales = null;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        String fecha = sf.format(cal.getTime());

        try {

            cecosToUpdate = asesoresDigitalesDAO.getCecosToUpdate();

            if (cecosToUpdate != null) {
                if (cecosToUpdate.size() > 0) {

                    for (NotificaAfDTO notificaAfDTO : cecosToUpdate) {
                        String geografia = "";
                        activaciones = null;
                        personales = null;

                        try {
                            geografia = notificaAfDTO.getCeco();
                            logger.info("Geografia a consultar: " + geografia);

                            activaciones = indicadoresBI.getActivacionesBazDigital(fecha, geografia);
                            personales = indicadoresBI.getColocacionBazDigital(fecha, geografia, "T");

                            if (activaciones != null && personales != null) {
                                if (!activaciones.getAsJsonObject("activacionesBazDigital").isJsonNull()) {

                                    JsonArray totalesActivaciones = activaciones.get("activacionesBazDigital").getAsJsonObject().get("totales").getAsJsonArray();

                                    String cantidadCompromiso = totalesActivaciones.get(3).getAsJsonObject().get("cantidad").getAsString();

                                    notificaAfDTO.setAvanceA(cantidadCompromiso);

                                }

                                if (!personales.getAsJsonObject("colocacionBAZDigital").isJsonNull()) {

                                    JsonArray totalesPersonales = personales.get("colocacionBAZDigital").getAsJsonObject().get("totales").getAsJsonArray();

                                    String cantidadCompromiso = totalesPersonales.get(4).getAsJsonObject().get("cantidad").getAsString();

                                    notificaAfDTO.setAvanceP(cantidadCompromiso);
                                }

                                notificaAfDTO.setCeco(notificaAfDTO.getCeco().substring(0, 6));
                                notificaAfDTO.setFecha(fecha);
                                asesoresDigitalesDAO.updateCeco(notificaAfDTO);
                            }

                        } catch (Exception e) {
                            logger.info("Ap al procesar el ceco " + geografia);
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*Metodos que sirven para administrar la tabla GETANOTIFICA_AF*/
    public List<NotificaAfDTO> consulta(String ceco) {

        List<NotificaAfDTO> resultado = null;

        try {
            resultado = asesoresDigitalesDAO.consulta(ceco);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public boolean inserta(NotificaAfDTO objInsertar) {

        boolean respuesta = false;

        try {
            respuesta = asesoresDigitalesDAO.inserta(objInsertar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return respuesta;
    }

    public boolean actualiza(NotificaAfDTO obActualizar) {
        boolean respuesta = false;

        try {
            respuesta = asesoresDigitalesDAO.actualiza(obActualizar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return respuesta;

    }

    public boolean elimina(int idEliminar) {
        boolean respuesta = false;

        try {
            respuesta = asesoresDigitalesDAO.elimina(idEliminar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return respuesta;

    }

    public boolean trunca() {

        boolean respuesta = false;

        try {
            respuesta = asesoresDigitalesDAO.trunca();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return respuesta;
    }

    private String calculaFecha() {

        Calendar cal = Calendar.getInstance();

        int diaSemana = cal.get(Calendar.DAY_OF_WEEK);

        cal.add(Calendar.DAY_OF_YEAR, 15 - diaSemana);

        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");

        String fecha = sf.format(cal.getTime());

        return fecha;

    }

    public static void main(String args[]) throws ParseException {

        int[] celdasExistentes = {1, 2, 3, 4, 6, 9, 10, 11, 38};

        int[] celdasCompletas = new int[40];

        int totalCelda = 40;

        int celdaExistente = 0;
        int celdaNueva = 0;

        int i = 0;
        int contExistenes = 0;

        boolean finCeldas = false;

        while (i < totalCelda) {

            if (!finCeldas) {
                celdaExistente = celdasExistentes[contExistenes];
            } else {
                celdaExistente = 9999999;
            }

            celdaNueva = i;

            if (celdaNueva == celdaExistente) {
                System.out.println("Existe la celda " + i);
                celdasCompletas[i] = celdaExistente;
                i++;
                contExistenes++;
            } else if (celdaNueva < celdaExistente) {
                System.out.println("No existe la celda... se agrega " + i);
                celdasCompletas[i] = i;
                i++;
            }

            if (contExistenes == celdasExistentes.length) {
                finCeldas = true;
            }

        }

        DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        Date date = formatDate.parse("20180722");

        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setMinimalDaysInFirstWeek(4);
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);

        int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
        int numberDayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);

        System.out.println("Fecha Actual: " + calendar.getTime());
        System.out.println("Semana Actual: " + numberWeekOfYear);
        System.out.println("Dia de la semana Actual :" + numberDayOfTheWeek);

        SimpleDateFormat f = new SimpleDateFormat("kmm");
        int fechaNum = Integer.parseInt(calendar.get(Calendar.DAY_OF_WEEK) + f.format(calendar.getTime()));

        System.out.println("Cadena comparar : " + fechaNum);

        // Si es jueves 4:00 pm calcula semana atras
        if (fechaNum > 51600) {
            calendar.add(Calendar.WEEK_OF_YEAR, -1);
        } else {
            calendar.add(Calendar.WEEK_OF_YEAR, -2);
        }

        int dia = 0;
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                dia = 1;
                break;
            case Calendar.TUESDAY:
                dia = 2;
                break;
            case Calendar.WEDNESDAY:
                dia = 3;
                break;
            case Calendar.THURSDAY:
                dia = 4;
                break;
            case Calendar.FRIDAY:
                dia = 5;
                break;
            case Calendar.SATURDAY:
                dia = 6;
                break;
            case Calendar.SUNDAY:
                dia = 7;
                break;
            default:
                break;
        }

        calendar.add(Calendar.DATE, (7 - dia));

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String fechaFormateada = df.format(calendar.getTime());

        System.out.println("Fecha a FINAL " + calendar.getTime());
        System.out.println("Semana calculada: " + calendar.get(Calendar.WEEK_OF_YEAR));
    }
}
