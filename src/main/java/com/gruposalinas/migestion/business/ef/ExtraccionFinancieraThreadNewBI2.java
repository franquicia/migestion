package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonObject;

public class ExtraccionFinancieraThreadNewBI2 extends Thread {

    //ExtraccionFinancieraBI bussines = new ExtraccionFinancieraBI();
    IndicadoresNewBI bussines = new IndicadoresNewBI();

    private int idService;
    private String fecha;
    private String geografia;
    private String nivel;
    private boolean termino = false;
    private JsonObject respuesta;

    public boolean isTermino() {
        return termino;
    }

    public void setTermino(boolean termino) {
        this.termino = termino;
    }

    public JsonObject getRespuesta() {
        return respuesta;
    }

    public ExtraccionFinancieraThreadNewBI2(int idService, String fecha, String geografia, String nivel) {

        this.idService = idService;
        this.fecha = fecha;
        this.geografia = geografia;
        this.nivel = nivel;

    }

    @Override
    public void run() {

        switch (idService) {
            case 6:
                respuesta = bussines.getConsumoColocacion(fecha, geografia, nivel);
                break;
            case 5:
                respuesta = bussines.getPersonalesColocacion(fecha, geografia, nivel);
                break;
            case 7:
                respuesta = bussines.getTAZColocacion(fecha, geografia, nivel);
                break;
            case 9:
                respuesta = bussines.getSaldoCaptacionPlazo(fecha, geografia, nivel);
                break;
            case 8:
                respuesta = bussines.getSaldoCaptacionVista(fecha, geografia, nivel);
                break;
            case 15:
                respuesta = bussines.getNormalidad(fecha, geografia, nivel);
                break;
            case 19:
                respuesta = bussines.getPagosDeServicio(fecha, geografia, nivel);
                break;
            case 18:
                respuesta = bussines.getTransferciasInternacionales(fecha, geografia, nivel);
                break;
            case 17:
                respuesta = bussines.getDineroExpress(fecha, geografia, nivel);
                break;
            case 20:
                respuesta = bussines.getCambios(fecha, geografia, nivel);
                break;
            case 1:
                respuesta = bussines.getContribucion(fecha, geografia, nivel);
                break;
            case 14:
                respuesta = bussines.getNoAbonadasPend(fecha, geografia, nivel);
                break;
            default:
                break;
        }

        termino = true;

        //System.out.println("Servicio " + idService + "termino: "+termino);
    }

}
