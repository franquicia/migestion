package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class IndicadoresNewBI {

    private Logger logger = LogManager.getLogger(IndicadoresNewBI.class);

    public String obtieneXmlResult(String fecha, String geografia, String nivel, String service) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        String strService = service;
        String cadena = "";

        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");

            //			System.setProperty("java.net.useSystemProxies", "true");
            //			System.setProperty("http.proxyHost", "10.50.8.20");
            //			System.setProperty("http.proxyPort", "8080");
            //			// System.setProperty("http.proxyUser", "B196228");
            //			// System.setProperty("http.proxyPassword", "Bancoazteca2345");
            //			System.setProperty("http.proxySet", "true");
            //			System.setProperty("https.proxyHost", "10.50.8.20");
            //			System.setProperty("https.proxyPort", "8080");
            //			// System.setProperty("https.proxyUser", "B196228");
            //			// System.setProperty("https.proxyPassword", "Bancoazteca2345");
            //			System.setProperty("https.proxySet", "true");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<siew:" + strService + ">"
                    + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "<siew:nivel>" + nivel + "</siew:nivel>"
                    + "</siew:" + strService + ">"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            //logger.info("PETICION SOA: "+xml);
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            Element rootNode2 = document.getRootElement();

            Element body2 = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response2 = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result2 = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap para llamado de " + service + " Exception :" + e);
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return cadenaXml;

    }

    public JsonObject getConsumoColocacion(String fecha, String geografia, String nivel) {

        //logger.info("---------CONSUMO "+geografia +"------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "ConsumoColocacion");
            //logger.info(xmlStr);

            //json = clearXml(xmlStr);
            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
            jsonArray = limpiaXMLVersus(xmlStr, flagsColumnas);
            json = jsonDiasSemana(jsonArray);

        } catch (Exception e) {
            logger.info("Ap en ConsumoColocacion() para : " + geografia + " Exception:  " + e);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("consumoColocacion", json);
        }

        logger.info("RESPUESTA DE COLOCACION: " + respuestaService);

        return respuestaService;

    }

    public JsonObject getPersonalesColocacion(String fecha, String geografia, String nivel) {

        //logger.info("---------PERSONALES------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "PersonalesColocacion");
            //logger.info(xmlStr);

            //json = clearXml(xmlStr);
            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
            jsonArray = limpiaXMLVersus(xmlStr, flagsColumnas);
            json = jsonDiasSemana(jsonArray);

        } catch (Exception e) {
            logger.info("Ap en getPersonalesColocacion() para " + geografia + " Exception:  " + e);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("personalesColocacion", json);
        }

        return respuestaService;

    }

    public JsonObject getTAZColocacion(String fecha, String geografia, String nivel) {

        //logger.info("---------TAZ------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {
//
//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TAZColocacion");
            //logger.info(xmlStr);

            //json = clearXml(xmlStr);
            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
            jsonArray = limpiaXMLVersus(xmlStr, flagsColumnas);
            json = jsonDiasSemana(jsonArray);

        } catch (Exception e) {
            logger.info("Ap en getTAZColocacion() para " + geografia + " Exception" + e);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("tazColocacion", json);
        }

        return respuestaService;

    }

    public JsonObject getSaldoCaptacionPlazo(String fecha, String geografia, String nivel) {

        //logger.info("---------CAPTACION PLAZO------------");
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "SaldoCaptacionPlazo");
            //logger.info(xmlStr);

            int[] flagsCaptacionPlazo = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiaXmlCaptacion(xmlStr, flagsCaptacionPlazo);

        } catch (Exception e) {
            logger.info("Ap en getSaldoCaptacionPlazo() para " + geografia + " Exception: " + e);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("saldoCaptacionPlazo", json);
        }

        return respuestaService;

    }

    public JsonObject getSaldoCaptacionVista(String fecha, String geografia, String nivel) {

        //logger.info("---------CAPTACION VISTA------------");
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "SaldoCaptacionVista");
            //logger.info(xmlStr);

            int[] flagsGuardadito = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiaXmlCaptacion(xmlStr, flagsGuardadito);

        } catch (Exception e) {
            logger.info("Ap en getSaldoCaptacionVista() para" + geografia + " Excpetion:  " + e);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("saldoCaptacionVista", json);
        }

        return respuestaService;

    }

    public JsonObject getNormalidad(String fecha, String geografia, String nivel) {

        //logger.info("---------NORMALIDAD------------");
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
            Date date = formatDate.parse(fecha);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 1);

            //logger.info("Fecha para la normalidad : "+calendar.getTime());
            int dia = calendar.get(Calendar.DAY_OF_WEEK);

            if (dia == 1) {
                calendar.add(Calendar.DATE, -7);
            } else {
                calendar.add(Calendar.DATE, -(dia - 1));
            }

            String semana = formatDate.format(calendar.getTime());

            //logger.info("Fecha :" + semana);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(semana, geografia, nivel, "Normalidad");
//			logger.info("PARAMETROS NORMALIDAD :"+semana+" Geografia "+geografia +" Nivel "+nivel );
//			logger.info(xmlStr);

            json = limpiXmlNormalidad(xmlStr);

        } catch (Exception e) {
            logger.info("Ap en getNormalidad() para " + geografia + " Exception: " + e);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("normalidad", json);
        }

        return respuestaService;
    }

    public JsonObject getPagosDeServicio(String fecha, String geografia, String nivel) {

        //logger.info("---------Pago de Servicios------------");
        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {

//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TransferenciasPagoServicios");
            //logger.info(xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap en getPagosDeServicio() para " + geografia + " Exception:  " + e);
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("pagosServicios", json);
        }

        return respuestaService;
    }

    public JsonObject getTransferciasInternacionales(String fecha, String geografia, String nivel) {

        //logger.info("---------Transferencias Internacionales------------");
        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {
//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TransferenciasInternacionales");
//			logger.info("Parametros Transeferencias "+geografia +" "+fecha +" "+nivel);
//			logger.info(xmlStr );

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap en getTransferciasInternacionales() para " + geografia + " Exception:  " + e);
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasInternacionales", json);
        }

        return respuestaService;
    }

    public JsonObject getDineroExpress(String fecha, String geografia, String nivel) {

        //logger.info("---------Dinero Express------------");
        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {
//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TranferenciasDEX");
            //logger.info(xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap en getDineroExpress() para " + geografia + " Exception: " + e);
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasDEX", json);
        }

        return respuestaService;
    }

    public JsonObject getCambios(String fecha, String geografia, String nivel) {

        //logger.info("---------Cambio Divisas------------");
        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {

//			logger.info("Fecha :" + fecha);
//			logger.info("Geografia :" + geografia);
//			logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TransferenciasCambios");
//			logger.info(xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap en getCambios() para " + geografia + " Exception:" + e);

            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasCambios", json);
        }

        return respuestaService;
    }

    public JsonObject getContribucion(String fecha, String geografia, String nivel) {
        //logger.info("---------Contribucion------------");

        JsonObject json = null;

        JsonObject respuestaService = new JsonObject();

        if (!nivel.equals("TR")) {

            if (nivel.equals("T")) {
                nivel = "G";
            }

            try {

                DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
                Date date = formatDate.parse(fecha);

                Calendar calendar = Calendar.getInstance();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.setMinimalDaysInFirstWeek(4);
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);

                int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                int numberDayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);

                //				logger.info("Fecha Actual: "+ calendar.getTime());
                //				logger.info("Semana Actual: "+ numberWeekOfYear);
                //				logger.info("Dia de la semana Actual :" + numberDayOfTheWeek);
                SimpleDateFormat f = new SimpleDateFormat("kmm");
                int fechaNum = Integer.parseInt(calendar.get(Calendar.DAY_OF_WEEK) + f.format(calendar.getTime()));

                //logger.info("Cadena comparar : "+fechaNum);
                // Si es jueves 4:00 pm calcula semana atras
                if (fechaNum > 51600) {
                    calendar.add(Calendar.WEEK_OF_YEAR, -1);
                } else {
                    calendar.add(Calendar.WEEK_OF_YEAR, -2);
                }

                int dia = 0;
                switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                    case Calendar.MONDAY:
                        dia = 1;
                        break;
                    case Calendar.TUESDAY:
                        dia = 2;
                        break;
                    case Calendar.WEDNESDAY:
                        dia = 3;
                        break;
                    case Calendar.THURSDAY:
                        dia = 4;
                        break;
                    case Calendar.FRIDAY:
                        dia = 5;
                        break;
                    case Calendar.SATURDAY:
                        dia = 6;
                        break;
                    case Calendar.SUNDAY:
                        dia = 7;
                        break;
                    default:
                        break;
                }

                calendar.add(Calendar.DATE, (7 - dia));

                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                String fechaFormateada = df.format(calendar.getTime());

                //				logger.info("Fecha a FINAL "+calendar.getTime());
                //				logger.info("Semana calculada: "+calendar.get(Calendar.WEEK_OF_YEAR));
                //
//								logger.info("Fecha :" + fechaFormateada);
//								logger.info("Geografia :" + geografia);
//								logger.info("Nivel :" + nivel);
                String xmlStr = obtieneXmlResult(fechaFormateada, geografia, nivel, "Contribucion");
                //logger.info(xmlStr);

                json = armaJsonContribucion(xmlStr);
                json.addProperty("semana", "" + calendar.get(Calendar.WEEK_OF_YEAR));

            } catch (Exception e) {
                logger.info("Ap en getContribucion() para " + geografia + "Exception: " + e);
                json = null;
            }

        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("contribucion", json);
        }

        return respuestaService;

    }

    public JsonObject getNoAbonadasPend(String fecha, String geografia, String nivel) {
        //logger.info("---------No Abonadas Pendientes por Surtir------------");

        Map<String, Object> jsonMap = null;

        JsonObject respuestaService = new JsonObject();

        if (!nivel.equals("TR")) {

            if (nivel.equals("T")) {
                nivel = "G";
            }

            try {

                DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
                Date date = formatDate.parse(fecha);

                Calendar calendar = Calendar.getInstance();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.setMinimalDaysInFirstWeek(4);
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);

                int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                int numberDayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);

                //				logger.info("Fecha Actual: "+ calendar.getTime());
                //				logger.info("Semana Actual: "+ numberWeekOfYear);
                //				logger.info("Dia de la semana Actual :" + numberDayOfTheWeek);
                SimpleDateFormat f = new SimpleDateFormat("kmm");
                int fechaNum = Integer.parseInt(calendar.get(Calendar.DAY_OF_WEEK) + f.format(calendar.getTime()));

                //				logger.info("Cadena comparar : "+fechaNum);
                // Si es jueves 4:00 pm calcula semana atras
                if (fechaNum > 51600) {
                    calendar.add(Calendar.WEEK_OF_YEAR, -1);
                } else {
                    calendar.add(Calendar.WEEK_OF_YEAR, -2);
                }

                int dia = 0;
                switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                    case Calendar.MONDAY:
                        dia = 1;
                        break;
                    case Calendar.TUESDAY:
                        dia = 2;
                        break;
                    case Calendar.WEDNESDAY:
                        dia = 3;
                        break;
                    case Calendar.THURSDAY:
                        dia = 4;
                        break;
                    case Calendar.FRIDAY:
                        dia = 5;
                        break;
                    case Calendar.SATURDAY:
                        dia = 6;
                        break;
                    case Calendar.SUNDAY:
                        dia = 7;
                        break;
                    default:
                        break;
                }

                calendar.add(Calendar.DATE, (7 - dia));

                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                String fechaFormateada = df.format(calendar.getTime());

                String xmlStr = obtieneXmlResult(fechaFormateada, geografia, nivel, "NAbonadasPendSurt");
                //				logger.info(xmlStr);

                jsonMap = armaJsonNoAbonadas(xmlStr);

            } catch (Exception e) {
                logger.info("Ap en getNoAbonadasPend() para " + geografia + " Exception: " + e);
                jsonMap = null;
            }

        }

        if (jsonMap == null) {
            respuestaService = null;
        } else {
            respuestaService.add("nuncaAbonadas", (JsonObject) jsonMap.get("nuncaAbonadas"));
            respuestaService.add("pendientesSurtrir", (JsonObject) jsonMap.get("pendientesSurtrir"));
        }

        return respuestaService;

    }

    public Map<String, Object> armaJsonNoAbonadas(String xmlStr) {

        Map<String, Object> mapJson = new HashMap<String, Object>();
        JsonObject nuncaAbonadas = new JsonObject();
        JsonObject pendientesSurtir = new JsonObject();

        try {
            /* Convierte String a XML */

            StringReader str = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(str);
            Element rootNode = document.getRootElement();

            Element data = rootNode.getChild("data");
            Element renglones = data.getChild("renglones");
            Element renglon = renglones.getChild("renglon");
            Element celdas = renglon.getChild("celdas");
            List<Element> listCeldas = celdas.getChildren("celda");

            for (Element element : listCeldas) {

                int celda = Integer.parseInt(element.getAttributeValue("pos"));
                String valor = element.getChild("caption").getValue();
                switch (celda) {
                    case 1:
                        break;
                    case 2:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 3:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 4:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 5:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 6:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 7:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                }

            }

            mapJson.put("pendientesSurtrir", pendientesSurtir);
            mapJson.put("nuncaAbonadas", nuncaAbonadas);

        } catch (Exception e) {
            logger.info("Ap en armaJsonNoAbonadas() " + e);
            return null;
        }

        //		logger.info("JSON SURTIDAS :"+pendientesSurtir.toString());
        //		logger.info("JSON NUNCA ABONADAS :"+nuncaAbonadas.toString());
        return mapJson;
    }

    public JsonObject armaJsonContribucion(String xmlStr) {

        JsonObject contribucion = new JsonObject();
        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            Element consulta = rootNode.getChild("Consulta");
            Element table = consulta.getChild("Table");

            DecimalFormat df = new DecimalFormat("###,###,###");

            contribucion.addProperty("posicion", table.getChild("POSICION").getValue());
            contribucion.addProperty("geografia", table.getChild("GEOGRAFIA").getValue());
            contribucion.addProperty("semana1", df.format(Double.parseDouble(table.getChild("SEM_1").getValue())));
            contribucion.addProperty("semana2", df.format(Double.parseDouble(table.getChild("SEM_2").getValue())));
            contribucion.addProperty("crecimiento", table.getChild("CRECIMIENTO").getValue());
            contribucion.addProperty("total", table.getChild("TOTAL").getValue());
            contribucion.addProperty("totalReg", table.getChild("T_REG").getValue());

            int posicion = Integer.parseInt(contribucion.get("posicion").getAsString());
            int total_reg = Integer.parseInt(contribucion.get("totalReg").getAsString());
            String imagen = "";

            int res = (int) ((posicion * 100) / total_reg);

            //			logger.info("Posicion "+posicion);
            //			logger.info("total_reg "+total_reg);
            //			logger.info("res " + (int)res);
            if (res >= 0 && res <= 20) {
                imagen = "oro";
            } else if (res > 20 && res <= 50) {
                imagen = "plata";
            } else if (res > 50 && res <= 80) {
                imagen = "bronce";
            } else if (res > 80 && res <= 100) {
                imagen = "carbon";
            }

            contribucion.addProperty("imagen", imagen);

        } catch (Exception e) {
            logger.info("Ap en armaJsonContribucion() " + e);
            return null;
        }

        return contribucion;
    }

    protected JsonArray limpiXmlNew(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            //			logger.info("Axes: "+axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            //logger.info("Columnas :"+columnas);
            //logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            //logger.info("Total GRID: "+ totalCeldas);
            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
                    //					    logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                    //						logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue());
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
                    //						logger.info("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                //logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //logger.info(arrayFilas.toString());
            //logger.info("Valores celda lista : " +listValoresCeldas.size());
            //logger.info("Valores totales lista : " +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            jsonArray = armaJsonNew(valores, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap en limpiXmlNew()" + e);
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonNew(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 4) {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                    //logger.info("cantidad: "+ valoresCeldas.get(i));
                    //logger.info("Cantidad vs"+ valoresCeldas.get( i - pos_vs ));
                    pos_vs += 3;
                } else {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

            }

            valor = new JsonObject();
            valor.addProperty("cantidad", valorStr);
            valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
            valor.addProperty("color", color);

            arrayValores.add(valor);

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                //				logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /*Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 4) {
                    porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                    pos_vsTot += 3;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

            }

            jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
            jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
            jsonTotales.addProperty("color", color);
            arrayValores.add(jsonTotales);

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonObject clearXml(String xlmStr) {

        JsonObject respuesta = new JsonObject();

        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xlmStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            List<Element> allChildren = rootNode.getChildren();


            /*ï¿½NORMAL*/
            String fmtValue = "";
            boolean flagAdd = false;
            String celda = "";

            ArrayList<String> listValores = new ArrayList<String>();

            for (Element element : allChildren) {
                if (element.getName().equals("CellData")) {

                    List<Element> listaDatos = element.getChildren();// Lista
                    // que
                    // guarda
                    // todas
                    // las
                    // etiquets
                    // "Cell"
                    for (Element elementChild : listaDatos) {
                        fmtValue = "";
                        flagAdd = false;
                        celda = elementChild.getAttributeValue("CellOrdinal");
                        List<Element> elementDatos = elementChild.getChildren();// Lista
                        // que
                        // guarda
                        // la
                        // etiquetas
                        // dentro
                        // de
                        // cada
                        // etiqueta
                        // "Cell"
                        for (Element elementDato : elementDatos) {
                            if (elementDato.getName().equals("FmtValue")) {
                                fmtValue = elementDato.getValue();
                            } else if (elementDato.getName().equals("Value")) {
                                List<Attribute> atributos = elementDato.getAttributes();
                                for (Attribute elementAtributo : atributos) { // Revisa
                                    // Atributos
                                    // de
                                    // la
                                    // celda
                                    if (!elementAtributo.getValue().equals("xsd:short")) {
                                        flagAdd = true;
                                    }
                                }
                            }

                        }

                        if (flagAdd) {
                            listValores.add(fmtValue);
                        }

                    }

                }
            }

            respuesta = createResult(listValores);

        } catch (Exception e) {

            respuesta = null;
        }

        return respuesta;

    }

    @SuppressWarnings("unused")
    protected JsonObject createResult(ArrayList<String> valores) {

        DecimalFormat formateador = new DecimalFormat("###,###.#");
        double[] totales = new double[9];
        int nuInteraciones = 8;

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        int indice = valores.size() - 1;

        try {

            /* Obtiene totales */
            while (nuInteraciones >= 0) {
                double valor = Double.parseDouble(valores.get(indice)) / 1000;
                totales[nuInteraciones] = valor;
                // totales[nuInteraciones] = (int)Math.round(valor);
                indice--;
                nuInteraciones--;
            }

            // int positionTotales = 0;
            int veces = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i <= indice; i++) {

                veces++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "";

                if (veces > 5) {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = Double.parseDouble(valores.get(i - pos_vs)) / 1000;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt= (int)Math.round(valor);
                    porcentaje = (valor / totales[veces - 1]) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", "" + (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces == 9) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces = 0;
                    pos_vs = 2;
                }

            }

            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.length; a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 4) {
                    porcentajeTot = (double) totales[a] / (double) totales[a - pos_vs] * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales[a]));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);
            //			logger.info(jsonDias.toString());

        } catch (Exception e) {
            logger.info("Ap " + e);
            jsonDias = null;
        }

        return jsonDias;

    }

    /*Limpia respuesta xml de los rubros Captacion*/
    @SuppressWarnings("unchecked")
    protected JsonObject limpiaXmlCaptacion(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();


            /*-----------------------------------------------Eliminar por pruebas--------------------------------------------------------------------*/
            //			logger.info("Celldata"+rootNode.getChild("CellData",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                //logger.info("Celda " +element.getAttributeValue("CellOrdinal"));
                numCeldas.add(Integer.parseInt(element.getAttributeValue("CellOrdinal")));

                String valor = element.getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonCaptacion(valoresCeldas, flagsColumnas, numCeldas);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        return jsonRepuestas;

    }

    /*Arma Json de los rubros Captacion*/
    @SuppressWarnings("unused")
    protected JsonObject armaJsonCaptacion(List<String> valoresCeldas, int[] flagsColumnas, ArrayList<Integer> numCeldas) {

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        try {
            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###.#");
            int columnas = flagsColumnas.length;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - columnas);
            int indiceColumnas = 0;

            while (indice < valoresCeldas.size()) {

                if (flagsColumnas[indiceColumnas] == 1) {
                    totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                }

                indiceColumnas++;
                indice++;
            }

            /*Obtener Filas*/
            int cont = 0;
            int veces = 1;
            int corte = 1;

            ArrayList<Double> valores = new ArrayList<Double>();

            while (cont < numCeldas.size() && cont != (valoresCeldas.size() - columnas)) {

                JsonObject valorPorcentaje = null;

                if (veces != numCeldas.get(cont)) {
                    break;
                }

                if (flagsColumnas[corte - 1] == 1) {
                    valores.add(Double.parseDouble(valoresCeldas.get(cont)));
                    //logger.info("$"+formateador.format(Double.parseDouble(valoresCeldas.get(cont))/1000.0));
                    //logger.info("$"+Double.parseDouble(valoresCeldas.get(cont))/1000);
                }

                if (corte == flagsColumnas.length) {
                    //logger.info("Agrega Fila");
                    corte = 1;
                    veces += 2;
                } else {
                    corte++;
                    veces++;
                }

                cont++;

            }

            // int positionTotales = 0;
            int veces1 = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i < valores.size(); i++) {

                veces1++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "verde";

                if (veces1 > 4) {
                    valor = valores.get(i) / 1000000.0;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = valores.get(i - pos_vs) / 1000.0;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = valores.get(i) / 1000;
                }

                if (porcentaje >= 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces1 == 7) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces1 = 0;
                    pos_vs = 2;
                }
            }
            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.size(); a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 3) {
                    porcentajeTot = (double) totales.get(a) / (double) totales.get(a - pos_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);

        } catch (Exception e) {
            logger.info("Ap " + e);
            jsonDias = null;
        }

        return jsonDias;
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected JsonObject limpiXmlNormalidad(String xmlStr) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            //logger.info("Celldata"+rootNode.getChild("CellData",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                //logger.info("Celda " +element.getAttributeValue("CellOrdinal"));

                String valor = element.getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonNormalidad(valoresCeldas);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        return jsonRepuestas;

    }

    @SuppressWarnings("unused")
    protected JsonObject armaJsonNormalidad(List<String> valoresCeldas) {

        String arrayNombres[] = {
            "Vigente De 0 A 1",
            "Temprana 2",
            "Atrasada 1a De 3 A 7",
            "Tardia 1b De 8 A 13",
            "Atrasada 2 A 13",
            "Vencida 2a De 14 A 25",
            "Dificil 2b De 26 A 39",
            "Vencida De 14 A 39",
            "Saldo Cartera De 0 A 39 Semanas",
            "Legal 40 a 55",
            "Cazadores 56 +",
            "Saldo De Cartera"};

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonFilas = new JsonObject();

        try {

            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###");

            DecimalFormat formateadorDecimales = new DecimalFormat("###.##");

            int columnas = 5;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - (columnas * 4));
            int indiceColumnas = 0;

            //			logger.info("Inicio :" +indice);
            while (indice < valoresCeldas.size() - (columnas * 3)) {

                //logger.info(formateador.format(Double.parseDouble(valoresCeldas.get(indice))/1000.0));
                totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                indiceColumnas++;
                indice++;
            }

            int fin = (valoresCeldas.size() - (columnas * 4));
            int veces = 0;

            JsonObject valorPorcentaje = null;

            int totalFilas = 0;
            for (int i = 0; i < valoresCeldas.size(); i++) {

                veces++;
                valorPorcentaje = new JsonObject();

                double porcentaje = 0.0;
                double valor = 0.0;

                int valorInt = 0;
                String color = "verde";

                valor = Double.parseDouble(valoresCeldas.get(i)) / 1000;
                //logger.info("Valor "+ formateador.format(valor));
                //logger.info("Total "+ formateador.format(totales.get(veces-1)));

                if (veces > 3) {
                    porcentaje = 0.0;
                } else {
                    porcentaje = (valor / (totales.get(veces - 1))) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                //logger.info("Porcentaje "+(int)Math.round(porcentaje)+"%");
                valorPorcentaje.addProperty("cantidad", formateador.format(valor));
                //valorPorcentaje.addProperty("porcentaje",  formateadorDecimales.format(porcentaje) +"%");
                valorPorcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorPorcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorPorcentaje);

                if (veces == 5) {

                    jsonFilas.add(arrayNombres[totalFilas], arrayValorPorcentaje);
                    arrayValorPorcentaje = new JsonArray();

                    veces = 0;

                    totalFilas++;
                }

            }
        } catch (Exception e) {
            jsonFilas = null;
        }

        //logger.info(jsonFilas.toString());
        return jsonFilas;

    }

    protected String getdia(int nuDia) {
        String dia = "";
        if (nuDia == 1) {
            dia = "lunes";
        } else if (nuDia == 2) {
            dia = "martes";
        } else if (nuDia == 3) {
            dia = "miercoles";
        } else if (nuDia == 4) {
            dia = "jueves";
        } else if (nuDia == 5) {
            dia = "viernes";
        } else if (nuDia == 6) {
            dia = "sabado";
        } else if (nuDia == 7) {
            dia = "domingo";
        } else if (nuDia == 8) {
            dia = "totales";
        }
        return dia;

    }

    public static String getProperty(int valor) {

        String propiedad = "";
        switch (valor) {
            case 1:
                propiedad = "cabeceras";
                break;
            case 2:
                propiedad = "noAbonadas";
                break;
            case 3:
                propiedad = "totalCuentas";
                break;
            case 4:
                propiedad = "pendientesSurtir";
                break;
            case 5:
                propiedad = "autorizadas";
                break;
            case 6:
                propiedad = "clientesNuevos";
                break;
            case 7:
                propiedad = "creditosOtorgados";
                break;
            case 8:
                propiedad = "planClientes";
                break;
            case 9:
                propiedad = "level";
                break;

            default:
                propiedad = "";
                break;
        }

        return propiedad;

    }

    public JsonObject jsonDiasSemana(JsonArray array) {

        JsonObject respuesta = new JsonObject();

        for (int i = 1; i <= array.size(); i++) {
            respuesta.add(getdia(i), array.get(i - 1).getAsJsonArray());
        }

        return respuesta;

    }

    protected JsonArray limpiaXMLVersus(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            //logger.info("Columnas :"+columnas);
            //logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;

            int columnasdia = 0;

            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
                    //					    logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                    //						logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()) / 1000;
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
                    logger.info("------------FILA-------------" + numFilas);
                    int posicionReal;
                    //Agrega los versus
                    if (numFilas == 1 || numFilas == rowsCount) {
                        posicionReal = 5;
                    } else {
                        posicionReal = listValoresCeldas.size() - 2;
                    }

                    double valorReal;

                    if (numFilas == rowsCount) {
                        valorReal = listaValoresTotales.get(posicionReal);
                    } else {
                        valorReal = listValoresCeldas.get(posicionReal);
                    }

                    for (int i = 1; i <= 4; i++) {
                        double valorVs;

                        if (numFilas == rowsCount) {
                            valorVs = listaValoresTotales.get(posicionReal - i);
                        } else {
                            valorVs = listValoresCeldas.get(posicionReal - i);
                        }

                        //double valorCalculado = valorVs  - valorReal;
                        double valorCalculado = valorReal - valorVs;

                        logger.info("Valor real " + valorReal);
                        logger.info("Valor vs: " + valorVs);
                        logger.info("Valor Calculado: " + valorVs);

                        if (numFilas == rowsCount) {
                            listaValoresTotales.add(valorCalculado);
                        } else {
                            listValoresCeldas.add(valorCalculado);
                        }

                    }
                    veces = 0;
                    numFilas++;

                }
                //logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //logger.info(arrayFilas.toString());
            //logger.info("Valores celda lista : " +listValoresCeldas.size());
            //logger.info("Valores totales lista : " +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", 11);

            int[] flags = {0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1};
            jsonArray = armaJsonVersus(valores, flags);

        } catch (Exception e) {
            logger.info("Ap " + e.getMessage());
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonVersus(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###.#");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 3;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 7) {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                    logger.info("cantidad: " + valoresCeldas.get(i));
                    logger.info("Cantidad vs" + valoresCeldas.get(i - pos_vs));
                    // ));
                    pos_vs += 2;
                } else {
                    logger.info("cantidad: " + valoresCeldas.get(i));
                    logger.info("Cantidad vs" + valoresTotales.get(veces - 1));
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

                valor = new JsonObject();
                valor.addProperty("cantidad", valorStr);
                valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valor.addProperty("color", color);

                arrayValores.add(valor);
            }

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 3;
            }

        }

        /* Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 3;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 7) {
                    logger.info("Valor total:" + valoresTotales.get(a));
                    logger.info("Valor contra : " + valoresTotales.get(a - pos_vsTot));
                    porcentajeTot = ((double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot)) * 100.0;
                    pos_vsTot += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayValores.add(jsonTotales);

            }

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

}
