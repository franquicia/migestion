package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.GeografiaBI;
import com.gruposalinas.migestion.business.ParametroBI;
import com.gruposalinas.migestion.domain.GeografiaDTO;
import com.gruposalinas.migestion.domain.ParametroDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class TiempoRealGraficaBI {

    private final Logger logger = LogManager.getLogger(TiempoRealGraficaBI.class);

    private String ceco;
    private int producto;
    private String geografia;

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String geografia) {
        this.ceco = geografia;
    }

    public int getProducto() {
        return producto;
    }

    public void setProducto(int producto) {
        this.producto = producto;
    }

    public String getData() {

        String jsonString = "";

        /*this.fecha = "20180802";
         this.nivel = "TR";
         this.geografia = "236737 - TERRITORIAL CENTRO";
         this.idIndicador = 5;*/
        geografia = obtieneGeografia(ceco);

        String xmlRespuesta = obtieneXmlResult();

        String respuesta = getJsonPorhora(xmlRespuesta);

        //System.out.println("{\"dataGraficas\":"+respuesta+"}");
        return "{\"dataGrafica\":" + respuesta + "}";

    }

    public String obtieneXmlResult() {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        //Nombre del metodo a invocar en el WEB Service
        String strService = "GraficaTiempoRealColocacion";

        String cadena = "";

        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");

            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<siew:" + strService + ">"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "<siew:producto>" + producto + "</siew:producto>"
                    + "</siew:" + strService + ">"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            //logger.info("PETICION SOA: " + xml);
            //System.out.println("PETICION SOA: " + xml);
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap al obtener el xml del servicio. Metodo obtieneXmlResult() " + e.getMessage());
            cadenaXml = null;
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return cadenaXml;
    }

    private String getJsonPorhora(String xml) {

        JsonArray jsonRespuesta = new JsonArray();
        String respuesta = "";

        try {

            StringReader stringReader = new StringReader(xml);
            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = builder.build(stringReader);

            //Se obtiene la raiz
            Element rootNode = document.getRootElement();
            Element consulta = rootNode.getChild("Consulta");
            List<Element> table = consulta.getChildren("Table");

            JsonObject hora = null;
            for (int i = 0; i < table.size(); i++) {

                Element elemento = table.get(i);

                hora = new JsonObject();
                hora.addProperty("hora", Integer.parseInt(elemento.getChild("HORA").getValue()));
                hora.addProperty("objetivoPorHora", elemento.getChild("OBJETIVO") != null ? Integer.parseInt(elemento.getChild("OBJETIVO").getValue()) : 0);
                hora.addProperty("colocadoPorHora", elemento.getChild("NIMPORTEVTA") != null ? Integer.parseInt(elemento.getChild("NIMPORTEVTA").getValue()) : 0);
                hora.addProperty("colocadoAnioAnt", elemento.getChild("NIMPORTEVTA2") != null ? Integer.parseInt(elemento.getChild("NIMPORTEVTA2").getValue()) : 0);
                hora.addProperty("acumuladoDia", elemento.getChild("NACUMONTO") != null ? Integer.parseInt(elemento.getChild("NACUMONTO").getValue()) : 0);
                hora.addProperty("acumuladoDiaAnioAnt", elemento.getChild("NACUMONTO2") != null ? Integer.parseInt(elemento.getChild("NACUMONTO2").getValue()) : 0);

                jsonRespuesta.add(hora);

            }

            String array = jsonRespuesta.toString();
            //System.out.println("Json: "+array);

        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }

        return jsonRespuesta.toString();
    }

    public String obtieneGeografia(String ceco) {

        GeografiaBI geografiaBi2 = (GeografiaBI) GTNAppContextProvider.getApplicationContext().getBean("GeografiaBI");

        boolean territorio = false;
        boolean zona = false;
        boolean region = false;
        String geografia = "11046";

        List<GeografiaDTO> listaGeografia;

        /*GeografiaDTO geografiaDTO = new GeografiaDTO();
         geografiaDTO.setIdRegion(236399);
         geografiaDTO.setIdZona(232822);
         geografiaDTO.setIdTerritorio(236737);
         geografiaDTO.setIdCeco(ceco);
         listaGeografia = new ArrayList<GeografiaDTO>();
         listaGeografia.add(geografiaDTO);*/
        try {

            ParametroBI parametro = (ParametroBI) GTNAppContextProvider.getApplicationContext().getBean("parametroBI");

            List<ParametroDTO> parametros = parametro.obtieneParametros("PREFIJO_REAL");
            geografia = parametros.get(0).getValor();

            listaGeografia = geografiaBi2.obtieneGeografia(ceco, "", "", "");

            if (listaGeografia != null) {
                if (listaGeografia.size() > 0) {

                    if (listaGeografia.get(0).getIdTerritorio() != 0) {
                        territorio = true;
                        geografia += "." + listaGeografia.get(0).getIdTerritorio();
                    }

                    if (listaGeografia.get(0).getIdZona() != 0) {
                        zona = true;
                        geografia += "." + listaGeografia.get(0).getIdZona();
                    }

                    if (listaGeografia.get(0).getIdRegion() != 0) {
                        region = true;
                        geografia += "." + listaGeografia.get(0).getIdRegion();
                    }

                    if ((Integer.parseInt(ceco) != listaGeografia.get(0).getIdTerritorio()) && (Integer.parseInt(ceco) != listaGeografia.get(0).getIdZona()) && (Integer.parseInt(ceco) != listaGeografia.get(0).getIdRegion())) {
                        geografia += "." + ceco.substring(2);
                    }

                }
            }

        } catch (Exception e) {
            logger.info("Ap en getGeografia() ");
            geografia = null;
        }
        return geografia;
    }

}
