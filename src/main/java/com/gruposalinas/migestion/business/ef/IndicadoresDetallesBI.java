package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IndicadoresDetallesBI extends Thread {

    private Logger logger = LogManager.getLogger(IndicadoresDetallesBI.class);

    private int procesoClase = 0;
    private String cecoClase = "";
    private int nivelHijo = 0;
    private String nivelClase = "";
    private String fechaClase = "";
    private JsonObject respuestaHilo = null;
    private JsonArray arrayHijos = null;
    private String negocioClase = "";

    public IndicadoresDetallesBI() {

    }

    public IndicadoresDetallesBI(int proceso, String fecha, String geografia, int nivel, String negocio) {

        negocioClase = "96";//negocio;  Se cambio por el negocio de CyC
        fechaClase = fecha;
        procesoClase = proceso;
        cecoClase = geografia;
        nivelHijo = nivel + 1;

        switch (nivel) {
            case 1:
                nivelClase = "TR";
                break;
            case 2:
                nivelClase = "Z";
                break;
            case 3:
                nivelClase = "R";
                break;
            case 4:
                nivelClase = "T";
                break;
        }

    }

    @Override
    public void run() {

        if (procesoClase == 1) {
            getIndicadores(fechaClase, cecoClase, nivelClase);
        } else {
            arrayHijos = getIndicadoresHijos(cecoClase.substring(0, 6));
        }

    }

    public List<CecoDTO> obtieneHijos(String ceco) {

        List<CecoDTO> listaCecos = new ArrayList<>();
//
//		Class<?> c1;
//		try {
//			Object object1 = GTNAppContextProvider.getApplicationContext().getBean("cecoBI");
//			c1 = object1.getClass();
//			Method method1 = c1.getDeclaredMethod("buscaCecosSuperior", new Class[]{String.class, String.class});
//			listaCecos =   (List<CecoDTO>) method1.invoke(object1, ceco,"82" );
//		} catch (Exception e) {
//
//		}

//		System.out.println("Negocio Clase :" +negocioClase);
        CecoBI cecoBi = (CecoBI) GTNAppContextProvider.getApplicationContext().getBean("cecoBI");
        listaCecos = cecoBi.buscaCecosSuperior(ceco, negocioClase);
        System.out.println("****** TAMA�O LISTA *****:" + listaCecos.size());

//		CecoDTO ceco1 = new CecoDTO();
//		ceco1.setIdCeco("480100");
//		ceco1.setDescCeco("MEGA DF LA LUNA");
//		ceco1.setIdNivel(nivelHijo);
//		CecoDTO ceco2 = new CecoDTO();
//		ceco2.setIdCeco("484624");
//		ceco2.setDescCeco("MEGA VILLA OLIMPICA");
//		ceco2.setIdNivel(nivelHijo);
//
//		CecoDTO ceco3 = new CecoDTO();
//		ceco3.setIdCeco("482325");
//		ceco3.setDescCeco("EKT AJUSCO");
//		ceco3.setIdNivel(nivelHijo);
//		listaCecos.add(ceco1);
//		listaCecos.add(ceco2);
//		listaCecos.add(ceco3);
        return listaCecos;
    }

    /*
     * Metodo que obtiene todos los indicadores de un Ceco en especifico
     *
     * */
    public void getIndicadores(String fecha, String geografia, String nivel) {

        JsonObject jsonGeneral = new JsonObject();
        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();

        ExtraccionFinancieraThreadNewBI consumo = new ExtraccionFinancieraThreadNewBI(1, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI personales = new ExtraccionFinancieraThreadNewBI(2, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI taz = new ExtraccionFinancieraThreadNewBI(3, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI captacionPlazo = new ExtraccionFinancieraThreadNewBI(4, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI captacionVista = new ExtraccionFinancieraThreadNewBI(5, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI normalidad = new ExtraccionFinancieraThreadNewBI(6, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI pagosServicios = new ExtraccionFinancieraThreadNewBI(7, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI transferenciasInternacionales = new ExtraccionFinancieraThreadNewBI(8, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI dineroExpress = new ExtraccionFinancieraThreadNewBI(9, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI cambios = new ExtraccionFinancieraThreadNewBI(10, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI contribucion = new ExtraccionFinancieraThreadNewBI(11, fecha, geografia, nivel);
        ExtraccionFinancieraThreadNewBI noAbonadasPendientes = new ExtraccionFinancieraThreadNewBI(12, fecha, geografia, nivel);

        try {
            long inicio = System.currentTimeMillis();
//			System.out.println("INICIA EJECUCION DE HILOS DE LA GEOGRAFIA  "+ geografia+" HORA :" +new Date());

            consumo.start();
            personales.start();
            taz.start();
            captacionPlazo.start();
            captacionVista.start();
            normalidad.start();
            pagosServicios.start();
            transferenciasInternacionales.start();
            dineroExpress.start();
            cambios.start();
            contribucion.start();
            noAbonadasPendientes.start();

            //Se ejecuta while para saber cuando
            while (consumo.isAlive()
                    || personales.isAlive()
                    || taz.isAlive()
                    || captacionPlazo.isAlive()
                    || captacionVista.isAlive()
                    || normalidad.isAlive()
                    || pagosServicios.isAlive()
                    || transferenciasInternacionales.isAlive()
                    || dineroExpress.isAlive()
                    || cambios.isAlive()
                    || contribucion.isAlive()
                    || noAbonadasPendientes.isAlive()) {

                /*Espera hasta que los hilos hayan terminado*/
            }

            long acabo = System.currentTimeMillis();
            long totaltiempo = (acabo - inicio) / 1000;

            System.out.println("HORA EN LA QUE TERMINA LOS HILOS " + acabo);
//			System.out.println("TERMINARON TODOS LOS HILOS EN " + totaltiempo +" SEGUNDOS DE LA GEOGRAFIA "+ geografia);

            if (consumo.getRespuesta() != null) {

                JsonArray totales = consumo.getRespuesta().get("consumoColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Consumo");
                total.addProperty("idService", "6");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", "6");
                jsonObject.addProperty("codigo", "cons");
                jsonObject.add("detalle", consumo.getRespuesta().get("consumoColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (personales.getRespuesta() != null) {

                JsonArray totales = personales.getRespuesta().get("personalesColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Personales");
                total.addProperty("idService", "5");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Personales");
                jsonObject.addProperty("idService", "5");
                jsonObject.add("detalle", personales.getRespuesta().get("personalesColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (taz.getRespuesta() != null) {
                JsonArray totales = taz.getRespuesta().get("tazColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                //total.addProperty("idService", "TAZ");
                total.addProperty("idService", "7");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "TAZ");
                jsonObject.addProperty("idService", "7");
                jsonObject.addProperty("codigo", "taz");
                jsonObject.add("detalle", taz.getRespuesta().get("tazColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (captacionPlazo.getRespuesta() != null) {

                JsonArray totales = captacionPlazo.getRespuesta().get("saldoCaptacionPlazo").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Saldo Captacion Plazo");
                total.addProperty("idService", "9");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Saldo Captacion Plazo");
                jsonObject.addProperty("idService", "9");

                jsonObject.add("detalle", captacionPlazo.getRespuesta().get("saldoCaptacionPlazo").getAsJsonObject());

                detalles.add(jsonObject);

            }
            if (captacionVista.getRespuesta() != null) {

                JsonArray totales = captacionVista.getRespuesta().get("saldoCaptacionVista").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Saldo Captacion Vista");
                total.addProperty("idService", "8");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Saldo Captacion Vista");
                jsonObject.addProperty("idService", "8");

                jsonObject.add("detalle", captacionVista.getRespuesta().get("saldoCaptacionVista").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (normalidad.getRespuesta() != null) {

                JsonArray totales = normalidad.getRespuesta().get("normalidad").getAsJsonObject().get("Vigente De 0 A 1")
                        .getAsJsonArray();
                String indicador = totales.get(2).getAsJsonObject().get("porcentaje").getAsString();

                String color = totales.get(0).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Normalidad");
                total.addProperty("idService", "15");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Normalidad");
                jsonObject.addProperty("idService", "15");

                jsonObject.add("detalle", normalidad.getRespuesta().get("normalidad").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (pagosServicios.getRespuesta() != null) {

                JsonArray totales = pagosServicios.getRespuesta().get("pagosServicios").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Pago de Servicios");
                total.addProperty("idService", "19");
                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Pago de Servicios");
                jsonObject.addProperty("idService", "19");

                jsonObject.add("detalle", pagosServicios.getRespuesta().get("pagosServicios").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (transferenciasInternacionales.getRespuesta() != null) {

                JsonArray totales = transferenciasInternacionales.getRespuesta().get("transferenciasInternacionales").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Transferencias Internacionales");
                total.addProperty("idService", "18");

                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Transferencias Internacionales");
                jsonObject.addProperty("idService", "18");
                jsonObject.add("detalle", transferenciasInternacionales.getRespuesta().get("transferenciasInternacionales").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (dineroExpress.getRespuesta() != null) {

                JsonArray totales = dineroExpress.getRespuesta().get("transferenciasDEX").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Dinero Express");
                total.addProperty("idService", "17");

                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Dinero Express");
                jsonObject.addProperty("idService", "17");

                jsonObject.add("detalle", dineroExpress.getRespuesta().get("transferenciasDEX").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (cambios.getRespuesta() != null) {

                JsonArray totales = cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Cambios Divisa");
                total.addProperty("idService", "20");

                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Cambios Divisa");
                jsonObject.addProperty("idService", "20");
                jsonObject.add("detalle", cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (contribucion.getRespuesta() != null) {

                String crecimientoSrt = contribucion.getRespuesta().get("contribucion").getAsJsonObject().get("crecimiento").getAsString();
                double crecimiento = Double.parseDouble(crecimientoSrt);

                int percent = (int) Math.round(crecimiento);

                String indicador = "" + percent + "%";
                String color = percent >= 1 ? "verde" : "rojo";
                //String color = contribucion.get("contribucion").getAsJsonObject().get("imagen").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Contribucion");
                total.addProperty("idService", "1");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Contribucion");
                jsonObject.addProperty("idService", "1");
                jsonObject.add("detalle", contribucion.getRespuesta().get("contribucion").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (noAbonadasPendientes.getRespuesta() != null) {

                JsonObject total = null;

                //Pendientes por surtir
                int pendSurtidas = Integer.parseInt(noAbonadasPendientes.getRespuesta().get("pendientesSurtrir").getAsJsonObject().get("pendientesSurtir").getAsString());
                String indicador = "" + pendSurtidas;
                String color = pendSurtidas > 0 ? "rojo" : "verde";

                total = new JsonObject();
                //total.addProperty("idService", "Pendientes por Surtir");
                total.addProperty("idService", "14");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Pendientes por Surtir");
                jsonObject.addProperty("idService", "14");

                jsonObject.add("detalle", noAbonadasPendientes.getRespuesta().get("pendientesSurtrir").getAsJsonObject());

                detalles.add(jsonObject);

                //Nunca Abonados
                int nuncaAbondas = Integer.parseInt(noAbonadasPendientes.getRespuesta().get("nuncaAbonadas").getAsJsonObject().get("noAbonadas").getAsString());
                int totalCuentas = Integer.parseInt(noAbonadasPendientes.getRespuesta().get("nuncaAbonadas").getAsJsonObject().get("totalCuentas").getAsString());
                int percent = 0;

                if (totalCuentas > 0) {
                    percent = nuncaAbondas / totalCuentas;
                }

                indicador = "" + percent + "%";
                color = percent >= 3 ? "verde" : "rojo";

                total = new JsonObject();
                //total.addProperty("idService", "Nunca Abonadas");
                total.addProperty("idService", "16");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                jsonObject = new JsonObject();
                //				jsonObject.addProperty("idService", "Nunca abonadas");
                jsonObject.addProperty("idService", "16");
                jsonObject.addProperty("codigo", "noabo");
                jsonObject.add("detalle", noAbonadasPendientes.getRespuesta().get("nuncaAbonadas").getAsJsonObject());

                detalles.add(jsonObject);

            }

            //xml = extraccionFinancieraBI.getIndicadores(fecha, geografia, nivel);
        } catch (Exception e) {
            logger.info("Ap en el servicio getIndicadores() " + e.getMessage());
            respuestaHilo = null;
        }

        jsonGeneral.add("indicadores", indicadores);
        jsonGeneral.add("detalles", detalles);
        jsonGeneral.addProperty("ceco", cecoClase);

        respuestaHilo = jsonGeneral;

//		System.out.println(geografia + " Respuesta Hilo : " + respuestaHilo);
        //return  jsonGeneral;
    }

    /*
     *  Metodo que obtiene los indicadores de cada hijo
     */
    public JsonArray getIndicadoresHijos(String ceco) {

        JsonArray hijosDatos = null;
        List<CecoDTO> lista = obtieneHijos(ceco);
        JsonArray listaHijos = new JsonArray();

        try {

            List<IndicadoresDetallesBI> arrayThreads = new ArrayList<IndicadoresDetallesBI>();

            for (CecoDTO cecoDTO : lista) {
                String geografia = cecoDTO.getIdCeco() + " - " + cecoDTO.getDescCeco();
                IndicadoresDetallesBI hilo = new IndicadoresDetallesBI(1, fechaClase, geografia, nivelHijo, negocioClase);
                arrayThreads.add(hilo);
                hilo.start();
            }

            /*Inicia Hilos*/
//    		for (IndicadoresDetallesBI hilo : arrayThreads) {
//    			hilo.start();
//    		}

            /*Ciclo que servira para verificar si todo los hilos ya terminaron*/
            boolean terminaron = false;
            List<IndicadoresDetallesBI> aElminar = new ArrayList<IndicadoresDetallesBI>();
            while (!terminaron) {
                Iterator<IndicadoresDetallesBI> it = arrayThreads.iterator();
                int cont = 0;

                //System.out.println("Contador :"+cont);
                if (cont == arrayThreads.size()) {
                    terminaron = true;
                }
                while (it.hasNext()) {
                    IndicadoresDetallesBI hilo = it.next();
                    if (hilo.respuestaHilo != null) {
                        listaHijos.add(hilo.respuestaHilo);
                        //System.out.println("Agregue CECO "+hilo.cecoClase);
                        aElminar.add(hilo);
                        cont++;
                    }
                }
                for (IndicadoresDetallesBI eliminarHilo : aElminar) {
                    arrayThreads.remove(eliminarHilo);
                }
                aElminar.clear();
            }

            /*Envia ArrayObject con los indicadores de cada hijo para generar tabla de totales*/
            hijosDatos = armaJsonHijos(listaHijos);

        } catch (Exception e) {
            logger.info("Ap en getIndicadoresHijos() ");
            logger.info("Exception para Ceco (" + ceco + ") " + e);

            return new JsonArray();
        }

        //System.out.println("Lista Respuesta " +listaHijos);
        return hijosDatos;

    }

    public JsonArray armaJsonHijos(JsonArray arrayHijos) {

        int arrayServices[] = {6, 5, 7, 9, 8, 15, 19, 18, 20, 17, 1, 14, 16};
        JsonArray totalesCeco = null;
        DecimalFormat formateador = new DecimalFormat("###,###.#");

        JsonArray allServices = new JsonArray();

        for (int i : arrayServices) {
            JsonArray tabla = new JsonArray();
            JsonObject service = new JsonObject();
            service.addProperty("idService", i);

            double arrayAcumulado1[] = new double[10];
            double arrayAcumuladosPercent[] = new double[10];

            for (JsonElement hijo : arrayHijos) {

                JsonObject fila = new JsonObject();
//    			System.out.println("Ceco" + hijo.getAsJsonObject().get("ceco").getAsString());
                fila.addProperty("ceco", hijo.getAsJsonObject().get("ceco").getAsString());

                totalesCeco = buscaTotalesService(i, hijo);

                //System.out.println("Totales : "+totalesCeco.size());
                fila.add("valores", totalesCeco);

                if (i == 6 || i == 5 || i == 7 || i == 9 || i == 8 || i == 15 || i == 19 || i == 20 || i == 17 || i == 18) {
                    int cont = 0;
                    for (JsonElement elemento : totalesCeco) {
                        arrayAcumulado1[cont] += Double.parseDouble(elemento.getAsJsonObject().get("cantidad").getAsString().replace(",", ""));
                        arrayAcumuladosPercent[cont] += Double.parseDouble(elemento.getAsJsonObject().get("porcentaje").getAsString().replace("%", ""));
                        cont++;
                    }
                }

                //System.out.println("Fila"+fila);
                tabla.add(fila);
            }

            JsonArray acumulados = new JsonArray();

            for (int d = 0; d < arrayAcumulado1.length; d++) {
                JsonObject acumulado = new JsonObject();

                acumulado.addProperty("cantidad", formateador.format(arrayAcumulado1[d]));
                acumulado.addProperty("porcentaje", arrayAcumuladosPercent[d]);
                if (arrayAcumulado1[d] >= 0) {
                    acumulado.addProperty("color", "verde");
                } else {
                    acumulado.addProperty("color", "rojo");
                }

                acumulados.add(acumulado);
            }

            service.add("tabla", tabla);
            service.add("totales", acumulados);
            allServices.add(service);
        }
        //System.out.println("Respuesta"+ allServices);

        return allServices;
    }

    public JsonArray buscaTotalesService(int idService, JsonElement json) {

        JsonArray detalles = json.getAsJsonObject().get("detalles").getAsJsonArray();
        JsonArray totales = new JsonArray();
        JsonObject detalle = null;
        JsonArray detalleArray = null;

        try {

            for (JsonElement jsonElement : detalles) {

                int service = Integer.parseInt(jsonElement.getAsJsonObject().get("idService").getAsString());

                if (service == idService) {
                    if (service == 1 || service == 14 || service == 16 || service == 6 || service == 5 || service == 7 || service == 9 || service == 8 || service == 15) {
                        detalle = jsonElement.getAsJsonObject().get("detalle").getAsJsonObject();
                    } else if (service == 19 || service == 18 || service == 17 || service == 9 || service == 20) {
                        detalleArray = jsonElement.getAsJsonObject().get("detalle").getAsJsonArray();
                    }
                    break;
                }
            }

            if (detalle != null || detalleArray != null) {

                switch (idService) {
                    case 6:
                        //Consumo Colocacion
                        //System.out.println("Consumo");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 5:
                        //Personales Colocacion
                        //System.out.println("Personales");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 7:
                        //TAZ Colocacion
                        //System.out.println("TAZ");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 9:
                        //TAZ Colocacion
                        //System.out.println("Captacion");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 8:
                        //TAZ Colocacion
                        //System.out.println("Captacion 2");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 15:
                        //System.out.println("Normalidad");
                        totales = detalle.getAsJsonObject().get("Saldo Cartera De 0 A 39 Semanas").getAsJsonArray();
                        break;
                    case 1:
                        //System.out.println("Contribucion");
                        //totales = new JsonArray();
                        totales = getTotalesObject(detalle);
                        break;
                    case 14:
                        //totales = new JsonArray();
                        //System.out.println("Pendientes Surtir");
                        totales = getTotalesObject(detalle);
                        break;
                    case 16:
                        //totales = new JsonArray();
                        //System.out.println("Nunca Abonadas");
                        totales = getTotalesObject(detalle);
                        break;
                    default:
                        totales = detalleArray.get(7).getAsJsonArray();
                        break;
                }
            }

//        	for (JsonElement jsonElement : totales) {
//        		System.out.println(jsonElement.getAsJsonObject().get("cantidad").getAsString());
//    		}
        } catch (Exception e) {
            logger.info("Ap en buscaTotalesService() " + e);
        }

        return totales;
    }

    public JsonArray getTotalesObject(JsonObject detalle) {

        JsonArray totales = new JsonArray();

        totales.add(detalle);

        return totales;

    }

    public static void main(String[] args) {

        String nivelStr = "R";
        int nivel = 0;

        if (nivelStr.equals("TR")) {
            nivel = 1;
        } else if (nivelStr.equals("Z")) {
            nivel = 2;
        } else if (nivelStr.equals("R")) {
            nivel = 3;
        } else if (nivelStr.equals("T")) {
            nivel = 4;
        }

        String fecha = "20171023";

        String geografia = "236399 - REGIONAL VENTAS COYOACAN TLALPAN";

        String negocio = "82";

        IndicadoresDetallesBI proceso1 = new IndicadoresDetallesBI(1, fecha, geografia, nivel, negocio);

        IndicadoresDetallesBI proceso2 = new IndicadoresDetallesBI(2, fecha, geografia, nivel, negocio);

        proceso1.start();
        proceso2.start();

        while (proceso1.isAlive()
                || proceso2.isAlive()) {
            /*Se ejecutaran mientras lo hilos sigan vivos los hilos*/

        }

        JsonObject resultadoFinal = new JsonObject();
        resultadoFinal.add("principal", proceso1.respuestaHilo);
        resultadoFinal.add("services", proceso2.arrayHijos);

        System.out.println("Json Final " + resultadoFinal);

    }

}
