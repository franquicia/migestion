package com.gruposalinas.migestion.business.ef;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.external.data.soap.sie.client.IndicadoresSIEBI;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.resources.GTNConstantes.rubrosNegocio;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IndicadoresNegocioCapBI {

    private static Logger logger = LogManager.getLogger(IndicadoresNegocioCapBI.class);
    private final ObjectMapper objectMapper = new ObjectMapper();

    public JsonObject getResultadoIndicador(int idService, String fecha, String geografia, String nivel) {
        IndicadoresBI bussines = new IndicadoresBI();
        IndicadoresCapBI bussinesCap = new IndicadoresCapBI();
        IndicadoresSIEBI indicadoresSIE = new IndicadoresSIEBI();
        JsonObject respuesta = null;
        GTNConstantes.rubrosNegocio rubro = GTNConstantes.rubrosNegocio.getType(idService);
        String jsonString = "{\"indicadores\":[],\"detalles\":[]}";
        switch (rubro) {
           
            case colocacionConsumo:
                respuesta = bussines.getConsumoColocacion(fecha, geografia, nivel);
                respuesta = this.getJsonRespuesta(idService, respuesta);
                jsonString = indicadoresSIE.consumoColocacion(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case colocacionPrestamosPer:
               respuesta = bussines.getPersonalesColocacion(fecha, geografia, nivel);
                respuesta = this.getJsonRespuesta(idService, respuesta);
                jsonString = indicadoresSIE.personalesColocacion(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case solicitudesPendientesSurtir:
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
           case colocacionTAZ:
               respuesta = bussines.getTAZColocacion(fecha, geografia, nivel);
               respuesta = this.getJsonRespuesta(idService, respuesta);
                jsonString = indicadoresSIE.tazColocacion(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case captacionSaldoPlazo:
                respuesta = bussines.getSaldoCaptacionPlazo(fecha, geografia, nivel);
                jsonString = indicadoresSIE.saldoCaptacionPlazo(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case captacionSaldoVista:
                respuesta = bussines.getSaldoCaptacionVista(fecha, geografia, nivel);
                jsonString = indicadoresSIE.saldoCaptacionVista(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case carteraNormalidad:
                respuesta = bussines.getNormalidad(fecha, geografia, nivel);
                respuesta = this.getJsonRespuesta(idService, respuesta);
                jsonString = indicadoresSIE.normalidad(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case negocioPagoServicios:
               respuesta = bussines.getPagosDeServicio(fecha, geografia, nivel);
               respuesta = this.getJsonRespuesta(idService, respuesta);
                jsonString = indicadoresSIE.transferenciasPagoServicios(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case negocioTransferenciaInter:
               respuesta = bussines.getTransferciasInternacionales(fecha, geografia, nivel);
               respuesta = this.getJsonRespuesta(idService, respuesta);
                jsonString = indicadoresSIE.transferenciasInternacionales(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case negocioTransferenciaDEX:
              respuesta = bussines.getDineroExpress(fecha, geografia, nivel);
             respuesta = this.getJsonRespuesta(idService, respuesta);
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case negocioCambios:
               respuesta = bussines.getCambios(fecha, geografia, nivel);
               respuesta = this.getJsonRespuesta(idService, respuesta);
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case contribucion:
               respuesta = bussines.getContribucion(fecha, geografia, nivel);
                respuesta = this.getJsonRespuesta(idService, respuesta);
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case carteraNuncaAbonadas:
               respuesta = bussines.getNoAbonadasPend(fecha, geografia, nivel);
                respuesta = this.getJsonRespuesta(idService, respuesta);
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case captacionSaldoFinalVista:
                respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "SF", 1);
                jsonString = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "SF", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case captacionNetaVista:
                respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "CN", 1);
                jsonString = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "CN", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case captacionSaldoFinalPlazo:
               respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "SF", 2);
                jsonString = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "SF", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case captacionNetaPlazo:
               respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "CN", 2);
                jsonString = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "CN", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case transferenciasDEX:
                respuesta = bussinesCap.getDineroExpress(fecha, geografia, nivel);
               respuesta = this.getJsonRespuesta(idService, respuesta);
                jsonString = indicadoresSIE.tranferenciasDEX(fecha, geografia, nivel, idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case numAperturasClientesNuevosVista:
               respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "NA", 1);
                jsonString = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "NA", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case montAperturasClientesNuevosVista:
              respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "MA", 1);
                jsonString = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "MA", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case numAperturasClientesNuevosPlazo:
               respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "NA", 2);
                jsonString = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "NA", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            case montAperturasClientesNuevosPlazo:
                respuesta = bussinesCap.getCaptaciones(fecha, geografia, nivel, "MA", 2);
                jsonString = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "MA", idService, false).toString();
                respuesta = new JsonParser().parse(jsonString).getAsJsonObject();
                break;
            default:
                respuesta = null;
                break;
        }
        return respuesta;
    }

   

    private List<CecoDTO> obtieneHijos(String ceco, String negocio) {
        List<CecoDTO> listaCecos = new ArrayList<>();
        CecoBI cecoBi = (CecoBI) GTNAppContextProvider.getApplicationContext().getBean("cecoBI");
        listaCecos = cecoBi.buscaCecosSuperior(ceco, "" + negocio);
        System.out.println("****** TOTAL CECOS HIJOS *****:" + listaCecos.size());
        return listaCecos;
    }

    public JsonObject getJsonRespuesta(int idService, JsonObject respuestaSIE) {

        JsonObject jsonGeneral = new JsonObject();
        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();

        GTNConstantes.rubrosNegocio rubro = GTNConstantes.rubrosNegocio.getType(idService);

        try {

            if (rubro == rubrosNegocio.colocacionConsumo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("consumoColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", "6");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", "6");
                jsonObject.addProperty("codigo", "CONSUMO");
                jsonObject.add("detalle", respuestaSIE.get("consumoColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.colocacionPrestamosPer && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("personalesColocacion").getAsJsonObject()
                        .get("totales").getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Personales");
                total.addProperty("idService", "5");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Personales");
                jsonObject.addProperty("idService", "5");
                jsonObject.addProperty("codigo", "PERSONALES");
                jsonObject.add("detalle", respuestaSIE.get("personalesColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.colocacionTAZ && respuestaSIE != null) {
                JsonArray totales = respuestaSIE.get("tazColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "TAZ");
                total.addProperty("idService", "7");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "TAZ");
                jsonObject.addProperty("idService", "7");
                jsonObject.addProperty("codigo", "TAZ");

                jsonObject.add("detalle", respuestaSIE.get("tazColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.captacionSaldoPlazo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("saldoCaptacionPlazo").getAsJsonObject()
                        .get("totales").getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Saldo Captacion Plazo");
                total.addProperty("idService", "9");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Saldo Captacion Plazo");
                jsonObject.addProperty("idService", "9");
                jsonObject.addProperty("codigo", "GUARDADITO");
                jsonObject.add("detalle", respuestaSIE.get("saldoCaptacionPlazo").getAsJsonObject());

                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.captacionSaldoVista && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("saldoCaptacionVista").getAsJsonObject()
                        .get("totales").getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Saldo Captacion Vista");
                total.addProperty("idService", "8");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Saldo Captacion Vista");
                jsonObject.addProperty("idService", "8");
                jsonObject.addProperty("codigo", "INVERSIONES");
                jsonObject.add("detalle", respuestaSIE.get("saldoCaptacionVista").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (rubro == rubrosNegocio.carteraNormalidad && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("normalidad").getAsJsonObject()
                        .get("Vigente De 0 A 1").getAsJsonArray();
                String indicador = totales.get(2).getAsJsonObject().get("porcentaje").getAsString();

                String color = totales.get(0).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Normalidad");
                total.addProperty("idService", "15");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Normalidad");
                jsonObject.addProperty("idService", "15");
                jsonObject.addProperty("codigo", "NORMALIDAD");
                jsonObject.add("detalle", respuestaSIE.get("normalidad").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.negocioPagoServicios && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("pagosServicios").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Pago de Servicios");
                total.addProperty("idService", "19");
                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Pago de Servicios");
                jsonObject.addProperty("idService", "19");
                jsonObject.addProperty("codigo", "PAGO_SERVICIOS");
                jsonObject.add("detalle", respuestaSIE.get("pagosServicios").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.negocioTransferenciaInter && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("transferenciasInternacionales")
                        .getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Transferencias
                // Internacionales");
                total.addProperty("idService", "18");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Transferencias
                // Internacionales");
                jsonObject.addProperty("idService", "18");
                jsonObject.add("detalle", respuestaSIE
                        .get("transferenciasInternacionales").getAsJsonArray());
                jsonObject.addProperty("codigo", "TRANS_INTERNACIONALES");
                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.negocioTransferenciaDEX && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("transferenciasDEX").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Dinero Express");
                total.addProperty("idService", "17");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Dinero Express");
                jsonObject.addProperty("idService", "17");
                jsonObject.addProperty("codigo", "DINERO EXPRESS");
                jsonObject.add("detalle", respuestaSIE.get("transferenciasDEX").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.negocioCambios && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("transferenciasCambios").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Cambios Divisa");
                total.addProperty("idService", "20");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Cambios Divisa");
                jsonObject.addProperty("idService", "20");
                jsonObject.add("detalle", respuestaSIE.get("transferenciasCambios").getAsJsonArray());
                jsonObject.addProperty("codigo", "CAMBIOS");
                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.contribucion && respuestaSIE != null) {

                String crecimientoSrt = respuestaSIE.get("contribucion").getAsJsonObject()
                        .get("crecimiento").getAsString();
                double crecimiento = Double.parseDouble(crecimientoSrt);

                int percent = (int) Math.round(crecimiento);

                String indicador = "" + percent + "%";
                String color = percent >= 1 ? "verde" : "rojo";
                // String color =
                // contribucion.get("contribucion").getAsJsonObject().get("imagen").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Contribucion");
                total.addProperty("idService", "1");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Contribucion");
                jsonObject.addProperty("idService", "1");
                jsonObject.add("detalle", respuestaSIE.get("contribucion").getAsJsonObject());
                jsonObject.addProperty("codigo", "CONTRIBUCION");
                detalles.add(jsonObject);
            }

            if (rubro == rubrosNegocio.solicitudesPendientesSurtir && respuestaSIE != null) {

                JsonObject total = null;

                // Pendientes por surtir
                int pendSurtidas = Integer.parseInt(respuestaSIE.get("pendientesSurtrir")
                        .getAsJsonObject().get("pendientesSurtir").getAsString());
                String indicador = "" + pendSurtidas;
                String color = pendSurtidas > 0 ? "rojo" : "verde";

                total = new JsonObject();
                // total.addProperty("idService", "Pendientes por Surtir");
                total.addProperty("idService", "14");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Pendientes por Surtir");
                jsonObject.addProperty("idService", "14");
                jsonObject.addProperty("codigo", "PENDIENTES_SURTIR");
                jsonObject.add("detalle",
                        respuestaSIE.get("pendientesSurtrir").getAsJsonObject());

                detalles.add(jsonObject);

                // Nunca Abonados
                int nuncaAbondas = Integer.parseInt(respuestaSIE.get("nuncaAbonadas")
                        .getAsJsonObject().get("noAbonadas").getAsString());
                int totalCuentas = Integer.parseInt(respuestaSIE.get("nuncaAbonadas")
                        .getAsJsonObject().get("totalCuentas").getAsString());
                int percent = 0;

                if (totalCuentas > 0) {
                    percent = nuncaAbondas / totalCuentas;
                }

                indicador = "" + percent + "%";
                color = percent >= 3 ? "verde" : "rojo";

                total = new JsonObject();
                // total.addProperty("idService", "Nunca Abonadas");
                total.addProperty("idService", "16");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Nunca abonadas");
                jsonObject.addProperty("idService", "16");
                jsonObject.addProperty("codigo", "noabo");
                jsonObject.add("detalle", respuestaSIE.get("nuncaAbonadas").getAsJsonObject());

                detalles.add(jsonObject);

            }

            // SEGUROS AGREGAR 2 MAS
            if (rubro == rubrosNegocio.segurosNoLigados && respuestaSIE != null) {

                JsonObject totalSegurosNoLigados = respuestaSIE;

                logger.info(" RESPUESTA DE SEGUROS NO LIGADOS" + totalSegurosNoLigados);

                // JsonArray totales =
                // segurosNoLigados.getRespuesta().get("segurosNoLigados").getAsJsonArray().get(7).getAsJsonArray();

                /*
                 * String indicador =
                 * totales.get(6).getAsJsonObject().get("porcentaje").getAsString(); String
                 * color = totales.get(6).getAsJsonObject().get("color").getAsString();
                 * JsonObject total = new JsonObject(); // total.addProperty("idService",
                 * "Cambios Divisa"); total.addProperty("idService", "20");
                 *
                 * // total.addProperty("indicador", color.equals("rojo") ? "-" + // indicador :
                 * indicador); // total.addProperty("color", color);
                 *
                 * total.addProperty("indicador", indicador); total.addProperty("color", color);
                 *
                 * indicadores.add(total); JsonObject jsonObject = new JsonObject(); //
                 * jsonObject.addProperty("idService", "Cambios Divisa");
                 * jsonObject.addProperty("idService", "20"); jsonObject.add("detalle",
                 * cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray());
                 *
                 * detalles.add(jsonObject);
                 */
            }

            if (rubro == rubrosNegocio.segurosPrestamos && respuestaSIE != null) {

                JsonObject totalSegurosPrestamos = respuestaSIE;

                logger.info(" RESPUESTA DE SEGUROS PRESTAMOS" + totalSegurosPrestamos);

                // JsonArray totales =
                // segurosNoLigados.getRespuesta().get("segurosNoLigados").getAsJsonArray().get(7).getAsJsonArray();

                /*
                 * String indicador =
                 * totales.get(6).getAsJsonObject().get("porcentaje").getAsString(); String
                 * color = totales.get(6).getAsJsonObject().get("color").getAsString();
                 * JsonObject total = new JsonObject(); // total.addProperty("idService",
                 * "Cambios Divisa"); total.addProperty("idService", "20");
                 *
                 * // total.addProperty("indicador", color.equals("rojo") ? "-" + // indicador :
                 * indicador); // total.addProperty("color", color);
                 *
                 * total.addProperty("indicador", indicador); total.addProperty("color", color);
                 *
                 * indicadores.add(total); JsonObject jsonObject = new JsonObject(); //
                 * jsonObject.addProperty("idService", "Cambios Divisa");
                 * jsonObject.addProperty("idService", "20"); jsonObject.add("detalle",
                 * cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray());
                 *
                 * detalles.add(jsonObject);
                 */
            }

            if (rubro == rubrosNegocio.segurosRenovaciones && respuestaSIE != null) {

                JsonObject totalSegurosRenovaciones = respuestaSIE;

                logger.info(" RESPUESTA DE SEGUROS PRESTAMOS" + totalSegurosRenovaciones);

                // JsonArray totales =
                // segurosNoLigados.getRespuesta().get("segurosNoLigados").getAsJsonArray().get(7).getAsJsonArray();

                /*
                 * String indicador =
                 * totales.get(6).getAsJsonObject().get("porcentaje").getAsString(); String
                 * color = totales.get(6).getAsJsonObject().get("color").getAsString();
                 * JsonObject total = new JsonObject(); // total.addProperty("idService",
                 * "Cambios Divisa"); total.addProperty("idService", "20");
                 *
                 * // total.addProperty("indicador", color.equals("rojo") ? "-" + // indicador :
                 * indicador); // total.addProperty("color", color);
                 *
                 * total.addProperty("indicador", indicador); total.addProperty("color", color);
                 *
                 * indicadores.add(total); JsonObject jsonObject = new JsonObject(); //
                 * jsonObject.addProperty("idService", "Cambios Divisa");
                 * jsonObject.addProperty("idService", "20"); jsonObject.add("detalle",
                 * cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray());
                 *
                 * detalles.add(jsonObject);
                 */
            }

            //Captación
            if (rubro == rubrosNegocio.captacionSaldoFinalVista && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionVista_SF").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "VSF");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionVista_SF").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.captacionNetaVista && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionVista_CN").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "VCN");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionVista_CN").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.numAperturasClientesNuevosVista && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionVista_NA").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "VNA");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionVista_NA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.montAperturasClientesNuevosVista && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionVista_MA").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "VMA");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionVista_MA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.captacionSaldoFinalPlazo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionPlazo_SF").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "PSF");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionPlazo_SF").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.captacionNetaPlazo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionPlazo_CN").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "PCN");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionPlazo_CN").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.numAperturasClientesNuevosPlazo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionPlazo_NA").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "PNA");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionPlazo_NA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.montAperturasClientesNuevosPlazo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.getAsJsonArray("captacionPlazo_MA").get(7)
                        .getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", idService + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "PMA");
                jsonObject.add("detalle",
                        respuestaSIE.get("captacionPlazo_MA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.transferenciasDEX && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("transferenciasDEX").getAsJsonArray()
                        .get(7).getAsJsonArray();

                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Dinero Express");
                total.addProperty("idService", idService + "");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Dinero Express");
                jsonObject.addProperty("idService", idService + "");
                jsonObject.addProperty("codigo", "DEX");
                jsonObject.add("detalle",
                        respuestaSIE.get("transferenciasDEX").getAsJsonArray());

                detalles.add(jsonObject);

            }

            jsonGeneral.add("indicadores", indicadores);
            jsonGeneral.add("detalles", detalles);

        } catch (Exception e) {
            logger.info("Ap en el servicio getIndicadores() ");
            jsonGeneral = null;
        }

        return jsonGeneral;

    }

}
