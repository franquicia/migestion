package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IndicadoresCaptacionTotalesBI extends Thread {

    private Logger logger = LogManager.getLogger(IndicadoresCaptacionTotalesBI.class);
    private int procesoClase = 0;
    private String cecoClase = "";
    private int nivelHijo = 0;
    private String nivelClase = "";
    private String fechaClase = "";
    private JsonObject respuestaHilo = null;
    private JsonArray arrayHijos = null;
    private String negocioClase = "";
    private int idService = 0;
    private boolean isNoAbonadasClase = true;

    @Override
    public void run() {

        if (procesoClase == 1) {
            getIndicadores(fechaClase, cecoClase, nivelClase, idService);
        } else {
            arrayHijos = getIndicadoresHijos(cecoClase.substring(0, 6));
        }

    }

    public IndicadoresCaptacionTotalesBI() {

    }

    public IndicadoresCaptacionTotalesBI(int proceso, String fecha, String geografia, int nivel, String negocio, int idIndicador,
            boolean isNoAbonadas) {

        negocioClase = negocio;
        fechaClase = fecha;
        procesoClase = proceso;
        cecoClase = geografia;
        nivelHijo = nivel + 1;
        idService = idIndicador;
        isNoAbonadasClase = isNoAbonadas;

        switch (nivel) {
            case 1:
                nivelClase = "TR";
                break;
            case 2:
                nivelClase = "Z";
                break;
            case 3:
                nivelClase = "R";
                break;
            case 4:
                nivelClase = "T";
                break;
        }

    }

    public List<CecoDTO> obtieneHijos(String ceco) {

        List<CecoDTO> listaCecos = new ArrayList<>();

//		System.out.println("Negocio Clase :" + negocioClase);
        CecoBI cecoBi = (CecoBI) GTNAppContextProvider.getApplicationContext().getBean("cecoBI");
        listaCecos = cecoBi.buscaCecosSuperior(ceco, "" + negocioClase);

        System.out.println("****** TAMA�O LISTA *****:" + listaCecos.size());

        return listaCecos;
    }

    /*
     * Metodo que obtiene todos los indicadores de un Ceco en especifico
     *
     */
    public void getIndicadores(String fecha, String geografia, String nivel, int servicio) {

        JsonObject jsonGeneral = new JsonObject();
        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();

        ExtraccionFinancieraThreadNewBI2 hiloIndicadores = new ExtraccionFinancieraThreadNewBI2(servicio, fecha,
                geografia, nivel);

        try {
            long inicio = System.currentTimeMillis();
//			System.out.println("INICIA EJECUCION DE HILOS DE LA GEOGRAFIA  " + geografia + " HORA :" + new Date());

            hiloIndicadores.start();

            // Se ejecuta while para saber cuando
            while (hiloIndicadores.isAlive()) {
                /* Espera hasta que los hilos hayan terminado */

            }

            long acabo = System.currentTimeMillis();
            long totaltiempo = (acabo - inicio) / 1000;

            System.out.println("HORA EN LA QUE TERMINA LOS HILOS " + acabo);
//			System.out
//					.println("TERMINARON TODOS LOS HILOS EN " + totaltiempo + " SEGUNDOS DE LA GEOGRAFIA " + geografia);

            if (servicio == 6 && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().get("consumoColocacion").getAsJsonObject()
                        .get("totales").getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", "6");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", "6");
                jsonObject.addProperty("codigo", "cons");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("consumoColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            // xml = extraccionFinancieraBI.getIndicadores(fecha, geografia,
            // nivel);
        } catch (Exception e) {
            logger.info("Ap en el servicio getIndicadores() " + e.getMessage());
            //e.printStackTrace();
            // respuestaHilo = null;
        }

        jsonGeneral.add("indicadores", indicadores);
        jsonGeneral.add("detalles", detalles);
        jsonGeneral.addProperty("ceco", cecoClase);

        respuestaHilo = jsonGeneral;

//		System.out.println(geografia + " Respuesta Hilo : " + jsonGeneral);
        // return jsonGeneral;
    }

    /*
     * Metodo que obtiene los indicadores de cada hijo
     */
    public JsonArray getIndicadoresHijos(String ceco) {

        JsonArray hijosDatos = null;
        List<CecoDTO> lista = obtieneHijos(ceco);
        JsonArray listaHijos = new JsonArray();

        try {

            List<IndicadoresCaptacionTotalesBI> arrayThreads = new ArrayList<IndicadoresCaptacionTotalesBI>();

            for (CecoDTO cecoDTO : lista) {
                String geografia = cecoDTO.getIdCeco() + " - " + cecoDTO.getDescCeco();
                IndicadoresCaptacionTotalesBI hilo = new IndicadoresCaptacionTotalesBI(1, fechaClase, geografia, nivelHijo, negocioClase,
                        idService, isNoAbonadasClase);
                arrayThreads.add(hilo);
                hilo.start();
            }

            /*
             * Ciclo que servira para verificar si todo los hilos ya terminaron
             */
            boolean terminaron = false;
            List<IndicadoresCaptacionTotalesBI> aElminar = new ArrayList<IndicadoresCaptacionTotalesBI>();
            while (!terminaron) {
                Iterator<IndicadoresCaptacionTotalesBI> it = arrayThreads.iterator();
                int cont = 0;

                // System.out.println("Contador :"+cont);
                if (cont == arrayThreads.size()) {
                    terminaron = true;
                }
                while (it.hasNext()) {
                    IndicadoresCaptacionTotalesBI hilo = it.next();
                    if (hilo.respuestaHilo != null) {
                        listaHijos.add(hilo.respuestaHilo);
                        System.out.println("Agregue CECO " + hilo.cecoClase);
                        aElminar.add(hilo);
                        cont++;
                    }
                }
                for (IndicadoresCaptacionTotalesBI eliminarHilo : aElminar) {
                    arrayThreads.remove(eliminarHilo);
                }
                aElminar.clear();
            }

            /*
             * Envia ArrayObject con los indicadores de cada hijo para generar
             * tabla de totales
             */
            hijosDatos = armaJsonHijos(listaHijos);

        } catch (Exception e) {
            logger.info("Ap en getIndicadoresHijos() ");
            logger.info("Exception para Ceco (" + ceco + ") " + e);

            return new JsonArray();
        }

        // System.out.println("Lista Respuesta " +listaHijos);
        return hijosDatos;

    }

    public JsonArray armaJsonHijos(JsonArray arrayHijos) {

        // int arrayServices[] = {6,5,7,9,8,15,19,18,20,17,1,14,16};
        JsonArray totalesCeco = null;
        DecimalFormat formateador = new DecimalFormat("###,###.#");

        JsonArray allServices = new JsonArray();

        // for (int i : arrayServices) {
        JsonArray tabla = new JsonArray();
        JsonObject service = new JsonObject();
        if (isNoAbonadasClase) {
            service.addProperty("idService", "16");
        } else {
            service.addProperty("idService", idService);
        }

        double arrayAcumulado1[] = new double[10];
        double arrayAcumuladosPercent[] = new double[10];

        for (JsonElement hijo : arrayHijos) {

            JsonObject fila = new JsonObject();
            System.out.println("Ceco" + hijo.getAsJsonObject().get("ceco").getAsString());
            fila.addProperty("ceco", hijo.getAsJsonObject().get("ceco").getAsString());

            totalesCeco = buscaTotalesService(idService, hijo);

            System.out.println("Totales : " + totalesCeco.size());

            fila.add("valores", totalesCeco);

            if (idService == 6 || idService == 5 || idService == 7 || idService == 9 || idService == 8
                    || idService == 15 || idService == 19 || idService == 20 || idService == 17 || idService == 18) {
                int cont = 0;
                for (JsonElement elemento : totalesCeco) {
                    arrayAcumulado1[cont] += Double
                            .parseDouble(elemento.getAsJsonObject().get("cantidad").getAsString().replace(",", ""));
                    arrayAcumuladosPercent[cont] += Double
                            .parseDouble(elemento.getAsJsonObject().get("porcentaje").getAsString().replace("%", ""));
                    cont++;
                }
            }

            System.out.println("Fila" + fila);
            tabla.add(fila);
        }

        JsonArray acumulados = new JsonArray();

        for (int d = 0; d < arrayAcumulado1.length; d++) {
            JsonObject acumulado = new JsonObject();

            acumulado.addProperty("cantidad", formateador.format(arrayAcumulado1[d]));
            acumulado.addProperty("porcentaje", arrayAcumuladosPercent[d]);
            if (arrayAcumulado1[d] >= 0) {
                acumulado.addProperty("color", "verde");
            } else {
                acumulado.addProperty("color", "rojo");
            }

            acumulados.add(acumulado);
        }

        service.add("tabla", tabla);
        service.add("totales", acumulados);
        allServices.add(service);
        // }
        System.out.println("Respuesta" + allServices);

        return allServices;
    }

    public JsonArray buscaTotalesService(int idService, JsonElement json) {

        System.out.println("*************¿ EL SERVICIO ES 16 ?  ************** " + isNoAbonadasClase);

        JsonArray detalles = json.getAsJsonObject().get("detalles").getAsJsonArray();
        JsonArray totales = new JsonArray();
        JsonObject detalle = null;
        JsonArray detalleArray = null;

        try {

            System.out.println("TAMAÑO detalles :" + detalles.size());

            for (JsonElement jsonElement : detalles) {

                int service = Integer.parseInt(jsonElement.getAsJsonObject().get("idService").getAsString());

                System.out.println("Indicador Interno " + service);
                // REvisar para obtener los dos
                if (service == idService || (service == 16 && isNoAbonadasClase)) {

                    if (!(service == 14 && isNoAbonadasClase)) {

                        if (service == 1 || service == 14 || service == 16 || service == 6 || service == 5
                                || service == 7 || service == 9 || service == 8 || service == 15) {
                            detalle = jsonElement.getAsJsonObject().get("detalle").getAsJsonObject();

                        } else if (service == 19 || service == 18 || service == 17 || service == 9 || service == 20) {
                            detalleArray = jsonElement.getAsJsonObject().get("detalle").getAsJsonArray();
                        }
                        break;
                    }
                }
            }

            if (detalle != null || detalleArray != null) {

                switch (idService) {
                    case 6:
                        // Consumo Colocacion
                        // System.out.println("Consumo");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 5:
                        // Personales Colocacion
                        // System.out.println("Personales");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 7:
                        // TAZ Colocacion
                        // System.out.println("TAZ");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 9:
                        // TAZ Colocacion
                        // System.out.println("Captacion");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 8:
                        // TAZ Colocacion
                        // System.out.println("Captacion 2");
                        totales = detalle.getAsJsonObject().get("totales").getAsJsonArray();
                        break;
                    case 15:
                        // System.out.println("Normalidad");
                        //totales = detalle.getAsJsonObject().get("Saldo Cartera De 0 A 39 Semanas").getAsJsonArray();
                        totales = detalle.getAsJsonObject().get("Vigente De 0 A 1").getAsJsonArray();
                        break;
                    case 1:
                        // System.out.println("Contribucion");
                        // totales = new JsonArray();
                        totales = getTotalesObject(detalle);
                        break;
                    case 14:
                        // totales = new JsonArray();
                        // System.out.println("Pendientes Surtir");
                        totales = getTotalesObject(detalle);
                        break;
                    case 16:
                        // totales = new JsonArray();
                        // System.out.println("Nunca Abonadas");
                        totales = getTotalesObject(detalle);
                        break;
                    default:
                        totales = detalleArray.get(7).getAsJsonArray();
                        break;
                }
            }

            // for (JsonElement jsonElement : totales) {
            // System.out.println(jsonElement.getAsJsonObject().get("cantidad").getAsString());
            // }
        } catch (Exception e) {
            logger.info("Ap en buscaTotalesService() " + e);
        }

        return totales;
    }

    public JsonArray getTotalesObject(JsonObject detalle) {

        JsonArray totales = new JsonArray();

        totales.add(detalle);

        return totales;

    }

    public JsonObject getJsonFinal(String fecha, String geografia, String nivelStr, String negocio, int idIndicador) {

        int nivel = 0;

        if (nivelStr.equals("TR")) {
            nivel = 1;
        } else if (nivelStr.equals("Z")) {
            nivel = 2;
        } else if (nivelStr.equals("R")) {
            nivel = 3;
        } else if (nivelStr.equals("T")) {
            nivel = 4;
        }

        boolean isNoAbnonadas = false;

        if (idIndicador == 16) {
            isNoAbnonadas = true;
            idIndicador = 14;
        }

        IndicadoresCaptacionTotalesBI proceso1 = new IndicadoresCaptacionTotalesBI(2, fecha, geografia, nivel, negocio, idIndicador,
                isNoAbnonadas);

        proceso1.start();

        while (proceso1.isAlive()) {
            /* Se ejecutaran mientras lo hilos sigan vivos los hilos */

        }

        JsonObject resultadoFinal = new JsonObject();
        resultadoFinal.add("respuesta", proceso1.arrayHijos);

        System.out.println("Json Final " + resultadoFinal);

        return resultadoFinal;
    }

    public JsonObject getTablaSemana(String fecha, String geografia, String nivelStr, String negocio, int idIndicador) {

        int nivel = 0;

        if (nivelStr.equals("TR")) {
            nivel = 1;
        } else if (nivelStr.equals("Z")) {
            nivel = 2;
        } else if (nivelStr.equals("R")) {
            nivel = 3;
        } else if (nivelStr.equals("T")) {
            nivel = 4;
        }

        boolean isNoAbonadas = false;

        if (idIndicador == 16) {
            isNoAbonadas = true;
            idIndicador = 14;
        }
        IndicadoresCaptacionTotalesBI proceso1 = new IndicadoresCaptacionTotalesBI(1, fecha, geografia, nivel, negocio, idIndicador,
                isNoAbonadas);

        proceso1.start();

        while (proceso1.isAlive()) {
            /* Se ejecutaran mientras lo hilos sigan vivos los hilos */
        }

        System.out.println(proceso1.respuestaHilo.toString());

        return proceso1.respuestaHilo;

    }

}
