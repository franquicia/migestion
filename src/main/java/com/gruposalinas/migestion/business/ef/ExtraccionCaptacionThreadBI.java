package com.gruposalinas.migestion.business.ef;

//import org.jboss.xnio.log.Logger;
import com.google.gson.JsonObject;

public class ExtraccionCaptacionThreadBI extends Thread {

    //ExtraccionFinancieraBI bussines = new ExtraccionFinancieraBI();
    IndicadoresBI bussines = new IndicadoresBI();

    private int idService;
    private String fecha;
    private String geografia;
    private String nivel;
    private boolean termino = false;
    private JsonObject respuesta;

    public boolean isTermino() {
        return termino;
    }

    public void setTermino(boolean termino) {
        this.termino = termino;
    }

    public JsonObject getRespuesta() {
        return respuesta;
    }

    public ExtraccionCaptacionThreadBI(int idService, String fecha, String geografia, String nivel) {

        this.idService = idService;
        this.fecha = fecha;
        this.geografia = geografia;
        this.nivel = nivel;

    }

    @Override
    public void run() {

        switch (idService) {
            case 1:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "SF", 1);
                break;
            case 2:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "CN", 1);
                break;
            case 3:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "NA", 1);
                break;
            case 4:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "MA", 1);
                break;
            case 5:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "SF", 2);
                break;
            case 6:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "CN", 2);
                break;
            case 7:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "NA", 2);
                break;
            case 8:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "MA", 2);
                break;
          //  case 9:
            //    respuesta = bussines.getDineroExpress(fecha, geografia, nivel);
             //   break;
            default:
                break;
        }

        termino = true;

        //System.out.println("Servicio " + idService + "termino: "+termino);
    }

}
