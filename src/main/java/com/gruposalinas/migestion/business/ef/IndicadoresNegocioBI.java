package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.resources.GTNConstantes.rubrosNegocio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IndicadoresNegocioBI {

    private static Logger logger = LogManager.getLogger(IndicadoresNegocioBI.class);

   

    public JsonObject getJsonRespuesta(int idService, JsonObject respuestaSIE) {

        JsonObject jsonGeneral = new JsonObject();
        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();

        GTNConstantes.rubrosNegocio rubro = GTNConstantes.rubrosNegocio.getType(idService);

        try {

            if (rubro == rubrosNegocio.colocacionConsumo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("consumoColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", "6");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", "6");
                jsonObject.addProperty("codigo", "CONSUMO");
                jsonObject.add("detalle", respuestaSIE.get("consumoColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.colocacionPrestamosPer && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("personalesColocacion").getAsJsonObject()
                        .get("totales").getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Personales");
                total.addProperty("idService", "5");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Personales");
                jsonObject.addProperty("idService", "5");
                jsonObject.addProperty("codigo", "PERSONALES");
                jsonObject.add("detalle", respuestaSIE.get("personalesColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.colocacionTAZ && respuestaSIE != null) {
                JsonArray totales = respuestaSIE.get("tazColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "TAZ");
                total.addProperty("idService", "7");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "TAZ");
                jsonObject.addProperty("idService", "7");
                jsonObject.addProperty("codigo", "TAZ");

                jsonObject.add("detalle", respuestaSIE.get("tazColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.captacionSaldoPlazo && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("saldoCaptacionPlazo").getAsJsonObject()
                        .get("totales").getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Saldo Captacion Plazo");
                total.addProperty("idService", "9");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Saldo Captacion Plazo");
                jsonObject.addProperty("idService", "9");
                jsonObject.addProperty("codigo", "GUARDADITO");
                jsonObject.add("detalle", respuestaSIE.get("saldoCaptacionPlazo").getAsJsonObject());

                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.captacionSaldoVista && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("saldoCaptacionVista").getAsJsonObject()
                        .get("totales").getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Saldo Captacion Vista");
                total.addProperty("idService", "8");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Saldo Captacion Vista");
                jsonObject.addProperty("idService", "8");
                jsonObject.addProperty("codigo", "INVERSIONES");
                jsonObject.add("detalle", respuestaSIE.get("saldoCaptacionVista").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (rubro == rubrosNegocio.carteraNormalidad && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("normalidad").getAsJsonObject()
                        .get("Vigente De 0 A 1").getAsJsonArray();
                String indicador = totales.get(2).getAsJsonObject().get("porcentaje").getAsString();

                String color = totales.get(0).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Normalidad");
                total.addProperty("idService", "15");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Normalidad");
                jsonObject.addProperty("idService", "15");
                jsonObject.addProperty("codigo", "NORMALIDAD");
                jsonObject.add("detalle", respuestaSIE.get("normalidad").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.negocioPagoServicios && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("pagosServicios").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Pago de Servicios");
                total.addProperty("idService", "19");
                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Pago de Servicios");
                jsonObject.addProperty("idService", "19");
                jsonObject.addProperty("codigo", "PAGO_SERVICIOS");
                jsonObject.add("detalle", respuestaSIE.get("pagosServicios").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.negocioTransferenciaInter && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("transferenciasInternacionales")
                        .getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Transferencias
                // Internacionales");
                total.addProperty("idService", "18");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Transferencias
                // Internacionales");
                jsonObject.addProperty("idService", "18");
                jsonObject.add("detalle", respuestaSIE
                        .get("transferenciasInternacionales").getAsJsonArray());
                jsonObject.addProperty("codigo", "TRANS_INTERNACIONALES");
                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.negocioTransferenciaDEX && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("transferenciasDEX").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Dinero Express");
                total.addProperty("idService", "17");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Dinero Express");
                jsonObject.addProperty("idService", "17");
                jsonObject.addProperty("codigo", "DINERO EXPRESS");
                jsonObject.add("detalle", respuestaSIE.get("transferenciasDEX").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (rubro == rubrosNegocio.negocioCambios && respuestaSIE != null) {

                JsonArray totales = respuestaSIE.get("transferenciasCambios").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Cambios Divisa");
                total.addProperty("idService", "20");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Cambios Divisa");
                jsonObject.addProperty("idService", "20");
                jsonObject.add("detalle", respuestaSIE.get("transferenciasCambios").getAsJsonArray());
                jsonObject.addProperty("codigo", "CAMBIOS");
                detalles.add(jsonObject);

            }

            if (rubro == rubrosNegocio.contribucion && respuestaSIE != null) {

                String crecimientoSrt = respuestaSIE.get("contribucion").getAsJsonObject()
                        .get("crecimiento").getAsString();
                double crecimiento = Double.parseDouble(crecimientoSrt);

                int percent = (int) Math.round(crecimiento);

                String indicador = "" + percent + "%";
                String color = percent >= 1 ? "verde" : "rojo";
                // String color =
                // contribucion.get("contribucion").getAsJsonObject().get("imagen").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Contribucion");
                total.addProperty("idService", "1");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Contribucion");
                jsonObject.addProperty("idService", "1");
                jsonObject.add("detalle", respuestaSIE.get("contribucion").getAsJsonObject());
                jsonObject.addProperty("codigo", "CONTRIBUCION");
                detalles.add(jsonObject);
            }

            if (rubro == rubrosNegocio.solicitudesPendientesSurtir && respuestaSIE != null) {

                JsonObject total = null;

                // Pendientes por surtir
                int pendSurtidas = Integer.parseInt(respuestaSIE.get("pendientesSurtrir")
                        .getAsJsonObject().get("pendientesSurtir").getAsString());
                String indicador = "" + pendSurtidas;
                String color = pendSurtidas > 0 ? "rojo" : "verde";

                total = new JsonObject();
                // total.addProperty("idService", "Pendientes por Surtir");
                total.addProperty("idService", "14");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Pendientes por Surtir");
                jsonObject.addProperty("idService", "14");
                jsonObject.addProperty("codigo", "PENDIENTES_SURTIR");
                jsonObject.add("detalle",
                        respuestaSIE.get("pendientesSurtrir").getAsJsonObject());

                detalles.add(jsonObject);

                // Nunca Abonados
                int nuncaAbondas = Integer.parseInt(respuestaSIE.get("nuncaAbonadas")
                        .getAsJsonObject().get("noAbonadas").getAsString());
                int totalCuentas = Integer.parseInt(respuestaSIE.get("nuncaAbonadas")
                        .getAsJsonObject().get("totalCuentas").getAsString());
                int percent = 0;

                if (totalCuentas > 0) {
                    percent = nuncaAbondas / totalCuentas;
                }

                indicador = "" + percent + "%";
                color = percent >= 3 ? "verde" : "rojo";

                total = new JsonObject();
                // total.addProperty("idService", "Nunca Abonadas");
                total.addProperty("idService", "16");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Nunca abonadas");
                jsonObject.addProperty("idService", "16");
                jsonObject.addProperty("codigo", "noabo");
                jsonObject.add("detalle", respuestaSIE.get("nuncaAbonadas").getAsJsonObject());

                detalles.add(jsonObject);

            }

            // SEGUROS AGREGAR 2 MAS
            if (rubro == rubrosNegocio.segurosNoLigados && respuestaSIE != null) {

                JsonObject totalSegurosNoLigados = respuestaSIE;

                logger.info(" RESPUESTA DE SEGUROS NO LIGADOS" + totalSegurosNoLigados);

                // JsonArray totales =
                // segurosNoLigados.getRespuesta().get("segurosNoLigados").getAsJsonArray().get(7).getAsJsonArray();

                /*
                 * String indicador =
                 * totales.get(6).getAsJsonObject().get("porcentaje").getAsString(); String
                 * color = totales.get(6).getAsJsonObject().get("color").getAsString();
                 * JsonObject total = new JsonObject(); // total.addProperty("idService",
                 * "Cambios Divisa"); total.addProperty("idService", "20");
                 *
                 * // total.addProperty("indicador", color.equals("rojo") ? "-" + // indicador :
                 * indicador); // total.addProperty("color", color);
                 *
                 * total.addProperty("indicador", indicador); total.addProperty("color", color);
                 *
                 * indicadores.add(total); JsonObject jsonObject = new JsonObject(); //
                 * jsonObject.addProperty("idService", "Cambios Divisa");
                 * jsonObject.addProperty("idService", "20"); jsonObject.add("detalle",
                 * cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray());
                 *
                 * detalles.add(jsonObject);
                 */
            }

            if (rubro == rubrosNegocio.segurosPrestamos && respuestaSIE != null) {

                JsonObject totalSegurosPrestamos = respuestaSIE;

                logger.info(" RESPUESTA DE SEGUROS PRESTAMOS" + totalSegurosPrestamos);

                // JsonArray totales =
                // segurosNoLigados.getRespuesta().get("segurosNoLigados").getAsJsonArray().get(7).getAsJsonArray();

                /*
                 * String indicador =
                 * totales.get(6).getAsJsonObject().get("porcentaje").getAsString(); String
                 * color = totales.get(6).getAsJsonObject().get("color").getAsString();
                 * JsonObject total = new JsonObject(); // total.addProperty("idService",
                 * "Cambios Divisa"); total.addProperty("idService", "20");
                 *
                 * // total.addProperty("indicador", color.equals("rojo") ? "-" + // indicador :
                 * indicador); // total.addProperty("color", color);
                 *
                 * total.addProperty("indicador", indicador); total.addProperty("color", color);
                 *
                 * indicadores.add(total); JsonObject jsonObject = new JsonObject(); //
                 * jsonObject.addProperty("idService", "Cambios Divisa");
                 * jsonObject.addProperty("idService", "20"); jsonObject.add("detalle",
                 * cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray());
                 *
                 * detalles.add(jsonObject);
                 */
            }

            if (rubro == rubrosNegocio.segurosRenovaciones && respuestaSIE != null) {

                JsonObject totalSegurosRenovaciones = respuestaSIE;

                logger.info(" RESPUESTA DE SEGUROS PRESTAMOS" + totalSegurosRenovaciones);

                // JsonArray totales =
                // segurosNoLigados.getRespuesta().get("segurosNoLigados").getAsJsonArray().get(7).getAsJsonArray();

                /*
                 * String indicador =
                 * totales.get(6).getAsJsonObject().get("porcentaje").getAsString(); String
                 * color = totales.get(6).getAsJsonObject().get("color").getAsString();
                 * JsonObject total = new JsonObject(); // total.addProperty("idService",
                 * "Cambios Divisa"); total.addProperty("idService", "20");
                 *
                 * // total.addProperty("indicador", color.equals("rojo") ? "-" + // indicador :
                 * indicador); // total.addProperty("color", color);
                 *
                 * total.addProperty("indicador", indicador); total.addProperty("color", color);
                 *
                 * indicadores.add(total); JsonObject jsonObject = new JsonObject(); //
                 * jsonObject.addProperty("idService", "Cambios Divisa");
                 * jsonObject.addProperty("idService", "20"); jsonObject.add("detalle",
                 * cambios.getRespuesta().get("transferenciasCambios").getAsJsonArray());
                 *
                 * detalles.add(jsonObject);
                 */
            }

            jsonGeneral.add("indicadores", indicadores);
            jsonGeneral.add("detalles", detalles);

        } catch (Exception e) {
            logger.info("Ap en el servicio getIndicadores() ");
            jsonGeneral = null;
        }

        return jsonGeneral;

    }

}
