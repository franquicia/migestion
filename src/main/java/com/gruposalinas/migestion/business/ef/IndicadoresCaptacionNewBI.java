package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class IndicadoresCaptacionNewBI {

    private Logger logger = LogManager.getLogger(IndicadoresCaptacionNewBI.class);

    /**
     * @param tipoCaptacion 1= Captacion Vista 2= Captacion Plazo
     *
     */
    public JsonObject getCaptaciones(String fecha, String geografia, String nivel, String tipoSaldo, int tipoCaptacion) {

        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        String wsName = "";
        String nombrePropiedad = "";

        try {
            logger.info("Fecha :" + fecha);
            logger.info("Geografia :" + geografia);
            logger.info("Nivel :" + nivel);
            logger.info("Tipo Saldo :" + tipoSaldo);

            if (tipoCaptacion == 1) {
                nombrePropiedad = "captacionVista";
                wsName = "SaldoCaptacionVistaMetrica";
            } else {
                nombrePropiedad = "captacionPlazo";
                wsName = "SaldoCaptacionPlazoMetrica";
            }

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, tipoSaldo, wsName);
            logger.info(xmlStr);

            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0};

            json = limpiXmlNewCaptaciones(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap Captaciones");
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add(nombrePropiedad + "_" + tipoSaldo, json);
        }

        return respuestaService;
    }

    public String obtieneXmlResult(String fecha, String geografia, String nivel, String tipoSaldo, String service) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        String strService = service;
        String cadena = "";

        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");

            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>" + "<siew:" + strService + ">"
                    + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "<siew:nivel>" + nivel + "</siew:nivel>"
                    + "<siew:metrica>" + tipoSaldo + "</siew:metrica>"
                    + "</siew:" + strService + ">" + "</soapenv:Body>" + "</soapenv:Envelope>";

            logger.info("PETICION SOA: " + xml);

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            Element rootNode2 = document.getRootElement();

            Element body2 = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response2 = body.getChild(strService + "Response",
                    Namespace.getNamespace("http://siewebservices/"));
            Element result2 = response.getChild(strService + "Result",
                    Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap obtieneXmlResult");
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return cadenaXml;

    }

    protected JsonArray limpiXmlNewCaptaciones(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            // Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            // Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples",
                            Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples",
                            Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            // logger.info("Columnas :"+columnas);
            // logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                // if(veces <= columnas){
                if (celda1 == celda2) {
                    // logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata)
                            .getChild("FmtValue",
                                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"))
                            .getValue();
                    // logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata)
                            .getChild("FmtValue",
                                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"))
                            .getValue());
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;

                    if (conCelldata < celdas.size()) {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                // }else{
                if (veces == columnas) {
                    // logger.info("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                // logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                // veces=1;
                /*
                 * arrayFilas.add(arrayValores); arrayValores = new JsonArray();
                 */

                // }

                /*
                 * if(cont == totalCeldas){ arrayFilas.add(arrayValores); }
                 */
            }

            // logger.info(arrayFilas.toString());
            // logger.info("Valores celda lista : " +listValoresCeldas.size());
            // logger.info("Valores totales lista : "
            // +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            jsonArray = armaJsonNewCaptaciones(valores, flagsColumnas);

        } catch (Exception e) {
            //logger.info("DETALLE LIMPIA XML" + e.getMessage());
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonNewCaptaciones(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;

        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 5) {

                    logger.info("cantidad real: " + formateador.format(valoresCeldas.get(i)));
                    logger.info("Cantidad vs " + formateador.format(valoresCeldas.get((i) - pos_vs)));

                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;

                    pos_vs += 3;
                } else {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

            }

            valor = new JsonObject();
            valor.addProperty("cantidad", valorStr);
            valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
            valor.addProperty("color", color);

            arrayValores.add(valor);

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /* Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 5) {
                    logger.info("cantidad real: " + formateador.format(valoresTotales.get(a)));
                    logger.info("Cantidad vs " + formateador.format(valoresTotales.get(a - pos_vsTot)));
                    porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                    logger.info("" + (int) Math.round(porcentajeTot) + "%");
                    pos_vsTot += 3;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

            }

            jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
            jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
            jsonTotales.addProperty("color", color);
            arrayValores.add(jsonTotales);

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

    public JsonObject getDineroExpress(String fecha, String geografia, String nivel) {

        logger.info("---------Dinero Express------------");

        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {
            logger.info("Fecha DEX:" + fecha);
            logger.info("Geografia DEX:" + geografia);
            logger.info("Nivel DEX:" + nivel);

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TranferenciasDEX");
            logger.info(xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap TranferenciasDEX");
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasDEX", json);
        }

        return respuestaService;
    }

    public String obtieneXmlResult(String fecha, String geografia, String nivel, String service) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        String strService = service;
        String cadena = "";

        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");

            //			System.setProperty("java.net.useSystemProxies", "true");
            //			System.setProperty("http.proxyHost", "10.50.8.20");
            //			System.setProperty("http.proxyPort", "8080");
            //			// System.setProperty("http.proxyUser", "B196228");
            //			// System.setProperty("http.proxyPassword", "Bancoazteca2345");
            //			System.setProperty("http.proxySet", "true");
            //			System.setProperty("https.proxyHost", "10.50.8.20");
            //			System.setProperty("https.proxyPort", "8080");
            //			// System.setProperty("https.proxyUser", "B196228");
            //			// System.setProperty("https.proxyPassword", "Bancoazteca2345");
            //			System.setProperty("https.proxySet", "true");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<siew:" + strService + ">"
                    + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "<siew:nivel>" + nivel + "</siew:nivel>"
                    + "</siew:" + strService + ">"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            //logger.info("PETICION SOA: "+xml);
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            Element rootNode2 = document.getRootElement();

            Element body2 = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response2 = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result2 = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap para llamado de " + service + " Exception :" + e);
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return cadenaXml;

    }

    protected JsonArray limpiXmlNew(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            //			logger.info("Axes: "+axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            //logger.info("Columnas :"+columnas);
            //logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            //logger.info("Total GRID: "+ totalCeldas);
            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
                    //					    logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                    //						logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue());
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
                    //						logger.info("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                //logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //logger.info(arrayFilas.toString());
            //logger.info("Valores celda lista : " +listValoresCeldas.size());
            //logger.info("Valores totales lista : " +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            jsonArray = armaJsonNew(valores, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap en limpiXmlNew()" + e);
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonNew(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 4) {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                    //logger.info("cantidad: "+ valoresCeldas.get(i));
                    //logger.info("Cantidad vs"+ valoresCeldas.get( i - pos_vs ));
                    pos_vs += 3;
                } else {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

            }

            valor = new JsonObject();
            valor.addProperty("cantidad", valorStr);
            valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
            valor.addProperty("color", color);

            arrayValores.add(valor);

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                //				logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /*Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 4) {
                    porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                    pos_vsTot += 3;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

            }

            jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
            jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
            jsonTotales.addProperty("color", color);
            arrayValores.add(jsonTotales);

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }
}
