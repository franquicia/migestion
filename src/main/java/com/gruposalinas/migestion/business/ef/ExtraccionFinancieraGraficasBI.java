package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.domain.AcumuladosSemana;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class ExtraccionFinancieraGraficasBI {

    private final Logger logger = LogManager.getLogger(ExtraccionFinancieraGraficasBI.class);

    private String fecha;
    private String nivel;
    private String geografia;
    private int idIndicador;

    public String getData() {

        JsonArray jsonRespuesta = null;

        /*this.fecha = "20180802";
         this.nivel = "TR";
         this.geografia = "236737 - TERRITORIAL CENTRO";
         this.idIndicador = 5;*/
        String xmlRespuesta = obtieneXmlResult();

        Map< String, Object> secciones = splitRealPlan(xmlRespuesta);

        boolean error = (boolean) secciones.get("error");

        //Si se pudo realizar la separacion de los XML (Seccion_Real y Seccion_Plan)
        if (!error) {

            jsonRespuesta = procesaInfo(secciones);

        }
        String respuesta = "";

        if (jsonRespuesta != null) {
            respuesta = jsonRespuesta.toString();
        } else {
            respuesta = "[]";
        }

        //System.out.println("{\"dataGraficas\":"+respuesta+"}");
        return "{\"dataGraficas\":" + respuesta + "}";

    }

    public String obtieneXmlResult() {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        String strService = getMethodWS(this.idIndicador);
        String cadena = "";

        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");

            // System.setProperty("java.net.useSystemProxies", "true");
            // System.setProperty("http.proxyHost", "10.50.8.20");
            // System.setProperty("http.proxyPort", "8080");
            // // System.setProperty("http.proxyUser", "B196228");
            // // System.setProperty("http.proxyPassword", "Bancoazteca2345");
            // System.setProperty("http.proxySet", "true");
            // System.setProperty("https.proxyHost", "10.50.8.20");
            // System.setProperty("https.proxyPort", "8080");
            // // System.setProperty("https.proxyUser", "B196228");
            // // System.setProperty("https.proxyPassword", "Bancoazteca2345");
            // System.setProperty("https.proxySet", "true");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            //rc.setConnectTimeout(30000);//Produccion
            rc.setConnectTimeout(30000);
            rc.setReadTimeout(30000);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>" + "<soapenv:Body>" + "<siew:" + strService + ">" + "<siew:fecha>" + this.fecha
                    + "</siew:fecha>" + "<siew:geografia>" + this.geografia + "</siew:geografia>" + "<siew:nivel>" + this.nivel
                    + "</siew:nivel>" + "</siew:" + strService + ">" + "</soapenv:Body>" + "</soapenv:Envelope>";

            //logger.info("PETICION SOA: " + xml);
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            Element rootNode2 = document.getRootElement();

            Element body2 = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response2 = body.getChild(strService + "Response",
                    Namespace.getNamespace("http://siewebservices/"));
            Element result2 = response.getChild(strService + "Result",
                    Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap obtieneXmlResult");
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return cadenaXml;
    }

    private String getMethodWS(int indicador) {

        String methodWS = "";

        switch (indicador) {

            case 5:
                methodWS = "GraficaColocacionPersonales";
                break;
            case 6:
                methodWS = "GraficaColocacionTotalConsumo";
                break;
            case 7:
                methodWS = "GraficaColocacionTAZ";
                break;
            case 19:
                methodWS = "TransferenciasPagoServicios";
                break;
        }

        return methodWS;
    }

    private Map<String, Object> splitRealPlan(String cadena) {

        Map<String, Object> secciones = new HashMap<>();

        try {
            String str_real = cadena.split("Seccion_Real")[1].split("Seccion_Plan")[0];
            String str_plan = cadena.split("Seccion_Real")[1].split("Seccion_Plan")[1];

            secciones.put("seccion_real", str_real);
            secciones.put("seccion_plan", str_plan);

            secciones.put("error", false);

        } catch (Exception e) {
            logger.info("Ap. Metodo splitRealPlan() " + e.getMessage());
            secciones.put("error", true);
        }

        return secciones;

    }

    private JsonArray procesaInfo(Map<String, Object> secciones) {

        Map<String, Object> realData = null;
        Map<String, Object> planData = null;
        JsonArray jsonArmado = null;

        try {

            realData = getInfoComplete(secciones.get("seccion_real").toString());
            planData = getInfoComplete(secciones.get("seccion_plan").toString());

            boolean errorReal = (boolean) realData.get("error");
            boolean errorPlan = (boolean) planData.get("error");

            String[][] tablaReal = null;
            if (!errorReal) {
                tablaReal = generaArray(realData);
            }

            String[][] tablaPlan = null;
            if (!errorPlan) {
                tablaPlan = generaArray(planData);
            }

            List<AcumuladosSemana> acumuladosGraficas = getAcumulados(tablaReal, (int) realData.get("columnas"), (int) realData.get("filas"), tablaPlan, (int) planData.get("columnas"), (int) planData.get("filas"));

            jsonArmado = armaJson(acumuladosGraficas);

        } catch (Exception e) {
            jsonArmado = null;
            logger.info("Ap. Metodo procesaInfo() " + e.getMessage());
            e.printStackTrace();
        }

        return jsonArmado;
    }

    private Map<String, Object> getInfoComplete(String xmlStr) {

        Map<String, Object> infoSeccion = new HashMap<>();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            //logger.info("Axes: "+axes0.size());

            int columnas = 0;
            int filas = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    filas = rows.size();
                }
            }

            infoSeccion.put("filas", filas);
            infoSeccion.put("columnas", columnas);
            infoSeccion.put("grid", celdas);

            //System.out.println("Columnas :"+columnas);
            //System.out.println("Filas :"+ filas);
            int totalCeldas = columnas * filas;

            //System.out.println("Total GRID: "+ totalCeldas);
            infoSeccion.put("error", false);
        } catch (Exception e) {
            infoSeccion.put("error", true);
        }

        //logger.info("Total GRID: "+ totalCeldas);
        return infoSeccion;

    }

    private String[][] generaArray(Map<String, Object> seccionData) throws Exception {

        int filas = (int) seccionData.get("filas");
        int columnas = (int) seccionData.get("columnas");

        int totalCeldas = columnas * filas;

        String[][] arrayData = new String[filas][columnas];

        List<Element> grid = (List<Element>) seccionData.get("grid");

        int cont = 0;

        for (int i = 0; i < filas; i++) { //Servira para llenar filas
            for (int j = 0; j < columnas; j++) {//Servira para llenar las columnas
                String valorCelda = grid.get(cont).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                arrayData[i][j] = valorCelda;
                //System.out.println(String.format("[%d][%d]:%s", i,j,valorCelda) );
                cont++;
            }
        }

        return arrayData;

    }

    public List<AcumuladosSemana> getAcumulados(String[][] tablaReal, int columnasReal, int filasReal, String[][] tablaPlan, int columnasPlan, int filasPlan) {

        int diasSumar = validaDia();
        int semanaDelAnio = semanaActual();

        boolean comparaSemana = false;

        long realAlDia = 0;
        long compromisoAlDia = 0;
        long compromisoSemana = 0;
        int veces = 1;

        List<AcumuladosSemana> semanas = new ArrayList<>();

        AcumuladosSemana semana = null;
        for (int i = 0; i < columnasReal; i++) {

            semana = new AcumuladosSemana();
            realAlDia = 0;
            compromisoAlDia = 0;
            compromisoSemana = 0;
            veces = 1;

            for (int j = 0; j < filasReal; j++) {

                //System.out.println(String.format("[%d][%d]: %s",j,i,tablaReal[j][i]));
                if (j == 0) {
                    int semanaColumna = Integer.parseInt(tablaReal[j][i]);

                    if (semanaColumna == 1) {
                        comparaSemana = true;
                    }

                    if (comparaSemana && semanaColumna == semanaDelAnio) {
                        semana.setSemanaActual(true);
                    }

                    semana.setSemana(semanaColumna);

                } else if (j == 11) {
                    semana.setRealAnioPasado(Math.round(Double.valueOf(tablaReal[j][i])));
                } else if (j == 8) {
                    semana.setTotalRealSemana(Math.round(Double.valueOf(tablaReal[j][i])));
                } else if (j != 9 & j != 10) {
                    if (veces <= diasSumar) {
                        realAlDia += Double.valueOf(tablaReal[j][i]);
                        veces++;
                    }
                }

            }
            semana.setAcumuladoAlDia(realAlDia);

            veces = 1;
            for (int j = 0; j < filasPlan; j++) {

                if (j == 0) {
                    compromisoSemana = Math.round(Double.valueOf(tablaPlan[j][i]));
                } else if (j != 8 & j != 9) {
                    if (veces <= diasSumar) {
                        compromisoAlDia += Math.round(Double.valueOf(tablaPlan[j][i]));
                        veces++;
                    }
                }
                //System.out.println(String.format("[%d][%d]: %s",j,i,tablaPlan[j][i]));

            }
            semana.setTotalCompromisoAlDia(compromisoAlDia);
            semana.setTotalCompromisoSemana(compromisoSemana);
            semanas.add(semana);
        }

        return semanas;
    }

    private JsonArray armaJson(List<AcumuladosSemana> semanas) {

        JsonArray respuesta = new JsonArray();
        JsonObject semanaJson = null;

        for (AcumuladosSemana semana : semanas) {
            semanaJson = new JsonObject();
            semanaJson.addProperty("semana", semana.getSemana());
            semanaJson.addProperty("realAlDia", semana.getAcumuladoAlDia());
            semanaJson.addProperty("compromisoAlDia", semana.getTotalCompromisoAlDia());
            semanaJson.addProperty("realSemana", semana.getTotalRealSemana());
            semanaJson.addProperty("compromisoSemana", semana.getTotalCompromisoSemana());
            semanaJson.addProperty("anioPasado", semana.getRealAnioPasado());
            semanaJson.addProperty("isSemanaActual", semana.isSemanaActual());
            respuesta.add(semanaJson);

        }

        return respuesta;

    }

    public int validaDia() {

        int totalDiasSumar = 0;

        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

            Date date = df.parse(this.fecha);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int diaSemana = cal.get(Calendar.DAY_OF_WEEK);

            switch (diaSemana) {
                case Calendar.MONDAY:
                    totalDiasSumar = 7;
                    break;
                case Calendar.TUESDAY:
                    totalDiasSumar = 1;
                    break;
                case Calendar.WEDNESDAY:
                    totalDiasSumar = 2;
                    break;
                case Calendar.THURSDAY:
                    totalDiasSumar = 3;
                    break;
                case Calendar.FRIDAY:
                    totalDiasSumar = 4;
                    break;
                case Calendar.SATURDAY:
                    totalDiasSumar = 5;
                    break;
                case Calendar.SUNDAY:
                    totalDiasSumar = 6;
                    break;
            }

        } catch (ParseException e) {

            e.printStackTrace();
        }

        return totalDiasSumar;

    }

    public int semanaActual() {

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        Date date;

        int semana = 0;

        try {
            date = df.parse(this.fecha);
            Calendar cal = Calendar.getInstance();
            cal.setFirstDayOfWeek(Calendar.MONDAY);
            cal.setMinimalDaysInFirstWeek(4);
            cal.setTime(date);

            semana = cal.get(Calendar.WEEK_OF_YEAR);

            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                semana = semana - 1;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return semana;

    }

    public static void main(String[] args) {

        ExtraccionFinancieraGraficasBI ejemplo = new ExtraccionFinancieraGraficasBI();
        ejemplo.getData();

    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public void setGeografia(String geografia) {
        this.geografia = geografia;
    }

    public void setIdIndicador(int idIndicador) {
        this.idIndicador = idIndicador;
    }

}
