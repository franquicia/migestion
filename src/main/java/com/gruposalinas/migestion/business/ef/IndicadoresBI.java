package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class IndicadoresBI {

    private Logger logger = LogManager.getLogger(IndicadoresBI.class);

    public JsonObject getIndicadores(String fecha, String geografia, String nivel) {

        JsonObject jsonGeneral = new JsonObject();

        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();
        try {

            long inicio = System.currentTimeMillis();
            JsonObject consumoColocacion = getConsumoColocacion(fecha, geografia, nivel);
            JsonObject personalesColocacion = getPersonalesColocacion(fecha, geografia, nivel);
            JsonObject tazColocacion = getTAZColocacion(fecha, geografia, nivel);
            JsonObject saldoCaptacionPlazo = getSaldoCaptacionPlazo(fecha, geografia, nivel);
            JsonObject saldocaptacionVista = getSaldoCaptacionVista(fecha, geografia, nivel);
            JsonObject normalidad = getNormalidad(fecha, geografia, nivel);
            JsonObject pagosServicios = getPagosDeServicio(fecha, geografia, nivel);
            JsonObject transferenciasInternacionales = getTransferciasInternacionales(fecha, geografia, nivel);
            JsonObject transferenciasDEX = getDineroExpress(fecha, geografia, nivel);
            JsonObject transferenciasCambios = getCambios(fecha, geografia, nivel);
            JsonObject contribucion = getContribucion(fecha, geografia, nivel);
            JsonObject noAbonadasPend = getNoAbonadasPend(fecha, geografia, nivel);

            long fin = System.currentTimeMillis();

            long totaTiempo = (fin - inicio) / 1000;

            logger.info("Tiempo de ejecucion : " + totaTiempo + " Segundos");

            if (consumoColocacion != null) {

                JsonArray totales = consumoColocacion.get("consumoColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", "6");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", "6");
                jsonObject.addProperty("codigo", "cons");
                jsonObject.add("detalle", consumoColocacion.get("consumoColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (personalesColocacion != null) {

                JsonArray totales = personalesColocacion.get("personalesColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Personales");
                total.addProperty("idService", "5");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Personales");
                jsonObject.addProperty("idService", "5");
                jsonObject.add("detalle", personalesColocacion.get("personalesColocacion").getAsJsonObject());

                detalles.add(jsonObject);
                ;
            }

            if (tazColocacion != null) {
                JsonArray totales = tazColocacion.get("tazColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "TAZ");
                total.addProperty("idService", "7");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "TAZ");
                jsonObject.addProperty("idService", "7");
                jsonObject.addProperty("codigo", "taz");
                jsonObject.add("detalle", tazColocacion.get("tazColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (saldoCaptacionPlazo != null) {

                JsonArray totales = saldoCaptacionPlazo.get("saldoCaptacionPlazo").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Saldo Captacion Plazo");
                total.addProperty("idService", "9");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Saldo Captacion Plazo");
                jsonObject.addProperty("idService", "9");

                jsonObject.add("detalle", saldoCaptacionPlazo.get("saldoCaptacionPlazo").getAsJsonObject());

                detalles.add(jsonObject);

            }
            if (saldocaptacionVista != null) {

                JsonArray totales = saldocaptacionVista.get("saldoCaptacionVista").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Saldo Captacion Vista");
                total.addProperty("idService", "8");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Saldo Captacion Vista");
                jsonObject.addProperty("idService", "8");

                jsonObject.add("detalle", saldocaptacionVista.get("saldoCaptacionVista").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (normalidad != null) {

                JsonArray totales = normalidad.get("normalidad").getAsJsonObject().get("Vigente De 0 A 1")
                        .getAsJsonArray();
                String indicador = totales.get(2).getAsJsonObject().get("porcentaje").getAsString();

                String color = totales.get(0).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Normalidad");
                total.addProperty("idService", "15");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Normalidad");
                jsonObject.addProperty("idService", "15");

                jsonObject.add("detalle", normalidad.get("normalidad").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (pagosServicios != null) {

                JsonArray totales = pagosServicios.get("pagosServicios").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Pago de Servicios");
                total.addProperty("idService", "19");
                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Pago de Servicios");
                jsonObject.addProperty("idService", "19");

                jsonObject.add("detalle", pagosServicios.get("pagosServicios").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (transferenciasInternacionales != null) {

                JsonArray totales = transferenciasInternacionales.get("transferenciasInternacionales").getAsJsonArray()
                        .get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Transferencias
                // Internacionales");
                total.addProperty("idService", "18");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Transferencias
                // Internacionales");
                jsonObject.addProperty("idService", "18");
                jsonObject.add("detalle",
                        transferenciasInternacionales.get("transferenciasInternacionales").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (transferenciasDEX != null) {

                JsonArray totales = transferenciasDEX.get("transferenciasDEX").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Dinero Express");
                total.addProperty("idService", "17");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Dinero Express");
                jsonObject.addProperty("idService", "17");

                jsonObject.add("detalle", transferenciasDEX.get("transferenciasDEX").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (transferenciasCambios != null) {

                JsonArray totales = transferenciasCambios.get("transferenciasCambios").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Cambios Divisa");
                total.addProperty("idService", "20");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Cambios Divisa");
                jsonObject.addProperty("idService", "20");
                jsonObject.add("detalle", transferenciasCambios.get("transferenciasCambios").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (contribucion != null) {

                String crecimientoSrt = contribucion.get("contribucion").getAsJsonObject().get("crecimiento")
                        .getAsString();
                double crecimiento = Double.parseDouble(crecimientoSrt);

                int percent = (int) Math.round(crecimiento);

                String indicador = "" + percent + "%";
                String color = percent >= 1 ? "verde" : "rojo";
                // String color =
                // contribucion.get("contribucion").getAsJsonObject().get("imagen").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Contribucion");
                total.addProperty("idService", "1");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Contribucion");
                jsonObject.addProperty("idService", "1");
                jsonObject.add("detalle", contribucion.get("contribucion").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (noAbonadasPend != null) {

                JsonObject total = null;

                // Pendientes por surtir
                int pendSurtidas = Integer.parseInt(noAbonadasPend.get("pendientesSurtrir").getAsJsonObject()
                        .get("pendientesSurtir").getAsString());
                String indicador = "" + pendSurtidas;
                String color = pendSurtidas > 0 ? "rojo" : "verde";

                total = new JsonObject();
                // total.addProperty("idService", "Pendientes por Surtir");
                total.addProperty("idService", "14");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Pendientes por Surtir");
                jsonObject.addProperty("idService", "14");

                jsonObject.add("detalle", noAbonadasPend.get("pendientesSurtrir").getAsJsonObject());

                detalles.add(jsonObject);

                // Nunca Abonados
                int nuncaAbondas = Integer.parseInt(
                        noAbonadasPend.get("nuncaAbonadas").getAsJsonObject().get("noAbonadas").getAsString());
                int totalCuentas = Integer.parseInt(
                        noAbonadasPend.get("nuncaAbonadas").getAsJsonObject().get("totalCuentas").getAsString());
                int percent = 0;

                if (totalCuentas > 0) {
                    percent = nuncaAbondas / totalCuentas;
                }

                indicador = "" + percent + "%";
                color = percent >= 3 ? "verde" : "rojo";

                total = new JsonObject();
                // total.addProperty("idService", "Nunca Abonadas");
                total.addProperty("idService", "16");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Nunca abonadas");
                jsonObject.addProperty("idService", "16");
                jsonObject.addProperty("codigo", "noabo");
                jsonObject.add("detalle", noAbonadasPend.get("nuncaAbonadas").getAsJsonObject());

                detalles.add(jsonObject);

            }

        } catch (Exception e) {
            logger.info("Ap getIndicadores");
            return null;
        }
        jsonGeneral.add("indicadores", indicadores);
        jsonGeneral.add("detalles", detalles);

        return jsonGeneral;

    }

    public String obtieneXmlResult(String fecha, String geografia, String nivel, String service) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        String strService = service;
        String cadena = "";

        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");

            // System.setProperty("java.net.useSystemProxies", "true");
            // System.setProperty("http.proxyHost", "10.50.8.20");
            // System.setProperty("http.proxyPort", "8080");
            // // System.setProperty("http.proxyUser", "B196228");
            // // System.setProperty("http.proxyPassword", "Bancoazteca2345");
            // System.setProperty("http.proxySet", "true");
            // System.setProperty("https.proxyHost", "10.50.8.20");
            // System.setProperty("https.proxyPort", "8080");
            // // System.setProperty("https.proxyUser", "B196228");
            // // System.setProperty("https.proxyPassword", "Bancoazteca2345");
            // System.setProperty("https.proxySet", "true");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            //rc.setConnectTimeout(30000);//Produccion
            rc.setConnectTimeout(30000);
            rc.setReadTimeout(30000);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>" + "<soapenv:Body>" + "<siew:" + strService + ">" + "<siew:fecha>" + fecha
                    + "</siew:fecha>" + "<siew:geografia>" + geografia + "</siew:geografia>" + "<siew:nivel>" + nivel
                    + "</siew:nivel>" + "</siew:" + strService + ">" + "</soapenv:Body>" + "</soapenv:Envelope>";

            //logger.info("PETICION SOA: " + xml);
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            Element rootNode2 = document.getRootElement();

            Element body2 = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response2 = body.getChild(strService + "Response",
                    Namespace.getNamespace("http://siewebservices/"));
            Element result2 = response.getChild(strService + "Result",
                    Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap en IndicadoresBI obtieneXmlResult " + "service: " + service + ", fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel);
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return cadenaXml;

    }

    public String obtieneXmlResult(String fecha, String geografia, String nivel, String tipoSaldo, String service) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        String strService = service;
        String cadena = "";

        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");

            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            //rc.setConnectTimeout(30000);//Produccion
            rc.setConnectTimeout(30000);
            rc.setReadTimeout(30000);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>" + "<siew:" + strService + ">"
                    + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "<siew:nivel>" + nivel + "</siew:nivel>"
                    + "<siew:metrica>" + tipoSaldo + "</siew:metrica>"
                    + "</siew:" + strService + ">" + "</soapenv:Body>" + "</soapenv:Envelope>";

            //logger.info("PETICION SOA: " + xml);
            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            Element rootNode2 = document.getRootElement();

            Element body2 = rootNode.getChild("Body",
                    Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response2 = body.getChild(strService + "Response",
                    Namespace.getNamespace("http://siewebservices/"));
            Element result2 = response.getChild(strService + "Result",
                    Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            logger.info("Ap en IndicadoresBI obtieneXmlResult saldo " + "service: " + service + ", fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", tipoSaldo: " + tipoSaldo);
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        return cadenaXml;

    }

    public JsonObject getConsumoColocacion(String fecha, String geografia, String nivel) {

        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "ConsumoColocacion");
            //logger.info("getConsumoColocacion " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            //json = clearXml(xmlStr);
            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
            jsonArray = limpiaXMLVersus(xmlStr, flagsColumnas);
            json = jsonDiasSemana(jsonArray);

            eliminaObjetos(json);

        } catch (Exception e) {
            logger.info("Ap getConsumoColocacion IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("consumoColocacion", json);
        }

        return respuestaService;

    }

    public JsonObject getPersonalesColocacion(String fecha, String geografia, String nivel) {

        //logger.info("---------PERSONALES------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            //logger.info("Fecha :" + fecha);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "PersonalesColocacion");
            //logger.info("getPersonalesColocacion " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            //json = clearXml(xmlStr);
            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
            jsonArray = limpiaXMLVersus(xmlStr, flagsColumnas);
            json = jsonDiasSemana(jsonArray);

        } catch (Exception e) {
            logger.info("Ap getPersonalesColocacion IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("personalesColocacion", json);
        }

        return respuestaService;

    }

    public JsonObject getTAZColocacion(String fecha, String geografia, String nivel) {

        //logger.info("---------TAZ------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            //logger.info("Fecha :" + fecha);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TAZColocacion");
            //logger.info("getTAZColocacion " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            //json = clearXml(xmlStr);
            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
            jsonArray = limpiaXMLVersus(xmlStr, flagsColumnas);
            json = jsonDiasSemana(jsonArray);

        } catch (Exception e) {
            logger.info("Ap getTAZColocacion IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("tazColocacion", json);
        }

        return respuestaService;

    }

    public JsonObject getSaldoCaptacionPlazo(String fecha, String geografia, String nivel) {

        //logger.info("---------CAPTACION PLAZO------------");
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            //logger.info("Fecha :" + fecha);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "SaldoCaptacionPlazo");
            //logger.info("getSaldoCaptacionPlazo " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            int[] flagsCaptacionPlazo = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiaXmlCaptacion(xmlStr, flagsCaptacionPlazo);

            //logger.info("json XMLResult: " + json);
        } catch (Exception e) {
            logger.info("Ap getSaldoCaptacionPlazo IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("saldoCaptacionPlazo", json);
        }

        return respuestaService;

    }

    public JsonObject getSaldoCaptacionVista(String fecha, String geografia, String nivel) {

        //logger.info("---------CAPTACION VISTA------------");
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            //logger.info("Fecha :" + fecha);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "SaldoCaptacionVista");
            //logger.info("getSaldoCaptacionVista " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            int[] flagsGuardadito = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiaXmlCaptacion(xmlStr, flagsGuardadito);

        } catch (Exception e) {
            logger.info("Ap getSaldoCaptacionVista IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("saldoCaptacionVista", json);
        }

        return respuestaService;

    }

    public JsonObject getNormalidad(String fecha, String geografia, String nivel) {

        //logger.info("---------NORMALIDAD------------");
        JsonObject json = null;
        JsonArray jsonp = null;
        JsonObject respuestaService = new JsonObject();

        JsonObject jsonFinal = null;

        try {

            DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
            Date date = formatDate.parse(fecha);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 1);

            //logger.info("Fecha para la normalidad : " + calendar.getTime());
            int dia = calendar.get(Calendar.DAY_OF_WEEK);

            if (dia == 1) {
                calendar.add(Calendar.DATE, -7);
            } else {
                calendar.add(Calendar.DATE, -(dia - 1));
            }

            String semana = formatDate.format(calendar.getTime());

            //logger.info("Fecha :" + semana);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(semana, geografia, nivel, "Normalidad");
            //logger.info("getNormalidad " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            if (!xmlStr.contains("Error")) {
                //logger.info(xmlStr);
                System.out.println(xmlStr);
            }

            int[] flagsColumnas = {1, 1, 1, 1, 0, 1, 0, 0, 0, 0};
            jsonp = limpiXmlNew(xmlStr, flagsColumnas);

            String arrayNombres[] = {"Vigente De 0 A 1", "Temprana 2", "Atrasada 1a De 3 A 7", "Tardia 1b De 8 A 13",
                "Atrasada 2 A 13", "Vencida 2a De 14 A 25", "Dificil 2b De 26 A 39", "Vencida De 14 A 39",
                "Saldo Cartera De 0 A 39 Semanas", "Legal 40 a 55", "Cazadores 56 +", "Saldo De Cartera"};

            jsonFinal = new JsonObject();

            for (int i = 0; i < jsonp.size(); i++) {

                jsonFinal.add(arrayNombres[i], jsonp.get(i));

            }

            //json = limpiXmlNormalidad(xmlStr);
        } catch (Exception e) {
            logger.info("Ap getNormalidad IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            jsonFinal = null;
            return null;
        }

        if (jsonFinal == null)//quitar la p
        {
            respuestaService = null;
        } else {
            respuestaService.add("normalidad", jsonFinal);//quitar la p
        }
        System.out.println(respuestaService.toString());

        return respuestaService;
    }

    public JsonObject getPagosDeServicio(String fecha, String geografia, String nivel) {

        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            //logger.info("Fecha :" + fecha);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TransferenciasPagoServicios");
            //logger.info("getPagosDeServicio " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap getPagosDeServicio IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("pagosServicios", json);
        }

        return respuestaService;
    }

    public JsonObject getTransferciasInternacionales(String fecha, String geografia, String nivel) {

        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {
            //logger.info("Fecha :" + fecha);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TransferenciasInternacionales");
            //logger.info("getTransferciasInternacionales " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap getTransferciasInternacionales IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasInternacionales", json);
        }

        return respuestaService;
    }

    public JsonObject getDineroExpress(String fecha, String geografia, String nivel) {

        //logger.info("---------Dinero Express------------");
        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {
            //logger.info("Fecha DEX:" + fecha);
            //logger.info("Geografia DEX:" + geografia);
            //logger.info("Nivel DEX:" + nivel);

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TranferenciasDEX");
            //logger.info("getDineroExpress " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap getDineroExpress IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasDEX", json);
        }

        return respuestaService;
    }

    public JsonObject getCambios(String fecha, String geografia, String nivel) {

        //logger.info("---------Cambio Divisas------------");
        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            //logger.info("Fecha :" + fecha);
            //logger.info("Geografia :" + geografia);
            //logger.info("Nivel :" + nivel);
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "TransferenciasCambios");
            //logger.info("getCambios " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap getCambios IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasCambios", json);
        }

        return respuestaService;
    }

    public JsonObject getContribucion(String fecha, String geografia, String nivel) {
        logger.info("---------Contribucion------------");

        JsonObject json = null;

        JsonObject respuestaService = new JsonObject();

        if (!nivel.equals("TR")) {

            if (nivel.equals("T")) {
                nivel = "G";
            }

            try {

                DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
                Date date = formatDate.parse(fecha);

                Calendar calendar = Calendar.getInstance();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.setMinimalDaysInFirstWeek(4);
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);

                int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                int numberDayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);

                logger.info("Fecha Actual: " + calendar.getTime());
                logger.info("Semana Actual: " + numberWeekOfYear);
                logger.info("Dia de la semana Actual :" + numberDayOfTheWeek);

                SimpleDateFormat f = new SimpleDateFormat("kmm");
                int fechaNum = Integer.parseInt(calendar.get(Calendar.DAY_OF_WEEK) + f.format(calendar.getTime()));

                logger.info("Cadena comparar : " + fechaNum);

                // Si es jueves 4:00 pm calcula semana atras
                if (fechaNum > 51600) {
                    calendar.add(Calendar.WEEK_OF_YEAR, -1);
                } else {
                    calendar.add(Calendar.WEEK_OF_YEAR, -2);
                }

                int dia = 0;
                switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                    case Calendar.MONDAY:
                        dia = 1;
                        break;
                    case Calendar.TUESDAY:
                        dia = 2;
                        break;
                    case Calendar.WEDNESDAY:
                        dia = 3;
                        break;
                    case Calendar.THURSDAY:
                        dia = 4;
                        break;
                    case Calendar.FRIDAY:
                        dia = 5;
                        break;
                    case Calendar.SATURDAY:
                        dia = 6;
                        break;
                    case Calendar.SUNDAY:
                        dia = 7;
                        break;
                    default:
                        break;
                }

                calendar.add(Calendar.DATE, (7 - dia));

                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                String fechaFormateada = df.format(calendar.getTime());

                logger.info("Fecha a FINAL " + calendar.getTime());
                logger.info("Semana calculada: " + calendar.get(Calendar.WEEK_OF_YEAR));

                logger.info("Fecha :" + fechaFormateada);
                logger.info("Geografia :" + geografia);
                logger.info("Nivel :" + nivel);

                String xmlStr = obtieneXmlResult(fechaFormateada, geografia, nivel, "Contribucion");
                //logger.info("getContribucion " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

                json = armaJsonContribucion(xmlStr);
                json.addProperty("semana", "" + calendar.get(Calendar.WEEK_OF_YEAR));

            } catch (Exception e) {
                logger.info("Ap getContribucion IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
                json = null;
            }

        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("contribucion", json);
        }

        return respuestaService;

    }

    public JsonObject getNoAbonadasPend(String fecha, String geografia, String nivel) {
        //logger.info("---------No Abonadas Pendientes por Surtir------------");

        Map<String, Object> jsonMap = null;

        JsonObject respuestaService = new JsonObject();

        if (!nivel.equals("TR")) {

            if (nivel.equals("T")) {
                nivel = "G";
            }

            try {

                DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
                Date date = formatDate.parse(fecha);

                Calendar calendar = Calendar.getInstance();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.setMinimalDaysInFirstWeek(4);
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);

                int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                int numberDayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);

                logger.info("Fecha Actual: " + calendar.getTime());
                logger.info("Semana Actual: " + numberWeekOfYear);
                logger.info("Dia de la semana Actual :" + numberDayOfTheWeek);

                SimpleDateFormat f = new SimpleDateFormat("kmm");
                int fechaNum = Integer.parseInt(calendar.get(Calendar.DAY_OF_WEEK) + f.format(calendar.getTime()));

                //logger.info("Cadena comparar : " + fechaNum);
                // Si es jueves 4:00 pm calcula semana atras
                if (fechaNum > 51600) {
                    calendar.add(Calendar.WEEK_OF_YEAR, -1);
                } else {
                    calendar.add(Calendar.WEEK_OF_YEAR, -2);
                }

                int dia = 0;
                switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                    case Calendar.MONDAY:
                        dia = 1;
                        break;
                    case Calendar.TUESDAY:
                        dia = 2;
                        break;
                    case Calendar.WEDNESDAY:
                        dia = 3;
                        break;
                    case Calendar.THURSDAY:
                        dia = 4;
                        break;
                    case Calendar.FRIDAY:
                        dia = 5;
                        break;
                    case Calendar.SATURDAY:
                        dia = 6;
                        break;
                    case Calendar.SUNDAY:
                        dia = 7;
                        break;
                    default:
                        break;
                }

                calendar.add(Calendar.DATE, (7 - dia));

                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                String fechaFormateada = df.format(calendar.getTime());

                String xmlStr = obtieneXmlResult(fechaFormateada, geografia, nivel, "NAbonadasPendSurt");
                //logger.info("getNoAbonadasPend " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

                jsonMap = armaJsonNoAbonadas(xmlStr);

            } catch (Exception e) {
                logger.info("Ap getNoAbonadasPend IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
                jsonMap = null;
            }

        }

        if (jsonMap == null) {
            respuestaService = null;
        } else {
            respuestaService.add("nuncaAbonadas", (JsonObject) jsonMap.get("nuncaAbonadas"));
            respuestaService.add("pendientesSurtrir", (JsonObject) jsonMap.get("pendientesSurtrir"));
        }

        return respuestaService;

    }

    /*METODOS PARA SEGUROS*/
    public JsonObject getSegurosNoLigados(String fecha, String geografia, String nivel) {

        //logger.info("---------SEGUROS NO LIGADOS Y ASESORES------------");
        JsonObject json = null;
        JsonObject json1 = null;
        JsonObject respuestaService = new JsonObject();

        try {

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "SegurosAsesores");
            //logger.info("getSegurosNoLigados1 " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);
            //OBTENER EL RESULTADO
            json = clearXmlSeguros(xmlStr);

            String xmlStr1 = obtieneXmlResult(fecha, geografia, nivel, "SegurosNoLigados");
            //logger.info("getSegurosNoLigados2 " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);
            //logger.info(xmlStr1);
            //OBTENER EL RESULTADO
            json1 = clearXmlSeguros(xmlStr1);

        } catch (Exception e) {
            logger.info("Ap getNoAbonadasPend IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("segurosNoLigados", json);
        }

        return respuestaService;

    }

    public JsonObject getSegurosPrestamos(String fecha, String geografia, String nivel) {

        //logger.info("---------SEGUROS PRESTAMOS------------");
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "SegurosPrestamos");
            //logger.info("getSegurosPrestamos " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            //OBTENER EL RESULTADO
            json = clearXmlSeguros(xmlStr);

        } catch (Exception e) {
            logger.info("Ap getSegurosPrestamos IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("segurosAsesores", json);
        }

        return respuestaService;

    }

    public JsonObject getSegurosRenovaciones(String fecha, String geografia, String nivel) {

        //logger.info("---------SEGUROS RENOVACIONES------------");
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "SegurosRenovaciones");
            //logger.info("getSegurosRenovaciones " + "fecha: " + fecha + "geografía: " + geografia + ", nivel: " + nivel + ", result = " + xmlStr);

            //OBTENER EL RESULTADO
            json = clearXmlSeguros(xmlStr);

        } catch (Exception e) {
            logger.info("Ap getSegurosRenovaciones IndicadoresBI " + "fecha: " + fecha + ", geografía: " + geografia + ", nivel: " + nivel);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("segurosAsesores", json);
        }

        return respuestaService;

    }

    //FIN SEGUROS
    public Map<String, Object> armaJsonNoAbonadas(String xmlStr) {

        Map<String, Object> mapJson = new HashMap<String, Object>();
        JsonObject nuncaAbonadas = new JsonObject();
        JsonObject pendientesSurtir = new JsonObject();

        try {
            /* Convierte String a XML */

            StringReader str = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(str);
            Element rootNode = document.getRootElement();

            Element data = rootNode.getChild("data");
            Element renglones = data.getChild("renglones");
            Element renglon = renglones.getChild("renglon");
            Element celdas = renglon.getChild("celdas");
            List<Element> listCeldas = celdas.getChildren("celda");

            for (Element element : listCeldas) {

                int celda = Integer.parseInt(element.getAttributeValue("pos"));
                String valor = element.getChild("caption").getValue();
                switch (celda) {
                    case 1:
                        break;
                    case 2:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 3:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 4:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 5:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 6:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 7:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                }

            }

            mapJson.put("pendientesSurtrir", pendientesSurtir);
            mapJson.put("nuncaAbonadas", nuncaAbonadas);

        } catch (Exception e) {
            logger.info("Ap arma json nuncaAbonadas");
            return null;
        }

        //logger.info("JSON SURTIDAS :" + pendientesSurtir.toString());
        //logger.info("JSON NUNCA ABONADAS :" + nuncaAbonadas.toString());
        return mapJson;
    }

    public JsonObject armaJsonContribucion(String xmlStr) {

        JsonObject contribucion = new JsonObject();
        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            Element consulta = rootNode.getChild("Consulta");
            Element table = consulta.getChild("Table");

            DecimalFormat df = new DecimalFormat("###,###,###");

            contribucion.addProperty("posicion", table.getChild("POSICION").getValue());
            contribucion.addProperty("geografia", table.getChild("GEOGRAFIA").getValue());
            contribucion.addProperty("semana1", df.format(Double.parseDouble(table.getChild("SEM_1").getValue())));
            contribucion.addProperty("semana2", df.format(Double.parseDouble(table.getChild("SEM_2").getValue())));
            contribucion.addProperty("crecimiento", table.getChild("CRECIMIENTO").getValue());
            contribucion.addProperty("total", table.getChild("TOTAL").getValue());
            contribucion.addProperty("totalReg", table.getChild("T_REG").getValue());

            int posicion = Integer.parseInt(contribucion.get("posicion").getAsString());
            int total_reg = Integer.parseInt(contribucion.get("totalReg").getAsString());
            String imagen = "";

            int res = (int) ((posicion * 100) / total_reg);

            logger.info("Posicion " + posicion);
            logger.info("total_reg " + total_reg);
            logger.info("res " + (int) res);

            if (res >= 0 && res <= 20) {
                imagen = "oro";
            } else if (res > 20 && res <= 50) {
                imagen = "plata";
            } else if (res > 50 && res <= 80) {
                imagen = "bronce";
            } else if (res > 80 && res <= 100) {
                imagen = "carbon";
            }

            contribucion.addProperty("imagen", imagen);

        } catch (Exception e) {
            logger.info("Ap arma json contribucion");
            return null;
        }

        return contribucion;
    }

    protected JsonArray limpiXmlNew(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            // Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            // Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples",
                            Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples",
                            Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            // logger.info("Columnas :"+columnas);
            // logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                // if(veces <= columnas){
                if (celda1 == celda2) {
                    // logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata)
                            .getChild("FmtValue",
                                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"))
                            .getValue();
                    // logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata)
                            .getChild("FmtValue",
                                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"))
                            .getValue());
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;

                    if (conCelldata < celdas.size()) {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                // }else{
                if (veces == columnas) {
                    // logger.info("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                // logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                // veces=1;
                /*
                 * arrayFilas.add(arrayValores); arrayValores = new JsonArray();
                 */

                // }

                /*
                 * if(cont == totalCeldas){ arrayFilas.add(arrayValores); }
                 */
            }

            // logger.info(arrayFilas.toString());
            // logger.info("Valores celda lista : " +listValoresCeldas.size());
            // logger.info("Valores totales lista : "
            // +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            jsonArray = armaJsonNew(valores, flagsColumnas);

        } catch (Exception e) {
            //logger.info("DETALLE LIMPIA XML " + e.getMessage());
            return null;
        }

        return jsonArray;

    }

    protected JsonArray limpiXmlNewCaptaciones(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            // Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            // Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples",
                            Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples",
                            Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            // logger.info("Columnas :"+columnas);
            // logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                // if(veces <= columnas){
                if (celda1 == celda2) {
                    // logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata)
                            .getChild("FmtValue",
                                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"))
                            .getValue();
                    // logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata)
                            .getChild("FmtValue",
                                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"))
                            .getValue());
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;

                    if (conCelldata < celdas.size()) {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                // }else{
                if (veces == columnas) {
                    // logger.info("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                // logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                // veces=1;
                /*
                 * arrayFilas.add(arrayValores); arrayValores = new JsonArray();
                 */

                // }

                /*
                 * if(cont == totalCeldas){ arrayFilas.add(arrayValores); }
                 */
            }

            // logger.info(arrayFilas.toString());
            // logger.info("Valores celda lista : " +listValoresCeldas.size());
            // logger.info("Valores totales lista : "
            // +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            jsonArray = armaJsonNewCaptaciones(valores, flagsColumnas);

        } catch (Exception e) {
            //logger.info("DETALLE LIMPIA XML" + e.getMessage());
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonNew(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 4) {

                    logger.info("cantidad: " + valoresCeldas.get(i));
                    logger.info("Cantidad vs" + valoresCeldas.get(i - pos_vs));
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;

                    pos_vs += 3;
                } else {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

            }

            valor = new JsonObject();
            valor.addProperty("cantidad", valorStr);
            valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
            valor.addProperty("color", color);

            arrayValores.add(valor);

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /* Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 4) {
                    porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                    pos_vsTot += 3;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

            }

            jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
            jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
            jsonTotales.addProperty("color", color);
            arrayValores.add(jsonTotales);

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonNewCaptaciones(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;

        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 5) {

                    logger.info("cantidad real: " + formateador.format(valoresCeldas.get(i)));
                    logger.info("Cantidad vs " + formateador.format(valoresCeldas.get((i) - pos_vs)));

                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;

                    pos_vs += 3;
                } else {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

            }

            valor = new JsonObject();
            valor.addProperty("cantidad", valorStr);
            valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
            valor.addProperty("color", color);

            arrayValores.add(valor);

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /* Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 5) {
                    logger.info("cantidad real: " + formateador.format(valoresTotales.get(a)));
                    logger.info("Cantidad vs " + formateador.format(valoresTotales.get(a - pos_vsTot)));
                    porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                    logger.info("" + (int) Math.round(porcentajeTot) + "%");
                    pos_vsTot += 3;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

            }

            jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
            jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
            jsonTotales.addProperty("color", color);
            arrayValores.add(jsonTotales);

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonObject clearXml(String xlmStr) {

        JsonObject respuesta = new JsonObject();

        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xlmStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            List<Element> allChildren = rootNode.getChildren();

            /* �NORMAL */
            String fmtValue = "";
            boolean flagAdd = false;
            String celda = "";

            ArrayList<String> listValores = new ArrayList<String>();

            for (Element element : allChildren) {
                if (element.getName().equals("CellData")) {

                    List<Element> listaDatos = element.getChildren();// Lista
                    // que
                    // guarda
                    // todas
                    // las
                    // etiquets
                    // "Cell"
                    for (Element elementChild : listaDatos) {
                        fmtValue = "";
                        flagAdd = false;
                        celda = elementChild.getAttributeValue("CellOrdinal");
                        List<Element> elementDatos = elementChild.getChildren();// Lista
                        // que
                        // guarda
                        // la
                        // etiquetas
                        // dentro
                        // de
                        // cada
                        // etiqueta
                        // "Cell"
                        for (Element elementDato : elementDatos) {
                            if (elementDato.getName().equals("FmtValue")) {
                                fmtValue = elementDato.getValue();
                            } else if (elementDato.getName().equals("Value")) {
                                List<Attribute> atributos = elementDato.getAttributes();
                                for (Attribute elementAtributo : atributos) { // Revisa
                                    // Atributos
                                    // de
                                    // la
                                    // celda
                                    if (!elementAtributo.getValue().equals("xsd:short")) {
                                        flagAdd = true;
                                    }
                                }
                            }

                        }

                        if (flagAdd) {
                            listValores.add(fmtValue);
                        }

                    }

                }
            }

            respuesta = createResult(listValores);

        } catch (Exception e) {
            logger.info("Ap clear xml");
            respuesta = null;
        }

        return respuesta;

    }

    //LIMPIA XML PARA SEGUROS
    @SuppressWarnings({"unchecked", "unused"})
    protected JsonObject clearXmlSeguros(String xlmStr) {

        JsonObject respuesta = new JsonObject();

        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xlmStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            List<Element> allChildren = rootNode.getChildren();

            /* �NORMAL */
            String fmtValue = "";
            boolean flagAdd = false;
            String celda = "";

            ArrayList<String> listValores = new ArrayList<String>();

            for (Element element : allChildren) {
                if (element.getName().equals("CellData")) {

                    List<Element> listaDatos = element.getChildren();// Lista
                    // que
                    // guarda
                    // todas
                    // las
                    // etiquets
                    // "Cell"
                    for (Element elementChild : listaDatos) {
                        fmtValue = "";
                        flagAdd = false;
                        celda = elementChild.getAttributeValue("CellOrdinal");
                        List<Element> elementDatos = elementChild.getChildren();// Lista
                        // que
                        // guarda
                        // la
                        // etiquetas
                        // dentro
                        // de
                        // cada
                        // etiqueta
                        // "Cell"
                        for (Element elementDato : elementDatos) {
                            if (elementDato.getName().equals("FmtValue")) {
                                fmtValue = elementDato.getValue();
                            } else if (elementDato.getName().equals("Value")) {
                                List<Attribute> atributos = elementDato.getAttributes();

                                for (Attribute elementAtributo : atributos) { // Revisa
                                    // Atributos
                                    // de
                                    // la
                                    // celda
                                    if (!elementAtributo.getValue().equals("xsd:short")) {
                                        flagAdd = true;
                                    }
                                }
                            }

                        }

                        if (flagAdd) {
                            listValores.add(fmtValue);
                        }

                    }

                }
            }

            respuesta = createResultSeguros(listValores);

        } catch (Exception e) {
            logger.info("Algo paso");
            respuesta = null;
        }

        return respuesta;

    }

    @SuppressWarnings("unused")
    protected JsonObject createResult(ArrayList<String> valores) {

        DecimalFormat formateador = new DecimalFormat("###,###.#");
        double[] totales = new double[9];
        int nuInteraciones = 8;

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        int indice = valores.size() - 1;

        try {

            /* Obtiene totales */
            while (nuInteraciones >= 0) {
                double valor = Double.parseDouble(valores.get(indice)) / 1000;
                totales[nuInteraciones] = valor;
                // totales[nuInteraciones] = (int)Math.round(valor);
                indice--;
                nuInteraciones--;
            }

            // int positionTotales = 0;
            int veces = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i <= indice; i++) {

                veces++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "";

                if (veces > 5) {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = Double.parseDouble(valores.get(i - pos_vs)) / 1000;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt= (int)Math.round(valor);
                    porcentaje = (valor / totales[veces - 1]) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", "" + (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces == 9) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces = 0;
                    pos_vs = 2;
                }

            }

            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.length; a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 4) {
                    porcentajeTot = (double) totales[a] / (double) totales[a - pos_vs] * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales[a]));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);
            logger.info(jsonDias.toString());

        } catch (Exception e) {
            logger.info("Ap createResult");
            jsonDias = null;
        }

        return jsonDias;

    }

    //CREATE RESULT SEGUROS
    @SuppressWarnings("unused")
    protected JsonObject createResultSeguros(ArrayList<String> valores) {

        JsonObject jsonResult = new JsonObject();

        jsonResult.addProperty("valor", valores.get(0));

        return jsonResult;

    }

    /* Limpia respuesta xml de los rubros Captacion */
    @SuppressWarnings("unchecked")
    protected JsonObject limpiaXmlCaptacion(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            // Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            // Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            /*-----------------------------------------------Eliminar por pruebas--------------------------------------------------------------------*/
            logger.info("Celldata" + rootNode.getChild("CellData",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                // logger.info("Celda "
                // +element.getAttributeValue("CellOrdinal"));
                numCeldas.add(Integer.parseInt(element.getAttributeValue("CellOrdinal")));

                String valor = element.getChild("FmtValue",
                        Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonCaptacion(valoresCeldas, flagsColumnas, numCeldas);

        } catch (Exception e) {
            //logger.info("Ap limpiaXmlCaptacion");
            return null;
        }

        return jsonRepuestas;

    }

    /* Arma Json de los rubros Captacion */
    @SuppressWarnings("unused")
    protected JsonObject armaJsonCaptacion(List<String> valoresCeldas, int[] flagsColumnas,
            ArrayList<Integer> numCeldas) {

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        try {
            /* Obtener totales */
            DecimalFormat formateador = new DecimalFormat("###,###.#");
            int columnas = flagsColumnas.length;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - columnas);
            int indiceColumnas = 0;

            while (indice < valoresCeldas.size()) {

                if (flagsColumnas[indiceColumnas] == 1) {
                    totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                }

                indiceColumnas++;
                indice++;
            }

            /* Obtener Filas */
            int cont = 0;
            int veces = 1;
            int corte = 1;

            ArrayList<Double> valores = new ArrayList<Double>();

            while (cont < numCeldas.size() && cont != (valoresCeldas.size() - columnas)) {

                JsonObject valorPorcentaje = null;

                if (veces != numCeldas.get(cont)) {
                    break;
                }

                if (flagsColumnas[corte - 1] == 1) {
                    valores.add(Double.parseDouble(valoresCeldas.get(cont)));
                    // logger.info("$"+formateador.format(Double.parseDouble(valoresCeldas.get(cont))/1000.0));
                    // logger.info("$"+Double.parseDouble(valoresCeldas.get(cont))/1000);
                }

                if (corte == flagsColumnas.length) {
                    // logger.info("Agrega Fila");
                    corte = 1;
                    veces += 2;
                } else {
                    corte++;
                    veces++;
                }

                cont++;

            }

            // int positionTotales = 0;
            int veces1 = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i < valores.size(); i++) {

                veces1++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "verde";

                if (veces1 > 4) {
                    valor = valores.get(i) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = valores.get(i - pos_vs) / 1000.0;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = valores.get(i) / 1000;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces1 == 7) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces1 = 0;
                    pos_vs = 2;
                }
            }
            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.size(); a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 3) {
                    porcentajeTot = (double) totales.get(a) / (double) totales.get(a - pos_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);

        } catch (Exception e) {
            logger.info("Ap armaJsonCaptacion");
            jsonDias = null;
        }

        return jsonDias;
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected JsonObject limpiXmlNormalidad(String xmlStr) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            // Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            // Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            // logger.info("Celldata"+rootNode.getChild("CellData",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData",
                    Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                // logger.info("Celda "
                // +element.getAttributeValue("CellOrdinal"));

                String valor = element.getChild("FmtValue",
                        Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonNormalidad(valoresCeldas);

        } catch (Exception e) {
            logger.info("Ap limpiXmlNormalidad");
            return null;
        }

        return jsonRepuestas;

    }

    @SuppressWarnings("unused")
    protected JsonObject armaJsonNormalidad(List<String> valoresCeldas) {

        String arrayNombres[] = {"Vigente De 0 A 1", "Temprana 2", "Atrasada 1a De 3 A 7", "Tardia 1b De 8 A 13",
            "Atrasada 2 A 13", "Vencida 2a De 14 A 25", "Dificil 2b De 26 A 39", "Vencida De 14 A 39",
            "Saldo Cartera De 0 A 39 Semanas", "Legal 40 a 55", "Cazadores 56 +", "Saldo De Cartera"};

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonFilas = new JsonObject();

        try {

            /* Obtener totales */
            DecimalFormat formateador = new DecimalFormat("###,###");

            DecimalFormat formateadorDecimales = new DecimalFormat("###.##");

            int columnas = 5;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - (columnas * 4));
            int indiceColumnas = 0;

            logger.info("Inicio :" + indice);
            while (indice < valoresCeldas.size() - (columnas * 3)) {

                // logger.info(formateador.format(Double.parseDouble(valoresCeldas.get(indice))/1000.0));
                totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                indiceColumnas++;
                indice++;
            }

            int fin = (valoresCeldas.size() - (columnas * 4));
            int veces = 0;

            JsonObject valorPorcentaje = null;

            int totalFilas = 0;
            for (int i = 0; i < valoresCeldas.size(); i++) {

                veces++;
                valorPorcentaje = new JsonObject();

                double porcentaje = 0.0;
                double valor = 0.0;

                int valorInt = 0;
                String color = "verde";

                valor = Double.parseDouble(valoresCeldas.get(i)) / 1000;
                // logger.info("Valor "+ formateador.format(valor));
                // logger.info("Total "+
                // formateador.format(totales.get(veces-1)));

                if (veces > 3) {
                    porcentaje = 0.0;
                } else {
                    porcentaje = (valor / (totales.get(veces - 1))) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                // logger.info("Porcentaje "+(int)Math.round(porcentaje)+"%");
                valorPorcentaje.addProperty("cantidad", formateador.format(valor));
                //valorPorcentaje.addProperty("porcentaje", formateadorDecimales.format(porcentaje) + "%");
                valorPorcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorPorcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorPorcentaje);

                if (veces == 5) {

                    jsonFilas.add(arrayNombres[totalFilas], arrayValorPorcentaje);
                    arrayValorPorcentaje = new JsonArray();

                    veces = 0;

                    totalFilas++;
                }

            }
        } catch (Exception e) {
            jsonFilas = null;
        }

        logger.info(jsonFilas.toString());

        return jsonFilas;

    }

    protected String getdia(int nuDia) {
        String dia = "";
        if (nuDia == 1) {
            dia = "lunes";
        } else if (nuDia == 2) {
            dia = "martes";
        } else if (nuDia == 3) {
            dia = "miercoles";
        } else if (nuDia == 4) {
            dia = "jueves";
        } else if (nuDia == 5) {
            dia = "viernes";
        } else if (nuDia == 6) {
            dia = "sabado";
        } else if (nuDia == 7) {
            dia = "domingo";
        } else if (nuDia == 8) {
            dia = "totales";
        }
        return dia;

    }

    public static String getProperty(int valor) {

        String propiedad = "";
        switch (valor) {
            case 1:
                propiedad = "cabeceras";
                break;
            case 2:
                propiedad = "noAbonadas";
                break;
            case 3:
                propiedad = "totalCuentas";
                break;
            case 4:
                propiedad = "pendientesSurtir";
                break;
            case 5:
                propiedad = "autorizadas";
                break;
            case 6:
                propiedad = "clientesNuevos";
                break;
            case 7:
                propiedad = "creditosOtorgados";
                break;
            case 8:
                propiedad = "planClientes";
                break;
            case 9:
                propiedad = "level";
                break;

            default:
                propiedad = "";
                break;
        }

        return propiedad;

    }

    // Servicio para obtener COLOCACION PERSONALES y COLOCACION CONSUMO
    public Map<String, String> getPorcentajesColocacion(String geografia, String nivel) {

        // aaaammdd
        String fecha = getNextSunday();

        Map<String, String> datos = new HashMap<String, String>();

        logger.info("GEOGRAFIA");
        logger.info(geografia);
        logger.info("GEOGRAFIA");

        String numeroGeo = geografia.split(" ")[0];
        logger.info("NUM - GEOGRAFIA");
        logger.info(numeroGeo);

        geografia = geografia.replace("%20", " ");

        ExtraccionFinancieraBI extraccionFinancieraBI = new ExtraccionFinancieraBI();
        IndicadoresTiempoRealBI indicadoresTiempoRealBI = new IndicadoresTiempoRealBI();

        logger.info("*******************INICIO INDICADORES TIEMPO REAL PARA REPORTE*******************");

        // PERSONALES
        // SE LLAMA CONSUMO COLOCACION TIEMPO REAL , PERO ES LA COLOCACION DE
        // PRESTAMOS ACUMULADOS PARA TIEMPO REAL
        JsonObject xmlPersonales = null;
        String hoy = getNameToday();
        String[] dias = {"lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"};

        try {

            xmlPersonales = extraccionFinancieraBI.getConsumoColocacionTiempoRealXML(fecha, geografia, nivel);
            //ES NULL NO HACER NADA ********
            if (xmlPersonales == null) {
                return null;
            } else {
                logger.info("PERSONALES SEMANA :)" + xmlPersonales);
                JsonObject data = xmlPersonales.getAsJsonObject("consumoColocacion");
                //logger.info("ARRAY DATA" + data.toString());

                double compromisoHoyPersonales = 0.0f;
                double arrastrePersonales = 0.0f;

                switch (hoy) {

                    case "lunes":
                        // Obtengo el compromiso de hoy
                        compromisoHoyPersonales = Double.parseDouble(
                                data.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 1; i++) {
                            arrastrePersonales += Double.parseDouble(data.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "martes":
                        // Obtengo el compromiso de hoy
                        compromisoHoyPersonales = Double.parseDouble(
                                data.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 2; i++) {
                            arrastrePersonales += Double.parseDouble(data.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "miercoles":
                        // Obtengo el compromiso de hoy
                        compromisoHoyPersonales = Double.parseDouble(
                                data.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 3; i++) {
                            arrastrePersonales += Double.parseDouble(data.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "jueves":
                        // Obtengo el compromiso de hoy
                        compromisoHoyPersonales = Double.parseDouble(
                                data.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 4; i++) {
                            arrastrePersonales += Double.parseDouble(data.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "viernes":
                        // Obtengo el compromiso de hoy
                        compromisoHoyPersonales = Double.parseDouble(
                                data.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 5; i++) {
                            arrastrePersonales += Double.parseDouble(data.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "sabado":
                        // Obtengo el compromiso de hoy
                        compromisoHoyPersonales = Double.parseDouble(
                                data.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 6; i++) {
                            arrastrePersonales += Double.parseDouble(data.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "domingo":
                        // Obtengo el compromiso de hoy
                        compromisoHoyPersonales = Double.parseDouble(
                                data.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 7; i++) {
                            arrastrePersonales += Double.parseDouble(data.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                }

                // Obtengo el avance de la colocacion PERSONALES en tiempo real
                String actualPersonales = indicadoresTiempoRealBI.getRealTotal(numeroGeo, nivel);
                logger.info("PERSONALES ACTUAL :" + actualPersonales.split(",")[2].split(":")[1]);

                int personalesActual = Integer.parseInt(actualPersonales.split(",")[2].split(":")[1]);

                if (personalesActual != -1000) {

                    arrastrePersonales = arrastrePersonales * -1;
                    logger.info("_____________ DATOS DE PERSONALES _____________");
                    logger.info("COMPROMISO HOY -- " + compromisoHoyPersonales);
                    logger.info("ARRASTRE -- " + arrastrePersonales * -1);
                    logger.info("AVANCE ACTUAL -- " + personalesActual);
                    String datosPersonales = getPorcentajeAvance(personalesActual, arrastrePersonales);

                    datos.put("ValorPersonales", "" + Math.round(Double.parseDouble(datosPersonales.split(",")[0])));
                    datos.put("ColorPersonales", datosPersonales.split(",")[1]);

                    logger.info("FIN DATOS DE PERSONALES");
                } else {

                    datos.put("ValorPersonales", "0");
                    datos.put("ColorPersonales", "null");
                    logger.info("FIN DATOS DE PERSONALES EN 0");
                }

            }
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

        try {
            // CONSUMO

            // SE LLAMA CONSUMO COLOCACION TIEMPO REAL , PERO ES LA COLOCACION DE
            // PRESTAMOS ACUMULADOS PARA TIEMPO REAL
            JsonObject xmlConsumo = extraccionFinancieraBI.getConsumoColocacionXML(fecha, geografia, nivel);

            if (xmlConsumo == null) {

                return null;

            } else {

                JsonObject data2 = xmlConsumo.getAsJsonObject("consumoColocacion");
                //logger.info("ARRAY DATA" + data2.toString());

                double compromisoHoyConsumo = 0.0f;
                double arrastreConsumo = 0.0f;

                switch (hoy) {

                    case "lunes":
                        // Obtengo el compromiso de hoy
                        compromisoHoyConsumo = Double.parseDouble(
                                data2.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 1; i++) {
                            arrastreConsumo += Double.parseDouble(data2.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "martes":
                        // Obtengo el compromiso de hoy
                        compromisoHoyConsumo = Double.parseDouble(
                                data2.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 2; i++) {
                            arrastreConsumo += Double.parseDouble(data2.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "miercoles":
                        // Obtengo el compromiso de hoy
                        compromisoHoyConsumo = Double.parseDouble(
                                data2.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 3; i++) {
                            arrastreConsumo += Double.parseDouble(data2.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "jueves":
                        // Obtengo el compromiso de hoy
                        compromisoHoyConsumo = Double.parseDouble(
                                data2.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 4; i++) {
                            arrastreConsumo += Double.parseDouble(data2.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "viernes":
                        // Obtengo el compromiso de hoy
                        compromisoHoyConsumo = Double.parseDouble(
                                data2.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 5; i++) {
                            arrastreConsumo += Double.parseDouble(data2.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "sabado":
                        // Obtengo el compromiso de hoy
                        compromisoHoyConsumo = Double.parseDouble(
                                data2.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 6; i++) {
                            arrastreConsumo += Double.parseDouble(data2.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                        }
                        break;
                    case "domingo":
                        // Obtengo el compromiso de hoy
                        compromisoHoyConsumo = Double.parseDouble(
                                data2.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                        for (int i = 0; i < 7; i++) {
                            arrastreConsumo += Double.parseDouble(data2.getAsJsonArray(dias[i]).get(5).getAsJsonObject()
                                    .get("cantidad").getAsString().replace(",", ""));
                            break;
                        }
                }

                // Obtengo el avance de la colocacion PERSONALES en tiempo real
                String actualConsumo = indicadoresTiempoRealBI.getRealTotalColocacion(numeroGeo, nivel);
                logger.info("********* CONSUMO ------- " + actualConsumo.split(",")[2].split(":")[1] + "*************");

                int consumoActual = Integer.parseInt(actualConsumo.split(",")[2].split(":")[1]);

                if (consumoActual != -1000) {

                    logger.info("_____________ DATOS DE CONSUMO ____________");
                    arrastreConsumo = arrastreConsumo * -1;
                    logger.info("COMPROMISO HOY -- " + compromisoHoyConsumo);
                    logger.info("ARRASTRE -- " + arrastreConsumo);
                    logger.info("AVANCE ACTUAL -- " + consumoActual);
                    logger.info("FIN DATOS DE CONSUMO");

                    String datosConsumo = getPorcentajeAvance(consumoActual, arrastreConsumo);

                    datos.put("ValorConsumo", "" + Math.round(Double.parseDouble(datosConsumo.split(",")[0])));
                    datos.put("ColorConsumo", datosConsumo.split(",")[1]);

                    logger.info("*******************FIN INDICADORES TIEMPO REAL PARA REPORTE*******************");

                } else {
                    datos.put("ValorConsumo", "0");
                    datos.put("ColorConsumo", "null");
                    logger.info("FIN DATOS DE CONSUMO EN 0");
                }

            }

        } catch (Exception e) {
            logger.info(e);
            return null;
        }

        return datos;
    }

    // Servicio para obtener COLOCACION PERSONALES y COLOCACION CONSUMO
    public Map<String, String> getPorcentajesColocacionTiempoReal(String ceco, String nombreCeco, String nivel) {

        IndicadoresTiempoRealBI indicadoresTiempoRealBI = new IndicadoresTiempoRealBI();

        ExtraccionFinancieraBI extraccionFinancieraBI = new ExtraccionFinancieraBI();

        logger.info("SERVICIO JORGE *****  CECO ---- " + ceco + "NIVEL ---- " + nivel + "NOMBRE CECO ---- " + nombreCeco);

        String realConsumo = indicadoresTiempoRealBI.getRealTotalColocacion(ceco, nivel);
        String resPersonales = indicadoresTiempoRealBI.getRealTotal(ceco, nivel);

        JsonParser parser = new JsonParser();

        JsonObject realConsumoObj = null;
        JsonObject realPersonalesObj = null;

        JsonObject compromisoConsumo = null;
        JsonObject compromisoPersonales = null;

        try {

            String fecha = getNextSunday();
            String geografia = ceco + " - " + nombreCeco;

            realConsumoObj = parser.parse(realConsumo).getAsJsonObject();
            realPersonalesObj = parser.parse(resPersonales).getAsJsonObject();

            compromisoConsumo = extraccionFinancieraBI.getConsumoColocacionXML(fecha, geografia, nivel);
            compromisoPersonales = extraccionFinancieraBI.getConsumoColocacionTiempoRealXML(fecha, geografia, nivel);

            logger.info("realConsumoObj.compromisoHoy... " + realConsumoObj.get("compromisoHoy").getAsString());
            logger.info("realConsumoObj.avance... " + realConsumoObj.get("avance").getAsString());

            logger.info("realPersonalesObj.compromisoHoy... " + realPersonalesObj.get("compromisoHoy").getAsString());
            logger.info("realPersonalesObj.avance... " + realPersonalesObj.get("avance").getAsString());

            JsonObject dataConsumo = compromisoConsumo.get("consumoColocacion").getAsJsonObject();
            JsonObject dataPersonales = compromisoPersonales.get("consumoColocacion").getAsJsonObject();

            /*xmlPersonales = extraccionFinancieraBI.getConsumoColocacionTiempoRealXML(fecha, geografia, nivel);
             logger.info("PERSONALES SEMANA :)" + xmlPersonales);
             JsonObject data = xmlPersonales.getAsJsonObject("consumoColocacion");
             logger.info("ARRAY DATA" + data.toString());
             */
 /* xmlPersonales == compromisoPersonales*/
            String hoy = getNameToday();
            String[] dias = {"lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"};

            double compromisoHoyPersonales = 0.0f;
            double compromisoDiasAnterioresPersonales = 0.0f;
            double colocadoDiasAnterioresPersonales = 0.0f;

            double compromisoHoyConsumo = 0.0f;
            double compromisoDiasAnterioresConsumo = 0.0f;
            double colocadoDiasAnterioresConsumo = 0.0f;

            switch (hoy) {

                case "lunes":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    compromisoDiasAnterioresPersonales += 0;
                    colocadoDiasAnterioresPersonales += 0;

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    compromisoDiasAnterioresConsumo += 0;
                    colocadoDiasAnterioresConsumo += 0;

                    break;
                case "martes":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 1; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                    }
                    break;
                case "miercoles":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 2; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "jueves":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 3; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "viernes":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 4; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "sabado":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 5; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "domingo":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 6; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
            }

            //Hora Actual -9
            double hora = this.getHora() * 60;
            //Minutos Transcurridos actualmente
            double minutos = this.getMinutos();

            //Total de minutos transcurridos
            double minutosTotales = hora + minutos;

            //minutos totales = 720
            //double compromisoPorMinutoPersonales =  compromisoHoyPersonales / 720;
            //double compromisoPorMinutoConsumo =  compromisoHoyConsumo / 720;
            //compromiso total al minuto
            //double compromisoAlMinutoPersonales = compromisoDiasAnterioresPersonales + (minutosTotales * compromisoPorMinutoPersonales);
            //double compromisoAlMinutoConsumo = compromisoDiasAnterioresConsumo + (minutosTotales * compromisoPorMinutoConsumo);
            double compromisoAlDiaPersonales = compromisoDiasAnterioresPersonales + compromisoHoyPersonales;
            double compromisoAlDiaConsumo = compromisoDiasAnterioresConsumo + compromisoHoyConsumo;

            double avanceTiempoRealPersonales = colocadoDiasAnterioresPersonales + Double.parseDouble(realPersonalesObj.get("avance").getAsString());
            double avanceTiempoRealConsumo = colocadoDiasAnterioresConsumo + Double.parseDouble(realConsumoObj.get("avance").getAsString());

            double porcentajePersonales = 0.0;
            double porcentajeConsumo = 0.0;

            double compromisoSemanalPersonales = 0.0;
            double compromisoSemanalConsumo = 0.0;

            for (int i = 0; i < 7; i++) {

                //PERSONALES
                compromisoSemanalPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                        .get("cantidad").getAsString().replace(",", ""));

                //CONSUMO
                compromisoSemanalConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                        .get("cantidad").getAsString().replace(",", ""));

            }

            /*COMPROMISO AL DIA COMPLETO*/
            if (compromisoSemanalPersonales != 0) {

                porcentajePersonales = (avanceTiempoRealPersonales * 100) / compromisoSemanalPersonales;
            }

            if (compromisoSemanalConsumo != 0) {

                porcentajeConsumo = (avanceTiempoRealConsumo * 100) / compromisoSemanalConsumo;
            }

            /*COMPROMISO AL MINUTO*/
 /*if (compromisoAlMinutoPersonales != 0){

             porcentajePersonales = ( avanceTiempoRealPersonales * 100 ) / compromisoAlMinutoPersonales;
             }

             if (compromisoAlMinutoConsumo != 0){

             porcentajeConsumo = ( avanceTiempoRealConsumo * 100 ) / compromisoAlMinutoConsumo;
             }
             */
            porcentajePersonales = Math.round(porcentajePersonales);
            porcentajeConsumo = Math.round(porcentajeConsumo);

            String colorPersonales = "green";
            String colorConsumo = "green";

            int porPer = (int) porcentajePersonales;
            int porCon = (int) porcentajeConsumo;

            int intCompromisoAlDiaConsumo = (int) Math.round(compromisoSemanalConsumo);
            int intCompromisoAlDiaPersonales = (int) Math.round(compromisoSemanalPersonales);

            int intAvanceTiempoRealConsumo = (int) Math.round(avanceTiempoRealConsumo);
            int intAvanceTiempoRealPersonales = (int) Math.round(avanceTiempoRealPersonales);

            if (porPer >= 65) {
                colorPersonales = "green";
                //porPer = 100;
            } else if (porPer > 54 && porPer < 65) {
                colorPersonales = "yellow";
            } else {
                colorPersonales = "red";
            }

            if (porCon >= 65) {
                colorConsumo = "green";
                //porCon = 100;
            } else if (porCon > 54 && porCon < 65) {
                colorConsumo = "yellow";
            } else {
                colorConsumo = "red";
            }

            if (porPer < 0) {
                porPer = 0;
            }
            if (porCon < 0) {
                porCon = 0;
            }

            Map<String, String> response = new HashMap();

            response.put("valorConsumo", porCon + "");
            response.put("valorPersonales", porPer + "");

            response.put("colorConsumo", colorConsumo);
            response.put("colorPersonales", colorPersonales);

            response.put("compromisoConsumo", intCompromisoAlDiaConsumo + "");
            response.put("compromisoPersonales", intCompromisoAlDiaPersonales + "");

            if (intAvanceTiempoRealConsumo == -1000) {
                response.put("avanceTiempoRealConsumo", 0 + "");
            } else {
                response.put("avanceTiempoRealConsumo", intAvanceTiempoRealConsumo + "");
            }
            if (intAvanceTiempoRealPersonales == -1000) {
                response.put("avanceTiempoRealPersonales", 0 + "");
            } else {
                response.put("avanceTiempoRealPersonales", intAvanceTiempoRealPersonales + "");
            }

            return response;

        } catch (Exception e) {

            double compromisoAlDiaPersonales = 0;
            double compromisoAlDiaConsumo = 0;

            double avanceTiempoRealPersonales = 0;
            double avanceTiempoRealConsumo = 0;

            double porcentajePersonales = 0.0;
            double porcentajeConsumo = 0.0;

            double compromisoSemanalPersonales = 0.0;
            double compromisoSemanalConsumo = 0.0;

            //RANDOM
            //RANDOM
            compromisoSemanalPersonales = (Math.random() * 100);

            avanceTiempoRealPersonales = (Math.random() * 100);

            compromisoAlDiaPersonales = (Math.random() * 10000);

            compromisoSemanalConsumo = (Math.random() * 100);

            avanceTiempoRealConsumo = (Math.random() * 100);

            compromisoAlDiaConsumo = (Math.random() * 10000);
            /*COMPROMISO AL DIA COMPLETO*/

            if (compromisoSemanalPersonales != 0) {

                porcentajePersonales = (avanceTiempoRealPersonales * 100) / compromisoAlDiaPersonales;
            }

            if (compromisoSemanalConsumo != 0) {

                porcentajeConsumo = (avanceTiempoRealConsumo * 100) / compromisoAlDiaConsumo;
            }

            /*COMPROMISO AL MINUTO*/
 /*if (compromisoAlMinutoPersonales != 0){

             porcentajePersonales = ( avanceTiempoRealPersonales * 100 ) / compromisoAlMinutoPersonales;
             }

             if (compromisoAlMinutoConsumo != 0){

             porcentajeConsumo = ( avanceTiempoRealConsumo * 100 ) / compromisoAlMinutoConsumo;
             }
             */
            porcentajePersonales = Math.round(porcentajePersonales);
            porcentajeConsumo = Math.round(porcentajeConsumo);

            String colorPersonales = "green";
            String colorConsumo = "green";

            int porPer = (int) porcentajePersonales;
            int porCon = (int) porcentajeConsumo;

            int intCompromisoAlDiaConsumo = (int) Math.round(compromisoAlDiaConsumo);
            int intCompromisoAlDiaPersonales = (int) Math.round(compromisoAlDiaPersonales);

            int intAvanceTiempoRealConsumo = (int) Math.round(avanceTiempoRealConsumo);
            int intAvanceTiempoRealPersonales = (int) Math.round(avanceTiempoRealPersonales);

            if (porPer >= 65) {
                colorPersonales = "green";
                //porPer = 100;
            } else if (porPer > 54 && porPer < 65) {
                colorPersonales = "yellow";
            } else {
                colorPersonales = "red";
            }

            if (porCon >= 65) {
                colorConsumo = "green";
                //porCon = 100;
            } else if (porCon > 54 && porCon < 65) {
                colorConsumo = "yellow";
            } else {
                colorConsumo = "red";
            }

            if (porPer < 0) {
                porPer = 0;
            }
            if (porCon < 0) {
                porCon = 0;
            }

            Map<String, String> response = new HashMap();

            response.put("valorConsumo", porCon + "");
            response.put("valorPersonales", porPer + "");

            response.put("colorConsumo", colorConsumo);
            response.put("colorPersonales", colorPersonales);

            response.put("compromisoConsumo", intCompromisoAlDiaConsumo + "");
            response.put("compromisoPersonales", intCompromisoAlDiaPersonales + "");

            if (intAvanceTiempoRealConsumo == -1000) {
                response.put("avanceTiempoRealConsumo", 0 + "");
            } else {
                response.put("avanceTiempoRealConsumo", intAvanceTiempoRealConsumo + "");
            }
            if (intAvanceTiempoRealPersonales == -1000) {
                response.put("avanceTiempoRealPersonales", 0 + "");
            } else {
                response.put("avanceTiempoRealPersonales", intAvanceTiempoRealPersonales + "");
            }

            return response;
        }

    }

    // Servicio para obtener COLOCACION PERSONALES y COLOCACION CONSUMO DE HOY
    public Map<String, String> getPorcentajesColocacionTiempoRealHoy(String ceco, String nombreCeco, String nivel) {

        IndicadoresTiempoRealBI indicadoresTiempoRealBI = new IndicadoresTiempoRealBI();

        ExtraccionFinancieraBI extraccionFinancieraBI = new ExtraccionFinancieraBI();

        logger.info("SERVICIO JORGE HOY *****  CECO ---- " + ceco + "NIVEL ---- " + nivel + "NOMBRE CECO ---- " + nombreCeco);

        String realConsumo = indicadoresTiempoRealBI.getRealTotalColocacion(ceco, nivel);
        logger.info("SERVICIO JORGE *****  realConsumo---- " + realConsumo);

        String resPersonales = indicadoresTiempoRealBI.getRealTotal(ceco, nivel);
        logger.info("SERVICIO JORGE *****  resPersonales---- " + resPersonales);

        JsonParser parser = new JsonParser();

        JsonObject realConsumoObj = null;
        JsonObject realPersonalesObj = null;

        JsonObject compromisoConsumo = null;
        JsonObject compromisoPersonales = null;
        try {

            String fecha = getNextSunday();
            String geografia = ceco + " - " + nombreCeco;

            realConsumoObj = parser.parse(realConsumo).getAsJsonObject();
            realPersonalesObj = parser.parse(resPersonales).getAsJsonObject();

            logger.info("SERVICIO JORGE *****  Fecha Alto---- " + fecha);

            compromisoConsumo = extraccionFinancieraBI.getConsumoColocacionXML(fecha, geografia, nivel);

            logger.info("SERVICIO JORGE *****  Fecha Medio---- " + fecha);
            compromisoPersonales = extraccionFinancieraBI.getConsumoColocacionTiempoRealXML(fecha, geografia, nivel);

            logger.info("SERVICIO JORGE *****  Fecha Bajo---- " + fecha);

            logger.info("realConsumoObj.compromisoHoy... " + realConsumoObj.get("compromisoHoy").getAsString());
            logger.info("realConsumoObj.avance... " + realConsumoObj.get("avance").getAsString());

            logger.info("realPersonalesObj.compromisoHoy... " + realPersonalesObj.get("compromisoHoy").getAsString());
            logger.info("realPersonalesObj.avance... " + realPersonalesObj.get("avance").getAsString());

            JsonObject dataConsumo = compromisoConsumo.get("consumoColocacion").getAsJsonObject();
            JsonObject dataPersonales = compromisoPersonales.get("consumoColocacion").getAsJsonObject();

            logger.info("dataConsumo... " + dataConsumo);
            logger.info("dataPersonales... " + dataPersonales);

            /*xmlPersonales = extraccionFinancieraBI.getConsumoColocacionTiempoRealXML(fecha, geografia, nivel);
             logger.info("PERSONALES SEMANA :)" + xmlPersonales);
             JsonObject data = xmlPersonales.getAsJsonObject("consumoColocacion");
             logger.info("ARRAY DATA" + data.toString());
             */
 /* xmlPersonales == compromisoPersonales*/
            String hoy = getNameToday();
            String[] dias = {"lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"};

            double compromisoHoyPersonales = 0.0f;
            double compromisoDiasAnterioresPersonales = 0.0f;
            double colocadoDiasAnterioresPersonales = 0.0f;

            double compromisoHoyConsumo = 0.0f;
            double compromisoDiasAnterioresConsumo = 0.0f;
            double colocadoDiasAnterioresConsumo = 0.0f;

            switch (hoy) {

                case "lunes":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    compromisoDiasAnterioresPersonales += 0;
                    colocadoDiasAnterioresPersonales += 0;

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    compromisoDiasAnterioresConsumo += 0;
                    colocadoDiasAnterioresConsumo += 0;

                    break;
                case "martes":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 1; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                    }
                    break;
                case "miercoles":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 2; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "jueves":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 3; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "viernes":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 4; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "sabado":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 5; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
                case "domingo":
                    // PERSONALES
                    compromisoHoyPersonales = Double.parseDouble(
                            dataPersonales.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    //CONSUMO
                    compromisoHoyConsumo = Double.parseDouble(
                            dataConsumo.getAsJsonArray(hoy).get(3).getAsJsonObject().get("cantidad").getAsString().replace(",", ""));

                    for (int i = 0; i < 6; i++) {

                        compromisoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        //CONSUMO
                        compromisoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));

                        colocadoDiasAnterioresConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(4).getAsJsonObject()
                                .get("cantidad").getAsString().replace(",", ""));
                    }
                    break;
            }

            //Hora Actual -9
            double hora = this.getHora() * 60;
            //Minutos Transcurridos actualmente
            double minutos = this.getMinutos();

            //Total de minutos transcurridos
            double minutosTotales = hora + minutos;

            //minutos totales = 720
            //double compromisoPorMinutoPersonales =  compromisoHoyPersonales / 720;
            //double compromisoPorMinutoConsumo =  compromisoHoyConsumo / 720;
            //compromiso total al minuto
            //double compromisoAlMinutoPersonales = compromisoDiasAnterioresPersonales + (minutosTotales * compromisoPorMinutoPersonales);
            //double compromisoAlMinutoConsumo = compromisoDiasAnterioresConsumo + (minutosTotales * compromisoPorMinutoConsumo);
            double compromisoAlDiaPersonales = compromisoHoyPersonales;
            double compromisoAlDiaConsumo = compromisoHoyConsumo;

            double avanceTiempoRealPersonales = Double.parseDouble(realPersonalesObj.get("avance").getAsString());
            double avanceTiempoRealConsumo = Double.parseDouble(realConsumoObj.get("avance").getAsString());

            double porcentajePersonales = 0.0;
            double porcentajeConsumo = 0.0;

            //double compromisoSemanalPersonales = 0.0;
            //double compromisoSemanalConsumo = 0.0;
            /*for (int i = 0; i < 7; i++) {

             //PERSONALES
             //compromisoSemanalPersonales += Double.parseDouble(dataPersonales.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
             .get("cantidad").getAsString().replace(",", ""));

             //CONSUMO
             //compromisoSemanalConsumo += Double.parseDouble(dataConsumo.getAsJsonArray(dias[i]).get(3).getAsJsonObject()
             .get("cantidad").getAsString().replace(",", ""));

             }*/
 /*COMPROMISO AL DIA COMPLETO*/
            if (compromisoAlDiaPersonales != 0) {
                porcentajePersonales = (avanceTiempoRealPersonales * 100) / compromisoAlDiaPersonales;
            }

            if (compromisoAlDiaConsumo != 0) {
                porcentajeConsumo = (avanceTiempoRealConsumo * 100) / compromisoAlDiaConsumo;
            }

            /*COMPROMISO AL MINUTO*/
 /*if (compromisoAlMinutoPersonales != 0){

             porcentajePersonales = ( avanceTiempoRealPersonales * 100 ) / compromisoAlMinutoPersonales;
             }

             if (compromisoAlMinutoConsumo != 0){

             porcentajeConsumo = ( avanceTiempoRealConsumo * 100 ) / compromisoAlMinutoConsumo;
             }
             */
            porcentajePersonales = Math.round(porcentajePersonales);
            porcentajeConsumo = Math.round(porcentajeConsumo);

            String colorPersonales = "green";
            String colorConsumo = "green";

            int porPer = (int) porcentajePersonales;
            int porCon = (int) porcentajeConsumo;

            int intCompromisoAlDiaConsumo = (int) Math.round(compromisoAlDiaConsumo);
            int intCompromisoAlDiaPersonales = (int) Math.round(compromisoAlDiaPersonales);

            int intAvanceTiempoRealConsumo = (int) Math.round(avanceTiempoRealConsumo);
            int intAvanceTiempoRealPersonales = (int) Math.round(avanceTiempoRealPersonales);

            if (porPer >= 65) {
                colorPersonales = "green";
                //porPer = 100;
            } else if (porPer > 54 && porPer < 65) {
                colorPersonales = "yellow";
            } else {
                colorPersonales = "red";
            }

            if (porCon >= 65) {
                colorConsumo = "green";
                //porCon = 100;
            } else if (porCon > 54 && porCon < 65) {
                colorConsumo = "yellow";
            } else {
                colorConsumo = "red";
            }

            if (porPer < 0) {
                porPer = 0;
            }
            if (porCon < 0) {
                porCon = 0;
            }

            Map<String, String> response = new HashMap();

            response.put("valorConsumo", porCon + "");
            response.put("valorPersonales", porPer + "");

            response.put("colorConsumo", colorConsumo);
            response.put("colorPersonales", colorPersonales);

            response.put("compromisoConsumo", intCompromisoAlDiaConsumo + "");
            response.put("compromisoPersonales", intCompromisoAlDiaPersonales + "");

            if (intAvanceTiempoRealConsumo == -1000) {
                response.put("avanceTiempoRealConsumo", 0 + "");
            } else {
                response.put("avanceTiempoRealConsumo", intAvanceTiempoRealConsumo + "");
            }
            if (intAvanceTiempoRealPersonales == -1000) {
                response.put("avanceTiempoRealPersonales", 0 + "");
            } else {
                response.put("avanceTiempoRealPersonales", intAvanceTiempoRealPersonales + "");
            }

            return response;

        } catch (Exception e) {

            double compromisoAlDiaPersonales = 0;
            double compromisoAlDiaConsumo = 0;

            double avanceTiempoRealPersonales = 0;
            double avanceTiempoRealConsumo = 0;

            double porcentajePersonales = 0.0;
            double porcentajeConsumo = 0.0;

            double compromisoSemanalPersonales = 0.0;
            double compromisoSemanalConsumo = 0.0;

            //RANDOM
            //RANDOM
            compromisoSemanalPersonales = (Math.random() * 100);

            avanceTiempoRealPersonales = (Math.random() * 100);

            compromisoAlDiaPersonales = (Math.random() * 10000);

            compromisoSemanalConsumo = (Math.random() * 100);

            avanceTiempoRealConsumo = (Math.random() * 100);

            compromisoAlDiaConsumo = (Math.random() * 10000);
            /*COMPROMISO AL DIA COMPLETO*/

            if (compromisoSemanalPersonales != 0) {

                porcentajePersonales = (avanceTiempoRealPersonales * 100) / compromisoAlDiaPersonales;
            }

            if (compromisoSemanalConsumo != 0) {

                porcentajeConsumo = (avanceTiempoRealConsumo * 100) / compromisoAlDiaConsumo;
            }

            /*COMPROMISO AL MINUTO*/
 /*if (compromisoAlMinutoPersonales != 0){

             porcentajePersonales = ( avanceTiempoRealPersonales * 100 ) / compromisoAlMinutoPersonales;
             }

             if (compromisoAlMinutoConsumo != 0){

             porcentajeConsumo = ( avanceTiempoRealConsumo * 100 ) / compromisoAlMinutoConsumo;
             }
             */
            porcentajePersonales = Math.round(porcentajePersonales);
            porcentajeConsumo = Math.round(porcentajeConsumo);

            String colorPersonales = "green";
            String colorConsumo = "green";

            int porPer = (int) porcentajePersonales;
            int porCon = (int) porcentajeConsumo;

            int intCompromisoAlDiaConsumo = (int) Math.round(compromisoAlDiaConsumo);
            int intCompromisoAlDiaPersonales = (int) Math.round(compromisoAlDiaPersonales);

            int intAvanceTiempoRealConsumo = (int) Math.round(avanceTiempoRealConsumo);
            int intAvanceTiempoRealPersonales = (int) Math.round(avanceTiempoRealPersonales);

            if (porPer >= 65) {
                colorPersonales = "green";
                //porPer = 100;
            } else if (porPer > 54 && porPer < 65) {
                colorPersonales = "yellow";
            } else {
                colorPersonales = "red";
            }

            if (porCon >= 65) {
                colorConsumo = "green";
                //porCon = 100;
            } else if (porCon > 54 && porCon < 65) {
                colorConsumo = "yellow";
            } else {
                colorConsumo = "red";
            }

            if (porPer < 0) {
                porPer = 0;
            }
            if (porCon < 0) {
                porCon = 0;
            }

            Map<String, String> response = new HashMap();

            response.put("valorConsumo", porCon + "");
            response.put("valorPersonales", porPer + "");

            response.put("colorConsumo", colorConsumo);
            response.put("colorPersonales", colorPersonales);

            response.put("compromisoConsumo", intCompromisoAlDiaConsumo + "");
            response.put("compromisoPersonales", intCompromisoAlDiaPersonales + "");

            if (intAvanceTiempoRealConsumo == -1000) {
                response.put("avanceTiempoRealConsumo", 0 + "");
            } else {
                response.put("avanceTiempoRealConsumo", intAvanceTiempoRealConsumo + "");
            }
            if (intAvanceTiempoRealPersonales == -1000) {
                response.put("avanceTiempoRealPersonales", 0 + "");
            } else {
                response.put("avanceTiempoRealPersonales", intAvanceTiempoRealPersonales + "");
            }

            return response;
        }

    }

    // GET FECHA PROXIMO DOMINGO
    private String getNextSunday() {

        logger.info("SERVICIOS GET NEXT SUNDAY :");

        String fecha = "";
        Calendar c = Calendar.getInstance();
        // Set the calendar to monday of the current week
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        // Print dates of the current week starting on Monday
        DateFormat df = new SimpleDateFormat("EEE yyyyMMdd");
        for (int i = 0; i < 7; i++) {
            String dia = df.format(c.getTime());
            logger.info("DIA CON FORMATO:" + dia);

            if (dia.contains("Sun") || dia.contains("sun") || dia.contains("dom") || dia.contains("Dom")) {

                fecha = dia.substring(4, dia.length());
                logger.info("SERVICIOS GET NEXT SUNDAY UN DIA:");
                logger.info(fecha);

            }
            c.add(Calendar.DATE, 1);
        }

        logger.info("SERVICIOS GET NEXT SUNDAY FECHA DE RETORNO :" + fecha);
        return fecha;
    }

    private String getNameToday() {

        String hoy = "";

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {

            case Calendar.MONDAY:
                hoy = "lunes";
                break;
            case Calendar.TUESDAY:
                hoy = "martes";
                break;
            case Calendar.WEDNESDAY:
                hoy = "miercoles";
                break;
            case Calendar.THURSDAY:
                hoy = "jueves";
                break;
            case Calendar.FRIDAY:
                hoy = "viernes";
                break;
            case Calendar.SATURDAY:
                hoy = "sabado";
                break;
            case Calendar.SUNDAY:
                hoy = "domingo";
                break;
        }

        return hoy;

    }

    public String getPorcentajeAvance(double avance, double lograr) {

        // arrastre es lo que tengo que lograr
        double porcentaje = 0;

        if (lograr > 0) {

            porcentaje = (avance * 100) / lograr;

        }

        String color = getColor(porcentaje);

        return porcentaje + "," + color;

    }

    public String getColor(double porcentaje) {

        String color = "red";

        Calendar calendario = new GregorianCalendar();
        int hora = calendario.get(Calendar.HOUR_OF_DAY);

        switch (hora) {

            case 7:
                color = "green";
                break;
            case 8:
                color = "green";
                break;
            case 9:
                color = "green";
                break;
            case 10:
                if (porcentaje > 5) {
                    color = "green";
                }
                break;
            case 11:
                if (porcentaje > 15) {
                    color = "green";
                }
                break;
            case 12:
                if (porcentaje > 25) {
                    color = "green";
                }
                break;
            case 13:
                if (porcentaje > 30) {
                    color = "green";
                }
                break;
            case 14:
                if (porcentaje > 40) {
                    color = "green";
                }
                break;
            case 15:
                if (porcentaje > 45) {
                    color = "green";
                }
                break;
            case 16:
                if (porcentaje > 55) {
                    color = "green";
                }
                break;
            case 17:
                if (porcentaje > 60) {
                    color = "green";
                }
                break;
            case 18:
                if (porcentaje > 70) {
                    color = "green";
                }
                break;
            case 19:
                if (porcentaje > 80) {
                    color = "green";
                }
                break;
            case 20:
                if (porcentaje > 90) {
                    color = "green";
                }
                break;
            case 21:
                if (porcentaje > 99) {
                    color = "green";
                }
                break;

            default:

                if (porcentaje > 99) {
                    color = "green";
                } else {
                    color = "red";

                }

        }

        return color;

    }

    private double getHora() {
        int hora = 0;

        Calendar calendario = new GregorianCalendar();
        hora = calendario.get(Calendar.HOUR_OF_DAY) - 9;

        logger.info(hora);

        return hora;

    }

    private double getMinutos() {
        int minutos = 0;

        Calendar calendario = new GregorianCalendar();
        minutos = calendario.get(Calendar.MINUTE);

        logger.info(minutos);

        return minutos;
    }

    protected JsonArray limpiaXMLVersus(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            //logger.info("Columnas :"+columnas);
            //logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            //logger.info("Total GRID: "+ totalCeldas);
            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;

            int columnasdia = 0;

            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
                    //					    logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                    //						logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()) / 1000;
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;

                    if (conCelldata < celdas.size()) {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
                    //logger.info("------------FILA-------------" +numFilas);
                    int posicionReal;
                    //Agrega los versus
                    if (numFilas == 1 || numFilas == rowsCount) {
                        posicionReal = 5;
                    } else {
                        posicionReal = listValoresCeldas.size() - 2;
                    }

                    double valorReal;

                    if (numFilas == rowsCount) {
                        valorReal = listaValoresTotales.get(posicionReal);
                    } else {
                        valorReal = listValoresCeldas.get(posicionReal);
                    }

                    for (int i = 1; i <= 4; i++) {
                        double valorVs;

                        if (numFilas == rowsCount) {
                            valorVs = listaValoresTotales.get(posicionReal - i);
                        } else {
                            valorVs = listValoresCeldas.get(posicionReal - i);
                        }

                        //double valorCalculado = valorVs  - valorReal;
                        double valorCalculado = valorReal - valorVs;

                        //logger.info("Valor real "+valorReal);
                        //logger.info("Valor vs: "+ valorVs);
                        //logger.info("Valor Calculado: "+ valorVs);
                        if (numFilas == rowsCount) {
                            listaValoresTotales.add(valorCalculado);
                        } else {
                            listValoresCeldas.add(valorCalculado);
                        }

                    }
                    veces = 0;
                    numFilas++;

                }
                //logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //logger.info(arrayFilas.toString());
            //logger.info("Valores celda lista : " +listValoresCeldas.size());
            //logger.info("Valores totales lista : " +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", 11);

            int[] flags = {0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1};
            jsonArray = armaJsonVersus(valores, flags);

        } catch (Exception e) {
            //logger.info("Ap "+e.getMessage());
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonVersus(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###.#");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 3;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 7) {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                    //logger.info("cantidad: "+ valoresCeldas.get(i));
                    //logger.info("Cantidad vs"+ valoresCeldas.get( i - pos_vs));
                    // ));
                    pos_vs += 2;
                } else {
                    //logger.info("cantidad: "+ valoresCeldas.get(i));
                    //logger.info("Cantidad vs"+ valoresTotales.get(veces - 1));
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

                valor = new JsonObject();
                valor.addProperty("cantidad", valorStr);
                valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valor.addProperty("color", color);

                arrayValores.add(valor);
            }

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                //logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 3;
            }

        }

        /* Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 3;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 7) {
                    //logger.info("Valor total:" +valoresTotales.get(a));
                    //logger.info("Valor contra : "+valoresTotales.get(a - pos_vsTot));
                    porcentajeTot = ((double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot)) * 100.0;
                    pos_vsTot += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayValores.add(jsonTotales);

            }

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

    public JsonObject jsonDiasSemana(JsonArray array) {

        JsonObject respuesta = new JsonObject();

        for (int i = 1; i <= array.size(); i++) {
            respuesta.add(getdia(i), array.get(i - 1).getAsJsonArray());
        }

        return respuesta;

    }

    public JsonObject eliminaObjetos(JsonObject json) {

        JsonObject jsonLocal = json;

        return jsonLocal;

    }

    public static void main(String[] args) {

        //JsonObject json =  getNormalidad( "20190421","482789 - MEGA AKROPOLIS MERIDA","T");
        //System.out.println(json.toString());
    }

    public JsonObject getActivacionesBazDigital(String fecha, String geografia) {
        logger.info("---------ActivacionesBazDigital------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();
        try {
            String xmlStr = obtieneXmlResult(fecha, geografia, "ActivacionesBazDigital");
            //logger.info(xmlStr);
            jsonArray = limpiaXMLActivaciones(xmlStr);
            json = jsonDiasSemana(jsonArray);
            eliminaObjetos(json);
        } catch (Exception e) {
            logger.info("Ap getActivacionesBazDigital:" + e.getMessage());
            return null;
        }
        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("activacionesBazDigital", json);
        }

        return respuestaService;
    }

    public JsonObject getColocacionBazDigital(String fecha, String geografia, String nivel) {
        logger.info("---------PersonalesColocacionBAZDigital------------");
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();
        try {
            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, "PersonalesColocacionBAZDigital");
            logger.info(xmlStr);
            jsonArray = limpiaXMLColocacion(xmlStr);
            json = jsonDiasSemana(jsonArray);
            eliminaObjetos(json);
        } catch (Exception e) {
            logger.info("Ap getColocacionBAZDigital:" + e.getMessage());
            return null;
        }
        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("colocacionBAZDigital", json);
        }

        return respuestaService;
    }

    protected JsonArray limpiaXMLActivaciones(String xmlStr) {
        JsonArray jsonArray = new JsonArray();
        try {
            StringReader stringReader = new StringReader(xmlStr);
            SAXBuilder builder = new SAXBuilder();
            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();
            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();
            List<Element> axes0 = axes.getChildren();

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;
            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            int totalCeldas = columnas * rowsCount;
            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;
            int veces = 0;
            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
            List<BigDecimal> listValoresCeldas = new ArrayList<BigDecimal>();
            List<BigDecimal> listaValoresTotales = new ArrayList<BigDecimal>();

            BigDecimal valorDouble = new BigDecimal(0.00);
            //BigDecimal divisor = new BigDecimal(30000);
            BigDecimal divisor = new BigDecimal(1);
            int numFilas = 1;

            boolean finCeldas = false;

            while (cont < totalCeldas) {
                veces++;

                if (!finCeldas) {
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                } else {
                    celda2 = 9999999;
                }

                celda1 = cont;

                if (celda1 == celda2) {
                    valorDouble = new BigDecimal(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()).divide(divisor, 8, RoundingMode.HALF_UP);
                    cont++;
                    conCelldata++;

                    //celda1 = cont;
//					if (conCelldata == celdas.size()) {
//						celda2 = 9999;
//					} else {
//						celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
//						//conCelldata++;
//					}
                } else if (celda1 < celda2) {
                    cont++;
//					celda1 = cont;
                    valorDouble = new BigDecimal(0.00);
//					celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                }

                if (conCelldata == celdas.size()) {
                    finCeldas = true;
                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                if (veces == columnas) {
                    logger.info("------------FILA-------------" + numFilas);
                    int posicionReal;

                    if (numFilas == 1 || numFilas == rowsCount) {
                        posicionReal = 4;
                    } else {
                        posicionReal = listValoresCeldas.size() - 1;
                    }

                    BigDecimal valorReal = new BigDecimal(0.00);

                    if (numFilas == rowsCount) {
                        valorReal = listaValoresTotales.get(posicionReal);
                    } else {
                        valorReal = listValoresCeldas.get(posicionReal);
                    }

                    for (int i = 1; i <= 3; i++) {
                        BigDecimal valorVs = new BigDecimal(0.00);

                        if (numFilas == rowsCount) {
                            valorVs = listaValoresTotales.get(posicionReal - i);
                        } else {
                            valorVs = listValoresCeldas.get(posicionReal - i);
                        }

                        //BigDecimal valorCalculado = valorVs.subtract(valorReal);
                        BigDecimal valorCalculado = valorReal.subtract(valorVs);

                        logger.info("Valor real " + valorReal.toString());
                        logger.info("Valor vs: " + valorVs.toString());
                        logger.info("Valor Calculado: " + valorVs.toString());

                        if (numFilas == rowsCount) {
                            listaValoresTotales.add(valorCalculado);
                        } else {
                            listValoresCeldas.add(valorCalculado);
                        }
                    }
                    veces = 0;
                    numFilas++;
                }
            }
            int[] flags = {0, 1, 1, 1, 1, 1, 1, 1};
            jsonArray = armaJsonArrayActivaciones(listValoresCeldas, listaValoresTotales, flags, 8);
        } catch (Exception e) {
            //logger.info("Ap en limpiaXMLActivaciones:" + e.getMessage());
            return null;
        }
        return jsonArray;
    }

    /*protected JsonArray limpiaXMLActivaciones(String xmlStr) {
     JsonArray jsonArray = new JsonArray();
     try {
     StringReader stringReader = new StringReader(xmlStr);
     SAXBuilder builder = new SAXBuilder();
     Document document = (Document) builder.build(stringReader);
     Element rootNode = document.getRootElement();
     Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
     Element cellData = rootNode.getChild("CellData",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
     List<Element> celdas = cellData.getChildren();
     List<Element> axes0 = axes.getChildren();

     int columnas = 0;
     int rowsCount = 0;

     Element tuplesHeaders = null;
     List<Element> headers = null;
     Element tuplesRows = null;
     List<Element> rows = null;

     for (Element element : axes0) {
     Attribute atributo = element.getAttribute("name");
     if (atributo.getValue().equals("Axis0")) {
     tuplesHeaders = element.getChild("Tuples",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
     headers = tuplesHeaders.getChildren();
     columnas = headers.size();
     } else if (atributo.getValue().equals("Axis1")) {
     tuplesRows = element.getChild("Tuples",	Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
     rows = tuplesRows.getChildren();
     rowsCount = rows.size();
     }
     }

     int totalCeldas = columnas * rowsCount;
     logger.info("Total GRID: "+ totalCeldas);

     int cont = 0;
     int conCelldata = 0;
     int veces = 0;
     int celda1 = cont;
     int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
     List<BigDecimal> listValoresCeldas = new ArrayList<BigDecimal>();
     List<BigDecimal> listaValoresTotales = new ArrayList<BigDecimal>();

     BigDecimal valorDouble = new BigDecimal(0.00);
     //BigDecimal divisor = new BigDecimal(30000);
     BigDecimal divisor = new BigDecimal(1);
     int numFilas = 1;

     while (cont < totalCeldas) {
     veces++;

     if (celda1 == celda2) {
     valorDouble = new BigDecimal(celdas.get(conCelldata).getChild("FmtValue",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()).divide(divisor, 8 , RoundingMode.HALF_UP);
     cont++;
     conCelldata++;

     celda1 = cont;
     if (conCelldata == celdas.size()) {
     celda2 = 9999;
     } else {
     celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
     //conCelldata++;
     }

     } else if (celda1 < celda2) {
     cont++;
     celda1 = cont;
     valorDouble = new BigDecimal(0.00);
     celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
     }


     if (numFilas == (rowsCount)) {
     listaValoresTotales.add(valorDouble);
     } else {
     listValoresCeldas.add(valorDouble);
     }

     if (veces == columnas) {
     logger.info("------------FILA-------------" +numFilas);
     int posicionReal;

     if (numFilas == 1 || numFilas == rowsCount)
     posicionReal = 4;
     else
     posicionReal = listValoresCeldas.size() - 1;

     BigDecimal valorReal = new BigDecimal(0.00);

     if (numFilas == rowsCount)
     valorReal = listaValoresTotales.get(posicionReal);
     else
     valorReal = listValoresCeldas.get(posicionReal);

     for (int i = 1; i <= 3; i++) {
     BigDecimal valorVs = new BigDecimal(0.00);

     if (numFilas == rowsCount)
     valorVs = listaValoresTotales.get(posicionReal - i);
     else
     valorVs = listValoresCeldas.get(posicionReal - i);

     //BigDecimal valorCalculado = valorVs.subtract(valorReal);
     BigDecimal valorCalculado = valorReal.subtract(valorVs);

     logger.info("Valor real "+valorReal.toString());
     logger.info("Valor vs: "+ valorVs.toString());
     logger.info("Valor Calculado: "+ valorVs.toString());

     if (numFilas == rowsCount)
     listaValoresTotales.add(valorCalculado);
     else
     listValoresCeldas.add(valorCalculado);
     }
     veces = 0;
     numFilas++;
     }
     }
     int[] flags = { 0, 1, 1, 1, 1, 1, 1, 1};
     jsonArray = armaJsonArrayActivaciones(listValoresCeldas, listaValoresTotales, flags, 8);
     } catch (Exception e) {
     logger.info("Ap en limpiaXMLActivaciones:" + e.getMessage());
     return null;
     }
     return jsonArray;
     }*///BKP_METODO_ANTERIOIR
    protected JsonArray limpiaXMLColocacion(String xmlStr) {
        JsonArray jsonArray = new JsonArray();
        try {
            StringReader stringReader = new StringReader(xmlStr);
            SAXBuilder builder = new SAXBuilder();
            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();
            List<Element> axes0 = axes.getChildren();
            int columnas = 0;
            int rowsCount = 0;
            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            int totalCeldas = columnas * rowsCount;
            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;
            int veces = 0;
            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
            List<BigDecimal> listValoresCeldas = new ArrayList<BigDecimal>();
            List<BigDecimal> listaValoresTotales = new ArrayList<BigDecimal>();

            BigDecimal valorDouble = new BigDecimal(0.00);
            BigDecimal divisor = new BigDecimal(30000);

            int numFilas = 1;

            while (cont < totalCeldas) {

                veces++;

                if (celda1 == celda2) {
                    valorDouble = new BigDecimal(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()).divide(divisor, 8, RoundingMode.HALF_UP);
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {
                    cont++;
                    celda1 = cont;
                    valorDouble = new BigDecimal(0.00);
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                if (veces == columnas) {
                    logger.info("------------FILA-------------" + numFilas);

                    int posicionReal;

                    if (numFilas == 1 || numFilas == rowsCount) {
                        posicionReal = 5;
                    } else {
                        posicionReal = listValoresCeldas.size() - 2;
                    }

                    BigDecimal valorReal;

                    if (numFilas == rowsCount) {
                        valorReal = listaValoresTotales.get(posicionReal);
                    } else {
                        valorReal = listValoresCeldas.get(posicionReal);
                    }

                    for (int i = 1; i <= 4; i++) {
                        BigDecimal valorVs;

                        if (numFilas == rowsCount) {
                            valorVs = listaValoresTotales.get(posicionReal - i);
                        } else {
                            valorVs = listValoresCeldas.get(posicionReal - i);
                        }

                        //BigDecimal valorCalculado = valorVs.subtract(valorReal);
                        BigDecimal valorCalculado = valorReal.subtract(valorVs);

                        logger.info("Valor real " + valorReal.toString());
                        logger.info("Valor vs: " + valorVs.toString());
                        logger.info("Valor Calculado: " + valorVs.toString());

                        if (numFilas == rowsCount) {
                            listaValoresTotales.add(valorCalculado);
                        } else {
                            listValoresCeldas.add(valorCalculado);
                        }
                    }
                    veces = 0;
                    numFilas++;
                }
            }
            int[] flags = {0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1};
            jsonArray = armaJsonArrayColocacion(listValoresCeldas, listaValoresTotales, flags, 11);
        } catch (Exception e) {
            //logger.info("Ap en limpiaXMLColocacion:" + e.getMessage());
            return null;
        }
        return jsonArray;
    }

    protected JsonArray armaJsonArrayActivaciones(List<BigDecimal> valoresCeldas, List<BigDecimal> valoresTotales, int[] flagsColumnas, int columnas) {
        JsonArray arrayFilas = new JsonArray();
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;
        int veces = 0;
        int pos_vs = 2;

        for (int i = 0; i < valoresCeldas.size(); i++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonActivacion(flagsColumnas, valoresCeldas, valoresTotales, pos_vs, i, veces, true);
                pos_vs = veces > 5 ? pos_vs + 2 : pos_vs;
                arrayValores.add(valor);
            }
            if (veces == columnas) {
                logger.info("------------FILA-------------");
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                veces = 0;
                pos_vs = 2;
            }
        }
        pos_vs = 2;
        veces = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonActivacion(flagsColumnas, valoresTotales, null, pos_vs, a, veces, false);
                arrayValores.add(valor);
            }
        }
        arrayFilas.add(arrayValores);
        return arrayFilas;
    }

    protected JsonArray armaJsonArrayColocacion(List<BigDecimal> valoresCeldas, List<BigDecimal> valoresTotales, int[] flagsColumnas, int columnas) {
        int veces = 0;
        int pos_vs = 3;
        JsonArray arrayFilas = new JsonArray();
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        for (int i = 0; i < valoresCeldas.size(); i++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonColocacion(flagsColumnas, valoresCeldas, valoresTotales, pos_vs, i, veces, true);
                pos_vs = veces > 7 ? pos_vs + 2 : pos_vs;
                arrayValores.add(valor);
            }
            if (veces == columnas) {
                logger.info("------------FILA-------------");
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                veces = 0;
                pos_vs = 3;
            }
        }
        pos_vs = 3;
        veces = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                valor = mapearObjetoToJsonColocacion(flagsColumnas, valoresTotales, null, pos_vs, a, veces, false);
                pos_vs = veces > 7 ? pos_vs + 2 : pos_vs;
                arrayValores.add(valor);
            }
        }
        arrayFilas.add(arrayValores);

        return arrayFilas;
    }

    private JsonObject mapearObjetoToJsonActivacion(int[] flagsColumnas, List<BigDecimal> listaInicio, List<BigDecimal> valoresTotales, int pos_vs, int i, int veces, boolean tipoLista) {
        BigDecimal porcentaje = new BigDecimal(0.00);
        JsonObject valor = new JsonObject();
        BigDecimal porCiento = new BigDecimal(100.00);
        BigDecimal divisor = new BigDecimal(0);

        if (veces > 5) {
            divisor = listaInicio.get(i - pos_vs);
            if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                porcentaje = listaInicio.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                logger.info("cantidad: " + listaInicio.get(i).toString());
                logger.info("Cantidad vs" + divisor.toString());
            }
        } else {
            if (tipoLista) {
                divisor = valoresTotales.get(veces - 1);
                if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                    porcentaje = listaInicio.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                    logger.info("cantidad: " + listaInicio.get(i).toString());
                    logger.info("Cantidad vs" + divisor.toString());
                }
            } else {
                porcentaje = new BigDecimal(100.00);
            }
        }

        valor = crearObjetoJson(listaInicio.get(i), porcentaje);
        return valor;
    }

    private JsonObject mapearObjetoToJsonColocacion(int[] flagsColumnas, List<BigDecimal> valoresCeldas, List<BigDecimal> valoresTotales, int pos_vs, int i, int veces, boolean tipoLista) {
        JsonObject valor = null;
        BigDecimal divisor = new BigDecimal(0.00);
        BigDecimal porcentaje = new BigDecimal(0.00);
        BigDecimal porCiento = new BigDecimal(100.00);

        if (veces > 7) {
            divisor = valoresCeldas.get(i - pos_vs);
            if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                porcentaje = valoresCeldas.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                logger.info("cantidad: " + valoresCeldas.get(i).toString());
                logger.info("Cantidad vs" + divisor.toString());
            }
        } else {
            if (tipoLista) {
                divisor = valoresTotales.get(veces - 1);
                if (divisor.compareTo(BigDecimal.ZERO) > 0) {
                    porcentaje = valoresCeldas.get(i).divide(divisor, 8, RoundingMode.HALF_UP).multiply(porCiento);
                    logger.info("cantidad: " + valoresCeldas.get(i).toString());
                    logger.info("Cantidad vs" + divisor.toString());
                }
            } else {
                porcentaje = new BigDecimal(100.00);
            }
        }

        valor = crearObjetoJson(valoresCeldas.get(i), porcentaje);

        return valor;
    }

    private JsonObject crearObjetoJson(BigDecimal monto, BigDecimal porcentaje) {
        JsonObject valor = new JsonObject();
        DecimalFormat formateador = new DecimalFormat("###,###.##");

        String color = porcentaje.compareTo(BigDecimal.ZERO) > 0 ? "verde" : "rojo";
        String valorStr = formateador.format(monto.setScale(4, RoundingMode.HALF_UP));

        valor.addProperty("cantidad", valorStr);
        valor.addProperty("porcentaje", porcentaje.setScale(2, RoundingMode.HALF_UP).intValue() + "%");
        valor.addProperty("color", color);

        return valor;
    }

    public String obtieneXmlResult(String fecha, String geografia, String service) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();
        String strService = service;
        String cadena = "";
        String cadenaXml = "";

        try {
            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");
            rc = (HttpURLConnection) url.openConnection();
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);

            rc.setConnectTimeout(30000);
            rc.setReadTimeout(30000);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>" + "<soapenv:Body>"
                    + "<siew:" + strService + ">"
                    + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "</siew:" + strService + ">"
                    + "</soapenv:Body>" + "</soapenv:Envelope>";

            logger.info("PETICION SOA: " + xml);

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();

            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            cadena = strBuild.toString().trim();

            StringReader stringReader = new StringReader(cadena);

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();

            Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild(strService + "Response", Namespace.getNamespace("http://siewebservices/"));
            Element result = response.getChild(strService + "Result", Namespace.getNamespace("http://siewebservices/"));

            cadenaXml = result.getValue();

        } catch (Exception e) {
            //logger.info(" Ap en obtieneXmlResult: " + e.getMessage());
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }
        return cadenaXml;
    }

    public String getCaptacionHijos(String fecha, String geografia, String nivel) {

        return "";

    }

    /**
     * @param tipoCaptacion 1= Captacion Vista 2= Captacion Plazo
     *
     */
    public JsonObject getCaptaciones(String fecha, String geografia, String nivel, String tipoSaldo, int tipoCaptacion) {

        JsonArray json = null;
        JsonObject respuestaService = new JsonObject();

        String wsName = "";
        String nombrePropiedad = "";

        try {
            logger.info("Fecha :" + fecha);
            logger.info("Geografia :" + geografia);
            logger.info("Nivel :" + nivel);
            logger.info("Tipo Saldo :" + tipoSaldo);

            if (tipoCaptacion == 1) {
                nombrePropiedad = "captacionVista";
                wsName = "SaldoCaptacionVistaMetrica";
            } else {
                nombrePropiedad = "captacionPlazo";
                wsName = "SaldoCaptacionPlazoMetrica";
            }

            String xmlStr = obtieneXmlResult(fecha, geografia, nivel, tipoSaldo, wsName);
            logger.info(xmlStr);

            int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0};

            json = limpiXmlNewCaptaciones(xmlStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap Captaciones");
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add(nombrePropiedad + "_" + tipoSaldo, json);
        }

        return respuestaService;
    }

}
