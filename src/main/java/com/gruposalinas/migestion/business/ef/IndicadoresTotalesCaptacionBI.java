package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.business.CecoBI;
import com.gruposalinas.migestion.domain.CecoDTO;
import com.gruposalinas.migestion.resources.GTNAppContextProvider;
import com.gruposalinas.migestion.resources.GTNConstantes.rubrosNegocio;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IndicadoresTotalesCaptacionBI extends Thread {

    private Logger logger = LogManager.getLogger(IndicadoresTotalesCaptacionBI.class);
    private int procesoClase = 0;
    private String cecoClase = "";
    private int nivelHijo = 0;
    private String nivelClase = "";
    private String fechaClase = "";
    private JsonObject respuestaHilo = null;
    private JsonArray arrayHijos = null;
    private String negocioClase = "";
    private int idService = 0;
    private boolean isNoAbonadasClase = true;

    @Override
    public void run() {

        if (procesoClase == 1) {
            getIndicadores(fechaClase, cecoClase, nivelClase, idService);
        } else {
            arrayHijos = getIndicadoresHijos(cecoClase.substring(0, 6));
        }

    }

    public IndicadoresTotalesCaptacionBI() {

    }

    public IndicadoresTotalesCaptacionBI(int proceso, String fecha, String geografia, int nivel, String negocio, int idIndicador,
            boolean isNoAbonadas) {

        negocioClase = negocio;
        fechaClase = fecha;
        procesoClase = proceso;
        cecoClase = geografia;
        nivelHijo = nivel + 1;
        idService = idIndicador;
        isNoAbonadasClase = isNoAbonadas;

        switch (nivel) {
            case 1:
                nivelClase = "TR";
                break;
            case 2:
                nivelClase = "Z";
                break;
            case 3:
                nivelClase = "R";
                break;
            case 4:
                nivelClase = "T";
                break;
        }

    }

    public List<CecoDTO> obtieneHijos(String ceco) {

        List<CecoDTO> listaCecos = new ArrayList<>();

//		System.out.println("Negocio Clase :" + negocioClase);
        CecoBI cecoBi = (CecoBI) GTNAppContextProvider.getApplicationContext().getBean("cecoBI");
        listaCecos = cecoBi.buscaCecosSuperior(ceco, "" + negocioClase);

        System.out.println("****** TAMA�O LISTA *****:" + listaCecos.size());

        return listaCecos;
    }

    /*
     * Metodo que obtiene todos los indicadores de un Ceco en especifico
     *
     */
    public void getIndicadores(String fecha, String geografia, String nivel, int servicio) {

        JsonObject jsonGeneral = new JsonObject();
        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();

        ExtraccionCaptacionThreadNewBI2 hiloIndicadores = new ExtraccionCaptacionThreadNewBI2(servicio, fecha,
                geografia, nivel);

        try {
            long inicio = System.currentTimeMillis();
//			System.out.println("INICIA EJECUCION DE HILOS DE LA GEOGRAFIA  " + geografia + " HORA :" + new Date());

            hiloIndicadores.start();

            // Se ejecuta while para saber cuando
            while (hiloIndicadores.isAlive()) {
                /* Espera hasta que los hilos hayan terminado */

            }

            long acabo = System.currentTimeMillis();
            long totaltiempo = (acabo - inicio) / 1000;

            System.out.println("HORA EN LA QUE TERMINA LOS HILOS " + acabo);
//			System.out
//					.println("TERMINARON TODOS LOS HILOS EN " + totaltiempo + " SEGUNDOS DE LA GEOGRAFIA " + geografia);

            if (servicio == rubrosNegocio.captacionSaldoFinalVista.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionVista_SF").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "VSF");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionVista_SF").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.captacionNetaVista.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionVista_CN").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "VCN");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionVista_CN").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.numAperturasClientesNuevosVista.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionVista_NA").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "VNA");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionVista_NA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.montAperturasClientesNuevosVista.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionVista_MA").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "VMA");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionVista_MA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.captacionSaldoFinalPlazo.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionPlazo_SF").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "PSF");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionPlazo_SF").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.captacionNetaPlazo.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionPlazo_CN").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "PCN");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionPlazo_CN").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.numAperturasClientesNuevosPlazo.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionPlazo_NA").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "PNA");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionPlazo_NA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.montAperturasClientesNuevosPlazo.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().getAsJsonArray("captacionPlazo_MA").get(7).getAsJsonArray();

                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Consumo");
                total.addProperty("idService", servicio + "");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idService", servicio + "");
                jsonObject.addProperty("codigo", "PMA");
                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("captacionPlazo_MA").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (servicio == rubrosNegocio.transferenciasDEX.getValue() && hiloIndicadores.getRespuesta() != null) {

                JsonArray totales = hiloIndicadores.getRespuesta().get("transferenciasDEX").getAsJsonArray().get(7)
                        .getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                // total.addProperty("idService", "Dinero Express");
                total.addProperty("idService", servicio + "");

                // total.addProperty("indicador", color.equals("rojo") ? "-" +
                // indicador : indicador);
                // total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                // jsonObject.addProperty("idService", "Dinero Express");
                jsonObject.addProperty("idService", servicio + "");

                jsonObject.add("detalle", hiloIndicadores.getRespuesta().get("transferenciasDEX").getAsJsonArray());

                detalles.add(jsonObject);

            }

            // xml = extraccionFinancieraBI.getIndicadores(fecha, geografia,
            // nivel);
        } catch (Exception e) {
            logger.info("Ap en el servicio getIndicadores() " + e.getMessage());
            //e.printStackTrace();
            // respuestaHilo = null;
        }

        jsonGeneral.add("indicadores", indicadores);
        jsonGeneral.add("detalles", detalles);
        jsonGeneral.addProperty("ceco", cecoClase);

        respuestaHilo = jsonGeneral;

//		System.out.println(geografia + " Respuesta Hilo : " + jsonGeneral);
        // return jsonGeneral;
    }

    /*
     * Metodo que obtiene los indicadores de cada hijo
     */
    public JsonArray getIndicadoresHijos(String ceco) {

        JsonArray hijosDatos = null;
        List<CecoDTO> lista = obtieneHijos(ceco);
        JsonArray listaHijos = new JsonArray();

        try {

            List<IndicadoresTotalesCaptacionBI> arrayThreads = new ArrayList<IndicadoresTotalesCaptacionBI>();

            for (CecoDTO cecoDTO : lista) {
                String geografia = cecoDTO.getIdCeco() + " - " + cecoDTO.getDescCeco();
                IndicadoresTotalesCaptacionBI hilo = new IndicadoresTotalesCaptacionBI(1, fechaClase, geografia, nivelHijo, negocioClase,
                        idService, isNoAbonadasClase);
                arrayThreads.add(hilo);
                hilo.start();
            }

            /*
             * Ciclo que servira para verificar si todo los hilos ya terminaron
             */
            boolean terminaron = false;
            List<IndicadoresTotalesCaptacionBI> aElminar = new ArrayList<IndicadoresTotalesCaptacionBI>();
            while (!terminaron) {
                Iterator<IndicadoresTotalesCaptacionBI> it = arrayThreads.iterator();
                int cont = 0;

                // System.out.println("Contador :"+cont);
                if (cont == arrayThreads.size()) {
                    terminaron = true;
                }
                while (it.hasNext()) {
                    IndicadoresTotalesCaptacionBI hilo = it.next();
                    if (hilo.respuestaHilo != null) {
                        listaHijos.add(hilo.respuestaHilo);
                        System.out.println("Agregue CECO " + hilo.cecoClase);
                        aElminar.add(hilo);
                        cont++;
                    }
                }
                for (IndicadoresTotalesCaptacionBI eliminarHilo : aElminar) {
                    arrayThreads.remove(eliminarHilo);
                }
                aElminar.clear();
            }

            /*
             * Envia ArrayObject con los indicadores de cada hijo para generar
             * tabla de totales
             */
            hijosDatos = armaJsonHijos(listaHijos);

        } catch (Exception e) {
            logger.info("Ap en getIndicadoresHijos() ");
            logger.info("Exception para Ceco (" + ceco + ") " + e);

            return new JsonArray();
        }

        // System.out.println("Lista Respuesta " +listaHijos);
        return hijosDatos;

    }

    public JsonArray armaJsonHijos(JsonArray arrayHijos) {

        // int arrayServices[] = {6,5,7,9,8,15,19,18,20,17,1,14,16};
        JsonArray totalesCeco = null;
        DecimalFormat formateador = new DecimalFormat("###,###.#");

        JsonArray allServices = new JsonArray();

        // for (int i : arrayServices) {
        JsonArray tabla = new JsonArray();
        JsonObject service = new JsonObject();
        if (isNoAbonadasClase) {
            service.addProperty("idService", "16");
        } else {
            service.addProperty("idService", idService);
        }

        double arrayAcumulado1[] = new double[11];
        double arrayAcumuladosPercent[] = new double[11];

        for (JsonElement hijo : arrayHijos) {

            JsonObject fila = new JsonObject();
            System.out.println("Ceco" + hijo.getAsJsonObject().get("ceco").getAsString());
            fila.addProperty("ceco", hijo.getAsJsonObject().get("ceco").getAsString());

            totalesCeco = buscaTotalesService(idService, hijo);

            System.out.println("Totales : " + totalesCeco.size());

            fila.add("valores", totalesCeco);

            int cont = 0;
            for (JsonElement elemento : totalesCeco) {
                arrayAcumulado1[cont] += Double
                        .parseDouble(elemento.getAsJsonObject().get("cantidad").getAsString().replace(",", ""));
                arrayAcumuladosPercent[cont] += Double
                        .parseDouble(elemento.getAsJsonObject().get("porcentaje").getAsString().replace("%", ""));
                cont++;
            }

            System.out.println("Fila" + fila);
            tabla.add(fila);
        }

        JsonArray acumulados = new JsonArray();

        for (int d = 0; d < arrayAcumulado1.length; d++) {
            JsonObject acumulado = new JsonObject();

            acumulado.addProperty("cantidad", formateador.format(arrayAcumulado1[d]));
            acumulado.addProperty("porcentaje", arrayAcumuladosPercent[d]);
            if (arrayAcumulado1[d] >= 0) {
                acumulado.addProperty("color", "verde");
            } else {
                acumulado.addProperty("color", "rojo");
            }

            acumulados.add(acumulado);
        }

        service.add("tabla", tabla);
        service.add("totales", acumulados);
        allServices.add(service);
        // }
        System.out.println("Respuesta" + allServices);

        return allServices;
    }

    public JsonArray buscaTotalesService(int idService, JsonElement json) {

        System.out.println("*************¿ EL SERVICIO ES 16 ?  ************** " + isNoAbonadasClase);

        JsonArray detalles = json.getAsJsonObject().get("detalles").getAsJsonArray();
        JsonArray totales = new JsonArray();
        JsonObject detalle = null;
        JsonArray detalleArray = null;

        try {

            System.out.println("TAMAÑO detalles :" + detalles.size());

            for (JsonElement jsonElement : detalles) {

                int service = Integer.parseInt(jsonElement.getAsJsonObject().get("idService").getAsString());

                System.out.println("Indicador Interno " + service);

                detalleArray = jsonElement.getAsJsonObject().get("detalle").getAsJsonArray();

            }

            if (detalle != null || detalleArray != null) {

                totales = detalleArray.get(7).getAsJsonArray();

            }

            // for (JsonElement jsonElement : totales) {
            // System.out.println(jsonElement.getAsJsonObject().get("cantidad").getAsString());
            // }
        } catch (Exception e) {
            logger.info("Ap en buscaTotalesService() " + e);
        }

        return totales;
    }

    public JsonArray getTotalesObject(JsonObject detalle) {

        JsonArray totales = new JsonArray();

        totales.add(detalle);

        return totales;

    }

    public JsonObject getJsonFinal(String fecha, String geografia, String nivelStr, String negocio, int idIndicador) {

        int nivel = 0;

        if (nivelStr.equals("TR")) {
            nivel = 1;
        } else if (nivelStr.equals("Z")) {
            nivel = 2;
        } else if (nivelStr.equals("R")) {
            nivel = 3;
        } else if (nivelStr.equals("T")) {
            nivel = 4;
        }

        boolean isNoAbnonadas = false;

        if (idIndicador == 16) {
            isNoAbnonadas = true;
            idIndicador = 14;
        }

        IndicadoresTotalesCaptacionBI proceso1 = new IndicadoresTotalesCaptacionBI(2, fecha, geografia, nivel, negocio, idIndicador,
                isNoAbnonadas);

        proceso1.start();

        while (proceso1.isAlive()) {
            /* Se ejecutaran mientras lo hilos sigan vivos los hilos */

        }

        JsonObject resultadoFinal = new JsonObject();
        resultadoFinal.add("respuesta", proceso1.arrayHijos);

        System.out.println("Json Final " + resultadoFinal);

        return resultadoFinal;
    }

    public JsonObject getTablaSemana(String fecha, String geografia, String nivelStr, String negocio, int idIndicador) {

        int nivel = 0;

        if (nivelStr.equals("TR")) {
            nivel = 1;
        } else if (nivelStr.equals("Z")) {
            nivel = 2;
        } else if (nivelStr.equals("R")) {
            nivel = 3;
        } else if (nivelStr.equals("T")) {
            nivel = 4;
        }

        boolean isNoAbonadas = false;

        if (idIndicador == 16) {
            isNoAbonadas = true;
            idIndicador = 14;
        }
        IndicadoresTotalesCaptacionBI proceso1 = new IndicadoresTotalesCaptacionBI(1, fecha, geografia, nivel, negocio, idIndicador,
                isNoAbonadas);

        proceso1.start();

        while (proceso1.isAlive()) {
            /* Se ejecutaran mientras lo hilos sigan vivos los hilos */
        }

        System.out.println(proceso1.respuestaHilo.toString());

        return proceso1.respuestaHilo;

    }

}
