package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonObject;
import com.gruposalinas.migestion.resources.GTNConstantes;

public class ExtraccionCaptacionThreadNewBI2 extends Thread {

    //ExtraccionFinancieraBI bussines = new ExtraccionFinancieraBI();
    IndicadoresCaptacionNewBI bussines = new IndicadoresCaptacionNewBI();

    private int idService;
    private String fecha;
    private String geografia;
    private String nivel;
    private boolean termino = false;
    private JsonObject respuesta;

    public boolean isTermino() {
        return termino;
    }

    public void setTermino(boolean termino) {
        this.termino = termino;
    }

    public JsonObject getRespuesta() {
        return respuesta;
    }

    public ExtraccionCaptacionThreadNewBI2(int idService, String fecha, String geografia, String nivel) {

        this.idService = idService;
        this.fecha = fecha;
        this.geografia = geografia;
        this.nivel = nivel;

    }

    @Override
    public void run() {

        GTNConstantes.rubrosNegocio rubro = GTNConstantes.rubrosNegocio.getType(idService);

        switch (rubro) {

            case captacionSaldoFinalVista:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "SF", 1);
                break;
            case captacionNetaVista:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "CN", 1);
                break;
            case numAperturasClientesNuevosVista:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "NA", 1);
                break;
            case montAperturasClientesNuevosVista:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "MA", 1);
                break;
            case captacionSaldoFinalPlazo:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "SF", 2);
                break;
            case captacionNetaPlazo:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "CN", 2);
                break;
            case numAperturasClientesNuevosPlazo:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "NA", 2);
                break;
            case montAperturasClientesNuevosPlazo:
                respuesta = bussines.getCaptaciones(fecha, geografia, nivel, "MA", 2);
                break;
            case transferenciasDEX:
                respuesta = bussines.getDineroExpress(fecha, geografia, nivel);
                break;
            default:
                break;
        }

        termino = true;

        //System.out.println("Servicio " + idService + "termino: "+termino);
    }

}
