package com.gruposalinas.migestion.business.ef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.migestion.cliente.ExtraccionFinancieraStub;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class ExtraccionFinancieraBI {

    private Logger logger = LogManager.getLogger(ExtraccionFinancieraBI.class);

    /**
     * @param fecha El parametro de fecha;
     * @param geografia Ceco del cual se quiere obtener informacion;
     * @param nivel TR=Terriorial, Z=Zona, T=Tienda;
     */
    public JsonObject getIndicadores(String fecha, String geografia, String nivel) {

        JsonObject jsonGeneral = new JsonObject();

        JsonArray detalles = new JsonArray();
        JsonArray indicadores = new JsonArray();
        try {

            long inicio = System.currentTimeMillis();
            JsonObject consumoColocacion = getConsumoColocacion(fecha, geografia, nivel);
            JsonObject personalesColocacion = getPersonalesColocacion(fecha, geografia, nivel);
            JsonObject tazColocacion = getTAZColocacion(fecha, geografia, nivel);
            JsonObject saldoCaptacionPlazo = getSaldoCaptacionPlazo(fecha, geografia, nivel);
            JsonObject saldocaptacionVista = getSaldoCaptacionVista(fecha, geografia, nivel);
            JsonObject normalidad = getNormalidad(fecha, geografia, nivel);
            JsonObject pagosServicios = getPagosDeServicio(fecha, geografia, nivel);
            JsonObject transferenciasInternacionales = getTransferciasInternacionales(fecha, geografia, nivel);
            JsonObject transferenciasDEX = getDineroExpress(fecha, geografia, nivel);
            JsonObject transferenciasCambios = getCambios(fecha, geografia, nivel);
            JsonObject contribucion = getContribucion(fecha, geografia, nivel);
            JsonObject noAbonadasPend = getNoAbonadasPend(fecha, geografia, nivel);

            long fin = System.currentTimeMillis();

            long totaTiempo = (fin - inicio) / 1000;

            logger.info("Tiempo de ejecucion : " + totaTiempo + " Segundos");

            if (consumoColocacion != null) {

                JsonArray totales = consumoColocacion.get("consumoColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Consumo");
                total.addProperty("idService", "6");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Consumo");
                jsonObject.addProperty("idService", "6");
                jsonObject.addProperty("codigo", "cons");
                jsonObject.add("detalle", consumoColocacion.get("consumoColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (personalesColocacion != null) {

                JsonArray totales = personalesColocacion.get("personalesColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Personales");
                total.addProperty("idService", "5");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Personales");
                jsonObject.addProperty("idService", "5");
                jsonObject.add("detalle", personalesColocacion.get("personalesColocacion").getAsJsonObject());

                detalles.add(jsonObject);
                ;
            }

            if (tazColocacion != null) {
                JsonArray totales = tazColocacion.get("tazColocacion").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(5).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(5).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                //total.addProperty("idService", "TAZ");
                total.addProperty("idService", "7");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "TAZ");
                jsonObject.addProperty("idService", "7");
                jsonObject.addProperty("codigo", "taz");
                jsonObject.add("detalle", tazColocacion.get("tazColocacion").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (saldoCaptacionPlazo != null) {

                JsonArray totales = saldoCaptacionPlazo.get("saldoCaptacionPlazo").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Saldo Captacion Plazo");
                total.addProperty("idService", "9");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Saldo Captacion Plazo");
                jsonObject.addProperty("idService", "9");

                jsonObject.add("detalle", saldoCaptacionPlazo.get("saldoCaptacionPlazo").getAsJsonObject());

                detalles.add(jsonObject);

            }
            if (saldocaptacionVista != null) {

                JsonArray totales = saldocaptacionVista.get("saldoCaptacionVista").getAsJsonObject().get("totales")
                        .getAsJsonArray();
                String indicador = totales.get(4).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(4).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Saldo Captacion Vista");
                total.addProperty("idService", "8");

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Saldo Captacion Vista");
                jsonObject.addProperty("idService", "8");

                jsonObject.add("detalle", saldocaptacionVista.get("saldoCaptacionVista").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (normalidad != null) {

                JsonArray totales = normalidad.get("normalidad").getAsJsonObject().get("Vigente De 0 A 1").getAsJsonArray();

                String indicador = totales.get(2).getAsJsonObject().get("porcentaje").getAsString();

                String color = totales.get(0).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Normalidad");
                total.addProperty("idService", "15");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Normalidad");
                jsonObject.addProperty("idService", "15");

                jsonObject.add("detalle", normalidad.get("normalidad").getAsJsonObject());

                detalles.add(jsonObject);

            }

            if (pagosServicios != null) {

                JsonArray totales = pagosServicios.get("pagosServicios").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();

                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Pago de Servicios");
                total.addProperty("idService", "19");
                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);

                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Pago de Servicios");
                jsonObject.addProperty("idService", "19");

                jsonObject.add("detalle", pagosServicios.get("pagosServicios").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (transferenciasInternacionales != null) {

                JsonArray totales = transferenciasInternacionales.get("transferenciasInternacionales").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Transferencias Internacionales");
                total.addProperty("idService", "18");

                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Transferencias Internacionales");
                jsonObject.addProperty("idService", "18");
                jsonObject.add("detalle", transferenciasInternacionales.get("transferenciasInternacionales").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (transferenciasDEX != null) {

                JsonArray totales = transferenciasDEX.get("transferenciasDEX").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Dinero Express");
                total.addProperty("idService", "17");

                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Dinero Express");
                jsonObject.addProperty("idService", "17");

                jsonObject.add("detalle", transferenciasDEX.get("transferenciasDEX").getAsJsonArray());

                detalles.add(jsonObject);

            }
            if (transferenciasCambios != null) {

                JsonArray totales = transferenciasCambios.get("transferenciasCambios").getAsJsonArray().get(7).getAsJsonArray();
                String indicador = totales.get(6).getAsJsonObject().get("porcentaje").getAsString();
                String color = totales.get(6).getAsJsonObject().get("color").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Cambios Divisa");
                total.addProperty("idService", "20");

                //			total.addProperty("indicador", color.equals("rojo") ? "-" + indicador : indicador);
                //			total.addProperty("color", color);
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Cambios Divisa");
                jsonObject.addProperty("idService", "20");
                jsonObject.add("detalle", transferenciasCambios.get("transferenciasCambios").getAsJsonArray());

                detalles.add(jsonObject);

            }

            if (contribucion != null) {

                String crecimientoSrt = contribucion.get("contribucion").getAsJsonObject().get("crecimiento").getAsString();
                double crecimiento = Double.parseDouble(crecimientoSrt);

                int percent = (int) Math.round(crecimiento);

                String indicador = "" + percent + "%";
                String color = percent >= 1 ? "verde" : "rojo";
                //String color = contribucion.get("contribucion").getAsJsonObject().get("imagen").getAsString();
                JsonObject total = new JsonObject();
                //total.addProperty("idService", "Contribucion");
                total.addProperty("idService", "1");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Contribucion");
                jsonObject.addProperty("idService", "1");
                jsonObject.add("detalle", contribucion.get("contribucion").getAsJsonObject());

                detalles.add(jsonObject);
            }

            if (noAbonadasPend != null) {

                JsonObject total = null;

                //Pendientes por surtir
                int pendSurtidas = Integer.parseInt(noAbonadasPend.get("pendientesSurtrir").getAsJsonObject().get("pendientesSurtir").getAsString());
                String indicador = "" + pendSurtidas;
                String color = pendSurtidas > 0 ? "rojo" : "verde";

                total = new JsonObject();
                //total.addProperty("idService", "Pendientes por Surtir");
                total.addProperty("idService", "14");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("idService", "Pendientes por Surtir");
                jsonObject.addProperty("idService", "14");

                jsonObject.add("detalle", noAbonadasPend.get("pendientesSurtrir").getAsJsonObject());

                detalles.add(jsonObject);

                //Nunca Abonados
                int nuncaAbondas = Integer.parseInt(noAbonadasPend.get("nuncaAbonadas").getAsJsonObject().get("noAbonadas").getAsString());
                int totalCuentas = Integer.parseInt(noAbonadasPend.get("nuncaAbonadas").getAsJsonObject().get("totalCuentas").getAsString());
                int percent = 0;

                if (totalCuentas > 0) {
                    percent = nuncaAbondas / totalCuentas;
                }

                indicador = "" + percent + "%";
                color = percent >= 3 ? "verde" : "rojo";

                total = new JsonObject();
                //total.addProperty("idService", "Nunca Abonadas");
                total.addProperty("idService", "16");
                total.addProperty("indicador", indicador);
                total.addProperty("color", color);

                indicadores.add(total);

                jsonObject = new JsonObject();
//				jsonObject.addProperty("idService", "Nunca abonadas");
                jsonObject.addProperty("idService", "16");
                jsonObject.addProperty("codigo", "noabo");
                jsonObject.add("detalle", noAbonadasPend.get("nuncaAbonadas").getAsJsonObject());

                detalles.add(jsonObject);

            }

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }
        jsonGeneral.add("indicadores", indicadores);
        jsonGeneral.add("detalles", detalles);

        return jsonGeneral;

    }

    public JsonObject getConsumoColocacion(String fecha, String geografia, String nivel) {

        logger.info("---------COLOCACION------------");

        ExtraccionFinancieraStub.ConsumoColocacion request = null;
        ExtraccionFinancieraStub.ConsumoColocacionResponse response = null;

        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.ConsumoColocacion();
            response = new ExtraccionFinancieraStub.ConsumoColocacionResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            response = financieraStub.consumoColocacion(request);

            String xlmStr = response.getConsumoColocacionResult();
            //logger.info(response.getConsumoColocacionResult());

            json = clearXml(xlmStr);

        } catch (Exception e) {
            logger.info("Ap " + e);
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("consumoColocacion", json);
        }

        return respuestaService;

    }

    //CONSUMO PERSONALES PARA TIEMPO REAL
    //METOSO PERSONALES TIEMPO REAL
    public JsonObject getConsumoColocacionTiempoRealXML(String fecha, String geografia, String nivel) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        //Cambio Uriel
        //JsonObject json = null;
        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<siew:PersonalesColocacion>"
                    + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "<siew:nivel>" + nivel + "</siew:nivel>"
                    + "</siew:PersonalesColocacion>"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();
            } finally {
                wr.close();
            }

            //rd = null;
            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();

        } catch (Exception e) {
            logger.info("Ap al consumir xmlColocacionPersonales");
            //return  null;
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        String cadena = strBuild.toString().trim();
        //logger.info("getPersonalesColocacionXML: fecha: " + fecha + ", geografia: " + geografia + ", nivel : " + nivel + " result =  " + cadena);

        if (cadena.contains("No se encontro informacion con los parametros especificados")) {

            //Cambio Uriel
            //json = null;
            jsonArray = null;

        } else {

            StringReader stringReader = new StringReader(cadena);

            try {

                SAXBuilder builder = new SAXBuilder();
                Document document = builder.build(stringReader);

                Element rootNode = document.getRootElement();

                Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
                Element response = body.getChild("PersonalesColocacionResponse", Namespace.getNamespace("http://siewebservices/"));
                Element result = response.getChild("PersonalesColocacionResult", Namespace.getNamespace("http://siewebservices/"));

                String resultado = result.getValue();

                //Cambio Uriel
                //json = clearXml(resultado);
                int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
                jsonArray = limpiaXMLVersus(resultado, flagsColumnas);
                json = jsonDiasSemana(jsonArray);

            } catch (Exception e) {
                logger.info("Ap colocacionPersonalesXML armar XML y JSON");
                return null;
            }

        }

        //Cambio Uriel
        /*if(json == null)
         respuestaService = null;
         else
         respuestaService.add("consumoColocacion", json);*/
        if (jsonArray == null) {
            respuestaService = null;
        } else {
            json = jsonDiasSemana(jsonArray);

            respuestaService.add("consumoColocacion", json);
        }

        return respuestaService;

    }
    //CONUSMO COLOCACION PARA TIEMPO REAL
    //CONSUMO

    public JsonObject getConsumoColocacionXML(String fecha, String geografia, String nivel) {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        StringBuilder strBuild = new StringBuilder();

        JsonArray jsonArray = null;
        JsonObject json = null;
        JsonObject respuestaService = new JsonObject();

        try {

            URL url = new URL(GTNConstantes.getURLExtraccionFinanciera() + "?WSDL");
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            //logger.info("---------FECHA------------" + fecha);
            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:siew=\"http://siewebservices/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<siew:ConsumoColocacion>"
                    + "<siew:fecha>" + fecha + "</siew:fecha>"
                    + "<siew:geografia>" + geografia + "</siew:geografia>"
                    + "<siew:nivel>" + nivel + "</siew:nivel>"
                    + "</siew:ConsumoColocacion>"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();
            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();
            } finally {
                wr.close();
            }

            //rd = null;
            InputStream inputSream = rc.getInputStream();
            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();

        } catch (Exception e) {
            logger.info("Ap xmlColocación no respondió");
            //return  null;
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }

            } catch (Exception e) {

            }
        }

        String cadena = strBuild.toString().trim();
        //logger.info("getConsumoColocacionXML: fecha: " + fecha + ", geografia: " + geografia + ", nivel : " + nivel + " result =  " + cadena);

        //contains asqsq **********
        if (cadena.contains("No se encontro informacion con los parametros especificados")) {

            jsonArray = null;

        } else {

            StringReader stringReader = new StringReader(cadena);

            try {

                SAXBuilder builder = new SAXBuilder();
                Document document = builder.build(stringReader);

                Element rootNode = document.getRootElement();

                Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
                Element response = body.getChild("ConsumoColocacionResponse", Namespace.getNamespace("http://siewebservices/"));
                Element result = response.getChild("ConsumoColocacionResult", Namespace.getNamespace("http://siewebservices/"));

                String resultado = result.getValue();

                //json = clearXml(resultado);
                /*	int[] flagsColumnas = { 0,1,1,1,1,1,0 };
                 jsonArray = limpiXmlNew(resultado, flagsColumnas);*/
                int[] flagsColumnas = {0, 1, 1, 1, 1, 1, 0};
                jsonArray = limpiaXMLVersus(resultado, flagsColumnas);
                json = jsonDiasSemana(jsonArray);
            } catch (Exception e) {
                logger.info("Ap xmlColocación armarXML y JSON");
                return null;
            }

        }

        if (jsonArray == null) {
            respuestaService = null;
        } else {
            json = jsonDiasSemana(jsonArray);

            //logger.info("**************ARRAY NUEVO**********"+json);
            respuestaService.add("consumoColocacion", json);
        }
        return respuestaService;

    }

    public JsonObject getPersonalesColocacion(String fecha, String geografia, String nivel) {

        logger.info("---------PERSONALES------------");

        ExtraccionFinancieraStub.PersonalesColocacion request = null;
        ExtraccionFinancieraStub.PersonalesColocacionResponse response = null;

        JsonObject json = null;

        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.PersonalesColocacion();
            response = new ExtraccionFinancieraStub.PersonalesColocacionResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + fecha);
            logger.info("Geografia :" + geografia);
            logger.info("Nivel :" + nivel);

            response = financieraStub.personalesColocacion(request);
            String xlmStr = response.getPersonalesColocacionResult();
            //logger.info(response.getPersonalesColocacionResult());

            json = clearXml(xlmStr);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("personalesColocacion", json);
        }

        return respuestaService;

    }

    public JsonObject getTAZColocacion(String fecha, String geografia, String nivel) {

        logger.info("---------TAZ------------");

        ExtraccionFinancieraStub.TAZColocacion request = null;
        ExtraccionFinancieraStub.TAZColocacionResponse response = null;

        JsonObject json = null;

        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.TAZColocacion();
            response = new ExtraccionFinancieraStub.TAZColocacionResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + fecha);
            logger.info("Geografia :" + geografia);
            logger.info("Nivel :" + nivel);

            response = financieraStub.tAZColocacion(request);
            String xlmStr = response.getTAZColocacionResult();
            //logger.info(response.getTAZColocacionResult());

            json = clearXml(xlmStr);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("tazColocacion", json);
        }

        return respuestaService;

    }

    public JsonObject getSaldoCaptacionPlazo(String fecha, String geografia, String nivel) {

        logger.info("---------CAPTACION PLAZO------------");

        ExtraccionFinancieraStub.SaldoCaptacionPlazo request = null;
        ExtraccionFinancieraStub.SaldoCaptacionPlazoResponse response = null;

        JsonObject json = null;

        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.SaldoCaptacionPlazo();
            response = new ExtraccionFinancieraStub.SaldoCaptacionPlazoResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + fecha);
            logger.info("Geografia :" + geografia);
            logger.info("Nivel :" + nivel);

            response = financieraStub.saldoCaptacionPlazo(request);
            String xlmStr = response.getSaldoCaptacionPlazoResult();
            //logger.info(response.getSaldoCaptacionPlazoResult());

            int[] flagsCaptacionPlazo = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};

            json = limpiaXmlCaptacion(xlmStr, flagsCaptacionPlazo);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("saldoCaptacionPlazo", json);
        }

        return respuestaService;

    }

    public JsonObject getSaldoCaptacionVista(String fecha, String geografia, String nivel) {

        logger.info("---------CAPTACION VISTA------------");

        ExtraccionFinancieraStub.SaldoCaptacionVista request = null;
        ExtraccionFinancieraStub.SaldoCaptacionVistaResponse response = null;

        JsonObject json = null;

        JsonObject respuestaService = new JsonObject();

        try {

            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.SaldoCaptacionVista();
            response = new ExtraccionFinancieraStub.SaldoCaptacionVistaResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + fecha);
            logger.info("Geografia :" + geografia);
            logger.info("Nivel :" + nivel);

            response = financieraStub.saldoCaptacionVista(request);
            String xlmStr = response.getSaldoCaptacionVistaResult();
            //logger.info(response.getSaldoCaptacionVistaResult());

            int[] flagsGuardadito = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};

            json = limpiaXmlCaptacion(xlmStr, flagsGuardadito);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("saldoCaptacionVista", json);
        }

        return respuestaService;

    }

    public JsonObject getNormalidad(String fecha, String geografia, String nivel) {

        logger.info("---------NORMALIDAD------------");

        ExtraccionFinancieraStub.Normalidad request = null;
        ExtraccionFinancieraStub.NormalidadResponse response = null;

        JsonObject json = null;
        JsonArray jsonp = null;

        JsonObject respuestaService = new JsonObject();

        try {

            DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
            Date date = formatDate.parse(fecha);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 1);

            logger.info("Fecha para la normalidad : " + calendar.getTime());

            int dia = calendar.get(Calendar.DAY_OF_WEEK);

            if (dia == 1) {
                calendar.add(Calendar.DATE, -7);
            } else {
                calendar.add(Calendar.DATE, -(dia - 1));
            }

            String semana = formatDate.format(calendar.getTime());

            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.Normalidad();
            response = new ExtraccionFinancieraStub.NormalidadResponse();

            request.setFecha(semana);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + request.getFecha());
            logger.info("Geografia :" + request.getGeografia());
            logger.info("Nivel :" + request.getNivel());

            response = financieraStub.normalidad(request);
            String xlmStr = response.getNormalidadResult();
            //logger.info(response.getNormalidadResult());

            //logger.info(xlmStr);
            //json = limpiXmlNormalidad(xlmStr);
            int[] flagsColumnas = {1, 1, 1, 1, 0, 1, 0};
            jsonp = limpiXmlNew(xlmStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        if (jsonp == null) {
            respuestaService = null;
        } else {
            respuestaService.add("normalidad", jsonp);
        }

        return respuestaService;

    }

    public JsonObject getPagosDeServicio(String fecha, String geografia, String nivel) {

        logger.info("---------Pago de Servicios------------");

        ExtraccionFinancieraStub.TransferenciasPagoServicios request = null;
        ExtraccionFinancieraStub.TransferenciasPagoServiciosResponse response = null;

        JsonArray json = null;

        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.TransferenciasPagoServicios();
            response = new ExtraccionFinancieraStub.TransferenciasPagoServiciosResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + request.getFecha());
            logger.info("Geografia :" + request.getGeografia());
            logger.info("Nivel :" + request.getNivel());

            response = financieraStub.transferenciasPagoServicios(request);
            String xlmStr = response.getTransferenciasPagoServiciosResult();

            //logger.info(response.getTransferenciasPagoServiciosResult());
            //logger.info(xlmStr);
            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xlmStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap ");
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("pagosServicios", json);
        }

        return respuestaService;
    }

    public JsonObject getTransferciasInternacionales(String fecha, String geografia, String nivel) {

        logger.info("---------Transferencias Internacionales------------");

        ExtraccionFinancieraStub.TransferenciasInternacionales request = null;
        ExtraccionFinancieraStub.TransferenciasInternacionalesResponse response = null;

        JsonArray json = null;

        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.TransferenciasInternacionales();
            response = new ExtraccionFinancieraStub.TransferenciasInternacionalesResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + request.getFecha());
            logger.info("Geografia :" + request.getGeografia());
            logger.info("Nivel :" + request.getNivel());

            response = financieraStub.transferenciasInternacionales(request);
            String xlmStr = response.getTransferenciasInternacionalesResult();

            //logger.info(response.getTransferenciasInternacionalesResult());
            //logger.info(xlmStr);
            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xlmStr, flagsColumnas);

        } catch (Exception e) {
            //logger.info("Ap "+e);
            //logger.info("ENTRE A EXCEPCION");
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasInternacionales", json);
        }

        return respuestaService;
    }

    public JsonObject getDineroExpress(String fecha, String geografia, String nivel) {

        logger.info("---------Dinero Express------------");

        ExtraccionFinancieraStub.TranferenciasDEX request = null;
        ExtraccionFinancieraStub.TranferenciasDEXResponse response = null;

        JsonArray json = null;

        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.TranferenciasDEX();
            response = new ExtraccionFinancieraStub.TranferenciasDEXResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + request.getFecha());
            logger.info("Geografia :" + request.getGeografia());
            logger.info("Nivel :" + request.getNivel());

            response = financieraStub.tranferenciasDEX(request);
            String xlmStr = response.getTranferenciasDEXResult();

            //logger.info(response.getTranferenciasDEXResult());
            //logger.info(xlmStr);
            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xlmStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap ");
            //logger.info("ENTRE A EXCEPCION");
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasDEX", json);
        }

        return respuestaService;
    }

    public JsonObject getCambios(String fecha, String geografia, String nivel) {

        logger.info("---------Cambio Divisas------------");

        ExtraccionFinancieraStub.TransferenciasCambios request = null;
        ExtraccionFinancieraStub.TransferenciasCambiosResponse response = null;

        JsonArray json = null;

        JsonObject respuestaService = new JsonObject();

        try {
            ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

            request = new ExtraccionFinancieraStub.TransferenciasCambios();
            response = new ExtraccionFinancieraStub.TransferenciasCambiosResponse();

            request.setFecha(fecha);
            request.setGeografia(geografia);
            request.setNivel(nivel);

            logger.info("Fecha :" + request.getFecha());
            logger.info("Geografia :" + request.getGeografia());
            logger.info("Nivel :" + request.getNivel());

            response = financieraStub.transferenciasCambios(request);
            String xlmStr = response.getTransferenciasCambiosResult();

            //logger.info(response.getTransferenciasCambiosResult());
            //logger.info(xlmStr);
            int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
            json = limpiXmlNew(xlmStr, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap ");
            //logger.info("ENTRE A EXCEPCION");
            json = null;
        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("transferenciasCambios", json);
        }

        return respuestaService;
    }

    public JsonObject getContribucion(String fecha, String geografia, String nivel) {
        logger.info("---------Contribucion------------");

        JsonObject json = null;

        JsonObject respuestaService = new JsonObject();

        if (!nivel.equals("TR")) {

            if (nivel.equals("T")) {
                nivel = "G";
            }

            ExtraccionFinancieraStub.Contribucion request = null;
            ExtraccionFinancieraStub.ContribucionResponse response = null;

            try {

                DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
                Date date = formatDate.parse(fecha);

                Calendar calendar = Calendar.getInstance();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.setMinimalDaysInFirstWeek(4);
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);

                int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                int numberDayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);

                logger.info("Fecha Actual: " + calendar.getTime());
                logger.info("Semana Actual: " + numberWeekOfYear);
                logger.info("Dia de la semana Actual :" + numberDayOfTheWeek);

                SimpleDateFormat f = new SimpleDateFormat("kmm");
                int fechaNum = Integer.parseInt(calendar.get(Calendar.DAY_OF_WEEK) + f.format(calendar.getTime()));

                logger.info("Cadena comparar : " + fechaNum);

                // Si es jueves 4:00 pm calcula semana atras
                if (fechaNum > 51600) {
                    calendar.add(Calendar.WEEK_OF_YEAR, -1);
                } else {
                    calendar.add(Calendar.WEEK_OF_YEAR, -2);
                }

                int dia = 0;
                switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                    case Calendar.MONDAY:
                        dia = 1;
                        break;
                    case Calendar.TUESDAY:
                        dia = 2;
                        break;
                    case Calendar.WEDNESDAY:
                        dia = 3;
                        break;
                    case Calendar.THURSDAY:
                        dia = 4;
                        break;
                    case Calendar.FRIDAY:
                        dia = 5;
                        break;
                    case Calendar.SATURDAY:
                        dia = 6;
                        break;
                    case Calendar.SUNDAY:
                        dia = 7;
                        break;
                    default:
                        break;
                }

                calendar.add(Calendar.DATE, (7 - dia));

                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                String fechaFormateada = df.format(calendar.getTime());

                logger.info("Fecha a FINAL " + calendar.getTime());
                logger.info("Semana calculada: " + calendar.get(Calendar.WEEK_OF_YEAR));

                ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

                request = new ExtraccionFinancieraStub.Contribucion();
                response = new ExtraccionFinancieraStub.ContribucionResponse();

                request.setFecha(fechaFormateada);
                request.setGeografia(geografia);
                request.setNivel(nivel);

                logger.info("Fecha :" + request.getFecha());
                logger.info("Geografia :" + request.getGeografia());
                logger.info("Nivel :" + request.getNivel());

                response = financieraStub.contribucion(request);
                String xlmStr = response.getContribucionResult();

                //logger.info(response.getContribucionResult());
                json = armaJsonContribucion(xlmStr);
                json.addProperty("semana", "" + calendar.get(Calendar.WEEK_OF_YEAR));

            } catch (Exception e) {
                logger.info("Ap ");
                json = null;
            }

        }

        if (json == null) {
            respuestaService = null;
        } else {
            respuestaService.add("contribucion", json);
        }

        return respuestaService;

    }

    public JsonObject getNoAbonadasPend(String fecha, String geografia, String nivel) {
        logger.info("---------No Abonadas Pendientes por Surtir------------");

        Map<String, Object> jsonMap = null;

        JsonObject respuestaService = new JsonObject();

        if (!nivel.equals("TR")) {

            if (nivel.equals("T")) {
                nivel = "G";
            }

            ExtraccionFinancieraStub.NAbonadasPendSurt request = null;
            ExtraccionFinancieraStub.NAbonadasPendSurtResponse response = null;

            try {

                DateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
                Date date = formatDate.parse(fecha);

                Calendar calendar = Calendar.getInstance();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.setMinimalDaysInFirstWeek(4);
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, 1);

                int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                int numberDayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);

                logger.info("Fecha Actual: " + calendar.getTime());
                logger.info("Semana Actual: " + numberWeekOfYear);
                logger.info("Dia de la semana Actual :" + numberDayOfTheWeek);

                SimpleDateFormat f = new SimpleDateFormat("kmm");
                int fechaNum = Integer.parseInt(calendar.get(Calendar.DAY_OF_WEEK) + f.format(calendar.getTime()));

                logger.info("Cadena comparar : " + fechaNum);

                calendar.add(Calendar.WEEK_OF_YEAR, -1);

                int dia = 0;
                switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                    case Calendar.MONDAY:
                        dia = 1;
                        break;
                    case Calendar.TUESDAY:
                        dia = 2;
                        break;
                    case Calendar.WEDNESDAY:
                        dia = 3;
                        break;
                    case Calendar.THURSDAY:
                        dia = 4;
                        break;
                    case Calendar.FRIDAY:
                        dia = 5;
                        break;
                    case Calendar.SATURDAY:
                        dia = 6;
                        break;
                    case Calendar.SUNDAY:
                        dia = 7;
                        break;
                    default:
                        break;
                }

                calendar.add(Calendar.DATE, (7 - dia));

                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                String fechaFormateada = df.format(calendar.getTime());

                logger.info("Fecha a FINAL " + calendar.getTime());
                logger.info("Semana calculada: " + calendar.get(Calendar.WEEK_OF_YEAR));

                ExtraccionFinancieraStub financieraStub = new ExtraccionFinancieraStub();

                request = new ExtraccionFinancieraStub.NAbonadasPendSurt();
                response = new ExtraccionFinancieraStub.NAbonadasPendSurtResponse();

                request.setFecha(fechaFormateada);
                request.setGeografia(geografia);
                request.setNivel(nivel);

                logger.info("Fecha :" + request.getFecha());
                logger.info("Geografia :" + request.getGeografia());
                logger.info("Nivel :" + request.getNivel());

                response = financieraStub.nAbonadasPendSurt(request);
                String xlmStr = response.getNAbonadasPendSurtResult();

                //logger.info(response.getContribucionResult());
                jsonMap = armaJsonNoAbonadas(xlmStr);

            } catch (Exception e) {
                logger.info("Ap ");
                jsonMap = null;
            }

        }

        if (jsonMap == null) {
            respuestaService = null;
        } else {
            respuestaService.add("nuncaAbonadas", (JsonObject) jsonMap.get("nuncaAbonadas"));
            respuestaService.add("pendientesSurtrir", (JsonObject) jsonMap.get("pendientesSurtrir"));
        }

        return respuestaService;

    }

    public Map<String, Object> armaJsonNoAbonadas(String xmlStr) {

        Map<String, Object> mapJson = new HashMap<String, Object>();
        JsonObject nuncaAbonadas = new JsonObject();
        JsonObject pendientesSurtir = new JsonObject();

        try {
            /* Convierte String a XML */

            StringReader str = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(str);
            Element rootNode = document.getRootElement();

            Element data = rootNode.getChild("data");
            Element renglones = data.getChild("renglones");
            Element renglon = renglones.getChild("renglon");
            Element celdas = renglon.getChild("celdas");
            List<Element> listCeldas = celdas.getChildren("celda");

            for (Element element : listCeldas) {

                int celda = Integer.parseInt(element.getAttributeValue("pos"));
                String valor = element.getChild("caption").getValue();
                switch (celda) {
                    case 1:
                        break;
                    case 2:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 3:
                        nuncaAbonadas.addProperty(getProperty(celda), valor);
                        break;
                    case 4:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 5:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 6:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                    case 7:
                        pendientesSurtir.addProperty(getProperty(celda), valor);
                        break;
                }

            }

            mapJson.put("pendientesSurtrir", pendientesSurtir);
            mapJson.put("nuncaAbonadas", nuncaAbonadas);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        //logger.info("JSON SURTIDAS :"+pendientesSurtir.toString());
        //logger.info("JSON NUNCA ABONADAS :"+nuncaAbonadas.toString());
        return mapJson;
    }

    public JsonObject armaJsonContribucion(String xmlStr) {

        JsonObject contribucion = new JsonObject();
        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            Element consulta = rootNode.getChild("Consulta");
            Element table = consulta.getChild("Table");

            DecimalFormat df = new DecimalFormat("###,###,###");

            contribucion.addProperty("posicion", table.getChild("POSICION").getValue());
            contribucion.addProperty("geografia", table.getChild("GEOGRAFIA").getValue());
            contribucion.addProperty("semana1", df.format(Double.parseDouble(table.getChild("SEM_1").getValue())));
            contribucion.addProperty("semana2", df.format(Double.parseDouble(table.getChild("SEM_2").getValue())));
            contribucion.addProperty("crecimiento", table.getChild("CRECIMIENTO").getValue());
            contribucion.addProperty("total", table.getChild("TOTAL").getValue());
            contribucion.addProperty("totalReg", table.getChild("T_REG").getValue());

            int posicion = Integer.parseInt(contribucion.get("posicion").getAsString());
            int total_reg = Integer.parseInt(contribucion.get("totalReg").getAsString());
            String imagen = "";
            int res = 0;

            if (total_reg > 0) {
                res = (int) ((posicion * 100) / total_reg);
            }

            logger.info("Posicion " + posicion);
            logger.info("total_reg " + total_reg);
            logger.info("res " + (int) res);

            if (res >= 0 && res <= 20) {
                imagen = "oro";
            } else if (res > 20 && res <= 50) {
                imagen = "plata";
            } else if (res > 50 && res <= 80) {
                imagen = "bronce";
            } else if (res > 80 && res <= 100) {
                imagen = "carbon";
            }

            contribucion.addProperty("imagen", imagen);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        return contribucion;
    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray limpiXmlNew(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            //logger.info("Columnas :"+columnas);
            //logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            logger.info("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
                    //					    logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                    //						logger.info("Valor str: "+valorStr);
                    valorDouble = (Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue())) / 1000;
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
                    //						logger.info("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                //logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //logger.info(arrayFilas.toString());
            //logger.info("Valores celda lista : " +listValoresCeldas.size());
            //logger.info("Valores totales lista : " +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            jsonArray = armaJsonNew(valores, flagsColumnas);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonNew(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 4) {
                    if (valoresCeldas.get(i - pos_vs) > 0) {
                        porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                        //logger.info("cantidad: "+ valoresCeldas.get(i));
                        //logger.info("Cantidad vs"+ valoresCeldas.get( i - pos_vs ));
                        pos_vs += 3;
                    }
                } else {
                    if (valoresTotales.get(veces - 1) > 0) {
                        porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                    }
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

                valor = new JsonObject();
                valor.addProperty("cantidad", valorStr);
                valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valor.addProperty("color", color);

                arrayValores.add(valor);

            }

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /*Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 4) {
                    if (valoresTotales.get(a - pos_vsTot) > 0) {
                        porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                        pos_vsTot += 3;
                    }
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayValores.add(jsonTotales);
            }

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonObject clearXml(String xlmStr) {

        JsonObject respuesta = new JsonObject();

        try {
            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xlmStr);

            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(stringReader);
            Element rootNode = document.getRootElement();

            List<Element> allChildren = rootNode.getChildren();


            /*ï¿½NORMAL*/
            String fmtValue = "";
            boolean flagAdd = false;
            String celda = "";

            ArrayList<String> listValores = new ArrayList<String>();

            for (Element element : allChildren) {
                if (element.getName().equals("CellData")) {

                    List<Element> listaDatos = element.getChildren();// Lista
                    // que
                    // guarda
                    // todas
                    // las
                    // etiquets
                    // "Cell"
                    for (Element elementChild : listaDatos) {
                        fmtValue = "";
                        flagAdd = false;
                        celda = elementChild.getAttributeValue("CellOrdinal");
                        List<Element> elementDatos = elementChild.getChildren();// Lista
                        // que
                        // guarda
                        // la
                        // etiquetas
                        // dentro
                        // de
                        // cada
                        // etiqueta
                        // "Cell"
                        for (Element elementDato : elementDatos) {
                            if (elementDato.getName().equals("FmtValue")) {
                                fmtValue = elementDato.getValue();
                            } else if (elementDato.getName().equals("Value")) {
                                List<Attribute> atributos = elementDato.getAttributes();
                                for (Attribute elementAtributo : atributos) { // Revisa
                                    // Atributos
                                    // de
                                    // la
                                    // celda
                                    if (!elementAtributo.getValue().equals("xsd:short")) {
                                        flagAdd = true;
                                    }
                                }
                            }

                        }

                        if (flagAdd) {
                            listValores.add(fmtValue);
                        }

                    }

                }
            }

            respuesta = createResult(listValores);

        } catch (Exception e) {

            respuesta = null;
        }

        return respuesta;

    }

    @SuppressWarnings("unused")
    protected JsonObject createResult(ArrayList<String> valores) {

        DecimalFormat formateador = new DecimalFormat("###,###.#");
        double[] totales = new double[9];
        int nuInteraciones = 8;

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        int indice = valores.size() - 1;

        //logger.info("indice.size() - 1 " + indice);
        try {

            /* Obtiene totales */
            while (nuInteraciones >= 0) {
                logger.info("valores.get(indice) " + valores.get(indice));
                double valor = Double.parseDouble(valores.get(indice)) / 1000;
                logger.info("valor [] " + valor);
                totales[nuInteraciones] = valor;
                // totales[nuInteraciones] = (int)Math.round(valor);
                indice--;
                nuInteraciones--;
            }

            //logger.info("totales [] " + totales);
            // int positionTotales = 0;
            int veces = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i <= indice; i++) {

                veces++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "";

                if (veces > 5) {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = Double.parseDouble(valores.get(i - pos_vs)) / 1000;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    if (valor_vs > 0) {
                        porcentaje = (valor / valor_vs) * 100.0;
                    }
                    pos_vs += 2;
                } else {
                    valor = Double.parseDouble(valores.get(i)) / 1000;
                    // valorInt= (int)Math.round(valor);
                    if (totales[veces - 1] > 0) {
                        porcentaje = (valor / totales[veces - 1]) * 100.0;
                    }
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                logger.info("valor... " + valor);

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", "" + (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces == 9) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces = 0;
                    pos_vs = 2;
                }

            }

            //logger.info("jsonDias " + jsonDias);
            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            //logger.info("jsonDias " + jsonDias);
            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.length; a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 4) {
                    if (totales[a - pos_vs] > 0) {
                        porcentajeTot = (double) totales[a] / (double) totales[a - pos_vs] * 100.0;
                        pos_vs += 2;
                    }
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales[a]));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);
            logger.info(jsonDias.toString());

        } catch (Exception e) {
            logger.info("Ap ");
            jsonDias = null;
        }

        return jsonDias;

    }

    /*Limpia respuesta xml de los rubros Captacion*/
    @SuppressWarnings("unchecked")
    protected JsonObject limpiaXmlCaptacion(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();


            /*-----------------------------------------------Eliminar por pruebas--------------------------------------------------------------------*/
            logger.info("Celldata" + rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                //logger.info("Celda " +element.getAttributeValue("CellOrdinal"));
                numCeldas.add(Integer.parseInt(element.getAttributeValue("CellOrdinal")));

                String valor = element.getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonCaptacion(valoresCeldas, flagsColumnas, numCeldas);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        return jsonRepuestas;

    }

    /*Arma Json de los rubros Captacion*/
    @SuppressWarnings("unused")
    protected JsonObject armaJsonCaptacion(List<String> valoresCeldas, int[] flagsColumnas, ArrayList<Integer> numCeldas) {

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        try {
            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###.#");
            int columnas = flagsColumnas.length;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - columnas);
            int indiceColumnas = 0;

            while (indice < valoresCeldas.size()) {

                if (flagsColumnas[indiceColumnas] == 1) {
                    totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                }

                indiceColumnas++;
                indice++;
            }

            /*Obtener Filas*/
            int cont = 0;
            int veces = 1;
            int corte = 1;

            ArrayList<Double> valores = new ArrayList<Double>();

            while (cont < numCeldas.size() && cont != (valoresCeldas.size() - columnas)) {

                JsonObject valorPorcentaje = null;

                if (veces != numCeldas.get(cont)) {
                    break;
                }

                if (flagsColumnas[corte - 1] == 1) {
                    valores.add(Double.parseDouble(valoresCeldas.get(cont)));
                    //logger.info("$"+formateador.format(Double.parseDouble(valoresCeldas.get(cont))/1000.0));
                    //logger.info("$"+Double.parseDouble(valoresCeldas.get(cont))/1000);
                }

                if (corte == flagsColumnas.length) {
                    //logger.info("Agrega Fila");
                    corte = 1;
                    veces += 2;
                } else {
                    corte++;
                    veces++;
                }

                cont++;

            }

            // int positionTotales = 0;
            int veces1 = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i < valores.size(); i++) {

                veces1++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "verde";

                if (veces1 > 4) {
                    valor = valores.get(i) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = valores.get(i - pos_vs) / 1000.0;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    if (valor_vs > 0) {
                        porcentaje = (valor / valor_vs) * 100.0;
                        pos_vs += 2;
                    }
                } else {
                    valor = valores.get(i) / 1000;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(valor));
                valorProcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces1 == 7) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces1 = 0;
                    pos_vs = 2;
                }
            }
            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.size(); a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 3) {
                    if (totales.get(a - pos_vs) > 0) {
                        porcentajeTot = (double) totales.get(a) / (double) totales.get(a - pos_vs) * 100.0;
                        pos_vs += 2;
                    }
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(totales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);

        } catch (Exception e) {
            logger.info("Ap ");
            jsonDias = null;
        }

        return jsonDias;
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected JsonObject limpiXmlNormalidad(String xmlStr) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            //logger.info("Celldata"+rootNode.getChild("CellData",Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));

            List<Element> listaCeldas = cellData.getChildren();
            List<String> valoresCeldas = new ArrayList<String>();

            for (Element element : listaCeldas) {
                //logger.info("Celda " +element.getAttributeValue("CellOrdinal"));

                String valor = element.getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                valoresCeldas.add(valor);
            }

            jsonRepuestas = armaJsonNormalidad(valoresCeldas);

        } catch (Exception e) {
            logger.info("Ap ");
            return null;
        }

        return jsonRepuestas;

    }

    @SuppressWarnings("unused")
    protected JsonObject armaJsonNormalidad(List<String> valoresCeldas) {

        String arrayNombres[] = {
            "Vigente De 0 A 1",
            "Temprana 2",
            "Atrasada 1a De 3 A 7",
            "Tardia 1b De 8 A 13",
            "Atrasada 2 A 13",
            "Vencida 2a De 14 A 25",
            "Dificil 2b De 26 A 39",
            "Vencida De 14 A 39",
            "Saldo Cartera De 0 A 39 Semanas",
            "Legal 40 a 55",
            "Cazadores 56 +",
            "Saldo De Cartera"};

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonFilas = new JsonObject();

        try {

            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###");
            int columnas = 5;

            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - (columnas * 4));
            int indiceColumnas = 0;

            logger.info("Inicio :" + indice);
            while (indice < valoresCeldas.size() - (columnas * 3)) {

                //logger.info(formateador.format(Double.parseDouble(valoresCeldas.get(indice))/1000.0));
                totales.add(Double.parseDouble(valoresCeldas.get(indice)) / 1000.0);
                indiceColumnas++;
                indice++;
            }

            int fin = (valoresCeldas.size() - (columnas * 4));
            int veces = 0;

            JsonObject valorPorcentaje = null;

            int totalFilas = 0;
            for (int i = 0; i < valoresCeldas.size(); i++) {

                veces++;
                valorPorcentaje = new JsonObject();

                double porcentaje = 0.0;
                double valor = 0.0;

                int valorInt = 0;
                String color = "verde";

                valor = Double.parseDouble(valoresCeldas.get(i)) / 1000;
                //logger.info("Valor "+ formateador.format(valor));
                //logger.info("Total "+ formateador.format(totales.get(veces-1)));

                if (veces > 3) {
                    porcentaje = 0.0;
                } else {
                    porcentaje = (valor / (totales.get(veces - 1))) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                //logger.info("Porcentaje "+(int)Math.round(porcentaje)+"%");
                valorPorcentaje.addProperty("cantidad", formateador.format(valor));
                valorPorcentaje.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valorPorcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorPorcentaje);

                if (veces == 5) {

                    jsonFilas.add(arrayNombres[totalFilas], arrayValorPorcentaje);
                    arrayValorPorcentaje = new JsonArray();

                    veces = 0;

                    totalFilas++;
                }

            }
        } catch (Exception e) {
            jsonFilas = null;
        }

        //logger.info(jsonFilas.toString());
        return jsonFilas;

    }

    protected String getdia(int nuDia) {
        String dia = "";
        if (nuDia == 1) {
            dia = "lunes";
        } else if (nuDia == 2) {
            dia = "martes";
        } else if (nuDia == 3) {
            dia = "miercoles";
        } else if (nuDia == 4) {
            dia = "jueves";
        } else if (nuDia == 5) {
            dia = "viernes";
        } else if (nuDia == 6) {
            dia = "sabado";
        } else if (nuDia == 7) {
            dia = "domingo";
        } else if (nuDia == 8) {
            dia = "totales";
        }
        return dia;

    }

    public static String getProperty(int valor) {

        String propiedad = "";
        switch (valor) {
            case 1:
                propiedad = "cabeceras";
                break;
            case 2:
                propiedad = "noAbonadas";
                break;
            case 3:
                propiedad = "totalCuentas";
                break;
            case 4:
                propiedad = "pendientesSurtir";
                break;
            case 5:
                propiedad = "autorizadas";
                break;
            case 6:
                propiedad = "clientesNuevos";
                break;
            case 7:
                propiedad = "creditosOtorgados";
                break;
            case 8:
                propiedad = "planClientes";
                break;
            case 9:
                propiedad = "level";
                break;

            default:
                propiedad = "";
                break;
        }

        return propiedad;

    }

    public JsonObject jsonDiasSemana(JsonArray array) {

        JsonObject respuesta = new JsonObject();

        for (int i = 1; i <= array.size(); i++) {
            respuesta.add(getdia(i), array.get(i - 1).getAsJsonArray());
        }

        return respuesta;

    }

    protected JsonArray limpiaXMLVersus(String xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonArray jsonArray = new JsonArray();
        try {

            /* Convierte String a XML */
            StringReader stringReader = new StringReader(xmlStr);

            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(stringReader);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            logger.info("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            //logger.info("Columnas :"+columnas);
            //logger.info("Filas :"+ rowsCount);
            int totalCeldas = columnas * rowsCount;

            //logger.info("Total GRID: "+ totalCeldas);
            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;

            int columnasdia = 0;

            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
                    //					    logger.info("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
                    //						logger.info("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue()) / 1000;
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = null;
                    valorDouble = 0.0;
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

                }

                if (numFilas == (rowsCount)) {
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
                    //logger.info("------------FILA-------------" +numFilas);
                    int posicionReal;
                    //Agrega los versus
                    if (numFilas == 1 || numFilas == rowsCount) {
                        posicionReal = 5;
                    } else {
                        posicionReal = listValoresCeldas.size() - 2;
                    }

                    double valorReal;

                    if (numFilas == rowsCount) {
                        valorReal = listaValoresTotales.get(posicionReal);
                    } else {
                        valorReal = listValoresCeldas.get(posicionReal);
                    }

                    for (int i = 1; i <= 4; i++) {
                        double valorVs = 0.0;
                        double valorCalculado = 0.0;

                        if (valorReal == 0.0) {
                            if (numFilas == rowsCount) {
                                valorVs = listaValoresTotales.get(posicionReal - i);
                            } else {
                                valorVs = listValoresCeldas.get(posicionReal - i);
                            }

                            valorCalculado = valorVs - valorReal;

                        }

                        //logger.info("Valor real "+valorReal);
                        //logger.info("Valor vs: "+ valorVs);
                        //logger.info("Valor Calculado: "+ valorVs);
                        if (numFilas == rowsCount) {
                            listaValoresTotales.add(valorCalculado);
                        } else {
                            listValoresCeldas.add(valorCalculado);
                        }

                    }
                    veces = 0;
                    numFilas++;

                }
                //logger.info("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //logger.info(arrayFilas.toString());
            //logger.info("Valores celda lista : " +listValoresCeldas.size());
            //logger.info("Valores totales lista : " +listaValoresTotales.size());
            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", 11);

            int[] flags = {0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1};
            jsonArray = armaJsonVersus(valores, flags);

        } catch (Exception e) {
            logger.info("Ap " + e.getMessage());
            return null;
        }

        return jsonArray;

    }

    @SuppressWarnings({"unchecked", "unused"})
    protected JsonArray armaJsonVersus(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###.#");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 3;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 7) {

                    if (valoresCeldas.get(i) == 0.0) {
                        if (valoresCeldas.get(i - pos_vs) > 0) {
                            porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                        }
                    }

                    //logger.info("cantidad: "+ valoresCeldas.get(i));
                    //logger.info("Cantidad vs"+ valoresCeldas.get( i - pos_vs));
                    // ));
                    pos_vs += 2;
                } else {
                    //logger.info("cantidad: "+ valoresCeldas.get(i));
                    //logger.info("Cantidad vs"+ valoresTotales.get(veces - 1));
                    if (valoresTotales.get(veces - 1) > 0) {
                        porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                    }
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

                valor = new JsonObject();
                valor.addProperty("cantidad", valorStr);
                valor.addProperty("porcentaje", (int) Math.round(porcentaje) + "%");
                valor.addProperty("color", color);

                arrayValores.add(valor);
            }

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                //logger.info("------------FILA-------------");
                veces = 0;
                pos_vs = 3;
            }

        }

        /* Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 3;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        int veces1 = 0;
        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            veces1++;
            if (flagsColumnas[veces1 - 1] == 1) {
                if (veces1 > 7) {

                    if (valoresTotales.get(a) == 0.0) {
                        if (valoresTotales.get(a - pos_vsTot) > 0) {
                            porcentajeTot = ((double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot)) * 100.0;
                        }
                    }
                    //logger.info("Valor total:" +valoresTotales.get(a));
                    //logger.info("Valor contra : "+valoresTotales.get(a - pos_vsTot));

                    pos_vsTot += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(valoresTotales.get(a)));
                jsonTotales.addProperty("porcentaje", "" + (int) Math.round(porcentajeTot) + "%");
                jsonTotales.addProperty("color", color);
                arrayValores.add(jsonTotales);

            }

        }

        arrayFilas.add(arrayValores);

        return arrayFilas;

    }

}
