package com.gruposalinas.migestion.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.migestion.dao.ObtieneTokenDAO;
import com.gruposalinas.migestion.domain.UsuarioInfoTokenDTO;
import com.gruposalinas.migestion.domain.Usuario_ADTO;

public class ContingenciaBI {

    private static Logger logger = LogManager.getLogger(ContingenciaBI.class);

    @Autowired
    ObtieneTokenDAO obtieneTokenDAO;

    @Autowired
    Usuario_ABI usuarioBI;

    @Autowired
    Usuario_ABI usuarioabi;

    public boolean CambiaPuesto(int idCeco) throws Exception {
        boolean respuesta = false;
        List<UsuarioInfoTokenDTO> usuarios = obtieneTokenDAO.consultaUsuarios(idCeco);
        for (UsuarioInfoTokenDTO user : usuarios) {
            List<Usuario_ADTO> res = usuarioBI.obtieneUsuario(user.getIdUsuario());
            if (res != null) {
                for (Usuario_ADTO r : res) {
                    r.setIdPuesto(25);
                    r.setFecha(r.getFecha().substring(0, 4) + r.getFecha().substring(5, 7) + r.getFecha().substring(8, 10));
                    respuesta = usuarioabi.actualizaUsuario(r);
                }
            }

        }
        return respuesta;
    }

}
