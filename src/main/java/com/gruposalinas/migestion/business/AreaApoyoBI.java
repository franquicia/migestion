package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.AreaApoyoDAO;
import com.gruposalinas.migestion.domain.AreaApoyoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AreaApoyoBI {

    private static Logger logger = LogManager.getLogger(AreaApoyoBI.class);

    private List<AreaApoyoDTO> listafila;

    @Autowired
    AreaApoyoDAO areaApoyoDAO;



    public boolean elimina(String idArea) {
        boolean respuesta = false;

        try {
            respuesta = areaApoyoDAO.elimina(idArea);
        } catch (Exception e) {
            logger.info("AreaApoyoBI||No fue posible eliminar :" + e.getMessage());
        }

        return respuesta;
    }

    public boolean eliminaEvi(String idEvid) {
        boolean respuesta = false;

        try {
            respuesta = areaApoyoDAO.eliminaEvi(idEvid);
        } catch (Exception e) {
            logger.info("AreaApoyoBI||No fue posible eliminar :" + e.getMessage());
        }

        return respuesta;
    }


    public List<AreaApoyoDTO> obtieneInfo() {

        try {
            listafila = areaApoyoDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("AreaApoyoBI||No fue posible obtener datos de la tabla area de apoyo :" + e.getMessage());
        }

        return listafila;
    }

    public boolean actualizaEvi(AreaApoyoDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = areaApoyoDAO.actualizaEvi(bean);
        } catch (Exception e) {
            logger.info("AreaApoyoBI||No fue posible actualizar :" + e.getMessage());
        }

        return respuesta;
    }

}
