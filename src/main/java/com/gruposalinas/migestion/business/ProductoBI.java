package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ProductoDAO;
import com.gruposalinas.migestion.domain.TipificacionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductoBI {

    @Autowired
    ProductoDAO productoDAO;

    public List<TipificacionDTO> consulta() {

        List<TipificacionDTO> lista = null;

        try {
            lista = productoDAO.consulta();
        } catch (Exception e) {

        }

        return lista;

    }

    public List<TipificacionDTO> consultaPadres() {

        List<TipificacionDTO> lista = null;

        try {
            lista = productoDAO.consultaPadres();
        } catch (Exception e) {

        }

        return lista;

    }

    public int inserta(TipificacionDTO bean) {

        int idCreado = 0;

        try {
            idCreado = productoDAO.inserta(bean);
        } catch (Exception e) {

        }

        return idCreado;

    }

    public boolean actualiza(TipificacionDTO bean) {

        boolean respuesta = false;

        try {
            respuesta = productoDAO.actualiza(bean);
        } catch (Exception e) {

        }

        return respuesta;

    }

    public boolean elimina(int idProducto) {
        boolean respuesta = false;

        try {
            respuesta = productoDAO.elimina(idProducto);
        } catch (Exception e) {

        }

        return respuesta;
    }

    public List<TipificacionDTO> getHijos(int idPadre) {

        List<TipificacionDTO> lista = null;

        try {
            lista = productoDAO.getHijos(idPadre);
        } catch (Exception e) {

        }

        return lista;

    }

}
