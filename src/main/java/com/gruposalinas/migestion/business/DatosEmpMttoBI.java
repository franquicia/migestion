package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.DatosEmpMttoDAO;
import com.gruposalinas.migestion.domain.DatosEmpMttoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class DatosEmpMttoBI {

    private static Logger logger = LogManager.getLogger(DatosEmpMttoBI.class);

    private List<DatosEmpMttoDTO> listafila;

    @Autowired
    DatosEmpMttoDAO datosEmpMttoDAO;

    public List<DatosEmpMttoDTO> obtieneDatos(int idUsuario) {

        try {
            listafila = datosEmpMttoDAO.obtieneDatos(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<DatosEmpMttoDTO> obtieneInfo() {

        try {
            listafila = datosEmpMttoDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Usuarios");

        }

        return listafila;
    }

}
