package com.gruposalinas.migestion.business;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import java.net.InetSocketAddress;
import java.net.Proxy;

/*
 import cn.teaey.apns4j.Apns4j;
 import cn.teaey.apns4j.network.ApnsChannel;
 import cn.teaey.apns4j.network.ApnsChannelFactory;
 import cn.teaey.apns4j.network.ApnsGateway;
 import cn.teaey.apns4j.network.async.ApnsService;
 import cn.teaey.apns4j.protocol.ApnsPayload;
 */
public class NotificacionVoIPBI {

    //private static Logger logger = LogManager.getLogger(NotificacionVoIPBI.class);
    /*

     private enum CertType {
     DEV;

     public ApnsGateway getGateway() {
     return this == DEV ? ApnsGateway.DEVELOPMENT: ApnsGateway.PRODUCTION;
     }
     }

     private enum NotificationType {
     Message, VoIP;

     public static NotificationType fromShort(short x) {
     NotificationType[] types = NotificationType.values();
     if (x >= 0 && x < types.length) {
     return types[x];
     }
     return null;
     }

     public String getCertPassword(CertType type) {
     if (this == VoIP || type == CertType.DEV) {
     return "miguel261291angel";
     }
     return "miguel261291angel";
     }

     public Map<CertType, String> getCerts() {
     Map<CertType, String> certs = new HashMap<>(2);

     if (this == NotificationType.Message) {

     certs.put(CertType.DEV, "/Users/b189871/Desktop/AlexNotificacion/voipGR.p12");
     }
     else if (this == NotificationType.VoIP) {
     certs.put(CertType.DEV, "/Users/b189871/Desktop/AlexNotificacion/voipGR.p12");

     }

     return certs;
     }

     public String getTitle() {
     return this == VoIP ? "Incoming Call" : "New Message";
     }

     }

     */
    @SuppressWarnings("unused")
    public boolean enviarNotificacion() {

        //METODO 2
        Proxy requestproxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 80));
        ApnsService service = APNS.newService()
                .withCert("/Users/b189871/Desktop/AlexNotificacion/voipGR.p12", "miguel261291angel")
                .withSandboxDestination()
                .withProxy(requestproxy)
                .build();

        String payload = APNS.newPayload().alertBody("Notificacion Enviada").customField("email", "amorales@elektra.com.mx").customField("personId", "987654").customField("tipo", "1")
                .customField("sucursal", "99995").customField("empleado", "189871").build();

        String token = "6955e8f9c2fd17ef7587c9dce6d20e529fb070ca777d2a74caf3f2576169a484";
        service.push(token, payload);

        String token1 = "46e510413f3dcade6358b4f442a870e25b5f9e25146929138a5b5257d6d781b9";
        service.push(token1, payload);

        return true;

    }

    public static void main(String[] args) {

        NotificacionVoIPBI notificacion = new NotificacionVoIPBI();

        notificacion.enviarNotificacion();

    }

}
