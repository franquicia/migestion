package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.LogDAO;
import com.gruposalinas.migestion.domain.LogDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class LogBI {

    private static Logger logger = LogManager.getLogger(LogBI.class);

    @Autowired
    LogDAO logDAO;

    List<LogDTO> listaErrores = null;

    public List<LogDTO> obtieneErrores(String fecha) {

        try {
            listaErrores = logDAO.obtieneErrores(fecha);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla de Errores");
        }

        return listaErrores;
    }

    public boolean insertaerror(int codigo, String mensaje, String origen) {
        boolean respuesta = false;
        try {
            respuesta = logDAO.inserta(codigo, mensaje, origen);
        } catch (Exception e) {
            logger.info("No fue posible insertar los datos");
        }

        return respuesta;
    }

}
