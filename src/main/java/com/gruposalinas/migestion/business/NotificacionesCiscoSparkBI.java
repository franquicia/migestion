package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.TokenUserCiscoSparkDAOImpl;
import com.gruposalinas.migestion.domain.TokenUserCiscoSparkDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class NotificacionesCiscoSparkBI {

    private static Logger logger = LogManager.getLogger(NotificacionesCiscoSparkBI.class);

    List<TokenUserCiscoSparkDTO> listaTokens;

    @Autowired
    TokenUserCiscoSparkDAOImpl tokenUserCisco;

    public boolean registraDispositivo(String tokenApple, String email, String personId, String tipo, String fecha) {

        boolean res = false;

        try {
            TokenUserCiscoSparkDTO obj = new TokenUserCiscoSparkDTO();
            obj.setToken(tokenApple);
            obj.setEmail(email);
            obj.setPersonId(personId);
            obj.setTipo(Integer.parseInt(tipo));
            obj.setFecha(fecha);

            res = tokenUserCisco.insertaDispositivo(obj);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            res = false;
            logger.info("Algo Ocurrio NOTIFICACION SPARK REGISTRADISPOSITIVO() ");
        }

        logger.info("NOTIFICACION SPARK REGISTRADISPOSITIVO() CORRECTO");

        return res;

    }

    public List<TokenUserCiscoSparkDTO> buscaDispositivos(String mail) {

        try {
            listaTokens = tokenUserCisco.obtienePorCorreo(mail);
        } catch (Exception e) {
            logger.info("Algo Ocurrio NOTIFICACION SPARK BUSCADISPOSITIVO() ");

        }

        logger.info("NOTIFICACION SPARK BUSCADISPOSITIVO() CORRECTO" + listaTokens.size());

        return listaTokens;
    }

    public boolean eliminaRegistro(String tokenApple) {

        boolean res = false;

        try {
            TokenUserCiscoSparkDTO obj = new TokenUserCiscoSparkDTO();
            obj.setToken(tokenApple);

            res = tokenUserCisco.eliminaRegistro(tokenApple);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            logger.info("Algo Ocurrio NOTIFICACION SPARK BAJAISPOSITIVO() ");
            res = false;
        }
        logger.info("NOTIFICACION SPARK BAJAISPOSITIVO() CORRECTO");
        return res;

    }

}
