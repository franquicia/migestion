package com.gruposalinas.migestion.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.migestion.dao.BibliotecaDigitalDAO;
import com.gruposalinas.migestion.domain.BibliotecaDigitalDTO;
import com.gruposalinas.migestion.domain.EnvioCorreoDTO;
import com.gruposalinas.migestion.domain.QuejaDTO;
import com.gruposalinas.migestion.util.UtilMail;
import com.gruposalinas.migestion.util.UtilObject;
import com.gruposalinas.migestion.util.UtilResource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

public class BibliotecaDigitalBI {

    private Logger logger = LogManager.getLogger(BibliotecaDigitalBI.class);

    @Autowired
    BibliotecaDigitalDAO bibliotecaDigitalDAO;

    

    public boolean enviaDocumento(String destinatario, List<String> copiados, String menu, String itemSeleccionado, String nombreArchivo, int idArchivo, String sucursal, String fechaSolicitado) {

        URLConnection conexion = null;
        String url = "http://10.51.210.237/biblioteca_digital/archivos/" + nombreArchivo;

        File adjunto = null;
        OutputStream file = null;
        InputStream in = null;
        boolean respuesta = false;

        try {

            String[] strArchivo = nombreArchivo.split("\\.");
            String nombre = strArchivo[0];
            String extension = strArchivo[1];

            File temp = adjunto.createTempFile(nombre, "." + extension);

            conexion = new URL(url).openConnection();
            conexion.connect();

            file = new FileOutputStream(temp);
            in = conexion.getInputStream();

            int b = 0;
            while (b != -1) {
                b = in.read();
                if (b != -1) {
                    file.write(b);
                }
            }

            Resource correoPlatilla = UtilResource.getResourceFromInternet("mailDocumento.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoPlatilla.getInputStream());

            strCorreoOwner = strCorreoOwner.replace("@nombreDocumento", "" + nombre);
            strCorreoOwner = strCorreoOwner.replace("@email", destinatario);
            strCorreoOwner = strCorreoOwner.replace("@sucursal", sucursal);
            strCorreoOwner = strCorreoOwner.replace("@fechasolicitud", fechaSolicitado);

            //copiados = new ArrayList<String>();
            //copiados.add("cvidal@bancoazteca.com.mx");
            respuesta = UtilMail.enviaCorreo(destinatario, copiados, "Solicitud de Documentos - " + menu + " - " + itemSeleccionado, strCorreoOwner, temp);

            EnvioCorreoDTO correoDTO = null;

            if (respuesta) {
                correoDTO = new EnvioCorreoDTO();
                correoDTO.setEmail(destinatario);
                correoDTO.setSucursal(sucursal);
                correoDTO.setFechaSolicitada(fechaSolicitado);
                correoDTO.setIdArchvio(idArchivo);

                bibliotecaDigitalDAO.registraEnvio(correoDTO);

            }

        } catch (Exception e) {

            respuesta = false;

        } finally {
            try {
                if (file != null) {
                    file.close();
                }

                if (in != null) {
                    in.close();
                }

            } catch (Exception e) {

            }

        }

        return respuesta;
    }
}
