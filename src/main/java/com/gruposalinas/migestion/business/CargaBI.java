package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.CargaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CargaBI {

    private static Logger logger = LogManager.getLogger(CargaBI.class);

    @Autowired
    CargaDAOImpl cargaDAO;

    public boolean cargaUsuariosPaso() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaUsuariosPaso();
        } catch (Exception e) {
            logger.info("CargaBI||cargaUsuariosPaso||No fue posible insertar los usuarios de paso:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaCecosPaso() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaCecosPaso();
        } catch (Exception e) {
            logger.info("CargaBI||cargaCecosPaso||No fue posible insertar los cecos de paso:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaSucursalesPaso() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaSucursalesPaso();
        } catch (Exception e) {
            logger.info("CargaBI||cargaSucursalesPaso||No fue posible insertar las sucursales de paso:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaPuestos() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaPuestos();
        } catch (Exception e) {
            logger.info("CargaBI||cargaPuestos||No fue posible insertar los puestos:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaUsuarios() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaUsuarios();
        } catch (Exception e) {
            logger.info("CargaBI||cargaUsuarios||No fue posible insertar los usuarios:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaCecos() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaCecos();
        } catch (Exception e) {
            logger.info("CargaBI||cargaCecos||No fue posible insertar los cecos:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaSucursales() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaSucursales();
        } catch (Exception e) {
            logger.info("CargaBI||cargaSucursales||No fue posible insertar las sucursales:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaGeografia() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaGeografia();
        } catch (Exception e) {
            logger.info("CargaBI||cargaGeografia||No fue posible insertar la geografia:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaTareas() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaTareas();
        } catch (Exception e) {
            logger.info("CargaBI||cargaTareas||No fue posible insertar las sucursales:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaLimpiaCecos() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaLimpiaCecos();
        } catch (Exception e) {
            logger.info("CargaBI||cargaLimpiaCecos||No fue posible limpiar las nombres de CECOS:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaCecosGuadalajara() {
        boolean respuesta = false;

        try {
            boolean res = cargaDAO.cargaCecos();
            boolean res2 = cargaDAO.cargaGeografia();

            if (res && res2) {
                respuesta = true;

            }

        } catch (Exception e) {
            logger.info("CargaBI||cargaCecosGuadalajara||No fue posible cargar cesos y geografia:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaUsuariosFRQ() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaUsuariosFRQ();
        } catch (Exception e) {
            logger.info("CargaBI||cargaUsuariosFRQ||No fue posible hacer MERGE de la tabla de USUARIOS de FRANQUICIA:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaUsuariosExternos() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaUsuariosExternos();
        } catch (Exception e) {
            logger.info("CargaBI||cargaUsuariosExternos||No fue posible hacer MERGE de la tabla de USUARIOS Externos:" + e.getMessage());
        }

        return respuesta;
    }

    public boolean cargaPerfilesMantenimiento() {
        boolean respuesta = false;

        try {
            respuesta = cargaDAO.cargaPerfilesMantenimiento();
        } catch (Exception e) {
            logger.info("CargaBI||cargaPerfilesMantenimiento||No fue posible hacer la Carga de los Perfiles de Mantenimiento:" + e.getMessage());
        }

        return respuesta;
    }
}
