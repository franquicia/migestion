package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.CuadrillaDAO;
import com.gruposalinas.migestion.domain.CuadrillaDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CuadrillaBI {

    private static Logger logger = LogManager.getLogger(CuadrillaBI.class);

    private List<CuadrillaDTO> listafila;

    @Autowired
    CuadrillaDAO cuadrillaDAO;

    public int inserta(CuadrillaDTO bean) {
        int respuesta = 0;

        try {
            respuesta = cuadrillaDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public boolean elimina(String idCuadrilla) {
        boolean respuesta = false;

        try {
            respuesta = cuadrillaDAO.elimina(idCuadrilla);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<CuadrillaDTO> obtieneDatos(int idCuadrilla) {

        try {
            listafila = cuadrillaDAO.obtieneDatos(idCuadrilla);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<CuadrillaDTO> obtieneInfo() {

        try {
            listafila = cuadrillaDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla cuadrilla");

        }

        return listafila;
    }

    public boolean actualiza(CuadrillaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = cuadrillaDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

    public List<CuadrillaDTO> obtienepass(int idCuadrilla, int idProveedor, int zona) {

        try {
            listafila = cuadrillaDAO.obtienepass(idCuadrilla, idProveedor, zona);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public boolean cargapass() {
        boolean respuesta = false;
        try {
            respuesta = cuadrillaDAO.cargapass();
        } catch (Exception e) {
            logger.info("No fue posible Cargar Los Passwords de Las cuadrillas");

        }
        return respuesta;
    }

    public boolean eliminaCuadrillaProveedor(int cuadrilla, int proveedor, int zona) {

        boolean respuesta = false;

        try {

            respuesta = cuadrillaDAO.eliminaCuadrillaProveedor(cuadrilla, proveedor, zona);

        } catch (Exception e) {
            logger.info("No fue posible ejecutar el proceso de eliminar cuadrilla por proveedor ");
        }

        return respuesta;

    }

}
