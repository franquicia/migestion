package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.CecoDAO;
import com.gruposalinas.migestion.domain.CecoComboDTO;
import com.gruposalinas.migestion.domain.CecoDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CecoBI {

    private static Logger logger = LogManager.getLogger(CecoBI.class);

    @Autowired

    CecoDAO cecoDAO;

    public List<CecoDTO> buscaCecos(String idCeco, String idCecoP, String activo, String negocio, String canal,
            String pais, String nombreCC) {
        List<CecoDTO> listaCecos = null;

        try {
            listaCecos = cecoDAO.buscaCecos(idCeco, idCecoP, activo, negocio, canal, pais, nombreCC);
        } catch (Exception e) {
            logger.info("No fue posible consultar los ceco");

        }

        return listaCecos;
    }

    public List<CecoDTO> buscaCecosSuperior(String idCeco, String negocio) {
        List<CecoDTO> listaCecos = null;

        try {
            listaCecos = cecoDAO.buscaCecosSuperior(idCeco, negocio);
        } catch (Exception e) {
            logger.info("No fue posible consultar por Ceco Superior");

        }

        return listaCecos;
    }

    public boolean insertaCeco(CecoDTO bean) {

        boolean resultado = false;

        try {
            resultado = cecoDAO.insertaCeco(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el CECO ");

        }

        return resultado;
    }

    public boolean actualizaCeco(CecoDTO bean) {

        boolean resultado = false;

        try {
            resultado = cecoDAO.actualizaCeco(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el CECO ");

        }

        return resultado;
    }

    public boolean eliminaCeco(int ceco) {

        boolean resultado = false;

        try {
            resultado = cecoDAO.eliminaCeco(ceco);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el CECO ");

        }

        return resultado;
    }

    public String negocioCeco(String ceco) {

        String resultado = "";

        try {
            resultado = cecoDAO.negocioCeco(ceco);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el CECO ");

        }

        return resultado;
    }

    public List<CecoComboDTO> getCecosCercanos(String ceco, String latitud, String longitud, int inserta_error) {

        List<CecoComboDTO> listaRespuesta = null;
        List<CecoComboDTO> listaCercanos = null;

        try {
            listaRespuesta = cecoDAO.getCecosCercanos(ceco, latitud, longitud, inserta_error);

            if (listaRespuesta != null && listaRespuesta.size() > 0) {
                listaCercanos = new ArrayList<CecoComboDTO>();

                for (CecoComboDTO cecoComboDTO : listaRespuesta) {
                    if (cecoComboDTO.cerca == 1) {
                        listaCercanos.add(cecoComboDTO);
                    }
                }

            }

        } catch (Exception e) {
            logger.info("No fue posible Obtener los cecos cercanos ");

        }

        return listaCercanos;
    }
}
