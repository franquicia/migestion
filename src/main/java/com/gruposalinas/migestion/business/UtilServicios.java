package com.gruposalinas.migestion.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.migestion.servicios.servidor.Servicios;

public class UtilServicios {

    private static Logger logger = LogManager.getLogger(Servicios.class);

    public BufferedReader consumeWebService(String urlStr, String cadena) {
        BufferedReader read = null;
        HttpURLConnection rc = null;
        OutputStreamWriter outStr = null;
        OutputStream out = null;

        try {
            URL url = new URL(urlStr);
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int longitudStr = cadena.length();
            rc.setRequestProperty("Content-Length", Integer.toString(longitudStr));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            out = rc.getOutputStream();
            outStr = new OutputStreamWriter(out);
            outStr.write(cadena, 0, longitudStr);
            outStr.flush();

            try {
                read = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            } catch (Exception exception) {
                // Si algo sale mal, obtiene el error.
                read = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }
        } catch (Exception e) {
//			e.printStackTrace();
            logger.info(e.getMessage());
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    logger.info("No fue posible cerrar el OutputStream");
                }
            }
        }
        return read;
    }
}
