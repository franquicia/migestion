package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.PeriodoDAOImpl;
import com.gruposalinas.migestion.domain.PeriodoDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PeriodoBI {

    private static Logger logger = LogManager.getLogger(PeriodoBI.class);

    private List<PeriodoDTO> listaPeriodo;

    @Autowired
    PeriodoDAOImpl periodoDAO;

    public List<PeriodoDTO> obtienePeriodo(String idPeriodo, String descripcion) {

        try {
            listaPeriodo = periodoDAO.obtienePeriodo(idPeriodo, descripcion);
        } catch (Exception e) {
            logger.info("No fue posible obtener el registro de los periodos");

        }

        return listaPeriodo;
    }

    public boolean insertaPeriodo(String idPeriodo, String descripcion, int commit) {
        boolean respuesta = false;

        try {
            respuesta = periodoDAO.insertaPeriodo(idPeriodo, descripcion, commit);
        } catch (Exception e) {
            logger.info("No fue posible insertar el periodo");

        }

        return respuesta;
    }

    public boolean actualizaPeriodo(String idPeriodo, String descripcion, int commit) {
        boolean respuesta = false;

        try {
            respuesta = periodoDAO.actualizaPeriodo(idPeriodo, descripcion, commit);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el periodo");

        }

        return respuesta;
    }

    public boolean eliminaPeriodo(String idPeriodo) {
        boolean respuesta = false;

        try {
            respuesta = periodoDAO.eliminaPeriodo(idPeriodo);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el periodo");

        }

        return respuesta;
    }
}
