package com.gruposalinas.migestion.business;

import com.gruposalinas.migestion.dao.ProveedorLoginDAO;
import com.gruposalinas.migestion.domain.ProveedorLoginDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProveedorLoginBI {

    private static Logger logger = LogManager.getLogger(ProveedorLoginBI.class);

    private List<ProveedorLoginDTO> listafila;

    @Autowired
    ProveedorLoginDAO proveedorLoginDAO;

    public int inserta(ProveedorLoginDTO bean) {
        int respuesta = 0;

        try {
            respuesta = proveedorLoginDAO.inserta(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar ");

        }

        return respuesta;
    }

    public boolean elimina(String idProveedor) {
        boolean respuesta = false;

        try {
            respuesta = proveedorLoginDAO.elimina(idProveedor);
        } catch (Exception e) {
            logger.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<ProveedorLoginDTO> obtieneDatos(int idProveedor) {

        try {
            listafila = proveedorLoginDAO.obtieneDatos(idProveedor);
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos");

        }

        return listafila;
    }

    public List<ProveedorLoginDTO> obtieneInfo() {

        try {
            listafila = proveedorLoginDAO.obtieneInfo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla de Login Proveedor");

        }

        return listafila;
    }

    public boolean actualiza(ProveedorLoginDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = proveedorLoginDAO.actualiza(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar");

        }

        return respuesta;
    }

}
