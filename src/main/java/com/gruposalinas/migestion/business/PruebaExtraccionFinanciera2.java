package com.gruposalinas.migestion.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class PruebaExtraccionFinanciera2 {

    static ArrayList<Integer> numCeldas = new ArrayList<Integer>();

    static String arrayNombres[] = {"V�gente De 0 A 1", "Temprana 2", "Atrasada 1a De 3 A 7", "Tard�a 1b De 8 A 13", "Atrasada 2 A 13", "Vencida 2a De 14 A 25", "Dificil 2b De 26 A 39", "Vencida De 14 A 39", "Saldo Cartera De 0 A 39 Semanas", "Legal 40 a 55", "Cazadores 56 +", "Saldo De Cartera"};

    @SuppressWarnings("unused")
    public static void main(String args[]) {

        SAXBuilder saxBuilder = new SAXBuilder();

        BufferedReader rd = null;
        StringReader srd = null;
        try {
            rd = new BufferedReader(new FileReader("E:\\Users\\b191312\\Desktop\\archivo.xml"));

            String inputLine = null;
            StringBuilder builder = new StringBuilder();

            //Store the contents of the file to the StringBuilder.
            while ((inputLine = rd.readLine()) != null) {
                builder.append(inputLine);
            }

            //Create a new tokenizer based on the StringReader class instance.
            srd = new StringReader(builder.toString());

        } catch (IOException ex) {
            System.err.println("An IOException was caught: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {

            }

        }

        int[] flagsColumnas = {1, 1, 1, 1, 1, 0, 1, 0, 1, 0};
        limpiXmlNew(srd, flagsColumnas);
        //System.out.println(limpiXmlNormalidad(srd).toString());

    }

    @SuppressWarnings({"unused", "unchecked"})
    public static JsonObject limpiXmlNew(StringReader xmlStr, int[] flagsColumnas) {

        ArrayList<Integer> numCeldas = new ArrayList<Integer>();
        JsonObject jsonRepuestas = new JsonObject();

        try {
            //
            //			/* Convierte String a XML */
            //			StringReader stringReader = new StringReader(xmlStr);
            //
            SAXBuilder builder = new SAXBuilder();

            //Se crea el documento a traves del archivo
            Document document = (Document) builder.build(xmlStr);

            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            Element axes = rootNode.getChild("Axes", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            Element cellData = rootNode.getChild("CellData", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
            List<Element> celdas = cellData.getChildren();

            List<Element> axes0 = axes.getChildren();
            System.out.println("Axes: " + axes0.size());

            int columnas = 0;
            int rowsCount = 0;

            Element tuplesHeaders = null;
            List<Element> headers = null;

            Element tuplesRows = null;
            List<Element> rows = null;

            for (Element element : axes0) {
                Attribute atributo = element.getAttribute("name");
                if (atributo.getValue().equals("Axis0")) {
                    tuplesHeaders = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    headers = tuplesHeaders.getChildren();
                    columnas = headers.size();
                } else if (atributo.getValue().equals("Axis1")) {
                    tuplesRows = element.getChild("Tuples", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset"));
                    rows = tuplesRows.getChildren();
                    rowsCount = rows.size();
                }
            }

            System.out.println("Columnas :" + columnas);
            System.out.println("Filas :" + rowsCount);
            int totalCeldas = columnas * rowsCount;

            System.out.println("Total GRID: " + totalCeldas);

            int cont = 0;
            int conCelldata = 0;

            int veces = 0;

            int celda1 = cont;
            int celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

            JsonArray arrayFilas = new JsonArray();
            JsonObject fila = null;
            JsonArray arrayValores = new JsonArray();
            JsonObject valor = null;

            List<Double> listValoresCeldas = new ArrayList<Double>();
            List<Double> listaValoresTotales = new ArrayList<Double>();

            double valorDouble = 0.0;

            int numFilas = 1;
            while (cont < totalCeldas) {

                veces++;
                String valorStr = "";

                //if(veces <= columnas){
                if (celda1 == celda2) {
//					    System.out.println("Veces :" +veces);
                    valorStr = celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue();
//						System.out.println("Valor str: "+valorStr);
                    valorDouble = Double.parseDouble(celdas.get(conCelldata).getChild("FmtValue", Namespace.getNamespace("urn:schemas-microsoft-com:xml-analysis:mddataset")).getValue());
                    cont++;
                    conCelldata++;

                    celda1 = cont;
                    if (conCelldata == celdas.size()) {
                        celda2 = 9999;
                    } else {
                        celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));
                    }

                } else if (celda1 < celda2) {

                    cont++;
                    celda1 = cont;
                    valorStr = "";
                    valorDouble = 0.0;
                    celda2 = Integer.parseInt(celdas.get(conCelldata).getAttributeValue("CellOrdinal"));

                }

                /*Realiza Logica*/
//					valor = new JsonObject();
//					valor.addProperty("cantidad",valorStr);
//					valor.addProperty("porcentaje", "");
//					valor.addProperty("color", "");
//					/*--------------*/
//					arrayValores.add(valor);
                if (numFilas == (rowsCount)) {
//						System.out.println("Valor doble: "+valorDouble);
                    listaValoresTotales.add(valorDouble);
                } else {
                    listValoresCeldas.add(valorDouble);
                }

                //}else{
                if (veces == columnas) {
//						System.out.println("------------FILA-------------" +numFilas);
                    veces = 0;
                    numFilas++;
                }
                //System.out.println("Agrega Fila "+numFilas+" Contador: "+cont);
                //veces=1;
                /*arrayFilas.add(arrayValores);
                 arrayValores = new JsonArray();*/

                //}
                /*if(cont == totalCeldas){
                 arrayFilas.add(arrayValores);
                 }*/
            }

            //System.out.println(arrayFilas.toString());
            System.out.println("Valores celda lista : " + listValoresCeldas.size());
            System.out.println("Valores totales lista : " + listaValoresTotales.size());

            Map<String, Object> valores = new HashMap<String, Object>();

            valores.put("listaCeldas", listValoresCeldas);
            valores.put("listaTotales", listaValoresTotales);
            valores.put("columnas", columnas);

            armaJsonNew(valores, flagsColumnas);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return jsonRepuestas;

    }

    @SuppressWarnings({"unchecked", "unused"})
    public static JsonArray armaJsonNew(Map<String, Object> listas, int[] flagsColumnas) {

        List<Double> valoresCeldas = (List<Double>) listas.get("listaCeldas");
        List<Double> valoresTotales = (List<Double>) listas.get("listaTotales");
        int columnas = Integer.parseInt(listas.get("columnas").toString());
        DecimalFormat formateador = new DecimalFormat("###,###");

        int veces = 0;

        JsonArray arrayFilas = new JsonArray();
        JsonObject fila = null;
        JsonArray arrayValores = new JsonArray();
        JsonObject valor = null;

        int pos_vs = 2;
        for (int i = 0; i < valoresCeldas.size(); i++) {

            double porcentaje = 0.0;
            String valorStr = "";
            String color = "";

            veces++;
            if (flagsColumnas[veces - 1] == 1) {
                if (veces > 4) {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresCeldas.get(i - pos_vs)) * 100.0;
                    System.out.println("cantidad: " + valoresCeldas.get(i));
                    System.out.println("Cantidad vs" + valoresCeldas.get(i - pos_vs));
                    pos_vs += 3;
                } else {
                    porcentaje = ((double) valoresCeldas.get(i) / (double) valoresTotales.get(veces - 1)) * 100.0;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorStr = formateador.format(valoresCeldas.get(i));

            }

            valor = new JsonObject();
            valor.addProperty("cantidad", valorStr);
            valor.addProperty("porcentaje", Math.abs((int) Math.round(porcentaje)));
            valor.addProperty("color", color);

            arrayValores.add(valor);

            if (veces == columnas) {
                arrayFilas.add(arrayValores);
                arrayValores = new JsonArray();
                System.out.println("------------FILA-------------");
                veces = 0;
                pos_vs = 2;
            }

        }

        /*Obtener Porcentajes de fila TOTALES */
        int pos_vsTot = 2;
        double porcentajeTot = 0.0;
        JsonObject jsonTotales = null;
        JsonArray arrayTotales = new JsonArray();

        for (int a = 0; a < valoresTotales.size(); a++) {
            jsonTotales = new JsonObject();
            String color = "";
            if (a > 4) {
                porcentajeTot = (double) valoresTotales.get(a) / (double) valoresTotales.get(a - pos_vsTot) * 100.0;
                pos_vs += 3;
            } else {
                porcentajeTot = 100.00;
            }

            if (porcentajeTot > 0) {
                color = "verde";
            } else {
                color = "rojo";
            }

            jsonTotales.addProperty("cantidad", formateador.format(Math.abs(valoresTotales.get(a))));
            jsonTotales.addProperty("porcentaje", "" + Math.abs((int) Math.round(porcentajeTot)) + "%");
            jsonTotales.addProperty("color", color);
            arrayValores.add(jsonTotales);

        }

        arrayFilas.add(arrayValores);

        System.out.println("" + arrayFilas.toString());

        return arrayFilas;

    }

    @SuppressWarnings("unused")
    public static JsonObject armaJsonPagosServicios(List<String> valoresCeldas, int[] flagsColumnas, List<Integer> numCeldas) {

        JsonArray arrayValorPorcentaje = new JsonArray();
        JsonObject jsonDias = new JsonObject();

        try {
            /*Obtener totales*/
            DecimalFormat formateador = new DecimalFormat("###,###.#");
            int columnas = flagsColumnas.length;

            System.out.println("Columnas :" + columnas);
            ArrayList<Double> totales = new ArrayList<Double>();

            int indice = (valoresCeldas.size() - columnas);
            int indiceColumnas = 0;

            while (indice < valoresCeldas.size()) {

                if (flagsColumnas[indiceColumnas] == 1) {
                    System.out.println(formateador.format(Double.parseDouble(valoresCeldas.get(indice))));
                    totales.add(Double.parseDouble(valoresCeldas.get(indice)));
                }

                indiceColumnas++;
                indice++;
            }

            /*Obtener Filas*/
            int cont = 0;
            int veces = 0;
            int corte = 1;

            ArrayList<Double> valores = new ArrayList<Double>();

            while (cont < numCeldas.size() && cont != (valoresCeldas.size() - columnas)) {

                JsonObject valorPorcentaje = null;

                if ((corte - 1) == 2) {
                    corte++;
                    System.out.println("Mi contador es :" + cont + " no debo tomar el valor");
                } else {
                    System.out.println("Mi contador es :" + cont + " si debo tomar el valor");
                    //					System.out.println("Num. Celda: "+numCeldas.get(cont));
                    //					System.out.println("Corte: "+(corte-1));
                    if (veces != numCeldas.get(cont)) {
                        break;
                    }

                    if (flagsColumnas[corte - 1] == 1) {
                        valores.add(Double.parseDouble(valoresCeldas.get(cont)));
                        System.out.println("$" + formateador.format(Double.parseDouble(valoresCeldas.get(cont))));
                        //System.out.println("$"+Double.parseDouble(valoresCeldas.get(cont)));
                    }

                    if (corte == flagsColumnas.length) {
                        System.out.println("Agrega Fila");
                        corte = 1;
                        veces += 1;
                    } else {
                        corte++;
                        veces++;
                    }

                    cont++;
                }

            }

            // int positionTotales = 0;
            int veces1 = 0;
            int dia = 1;

            JsonObject valorProcentaje = null;

            int pos_vs = 2;

            for (int i = 0; i < valores.size(); i++) {

                veces1++;
                valorProcentaje = new JsonObject();
                double porcentaje = 0.0;

                double valor = 0.0;
                int valorInt = 0;
                int posicion = 3;
                String color = "verde";

                if (veces1 > 4) {
                    valor = valores.get(i) / 1000;
                    // valorInt = (int)Math.round(valor);
                    double valor_vs = valores.get(i - pos_vs) / 1000.0;
                    // int valor_vsInt = (int)Math.round(valor_vs);
                    porcentaje = (valor / valor_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    valor = valores.get(i) / 1000;
                }

                if (porcentaje > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                valorProcentaje.addProperty("cantidad", formateador.format(Math.abs(valor)));
                valorProcentaje.addProperty("porcentaje", Math.abs((int) Math.round(porcentaje)) + "%");
                valorProcentaje.addProperty("color", color);

                arrayValorPorcentaje.add(valorProcentaje);

                if (veces1 == 7) {
                    jsonDias.add(getdia(dia), arrayValorPorcentaje);
                    dia++;
                    arrayValorPorcentaje = new JsonArray();
                    veces1 = 0;
                    pos_vs = 2;
                }
            }
            // rellena dias
            while (dia < 8) {
                jsonDias.add(getdia(dia), new JsonArray());
                dia++;
            }

            // Porcentaje de Totales
            int pos_vsTot = 2;
            double porcentajeTot = 0.0;
            int veces_tot = 1;
            JsonObject jsonTotales = null;
            JsonArray arrayTotales = new JsonArray();

            for (int a = 0; a < totales.size(); a++) {
                jsonTotales = new JsonObject();
                String color = "";
                if (a > 3) {
                    porcentajeTot = (double) totales.get(a) / (double) totales.get(a - pos_vs) * 100.0;
                    pos_vs += 2;
                } else {
                    porcentajeTot = 100.00;
                }

                if (porcentajeTot > 0) {
                    color = "verde";
                } else {
                    color = "rojo";
                }

                jsonTotales.addProperty("cantidad", formateador.format(Math.abs(totales.get(a))));
                jsonTotales.addProperty("porcentaje", "" + Math.abs((int) Math.round(porcentajeTot)) + "%");
                jsonTotales.addProperty("color", color);
                arrayTotales.add(jsonTotales);

            }

            jsonDias.add("totales", arrayTotales);

        } catch (Exception e) {
            e.printStackTrace();
            jsonDias = null;
        }

        return jsonDias;

    }

    static protected String getdia(int nuDia) {
        String dia = "";
        if (nuDia == 1) {
            dia = "lunes";
        } else if (nuDia == 2) {
            dia = "martes";
        } else if (nuDia == 3) {
            dia = "miercoles";
        } else if (nuDia == 4) {
            dia = "jueves";
        } else if (nuDia == 5) {
            dia = "viernes";
        } else if (nuDia == 6) {
            dia = "sabado";
        } else if (nuDia == 7) {
            dia = "domingo";
        }
        return dia;

    }
}
