package com.gruposalinas.migestion.controller;

import com.gruposalinas.migestion.business.ef.IndicadoresBI;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/controladores")
public class Controladores {

    private static Logger logger = LogManager.getLogger(Controladores.class);

    @Autowired
    IndicadoresBI indicadoresBI;

    //10.51.210.239:8080/migestion/getPorcentajesColocacion.htm?geografia=480100 - MEGA DF LA LUNA&nivel=T
    @RequestMapping(value = "/getPorcentajesColocacion", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> getPorcentajesColocacion(
            @RequestParam(value = "geografia", required = true, defaultValue = "") String geografia,
            @RequestParam(value = "nivel", required = true, defaultValue = "") String nivel, HttpServletRequest request) throws UnsupportedEncodingException {

        Map<String, String> res = null;

        try {
            res = indicadoresBI.getPorcentajesColocacion(geografia, nivel);
            if (res != null) {
                //logger.info(res.toString());
            }
        } catch (Exception e) {
            //logger.info(e);
        }
        return res;
    }

    //10.51.210.239:8080/migestion/controladores/getPorcentajesColocacion.json?ceco=480100&nombreCeco=MEGA DF LA LUNA&nivel=T
    @RequestMapping(value = "/getPorcentajesColocacionTiempoReal", method = RequestMethod.GET)
    public @ResponseBody
    String getPorcentajesColocacionTiempoReal(
            @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
            @RequestParam(value = "nivel", required = true, defaultValue = "") String nivel, HttpServletRequest request) throws UnsupportedEncodingException {

        Map<String, String> res = null;

        String result = "";

        try {
            res = indicadoresBI.getPorcentajesColocacionTiempoReal(ceco, nombreCeco, nivel);
            if (res != null) {
                //logger.info("res...   " + res.toString());
                result
                        = "{"
                        + "\"idCeco\":\"" + ceco + "\","
                        + "\"descCeco\":\"" + nombreCeco + "\","
                        + "\"colorPersonales\":\"" + res.get("colorPersonales") + "\","
                        + "\"valorConsumo\":" + res.get("valorConsumo") + ","
                        + "\"colorConsumo\":\"" + res.get("colorConsumo") + "\","
                        + "\"valorPersonales\":" + res.get("valorPersonales") + ","
                        + "\"compromisoConsumo\":" + res.get("compromisoConsumo") + ","
                        + "\"compromisoPersonales\":" + res.get("compromisoPersonales") + ","
                        + "\"avanceTiempoRealConsumo\":" + res.get("avanceTiempoRealConsumo") + ","
                        + "\"avanceTiempoRealPersonales\":" + res.get("avanceTiempoRealPersonales") + ""
                        + "}";
            } else {

                String fallo = "-";

                result
                        = "{"
                        + "\"idCeco\":\"" + ceco + "\","
                        + "\"descCeco\":\"" + nombreCeco + "\","
                        + "\"colorPersonales\":\"" + fallo + "\", "
                        + "\"valorConsumo\":\"" + fallo + "\", "
                        + "\"colorConsumo\":\"" + fallo + "\", "
                        + "\"valorPersonales\":\"" + fallo + "\","
                        + "\"compromisoConsumo\":\"" + fallo + "\","
                        + "\"compromisoPersonales\":\"" + fallo + "\","
                        + "\"avanceTiempoRealConsumo\":\"" + fallo + "\","
                        + "\"avanceTiempoRealPersonales\":\"" + fallo + "\""
                        + "}";
            }

        } catch (Exception e) {
            logger.info(e);

            String fallo = "-";

            result
                    = "{"
                    + "\"idCeco\":\"" + ceco + "\","
                    + "\"descCeco\":\"" + nombreCeco + "\","
                    + "\"colorPersonales\":\"" + fallo + "\", "
                    + "\"valorConsumo\":\"" + fallo + "\", "
                    + "\"colorConsumo\":\"" + fallo + "\", "
                    + "\"valorPersonales\":\"" + fallo + "\","
                    + "\"compromisoConsumo\":\"" + fallo + "\","
                    + "\"compromisoPersonales\":\"" + fallo + "\","
                    + "\"avanceTiempoRealConsumo\":\"" + fallo + "\","
                    + "\"avanceTiempoRealPersonales\":\"" + fallo + "\""
                    + "}";
        }

        /*
		result =
			"{"+
				"\"idCeco\":\""+ceco+"\","+
				"\"descCeco\":\""+nombreCeco+"\","+
				"\"colorPersonales\":\""+"red"+"\","+
				"\"valorConsumo\":"+"78"+","+
				"\"colorConsumo\":\""+"green"+"\","+
				"\"valorPersonales\":"+"64"+","+
				"\"compromisoConsumo\":"+1000+","+
				"\"compromisoPersonales\":"+1000+","+
				"\"avanceTiempoRealConsumo\":"+780+","+
				"\"avanceTiempoRealPersonales\":"+640+""+
			"}";
         */
        return result;
    }

    //10.51.210.239:8080/migestion/controladores/getPorcentajesColocacionTiempoRealHoy.json?ceco=480100&nombreCeco=MEGA DF LA LUNA&nivel=T
    @RequestMapping(value = "/getPorcentajesColocacionTiempoRealHoy", method = RequestMethod.GET)
    public @ResponseBody
    String getPorcentajesColocacionTiempoRealHoy(
            @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
            @RequestParam(value = "nivel", required = true, defaultValue = "") String nivel, HttpServletRequest request) throws UnsupportedEncodingException {

        Map<String, String> res = null;

        String result = "";

        try {
            res = indicadoresBI.getPorcentajesColocacionTiempoRealHoy(ceco, nombreCeco, nivel);
            if (res != null) {
                logger.info(res.toString());
                result
                        = "{"
                        + "\"idCeco\":\"" + ceco + "\","
                        + "\"descCeco\":\"" + nombreCeco + "\","
                        + "\"colorPersonales\":\"" + res.get("colorPersonales") + "\","
                        + "\"valorConsumo\":" + res.get("valorConsumo") + ","
                        + "\"colorConsumo\":\"" + res.get("colorConsumo") + "\","
                        + "\"valorPersonales\":" + res.get("valorPersonales") + ","
                        + "\"compromisoConsumo\":" + res.get("compromisoConsumo") + ","
                        + "\"compromisoPersonales\":" + res.get("compromisoPersonales") + ","
                        + "\"avanceTiempoRealConsumo\":" + res.get("avanceTiempoRealConsumo") + ","
                        + "\"avanceTiempoRealPersonales\":" + res.get("avanceTiempoRealPersonales") + ""
                        + "}";
            } else {

                String fallo = "-";

                result
                        = "{"
                        + "\"idCeco\":\"" + ceco + "\","
                        + "\"descCeco\":\"" + nombreCeco + "\","
                        + "\"colorPersonales\":\"" + fallo + "\", "
                        + "\"valorConsumo\":\"" + fallo + "\", "
                        + "\"colorConsumo\":\"" + fallo + "\", "
                        + "\"valorPersonales\":\"" + fallo + "\","
                        + "\"compromisoConsumo\":\"" + fallo + "\","
                        + "\"compromisoPersonales\":\"" + fallo + "\","
                        + "\"avanceTiempoRealConsumo\":\"" + fallo + "\","
                        + "\"avanceTiempoRealPersonales\":\"" + fallo + "\""
                        + "}";
            }

        } catch (Exception e) {
            logger.info(e);

            String fallo = "-";

            result
                    = "{"
                    + "\"idCeco\":\"" + ceco + "\","
                    + "\"descCeco\":\"" + nombreCeco + "\","
                    + "\"colorPersonales\":\"" + fallo + "\", "
                    + "\"valorConsumo\":\"" + fallo + "\", "
                    + "\"colorConsumo\":\"" + fallo + "\", "
                    + "\"valorPersonales\":\"" + fallo + "\","
                    + "\"compromisoConsumo\":\"" + fallo + "\","
                    + "\"compromisoPersonales\":\"" + fallo + "\","
                    + "\"avanceTiempoRealConsumo\":\"" + fallo + "\","
                    + "\"avanceTiempoRealPersonales\":\"" + fallo + "\""
                    + "}";
        }

        /*
		result =
			"{"+
				"\"idCeco\":\""+ceco+"\","+
				"\"descCeco\":\""+nombreCeco+"\","+
				"\"colorPersonales\":\""+"red"+"\","+
				"\"valorConsumo\":"+"78"+","+
				"\"colorConsumo\":\""+"green"+"\","+
				"\"valorPersonales\":"+"64"+","+
				"\"compromisoConsumo\":"+1000+","+
				"\"compromisoPersonales\":"+1000+","+
				"\"avanceTiempoRealConsumo\":"+780+","+
				"\"avanceTiempoRealPersonales\":"+640+""+
			"}";
         */
        return result;
    }

}
