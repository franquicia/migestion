package com.gruposalinas.migestion.controller;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.gruposalinas.migestion.domain.DetalleTareasDTO;
import com.gruposalinas.migestion.domain.MisTareasDTO;
import com.gruposalinas.migestion.domain.MisTareasHeadDTO;
import com.gruposalinas.migestion.domain.TareasMiGenteDTO;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/tareas")
public class TareasController {

    public final static String STR_URI_EKT_DES = "http://10.50.109.47:8081/ServicioRest/services";
    public final static String MTD_PROYLIST = "/admonIndicadorTarea/getTareasCreador?";
    public final static String MTD_RESP_DETA = "/initPerfil/projectDetails?projectId=";
    public final static String MTD_RESP_DET = "/initGerente/projectDetails?";
    public final static String MTD_APRUEBA_TAREA = "/initPerfil/projectList?";
    public final static String MTD_KPIHEADER = "/admonIndicadorTarea/getHeadTarea?";

    //http://localhost:8080/migestion/tareas/TasksMyPeople.json?Usuario=<?>&inicio=<?>&fin=<?>&Ceco=<?>
    @RequestMapping(value = "/TasksMyPeople", method = RequestMethod.GET)
    public ModelAndView getTasksMyPeople(HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException {

        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        String parametros = String.format("{'Usuario':'%s','inicio': '%s','fin': '%s','Ceco': '%s'}",
                request.getParameter("Usuario"),
                request.getParameter("inicio"),
                request.getParameter("fin"),
                request.getParameter("Ceco"));

        parametros = URLEncoder.encode(parametros, "UTF-8");

        HttpURLConnection conn = null;
        JsonArray jsonArray = null;
        try {
            conn = (HttpURLConnection) new URL(STR_URI_EKT_DES + MTD_PROYLIST + "parametros=" + parametros).openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            jsonArray = new JsonParser().parse(new InputStreamReader(conn.getInputStream(), "UTF-8")).getAsJsonArray();

            conn.disconnect();

            mv.addObject("TasksMyPeople", new GsonBuilder().create().fromJson(jsonArray, new TypeToken<List<TareasMiGenteDTO>>() {
            }.getType()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (jsonArray != null) {
                jsonArray = null;
            }
        }

        return mv;

    }

    //http://localhost:8080/migestion/tareas/tasksDetail.json?projectId=<?>
    @RequestMapping(value = "/tasksDetail", method = RequestMethod.GET)
    public ModelAndView getTasksDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException {

        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        HttpURLConnection conn = null;
        JsonObject jsonObject = null;
        JsonElement jsonElement = null;
        try {
            conn = (HttpURLConnection) new URL(STR_URI_EKT_DES + MTD_RESP_DETA + request.getParameter("projectId")).openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            jsonObject = (JsonObject) new JsonParser().parse(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            jsonElement = jsonObject.getAsJsonArray("ProjectData").get(0);

            conn.disconnect();

            mv.addObject("detalleTarea", new GsonBuilder().create().fromJson(jsonElement, DetalleTareasDTO.class));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (jsonObject != null) {
                jsonObject = null;
            }
            if (jsonElement != null) {
                jsonElement = null;
            }
        }

        return mv;

    }

    //http://localhost:8080/migestion/tareas/myTasks.json?userId=<?>&idOwner=<?>&unitId=<?>&fromDate=<?>&toDate=<?>&tipo=<?>&negocio=<?>&completed=<?>
    @RequestMapping(value = "/myTasks", method = RequestMethod.GET)
    public ModelAndView getMyTasks(HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException {

        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        String parametros = String.format("userId=%s&idOwner=%s&unitId=%s&fromDate=%s&toDate=%s&tipo=%s&negocio=%s&completed=%s",
                request.getParameter("userId"),
                request.getParameter("idOwner"),
                request.getParameter("unitId"),
                request.getParameter("fromDate"),
                request.getParameter("toDate"),
                request.getParameter("tipo"),
                request.getParameter("negocio"),
                request.getParameter("completed"));

        HttpURLConnection conn = null;
        JsonArray jsonArray = null;
        try {
            conn = (HttpURLConnection) new URL(STR_URI_EKT_DES + MTD_APRUEBA_TAREA + parametros).openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            jsonArray = (JsonArray) new JsonParser().parse(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            conn.disconnect();

            mv.addObject("myTasks", new GsonBuilder().create().fromJson(jsonArray, new TypeToken<List<MisTareasDTO>>() {
            }.getType()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (jsonArray != null) {
                jsonArray = null;
            }
        }

        return mv;
    }

    //http://localhost:8080/migestion/tareas/myTasksHead.json?idTarea=<?>&idCeco=<?>
    @RequestMapping(value = "/myTasksHead", method = RequestMethod.GET)
    public ModelAndView getMyTasksHead(HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException {

        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        String parametros = String.format("idTarea=%s&idCeco=%s",
                request.getParameter("idTarea"),
                request.getParameter("idCeco"));

        HttpURLConnection conn = null;
        JsonElement jsonElement = null;
        try {
            conn = (HttpURLConnection) new URL(STR_URI_EKT_DES + MTD_KPIHEADER + parametros).openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            jsonElement = new JsonParser().parse(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            conn.disconnect();

            mv.addObject("myTasksHead", new GsonBuilder().create().fromJson(jsonElement.getAsJsonArray(), new TypeToken<List<MisTareasHeadDTO>>() {
            }.getType()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (jsonElement != null) {
                jsonElement = null;
            }
        }

        return mv;

    }

    //http://localhost:8080/migestion/tareas/tasksDetailGerente.json?unitId=<?>&userId=<?>&projectId=<?>
    @RequestMapping(value = "/tasksDetailGerente", method = RequestMethod.GET)
    public ModelAndView getTasksDetailGerente(HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException {

        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        String parametros = String.format("unitId=%s&userId=%s&projectId=%s",
                request.getParameter("unitId"),
                request.getParameter("userId"),
                request.getParameter("projectId"));

        HttpURLConnection conn = null;
        JsonObject jsonObject = null;
        JsonElement jsonElement = null;
        try {
            conn = (HttpURLConnection) new URL(STR_URI_EKT_DES + MTD_RESP_DET + parametros).openConnection();
            System.out.println(STR_URI_EKT_DES + MTD_RESP_DET + parametros);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            jsonObject = (JsonObject) new JsonParser().parse(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            jsonElement = jsonObject.getAsJsonArray("ProjectData").get(0);

            conn.disconnect();
            mv.addObject("detalleTareaGerente", new GsonBuilder().create().fromJson(jsonElement, DetalleTareasDTO.class));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (jsonObject != null) {
                jsonObject = null;
            }
            if (jsonElement != null) {
                jsonElement = null;
            }
        }

        return mv;

    }
}
