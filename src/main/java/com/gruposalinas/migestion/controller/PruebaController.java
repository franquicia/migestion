package com.gruposalinas.migestion.controller;

import com.gruposalinas.migestion.business.ArqueoBI;
import com.gruposalinas.migestion.domain.ArqueoDTO;
import com.gruposalinas.migestion.util.MiGestionPruebas;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PruebaController {

	
	@Autowired
	ArqueoBI arqueoBI;
	
	@RequestMapping(value="/pruebasGestion.htm", method = RequestMethod.GET)
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response) throws Exception{ 
		List<ArqueoDTO> listaMovil = null;
			
		
		listaMovil = arqueoBI.obtieneNombre("236737");
	

		return new ModelAndView("pruebaMiGestion","json",listaMovil.get(0).getNombreCeco());
	}

}
