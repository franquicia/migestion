package com.gruposalinas.migestion.controller;

import com.gruposalinas.migestion.domain.UsuarioDTO;
import com.gruposalinas.migestion.resources.GTNConstantes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class VistaCumplimientoTareas {

    private static Logger logger = LogManager.getLogger(VistaCumplimientoTareas.class);
    private String nombreCecoAux;
    @SuppressWarnings("unused")
    private boolean opcionReporte;

    /*DEJO EL METODO PARA HACER PRUEBAS PERO SOLO NECESITO A PARTIR DE VISTA-FILTRO-CUMPLIMIENTO-VISITAS*/
    @RequestMapping(value = {"central/vistaCumplimientoTareas.htm", "central/vistaCumplimientoTareasMovil.htm"}, method = RequestMethod.GET)
    public ModelAndView vistaCumplimientoTareas(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String uri = request.getRequestURI();
        if (uri.contains("central/vistaCumplimientoTareasMovil.htm")) {
            this.opcionReporte = true;
        } else {
            this.opcionReporte = false;
        }

        logger.info("...ENTRO AL CONTROLLER vistaCumplimientoTareas...");

        int idUsuario = 0;

        UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
        idUsuario = Integer.parseInt(userSession.getIdUsuario());
        //idUsuario=196228;
        logger.info("USUARIO " + idUsuario);
        ModelAndView mv = new ModelAndView("vistaCumplimientoTareas");
        mv.addObject("idUsuario", idUsuario);
        mv.addObject("rutaHostImagen", GTNConstantes.getRutaImagen());
        return mv;
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "central/vistaFiltroCumplimientoTareas.htm", method = RequestMethod.POST)
    public ModelAndView vistaFiltroCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
            @RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
            @RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,
            @RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,
            @RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
            @RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
            @RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
            @RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
            @RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception {

        //logger.info("...ENTRO AL CONTROLLER vistaFiltroCumplimientoTareas...");
        int idUsuario = 0;
        UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
        idUsuario = Integer.parseInt(userSession.getIdUsuario());

        if (nivelPerfil == 1) {
            banderaCecoPadre = 1;
        }
        logger.info("banderaCecoPadre... " + banderaCecoPadre);
        logger.info("nivelPerfil... " + nivelPerfil);
        logger.info("porcentajeG... " + porcentajeG);
        logger.info("idCecoPadre... " + idCecoPadre);
        logger.info("nombreCeco... " + nombreCeco);
        logger.info("seleccionAno... " + seleccionAno);
        logger.info("seleccionMes... " + seleccionMes);
        logger.info("seleccionCanal... " + seleccionCanal);
        logger.info("seleccionPais... " + seleccionPais);

        this.nombreCecoAux = nombreCeco.split("/")[0];
        logger.info("nombreCecoAux " + this.nombreCecoAux);

        //idUsuario=196228;
        logger.info("USUARIO " + idUsuario);
        ModelAndView mv = new ModelAndView("vistaFiltroCumplimientoTareas");

        List l = new ArrayList(8);
        mv.addObject("nivelPerfil", 0);
        mv.addObject("porcentajeG", 10);
        mv.addObject("idCeco", 1);
        mv.addObject("idCecoPadre", 2);
        mv.addObject("nombreCeco", "miCeco");
        mv.addObject("banderaCecoPadre", true);
        mv.addObject("seleccionAno", 2017);
        mv.addObject("seleccionMes", 6);
        mv.addObject("seleccionCanal", "1");
        mv.addObject("seleccionPais", "2");
        mv.addObject("porcentaje", 10);
        mv.addObject("conteo", 8);
        mv.addObject("listaChecklist", l);
        mv.addObject("listaSucursales", l);
        mv.addObject("listaSucursalesSize", l.size());

        //PERFIL 0 - 1 VA A  vistaFiltroCumplimientoTareas
        //PERFIL 2 - 3 - 4 va a vistaTerritCumplimientoTareas
        return mv;
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "central/vistaTerritCumplimientoTareas.htm", method = RequestMethod.POST)
    public ModelAndView vistaTerritCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,
            @RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
            @RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
            @RequestParam(value = "idCeco", required = true, defaultValue = "0") int idCeco,
            @RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,
            @RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
            @RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
            @RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
            @RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
            @RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception {

        //logger.info("...ENTRO AL CONTROLLER vistaTerritCumplimientoTareas...");
        logger.info("porcentajeG... " + porcentajeG);
        logger.info("nivelPerfil... " + nivelPerfil);
        logger.info("idCeco... " + idCeco);
        logger.info("idCecoPadre... " + idCecoPadre);
        logger.info("nombreCeco... " + nombreCeco);
        logger.info("banderaCecoPadre... " + banderaCecoPadre);
        logger.info("seleccionAno... " + seleccionAno);
        logger.info("seleccionMes... " + seleccionMes);
        logger.info("seleccionCanal... " + seleccionCanal);
        logger.info("seleccionPais... " + seleccionPais);

        String nombreAux[] = nombreCeco.split(",");
        int nombreAuxSize = nombreAux.length;
        this.nombreCecoAux = nombreAux[nombreAuxSize - 1];
        logger.info("nombreCecoAux " + nombreCecoAux);

        int idUsuario = 0;
        UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
        idUsuario = Integer.parseInt(userSession.getIdUsuario());
        //idUsuario=196228;
        logger.info("USUARIO " + idUsuario);
        ModelAndView mv = new ModelAndView("vistaTerritCumplimientoTareas");

        List l = new ArrayList(8);

        mv.addObject("nivelPerfil", 0);
        mv.addObject("porcentajeG", 10);
        mv.addObject("idCeco", 1);
        mv.addObject("idCecoPadre", 2);
        mv.addObject("nombreCeco", "miCeco");
        mv.addObject("banderaCecoPadre", true);
        mv.addObject("seleccionAno", 2017);
        mv.addObject("seleccionMes", 6);
        mv.addObject("seleccionCanal", "1");
        mv.addObject("seleccionPais", "2");
        mv.addObject("porcentaje", 10);
        mv.addObject("conteo", 1);
        mv.addObject("listaChecklist", l);
        mv.addObject("listaSucursales", l);
        mv.addObject("listaSucursalesSize", 10);

        return mv;
    }

    //ES POST
    //localhost:8080/migestion/central/vistaDetalleCumplimientoVisitas.htm
    @SuppressWarnings({"unused", "rawtypes"})
    @RequestMapping(value = "central/vistaDetalleCumplimientoTareas.htm", method = RequestMethod.POST)
    public ModelAndView vistaDetalleCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,
            @RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,
            @RequestParam(value = "idCeco", required = true, defaultValue = "0") int idCeco,
            @RequestParam(value = "idCecoHijo", required = true, defaultValue = "0") int idCecoHijo,
            @RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
            @RequestParam(value = "idCheck", required = true, defaultValue = "0") int idCheck,
            @RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
            @RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
            @RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
            @RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
            @RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
            @RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception {

        logger.info("...ENTRO AL CONTROLLER vistaDetalleCumplimientoTareas...");

        /*logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("idCeco... "+idCeco);
		logger.info("idCecoHijo... "+idCecoHijo);
		logger.info("nombreCeco... "+nombreCeco);
		logger.info("idCheck... "+idCheck);
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
         */
        int idUsuario = 0;
        UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");

        ModelAndView mv = new ModelAndView("vistaDetalleCumplimientoTareas");
        List l = new ArrayList(8);

        mv.addObject("nivelPerfil", 0);
        mv.addObject("porcentajeG", 10);
        mv.addObject("idCeco", 1);
        mv.addObject("idCecoPadre", 2);
        mv.addObject("nombreCeco", "miCeco");
        mv.addObject("banderaCecoPadre", true);
        mv.addObject("seleccionAno", 2017);
        mv.addObject("seleccionMes", 6);
        mv.addObject("seleccionCanal", "1");
        mv.addObject("seleccionPais", "2");
        mv.addObject("porcentaje", 10);
        mv.addObject("conteo", 1);
        mv.addObject("listaChecklist", l);
        mv.addObject("listaSucursales", l);
        mv.addObject("listaSucursalesSize", 10);

        return mv;
    }

    @SuppressWarnings({"rawtypes", "unused"})
    @RequestMapping(value = "/ajaxFiltroSucursal", method = RequestMethod.GET)
    public @ResponseBody
    List<List> ajaxFiltroSucursal(
            @RequestParam(value = "idCeco", required = true, defaultValue = "0") int idCeco,
            @RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
            @RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
            @RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
            @RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
            @RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais, HttpServletRequest request) throws Exception {

        logger.info("...ENTRO AL AJAX ajaxFiltroSucursal...");

        logger.info("idCeco... " + idCeco);
        logger.info("banderaCecoPadre... " + banderaCecoPadre);
        logger.info("seleccionAno... " + seleccionAno);
        logger.info("seleccionMes... " + seleccionMes);
        logger.info("seleccionCanal... " + seleccionCanal);
        logger.info("seleccionPais... " + seleccionPais);

        int idUsuario = 0;
        UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
        idUsuario = Integer.parseInt(userSession.getIdUsuario());
        //idUsuario=196228;
        logger.info("USUARIO " + idUsuario);

        List<List> listaReporteCum = new ArrayList<List>();

        int porcentaje = 0, conteo = 0;
        Map<String, Object> map = new HashMap<String, Object>();
        List<List> listRetorno = new ArrayList<List>();

        return listRetorno;
    }

}
