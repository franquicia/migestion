package com.gruposalinas.migestion.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.migestion.business.ExternoPuestoBI;
import com.gruposalinas.migestion.domain.ExternoPuestoDTO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Controller
public class InicioController {

    @Autowired
    ExternoPuestoBI externopuestobi;

    private static final Logger logger = LogManager.getLogger(InicioController.class);

    @RequestMapping(value = "central/inicio.htm", method = RequestMethod.GET)
    public ModelAndView mensajes(HttpServletRequest request, Exception ex) {
        //logger.info("ENTRO A inicio.HTM");
        //System.out.println("SI ESTOY EN INICIO!!!");
        return new ModelAndView("mensajes");
    }

    @RequestMapping(value = "central/store.htm", method = RequestMethod.GET)
    public ModelAndView controllerStore(HttpServletRequest request, Exception ex) {
        return new ModelAndView("pruebaStore");
    }

    @RequestMapping(value = "central/cierraSesion.htm", method = RequestMethod.GET)
    public String cierraSesionCentral(HttpServletRequest request, HttpServletResponse response) throws Exception {
        /*
		 * request.removeAttribute("user"); request.removeAttribute("nombre");
		 * request.removeAttribute("nomSucursal"); request.removeAttribute("urlServer");
		 * request.removeAttribute("perfilAdmin");
		 * request.removeAttribute("perfilReportes");
		 * request.removeAttribute("servidorApache");
		 * request.removeAttribute("startTime");
         */

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        request.getSession().setAttribute("user", null);
        request.getSession().invalidate();
        return "redirect:http://portal.socio.gs/Logout_azteca/cerrar_sesion.html";
        // return "http://portal.socio.gs/Logout_azteca/cerrar_sesion.html";
        // Para ocupara este cierre de sesion se tiene que crear un DNS para checklist y
        // pueda cerrar toda la sesion
        // return "redirect:http://portal.socio.gs/logoutportal";
    }

    @RequestMapping(value = "central/documentoPuestoExterno.htm", method = RequestMethod.POST)
    public ModelAndView postcontrollerPuestoExt(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idusuario", required = true) String numEmpleado,
            @RequestParam(value = "puesto", required = true) String puesto) throws IOException {
        String salida = "puestosExternos";
        ModelAndView mv = new ModelAndView(salida, "command", new ExternoPuestoDTO());

        //logger.info("Idusuario-->" + numEmpleado);
        //logger.info("puesto" + puesto);
        if (numEmpleado.contains("T")) {
            numEmpleado = numEmpleado.replaceAll("T", "");
        }
        try {
            //logger.info("paso");
            // List<ExternoPuestoDTO> lista= externopuestobi.actualizaPuesto();
        } catch (Exception e) {

        }
        return mv;

    }

    @RequestMapping(value = "central/aviso-de-privacidad.htm", method = RequestMethod.GET)
    public ModelAndView getavisoprivacidad(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        String salida = "aviso-de-privacidad";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        return mv;
    }

    @RequestMapping(value = "central/terminos-y-condiciones.htm", method = RequestMethod.GET)
    public ModelAndView getterminosycondiciones(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        String salida = "terminos-y-condiciones";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        return mv;
    }
}
