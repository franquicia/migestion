package com.gruposalinas.migestion.controller;

import com.gruposalinas.migestion.business.NotificacionesFRQBI;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class NotificacionesController {

    @Autowired
    NotificacionesFRQBI notificacionesBI;

    @RequestMapping(value = {"tienda/enviarNotificacion.htm", "central/enviarNotificacion.htm"}, method = RequestMethod.GET)
    public ModelAndView menuChecklist(HttpServletRequest requqest, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("enviarNotificacion");
        return mv;
    }

    @RequestMapping(value = {"tienda/enviarAccion.htm", "central/enviarAccion.htm"}, method = RequestMethod.POST)
    public ModelAndView asignacionChecklist(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "cecoPadre", required = true, defaultValue = "null") String cecoPadre,
            @RequestParam(value = "titulo", required = true, defaultValue = "false") String titulo,
            @RequestParam(value = "mensaje", required = true, defaultValue = "false") String mensaje)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("enviarAccion");
        String error = "SIN ERROR EN LA EJECUCION";
        int totalNotif = 0;
        try {
            totalNotif = notificacionesBI.enviarNotificaciones(Integer.parseInt(cecoPadre), titulo, mensaje);
        } catch (Exception e) {

            e.printStackTrace();
            error = "ERROR AL EJECUTAR:" + "\n" + e.getMessage();

        }
        mv.addObject("totalEnvio", totalNotif);
        mv.addObject("error", error);

        return mv;
    }
}
