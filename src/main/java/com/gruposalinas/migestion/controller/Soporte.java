package com.gruposalinas.migestion.controller;

import com.gruposalinas.migestion.domain.UsuarioDTO;
import com.gruposalinas.migestion.util.Ambientes;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/soporte")
public class Soporte {

    private static final Logger logger = LogManager.getLogger(Soporte.class);

    //http://localhost:8080/checklist/soporte/inicio.htm
    @RequestMapping(value = "/inicio", method = RequestMethod.GET)
    public ModelAndView getIndexSoporte(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        } else if (Ambientes.AMBIENTE.value().equals(Ambientes.DESARROLLO)) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(99999999))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexSoporte");

        //logger.info(request.getSession().getAttributeNames());
        mv.addObject("paso", new String("0"));

        return mv;
    }

}
