package com.gruposalinas.migestion.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.filter.GenericFilterBean;

public class FilterWsdl extends GenericFilterBean {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub

        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final HttpServletResponse httpResponse = (HttpServletResponse) response;
        // System.out.println("request is " + httpRequest.getClass());
        System.out.println(" URL Servicio WSDL: " + httpRequest.getRequestURL());
        // System.out.println("response is " + httpResponse.getClass());

        if (httpRequest.getMethod().compareTo("POST") == 0) {
            chain.doFilter(httpRequest, httpResponse);
        }

    }

}
