package com.gruposalinas.migestion.resources;

import com.gruposalinas.migestion.util.Ambientes;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

public class GTNConstantes {

    //public static final String URL_SERVIDOR_DESARROLLO 			= "http://10.50.109.39:8080"; //SOCIOSdesarrollobienestar.socio.gs
    //public static final String URL_SERVIDOR_DESARROLLO 			= "http://desarrollobienestar.socio.gs:8080";
    public static final String URL_SERVIDOR_DESARROLLO = "http://10.50.109.47:8080"; //Aquí va la ip de ambqiente de desarrollo
    public static final String URL_SERVIDOR_LOCAL = "http://" + GTNConstantes.getIPLocal() + ":8080"; //Aquí va la ip del desarrollador
    //public static final String URL_SERVIDOR_PRODUCCION 			= "http://10.53.29.67:9991";
    //public static final String URL_SERVIDOR_PRODUCCION 			= "http://10.53.29.67:9990";
    public static final String URL_SERVIDOR_PRODUCCION = "http://10.53.33.83:80";

    //public static final String URL_SERVIDOR_A_DESARROLLO 		= "http://10.50.109.39";
    public static final String URL_SERVIDOR_A_DESARROLLO = "http://10.50.109.47"; //Aquí va la ip de ambiente de desarrollo
    public static final String URL_SERVIDOR_A_LOCAL = "http://" + GTNConstantes.getIPLocal(); //Aquí va la ip del desarrollador
    //public static final String URL_SERVIDOR_A_PRODUCCION 		= "http://10.53.29.67";
    public static final String URL_SERVIDOR_A_PRODUCCION = "http://10.53.33.83";

    //public static final String IP_ORIGEN_DESARROLLO 			= "10.50.109.39";
    public static final String IP_ORIGEN_DESARROLLO = "10.50.109.47";
    public static final String IP_ORIGEN_LOCAL = GTNConstantes.getIPLocal();
    //public static final String IP_ORIGEN_PRODUCCION 			= "10.53.29.67";
    public static final String IP_ORIGEN_PRODUCCION = "10.53.33.83";

    public static final String IP_HOST_DESARROLLO = "192.168.2.1"; //Ip para pruebas en equipo local
    public static final String IP_HOST_PRODUCCION = "10.53.33.76";//Esta es la ip del servidor donde sera ejecutado el Scheduler

    //public static final String LLAVE_ENCRIPCION_DESARROLLO 		= "fce099383332d9ffa34c4f8a50dda9a0="; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String LLAVE_ENCRIPCION_DESARROLLO = "eJG7R3O/Z+8a/FzQb4X7zv5Um++9tHo="; //desarrollo nueva se debe comentar para activar simulacion de producción
    //public static final String LLAVE_ENCRIPCION_PRODUCCION 		= "fce099383332d9ffa34c4f8a50dda9a0=";
    public static final String LLAVE_ENCRIPCION_PRODUCCION = "Ux+Z68QHllMW6jyUfPaO2f5h7Oost8U=";//llave nueva de produccion
    public static final String LLAVE_ENCRIPCION_LOCAL = "fce099383332d9ffa34c4f8a50dda9a0=";//id=666
    public static final String LLAVE_ENCRIPCION_LOCAL_IOS = "fce099383332d9ff";//id=7

    //public static final String URL_WSTOKEN_DESARROLLO			= "http://desarrollo.socio.gs/Login5/token_esp/token_esp.asmx?WSDL"; //unreacheable
    //public static final String URL_WSTOKEN_PRODUCCION			= "http://portal.socio.gs/Login5/token_esp/token_esp.asmx?WSDL"; //unreacheable
    //public static final String URL_WSTOKEN_DESARROLLO				= "http://10.50.158.63/Login5/token_esp/token_esp.asmx?WSDL"; //desarrollo original se debe comentar para activar simulacion de producción
    //public static final String URL_WSTOKEN_DESARROLLO				= "http://portal.socio.gs/Login5/token_esp/token_esp.asmx?WSDL"; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String URL_WSTOKEN_DESARROLLO = "http://10.50.158.63/Login5/token_esp/token_esp.asmx?WSDL"; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String URL_WSTOKEN_PRODUCCION = "https://authapps.socio.gs/Login5/token_esp/token_esp.asmx?wsdl"; //ip para servicio de llave maestra

    //public static final String URL_LOGIN_LLAVE_DESARROLLO		= "http://desarrollo.socio.gs/Login5/login/default.aspx?token="; //unreacheable
    //public static final String URL_LOGIN_LLAVE_PRODUCCION		= "http://portal.socio.gs/Login5/login/default.aspx?token="; //unreacheable
    public static final String URL_LOGIN_LLAVE_DESARROLLO = "https://10.50.158.63/Login5/login/default.aspx?token=";  //desarrollo original se debe comentar para activar simulacion de producción
    //public static final String URL_LOGIN_LLAVE_DESARROLLO			= "https://portal.socio.gs/Login5/login/default.aspx?token=";  //desarrollo original se debe comentar para activar simulacion de producción
    public static final String URL_LOGIN_LLAVE_PRODUCCION = "https://authapps.socio.gs/Login5/login/default.aspx?token="; //ip para servicio de llave maestra

    //public static final String ID_APP_PORTALES_DESARROLLO 		= "59";
    public static final String ID_APP_PORTALES_DESARROLLO = "2"; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String ID_APP_PORTALES_PRODUCCION = "59";
    public static final String ID_APP_PORTALES_ID_LLAVE = "666";
    public static final String ID_APP_PORTALES_ID_LLAVE_IOS = "7";

    public static final String RUTA_IMAGEN_DESARROLLO = "http://10.50.109.39/Sociounico"; //Aquí va la url de desarrollo
    public static final String RUTA_IMAGEN_PRODUCCION = "http://imgsbienestar.socio.gs/sociounico";

    public static final String RUTA_IMAGEN_DESARROLLO_MOVIL = "http://10.50.109.39/Sociounico"; //Aquí va la url de desarrollo
    public static final String RUTA_IMAGEN_PRODUCCION_MOVIL = "https://200.38.122.186:8443/sociounico";

    public final static String URL_FIRMA_DESARROLLO = "https://10.51.146.186:8443/YakanaAuthServer/webresources/";
    public final static String URL_FIRMA_PRODUCCION = "https://10.63.11.173:8443/YakanaAuthServer/webresources/";//Servicio yakana

    public final static String URL_PRODUCTIVIDAD_DESARROLLO = "http://10.54.24.81:7082/WebServiceExtraccionRankingBAZ/ExtraccionRankingBAZ.asmx";
    public final static String URL_PRODUCTIVIDAD_PRODUCCION = "http://10.54.24.118:7082/WebServiceExtraccionRankingBAZ/ExtraccionRankingBAZ.asmx";

    public final static String URL_ARQUEO_DESARROLLO = "http://10.53.26.158/";
    public final static String URL_ARQUEO_PRODUCCION = "http://10.53.26.158/";

    public final static String URL_INCIDENTES_DESARROLLO = "http://10.54.28.194/wsGestionIncFranquicias/Service1.svc";
    public final static String URL_INCIDENTES_PRODUCCION = "http://10.54.21.38/wsGestionIncFranquicias/Service1.svc";

    //public final static String URL_INCIDENTES_REMEDY_DESARROLLO = "http://10.51.241.62/Arsys/WCFMttoQA/Service.svc";
    //public final static String URL_INCIDENTES_REMEDY_DESARROLLO = "http://10.54.28.194/WsMttoApp/Service.svc";
    public final static String URL_INCIDENTES_REMEDY_DESARROLLO = "http://10.54.68.156/Arsys/WCFMttoQA/Service.svc?wsdl"; //Nueva ubicacion remedy
    //public final static String URL_INCIDENTES_REMEDY_DESARROLLO = "http://10.54.21.38/Arsys/WCFMantenimiento/Service.svc";
    public final static String URL_INCIDENTES_REMEDY_PRODUCCION = "http://10.54.21.38/Arsys/WCFMantenimiento/Service.svc";

    public final static String URL_INCIDENTES_REMEDY_LIMPIEZA_DESARROLLO = "http://10.54.68.156/Arsys/QALimpieza/Service.svc";
    //public final static String URL_INCIDENTES_REMEDY_LIMPIEZA_DESARROLLO = "http://10.54.21.38/Arsys/WCFLimpieza/Service.svc";
    public final static String URL_INCIDENTES_REMEDY_LIMPIEZA_PRODUCCION = "http://10.54.21.38/Arsys/WCFLimpieza/Service.svc";

    //public final static String URL_INDICADORES_RH_DESARROLLO	= "http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getIndicadores";
    public final static String URL_INDICADORES_RH_PRODUCCION = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getIndicadores";

    //public final static String URL_DETALLEPLANTILLA_RH_DESARROLLO			= "http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getDetallePlantilla";
    public final static String URL_DETALLEPLANTILLA_RH_PRODUCCION = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetallePlantilla";
    //public final static String URL_DETALLEROTACION_RH_DESARROLLO			= "http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getDetalleRotacion";
    public final static String URL_DETALLEROTACION_RH_PRODUCCION = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleRotacion";
    //public final static String URL_DETALLECERTIFICACION_RH_DESARROLLO		= "http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getDetalleCertificacion";
    public final static String URL_DETALLECERTIFICACION_RH_PRODUCCION = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleCertificacion";
    //public final static String URL_DETALLECUBRIMIENTO_RH_DESARROLLO			= "http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getDetalleCubrimiento";
    public final static String URL_DETALLECUBRIMIENTO_RH_PRODUCCION = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleCubrimiento";
    //public final static String URL_DETALLEGARANTIA_RH_DESARROLLO			= "http://10.51.218.231:8085/InterfacesRH/springservices/Indicadores/getDetalleGarantia";
    public final static String URL_DETALLEGARANTIA_RH_PRODUCCION = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleGarantia";

    public final static String URL_INDICADORES_RH_DESARROLLO = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getIndicadores";

    public final static String URL_DETALLEPLANTILLA_RH_DESARROLLO = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetallePlantilla";
    public final static String URL_DETALLEROTACION_RH_DESARROLLO = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleRotacion";
    public final static String URL_DETALLECERTIFICACION_RH_DESARROLLO = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleCertificacion";
    public final static String URL_DETALLECUBRIMIENTO_RH_DESARROLLO = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleCubrimiento";
    public final static String URL_DETALLEGARANTIA_RH_DESARROLLO = "http://10.53.29.252:8082/InterfacesRH/springservices/Indicadores/getDetalleGarantia";

    public final static String STR_URI_EKT_DES = "http://10.50.109.47:8081/ServicioRest/services"; //Constantes para consumir servicios de ServicioRest
    public final static String STR_URI_EKT_PROD = "http://10.63.21.39:28080/ServicioRest/services"; //Constantes para consumir servicios de ServicioRest

    public final static String URL_EXTRACCION_DESARROLLO = "http://10.54.24.209:7082/WSInfoFinanciero/ExtraccionFinanciera.asmx";
    public final static String URL_EXTRACCION_PRODUCCION = "http://10.54.24.118:7082/WSInfoFinanciero/ExtraccionFinanciera.asmx";

    public final static String URL_REMEDY_DESARROLLO = "http://10.54.28.194/WsMttoApp/Service.svc";
    public final static String URL_REMEDY_PRODUCCION = "http://10.54.21.38/WsMttoApp/Service.svc";

    public final static String URL_EXTRACCION_REST_DESARROLLO = "http://10.54.24.81:7082/WebServiceFinanciero/ExtraccionFinanciera.svc/informacion";
    public final static String URL_EXTRACCION_REST_PRODUCCION = "http://10.54.24.118:7082/WebServiceFinanciero/ExtraccionFinanciera.svc/informacion";

    public final static String URL_SU_DESARROLLO = "http://10.50.109.39:8080/citasRest/";
    public final static String URL_SU_PRODUCCION = "http://sistemasbienestar.socio.gs:9991/servicios/";

    public final static String URL_SOCIO_UNICO_DESARROLLO = "http://10.50.109.39:8080/citasRest/";
    public final static String URL_SOCIO_UNICO_PRODUCCION = "http://sistemasbienestar.socio.gs:9990/citasRest/";

    /*Constantes para el envío de correo*/
    public static final String CTE_HOST_CORREO = "10.63.200.79";
    public static final String CTE_REMITENTE_SG = "sistemas-franquicia@bancoazteca.com.mx";

    public final static String MASTEREY_ENCRYPT = "4WMJz+AdWabMjaiV";
    public final static String MASTERKEY_URL = "http://10.50.169.89/wsAppLoginParam/Service.asmx?WSDL";

    //constantes de firma azteca
    public final static String XML_WS_FIRMA_AZTECA = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soap:Header/><soap:Body><tem:GetData><tem:no_empleado>@idEmpleado</tem:no_empleado><tem:token>@token</tem:token><tem:entidad>1</tem:entidad></tem:GetData></soap:Body></soap:Envelope>";
    public final static String URL_WS_FIRMA_AZTECA = "http://10.50.169.43/wsByPass/WsByPass.asmx?wsdl";

    //Constantes de servicios de tareas de ServicioRest
    public final static String MTD_RESP_PROY = "/initGerente/projectList?";
    public final static String MTD_RESP_DET = "/initGerente/projectDetails?";
    public final static String MTD_UPDATE_PASO = "/pasoService/updatePaso?";

    public final static String MTD_UPDATE_QUIZ = "/quizService/updateQuiz?";
    public final static String MTD_EVAL_QUIZ = "/quizEval/eval?";
    public final static String MTD_UPD_SURVEY = "/encuesta/uptSurvey?";
    public final static String MTD_STATUS = "/tareaService/statusUpd?";
    public final static String MTD_INSERTRESP_CHECKLIST_NUEVO = "/checkListNuevoService/insertaRespuestaUsuario?datos=";

    //Constantes para descargar quiz (tipo 3)
    public final static String MTD_QUIZ = "/quizService/fetchQuiz?";

    //Constantes para descargar quiz (tipo 4)
    public final static String STR_PASOSCHECKLIST = "/chklist/getCheckList?";

    //Constantes para descargar quiz (tipo 6)
    public final static String MTD_RESPUESTAS_CHECKLIST = "/checkListNuevoService/consultaPreguntaCheckList?";
    public final static String STR_CONSULTA_PREG_MATRIZ = "/checkListNuevoService/consultaPreguntaCheckListMatriz?op=1&idPaso=";

    //Constantes para descargar survey
    public final static String MTD_FETCH_SURVEY = "/encuesta/fetchSurvey?";

    //Validar parametros para realizar tareas
    public final static String MV_PARAMS_VALIDA = "/parametrosConfiguracion/admonParametros?op=2&idParametro=";

    public final static String URL_CONTEO_TAREAS_DESARROLLO = "http://10.50.109.47:8081/ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq";
    public final static String URL_CONTEO_TAREAS_PRODUCCION = "http://10.63.21.39:28080/ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq";

    public final static String URL_CONTEO_TARCOM_DESARROLLO = "http://10.50.109.47:8081/ServicioRest/services/initGerente/projectList?";
    public final static String URL_CONTEO_TARCOM_PRODUCCION = "http://10.63.21.39:28080/ServicioRest/services/initGerente/projectList?";

    public final static String URL_REFLEXIS_DESARROLLO = "http://10.50.109.47:8081/";
    public final static String URL_REFLEXIS_PRODUCCION = "http://10.63.21.39:28080/";

    public final static String URL_WS_VALIDAUSUARIO_DESARROLLO = "http://10.63.14.80/wslinuxvalidausuario.asmx";
    public final static String URL_WS_VALIDAUSUARIO_PRODUCCION = "http://10.63.14.80/wslinuxvalidausuario.asmx";

    public final static int NEGOCIO_DESARROLLO = 20;
    public final static int NEGOCIO_PRODUCCION = 41;

    public static enum rubrosNegocio {

        //CyC
        contribucion(1),
        arqueoCierre(2),
        arqueoApertura(3),
        arqueoSorpresaRegional(4),
        colocacionPrestamosPer(5),
        colocacionConsumo(6),
        colocacionTAZ(7),
        captacionSaldoVista(8),
        captacionSaldoPlazo(9),
        segurosPrestamos(10),
        segurosRenovaciones(11),
        segurosNoLigados(12),
        afore(13),
        solicitudesPendientesSurtir(14),
        carteraNormalidad(15),
        carteraNuncaAbonadas(16),
        negocioTransferenciaDEX(17),
        negocioTransferenciaInter(18),
        negocioPagoServicios(19),
        negocioCambios(20),
        arqueoSorpresaGerente(21),
        solicitudesA(22),
        solicitudesB(23),
        solicitudesC(24),
        //Captación
        captacionSaldoFinalVista(25),
        captacionNetaVista(26),
        captacionSaldoFinalPlazo(27),
        captacionNetaPlazo(28),
        transferenciasDEX(29),
        numAperturasClientesNuevosVista(30),
        montAperturasClientesNuevosVista(31),
        numAperturasClientesNuevosPlazo(32),
        montAperturasClientesNuevosPlazo(33);

        private int value;

        rubrosNegocio(int value) {
            this.value = value;
        }

        public static rubrosNegocio getType(int value) {

            switch (value) {

                case 1:
                    return contribucion;
                case 2:
                    return arqueoCierre;
                case 3:
                    return arqueoApertura;
                case 4:
                    return arqueoSorpresaRegional;
                case 5:
                    return colocacionPrestamosPer;
                case 6:
                    return colocacionConsumo;
                case 7:
                    return colocacionTAZ;
                case 8:
                    return captacionSaldoVista;
                case 9:
                    return captacionSaldoPlazo;
                case 10:
                    return segurosPrestamos;
                case 11:
                    return segurosRenovaciones;
                case 12:
                    return segurosNoLigados;
                case 13:
                    return afore;
                case 14:
                    return solicitudesPendientesSurtir;
                case 15:
                    return carteraNormalidad;
                case 16:
                    return carteraNuncaAbonadas;
                case 17:
                    return negocioTransferenciaDEX;
                case 18:
                    return negocioTransferenciaInter;
                case 19:
                    return negocioPagoServicios;
                case 20:
                    return negocioCambios;

                case 21:
                    return arqueoSorpresaGerente;

                case 22:
                    return solicitudesA;

                case 23:
                    return solicitudesB;

                case 24:
                    return solicitudesC;
                case 25:
                    return captacionSaldoFinalVista;
                case 26:
                    return captacionNetaVista;
                case 27:
                    return captacionSaldoFinalPlazo;
                case 28:
                    return captacionNetaPlazo;
                case 29:
                    return transferenciasDEX;
                case 30:
                    return numAperturasClientesNuevosVista;
                case 31:
                    return montAperturasClientesNuevosVista;
                case 32:
                    return numAperturasClientesNuevosPlazo;
                case 33:
                    return montAperturasClientesNuevosPlazo;

            }

            return contribucion;
        }

        public int getValue() {
            return value;
        }

    }

    public static String getURLServer() {
        String server = URL_SERVIDOR_LOCAL;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_SERVIDOR_PRODUCCION;
        } else {
            server = URL_SERVIDOR_DESARROLLO;
        }
        return server;
    }

    public static String getURLArqueo() {
        String server = URL_ARQUEO_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.DESARROLLO)) {
            server = URL_ARQUEO_DESARROLLO;
        }
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_ARQUEO_PRODUCCION;
        }
        return server;
    }

    public static String getURLApacheServer() {
        String server = URL_SERVIDOR_A_LOCAL;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.DESARROLLO)) {
            server = URL_SERVIDOR_A_DESARROLLO;
        }
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_SERVIDOR_A_PRODUCCION;
        }
        return server;
    }

    public static String getIpOrigen() {
        String ip = IP_ORIGEN_LOCAL;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.DESARROLLO)) {
            ip = IP_ORIGEN_DESARROLLO;
        }
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            ip = IP_ORIGEN_PRODUCCION;
        }
        return ip;
    }

    public static String getUrlWsToken() {
        String url = URL_WSTOKEN_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            url = URL_WSTOKEN_PRODUCCION;
        }
        return url;
    }

    public static String getUrlLoginLlave() {
        String url = URL_LOGIN_LLAVE_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            url = URL_LOGIN_LLAVE_PRODUCCION;
        }
        return url;
    }

    public static String getIdApp() {
        String url = ID_APP_PORTALES_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            url = ID_APP_PORTALES_PRODUCCION;
        }
        return url;
    }

    public static String getLLave() {
        String llave = LLAVE_ENCRIPCION_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            llave = LLAVE_ENCRIPCION_PRODUCCION;
        }
        return llave;
    }

    public static String getLlaveEncripcionLocal() {
        return LLAVE_ENCRIPCION_LOCAL;
    }

    public static String getLlaveEncripcionLocalIOS() {
        return LLAVE_ENCRIPCION_LOCAL_IOS;
    }

    public static String getIpHost() {
        String ip = IP_HOST_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            ip = IP_HOST_PRODUCCION;
        }
        return ip;
    }

    public static String getRutaImagen() {
        String server = RUTA_IMAGEN_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = RUTA_IMAGEN_PRODUCCION;
        }
        return server;
    }

    public static String getRutaImagenMovil() {
        String server = RUTA_IMAGEN_DESARROLLO_MOVIL;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = RUTA_IMAGEN_PRODUCCION_MOVIL;
        }
        return server;
    }

    public static String getURLFirma() {
        String server = URL_FIRMA_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_FIRMA_PRODUCCION;
        }
        return server;
    }

    public static String getURLProductividad() {
        String server = URL_PRODUCTIVIDAD_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_PRODUCTIVIDAD_PRODUCCION;
        }
        return server;
    }

    public static String getURLProdServicioRest() {
        String server = STR_URI_EKT_DES;

        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = STR_URI_EKT_PROD;
        }
        return server;
    }

    public static String getURLIncidentes() {
        String server = URL_INCIDENTES_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_INCIDENTES_PRODUCCION;
        }
        return server;
    }

    public static String getURLIncidentesRemedy() {
        String server = URL_INCIDENTES_REMEDY_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_INCIDENTES_REMEDY_PRODUCCION;
        }
        return server;
    }

    public static String getURLIncidentesRemedyLimpieza() {
        String server = URL_INCIDENTES_REMEDY_LIMPIEZA_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_INCIDENTES_REMEDY_LIMPIEZA_PRODUCCION;
        }
        return server;
    }

    public static String getURLExtraccionFinanciera() {
        String server = URL_EXTRACCION_DESARROLLO;

        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_EXTRACCION_PRODUCCION;
        }
        return server;
    }

    public static String getURLRemedy() {
        String server = URL_REMEDY_DESARROLLO;

        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_REMEDY_PRODUCCION;
        }
        return server;
    }

    //SERVICIO DE RH
    public static String getURLIndicadoresRH() {
        String server = URL_INDICADORES_RH_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_INDICADORES_RH_PRODUCCION;
        }
        return server;
    }

    public static String getURLdetallePlantillaRH() {
        String server = URL_DETALLEPLANTILLA_RH_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_DETALLEPLANTILLA_RH_PRODUCCION;
        }
        return server;
    }

    public static String getURLdetalleRotacionRH() {
        String server = URL_DETALLEROTACION_RH_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_DETALLEROTACION_RH_PRODUCCION;
        }
        return server;
    }

    public static String getURLdetalleCertificacionRH() {
        String server = URL_DETALLECERTIFICACION_RH_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_DETALLECERTIFICACION_RH_PRODUCCION;
        }
        return server;
    }

    public static String getURLdetalleCubrimientoRH() {
        String server = URL_DETALLECUBRIMIENTO_RH_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_DETALLECUBRIMIENTO_RH_PRODUCCION;
        }
        return server;
    }

    public static String getURLdetalleGarantiaRH() {
        String server = URL_DETALLEGARANTIA_RH_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_DETALLEGARANTIA_RH_PRODUCCION;
        }
        return server;
    }
    //SERVICIO DE RH

    //REST EXTRACCION FINANCIERA
    public static String getURLExtraccionFinancieraRest() {
        String server = URL_EXTRACCION_REST_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_EXTRACCION_REST_PRODUCCION;
        }
        return server;
    }

	//CONTEO DE REFLEXIS
    public static String getURLReflexis() {
        String server = URL_REFLEXIS_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_REFLEXIS_PRODUCCION;
        }
        return server;
    }

    // NUEVO TOKEN
    public static String getURLWSUnixValidaUsuario() {
        String server = URL_WS_VALIDAUSUARIO_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_WS_VALIDAUSUARIO_PRODUCCION;
        }
        return server;
    }

    public static String getURLSocioUnico() {
        String server = URL_SU_DESARROLLO;

        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_SU_PRODUCCION;
        }
        return server;
    }

    public static String getURLSocioUnico2() {
        String server = URL_SOCIO_UNICO_DESARROLLO;

        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            server = URL_SOCIO_UNICO_PRODUCCION;
        }
        return server;
    }

    // NUEVO TOKEN
    public static int getNegocio() {
        int negocio = NEGOCIO_DESARROLLO;
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            negocio = NEGOCIO_PRODUCCION;
        }
        return negocio;
    }

    public static String getIPLocal() {
        String localIP = null;
        try {
            Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netint : Collections.list(nets)) {
                if (!netint.isLoopback()) {
                    Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                    for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                        if (!inetAddress.getHostAddress().contains(":")) {
                            localIP = inetAddress.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
        }
        if (localIP == null) {
            localIP = "127.0.0.1";
        }
        return localIP;
    }

}
