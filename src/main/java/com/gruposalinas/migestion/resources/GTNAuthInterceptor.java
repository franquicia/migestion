package com.gruposalinas.migestion.resources;

import com.gruposalinas.migestion.business.SesionBI;
import com.gruposalinas.migestion.domain.RecursoPerfilDTO;
import com.gruposalinas.migestion.domain.SesionDTO;
import com.gruposalinas.migestion.domain.TokenDTO;
import com.gruposalinas.migestion.domain.UsuarioDTO;
import com.gruposalinas.migestion.util.StrCipher;
import com.gruposalinas.migestion.util.UtilCryptoGS;
import com.gruposalinas.migestion.util.UtilDate;
import com.gruposalinas.migestion.util.UtilGTN;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class GTNAuthInterceptor implements HandlerInterceptor {

    @Autowired
    SesionBI sesionBI;

    boolean banderaRedirect = false;
    boolean banderaSinLlave = false;

    private static final Logger logger = LogManager.getLogger(GTNAuthInterceptor.class);
    @SuppressWarnings("rawtypes")
    public Map map;

    /*
     * Caso 1.- NO existe el usuario en sesion, es la primera vez que se manda a
     * llamar la petición, Se solicita el logueo con llave maestra, se pide el
     * token y se redirecciona hacía el portal de llave el portal de llave
     * validara si existe o no usuario logueado, si existe el usuario entonces
     * redirecciona de regreso al sistema, si no hay sesión entonces lo manda al
     * logueo de llave
     *
     * Caso 2.- NO existe en sesion el usuario, pero es posible que vengan los
     * parametros encriptados como resultado del proceso de logueo por llave
     * maestra
     *
     */
    public void emptyMap() {
        this.map = null;
    }

    @SuppressWarnings({"rawtypes", "static-access"})
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        boolean flag = true;
        UsuarioDTO user = null;
        long startTime = System.currentTimeMillis();
        request.setAttribute("startTime", startTime);
        user = (UsuarioDTO) request.getSession().getAttribute("user");
        Map mapDesencriptado = null;
        map = request.getParameterMap(); // obtenemos los parametros del request
        boolean flagEmptyMap = map.isEmpty(); // Si no tiene parametros

        String uri = request.getRequestURI();
        String uriFinal = request.getQueryString() != null ? uri + "?" + request.getQueryString() : uri + "";

        if (uri.contains("/migestion/actuator")) {
            return flag;
        }
        if (uri.contains("/migestion/api-local")) {
            return flag;
        }

        if (uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl")
                || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl")
                || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl")
                || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idapl")
                || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario")
                || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario")
                || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idUsuario")
                || uriFinal.contains("central/principal.htm")
                || uriFinal.contains("catalogosService/altaEmpleadoExterno.json")
                || uriFinal.contains("central/terminos-y-condiciones.htm")
                || uriFinal.contains("central/aviso-de-privacidad.htm")
                || uriFinal.contains("cargaServices/convertImageExif.json")
                || uriFinal.contains("catalogosService/postGuardaArchivos.json")
                || (uriFinal.contains("/migestion/soporte/") && uriFinal.contains(".htm?idUsuario"))) {
            user = null;
        }
        if (uri.contains("/version")) {
            return true;
        }

        if (user == null) { // No existe el usuario en sessión

            if (uri.contains("/migestion/servicios/") || uri.contains("/migestion/serviciosMantenimiento/") || uri.contains("/migestion/notificacionesCiscoSparkService/") || uri.contains("/migestion/services/") || uri.contains("/migestion/controladores/")
                    || uri.contains("/migestion/errores/") || uri.contains("/migestion/infoResponse/") || uri.contains("/migestion/servicio/")
                    || uri.contains("/migestion/static/") || uri.contains("tienda/inicio.htm") || uri.contains("/migestion/notificacionesService/")
                    || uri.contains("migestion/mensajes.htm") || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl")
                    || (uriFinal.contains("/migestion/soporte/") && uriFinal.contains(".htm?idUsuario"))
                    || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl") || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl")
                    || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idapl")
                    || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                    || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario") || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idUsuario")
                    || uriFinal.contains("central/terminos-y-condiciones.htm") || uriFinal.contains("central/aviso-de-privacidad.htm") || uriFinal.contains("cargaServices/convertImageExif.json")
                    || uriFinal.contains("central/store.htm") || uriFinal.contains("central/storeIndex.htm") || uriFinal.contains("ejecutaServiciosEncriptados") || uriFinal.contains("central/principal.htm")
                    || uriFinal.contains("catalogosService/altaEmpleadoExterno.json") || uriFinal.contains("catalogosService/postGuardaArchivos.json")) {
                // Aqui van url conocidas que queremos que redireccione aunque no haya sesion
                //logger.info("VAMOS A LOGUEAR SIN LLAVE 0: ");

                String params = request.getQueryString() != null ? "?" + request.getQueryString() : "";
                if (uri.contains("tienda/inicio.htm") && params.contains("nombre")) {
                    flag = true;
                    String urlServer = GTNConstantes.getURLServer();
                    request.getSession().setAttribute("urlServer", urlServer);
                } else if (uri.contains("/migestion/servicios") || uri.contains("/migestion/serviciosMantenimiento/") || uri.contains("/migestion/notificacionesCiscoSparkService/") || uri.contains("/migestion/services/") || uri.contains("/migestion/controladores/")
                        || uri.contains("/migestion/errores/") || uri.contains("/migestion/infoResponse/") || uri.contains("/migestion/servicio/")
                        || uri.contains("/migestion/static/") || uri.contains("/migestion/mensajes.htm") || uri.contains("/migestion/notificacionesService/")
                        || (uriFinal.contains("/migestion/soporte/") && uriFinal.contains(".htm?idUsuario"))
                        || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl")
                        || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl") || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idapl")
                        || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                        || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario") || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idUsuario")
                        || uriFinal.contains("central/terminos-y-condiciones.htm") || uriFinal.contains("central/aviso-de-privacidad.htm") || uriFinal.contains("cargaServices/convertImageExif.json")
                        || uriFinal.contains("central/store.htm") || uriFinal.contains("central/storeIndex.htm") || uriFinal.contains("ejecutaServiciosEncriptados")
                        || uriFinal.contains("central/principal.htm") || uriFinal.contains("catalogosService/altaEmpleadoExterno.json") || uriFinal.contains("catalogosService/postGuardaArchivos.json")) {

                    if (uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl")
                            || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl") || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idapl")
                            || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                            || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario") || uriFinal.contains("central/vistaCumplimientoTareasMovil.htm?idUsuario")
                            || (uriFinal.contains("/migestion/soporte/") && uriFinal.contains(".htm?idUsuario"))
                            || uriFinal.contains("central/principal.htm") || uriFinal.contains("catalogosService/altaEmpleadoExterno.json")) {

                        // logger.info("request.getSession.. " + request.getSession());
                        HttpSession session = request.getSession(false);
                        // logger.info("session.. " + session.getAttribute("user"));
                        session.invalidate();
                        // logger.info("session.. " + session);

                        String uriMovilCompleta = request.getQueryString();
                        // logger.info("uriMovilCompleta.. " + uriMovilCompleta);
                        String idUser = "";
                        if (uriFinal.contains("?idUsuario")) {
                            idUser = request.getParameter("idUsuario");
                        } else {
                            UtilCryptoGS cifra = new UtilCryptoGS();
                            StrCipher cifraIOS = new StrCipher();
                            int idLlave = Integer.parseInt(request.getParameter("idapl"));
                            //  logger.info("idLlave.. " + idLlave);
                            String uriMovil = request.getQueryString();
                            uriMovil = uriMovil.split("&")[1];
                            // logger.info("uriMovil.. " + uriMovil);
                            String urides = "";
                            if (idLlave == 7) {
                                urides = (cifraIOS.decrypt(uriMovil, GTNConstantes.getLlaveEncripcionLocalIOS()));
                            } else if (idLlave == 666) {
                                urides = (cifra.decryptParams(uriMovil));
                            }
                            // logger.info("urides.. " + urides);
                            idUser = urides.split("=")[1];
                        }
                        //  logger.info("idUser.. " + idUser);
                        UsuarioDTO userSession = new UsuarioDTO();
                        userSession.setIdUsuario(idUser);

                        session = request.getSession();
                        session.setAttribute("user", userSession);

                        //  logger.info("session.. " + session);
                        //  logger.info("session.. " + session.getAttribute("user"));
                    }
                    banderaSinLlave = true;
                    flag = true;
                    String urlServer = GTNConstantes.getURLServer();
                    request.getSession().setAttribute("urlServer", urlServer);
                    if (uriFinal.contains("/migestion/soporte/") && uriFinal.contains(".htm?idUsuario")) {
                        response.sendRedirect("/migestion");
                    }
                } else if (uri.contains("migestion/mensajes.htm")) {
                    flag = true;
                    String urlServer = GTNConstantes.getURLServer();
                    request.getSession().setAttribute("urlServer", urlServer);
                } else {
                    //flag = false;
                    //response.sendRedirect("redirecciona.htm");

                    //logger.info("flag:"+flagEmptyMap);
                    /*VALIDA LA SESION*/
                    if (flagEmptyMap) { // Si esta vació
                        flag = getTokenAndRedirect(request, response);
                    } else if (!flagEmptyMap) { // No esta vacío es valido, debemos sacar el usuario y loguearlo en el sistema
                        UtilCryptoGS utilCrypto = new UtilCryptoGS();
                        //logger.info("map: "+map);
                        mapDesencriptado = utilCrypto.decryptMap(map); // Desencriptamos el mapa
                        //logger.info("mapDesencriptado: "+mapDesencriptado);
                        String idUser = "";
                        String nomUser = "";
                        try {
                            idUser = (String) mapDesencriptado.get("employeeid");
                            nomUser = (String) mapDesencriptado.get("name");
                            if (nomUser != null) {
                                nomUser = WordUtils.capitalize(nomUser.toLowerCase());
                            }
                            // logger.info("Valor idUser: " + idUser + " valor nomUser: " + nomUser);
                        } catch (Exception e) {
                            //logger.info("Ocurrio algo ");
                        }

                        if (idUser != null) {
                            setInfoInicial(idUser, nomUser, request);
                        } else {
                            flag = getTokenAndRedirect(request, response);
                        }
                    }
                    /*VALIDA LA SESION*/
                }
            } else if (uri.contains("central")) {
                /*VALIDA LA SESION*/
                if (flagEmptyMap) { // Si esta vació
                    flag = getTokenAndRedirect(request, response);
                } else if (!flagEmptyMap) { // No esta vacío es valido, debemos sacar el usuario y loguearlo en el sistema
                    UtilCryptoGS utilCrypto = new UtilCryptoGS();
                    //logger.info("map 1: "+map);
                    mapDesencriptado = utilCrypto.decryptMap(map); // Desencriptamos el mapa
                    //logger.info("mapDesencriptado 1: "+mapDesencriptado);
                    String idUser = "";
                    String nomUser = "";
                    try {
                        idUser = (String) mapDesencriptado.get("employeeid");
                        nomUser = (String) mapDesencriptado.get("name");
                        if (nomUser != null) {
                            nomUser = WordUtils.capitalize(nomUser.toLowerCase());
                        }
                        //  logger.info("Valor idUser 1: " + idUser + " valor nomUser 1: " + nomUser);
                    } catch (Exception e) {
                        // logger.info("Ocurrio algo ");
                    }
                    //nomUser = WordUtils.capitalize(nomUser.toLowerCase());
                    if (idUser != null) {
                        setInfoInicial(idUser, nomUser, request);
                    } else {
                        flag = getTokenAndRedirect(request, response);
                    }
                }
            } else {
                /*VALIDA LA SESION*/
                if (flagEmptyMap) { // Si esta vació
                    flag = getTokenAndRedirect(request, response);
                } else if (!flagEmptyMap) { // No esta vacío es valido, debemos sacar el usuario y loguearlo en el sistema
                    UtilCryptoGS utilCrypto = new UtilCryptoGS();
                    //logger.info("map 2: "+map);
                    mapDesencriptado = utilCrypto.decryptMap(map); // Desencriptamos el mapa
                    //logger.info("mapDesencriptado 2: "+mapDesencriptado);
                    String idUser = "";
                    String nomUser = "";
                    try {
                        idUser = (String) mapDesencriptado.get("employeeid");
                        nomUser = (String) mapDesencriptado.get("name");
                        //logger.info("Valor idUser 2: " + idUser + " valor nomUser 2: "+ nomUser);
                    } catch (Exception e) {
                        // logger.info("Ocurrio algo ");
                    }
                    if (idUser != null) {
                        setInfoInicial(idUser, nomUser, request);
                    } else {
                        flag = getTokenAndRedirect(request, response);
                    }
                }
                /*VALIDA LA SESION*/
            }
        }
        return flag;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        // Se ejecuta siempre y cuando el preHandle devuelve true
        long startTime = (Long) request.getAttribute("startTime");
        long endTime = System.currentTimeMillis();
        long executeTime = endTime - startTime;
        String path = request.getRequestURI();
        String clean = request.getRequestURI().replace('\n', '_').replace('\r', '_');
        UsuarioDTO userSession = null;
        String usuario = "notLogged";
        String alertMsg = "";

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        String cleanedIpAddress = ipAddress.replace('\n', '_').replace('\r', '_');

        if (request != null) {
            //System.out.println("print1: "+request.getSession().getId());
            //System.out.println("print2: "+request.getRemoteAddr());
            //System.out.println("print3: "+request.getHeader("USER-AGENT"));
            String IP = request.getRemoteAddr();
            //System.out.println("Bandera sin llave: "+banderaSinLlave);

            HttpSession session = request.getSession();
            // logger.info("session de posHandle: " + session);
            if (session != null && session.getAttribute("user") != null) {
                //logger.info("session de posHandle: ENTRE");
                userSession = (UsuarioDTO) request.getSession().getAttribute("user");
                usuario = userSession.getIdUsuario();
                // logger.info("session de posHandle: ENTRE VALOR: " + usuario);

                try {
                    Date date = new Date();
                    //Caso 1: obtener la hora y salida por pantalla con formato:
                    DateFormat hourFormat = new SimpleDateFormat("HH:mm");
                    //System.out.println("Hora: "+hourFormat.format(date));
                    //Caso 2: obtener la fecha y salida por pantalla con formato:
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    //System.out.println("Fecha: "+dateFormat.format(date));
                    //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
                    DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    //System.out.println("Hora y fecha: "+hourdateFormat.format(date));

                    //--------------------Si la peticion se hace en el mismo navegador, la sesion sigue viva.---------------------
                    List<SesionDTO> listSesion = sesionBI.buscaSesion(request.getHeader("USER-AGENT"), usuario, request.getSession().getId(), null, null, "1");
                    if (banderaSinLlave == false) {
                        if (listSesion.size() != 0) {
                            SesionDTO ActSes = listSesion.get(0);
                            String fech = ActSes.getFechaLogueo().trim().split(" ")[0].split("-")[2] + "/" + ActSes.getFechaLogueo().trim().split(" ")[0].split("-")[1] + "/" + ActSes.getFechaLogueo().trim().split(" ")[0].split("-")[0] + " " + ActSes.getFechaLogueo().trim().split(" ")[1].split(":")[0] + ":" + ActSes.getFechaLogueo().trim().split(" ")[1].split(":")[1];
                            ActSes.setFechaLogueo(fech);
                            ActSes.setFechaActualiza(hourdateFormat.format(date).trim());
                            //logger.info("La Sesion: "+ActSes.toString());
                            if (sesionBI.actualizaSesion(ActSes)) {
                                // logger.info("La Sesion Actualizo correctamente");
                            } else {
                                //logger.info("Ocurrio algo al actualizar la sesion");
                            }
                        } else {

                            boolean flag_NAV = false, flag_EXP = false;
                            //-------------------------en caso de que la peticion proenga de distinto navegador--------------------------
                            List<SesionDTO> listSesion2 = sesionBI.buscaSesion("", usuario, "", "", "", "1");
                            if (listSesion2.size() != 0) {
                                for (SesionDTO dat : listSesion2) {
                                    if (!request.getHeader("USER-AGENT").trim().equals(dat.getNavegador().trim()) && !request.getSession().getId().trim().equals(dat.getIp().trim())) {
                                        flag_NAV = true;
                                        eraseCookie(request, response);
                                        if (banderaRedirect == false) {
                                            getTokenAndRedirect(request, response);
                                            banderaRedirect = true;
                                        } else {
                                            SesionDTO ActSes = new SesionDTO();
                                            ActSes.setNavegador(request.getHeader("USER-AGENT").trim());
                                            ActSes.setIdUsuario(Integer.parseInt(usuario));
                                            ActSes.setIp(request.getSession().getId().trim());
                                            ActSes.setFechaLogueo(hourdateFormat.format(date).trim());
                                            ActSes.setFechaActualiza(hourdateFormat.format(date).trim());
                                            ActSes.setBandera(1);
                                            ActSes.setSesionNav(IP);
                                            //   logger.info("Inserto la sesion: " + sesionBI.insertaSesion(ActSes));

                                        }

                                        break;
                                    }
                                    if (!request.getSession().getId().trim().equals(dat.getIp().trim()) && flag_NAV == false) {
                                        flag_EXP = true;
                                        SesionDTO ActSes = listSesion2.get(0);
                                        ActSes.setFechaActualiza(hourdateFormat.format(date).trim());
                                        String fech = ActSes.getFechaLogueo().trim().split(" ")[0].split("-")[2] + "/" + ActSes.getFechaLogueo().trim().split(" ")[0].split("-")[1] + "/" + ActSes.getFechaLogueo().trim().split(" ")[0].split("-")[0] + " " + ActSes.getFechaLogueo().trim().split(" ")[1].split(":")[0] + ":" + ActSes.getFechaLogueo().trim().split(" ")[1].split(":")[1];
                                        ActSes.setFechaLogueo(fech);
                                        ActSes.setBandera(0);

                                        //logger.info("La Sesion: "+ActSes.toString());
                                        if (sesionBI.actualizaSesion(ActSes)) {
                                            //  logger.info("La Sesion Actualizo correctamente");
                                        } else {
                                            //  logger.info("Ocurrio algo al actualizar la sesion");
                                        }
                                        //Cierra la sesion
                                        request.removeAttribute("user");
                                        request.removeAttribute("nombre");
                                        request.removeAttribute("nomSucursal");
                                        HttpSession sessionx = request.getSession(false);
                                        if (request.isRequestedSessionIdValid() && sessionx != null) {
                                            sessionx.invalidate();
                                        }

                                        eraseCookie(request, response);

                                        response.sendRedirect("/migestion/central/cierraSesion.htm");

                                        break;
                                    }
                                }

                            } else {
                                SesionDTO ActSes = new SesionDTO();
                                ActSes.setNavegador(request.getHeader("USER-AGENT").trim());
                                ActSes.setIdUsuario(Integer.parseInt(usuario));
                                ActSes.setIp(request.getSession().getId().trim());
                                ActSes.setFechaLogueo(hourdateFormat.format(date).trim());
                                ActSes.setFechaActualiza(hourdateFormat.format(date).trim());
                                ActSes.setBandera(1);
                                ActSes.setSesionNav(IP);
                                //  logger.info("Inserto la sesion: " + sesionBI.insertaSesion(ActSes));

                            }
                        }
                    }
                } catch (Exception e) {
                    // logger.info("Ocurrio algo: " + e);
                }

                String cleanedUser = usuario.replace('\n', '_').replace('\r', '_');

                if (!usuario.equals(cleanedUser) || !ipAddress.equals(cleanedIpAddress) || !path.equals(clean)) {
                    alertMsg = " (PRECAUCIÓN AL LEER EL LOG)";
                }

                logger.info("ipClient: " + cleanedIpAddress + " - User: " + cleanedUser + " - migestion - path: " + clean + " - Tiempo de ejecucion: " + executeTime + " ms" + " - " + alertMsg);
            } else {
                logger.info("ipClient: " + cleanedIpAddress + " - migestion - path: " + clean + " - Tiempo de ejecucion: " + executeTime + " ms" + " - " + alertMsg);
            }

        }

    }

    private void eraseCookie(HttpServletRequest req, HttpServletResponse resp) {
        javax.servlet.http.Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (javax.servlet.http.Cookie cookie : cookies) {
                cookie.setValue("");
                cookie.setPath("/");
                cookie.setMaxAge(0);
                cookie.setValue(null);
                cookie.setDomain(req.getHeader("host"));
                resp.addCookie(cookie);
            }
        }
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // TODO Auto-generated method stub
    }

    @SuppressWarnings("unused")
    public boolean getTokenAndRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException, KeyException, GeneralSecurityException {
        // logger.info("1.- **********************  INICIA getTokenAndRedirect  **********************");
        String params = request.getQueryString() != null ? "?" + request.getQueryString() : "";
        UtilCryptoGS utilCrypto = new UtilCryptoGS();
        TokenDTO token = new TokenDTO();
        token.setBandera(false);
        token.setIpOrigen(GTNConstantes.getIpOrigen());
        token.setIdAplicacion(GTNConstantes.getIdApp());
        //token.setUrlRedirect(FRQConstantes.getURLServer() + request.getRequestURI() + params);
        token.setUrlRedirect(GTNConstantes.getURLServer() + request.getRequestURI());
        token.setFechaHora(UtilDate.getSysDate("ddMMyyyy hh:mm:ss"));
        token.setCadenaOriginal(utilCrypto.generaToken(token));

        try {
            //logger.info("1.- ENTRO AL TRY DE GENERATOKEN");
            /**
             * Se sustituye la versión generada por axis*
             */
            /**
             * * Mandamos a llamar el web-service para generar el token **
             */
            //logger.info("Cadena original... " + token.getCadenaOriginal());
            String ret = UtilGTN.conectMasterKey(token.getCadenaOriginal());
            //logger.info("IMPRIME EL RET ------ " + ret);
            if (!ret.equals("error")) {
                token.setToken(ret);
                //logger.info("1.- ENTRO EN EL IF DE ER EN EL TRY");
            }

        } catch (Exception ex) {
            //logger.info("Algo sucedio"+ex.getMessage());
        }
        /**
         * * Termina llamado al web-service para generar el token **
         */
        // logger.info("1.- TERMINO DE INVOCACION SERVICIO PARA GENERAR TOKEN");

        String path = request.getRequestURI();
        String clean = request.getRequestURI().replace('\n', '_').replace('\r', '_');
        UsuarioDTO userSession = null;
        String usuario = "notLogged";
        String alertMsg = "";

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        //logger.info("1.- path:	" + path);
        //logger.info("1.- clean:	" + clean);
        //logger.info("1.- ipAddress:	" + ipAddress);
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
            //logger.info("1.- ipAddress == null:	" + request.getRemoteAddr());
        }

        if (request != null) {
            //logger.info("1.- request != null");
            HttpSession session = request.getSession();
            //logger.info("1.- session : " + session);
            //logger.info("1.- session.getAttribute('user') : " + session.getAttribute("user"));
            if (session != null && session.getAttribute("user") != null) {
                //logger.info("session != null && session.getAttribute(user) != null");
                userSession = (UsuarioDTO) request.getSession().getAttribute("user");
                usuario = userSession.getIdUsuario();
                //  logger.info("ASIGNACION DE USUARIO: " + usuario);
            }
        }
        String cleanedUser = usuario.replace('\n', '_').replace('\r', '_');
        String cleanedIpAddress = ipAddress.replace('\n', '_').replace('\r', '_');

        if (!usuario.equals(cleanedUser) || !ipAddress.equals(cleanedIpAddress) || !path.equals(clean)) {
            alertMsg = " (PRECAUCIÓN AL LEER EL LOG)";
        }

        logger.info("1.- IP CLIENT: " + cleanedIpAddress + " - USER: " + cleanedUser + " - migestion - PATH: " + clean + " - " + alertMsg);
        logger.info("1.- DATOS DE REDIRECCION. response.sendRedirect: " + GTNConstantes.getUrlLoginLlave() + token.getToken() + "&id=" + GTNConstantes.getIdApp());

        response.sendRedirect(GTNConstantes.getUrlLoginLlave() + token.getToken() + "&id=" + GTNConstantes.getIdApp()); // Redireccionamos

        return false;

    }

    @SuppressWarnings("unused")
    public void setInfoInicial(String idUser, String nomUser, HttpServletRequest request) {
        boolean isAdmin = true;
        /* Informacion del usuario */
 /*if (idUser.equals("99999999")) {
         // idUser = "232531";
         //idUser = "176361";
         }*/
        UsuarioDTO userSession = new UsuarioDTO();
        //userSession.setIdUsuario(""+189870);
        userSession.setIdUsuario(idUser);
        userSession.setAdmin(isAdmin);
        userSession.setNombre(nomUser);

        RecursoPerfilDTO perfilAdmin = null;

        /*
         Class<?> c5;
         try {
         Object object5 = GTNAppContextProvider.getApplicationContext().getBean("recursoPerfilBI");
         c5 = object5.getClass();
         Method method5 = c5.getDeclaredMethod("obtienePermisos", new Class[]{int.class, int.class});
         perfilAdmin =  (RecursoPerfilDTO) method5.invoke(object5,1,Integer.parseInt(idUser));
         } catch (Exception e) {
         logger.info("Algo paso: " +e);
         }


         RecursoPerfilDTO perfilReportes= null;
         /*LLAMADO DE PERFILADO REPORTERIA
         try {
         Object object5 = FRQAppContextProvider.getApplicationContext().getBean("recursoPerfilBI");
         c5 = object5.getClass();
         Method method5 = c5.getDeclaredMethod("obtienePermisos", new Class[]{int.class, int.class});
         perfilReportes =  (RecursoPerfilDTO) method5.invoke(object5,4,Integer.parseInt(idUser));
         } catch (Exception e) {
         logger.info("Algo paso: " +e);
         }

         */
        if (perfilAdmin != null) {
            request.getSession().setAttribute("perfilAdmin", 1);
        } else {
            request.getSession().setAttribute("perfilAdmin", 0);
        }

        if (perfilAdmin != null) {
            request.getSession().setAttribute("perfilReportes", 1);
        } else {
            request.getSession().setAttribute("perfilReportes", 0);
        }

        request.getSession().setAttribute("user", userSession);
        request.getSession().setAttribute("nombre", nomUser);
        request.getSession().setAttribute("nomsucursal", "Central");

        /*
         * En las siguientes líneas se debe poner información necesaria que
         * siempre se debe cargar cuando el usuario se loguea
         */
        String servidorApache = GTNConstantes.getURLApacheServer();
        request.getSession().setAttribute("servidorApache", servidorApache);
    }
}
