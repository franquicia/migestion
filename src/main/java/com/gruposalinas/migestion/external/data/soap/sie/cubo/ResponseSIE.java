/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author cescobarh
 */
public class ResponseSIE {

    private final Logger logger = LogManager.getLogger();

    private String cuboXML;
    private final JacksonXmlModule xmlModule = new JacksonXmlModule();
    private final XmlMapper xmlMapper;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private Root root;

    public ResponseSIE(String cuboXML) {
        this.cuboXML = cuboXML;
        xmlModule.setDefaultUseWrapper(false);
        xmlMapper = new XmlMapper(xmlModule);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    private Root getRoot() {
        try {
            return xmlMapper.readValue(cuboXML, Root.class);
        } catch (IOException ex) {
            logger.warn("Error", ex);
        }
        return null;
    }

    private ArrayNode getConsulta() {
        try {
            InputStream is = new ByteArrayInputStream(cuboXML.getBytes(StandardCharsets.UTF_8));
            RootSniffingXMLStreamReader sr = new RootSniffingXMLStreamReader(XMLInputFactory.newInstance().createXMLStreamReader(is));
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.registerModule(new SimpleModule().addDeserializer(Object.class, new ArrayInferringUntypedObjectDeserializer()));
            ArrayNode value = xmlMapper.readValue(sr, ArrayNode.class);
            return value;
        } catch (IOException ex) {
            logger.warn("Error", ex);
        } catch (XMLStreamException ex) {
            logger.warn("Error", ex);
        }
        return null;
    }

    public TableView getTable() {
        this.creanXML();
        if (cuboXML.startsWith("<root")) {
            return this.proccessRoot();
        } else if (cuboXML.startsWith("<Consulta>")) {
            return this.proccessConsulta();
        }
        return null;
    }

    private void creanXML() {
        String startTag = "<root";
        String endTag = "</root>";
        if (cuboXML != null && cuboXML.indexOf(startTag) > 0 && cuboXML.indexOf(endTag) > 0) {
            cuboXML = cuboXML.substring(cuboXML.indexOf(startTag), (cuboXML.indexOf(endTag) + endTag.length()));
        }
        startTag = "<Consulta>";
        endTag = "</Consulta>";
        if (cuboXML != null && cuboXML.indexOf(startTag) > 0 && cuboXML.indexOf(endTag) > 0) {
            cuboXML = cuboXML.substring(cuboXML.indexOf(startTag), (cuboXML.indexOf(endTag) + endTag.length()));
        }
    }

    private TableView proccessConsulta() {
        TableView tv = new TableView();
        try {
            ArrayNode consulta = this.getConsulta();
            if (consulta != null) {
                ObjectNode itemContainer = objectMapper.createObjectNode();
                itemContainer.set("Rows", consulta);
                tv.setTableView(itemContainer);
            } else {
                tv.setTableView(objectMapper.createObjectNode());
            }
            String data = objectMapper.writeValueAsString(tv);
            System.out.println(data);
        } catch (JsonProcessingException ex) {
            logger.warn("Error", ex);
        }
        return tv;
    }

    private TableView proccessRoot() {
        TableView tv = new TableView();
        this.root = this.getRoot();
        if (this.root != null) {
            ObjectNode table = objectMapper.createObjectNode();
            ObjectNode row;
            ArrayNode columnRows = objectMapper.createArrayNode();
            ArrayNode columns = objectMapper.createArrayNode();
            ObjectNode cellData;
            ArrayNode cellsData = objectMapper.createArrayNode();

            List<Axis> axes = this.root.getAxes().getAxis();
            Axis axis0 = null;
            Axis axis1 = null;
            for (Axis axe : axes) {
                if (axe.getName().contains("Axis0")) {
                    axis0 = axe;
                }
                if (axe.getName().contains("Axis1")) {
                    axis1 = axe;
                }
            }

            if (axis0 != null) {
                List<Tuple> tuples = axis0.getTuples().getTuple();
                for (Tuple tuple : tuples) {
                    columns.add(tuple.getMember().getCaption());
                }
            }

            if (axis1 != null) {
                List<Tuple> tuples = axis1.getTuples().getTuple();
                for (Tuple tuple : tuples) {
                    columnRows.add(tuple.getMember().getCaption());
                }
            }

            List<Cell> cells = this.root.getCellData().getCell();
            for (Cell cell : cells) {
                cellData = objectMapper.createObjectNode();
                cellData.put("CellOrdinal", cell.getCellOrdinal());
                cellData.put("CellValue", cell.getFmtValue());
                cellsData.add(cellData);
            }
            int cellNum = 0;
            String value;
            for (int columnRow = 0; columnRow < columnRows.size(); columnRow++) {
                row = objectMapper.createObjectNode();
                for (int column = 0; column < columns.size(); column++) {
                    value = null;
                    for (int cell = 0; cell < cellsData.size(); cell++) {
                        if (cellNum == cellsData.get(cell).get("CellOrdinal").asInt()) {
                            value = cellsData.get(cell).get("CellValue").asText();
                        }
                    }
                    if (row.get(columns.get(column).asText()) == null) {
                        row.put(columns.get(column).asText(), value);
                    } else {
                        row.put((columns.get(column).asText() + "-" + (column + 1)), value);
                    }

                    cellNum++;
                }
                table.set(columnRows.get(columnRow).asText(), row);;
            }
            tv.setTableView(table);
        } else {
            tv.setTableView(objectMapper.createObjectNode());
        }
//        try {
//            String data = objectMapper.writeValueAsString(tv);
//            System.out.println(data);
//        } catch (Exception ex) {
//
//        }
        return tv;
    }

}
