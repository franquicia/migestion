/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap;

/**
 *
 * @author cescobarh
 */
public class FuncionesDTO {

    private String sieURL;

    public String getSieURL() {
        return sieURL;
    }

    public void setSieURL(String sieURL) {
        this.sieURL = sieURL;
    }

    @Override
    public String toString() {
        return "FuncionesDTO{" + "sieURL=" + sieURL + '}';
    }

}
