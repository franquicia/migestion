/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class Cube {

    @JacksonXmlProperty(localName = "CubeName")
    private String CubeName;

    @JacksonXmlProperty(localName = "LastSchemaUpdate")
    private String LastSchemaUpdate;

    @JacksonXmlProperty(localName = "LastDataUpdate")
    private String LastDataUpdate;

    public String getLastSchemaUpdate() {
        return LastSchemaUpdate;
    }

    public void setLastSchemaUpdate(String LastSchemaUpdate) {
        this.LastSchemaUpdate = LastSchemaUpdate;
    }

    public String getCubeName() {
        return CubeName;
    }

    public void setCubeName(String CubeName) {
        this.CubeName = CubeName;
    }

    public String getLastDataUpdate() {
        return LastDataUpdate;
    }

    public void setLastDataUpdate(String LastDataUpdate) {
        this.LastDataUpdate = LastDataUpdate;
    }

    @Override
    public String toString() {
        return "Cube{" + "CubeName=" + CubeName + ", LastSchemaUpdate=" + LastSchemaUpdate + ", LastDataUpdate=" + LastDataUpdate + '}';
    }

}
