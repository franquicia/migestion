/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class Cell {

    @JacksonXmlProperty(localName = "FmtValue")
    private String FmtValue;

    @JacksonXmlProperty(localName = "CellOrdinal", isAttribute = true)
    private String CellOrdinal;

    @JacksonXmlProperty(localName = "Value")
    private Value Value;

    public Cell() {
    }

    public String getFmtValue() {
        return FmtValue;
    }

    public void setFmtValue(String FmtValue) {
        this.FmtValue = FmtValue;
    }

    public String getCellOrdinal() {
        return CellOrdinal;
    }

    public void setCellOrdinal(String CellOrdinal) {
        this.CellOrdinal = CellOrdinal;
    }

    public Value getValue() {
        return Value;
    }

    public void setValue(Value Value) {
        this.Value = Value;
    }

    @Override
    public String toString() {
        return "Cell{" + "FmtValue=" + FmtValue + ", CellOrdinal=" + CellOrdinal + ", Value=" + Value + '}';
    }

}
