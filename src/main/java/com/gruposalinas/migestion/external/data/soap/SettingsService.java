/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;

/**
 *
 * @author cescobarh
 */
public class SettingsService {

    private BindingProvider provider = null;
    private String url = null;
    private String user;
    private String password;
    private boolean motorize = false;

    public SettingsService(BindingProvider provider, String url) {
        this.provider = provider;
        this.url = url;
    }

    public SettingsService(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public SettingsService(BindingProvider provider, String url, boolean motorize) {
        this.provider = provider;
        this.url = url;
        this.motorize = motorize;
    }

    public SettingsService(BindingProvider provider, String url, String user, String password, boolean motorize) {
        this.provider = provider;
        this.url = url;
        this.user = user;
        this.password = password;
        this.motorize = motorize;
    }

    public SettingsService(BindingProvider provider, String url, String user, String password) {
        this.provider = provider;
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void authenticate() {
        if (this.user != null && this.password != null) {
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, password.toCharArray());
                }
            });
        }
    }

    public void addHandlers() {
        //Obtener el enlace
        Binding enlace = this.provider.getBinding();
        //Asignar la URL del ambiente y las credenciales de acceso
        this.provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, this.url);
        if (this.user != null) {
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, this.user);
        }
        if (this.password != null) {
            this.provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, this.password);
        }
        if (this.motorize) { //Si se solicito monitorear la ejecucion...
            //Crear una instancia para los manejadores
            List manejadores = enlace.getHandlerChain();
            //Verificar que la lista de manejadores no sea nula
            if (manejadores == null) { //Si la lista de manejadores es nula...
                //Crear una lista vacia para los manejadores
                manejadores = new ArrayList();
            }
            //Agregar el monitor a la lista de manejadores
            manejadores.add(new HandlerChainSOAP());
            //Asignar la lista nueva de manejadores al enlace
            enlace.setHandlerChain(manejadores);
        }
    }

    public BindingProvider getProvider() {
        return provider;
    }

    public void setProvider(BindingProvider provider) {
        this.provider = provider;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isMotorize() {
        return motorize;
    }

    public void setMotorize(boolean motorize) {
        this.motorize = motorize;
    }
}
