/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.client;

import com.gruposalinas.migestion.external.data.soap.FuncionesDTO;
import com.gruposalinas.migestion.external.data.soap.SettingsService;
import com.gruposalinas.migestion.external.data.soap.sie.ExtraccionFinanciera;
import com.gruposalinas.migestion.external.data.soap.sie.ExtraccionFinancieraSoap;
import com.gruposalinas.migestion.external.data.soap.sie.cubo.ResponseSIE;
import com.gruposalinas.migestion.external.data.soap.sie.cubo.TableView;
import com.gruposalinas.migestion.resources.GTNConstantes;
import com.gruposalinas.migestion.util.Ambientes;
import javax.xml.ws.BindingProvider;

/**
 *
 * @author cescobarh
 */
public class ProcessSIE {

    private final static FuncionesDTO funcionesCalidad = new FuncionesDTO();
    private final static FuncionesDTO funcionesProduccion = new FuncionesDTO();
    private final FuncionesDTO funciones;
    private final ExtraccionFinanciera sie_service;
    private final ExtraccionFinancieraSoap sie_port;

    public ProcessSIE() {
        funcionesCalidad.setSieURL(GTNConstantes.URL_EXTRACCION_DESARROLLO);
        funcionesProduccion.setSieURL(GTNConstantes.URL_EXTRACCION_PRODUCCION);
        if (Ambientes.AMBIENTE.value().equals(Ambientes.PRODUCTIVO)) {
            funciones = funcionesProduccion;
        } else {
            funciones = funcionesCalidad;
        }
        sie_service = new ExtraccionFinanciera();
        sie_port = sie_service.getExtraccionFinancieraSoap();
        SettingsService settingsService = new SettingsService((BindingProvider) sie_port, funciones.getSieURL(), false);
        settingsService.addHandlers();
    }

    public TableView activacionesBazDigital(String fecha, String geografia) {
        String cuboString = sie_port.activacionesBazDigital(fecha, geografia);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView colocacionAcumulada(String geografia, String nivel) {
        String cuboString = sie_port.colocacionAcumulada(geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView compromisoColocacion(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.compromisoColocacion(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView consumoColocacion(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.consumoColocacion(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView contribucion(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.contribucion(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView graficaColocacionPersonales(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.graficaColocacionPersonales(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView graficaColocacionTAZ(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.graficaColocacionTAZ(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView graficaColocacionTotalConsumo(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.graficaColocacionTotalConsumo(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView graficaColocacionTotalConsumo(String geografia, String producto) {
        String cuboString = sie_port.graficaTiempoRealColocacion(geografia, producto);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView graficaTiempoRealColocacion(String geografia, String producto) {
        String cuboString = sie_port.graficaTiempoRealColocacion(geografia, producto);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView nAbonadasPendSurt(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.nAbonadasPendSurt(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView normalidad(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.normalidad(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView personalesColocacion(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.personalesColocacion(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView personalesColocacionBAZDigital(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.personalesColocacionBAZDigital(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView saldoCaptacionPlazo(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.saldoCaptacionPlazo(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView saldoCaptacionPlazoMetrica(String fecha, String geografia, String nivel, String metrica) {
        String cuboString = sie_port.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, metrica);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView saldoCaptacionVista(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.saldoCaptacionVista(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView saldoCaptacionVistaMetrica(String fecha, String geografia, String nivel, String metrica) {
        String cuboString = sie_port.saldoCaptacionVistaMetrica(fecha, geografia, nivel, metrica);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView tazColocacion(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.tazColocacion(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView tiempoRealColocacion(String geografia, String nivel) {
        String cuboString = sie_port.tiempoRealColocacion(geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView tiempoRealColocacionConsumo(String geografia, String nivel) {
        String cuboString = sie_port.tiempoRealColocacionConsumo(geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView tiempoRealColocacionCredito(String geografia, String nivel) {
        String cuboString = sie_port.tiempoRealColocacionCredito(geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView tiempoRealColocacionPersonales(String geografia, String nivel) {
        String cuboString = sie_port.tiempoRealColocacionPersonales(geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView trColocacionConsumoPorHora(String geografia) {
        String cuboString = sie_port.trColocacionConsumoPorHora(geografia);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView trColocacionCreditoPorHora(String geografia) {
        String cuboString = sie_port.trColocacionCreditoPorHora(geografia);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView trColocacionPersonalesPorHora(String geografia) {
        String cuboString = sie_port.trColocacionPersonalesPorHora(geografia);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView tranferenciasDEX(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.tranferenciasDEX(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView transferenciasCambios(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.transferenciasCambios(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView transferenciasInternacionales(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.transferenciasInternacionales(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }

    public TableView transferenciasPagoServicios(String fecha, String geografia, String nivel) {
        String cuboString = sie_port.transferenciasPagoServicios(fecha, geografia, nivel);
        return new ResponseSIE(cuboString).getTable();
    }
}
