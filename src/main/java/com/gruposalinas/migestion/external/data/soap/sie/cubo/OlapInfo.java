/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class OlapInfo {

    @JacksonXmlProperty(localName = "CubeInfo")
    private CubeInfo CubeInfo;

    @JacksonXmlProperty(localName = "AxesInfo")
    private AxesInfo AxesInfo;

    @JacksonXmlProperty(localName = "CellInfo")
    private CellInfo CellInfo;

    public CellInfo getCellInfo() {
        return CellInfo;
    }

    public void setCellInfo(CellInfo CellInfo) {
        this.CellInfo = CellInfo;
    }

    public CubeInfo getCubeInfo() {
        return CubeInfo;
    }

    public void setCubeInfo(CubeInfo CubeInfo) {
        this.CubeInfo = CubeInfo;
    }

    public AxesInfo getAxesInfo() {
        return AxesInfo;
    }

    public void setAxesInfo(AxesInfo AxesInfo) {
        this.AxesInfo = AxesInfo;
    }

    @Override
    public String toString() {
        return "OlapInfo{" + "CubeInfo=" + CubeInfo + ", AxesInfo=" + AxesInfo + ", CellInfo=" + CellInfo + '}';
    }

}
