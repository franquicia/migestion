/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class Axis {

    @JacksonXmlProperty(localName = "Tuples")
    private Tuples Tuples;

    @JacksonXmlProperty(localName = "name")
    private String name;

    public Tuples getTuples() {
        return Tuples;
    }

    public void setTuples(Tuples Tuples) {
        this.Tuples = Tuples;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Axis{" + "Tuples=" + Tuples + ", name=" + name + '}';
    }

}
