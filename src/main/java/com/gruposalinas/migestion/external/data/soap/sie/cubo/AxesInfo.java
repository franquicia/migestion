/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class AxesInfo {

    @JacksonXmlProperty(localName = "AxisInfo")
    private List<AxisInfo> AxisInfo;

    public List<AxisInfo> getAxisInfo() {
        return AxisInfo;
    }

    public void setAxisInfo(List<AxisInfo> AxisInfo) {
        this.AxisInfo = AxisInfo;
    }

    @Override
    public String toString() {
        return "AxesInfo{" + "AxisInfo=" + AxisInfo + '}';
    }

}
