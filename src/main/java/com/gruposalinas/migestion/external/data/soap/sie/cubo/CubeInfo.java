/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class CubeInfo {

    @JacksonXmlProperty(localName = "Cube")
    private Cube Cube;

    public Cube getCube() {
        return Cube;
    }

    public void setCube(Cube Cube) {
        this.Cube = Cube;
    }

    @Override
    public String toString() {
        return "CubeInfo{" + "Cube=" + Cube + '}';
    }

}
