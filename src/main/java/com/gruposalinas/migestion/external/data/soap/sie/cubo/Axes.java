/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Axes {

    @JacksonXmlProperty(localName = "Axis")
    private List<Axis> Axis;

    public List<Axis> getAxis() {
        return Axis;
    }

    public void setAxis(List<Axis> Axis) {
        this.Axis = Axis;
    }

    @Override
    public String toString() {
        return "Axes{" + "Axis=" + Axis + '}';
    }

}
