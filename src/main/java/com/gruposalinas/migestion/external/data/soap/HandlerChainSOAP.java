/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author cescobarh
 */
public class HandlerChainSOAP implements SOAPHandler<SOAPMessageContext> {

    private TransformerFactory transformerFactory = null;
    private Transformer transformer = null;
    private DocumentBuilderFactory dbf = null;
    private DocumentBuilder db = null;

    public HandlerChainSOAP() {
        try {
            transformerFactory = TransformerFactory.newInstance();
            transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
            dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            db = dbf.newDocumentBuilder();
        } catch (TransformerConfigurationException | TransformerFactoryConfigurationError | ParserConfigurationException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        System.out.println("Manejador del mensaje SOAP invocado...");
        out(context);
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {

    }

    private void out(SOAPMessageContext messageContext) {
        String xml = "";
        SOAPMessage mensajeSOAP = messageContext.getMessage();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            mensajeSOAP.writeTo(out);
            xml = out.toString("UTF-8");
        } catch (SOAPException | IOException ex) {
            System.out.println(ex);
        }

        String fase = "Fase de ";
        Boolean mensajeDeSalida = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (mensajeDeSalida) {
            fase += "Solicitud (REQUEST): \n";
        } else {
            fase += "Respuesta (RESPONSE): \n";
        }
        System.out.println(fase);
        System.out.println(parseXML(xml));
    }

    public String parseXML(String xml) {
        if (transformer == null || db == null) {
            return xml;
        }
        InputSource ipXML = new InputSource(new StringReader(xml));
        Document documento;
        try {
            documento = db.parse(ipXML);
            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            DOMSource ds = new DOMSource(documento);
            transformer.transform(ds, sr);
            return sw.toString();
        } catch (SAXException | IOException | TransformerException ex) {
            System.out.println(ex);
            return xml;
        }
    }

}
