/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.client;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gruposalinas.migestion.external.data.soap.sie.cubo.TableView;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class IndicadoresSIEBI {

    private final ProcessSIE sie = new ProcessSIE();
    private TableView tv;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private List<String> days = new ArrayList<String>();

    public IndicadoresSIEBI() {
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        days.add("Domingo");
        days.add("Lunes");
        days.add("Martes");
        days.add("Miercoles");
        days.add("Jueves");
        days.add("Viernes");
        days.add("Sabado");
    }

    public ObjectNode activacionesBazDigital(String fecha, String geografia, int idServicio, boolean totales) {
        tv = sie.activacionesBazDigital(fecha, geografia);
        return null;
    }

    public ObjectNode colocacionAcumulada(String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.colocacionAcumulada(geografia, nivel);
        return null;
    }

    public ObjectNode compromisoColocacion(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.compromisoColocacion(fecha, geografia, nivel);
        return null;
    }

    public ObjectNode consumoColocacion(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.consumoColocacion(fecha, geografia, nivel);
        ObjectNode mappedDataCredito = this.mappingDataCredito(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCredito(geografia, mappedDataCredito);
        } else {
            return this.columnsToArrayStrunctureCredito(mappedDataCredito, idServicio);
        }
    }

    public ObjectNode contribucion(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.contribucion(fecha, geografia, nivel);
        return null;
    }

    public ObjectNode graficaColocacionPersonales(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.graficaColocacionPersonales(fecha, geografia, nivel);
        return null;
    }

    public ObjectNode graficaColocacionTAZ(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.graficaColocacionTAZ(fecha, geografia, nivel);
        return null;
    }

    public ObjectNode graficaColocacionTotalConsumo(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.graficaColocacionTotalConsumo(fecha, geografia, nivel);
        return null;
    }

    public ObjectNode graficaColocacionTotalConsumo(String geografia, String producto, int idServicio, boolean totales) {
        tv = sie.graficaTiempoRealColocacion(geografia, producto);
        return null;
    }

    public ObjectNode graficaTiempoRealColocacion(String geografia, String producto, int idServicio, boolean totales) {
        tv = sie.graficaTiempoRealColocacion(geografia, producto);
        return null;
    }

    public ObjectNode nAbonadasPendSurt(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.nAbonadasPendSurt(fecha, geografia, nivel);
        return null;
    }

    public ObjectNode normalidad(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.normalidad(fecha, geografia, nivel);
        ObjectNode mappedDataContribucion = this.mappingDataContribucion(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureContribucion(geografia, mappedDataContribucion);
        } else {
            return this.columnsToArrayStrunctureContribucion(mappedDataContribucion, idServicio);
        }
    }

    public ObjectNode personalesColocacion(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.personalesColocacion(fecha, geografia, nivel);
        ObjectNode mappedDataCredito = this.mappingDataCredito(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCredito(geografia, mappedDataCredito);
        } else {
            return this.columnsToArrayStrunctureCredito(mappedDataCredito, idServicio);
        }
    }

    public ObjectNode personalesColocacionBAZDigital(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.personalesColocacionBAZDigital(fecha, geografia, nivel);
        return null;
    }

    public ObjectNode saldoCaptacionPlazo(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.saldoCaptacionPlazo(fecha, geografia, nivel);
        ObjectNode mappedDataCaptacion = this.mappingDataCaptacion(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataCaptacion);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataCaptacion, idServicio);
        }
    }

    public ObjectNode saldoCaptacionPlazoMetrica(String fecha, String geografia, String nivel, String metrica, int idServicio, boolean totales) {
        tv = sie.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, metrica);
        ObjectNode mappedDataCaptacion = this.mappingDataCaptacion(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataCaptacion);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataCaptacion, idServicio);
        }
    }

    public ObjectNode saldoCaptacionVista(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.saldoCaptacionVista(fecha, geografia, nivel);
        ObjectNode mappedDataCaptacion = this.mappingDataCaptacion(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataCaptacion);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataCaptacion, idServicio);
        }
    }

    public ObjectNode saldoCaptacionVistaMetrica(String fecha, String geografia, String nivel, String metrica, int idServicio, boolean totales) {
        tv = sie.saldoCaptacionVistaMetrica(fecha, geografia, nivel, metrica);
        ObjectNode mappedDataCaptacion = this.mappingDataCaptacion(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataCaptacion);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataCaptacion, idServicio);
        }
    }

    public ObjectNode tazColocacion(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.tazColocacion(fecha, geografia, nivel);
        ObjectNode mappedDataCredito = this.mappingDataCredito(tv.getTableView());
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCredito(geografia, mappedDataCredito);
        } else {
            return this.columnsToArrayStrunctureCredito(mappedDataCredito, idServicio);
        }
    }

    public ObjectNode tiempoRealColocacion(String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.tiempoRealColocacion(geografia, nivel);
        return null;
    }

    public ObjectNode tiempoRealColocacionConsumo(String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.tiempoRealColocacionConsumo(geografia, nivel);
        return null;
    }

    public ObjectNode tiempoRealColocacionCredito(String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.tiempoRealColocacionCredito(geografia, nivel);
        return null;
    }

    public ObjectNode tiempoRealColocacionPersonales(String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.tiempoRealColocacionPersonales(geografia, nivel);
        return null;
    }

    public ObjectNode trColocacionConsumoPorHora(String geografia, int idServicio, boolean totales) {
        tv = sie.trColocacionConsumoPorHora(geografia);
        return null;
    }

    public ObjectNode trColocacionCreditoPorHora(String geografia, int idServicio, boolean totales) {
        tv = sie.trColocacionCreditoPorHora(geografia);
        return null;
    }

    public ObjectNode trColocacionPersonalesPorHora(String geografia, int idServicio, boolean totales) {
        tv = sie.trColocacionPersonalesPorHora(geografia);
        return null;
    }

    public ObjectNode tranferenciasDEX(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.tranferenciasDEX(fecha, geografia, nivel);
        ObjectNode mappedDataTransferencias = this.mappingDataCaptacionTransferencias(tv.getTableView(), idServicio);
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataTransferencias);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataTransferencias, idServicio);
        }
    }

    public ObjectNode transferenciasCambios(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.transferenciasCambios(fecha, geografia, nivel);
        ObjectNode mappedDataTransferencias = this.mappingDataCaptacionTransferencias(tv.getTableView(), idServicio);
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataTransferencias);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataTransferencias, idServicio);
        }
    }

    public ObjectNode transferenciasInternacionales(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.transferenciasInternacionales(fecha, geografia, nivel);
        ObjectNode mappedDataTransferencias = this.mappingDataCaptacionTransferencias(tv.getTableView(), idServicio);
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataTransferencias);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataTransferencias, idServicio);
        }
    }

    public ObjectNode transferenciasPagoServicios(String fecha, String geografia, String nivel, int idServicio, boolean totales) {
        tv = sie.transferenciasPagoServicios(fecha, geografia, nivel);
        ObjectNode mappedDataTransferencias = this.mappingDataCaptacionTransferencias(tv.getTableView(), idServicio);
        if (totales) {
            return this.columnsTotalesToArrayStrunctureCaptacion(geografia, mappedDataTransferencias);
        } else {
            return this.columnsToArrayStrunctureCaptacion(mappedDataTransferencias, idServicio);
        }
    }

    private ObjectNode mappingDataContribucion(ObjectNode tableView) {
        ObjectNode newNodeFactor = objectMapper.createObjectNode();
        if (tableView != null && tableView.size() > 0) {
            Iterator<String> fieldNames = tableView.fieldNames();
            ObjectNode newNodeTemp;
            String nameField;

            Double anioAnteriorTotalMonto;
            Double anioAnteriorMonto;
            Double anioAnteriorPorcentaje;

            Double periodoAnteriorTotalMonto;
            Double periodoAnteriorMonto;
            Double periodoAnteriorPorcentaje;

            Double actualTotalMonto;
            Double actualMonto;
            Double actualPorcentaje;

            Double vsCompromisoPeriodoAnteriorMonto;
            Double vsCompromisoPeriodoAnteriorPorcentaje;
            Double vsCompromisoAnioAnteriorMonto;
            Double vsCompromisoAnioAnteriorPorcentaje;
            DecimalFormat decimalFormatMonto = new DecimalFormat("#");
            DecimalFormat decimalFormatPorcentajes = new DecimalFormat("#");
            JsonNode item;
            JsonNode itemTotales;

            while (fieldNames.hasNext()) {
                nameField = fieldNames.next();

                item = tableView.get(nameField);
                itemTotales = tableView.get("SALDO DE CARTERA");

                anioAnteriorTotalMonto = (itemTotales.get("AÑO ANTERIOR$") == null ? 0d : itemTotales.get("AÑO ANTERIOR$").asDouble());
                periodoAnteriorTotalMonto = (itemTotales.get("PERIODO ANTERIOR$") == null ? 0d : itemTotales.get("PERIODO ANTERIOR$").asDouble());
                actualTotalMonto = (itemTotales.get("ACTUAL$") == null ? 0d : itemTotales.get("ACTUAL$").asDouble());

                newNodeTemp = objectMapper.createObjectNode();

                newNodeTemp.put("Titulo", nameField);
                newNodeTemp.put("AnioAnteriorMonto", decimalFormatMonto.format((item.get("AÑO ANTERIOR$") == null ? 0d : item.get("AÑO ANTERIOR$").asDouble())));
                anioAnteriorPorcentaje = (newNodeTemp.get("AnioAnteriorMonto").asDouble() * 100 / anioAnteriorTotalMonto);
                anioAnteriorPorcentaje = (anioAnteriorPorcentaje.isNaN() || anioAnteriorPorcentaje.isInfinite() ? 0d : anioAnteriorPorcentaje);
                newNodeTemp.put("AnioAnteriorPorcentaje", decimalFormatMonto.format(anioAnteriorPorcentaje));

                newNodeTemp.put("PeriodoAnteriorMonto", decimalFormatMonto.format((item.get("PERIODO ANTERIOR$") == null ? 0d : item.get("PERIODO ANTERIOR$").asDouble())));
                periodoAnteriorPorcentaje = (newNodeTemp.get("PeriodoAnteriorMonto").asDouble() * 100 / periodoAnteriorTotalMonto);
                periodoAnteriorPorcentaje = (periodoAnteriorPorcentaje.isNaN() || periodoAnteriorPorcentaje.isInfinite() ? 0d : periodoAnteriorPorcentaje);
                newNodeTemp.put("PeriodoAnteriorPorcentaje", decimalFormatMonto.format(periodoAnteriorPorcentaje));

                newNodeTemp.put("ActualMonto", decimalFormatMonto.format((item.get("ACTUAL$") == null ? 0d : item.get("ACTUAL$").asDouble())));
                actualPorcentaje = (newNodeTemp.get("ActualMonto").asDouble() * 100 / actualTotalMonto);
                actualPorcentaje = (actualPorcentaje.isNaN() || actualPorcentaje.isInfinite() ? 0d : actualPorcentaje);
                newNodeTemp.put("ActualPorcentaje", decimalFormatMonto.format(actualPorcentaje));

                anioAnteriorMonto = newNodeTemp.get("AnioAnteriorMonto").asDouble();
                periodoAnteriorMonto = newNodeTemp.get("PeriodoAnteriorMonto").asDouble();
                actualMonto = newNodeTemp.get("ActualMonto").asDouble();

                vsCompromisoPeriodoAnteriorMonto = (actualMonto - periodoAnteriorMonto);
                vsCompromisoPeriodoAnteriorMonto = (vsCompromisoPeriodoAnteriorMonto.isNaN() || vsCompromisoPeriodoAnteriorMonto.isInfinite() ? 0d : vsCompromisoPeriodoAnteriorMonto);

                vsCompromisoPeriodoAnteriorPorcentaje = (vsCompromisoPeriodoAnteriorMonto * 100 / periodoAnteriorMonto);
                vsCompromisoPeriodoAnteriorPorcentaje = (vsCompromisoPeriodoAnteriorPorcentaje.isNaN() || vsCompromisoPeriodoAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoPeriodoAnteriorPorcentaje);

                vsCompromisoAnioAnteriorMonto = (actualMonto - anioAnteriorMonto);
                vsCompromisoAnioAnteriorMonto = (vsCompromisoAnioAnteriorMonto.isNaN() || vsCompromisoAnioAnteriorMonto.isInfinite() ? 0d : vsCompromisoAnioAnteriorMonto);

                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorMonto * 100 / anioAnteriorMonto);
                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorPorcentaje.isNaN() || vsCompromisoAnioAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoAnioAnteriorPorcentaje);

                newNodeTemp.put("VsCompromisoPeriodoAnteriorMonto", decimalFormatMonto.format(vsCompromisoPeriodoAnteriorMonto));
                newNodeTemp.put("VsCompromisoPeriodoAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoPeriodoAnteriorPorcentaje));
                newNodeTemp.put("VsCompromisoAnioAnteriorMonto", decimalFormatMonto.format(vsCompromisoAnioAnteriorMonto));
                newNodeTemp.put("VsCompromisoAnioAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoAnioAnteriorPorcentaje));
                nameField = (nameField.equals("SALDO DE CARTERA") ? "Totales" : nameField);

                newNodeFactor.set(nameField, newNodeTemp);
            }
        }
        return newNodeFactor;
    }

    private ObjectNode mappingDataCredito(ObjectNode tableView) {
        ObjectNode newNodeFactor = objectMapper.createObjectNode();
        if (tableView != null && tableView.size() > 0) {
            Iterator<String> fieldNames = tableView.fieldNames();
            ObjectNode newNodeTemp;
            String nameField;

            Double anioAnteriorTotalMonto;
            Double anioAnteriorMonto;
            Double anioAnteriorPorcentaje;

            Double dosSemanasAnterioresTotalMonto;
            Double dosSemanasAnterioresMonto;
            Double dosSemanasAnterioresPorcentaje;

            Double semanaAnteriorTotalMonto;
            Double semanaAnteriorMonto;
            Double semanaAnteriorPorcentaje;

            Double compromisoTotalMonto;
            Double compromisoMonto;
            Double compromisoPorcentaje;

            Double actualTotalMonto;
            Double actualMonto;
            Double actualPorcentaje;

            Double vsCompromisoMonto;
            Double vsCompromisoPorcentaje;
            Double vsCompromisoSemanaAnteriorMonto;
            Double vsCompromisoSemanaAnteriorPorcentaje;
            Double vsCompromisoDosSemanasAnterioresMonto;
            Double vsCompromisoDosSemanasAnterioresPorcentaje;
            Double vsCompromisoAnioAnteriorMonto;
            Double vsCompromisoAnioAnteriorPorcentaje;
            DecimalFormat decimalFormatMonto = new DecimalFormat("#");
            DecimalFormat decimalFormatPorcentajes = new DecimalFormat("#");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            Calendar calendar = Calendar.getInstance();
            Date dateTemp;
            JsonNode item;
            JsonNode itemTotales;

            while (fieldNames.hasNext()) {
                nameField = fieldNames.next();

                item = tableView.get(nameField);
                itemTotales = tableView.get("TOTAL COLOCACIÓN BRUTA");

                anioAnteriorTotalMonto = (itemTotales.get("AÑO ANT") == null ? 0d : itemTotales.get("AÑO ANT").asDouble());
                dosSemanasAnterioresTotalMonto = (itemTotales.get("2 SEM ANT") == null ? 0d : itemTotales.get("2 SEM ANT").asDouble());
                semanaAnteriorTotalMonto = (itemTotales.get("SEM ANT") == null ? 0d : itemTotales.get("SEM ANT").asDouble());
                compromisoTotalMonto = (itemTotales.get("COMPROMISO") == null ? 0d : itemTotales.get("COMPROMISO").asDouble());
                actualTotalMonto = (itemTotales.get("REAL") == null ? 0d : itemTotales.get("REAL").asDouble());

                newNodeTemp = objectMapper.createObjectNode();

                try {
                    dateTemp = formatter.parse(nameField);
                    calendar.setTime(dateTemp);
                    newNodeTemp.put("Dia", days.get(calendar.get(Calendar.DAY_OF_WEEK) - 1));
                } catch (Exception ex) {
                    newNodeTemp.put("Dia", "Totales");
                }
                newNodeTemp.put("AnioAnteriorMonto", decimalFormatMonto.format((item.get("AÑO ANT") == null ? 0d : item.get("AÑO ANT").asDouble())));
                anioAnteriorPorcentaje = (newNodeTemp.get("AnioAnteriorMonto").asDouble() * 100 / anioAnteriorTotalMonto);
                anioAnteriorPorcentaje = (anioAnteriorPorcentaje.isNaN() || anioAnteriorPorcentaje.isInfinite() ? 0d : anioAnteriorPorcentaje);
                newNodeTemp.put("AnioAnteriorPorcentaje", decimalFormatMonto.format(anioAnteriorPorcentaje));

                newNodeTemp.put("DosSemanasAnterioresMonto", decimalFormatMonto.format((item.get("2 SEM ANT") == null ? 0d : item.get("2 SEM ANT").asDouble())));
                dosSemanasAnterioresPorcentaje = (newNodeTemp.get("DosSemanasAnterioresMonto").asDouble() * 100 / dosSemanasAnterioresTotalMonto);
                dosSemanasAnterioresPorcentaje = (dosSemanasAnterioresPorcentaje.isNaN() || dosSemanasAnterioresPorcentaje.isInfinite() ? 0d : dosSemanasAnterioresPorcentaje);
                newNodeTemp.put("DosSemanasAnterioresPorcentaje", decimalFormatMonto.format(dosSemanasAnterioresPorcentaje));

                newNodeTemp.put("SemanaAnteriorMonto", decimalFormatMonto.format((item.get("SEM ANT") == null ? 0d : item.get("SEM ANT").asDouble())));
                semanaAnteriorPorcentaje = (newNodeTemp.get("SemanaAnteriorMonto").asDouble() * 100 / semanaAnteriorTotalMonto);
                semanaAnteriorPorcentaje = (semanaAnteriorPorcentaje.isNaN() || semanaAnteriorPorcentaje.isInfinite() ? 0d : semanaAnteriorPorcentaje);
                newNodeTemp.put("SemanaAnteriorPorcentaje", decimalFormatMonto.format(semanaAnteriorPorcentaje));

                newNodeTemp.put("CompromisoMonto", decimalFormatMonto.format((item.get("COMPROMISO") == null ? 0d : item.get("COMPROMISO").asDouble())));
                compromisoPorcentaje = (newNodeTemp.get("CompromisoMonto").asDouble() * 100 / compromisoTotalMonto);
                compromisoPorcentaje = (compromisoPorcentaje.isNaN() || compromisoPorcentaje.isInfinite() ? 0d : compromisoPorcentaje);
                newNodeTemp.put("CompromisoMontoPorcentaje", decimalFormatMonto.format(compromisoPorcentaje));

                newNodeTemp.put("ActualMonto", decimalFormatMonto.format((item.get("REAL") == null ? 0d : item.get("REAL").asDouble())));
                actualPorcentaje = (newNodeTemp.get("ActualMonto").asDouble() * 100 / actualTotalMonto);
                actualPorcentaje = (actualPorcentaje.isNaN() || actualPorcentaje.isInfinite() ? 0d : actualPorcentaje);
                newNodeTemp.put("ActualPorcentaje", decimalFormatMonto.format(actualPorcentaje));

                anioAnteriorMonto = newNodeTemp.get("AnioAnteriorMonto").asDouble();
                dosSemanasAnterioresMonto = newNodeTemp.get("DosSemanasAnterioresMonto").asDouble();
                semanaAnteriorMonto = newNodeTemp.get("SemanaAnteriorMonto").asDouble();
                compromisoMonto = newNodeTemp.get("CompromisoMonto").asDouble();
                actualMonto = newNodeTemp.get("ActualMonto").asDouble();

                vsCompromisoMonto = (actualMonto - compromisoMonto);
                vsCompromisoMonto = (vsCompromisoMonto.isNaN() || vsCompromisoMonto.isInfinite() ? 0d : vsCompromisoMonto);

                vsCompromisoPorcentaje = (vsCompromisoMonto * 100 / compromisoMonto);
                vsCompromisoPorcentaje = (vsCompromisoPorcentaje.isNaN() || vsCompromisoPorcentaje.isInfinite() ? 0d : vsCompromisoPorcentaje);

                vsCompromisoDosSemanasAnterioresMonto = (actualMonto - dosSemanasAnterioresMonto);
                vsCompromisoDosSemanasAnterioresMonto = (vsCompromisoDosSemanasAnterioresMonto.isNaN() || vsCompromisoDosSemanasAnterioresMonto.isInfinite() ? 0d : vsCompromisoDosSemanasAnterioresMonto);

                vsCompromisoDosSemanasAnterioresPorcentaje = (vsCompromisoDosSemanasAnterioresMonto * 100 / dosSemanasAnterioresMonto);
                vsCompromisoDosSemanasAnterioresPorcentaje = (vsCompromisoDosSemanasAnterioresPorcentaje.isNaN() || vsCompromisoDosSemanasAnterioresPorcentaje.isInfinite() ? 0d : vsCompromisoDosSemanasAnterioresPorcentaje);

                vsCompromisoSemanaAnteriorMonto = (actualMonto - semanaAnteriorMonto);
                vsCompromisoSemanaAnteriorMonto = (vsCompromisoSemanaAnteriorMonto.isNaN() || vsCompromisoSemanaAnteriorMonto.isInfinite() ? 0d : vsCompromisoSemanaAnteriorMonto);

                vsCompromisoSemanaAnteriorPorcentaje = (vsCompromisoSemanaAnteriorMonto * 100 / semanaAnteriorMonto);
                vsCompromisoSemanaAnteriorPorcentaje = (vsCompromisoSemanaAnteriorPorcentaje.isNaN() || vsCompromisoSemanaAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoSemanaAnteriorPorcentaje);

                vsCompromisoAnioAnteriorMonto = (actualMonto - anioAnteriorMonto);
                vsCompromisoAnioAnteriorMonto = (vsCompromisoAnioAnteriorMonto.isNaN() || vsCompromisoAnioAnteriorMonto.isInfinite() ? 0d : vsCompromisoAnioAnteriorMonto);

                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorMonto * 100 / anioAnteriorMonto);
                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorPorcentaje.isNaN() || vsCompromisoAnioAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoAnioAnteriorPorcentaje);

                newNodeTemp.put("VsCompromisoMonto", decimalFormatMonto.format(vsCompromisoMonto));
                newNodeTemp.put("VsCompromisoPorcentaje", decimalFormatPorcentajes.format(vsCompromisoPorcentaje));
                newNodeTemp.put("VsCompromisoDosSemanasAnterioresMonto", decimalFormatMonto.format(vsCompromisoDosSemanasAnterioresMonto));
                newNodeTemp.put("VsCompromisoDosSemanasAnterioresPorcentaje", decimalFormatPorcentajes.format(vsCompromisoDosSemanasAnterioresPorcentaje));
                newNodeTemp.put("VsCompromisoSemanaAnteriorMonto", decimalFormatMonto.format(vsCompromisoSemanaAnteriorMonto));
                newNodeTemp.put("VsCompromisoSemanaAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoSemanaAnteriorPorcentaje));
                newNodeTemp.put("VsCompromisoAnioAnteriorMonto", decimalFormatMonto.format(vsCompromisoAnioAnteriorMonto));
                newNodeTemp.put("VsCompromisoAnioAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoAnioAnteriorPorcentaje));
                nameField = (nameField.equals("TOTAL COLOCACIÓN BRUTA") ? "Totales" : nameField);

                newNodeFactor.set(nameField, newNodeTemp);
            }
        }
        return newNodeFactor;
    }

    private ObjectNode mappingDataCaptacion(ObjectNode tableView) {
        ObjectNode newNodeFactor = objectMapper.createObjectNode();
        if (tableView != null && tableView.size() > 0) {
            Iterator<String> fieldNames = tableView.fieldNames();
            ObjectNode newNodeTemp;
            String nameField;

            Double anioAnteriorTotalMonto;
            Double anioAnteriorMonto;
            Double anioAnteriorPorcentaje;

            Double periodoAnteriorTotalMonto;
            Double periodoAnteriorMonto;
            Double periodoAnteriorPorcentaje;

            Double compromisoTotalMonto;
            Double compromisoMonto;
            Double compromisoPorcentaje;

            Double actualTotalMonto;
            Double actualMonto;
            Double actualPorcentaje;

            Double vsCompromisoMonto;
            Double vsCompromisoPorcentaje;
            Double vsCompromisoPeriodoAnteriorMonto;
            Double vsCompromisoPeriodoAnteriorPorcentaje;
            Double vsCompromisoAnioAnteriorMonto;
            Double vsCompromisoAnioAnteriorPorcentaje;
            DecimalFormat decimalFormatMonto = new DecimalFormat("#");
            DecimalFormat decimalFormatPorcentajes = new DecimalFormat("#");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            Calendar calendar = Calendar.getInstance();
            Date dateTemp;
            JsonNode item;
            JsonNode itemTotales;

            while (fieldNames.hasNext()) {
                nameField = fieldNames.next();

                item = tableView.get(nameField);
                itemTotales = tableView.get("SALDO CLIENTES");

                anioAnteriorTotalMonto = (itemTotales.get("AÑO ANTERIOR") == null ? 0d : itemTotales.get("AÑO ANTERIOR").asDouble());
                periodoAnteriorTotalMonto = (itemTotales.get("PERIODO ANTERIOR") == null ? 0d : itemTotales.get("PERIODO ANTERIOR").asDouble());
                compromisoTotalMonto = (itemTotales.get("COMPROMISO") == null ? 0d : itemTotales.get("COMPROMISO").asDouble());
                actualTotalMonto = (itemTotales.get("ACTUAL") == null ? 0d : itemTotales.get("ACTUAL").asDouble());

                newNodeTemp = objectMapper.createObjectNode();

                try {
                    dateTemp = formatter.parse(nameField);
                    calendar.setTime(dateTemp);
                    newNodeTemp.put("Dia", days.get(calendar.get(Calendar.DAY_OF_WEEK) - 1));
                } catch (Exception ex) {
                    newNodeTemp.put("Dia", "Totales");
                }
                newNodeTemp.put("AnioAnteriorMonto", decimalFormatMonto.format((item.get("AÑO ANTERIOR") == null ? 0d : item.get("AÑO ANTERIOR").asDouble())));
                anioAnteriorPorcentaje = (newNodeTemp.get("AnioAnteriorMonto").asDouble() * 100 / anioAnteriorTotalMonto);
                anioAnteriorPorcentaje = (anioAnteriorPorcentaje.isNaN() || anioAnteriorPorcentaje.isInfinite() ? 0d : anioAnteriorPorcentaje);
                newNodeTemp.put("AnioAnteriorPorcentaje", decimalFormatMonto.format(anioAnteriorPorcentaje));

                newNodeTemp.put("PeriodoAnteriorMonto", decimalFormatMonto.format((item.get("PERIODO ANTERIOR") == null ? 0d : item.get("PERIODO ANTERIOR").asDouble())));
                periodoAnteriorPorcentaje = (newNodeTemp.get("PeriodoAnteriorMonto").asDouble() * 100 / periodoAnteriorTotalMonto);
                periodoAnteriorPorcentaje = (periodoAnteriorPorcentaje.isNaN() || periodoAnteriorPorcentaje.isInfinite() ? 0d : periodoAnteriorPorcentaje);
                newNodeTemp.put("PeriodoAnteriorPorcentaje", decimalFormatMonto.format(periodoAnteriorPorcentaje));

                newNodeTemp.put("CompromisoMonto", decimalFormatMonto.format((item.get("COMPROMISO") == null ? 0d : item.get("COMPROMISO").asDouble())));
                compromisoPorcentaje = (newNodeTemp.get("CompromisoMonto").asDouble() * 100 / compromisoTotalMonto);
                compromisoPorcentaje = (compromisoPorcentaje.isNaN() || compromisoPorcentaje.isInfinite() ? 0d : compromisoPorcentaje);
                newNodeTemp.put("CompromisoMontoPorcentaje", decimalFormatMonto.format(compromisoPorcentaje));

                newNodeTemp.put("ActualMonto", decimalFormatMonto.format((item.get("ACTUAL") == null ? 0d : item.get("ACTUAL").asDouble())));
                actualPorcentaje = (newNodeTemp.get("ActualMonto").asDouble() * 100 / actualTotalMonto);
                actualPorcentaje = (actualPorcentaje.isNaN() || actualPorcentaje.isInfinite() ? 0d : actualPorcentaje);
                newNodeTemp.put("ActualPorcentaje", decimalFormatMonto.format(actualPorcentaje));

                anioAnteriorMonto = newNodeTemp.get("AnioAnteriorMonto").asDouble();
                periodoAnteriorMonto = newNodeTemp.get("PeriodoAnteriorMonto").asDouble();
                compromisoMonto = newNodeTemp.get("CompromisoMonto").asDouble();
                actualMonto = newNodeTemp.get("ActualMonto").asDouble();

                vsCompromisoMonto = (actualMonto - compromisoMonto);
                vsCompromisoMonto = (vsCompromisoMonto.isNaN() || vsCompromisoMonto.isInfinite() ? 0d : vsCompromisoMonto);

                vsCompromisoPorcentaje = (vsCompromisoMonto * 100 / compromisoMonto);
                vsCompromisoPorcentaje = (vsCompromisoPorcentaje.isNaN() || vsCompromisoPorcentaje.isInfinite() ? 0d : vsCompromisoPorcentaje);

                vsCompromisoPeriodoAnteriorMonto = (actualMonto - periodoAnteriorMonto);
                vsCompromisoPeriodoAnteriorMonto = (vsCompromisoPeriodoAnteriorMonto.isNaN() || vsCompromisoPeriodoAnteriorMonto.isInfinite() ? 0d : vsCompromisoPeriodoAnteriorMonto);

                vsCompromisoPeriodoAnteriorPorcentaje = (vsCompromisoPeriodoAnteriorMonto * 100 / periodoAnteriorMonto);
                vsCompromisoPeriodoAnteriorPorcentaje = (vsCompromisoPeriodoAnteriorPorcentaje.isNaN() || vsCompromisoPeriodoAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoPeriodoAnteriorPorcentaje);

                vsCompromisoAnioAnteriorMonto = (actualMonto - anioAnteriorMonto);
                vsCompromisoAnioAnteriorMonto = (vsCompromisoAnioAnteriorMonto.isNaN() || vsCompromisoAnioAnteriorMonto.isInfinite() ? 0d : vsCompromisoAnioAnteriorMonto);

                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorMonto * 100 / anioAnteriorMonto);
                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorPorcentaje.isNaN() || vsCompromisoAnioAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoAnioAnteriorPorcentaje);

                newNodeTemp.put("VsCompromisoMonto", decimalFormatMonto.format(vsCompromisoMonto));
                newNodeTemp.put("VsCompromisoPorcentaje", decimalFormatPorcentajes.format(vsCompromisoPorcentaje));
                newNodeTemp.put("VsCompromisoPeriodoAnteriorMonto", decimalFormatMonto.format(vsCompromisoPeriodoAnteriorMonto));
                newNodeTemp.put("VsCompromisoPeriodoAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoPeriodoAnteriorPorcentaje));
                newNodeTemp.put("VsCompromisoAnioAnteriorMonto", decimalFormatMonto.format(vsCompromisoAnioAnteriorMonto));
                newNodeTemp.put("VsCompromisoAnioAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoAnioAnteriorPorcentaje));
                nameField = (nameField.equals("SALDO CLIENTES") ? "Totales" : nameField);

                newNodeFactor.set(nameField, newNodeTemp);
            }
        }
        return newNodeFactor;
    }

    private ObjectNode mappingDataCaptacionTransferencias(ObjectNode tableView, int idServicio) {
        ObjectNode newNodeFactor = objectMapper.createObjectNode();
        if (tableView != null && tableView.size() > 0) {
            Iterator<String> fieldNames = tableView.fieldNames();
            ObjectNode newNodeTemp;
            String nameField;

            Double anioAnteriorTotalMonto;
            Double anioAnteriorMonto;
            Double anioAnteriorPorcentaje;

            Double periodoAnteriorTotalMonto;
            Double periodoAnteriorMonto;
            Double periodoAnteriorPorcentaje;

            Double compromisoTotalMonto;
            Double compromisoMonto;
            Double compromisoPorcentaje;

            Double actualTotalMonto;
            Double actualMonto;
            Double actualPorcentaje;

            Double vsCompromisoMonto;
            Double vsCompromisoPorcentaje;
            Double vsCompromisoPeriodoAnteriorMonto;
            Double vsCompromisoPeriodoAnteriorPorcentaje;
            Double vsCompromisoAnioAnteriorMonto;
            Double vsCompromisoAnioAnteriorPorcentaje;
            DecimalFormat decimalFormatMonto = new DecimalFormat("#");
            DecimalFormat decimalFormatPorcentajes = new DecimalFormat("#");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            Calendar calendar = Calendar.getInstance();
            Date dateTemp;
            JsonNode item;
            JsonNode itemTotales;

            String attrAnioAnterior;
            String attrPeriodoAnterior;
            String attrCompromiso;
            String attrActual;

            if (idServicio == 19) {//Se cambia la descripción de los atributos ya que son diferentes para el servicio de pago de servicios y todo lo demas funciona igual
                attrAnioAnterior = "Año Ant";
                attrPeriodoAnterior = "Sem Ant";
                attrCompromiso = "Compromiso";
                attrActual = "Real";
            } else {
                attrAnioAnterior = "AÑO ANTERIOR";
                attrPeriodoAnterior = "PERIODO ANTERIOR";
                attrCompromiso = "PLAN";
                attrActual = "ACTUAL";
            }

            while (fieldNames.hasNext()) {
                nameField = fieldNames.next();

                item = tableView.get(nameField);
                itemTotales = tableView.get("TOTAL TRANSACCIONES");

                anioAnteriorTotalMonto = (itemTotales.get(attrAnioAnterior) == null ? 0d : itemTotales.get(attrAnioAnterior).asDouble());
                periodoAnteriorTotalMonto = (itemTotales.get(attrPeriodoAnterior) == null ? 0d : itemTotales.get(attrPeriodoAnterior).asDouble());
                compromisoTotalMonto = (itemTotales.get(attrCompromiso) == null ? 0d : itemTotales.get(attrCompromiso).asDouble());
                actualTotalMonto = (itemTotales.get(attrActual) == null ? 0d : itemTotales.get(attrActual).asDouble());

                newNodeTemp = objectMapper.createObjectNode();

                try {
                    dateTemp = formatter.parse(nameField);
                    calendar.setTime(dateTemp);
                    newNodeTemp.put("Dia", days.get(calendar.get(Calendar.DAY_OF_WEEK) - 1));
                } catch (Exception ex) {
                    newNodeTemp.put("Dia", days.get(calendar.get(Calendar.DAY_OF_WEEK) - 1));
                }
                newNodeTemp.put("AnioAnteriorMonto", decimalFormatMonto.format((item.get(attrAnioAnterior) == null ? 0d : item.get(attrAnioAnterior).asDouble())));
                anioAnteriorPorcentaje = (newNodeTemp.get("AnioAnteriorMonto").asDouble() * 100 / anioAnteriorTotalMonto);
                anioAnteriorPorcentaje = (anioAnteriorPorcentaje.isNaN() || anioAnteriorPorcentaje.isInfinite() ? 0d : anioAnteriorPorcentaje);
                newNodeTemp.put("AnioAnteriorPorcentaje", decimalFormatMonto.format(anioAnteriorPorcentaje));

                newNodeTemp.put("PeriodoAnteriorMonto", decimalFormatMonto.format((item.get(attrPeriodoAnterior) == null ? 0d : item.get(attrPeriodoAnterior).asDouble())));
                periodoAnteriorPorcentaje = (newNodeTemp.get("PeriodoAnteriorMonto").asDouble() * 100 / periodoAnteriorTotalMonto);
                periodoAnteriorPorcentaje = (periodoAnteriorPorcentaje.isNaN() || periodoAnteriorPorcentaje.isInfinite() ? 0d : periodoAnteriorPorcentaje);
                newNodeTemp.put("PeriodoAnteriorPorcentaje", decimalFormatMonto.format(periodoAnteriorPorcentaje));

                newNodeTemp.put("CompromisoMonto", decimalFormatMonto.format((item.get(attrCompromiso) == null ? 0d : item.get(attrCompromiso).asDouble())));
                compromisoPorcentaje = (newNodeTemp.get("CompromisoMonto").asDouble() * 100 / compromisoTotalMonto);
                compromisoPorcentaje = (compromisoPorcentaje.isNaN() || compromisoPorcentaje.isInfinite() ? 0d : compromisoPorcentaje);
                newNodeTemp.put("CompromisoMontoPorcentaje", decimalFormatMonto.format(compromisoPorcentaje));

                newNodeTemp.put("ActualMonto", decimalFormatMonto.format((item.get(attrActual) == null ? 0d : item.get(attrActual).asDouble())));
                actualPorcentaje = (newNodeTemp.get("ActualMonto").asDouble() * 100 / actualTotalMonto);
                actualPorcentaje = (actualPorcentaje.isNaN() || actualPorcentaje.isInfinite() ? 0d : actualPorcentaje);
                newNodeTemp.put("ActualPorcentaje", decimalFormatMonto.format(actualPorcentaje));

                anioAnteriorMonto = newNodeTemp.get("AnioAnteriorMonto").asDouble();
                periodoAnteriorMonto = newNodeTemp.get("PeriodoAnteriorMonto").asDouble();
                compromisoMonto = newNodeTemp.get("CompromisoMonto").asDouble();
                actualMonto = newNodeTemp.get("ActualMonto").asDouble();

                vsCompromisoMonto = (actualMonto - compromisoMonto);
                vsCompromisoMonto = (vsCompromisoMonto.isNaN() || vsCompromisoMonto.isInfinite() ? 0d : vsCompromisoMonto);

                vsCompromisoPorcentaje = (vsCompromisoMonto * 100 / compromisoMonto);
                vsCompromisoPorcentaje = (vsCompromisoPorcentaje.isNaN() || vsCompromisoPorcentaje.isInfinite() ? 0d : vsCompromisoPorcentaje);

                vsCompromisoPeriodoAnteriorMonto = (actualMonto - periodoAnteriorMonto);
                vsCompromisoPeriodoAnteriorMonto = (vsCompromisoPeriodoAnteriorMonto.isNaN() || vsCompromisoPeriodoAnteriorMonto.isInfinite() ? 0d : vsCompromisoPeriodoAnteriorMonto);

                vsCompromisoPeriodoAnteriorPorcentaje = (vsCompromisoPeriodoAnteriorMonto * 100 / periodoAnteriorMonto);
                vsCompromisoPeriodoAnteriorPorcentaje = (vsCompromisoPeriodoAnteriorPorcentaje.isNaN() || vsCompromisoPeriodoAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoPeriodoAnteriorPorcentaje);

                vsCompromisoAnioAnteriorMonto = (actualMonto - anioAnteriorMonto);
                vsCompromisoAnioAnteriorMonto = (vsCompromisoAnioAnteriorMonto.isNaN() || vsCompromisoAnioAnteriorMonto.isInfinite() ? 0d : vsCompromisoAnioAnteriorMonto);

                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorMonto * 100 / anioAnteriorMonto);
                vsCompromisoAnioAnteriorPorcentaje = (vsCompromisoAnioAnteriorPorcentaje.isNaN() || vsCompromisoAnioAnteriorPorcentaje.isInfinite() ? 0d : vsCompromisoAnioAnteriorPorcentaje);

                newNodeTemp.put("VsCompromisoMonto", decimalFormatMonto.format(vsCompromisoMonto));
                newNodeTemp.put("VsCompromisoPorcentaje", decimalFormatPorcentajes.format(vsCompromisoPorcentaje));
                newNodeTemp.put("VsCompromisoPeriodoAnteriorMonto", decimalFormatMonto.format(vsCompromisoPeriodoAnteriorMonto));
                newNodeTemp.put("VsCompromisoPeriodoAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoPeriodoAnteriorPorcentaje));
                newNodeTemp.put("VsCompromisoAnioAnteriorMonto", decimalFormatMonto.format(vsCompromisoAnioAnteriorMonto));
                newNodeTemp.put("VsCompromisoAnioAnteriorPorcentaje", decimalFormatPorcentajes.format(vsCompromisoAnioAnteriorPorcentaje));
                nameField = (nameField.equals("TOTAL TRANSACCIONES") ? "Totales" : nameField);

                newNodeFactor.set(nameField, newNodeTemp);
            }
        }

        return newNodeFactor;
    }

    private ObjectNode columnsToArrayStrunctureContribucion(ObjectNode items, int idServicio) {
        ObjectNode response = objectMapper.createObjectNode();
        if (items != null) {
            String codigo = this.codigoFromIdServicio(idServicio);
            ArrayNode indicadores = objectMapper.createArrayNode();
            ObjectNode detalle = objectMapper.createObjectNode();
            ArrayNode detallesContenedor = objectMapper.createArrayNode();
            ObjectNode detalles = objectMapper.createObjectNode();
            ArrayNode row;
            ObjectNode attribute;
            String valueAttribute;
            String keyAttribute;
            String color = "";
            ObjectNode totales = objectMapper.createObjectNode();
            DecimalFormat formart = new DecimalFormat("###,###");

            JsonNode totalesTemp = items.get("Totales");
            if (totalesTemp != null) {
                totales.put("indicador", totalesTemp.get("ActualPorcentaje").asText() + "%");
                color = totalesTemp.get("ActualPorcentaje").asDouble() > 0 ? "verde" : "rojo";
                totales.put("color", color);
                totales.put("idService", "" + idServicio);
            }
            indicadores.add(totales);
            for (JsonNode rowTemp : items) {
                row = objectMapper.createArrayNode();
                Iterator<String> fieldNames = rowTemp.fieldNames();
                while (fieldNames.hasNext()) {
                    keyAttribute = fieldNames.next();
                    if (!keyAttribute.equals("Titulo") && !keyAttribute.equals("AnioAnteriorPorcentaje") && !keyAttribute.equals("PeriodoAnteriorPorcentaje") && !keyAttribute.equals("ActualPorcentaje")) {
                        valueAttribute = rowTemp.get(keyAttribute).asText();
                        attribute = objectMapper.createObjectNode();

                        color = rowTemp.get(keyAttribute).asDouble() > 0 ? "verde" : "rojo";
                        attribute.put("color", color);
                        attribute.put("cantidad", formart.format(new Double(valueAttribute)));

                        if (keyAttribute.equals("AnioAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("AnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("PeriodoAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("PeriodoAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("ActualMonto")) {
                            attribute.put("porcentaje", rowTemp.get("ActualPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPeriodoAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoAnioAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje") || keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                            attribute.put("color", "");
                            attribute.put("cantidad", "0");
                        }

                        attribute.put("porcentaje", (attribute.get("porcentaje") == null ? "0" : attribute.get("porcentaje").asText()) + "%");

                        row.add(attribute);
                    }
                }
                detalle.set(rowTemp.get("Titulo").asText().toLowerCase(), row);
            }
            detalles.put("codigo", codigo);
            detalles.put("idService", "" + idServicio);
            detalles.set("detalle", detalle);
            detallesContenedor.add(detalles);
            response.set("indicadores", indicadores);
            response.set("detalles", detallesContenedor);
        }
        return response;
    }

    private ObjectNode columnsTotalesToArrayStrunctureContribucion(String geografia, ObjectNode items) {
        ObjectNode response = objectMapper.createObjectNode();
        if (items != null) {
            ArrayNode row;
            ObjectNode attribute;
            String valueAttribute;
            String keyAttribute;
            String color = "";
            DecimalFormat formart = new DecimalFormat("###,###");
            ObjectNode rowTotales = items.with("Totales");
            row = objectMapper.createArrayNode();
            Iterator<String> fieldNames = rowTotales.fieldNames();
            while (fieldNames.hasNext()) {
                keyAttribute = fieldNames.next();
                if (!keyAttribute.equals("Titulo") && !keyAttribute.equals("AnioAnteriorPorcentaje") && !keyAttribute.equals("PeriodoAnteriorPorcentaje") && !keyAttribute.equals("ActualPorcentaje")) {
                    valueAttribute = rowTotales.get(keyAttribute).asText();
                    attribute = objectMapper.createObjectNode();

                    color = rowTotales.get(keyAttribute).asDouble() > 0 ? "verde" : "rojo";
                    attribute.put("color", color);
                    attribute.put("cantidad", formart.format(new Double(valueAttribute)));

                    if (keyAttribute.equals("AnioAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("AnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("PeriodoAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("PeriodoAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("ActualMonto")) {
                        attribute.put("porcentaje", rowTotales.get("ActualPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPeriodoAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoAnioAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje") || keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                        attribute.put("color", "");
                        attribute.put("cantidad", "0");
                    }

                    attribute.put("porcentaje", (attribute.get("porcentaje") == null ? "0" : attribute.get("porcentaje").asText()) + "%");

                    row.add(attribute);
                }
            }
            response.put("ceco", geografia);
            response.set("valores", row);
        }
        return response;
    }

    private ObjectNode columnsToArrayStrunctureCredito(ObjectNode items, int idServicio) {
        ObjectNode response = objectMapper.createObjectNode();
        if (items != null) {
            String codigo = this.codigoFromIdServicio(idServicio);
            ArrayNode indicadores = objectMapper.createArrayNode();
            ObjectNode detalle = objectMapper.createObjectNode();
            ArrayNode detallesContenedor = objectMapper.createArrayNode();
            ObjectNode detalles = objectMapper.createObjectNode();
            ArrayNode row;
            ObjectNode attribute;
            String valueAttribute;
            String keyAttribute;
            String color = "";
            ObjectNode totales = objectMapper.createObjectNode();
            DecimalFormat formart = new DecimalFormat("###,###");

            JsonNode totalesTemp = items.get("Totales");
            if (totalesTemp != null) {
                totales.put("indicador", totalesTemp.get("VsCompromisoPorcentaje").asText() + "%");
                color = totalesTemp.get("VsCompromisoPorcentaje").asDouble() > 0 ? "verde" : "rojo";
                totales.put("color", color);
                totales.put("idService", "" + idServicio);
            }
            indicadores.add(totales);
            for (JsonNode rowTemp : items) {
                row = objectMapper.createArrayNode();
                Iterator<String> fieldNames = rowTemp.fieldNames();
                while (fieldNames.hasNext()) {
                    keyAttribute = fieldNames.next();
                    if (!keyAttribute.equals("AnioAnteriorPorcentaje") && !keyAttribute.equals("DosSemanasAnterioresPorcentaje") && !keyAttribute.equals("SemanaAnteriorPorcentaje") && !keyAttribute.equals("CompromisoMontoPorcentaje") && !keyAttribute.equals("ActualPorcentaje")) {
                        valueAttribute = rowTemp.get(keyAttribute).asText();
                        attribute = objectMapper.createObjectNode();

                        if (!keyAttribute.equals("Dia")) {
                            color = rowTemp.get(keyAttribute).asDouble() > 0 ? "verde" : "rojo";
                            attribute.put("color", color);
                            attribute.put("cantidad", formart.format(new Double(valueAttribute)));

                        }

                        if (keyAttribute.equals("Dia")) {
                            attribute.put("color", "");
                            attribute.put("cantidad", "");
                            attribute.put("porcentaje", "0");
                        }

                        if (keyAttribute.equals("AnioAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("AnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("DosSemanasAnterioresMonto")) {
                            attribute.put("porcentaje", rowTemp.get("DosSemanasAnterioresPorcentaje").asText());
                        }

                        if (keyAttribute.equals("SemanaAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("SemanaAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("CompromisoMonto")) {
                            attribute.put("porcentaje", rowTemp.get("CompromisoMontoPorcentaje").asText());
                        }

                        if (keyAttribute.equals("ActualMonto")) {
                            attribute.put("porcentaje", rowTemp.get("ActualPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoSemanaAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoSemanaAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoDosSemanasAnterioresMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoDosSemanasAnterioresPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoAnioAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoSemanaAnteriorPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoSemanaAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoDosSemanasAnterioresPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoDosSemanasAnterioresPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPorcentaje") || keyAttribute.equals("VsCompromisoSemanaAnteriorPorcentaje") || keyAttribute.equals("VsCompromisoDosSemanasAnterioresPorcentaje") || keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                            attribute.put("color", "");
                            attribute.put("cantidad", "0");
                        }

                        attribute.put("porcentaje", (attribute.get("porcentaje") == null ? "0" : attribute.get("porcentaje").asText()) + "%");

                        if (keyAttribute.equals("AnioAnteriorMonto") || keyAttribute.equals("DosSemanasAnterioresMonto") || keyAttribute.equals("SemanaAnteriorMonto") || keyAttribute.equals("CompromisoMonto") || keyAttribute.equals("ActualMonto") || keyAttribute.equals("VsCompromisoAnioAnteriorMonto") || keyAttribute.equals("VsCompromisoDosSemanasAnterioresMonto") || keyAttribute.equals("VsCompromisoSemanaAnteriorMonto") || keyAttribute.equals("VsCompromisoMonto")) {
                            row.add(attribute);
                        }
                    }
                }
                detalle.set(rowTemp.get("Dia").asText().toLowerCase(), row);
            }
            detalles.put("codigo", codigo);
            detalles.put("idService", "" + idServicio);
            detalles.set("detalle", detalle);
            detallesContenedor.add(detalles);
            response.set("indicadores", indicadores);
            response.set("detalles", detallesContenedor);
        }
        return response;
    }

    private ObjectNode columnsTotalesToArrayStrunctureCredito(String geografia, ObjectNode items) {
        ObjectNode response = objectMapper.createObjectNode();
        if (items != null) {
            ArrayNode row;
            ObjectNode attribute;
            String valueAttribute;
            String keyAttribute;
            String color = "";
            DecimalFormat formart = new DecimalFormat("###,###");
            ObjectNode rowTotales = items.with("Totales");
            row = objectMapper.createArrayNode();
            Iterator<String> fieldNames = rowTotales.fieldNames();
            while (fieldNames.hasNext()) {
                keyAttribute = fieldNames.next();
                if (!keyAttribute.equals("AnioAnteriorPorcentaje") && !keyAttribute.equals("DosSemanasAnterioresPorcentaje") && !keyAttribute.equals("SemanaAnteriorPorcentaje") && !keyAttribute.equals("CompromisoMontoPorcentaje") && !keyAttribute.equals("ActualPorcentaje")) {
                    valueAttribute = rowTotales.get(keyAttribute).asText();
                    attribute = objectMapper.createObjectNode();

                    if (!keyAttribute.equals("Dia")) {
                        color = rowTotales.get(keyAttribute).asDouble() > 0 ? "verde" : "rojo";
                        attribute.put("color", color);
                        attribute.put("cantidad", formart.format(new Double(valueAttribute)));

                    }

                    if (keyAttribute.equals("Dia")) {
                        attribute.put("color", "");
                        attribute.put("cantidad", "");
                        attribute.put("porcentaje", "0");
                    }

                    if (keyAttribute.equals("AnioAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("AnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("DosSemanasAnterioresMonto")) {
                        attribute.put("porcentaje", rowTotales.get("DosSemanasAnterioresPorcentaje").asText());
                    }

                    if (keyAttribute.equals("SemanaAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("SemanaAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("CompromisoMonto")) {
                        attribute.put("porcentaje", rowTotales.get("CompromisoMontoPorcentaje").asText());
                    }

                    if (keyAttribute.equals("ActualMonto")) {
                        attribute.put("porcentaje", rowTotales.get("ActualPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoSemanaAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoSemanaAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoAnioAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoSemanaAnteriorPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoSemanaAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPorcentaje") || keyAttribute.equals("VsCompromisoSemanaAnteriorPorcentaje") || keyAttribute.equals("VsCompromisoDosSemanasAnterioresPorcentaje") || keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                        attribute.put("color", "");
                        attribute.put("cantidad", "0");
                    }

                    attribute.put("porcentaje", (attribute.get("porcentaje") == null ? "0" : attribute.get("porcentaje").asText()) + "%");

                    row.add(attribute);
                }
            }
            response.put("ceco", geografia);
            response.set("valores", row);
        }
        return response;
    }

    private ObjectNode columnsToArrayStrunctureCaptacion(ObjectNode items, int idServicio) {
        ObjectNode response = objectMapper.createObjectNode();
        if (items != null) {
            String codigo = this.codigoFromIdServicio(idServicio);
            ArrayNode indicadores = objectMapper.createArrayNode();
            ArrayNode detalle = objectMapper.createArrayNode();
            ArrayNode detallesContenedor = objectMapper.createArrayNode();
            ObjectNode detalles = objectMapper.createObjectNode();
            ArrayNode row;
            ObjectNode attribute;
            String valueAttribute;
            String keyAttribute;
            String color = "";
            ObjectNode totales = objectMapper.createObjectNode();
            DecimalFormat formart = new DecimalFormat("###,###");

            JsonNode totalesTemp = items.get("Totales");
            if (totalesTemp != null) {
                totales.put("indicador", totalesTemp.get("VsCompromisoPorcentaje").asText() + "%");
                color = totalesTemp.get("VsCompromisoPorcentaje").asDouble() > 0 ? "verde" : "rojo";
                totales.put("color", color);
                totales.put("idService", "" + idServicio);
            }
            indicadores.add(totales);

            for (JsonNode rowTemp : items) {
                row = objectMapper.createArrayNode();
                Iterator<String> fieldNames = rowTemp.fieldNames();
                while (fieldNames.hasNext()) {
                    keyAttribute = fieldNames.next();
                    if (!keyAttribute.equals("AnioAnteriorPorcentaje") && !keyAttribute.equals("PeriodoAnteriorPorcentaje") && !keyAttribute.equals("CompromisoMontoPorcentaje") && !keyAttribute.equals("ActualPorcentaje")) {
                        valueAttribute = rowTemp.get(keyAttribute).asText();
                        attribute = objectMapper.createObjectNode();

                        if (!keyAttribute.equals("Dia")) {
                            color = rowTemp.get(keyAttribute).asDouble() > 0 ? "verde" : "rojo";
                            attribute.put("color", color);
                            attribute.put("cantidad", formart.format(new Double(valueAttribute)));

                        }

                        if (keyAttribute.equals("Dia")) {
                            attribute.put("color", "");
                            attribute.put("cantidad", "");
                            attribute.put("porcentaje", "0");
                        }

                        if (keyAttribute.equals("AnioAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("AnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("PeriodoAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("PeriodoAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("CompromisoMonto")) {
                            attribute.put("porcentaje", rowTemp.get("CompromisoMontoPorcentaje").asText());
                        }

                        if (keyAttribute.equals("ActualMonto")) {
                            attribute.put("porcentaje", rowTemp.get("ActualPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPeriodoAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoAnioAnteriorMonto")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                            attribute.put("porcentaje", rowTemp.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                        }

                        if (keyAttribute.equals("VsCompromisoPorcentaje") || keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje") || keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                            attribute.put("color", "");
                            attribute.put("cantidad", "0");
                        }

                        attribute.put("porcentaje", (attribute.get("porcentaje") == null ? "0" : attribute.get("porcentaje").asText()) + "%");

                        row.add(attribute);
                    }
                }
                detalle.add(row);
            }
            detalles.put("codigo", codigo);
            detalles.put("idService", "" + idServicio);
            detalles.set("detalle", detalle);
            detallesContenedor.add(detalles);
            response.set("indicadores", indicadores);
            response.set("detalles", detallesContenedor);
        }
        return response;
    }

    private ObjectNode columnsTotalesToArrayStrunctureCaptacion(String geografia, ObjectNode items) {
        ObjectNode response = objectMapper.createObjectNode();
        if (items != null) {
            ArrayNode row;
            ObjectNode attribute;
            String valueAttribute;
            String keyAttribute;
            String color = "";
            DecimalFormat formart = new DecimalFormat("###,###");
            ObjectNode rowTotales = items.with("Totales");
            row = objectMapper.createArrayNode();
            Iterator<String> fieldNames = rowTotales.fieldNames();
            while (fieldNames.hasNext()) {
                keyAttribute = fieldNames.next();
                if (!keyAttribute.equals("AnioAnteriorPorcentaje") && !keyAttribute.equals("PeriodoAnteriorPorcentaje") && !keyAttribute.equals("CompromisoMontoPorcentaje") && !keyAttribute.equals("ActualPorcentaje")) {
                    valueAttribute = rowTotales.get(keyAttribute).asText();
                    attribute = objectMapper.createObjectNode();

                    if (!keyAttribute.equals("Dia")) {
                        color = rowTotales.get(keyAttribute).asDouble() > 0 ? "verde" : "rojo";
                        attribute.put("color", color);
                        attribute.put("cantidad", formart.format(new Double(valueAttribute)));

                    }

                    if (keyAttribute.equals("Dia")) {
                        attribute.put("color", "");
                        attribute.put("cantidad", "");
                        attribute.put("porcentaje", "0");
                    }

                    if (keyAttribute.equals("AnioAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("AnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("PeriodoAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("PeriodoAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("CompromisoMonto")) {
                        attribute.put("porcentaje", rowTotales.get("CompromisoMontoPorcentaje").asText());
                    }

                    if (keyAttribute.equals("ActualMonto")) {
                        attribute.put("porcentaje", rowTotales.get("ActualPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPeriodoAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoAnioAnteriorMonto")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoPeriodoAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                        attribute.put("porcentaje", rowTotales.get("VsCompromisoAnioAnteriorPorcentaje").asText());
                    }

                    if (keyAttribute.equals("VsCompromisoPorcentaje") || keyAttribute.equals("VsCompromisoPeriodoAnteriorPorcentaje") || keyAttribute.equals("VsCompromisoAnioAnteriorPorcentaje")) {
                        attribute.put("color", "");
                        attribute.put("cantidad", "0");
                    }

                    attribute.put("porcentaje", (attribute.get("porcentaje") == null ? "0" : attribute.get("porcentaje").asText()) + "%");

                    row.add(attribute);
                }
            }
            response.put("ceco", geografia);
            response.set("valores", row);
        }
        return response;
    }

    private String codigoFromIdServicio(int idServicio) {
        switch (idServicio) {
            case 1:
                return "CONTRIBUCION";
            case 5:
                return "PERSONALES";
            case 6:
                return "CONSUMO";
            case 7:
                return "TAZ";
            case 8:
                return "INVERSIONES";
            case 9:
                return "GUARDADITO";
            case 15:
                return "NORMALIDAD";
            case 16:
                return "noabo";
            case 19:
                return "PAGO_SERVICIOS";
            case 17:
                return "DINERO EXPRESS";
            case 18:
                return "TRANS_INTERNACIONALES";
            case 20:
                return "CAMBIOS";
            case 25:
                return "VSF";
            case 26:
                return "VCN";
            case 27:
                return "PSF";
            case 28:
                return "PCN";
            case 29:
                return "DEX";
            case 30:
                return "VNA";
            case 31:
                return "VMA";
            case 32:
                return "PNA";
            case 33:
                return "PMA";
            default:
                return "Desconocido";
        }
    }

}
