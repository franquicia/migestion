/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Tuples {

    @JacksonXmlProperty(localName = "Tuple")
    private List<Tuple> Tuple;

    public List<Tuple> getTuple() {
        return Tuple;
    }

    public void setTuple(List<Tuple> Tuple) {
        this.Tuple = Tuple;
    }

    @Override
    public String toString() {
        return "Tuples{" + "Tuple=" + Tuple + '}';
    }

}
