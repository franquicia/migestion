/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class AxisInfo {

    @JacksonXmlProperty(localName = "HierarchyInfo")
    private HierarchyInfo HierarchyInfo;

    @JacksonXmlProperty(localName = "name")
    private String name;

    public HierarchyInfo getHierarchyInfo() {
        return HierarchyInfo;
    }

    public void setHierarchyInfo(HierarchyInfo HierarchyInfo) {
        this.HierarchyInfo = HierarchyInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AxisInfo{" + "HierarchyInfo=" + HierarchyInfo + ", name=" + name + '}';
    }

}
