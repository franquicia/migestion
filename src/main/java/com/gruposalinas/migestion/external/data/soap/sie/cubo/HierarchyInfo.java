/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class HierarchyInfo {

    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    @JacksonXmlProperty(localName = "UName")
    private UName UName;

    @JacksonXmlProperty(localName = "DisplayInfo")
    private DisplayInfo DisplayInfo;

    @JacksonXmlProperty(localName = "LName")
    private LName LName;

    @JacksonXmlProperty(localName = "LNum")
    private LNum LNum;

    @JacksonXmlProperty(localName = "Caption")
    private Caption Caption;

    public UName getUName() {
        return UName;
    }

    public void setUName(UName UName) {
        this.UName = UName;
    }

    public DisplayInfo getDisplayInfo() {
        return DisplayInfo;
    }

    public void setDisplayInfo(DisplayInfo DisplayInfo) {
        this.DisplayInfo = DisplayInfo;
    }

    public LName getLName() {
        return LName;
    }

    public void setLName(LName LName) {
        this.LName = LName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LNum getLNum() {
        return LNum;
    }

    public void setLNum(LNum LNum) {
        this.LNum = LNum;
    }

    public Caption getCaption() {
        return Caption;
    }

    public void setCaption(Caption Caption) {
        this.Caption = Caption;
    }

    @Override
    public String toString() {
        return "HierarchyInfo{" + "name=" + name + ", UName=" + UName + ", DisplayInfo=" + DisplayInfo + ", LName=" + LName + ", LNum=" + LNum + ", Caption=" + Caption + '}';
    }

}
