/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class Root {

    @JacksonXmlProperty(localName = "OlapInfo")
    private OlapInfo OlapInfo;

    @JacksonXmlProperty(localName = "Axes")
    private Axes Axes;

    @JacksonXmlProperty(localName = "CellData")
    private CellData CellData;

    public Axes getAxes() {
        return Axes;
    }

    public void setAxes(Axes Axes) {
        this.Axes = Axes;
    }

    public OlapInfo getOlapInfo() {
        return OlapInfo;
    }

    public void setOlapInfo(OlapInfo OlapInfo) {
        this.OlapInfo = OlapInfo;
    }

    public CellData getCellData() {
        return CellData;
    }

    public void setCellData(CellData CellData) {
        this.CellData = CellData;
    }

    @Override
    public String toString() {
        return "Root{" + "OlapInfo=" + OlapInfo + ", Axes=" + Axes + ", CellData=" + CellData + '}';
    }

}
