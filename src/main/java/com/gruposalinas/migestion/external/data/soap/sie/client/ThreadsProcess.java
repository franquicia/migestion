/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gruposalinas.migestion.resources.GTNConstantes;

/**
 *
 * @author cescobarh
 *//*
public class ThreadsProcess implements Runnable {

    private final static ObjectMapper objectMapper = new ObjectMapper();
    private static ArrayNode tablaValue = objectMapper.createArrayNode();
    private ObjectNode totalIndicadorCecoHijo;
    private int idService;
    private String fecha;
    private String geografia;
    private String nivel;
    private IndicadoresSIEBI indicadoresSIE = new IndicadoresSIEBI();

    public ThreadsProcess() {

    }

    public ThreadsProcess(int idService, String fecha, String geografia, String nivel) {
        this.idService = idService;
        this.fecha = fecha;
        this.geografia = geografia;
        this.nivel = nivel;

    }

    @Override
    public void run() {
        totalIndicadorCecoHijo = this.getResultadoTotalesIndicador(idService, fecha, geografia, nivel);
        tablaValue.add(totalIndicadorCecoHijo);

    }

    public ArrayNode getTablaValue() {
        return tablaValue;
    }

    public ObjectNode getResultadoTotalesIndicador(int idService, String fecha, String geografia, String nivel) {
        ObjectNode respuesta = objectMapper.createObjectNode();
        GTNConstantes.rubrosNegocio rubro = GTNConstantes.rubrosNegocio.getType(idService);
        switch (rubro) {
            case colocacionConsumo:
                respuesta = indicadoresSIE.consumoColocacion(fecha, geografia, nivel, idService, true);
                break;
            case colocacionPrestamosPer:
                respuesta = indicadoresSIE.personalesColocacion(fecha, geografia, nivel, idService, true);
                break;
            case colocacionTAZ:
                respuesta = indicadoresSIE.tazColocacion(fecha, geografia, nivel, idService, true);
                break;
            case captacionSaldoPlazo:
                respuesta = indicadoresSIE.saldoCaptacionPlazo(fecha, geografia, nivel, idService, true);
                break;
            case captacionSaldoVista:
                respuesta = indicadoresSIE.saldoCaptacionVista(fecha, geografia, nivel, idService, true);
                break;
            case carteraNormalidad:
                respuesta = indicadoresSIE.normalidad(fecha, geografia, nivel, idService, true);
                break;
            case negocioPagoServicios:
                respuesta = indicadoresSIE.transferenciasPagoServicios(fecha, geografia, nivel, idService, true);
                break;
            case negocioTransferenciaInter:
                respuesta = indicadoresSIE.transferenciasInternacionales(fecha, geografia, nivel, idService, true);
                break;
            case negocioTransferenciaDEX:
                respuesta = indicadoresSIE.tranferenciasDEX(fecha, geografia, nivel, idService, true);
                break;
            case negocioCambios:

                break;
            case contribucion:
                respuesta = indicadoresSIE.contribucion(fecha, geografia, nivel, idService, true);
                break;
            case carteraNuncaAbonadas:
                respuesta = indicadoresSIE.nAbonadasPendSurt(fecha, geografia, nivel, idService, true);
                break;
            case captacionSaldoFinalVista:
                respuesta = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "SF", idService, true);
                break;
            case captacionNetaVista:
                respuesta = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "CN", idService, true);
                break;
            case captacionSaldoFinalPlazo:
                respuesta = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "SF", idService, true);
                break;
            case captacionNetaPlazo:
                respuesta = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "CN", idService, true);
                break;
            case transferenciasDEX:
                respuesta = indicadoresSIE.tranferenciasDEX(fecha, geografia, nivel, idService, true);
                break;
            case numAperturasClientesNuevosVista:
                respuesta = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "NA", idService, true);
                break;
            case montAperturasClientesNuevosVista:
                respuesta = indicadoresSIE.saldoCaptacionVistaMetrica(fecha, geografia, nivel, "MA", idService, true);
                break;
            case numAperturasClientesNuevosPlazo:
                respuesta = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "NA", idService, true);
                break;
            case montAperturasClientesNuevosPlazo:
                respuesta = indicadoresSIE.saldoCaptacionPlazoMetrica(fecha, geografia, nivel, "MA", idService, true);
                break;
            default:
                respuesta = null;
                break;
        }
        return respuesta;
    }

}*/
