/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class Tuple {

    @JacksonXmlProperty(localName = "Member")
    private Member Member;

    public Member getMember() {
        return Member;
    }

    public void setMember(Member Member) {
        this.Member = Member;
    }

    @Override
    public String toString() {
        return "Tuple{" + "Member=" + Member + '}';
    }

}
