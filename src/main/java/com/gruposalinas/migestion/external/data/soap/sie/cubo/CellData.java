/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class CellData {

    @JacksonXmlProperty(localName = "Cell")
    private List<Cell> Cell;

    public List<Cell> getCell() {
        return Cell;
    }

    public void setCell(List<Cell> Cell) {
        this.Cell = Cell;
    }

    @Override
    public String toString() {
        return "CellData{" + "Cell=" + Cell + '}';
    }

}
