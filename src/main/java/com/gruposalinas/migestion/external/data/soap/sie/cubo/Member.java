/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class Member {

    @JacksonXmlProperty(localName = "Hierarchy")
    private String Hierarchy;

    @JacksonXmlProperty(localName = "UName")
    private String UName;

    @JacksonXmlProperty(localName = "DisplayInfo")
    private String DisplayInfo;

    @JacksonXmlProperty(localName = "LName")
    private String LName;

    @JacksonXmlProperty(localName = "LNum")
    private String LNum;

    @JacksonXmlProperty(localName = "Caption")
    private String Caption;

    public String getHierarchy() {
        return Hierarchy;
    }

    public void setHierarchy(String Hierarchy) {
        this.Hierarchy = Hierarchy;
    }

    public String getUName() {
        return UName;
    }

    public void setUName(String UName) {
        this.UName = UName;
    }

    public String getDisplayInfo() {
        return DisplayInfo;
    }

    public void setDisplayInfo(String DisplayInfo) {
        this.DisplayInfo = DisplayInfo;
    }

    public String getLName() {
        return LName;
    }

    public void setLName(String LName) {
        this.LName = LName;
    }

    public String getLNum() {
        return LNum;
    }

    public void setLNum(String LNum) {
        this.LNum = LNum;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String Caption) {
        this.Caption = Caption;
    }

    @Override
    public String toString() {
        return "Member{" + "Hierarchy=" + Hierarchy + ", UName=" + UName + ", DisplayInfo=" + DisplayInfo + ", LName=" + LName + ", LNum=" + LNum + ", Caption=" + Caption + '}';
    }

}
