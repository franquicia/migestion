/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 *
 * @author cescobarh
 */
public class TableView {

    private ObjectNode tableView;

    public ObjectNode getTableView() {
        return tableView;
    }

    public void setTableView(ObjectNode tableView) {
        this.tableView = tableView;
    }

    @Override
    public String toString() {
        return "TableView{" + "tableView=" + tableView + '}';
    }

}
