/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.migestion.external.data.soap.sie.cubo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author cescobarh
 */
public class CellInfo {

    @JacksonXmlProperty(localName = "FmtValue")
    private FmtValue FmtValue;

    @JacksonXmlProperty(localName = "CellOrdinal")
    private CellOrdinal CellOrdinal;

    @JacksonXmlProperty(localName = "Value")
    private Value Value;

    public FmtValue getFmtValue() {
        return FmtValue;
    }

    public void setFmtValue(FmtValue FmtValue) {
        this.FmtValue = FmtValue;
    }

    public CellOrdinal getCellOrdinal() {
        return CellOrdinal;
    }

    public void setCellOrdinal(CellOrdinal CellOrdinal) {
        this.CellOrdinal = CellOrdinal;
    }

    public Value getValue() {
        return Value;
    }

    public void setValue(Value Value) {
        this.Value = Value;
    }

    @Override
    public String toString() {
        return "CellInfo{" + "FmtValue=" + FmtValue + ", CellOrdinal=" + CellOrdinal + ", Value=" + Value + '}';
    }

}
