package com.gruposalinas.migestion.domain;

public class MysteryDTO {

    private String idCeco;
    private String nombreCeco;
    private int nivel;
    private int cantidadVideos;

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public void setCantidadVideos(int cantidadVideos) {
        this.cantidadVideos = cantidadVideos;
    }

    public int getCantidadVideos() {
        return this.cantidadVideos;
    }

}
