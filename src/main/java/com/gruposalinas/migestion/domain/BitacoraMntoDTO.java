package com.gruposalinas.migestion.domain;

public class BitacoraMntoDTO {

    private Integer idAmbito;
    private int idUsuario;
    private String idCeco;
    private String idFolio;
    private String fechaVisita;
    private String nombreProvee;
    private String tipoMantento;
    private String fechaSolicitud;
    private String horaEntrada;
    private String horaSalida;
    private String numPersonas;
    private String detalle;
    private String rutaProveedor;
    private String rutaGerente;
    private String userModif;
    private String fechaModif;

    public String getUserModif() {
        return userModif;
    }

    public void setUserModif(String userModif) {
        this.userModif = userModif;
    }

    public String getFechaModif() {
        return fechaModif;
    }

    public void setFechaModif(String fechaModif) {
        this.fechaModif = fechaModif;
    }

    public Integer getIdAmbito() {
        return idAmbito;
    }

    public void setIdAmbito(Integer idAmbito) {
        this.idAmbito = idAmbito;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUusuario) {
        this.idUsuario = idUusuario;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(String ifFolio) {
        this.idFolio = ifFolio;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public String getNombreProvee() {
        return nombreProvee;
    }

    public void setNombreProvee(String nombreProvee) {
        this.nombreProvee = nombreProvee;
    }

    public String getTipoMantento() {
        return tipoMantento;
    }

    public void setTipoMantento(String tipoMantento) {
        this.tipoMantento = tipoMantento;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getNumPersonas() {
        return numPersonas;
    }

    public void setNumPersonas(String numPersonas) {
        this.numPersonas = numPersonas;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getRutaProveedor() {
        return rutaProveedor;
    }

    public void setRutaProveedor(String rutaProveedor) {
        this.rutaProveedor = rutaProveedor;
    }

    public String getRutaGerente() {
        return rutaGerente;
    }

    public void setRutaGerente(String rutaGerente) {
        this.rutaGerente = rutaGerente;
    }
}
