package com.gruposalinas.migestion.domain;

public class IncidenciasDTO {

    private int idTipo;
    private String servicio;
    private String ubicacion;
    private String incidencia;
    private String plantilla;
    private String status;

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return "IncidenciasDTO [idTipo=" + idTipo + ",servicio=" + servicio + ",ubicacion=" + ubicacion + ",incidencia=" + incidencia + ",plantilla=" + plantilla + ",status=" + status + "]";
    }

}
