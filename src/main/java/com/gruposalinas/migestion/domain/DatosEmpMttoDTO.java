package com.gruposalinas.migestion.domain;

public class DatosEmpMttoDTO {

    private int idUsuario;
    private String nombre;
    private String descripcion;
    private int ceco;
    private String telefono;
    private int idPuesto;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCeco() {
        return ceco;
    }

    public void setCeco(int ceco) {
        this.ceco = ceco;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    @Override
    public String toString() {
        return "DatosEmpMttoDTO [idUsuario=" + idUsuario + ", nombre=" + nombre + ", descripcion=" + descripcion
                + ", ceco=" + ceco + ", telefono=" + telefono + ", idPuesto=" + idPuesto + "]";
    }

}
