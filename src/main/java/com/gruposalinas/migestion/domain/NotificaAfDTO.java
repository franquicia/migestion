package com.gruposalinas.migestion.domain;

public class NotificaAfDTO {

    private int idNotifica;
    private String ceco;
    private String compromisoP;
    private String avanceP;
    private String compromisoA;
    private String avanceA;
    private String fecha;

    public int getIdNotifica() {
        return idNotifica;
    }

    public void setIdNotifica(int idNotifica) {
        this.idNotifica = idNotifica;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getCompromisoP() {
        return compromisoP;
    }

    public void setCompromisoP(String compromisoP) {
        this.compromisoP = compromisoP;
    }

    public String getAvanceP() {
        return avanceP;
    }

    public void setAvanceP(String avanceP) {
        this.avanceP = avanceP;
    }

    public String getCompromisoA() {
        return compromisoA;
    }

    public void setCompromisoA(String compromisoA) {
        this.compromisoA = compromisoA;
    }

    public String getAvanceA() {
        return avanceA;
    }

    public void setAvanceA(String avanceA) {
        this.avanceA = avanceA;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

}
