package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;

public class QuizListaChecklistDTO {

    @SerializedName("idRespuesta")
    private String idRespuesta;
    @SerializedName("respuesta")
    private String respuesta;
    @SerializedName("ponderacion")
    private String ponderacion;
    @SerializedName("tipoRespuesta")
    private String tipoRespuesta;
    @SerializedName("comentario")
    private ListaCheckListComentario comentario;
    @SerializedName("evidencia")
    private ListaCheckListEvidencia evidencia;
    private String comentarioRespuesta;
    private String evidenciaEspuesta;

    public String getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(String idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(String ponderacion) {
        this.ponderacion = ponderacion;
    }

    public String getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(String tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public ListaCheckListComentario getComentario() {
        return comentario;
    }

    public void setComentario(ListaCheckListComentario comentario) {
        this.comentario = comentario;
    }

    public ListaCheckListEvidencia getEvidencia() {
        return evidencia;
    }

    public void setEvidencia(ListaCheckListEvidencia evidencia) {
        this.evidencia = evidencia;
    }

    public String getComentarioRespuesta() {
        return comentarioRespuesta;
    }

    public void setComentarioRespuesta(String comentarioRespuesta) {
        this.comentarioRespuesta = comentarioRespuesta;
    }

    public String getEvidenciaEspuesta() {
        return evidenciaEspuesta;
    }

    public void setEvidenciaEspuesta(String evidenciaEspuesta) {
        this.evidenciaEspuesta = evidenciaEspuesta;
    }

    class ListaCheckListEvidencia {

        @SerializedName("idComentario")
        private String idComentario;
        @SerializedName("comentarioPregunta")
        private String comentarioPregunta;

        public String getIdComentario() {
            return idComentario;
        }

        public void setIdComentario(String idComentario) {
            this.idComentario = idComentario;
        }

        public String getComentarioPregunta() {
            return comentarioPregunta;
        }

        public void setComentarioPregunta(String comentarioPregunta) {
            this.comentarioPregunta = comentarioPregunta;
        }
    }

    class ListaCheckListComentario {

        @SerializedName("idEvidencia")
        private String idEvidencia;
        @SerializedName("muestra")
        private String muestra;
        @SerializedName("comentarioEvidencia")
        private String comentarioEvidencia;

        public String getIdEvidencia() {
            return idEvidencia;
        }

        public void setIdEvidencia(String idEvidencia) {
            this.idEvidencia = idEvidencia;
        }

        public String getMuestra() {
            return muestra;
        }

        public void setMuestra(String muestra) {
            this.muestra = muestra;
        }

        public String getComentarioEvidencia() {
            return comentarioEvidencia;
        }

        public void setComentarioEvidencia(String comentarioEvidencia) {
            this.comentarioEvidencia = comentarioEvidencia;
        }
    }
}
