package com.gruposalinas.migestion.domain;

public class DetalleCertificacionRHDTO {

    private String nombres;
    private String apellidoPat;
    private String apellidoMat;
    private String nombreCurso;
    private String empleado;
    private String indicador;
    private String puesto;
    private String calificacion;

    public DetalleCertificacionRHDTO() {
        super();
    }

    public DetalleCertificacionRHDTO(String nombres, String apellidoPat, String apellidoMat, String nombreCurso,
            String empleado, String indicador) {
        super();
        this.nombres = nombres;
        this.apellidoPat = apellidoPat;
        this.apellidoMat = apellidoMat;
        this.nombreCurso = nombreCurso;
        this.empleado = empleado;
        this.indicador = indicador;
        this.calificacion = calificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPat() {
        return apellidoPat;
    }

    public void setApellidoPat(String apellidoPat) {
        this.apellidoPat = apellidoPat;
    }

    public String getApellidoMat() {
        return apellidoMat;
    }

    public void setApellidoMat(String apellidoMat) {
        this.apellidoMat = apellidoMat;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getIndicador() {
        return indicador;
    }

    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "DetalleCertificacionRHDTO [nombres=" + nombres + ", apellidoPat=" + apellidoPat + ", apellidoMat="
                + apellidoMat + ", nombreCurso=" + nombreCurso + ", empleado=" + empleado + ", indicador=" + indicador
                + ", puesto=" + puesto + ", calificacion=" + calificacion + "]";
    }

}
