package com.gruposalinas.migestion.domain;

public class FilasDTO {

    private int idFila;
    private int numEconomico;
    private int idGrupo;

    public int getIdFila() {
        return idFila;
    }

    public void setIdFila(int idFila) {
        this.idFila = idFila;
    }

    public int getNumEconomico() {
        return numEconomico;
    }

    public void setNumEconomico(int numEconomico) {
        this.numEconomico = numEconomico;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String toString() {
        return "FilasDTO [idFila=" + idFila + ",numEconomico=" + numEconomico + ",idGrupo=" + idGrupo + "]";
    }

}
