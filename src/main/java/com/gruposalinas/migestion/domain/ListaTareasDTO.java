package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;

public class ListaTareasDTO {

    @SerializedName("PID")
    private String pId;
    @SerializedName("PName")
    private String pName;
    @SerializedName("Start")
    private String start;
    @SerializedName("Due")
    private String due;
    @SerializedName("PType")
    private String pType;
    @SerializedName("PTypeDesc")
    private String pTypeDesc;
    @SerializedName("Comments")
    private String comments;
    @SerializedName("FAprobacion")
    private String fAprobacion;
    @SerializedName("fTermino")
    private String fTermino;
    @SerializedName("Attachments")
    private String attachments;
    @SerializedName("Priority")
    private String priority;
    @SerializedName("AssignedTo")
    private String assignedTo;
    @SerializedName("StatusCd")
    private String statusCd;
    @SerializedName("StDesc")
    private String stDesc;
    @SerializedName("descPuesto")
    private String descPuesto;
    @SerializedName("subordinado")
    private boolean subordinado;
    @SerializedName("nivel")
    private String nivel;

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getpTypeDesc() {
        return pTypeDesc;
    }

    public void setpTypeDesc(String pTypeDesc) {
        this.pTypeDesc = pTypeDesc;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getfAprobacion() {
        return fAprobacion;
    }

    public void setfAprobacion(String fAprobacion) {
        this.fAprobacion = fAprobacion;
    }

    public String getfTermino() {
        return fTermino;
    }

    public void setfTermino(String fTermino) {
        this.fTermino = fTermino;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getStDesc() {
        return stDesc;
    }

    public void setStDesc(String stDesc) {
        this.stDesc = stDesc;
    }

    public String getDescPuesto() {
        return descPuesto;
    }

    public void setDescPuesto(String descPuesto) {
        this.descPuesto = descPuesto;
    }

    public boolean getSubordinado() {
        return subordinado;
    }

    public void setSubordinado(boolean subordinado) {
        this.subordinado = subordinado;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
}
