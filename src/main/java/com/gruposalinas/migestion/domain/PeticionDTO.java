package com.gruposalinas.migestion.domain;

public class PeticionDTO {

    private String origenPeticion;
    private String fechaPeticion;
    private int conteoPeticion;

    public String getOrigenPeticion() {
        return origenPeticion;
    }

    public void setOrigenPeticion(String origenPeticion) {
        this.origenPeticion = origenPeticion;
    }

    public String getFechaPeticion() {
        return fechaPeticion;
    }

    public void setFechaPeticion(String fechaPeticion) {
        this.fechaPeticion = fechaPeticion;
    }

    public int getConteoPeticion() {
        return conteoPeticion;
    }

    public void setConteoPeticion(int conteoPeticion) {
        this.conteoPeticion = conteoPeticion;
    }

}
