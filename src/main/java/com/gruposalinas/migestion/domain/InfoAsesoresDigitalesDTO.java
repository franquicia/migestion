package com.gruposalinas.migestion.domain;

public class InfoAsesoresDigitalesDTO {

    private int id;
    private String ceco;
    private String compromiso_personales;
    private String avance_personales;
    private String compromiso_activaciones;
    private String avance_activaciones;
    private String fecha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getCompromiso_personales() {
        return compromiso_personales;
    }

    public void setCompromiso_personales(String compromiso_personales) {
        this.compromiso_personales = compromiso_personales;
    }

    public String getAvance_personales() {
        return avance_personales;
    }

    public void setAvance_personales(String avance_personales) {
        this.avance_personales = avance_personales;
    }

    public String getCompromiso_activaciones() {
        return compromiso_activaciones;
    }

    public void setCompromiso_activaciones(String compromiso_activaciones) {
        this.compromiso_activaciones = compromiso_activaciones;
    }

    public String getAvance_activaciones() {
        return avance_activaciones;
    }

    public void setAvance_activaciones(String avance_activaciones) {
        this.avance_activaciones = avance_activaciones;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

}
