package com.gruposalinas.migestion.domain;

public class DepuraActFijoDTO {

    private int idDepAct;
    private int idusuario;
    private String ceco;
    private String ubicacion;
    private String involucrados;
    private String razonsoc;
    private String domicilio;
    private String responsable;
    private String fecha;
    private double precio;
    private int idactivofijo;
    private String serie;
    private String marca;
    private String placa;
    private String descripcion;
    private String rfc;
    private String testigo;
    private int idEvi;
    private String nombreArc;
    private String ruta;
    private String desc;
    private String periodo;

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getTestigo() {
        return testigo;
    }

    public void setTestigo(String testigo) {
        this.testigo = testigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdactivofijo() {
        return idactivofijo;
    }

    public void setIdactivofijo(int idactivofijo) {
        this.idactivofijo = idactivofijo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getIdDepAct() {
        return idDepAct;
    }

    public void setIdDepAct(int idDepAct) {
        this.idDepAct = idDepAct;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getInvolucrados() {
        return involucrados;
    }

    public void setInvolucrados(String involucrados) {
        this.involucrados = involucrados;
    }

    public String getRazonsoc() {
        return razonsoc;
    }

    public void setRazonsoc(String razonsoc) {
        this.razonsoc = razonsoc;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getIdEvi() {
        return idEvi;
    }

    public void setIdEvi(int idEvi) {
        this.idEvi = idEvi;
    }

    public String getNombreArc() {
        return nombreArc;
    }

    public void setNombreArc(String nombreArc) {
        this.nombreArc = nombreArc;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "DepuraActFijoDTO [idDepAct=" + idDepAct + ", idusuario=" + idusuario + ", ceco=" + ceco + ", ubicacion="
                + ubicacion + ", involucrados=" + involucrados + ", razonsoc=" + razonsoc + ", domicilio=" + domicilio
                + ", responsable=" + responsable + ", fecha=" + fecha + ", precio=" + precio + ", idactivofijo="
                + idactivofijo + ", serie=" + serie + ", marca=" + marca + ", placa=" + placa + ", descripcion="
                + descripcion + ", rfc=" + rfc + ", testigo=" + testigo + ", idEvi=" + idEvi + ", nombreArc="
                + nombreArc + ", ruta=" + ruta + ", desc=" + desc + ", periodo=" + periodo + "]";
    }

}
