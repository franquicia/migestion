package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ChecklistDTO {

    @SerializedName("idPregunta")
    private String idPregunta;
    @SerializedName("numPregunta")
    private int numPregunta;
    @SerializedName("tipoPregunta")
    private String tipoPregunta;
    @SerializedName("pregunta")
    private String pregunta;
    @SerializedName("posible_respuesta")
    private String posible_respuesta;
    @SerializedName("comentarioPregunta")
    private String comentarioPregunta;
    @SerializedName("url_reference")
    private String url_reference;
    @SerializedName("activo")
    private int activo;
    @SerializedName("idCheckList")
    private int idCheckList;
    private List<String> respList;
    @SerializedName("valMax")
    private long valMax;
    @SerializedName("valMin")
    private long valMin;

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getNumPregunta() {
        return numPregunta;
    }

    public void setNumPregunta(int numPregunta) {
        this.numPregunta = numPregunta;
    }

    public String getTipoPregunta() {
        return tipoPregunta;
    }

    public void setTipoPregunta(String tipoPregunta) {
        this.tipoPregunta = tipoPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getPosible_respuesta() {
        return posible_respuesta;
    }

    public void setPosible_respuesta(String posible_respuesta) {
        this.posible_respuesta = posible_respuesta;
    }

    public String getComentarioPregunta() {
        return comentarioPregunta;
    }

    public void setComentarioPregunta(String comentarioPregunta) {
        this.comentarioPregunta = comentarioPregunta;
    }

    public String getUrl_reference() {
        return url_reference;
    }

    public void setUrl_reference(String url_reference) {
        this.url_reference = url_reference;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(int idCheckList) {
        this.idCheckList = idCheckList;
    }

    public List<String> getRespList() {
        return respList;
    }

    public void setRespList(List<String> respList) {
        this.respList = respList;
    }

    public long getValMax() {
        return valMax;
    }

    public void setValMax(long valMax) {
        this.valMax = valMax;
    }

    public long getValMin() {
        return valMin;
    }

    public void setValMin(long valMin) {
        this.valMin = valMin;
    }
}
