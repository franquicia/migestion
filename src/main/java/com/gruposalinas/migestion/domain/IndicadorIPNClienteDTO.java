package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class IndicadorIPNClienteDTO {

    @SerializedName("IDCeco")
    private Integer fiCeco;

    @SerializedName("Ceco")
    private String fcCeco;

    @SerializedName("IDTerritorio")
    private Integer fiTerritorio;

    @SerializedName("IDZona")
    private Integer fiZona;

    @SerializedName("IDRegion")
    private Integer fiRegion;

    @SerializedName("IDSucursal")
    private Integer fiSucursal;

    @SerializedName("NumeroEconomico")
    private Integer fiNumEconomico;

    @SerializedName("Sucursal")
    private String fcSucursal;

    @SerializedName("IDPeriodo")
    private Integer fiQ;

    @SerializedName("Periodo")
    private String fcQ;

    @SerializedName("Anio")
    private Integer fiAnio;

    @SerializedName("NumeroEvaluacion")
    private Integer fiNumEval;

    @SerializedName("Promotor")
    private Integer fiPromotor;

    @SerializedName("Pasivo")
    private Integer fiPasivo;

    @SerializedName("Detractivo")
    private Integer fiDetractivo;

    @SerializedName("IPN")
    private BigDecimal fiIPN;

    @SerializedName("UsuarioModificacion")
    private transient String fcUsuarioMod;

    @SerializedName("FechaModificacion")
    private transient Timestamp fdFechaMod;

    public Integer getFiCeco() {
        return fiCeco;
    }

    public void setFiCeco(Integer fiCeco) {
        this.fiCeco = fiCeco;
    }

    public String getFcCeco() {
        return fcCeco;
    }

    public void setFcCeco(String fcCeco) {
        this.fcCeco = fcCeco;
    }

    public Integer getFiTerritorio() {
        return fiTerritorio;
    }

    public void setFiTerritorio(Integer fiTerritorio) {
        this.fiTerritorio = fiTerritorio;
    }

    public Integer getFiZona() {
        return fiZona;
    }

    public void setFiZona(Integer fiZona) {
        this.fiZona = fiZona;
    }

    public Integer getFiRegion() {
        return fiRegion;
    }

    public void setFiRegion(Integer fiRegion) {
        this.fiRegion = fiRegion;
    }

    public Integer getFiSucursal() {
        return fiSucursal;
    }

    public void setFiSucursal(Integer fiSucursal) {
        this.fiSucursal = fiSucursal;
    }

    public Integer getFiNumEconomico() {
        return fiNumEconomico;
    }

    public void setFiNumEconomico(Integer fiNumEconomico) {
        this.fiNumEconomico = fiNumEconomico;
    }

    public String getFcSucursal() {
        return fcSucursal;
    }

    public void setFcSucursal(String fcSucursal) {
        this.fcSucursal = fcSucursal;
    }

    public Integer getFiNumEval() {
        return fiNumEval;
    }

    public Integer getFiQ() {
        return fiQ;
    }

    public void setFiQ(Integer fiQ) {
        this.fiQ = fiQ;
    }

    public String getFcQ() {
        return fcQ;
    }

    public void setFcQ(String fcQ) {
        this.fcQ = fcQ;
    }

    public Integer getFiAnio() {
        return fiAnio;
    }

    public void setFiAnio(Integer fiAnio) {
        this.fiAnio = fiAnio;
    }

    public void setFiNumEval(Integer fiNumEval) {
        this.fiNumEval = fiNumEval;
    }

    public Integer getFiPromotor() {
        return fiPromotor;
    }

    public void setFiPromotor(Integer fiPromotor) {
        this.fiPromotor = fiPromotor;
    }

    public Integer getFiPasivo() {
        return fiPasivo;
    }

    public void setFiPasivo(Integer fiPasivo) {
        this.fiPasivo = fiPasivo;
    }

    public Integer getFiDetractivo() {
        return fiDetractivo;
    }

    public void setFiDetractivo(Integer fiDetractivo) {
        this.fiDetractivo = fiDetractivo;
    }

    public BigDecimal getFiIPN() {
        return fiIPN;
    }

    public void setFiIPN(BigDecimal fiIPN) {
        this.fiIPN = fiIPN;
    }

    public String getFcUsuarioMod() {
        return fcUsuarioMod;
    }

    public void setFcUsuarioMod(String fcUsuarioMod) {
        this.fcUsuarioMod = fcUsuarioMod;
    }

    public Timestamp getFdFechaMod() {
        return fdFechaMod;
    }

    public void setFdFechaMod(Timestamp fdFechaMod) {
        this.fdFechaMod = fdFechaMod;
    }

    @Override
    public String toString() {
        return "IndicadorIPNClienteDTO{" + "fiTerritorio=" + fiTerritorio + ", fiZona=" + fiZona + ", fiRegion=" + fiRegion + ", fiSucursal=" + fiSucursal + ", fiNumEconomico=" + fiNumEconomico + ", fcSucursal=" + fcSucursal + ", fiQ=" + fiQ + ", fcQ=" + fcQ + ", fiAnio=" + fiAnio + ", fiNumEval=" + fiNumEval + ", fiPromotor=" + fiPromotor + ", fiPasivo=" + fiPasivo + ", fiDetractivo=" + fiDetractivo + ", fiIPN=" + fiIPN + ", fcUsuarioMod=" + fcUsuarioMod + ", fdFechaMod=" + fdFechaMod + '}';
    }

}
