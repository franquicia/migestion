package com.gruposalinas.migestion.domain;

public class FranquiciaDataDTO {

    String claveUbicacion;
    String nombre;
    int idDashboard;
    String nombreDashboard;
    int idAlcance;
    String nombreAlcance;
    String nombreDimension;
    String nombreRubro;
    String descripcionRubro;
    String respuestaTexto;
    int ponderacionEvaluador;
    int ponderacionObtenido;
    int ponderacionImperdonable;
    int idEncuesta;
    int idEncuestaContestada;

    public String getClaveUbicacion() {
        return claveUbicacion;
    }

    public void setClaveUbicacion(String claveUbicacion) {
        this.claveUbicacion = claveUbicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(int idDashboard) {
        this.idDashboard = idDashboard;
    }

    public String getNombreDashboard() {
        return nombreDashboard;
    }

    public void setNombreDashboard(String nombreDashboard) {
        this.nombreDashboard = nombreDashboard;
    }

    public int getIdAlcance() {
        return idAlcance;
    }

    public void setIdAlcance(int idAlcance) {
        this.idAlcance = idAlcance;
    }

    public String getNombreAlcance() {
        return nombreAlcance;
    }

    public void setNombreAlcance(String nombreAlcance) {
        this.nombreAlcance = nombreAlcance;
    }

    public String getNombreDimension() {
        return nombreDimension;
    }

    public void setNombreDimension(String nombreDimension) {
        this.nombreDimension = nombreDimension;
    }

    public String getNombreRubro() {
        return nombreRubro;
    }

    public void setNombreRubro(String nombreRubro) {
        this.nombreRubro = nombreRubro;
    }

    public String getDescripcionRubro() {
        return descripcionRubro;
    }

    public void setDescripcionRubro(String descripcionRubro) {
        this.descripcionRubro = descripcionRubro;
    }

    public String getRespuestaTexto() {
        return respuestaTexto;
    }

    public void setRespuestaTexto(String respuestaTexto) {
        this.respuestaTexto = respuestaTexto;
    }

    public int getPonderacionEvaluador() {
        return ponderacionEvaluador;
    }

    public void setPonderacionEvaluador(int ponderacionEvaluador) {
        this.ponderacionEvaluador = ponderacionEvaluador;
    }

    public int getPonderacionObtenido() {
        return ponderacionObtenido;
    }

    public void setPonderacionObtenido(int ponderacionObtenido) {
        this.ponderacionObtenido = ponderacionObtenido;
    }

    public int getPonderacionImperdonable() {
        return ponderacionImperdonable;
    }

    public void setPonderacionImperdonable(int ponderacionImperdonable) {
        this.ponderacionImperdonable = ponderacionImperdonable;
    }

    public int getIdEncuesta() {
        return idEncuesta;
    }

    public void setIdEncuesta(int idEncuesta) {
        this.idEncuesta = idEncuesta;
    }

    public int getIdEncuestaContestada() {
        return idEncuestaContestada;
    }

    public void setIdEncuestaContestada(int idEncuestaContestada) {
        this.idEncuestaContestada = idEncuestaContestada;
    }

    @Override
    public String toString() {
        return "FranquiciaData [claveUbicacion=" + claveUbicacion + ", nombre=" + nombre + ", idDashboard="
                + idDashboard + ", nombreDashboard=" + nombreDashboard + ", idAlcance=" + idAlcance
                + ", nombreAlcance=" + nombreAlcance + ", nombreDimension=" + nombreDimension + ", nombreRubro="
                + nombreRubro + ", descripcionRubro=" + descripcionRubro + ", respuestaTexto=" + respuestaTexto
                + ", ponderacionEvaluador=" + ponderacionEvaluador + ", ponderacionObtenido=" + ponderacionObtenido
                + ", ponderacionImperdonable=" + ponderacionImperdonable + ", idEncuesta=" + idEncuesta
                + ", idEncuestaContestada=" + idEncuestaContestada + "]";
    }

}
