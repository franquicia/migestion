package com.gruposalinas.migestion.domain;

public class BlocDTO {

    private int idBloc;
    private String titulo;
    private String nota;
    private int idUsuario;
    private String fechaCreacion;

    public int getIdBloc() {
        return idBloc;
    }

    public void setIdBloc(int idBloc) {
        this.idBloc = idBloc;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

}
