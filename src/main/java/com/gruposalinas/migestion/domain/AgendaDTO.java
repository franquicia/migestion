package com.gruposalinas.migestion.domain;

public class AgendaDTO {

    private int idAgenda;
    private String idGoogle;
    private String titulo;
    private String evento;
    private int periodo;
    private int alerta;
    private String horarioIni;
    private String horarioFin;
    private int idUsuario;
    private String correo;

    public int getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(int idAgenda) {
        this.idAgenda = idAgenda;
    }

    public String getIdGoogle() {
        return idGoogle;
    }

    public void setIdGoogle(String idGoogle) {
        this.idGoogle = idGoogle;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public String getHorarioIni() {
        return horarioIni;
    }

    public void setHorarioIni(String horarioIni) {
        this.horarioIni = horarioIni;
    }

    public String getHorarioFin() {
        return horarioFin;
    }

    public void setHorarioFin(String horarioFin) {
        this.horarioFin = horarioFin;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getAlerta() {
        return alerta;
    }

    public void setAlerta(int alerta) {
        this.alerta = alerta;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "AgendaDTO [idAgenda=" + idAgenda + ", idGoogle=" + idGoogle + ", titulo=" + titulo + ", evento="
                + evento + ", periodo=" + periodo + ", alerta=" + alerta + ", horarioIni=" + horarioIni
                + ", horarioFin=" + horarioFin + ", idUsuario=" + idUsuario + "]";
    }

}
