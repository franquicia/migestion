package com.gruposalinas.migestion.domain;

public class DatosRHDTO {

    private int plantilla;
    private int activosPurosPlantilla;
    private int vacantesPurasPlantilla;

    private int cubrimientoAcumulado;
    private int activosPuros;
    private int vacantesPuras;

    private int rotacionAcumulada;
    private int activos;
    private int bajasAcumuladas;

    private int certificacionTotal;
    private int certificados;
    private int noCertificados;

    private int tendenciaRotacion;

    private int garantiaTotal;
    private int garantizados;
    private int noGarantizados;

    private String strOpcion;

    public int getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(int plantilla) {
        this.plantilla = plantilla;
    }

    public int getActivosPurosPlantilla() {
        return activosPurosPlantilla;
    }

    public void setActivosPurosPlantilla(int activosPurosPlantilla) {
        this.activosPurosPlantilla = activosPurosPlantilla;
    }

    public int getVacantesPurasPlantilla() {
        return vacantesPurasPlantilla;
    }

    public void setVacantesPurasPlantilla(int vacantesPurasPlantilla) {
        this.vacantesPurasPlantilla = vacantesPurasPlantilla;
    }

    public int getCubrimientoAcumulado() {
        return cubrimientoAcumulado;
    }

    public void setCubrimientoAcumulado(int cubrimientoAcumulado) {
        this.cubrimientoAcumulado = cubrimientoAcumulado;
    }

    public int getActivosPuros() {
        return activosPuros;
    }

    public void setActivosPuros(int activosPuros) {
        this.activosPuros = activosPuros;
    }

    public int getVacantesPuras() {
        return vacantesPuras;
    }

    public void setVacantesPuras(int vacantesPuras) {
        this.vacantesPuras = vacantesPuras;
    }

    public int getRotacionAcumulada() {
        return rotacionAcumulada;
    }

    public void setRotacionAcumulada(int rotacionAcumulada) {
        this.rotacionAcumulada = rotacionAcumulada;
    }

    public int getActivos() {
        return activos;
    }

    public void setActivos(int activos) {
        this.activos = activos;
    }

    public int getBajasAcumuladas() {
        return bajasAcumuladas;
    }

    public void setBajasAcumuladas(int bajasAcumuladas) {
        this.bajasAcumuladas = bajasAcumuladas;
    }

    public int getCertificacionTotal() {
        return certificacionTotal;
    }

    public void setCertificacionTotal(int certificacionTotal) {
        this.certificacionTotal = certificacionTotal;
    }

    public int getCertificados() {
        return certificados;
    }

    public void setCertificados(int certificados) {
        this.certificados = certificados;
    }

    public int getNoCertificados() {
        return noCertificados;
    }

    public void setNoCertificados(int noCertificados) {
        this.noCertificados = noCertificados;
    }

    public int getTendenciaRotacion() {
        return tendenciaRotacion;
    }

    public void setTendenciaRotacion(int tendenciaRotacion) {
        this.tendenciaRotacion = tendenciaRotacion;
    }

    public String getStrOpcion() {
        return strOpcion;
    }

    public void setStrOpcion(String strOpcion) {
        this.strOpcion = strOpcion;
    }

    public int getGarantiaTotal() {
        return garantiaTotal;
    }

    public void setGarantiaTotal(int garantiaTotal) {
        this.garantiaTotal = garantiaTotal;
    }

    public int getGarantizados() {
        return garantizados;
    }

    public void setGarantizados(int garantizados) {
        this.garantizados = garantizados;
    }

    public int getNoGarantizados() {
        return noGarantizados;
    }

    public void setNoGarantizados(int noGarantizados) {
        this.noGarantizados = noGarantizados;
    }

    @Override
    public String toString() {
        return "DatosRHDTO [plantilla=" + plantilla + ", activosPurosPlantilla=" + activosPurosPlantilla
                + ", vacantesPurasPlantilla=" + vacantesPurasPlantilla + ", cubrimientoAcumulado="
                + cubrimientoAcumulado + ", activosPuros=" + activosPuros + ", vacantesPuras=" + vacantesPuras
                + ", rotacionAcumulada=" + rotacionAcumulada + ", activos=" + activos + ", bajasAcumuladas="
                + bajasAcumuladas + ", certificacionTotal=" + certificacionTotal + ", certificados=" + certificados
                + ", noCertificados=" + noCertificados + ", tendenciaRotacion=" + tendenciaRotacion + ", garantiaTotal="
                + garantiaTotal + ", garantizados=" + garantizados + ", noGarantizados=" + noGarantizados
                + ", strOpcion=" + strOpcion + "]";
    }

}
