package com.gruposalinas.migestion.domain;

public class ActivoFijoDTO {

    private int idActivoFijo;
    private int depActivoFijo;
    private String placa;
    private String descripcion;
    private String marca;
    private String serie;

    private String ceco;

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public Integer getIdActivoFijo() {
        return idActivoFijo;
    }

    public void setIdActivoFijo(int idActivoFijo) {
        this.idActivoFijo = idActivoFijo;
    }

    public int getDepActivoFijo() {
        return depActivoFijo;
    }

    public void setDepActivoFijo(int depActivoFijo) {
        this.depActivoFijo = depActivoFijo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    @Override
    public String toString() {
        return "ActivoFijoDTO [idActivoFijo=" + idActivoFijo + ", depActivoFijo=" + depActivoFijo + ", placa=" + placa
                + ", descripcion=" + descripcion + ", marca=" + marca + ", serie=" + serie + ", ceco=" + ceco + "]";
    }

}
