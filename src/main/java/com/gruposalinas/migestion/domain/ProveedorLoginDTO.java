package com.gruposalinas.migestion.domain;

public class ProveedorLoginDTO {

    private int idpass;
    private int idProveedor;
    private String passw;
    private String periodo;

    public int getIdpass() {
        return idpass;
    }

    public void setIdpass(int idpass) {
        this.idpass = idpass;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "ProveedorLoginDTO [idpass=" + idpass + ", idProveedor=" + idProveedor + ", passw=" + passw
                + ", periodo=" + periodo + "]";
    }

}
