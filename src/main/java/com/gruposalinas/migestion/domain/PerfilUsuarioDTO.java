package com.gruposalinas.migestion.domain;

public class PerfilUsuarioDTO {

    private int idUsuario;
    private int idPerfil;
    private String idActor;
    private String bandNvoEsq;
    private String tipoProyecto;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getIdActor() {
        return idActor;
    }

    public void setIdActor(String idActor) {
        this.idActor = idActor;
    }

    public String getBandNvoEsq() {
        return bandNvoEsq;
    }

    public void setBandNvoEsq(String bandNvoEsq) {
        this.bandNvoEsq = bandNvoEsq;
    }

    public String getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    @Override
    public String toString() {
        return "PerfilUsuarioDTO [idUsuario=" + idUsuario + ", idPerfil=" + idPerfil + ", idActor=" + idActor
                + ", bandNvoEsq=" + bandNvoEsq + ", tipoProyecto=" + tipoProyecto + "]";
    }

}
