package com.gruposalinas.migestion.domain;

public class ProgLimpiezaDTO {

    private int idProg;
    private int idUsuario;
    private String ceco;
    private String area;
    private String articulo;
    private String frecuencia;
    private String horario;
    private String responsable;
    private String supervisor;
    private String periodo;
    private int bandera;
    private int idArea;

    public int getIdProg() {
        return idProg;
    }

    public void setIdProg(int idProg) {
        this.idProg = idProg;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getBandera() {
        return bandera;
    }

    public void setBandera(int bandera) {
        this.bandera = bandera;
    }

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    @Override
    public String toString() {
        return "ProgLimpiezaDTO [idProg=" + idProg + ", idUsuario=" + idUsuario + ", ceco=" + ceco + ", area=" + area
                + ", articulo=" + articulo + ", frecuencia=" + frecuencia + ", horario=" + horario + ", responsable="
                + responsable + ", supervisor=" + supervisor + ", periodo=" + periodo + ", bandera=" + bandera
                + ", idArea=" + idArea + "]";
    }

}
