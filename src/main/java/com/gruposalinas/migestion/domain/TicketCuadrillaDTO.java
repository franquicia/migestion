package com.gruposalinas.migestion.domain;

public class TicketCuadrillaDTO {

    private int idtkCuadri;
    private int idCuadrilla;
    private int idProveedor;
    private String ticket;
    private int zona;
    private String liderCua;
    private int status;

    public int getIdtkCuadri() {
        return idtkCuadri;
    }

    public void setIdtkCuadri(int idtkCuadri) {
        this.idtkCuadri = idtkCuadri;
    }

    public int getIdCuadrilla() {
        return idCuadrilla;
    }

    public void setIdCuadrilla(int idCuadrilla) {
        this.idCuadrilla = idCuadrilla;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getZona() {
        return zona;
    }

    public void setZona(int zona) {
        this.zona = zona;
    }

    public String getLiderCua() {
        return liderCua;
    }

    public void setLiderCua(String liderCua) {
        this.liderCua = liderCua;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Override
    public String toString() {
        return "TicketCuadrillaDTO [idtkCuadri=" + idtkCuadri + ", idCuadrilla=" + idCuadrilla + ", idProveedor="
                + idProveedor + ", ticket=" + ticket + ", zona=" + zona + ", liderCua=" + liderCua + ", status="
                + status + "]";
    }

}
