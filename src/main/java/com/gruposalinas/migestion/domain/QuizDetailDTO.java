package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;

public class QuizDetailDTO {

    @SerializedName("idQuiz")
    private String idQuiz;
    @SerializedName("idPreguntaQuiz")
    private String idPreguntaQuiz;
    @SerializedName("idTipoEncuesta")
    private String idTipoEncuesta;
    @SerializedName("pregunta")
    private String pregunta;
    @SerializedName("respuestas")
    private String respuestas;
    @SerializedName("respuestaCorrecta")
    private String respuestaCorrecta;
    @SerializedName("scorePregunta")
    private String scorePregunta;

    public String getIdQuiz() {
        return idQuiz;
    }

    public void setIdQuiz(String idQuiz) {
        this.idQuiz = idQuiz;
    }

    public String getIdPreguntaQuiz() {
        return idPreguntaQuiz;
    }

    public void setIdPreguntaQuiz(String idPreguntaQuiz) {
        this.idPreguntaQuiz = idPreguntaQuiz;
    }

    public String getIdTipoEncuesta() {
        return idTipoEncuesta;
    }

    public void setIdTipoEncuesta(String idTipoEncuesta) {
        this.idTipoEncuesta = idTipoEncuesta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public String getScorePregunta() {
        return scorePregunta;
    }

    public void setScorePregunta(String scorePregunta) {
        this.scorePregunta = scorePregunta;
    }
}
