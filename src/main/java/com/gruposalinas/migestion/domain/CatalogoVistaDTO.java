package com.gruposalinas.migestion.domain;

public class CatalogoVistaDTO {

    private int idCatVista;
    private String descripcion;
    private int idModPadre;

    public int getIdCatVista() {
        return this.idCatVista;
    }

    public void setIdCatVista(int idCatVista) {
        this.idCatVista = idCatVista;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdModPadre() {
        return this.idModPadre;
    }

    public void setIdModPadre(int idModPadre) {
        this.idModPadre = idModPadre;
    }

}
