package com.gruposalinas.migestion.domain;

public class ExternoDTO {

    private int numEmpleado;
    private String nombre;
    private String aPaterno;
    private String aMaterno;
    private String direccion;
    private int numCasa;
    private int numCelular;
    private String RFCEmpleado;
    private String cencosNum;
    private int puesto;
    private String fechaIngreso;
    private String fechaBaja;
    private String funcion;
    private int usuarioID;
    private int empSituacion;
    private int honorarios;
    private String email;
    private int numEmergencia;
    private String perEmergencia;
    private int numExt;
    private String sueldo;
    private String horarioIn;
    private String horarioFin;
    private int semestre;
    private String actividades;
    private String liberacion;
    private String elaboracion;
    private int llave;
    private int red;
    private int lotus;
    private int credencial;
    private int folioDataSec;
    private String pais;
    private String nombreCompleto;
    private String asistencia;
    private String curp;

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNumCasa() {
        return numCasa;
    }

    public void setNumCasa(int numCasa) {
        this.numCasa = numCasa;
    }

    public int getNumCelular() {
        return numCelular;
    }

    public void setNumCelular(int numCelular) {
        this.numCelular = numCelular;
    }

    public String getRFCEmpleado() {
        return RFCEmpleado;
    }

    public void setRFCEmpleado(String rFCEmpleado) {
        RFCEmpleado = rFCEmpleado;
    }

    public String getCencosNum() {
        return cencosNum;
    }

    public void setCencosNum(String cencosNum) {
        this.cencosNum = cencosNum;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public int getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(int usuarioID) {
        this.usuarioID = usuarioID;
    }

    public int getEmpSituacion() {
        return empSituacion;
    }

    public void setEmpSituacion(int empSituacion) {
        this.empSituacion = empSituacion;
    }

    public int getHonorarios() {
        return honorarios;
    }

    public void setHonorarios(int honorarios) {
        this.honorarios = honorarios;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNumEmergencia() {
        return numEmergencia;
    }

    public void setNumEmergencia(int numEmergencia) {
        this.numEmergencia = numEmergencia;
    }

    public String getPerEmergencia() {
        return perEmergencia;
    }

    public void setPerEmergencia(String perEmergencia) {
        this.perEmergencia = perEmergencia;
    }

    public int getNumExt() {
        return numExt;
    }

    public void setNumExt(int numExt) {
        this.numExt = numExt;
    }

    public String getSueldo() {
        return sueldo;
    }

    public void setSueldo(String sueldo) {
        this.sueldo = sueldo;
    }

    public String getHorarioIn() {
        return horarioIn;
    }

    public void setHorarioIn(String horarioIn) {
        this.horarioIn = horarioIn;
    }

    public String getHorarioFin() {
        return horarioFin;
    }

    public void setHorarioFin(String horarioFin) {
        this.horarioFin = horarioFin;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public String getLiberacion() {
        return liberacion;
    }

    public void setLiberacion(String liberacion) {
        this.liberacion = liberacion;
    }

    public String getElaboracion() {
        return elaboracion;
    }

    public void setElaboracion(String elaboracion) {
        this.elaboracion = elaboracion;
    }

    public int getLlave() {
        return llave;
    }

    public void setLlave(int llave) {
        this.llave = llave;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getLotus() {
        return lotus;
    }

    public void setLotus(int lotus) {
        this.lotus = lotus;
    }

    public int getCredencial() {
        return credencial;
    }

    public void setCredencial(int credencial) {
        this.credencial = credencial;
    }

    public int getFolioDataSec() {
        return folioDataSec;
    }

    public void setFolioDataSec(int folioDataSec) {
        this.folioDataSec = folioDataSec;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

}
