package com.gruposalinas.migestion.domain;

public class GuiaDHLDTO {

    private int idGuia;
    private String ceco;
    private int idUsuario;
    private int status;
    private String periodo;
    private String nombreArch;
    private String ruta;
    private int idEvid;
    private String descr;

    public int getIdGuia() {
        return idGuia;
    }

    public void setIdGuia(int idGuia) {
        this.idGuia = idGuia;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getNombreArch() {
        return nombreArch;
    }

    public void setNombreArch(String nombreArch) {
        this.nombreArch = nombreArch;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public int getIdEvid() {
        return idEvid;
    }

    public void setIdEvid(int idEvid) {
        this.idEvid = idEvid;

    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Override
    public String toString() {
        return "GuiaDHLDTO [idGuia=" + idGuia + ", ceco=" + ceco + ", idUsuario=" + idUsuario + ", status=" + status
                + ", periodo=" + periodo + ", nombreArch=" + nombreArch + ", ruta=" + ruta + ", idEvid=" + idEvid
                + ", descr=" + descr + "]";
    }

}
