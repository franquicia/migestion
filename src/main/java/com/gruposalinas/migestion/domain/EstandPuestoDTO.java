package com.gruposalinas.migestion.domain;

public class EstandPuestoDTO {

    private int idPuesto;
    private int idUsuario;
    private String ceco;
    private String mueble1caj1;
    private String mueble1caj2;
    private String mueble1caj3;
    private String mueble2caj1;
    private String mueble2caj2;
    private String mueble2caj6;
    private String periodo;
    private int idMueble;
    private int bandera;//cajones mueble
    private int puesto;// tipo muebles (financieros o caja)

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getMueble1caj1() {
        return mueble1caj1;
    }

    public void setMueble1caj1(String mueble1caj1) {
        this.mueble1caj1 = mueble1caj1;
    }

    public String getMueble1caj2() {
        return mueble1caj2;
    }

    public void setMueble1caj2(String mueble1caj2) {
        this.mueble1caj2 = mueble1caj2;
    }

    public String getMueble1caj3() {
        return mueble1caj3;
    }

    public void setMueble1caj3(String mueble1caj3) {
        this.mueble1caj3 = mueble1caj3;
    }

    public String getMueble2caj1() {
        return mueble2caj1;
    }

    public void setMueble2caj1(String mueble2caj1) {
        this.mueble2caj1 = mueble2caj1;
    }

    public String getMueble2caj2() {
        return mueble2caj2;
    }

    public void setMueble2caj2(String mueble2caj2) {
        this.mueble2caj2 = mueble2caj2;
    }

    public String getMueble2caj6() {
        return mueble2caj6;
    }

    public void setMueble2caj6(String mueble2caj6) {
        this.mueble2caj6 = mueble2caj6;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getIdMueble() {
        return idMueble;
    }

    public void setIdMueble(int idMueble) {
        this.idMueble = idMueble;
    }

    public int getBandera() {
        return bandera;
    }

    public void setBandera(int bandera) {
        this.bandera = bandera;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "EstandPuestoDTO [idPuesto=" + idPuesto + ", idUsuario=" + idUsuario + ", ceco=" + ceco
                + ", mueble1caj1=" + mueble1caj1 + ", mueble1caj2=" + mueble1caj2 + ", mueble1caj3=" + mueble1caj3
                + ", mueble2caj1=" + mueble2caj1 + ", mueble2caj2=" + mueble2caj2 + ", mueble2caj6=" + mueble2caj6
                + ", periodo=" + periodo + ", idMueble=" + idMueble + ", bandera=" + bandera + ", puesto=" + puesto
                + "]";
    }

}
