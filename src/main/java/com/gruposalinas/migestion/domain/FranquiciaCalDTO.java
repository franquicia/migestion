package com.gruposalinas.migestion.domain;

public class FranquiciaCalDTO {

    int idDashboard;
    String nombreDashboard;
    int idAlcance;
    String nombreAlcance;
    int anio;
    String nivel;
    String nombreNivel;
    int idNombreNivel;
    int clave;
    int suma;
    int totalUbicaciones;
    int total;
    int nivelCeco;

    public int getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(int idDashboard) {
        this.idDashboard = idDashboard;
    }

    public String getNombreDashboard() {
        return nombreDashboard;
    }

    public void setNombreDashboard(String nombreDashboar) {
        this.nombreDashboard = nombreDashboar;
    }

    public int getIdAlcance() {
        return idAlcance;
    }

    public void setIdAlcance(int idAlcance) {
        this.idAlcance = idAlcance;
    }

    public String getNombreAlcance() {
        return nombreAlcance;
    }

    public void setNombreAlcance(String nombreAlcance) {
        this.nombreAlcance = nombreAlcance;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNombreNivel() {
        return nombreNivel;
    }

    public void setNombreNivel(String nombreNivel) {
        this.nombreNivel = nombreNivel;
    }

    public int getIdNombreNivel() {
        return idNombreNivel;
    }

    public void setIdNombreNivel(int idNombreNivel) {
        this.idNombreNivel = idNombreNivel;
    }

    public int getClave() {
        return this.clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public int getSuma() {
        return suma;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }

    public int getTotalUbicaciones() {
        return totalUbicaciones;
    }

    public void setTotalUbicaciones(int totalUbicaciones) {
        this.totalUbicaciones = totalUbicaciones;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getNivelCeco() {
        return nivelCeco;
    }

    public void setNivelCeco(int nivelCeco) {
        this.nivelCeco = nivelCeco;
    }

    @Override
    public String toString() {
        return "FranquiciaCal [idDashboard=" + idDashboard + ", nombreDashboard=" + nombreDashboard + ", idAlcance="
                + idAlcance + ", nombreAlcance=" + nombreAlcance + ", nivel=" + nivel + ", nombreNivel="
                + nombreNivel + ", idNombreNivel=" + idNombreNivel + ", suma=" + suma + ", totalUbicaciones="
                + totalUbicaciones + ", total=" + total + "]";
    }

}
