package com.gruposalinas.migestion.domain;

public class UsuarioInfoTokenDTO {

    private int idUsuario;
    private String nombre;
    private String ceco;
    private String nombrececo;
    private String token;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getNombrececo() {
        return nombrececo;
    }

    public void setNombrececo(String nombrececo) {
        this.nombrececo = nombrececo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
