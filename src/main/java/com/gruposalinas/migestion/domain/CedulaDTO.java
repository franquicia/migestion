package com.gruposalinas.migestion.domain;

public class CedulaDTO {

    private int idUsuario;
    private int idSuc;
    private String fecha;
    private String rutafoto;
    private String rutaFirmaAcepta;
    private String rutaFirmaJefe;
    private String tipoSangre;
    private String idSeguroSoc;
    private int alergia;
    private String descalergia;
    private int enfermedad;
    private String descEnfermedad;
    private int tratamiento;
    private String descTratamiento;
    private String telContacto;
    private String contactoEmerg;
    private String telSocio;
    private String periodo;
    private String nombre;
    private String descPuesto;
    private int bandera;

    public int getBandera() {
        return bandera;
    }

    public void setBandera(int bandera) {
        this.bandera = bandera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescPuesto() {
        return descPuesto;
    }

    public void setDescPuesto(String descPuesto) {
        this.descPuesto = descPuesto;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdSuc() {
        return idSuc;
    }

    public void setIdSuc(int idSuc) {
        this.idSuc = idSuc;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRutafoto() {
        return rutafoto;
    }

    public void setRutafoto(String rutafoto) {
        this.rutafoto = rutafoto;
    }

    public String getRutaFirmaAcepta() {
        return rutaFirmaAcepta;
    }

    public void setRutaFirmaAcepta(String rutaFirmaAcepta) {
        this.rutaFirmaAcepta = rutaFirmaAcepta;
    }

    public String getRutaFirmaJefe() {
        return rutaFirmaJefe;
    }

    public void setRutaFirmaJefe(String rutaFirmaJefe) {
        this.rutaFirmaJefe = rutaFirmaJefe;
    }

    public String getTipoSangre() {
        return tipoSangre;
    }

    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    public String getIdSeguroSoc() {
        return idSeguroSoc;
    }

    public void setIdSeguroSoc(String idSeguroSoc) {
        this.idSeguroSoc = idSeguroSoc;
    }

    public int getAlergia() {
        return alergia;
    }

    public void setAlergia(int alergia) {
        this.alergia = alergia;
    }

    public String getDescalergia() {
        return descalergia;
    }

    public void setDescalergia(String descalergia) {
        this.descalergia = descalergia;
    }

    public int getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(int enfermedad) {
        this.enfermedad = enfermedad;
    }

    public String getDescEnfermedad() {
        return descEnfermedad;
    }

    public void setDescEnfermedad(String descEnfermedad) {
        this.descEnfermedad = descEnfermedad;
    }

    public int getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(int tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getDescTratamiento() {
        return descTratamiento;
    }

    public void setDescTratamiento(String descTratamiento) {
        this.descTratamiento = descTratamiento;
    }

    public String getTelContacto() {
        return telContacto;
    }

    public void setTelContacto(String telContacto) {
        this.telContacto = telContacto;
    }

    public String getContactoEmerg() {
        return contactoEmerg;
    }

    public void setContactoEmerg(String contactoEmerg) {
        this.contactoEmerg = contactoEmerg;
    }

    public String getTelSocio() {
        return telSocio;
    }

    public void setTelSocio(String telSocio) {
        this.telSocio = telSocio;
    }

    @Override
    public String toString() {
        return "CedulaDTO [idUsuario=" + idUsuario + ", idSuc=" + idSuc + ", fecha=" + fecha + ", rutafoto=" + rutafoto
                + ", rutaFirmaAcepta=" + rutaFirmaAcepta + ", rutaFirmaJefe=" + rutaFirmaJefe + ", tipoSangre="
                + tipoSangre + ", idSeguroSoc=" + idSeguroSoc + ", alergia=" + alergia + ", descalergia=" + descalergia
                + ", enfermedad=" + enfermedad + ", descEnfermedad=" + descEnfermedad + ", tratamiento=" + tratamiento
                + ", descTratamiento=" + descTratamiento + ", telContacto=" + telContacto + ", contactoEmerg="
                + contactoEmerg + ", telSocio=" + telSocio + ", periodo=" + periodo + ", nombre=" + nombre
                + ", descPuesto=" + descPuesto + ", bandera=" + bandera + "]";
    }

}
