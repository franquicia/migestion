package com.gruposalinas.migestion.domain;

public class ProductividadDTO {

    private String nivel;
    private String fecha;
    private String tipo_geografia;
    private int posicion;
    private double contribucion;
    private int pos_contribucion;
    private double prestamos;
    private int pos_prestamos;
    private double captacion;
    private int pos_captacion;
    private double sipa;
    private int pos_sipa;
    private double seguros;
    private int pos_seguros;
    private double gente;
    private int pos_gente;
    private double afore;
    private int pos_afore;
    private double total;
    private int id_cc;
    private String cc;
    private String cc_padre;
    private int empleado;
    private String desde;
    private int Treg;

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo_geografia() {
        return tipo_geografia;
    }

    public void setTipo_geografia(String tipo_geografia) {
        this.tipo_geografia = tipo_geografia;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public double getContribucion() {
        return contribucion;
    }

    public void setContribucion(double contribucion) {
        this.contribucion = contribucion;
    }

    public int getPos_contribucion() {
        return pos_contribucion;
    }

    public void setPos_contribucion(int pos_contribucion) {
        this.pos_contribucion = pos_contribucion;
    }

    public double getPrestamos() {
        return prestamos;
    }

    public void setPrestamos(double prestamos) {
        this.prestamos = prestamos;
    }

    public int getPos_prestamos() {
        return pos_prestamos;
    }

    public void setPos_prestamos(int pos_prestamos) {
        this.pos_prestamos = pos_prestamos;
    }

    public double getCaptacion() {
        return captacion;
    }

    public void setCaptacion(double captacion) {
        this.captacion = captacion;
    }

    public int getPos_captacion() {
        return pos_captacion;
    }

    public void setPos_captacion(int pos_captacion) {
        this.pos_captacion = pos_captacion;
    }

    public double getSipa() {
        return sipa;
    }

    public void setSipa(double sipa) {
        this.sipa = sipa;
    }

    public int getPos_sipa() {
        return pos_sipa;
    }

    public void setPos_sipa(int pos_sipa) {
        this.pos_sipa = pos_sipa;
    }

    public double getSeguros() {
        return seguros;
    }

    public void setSeguros(double seguros) {
        this.seguros = seguros;
    }

    public int getPos_seguros() {
        return pos_seguros;
    }

    public void setPos_seguros(int pos_seguros) {
        this.pos_seguros = pos_seguros;
    }

    public double getGente() {
        return gente;
    }

    public void setGente(double gente) {
        this.gente = gente;
    }

    public int getPos_gente() {
        return pos_gente;
    }

    public void setPos_gente(int pos_gente) {
        this.pos_gente = pos_gente;
    }

    public double getAfore() {
        return afore;
    }

    public void setAfore(double afore) {
        this.afore = afore;
    }

    public int getPos_afore() {
        return pos_afore;
    }

    public void setPos_afore(int pos_afore) {
        this.pos_afore = pos_afore;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getId_cc() {
        return id_cc;
    }

    public void setId_cc(int id_cc) {
        this.id_cc = id_cc;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getCc_padre() {
        return cc_padre;
    }

    public void setCc_padre(String cc_padre) {
        this.cc_padre = cc_padre;
    }

    public int getEmpleado() {
        return empleado;
    }

    public void setEmpleado(int empleado) {
        this.empleado = empleado;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public int getTreg() {
        return Treg;
    }

    public void setTreg(int treg) {
        Treg = treg;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
    private String periodo;

}
