package com.gruposalinas.migestion.domain;

public class ExternoPuestoDTO {

    private int numEmpleado;
    private int puesto;

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "ExternoPuestoDTO [numEmpleado=" + numEmpleado + ", puesto=" + puesto + "]";
    }

}
