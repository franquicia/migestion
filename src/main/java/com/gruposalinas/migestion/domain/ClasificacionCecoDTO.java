package com.gruposalinas.migestion.domain;

public class ClasificacionCecoDTO {

    private String ceco;
    private String cecoPadre;
    private int clasificacion;
    private String tipoGeografia;
    private String nivel;

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getCecoPadre() {
        return cecoPadre;
    }

    public void setCecoPadre(String cecoPadre) {
        this.cecoPadre = cecoPadre;
    }

    public int getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(int clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getTipoGeografia() {
        return tipoGeografia;
    }

    public void setTipoGeografia(String tipoGeografia) {
        this.tipoGeografia = tipoGeografia;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

}
