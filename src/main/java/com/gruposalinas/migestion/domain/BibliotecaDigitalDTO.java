package com.gruposalinas.migestion.domain;

public class BibliotecaDigitalDTO {

    private int idCategoria;
    private String descripcionCategoria;
    private String rutaIconoCategoria;
    private String nombreIconoCategoria;
    private int idDetalleCategoria;
    private String descripcionDetalleCategoria;
    private String tipoCategoria;
    private int idArchivo;
    private String rutaArchivo;
    private String nombreArchivo;
    private int ordenCategoria;
    private int ordenItem;

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    public void setDescripcionCategoria(String descripcionCategoria) {
        this.descripcionCategoria = descripcionCategoria;
    }

    public String getRutaIconoCategoria() {
        return rutaIconoCategoria;
    }

    public void setRutaIconoCategoria(String rutaIconoCategoria) {
        this.rutaIconoCategoria = rutaIconoCategoria;
    }

    public String getNombreIconoCategoria() {
        return nombreIconoCategoria;
    }

    public void setNombreIconoCategoria(String nombreIconoCategoria) {
        this.nombreIconoCategoria = nombreIconoCategoria;
    }

    public int getIdDetalleCategoria() {
        return idDetalleCategoria;
    }

    public void setIdDetalleCategoria(int idDetalleCategoria) {
        this.idDetalleCategoria = idDetalleCategoria;
    }

    public String getDescripcionDetalleCategoria() {
        return descripcionDetalleCategoria;
    }

    public void setDescripcionDetalleCategoria(String descripcionDetalleCategoria) {
        this.descripcionDetalleCategoria = descripcionDetalleCategoria;
    }

    public String getTipoCategoria() {
        return tipoCategoria;
    }

    public void setTipoCategoria(String tipoCategoria) {
        this.tipoCategoria = tipoCategoria;
    }

    public int getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(int idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public int getOrdenCategoria() {
        return ordenCategoria;
    }

    public void setOrdenCategoria(int ordenCategoria) {
        this.ordenCategoria = ordenCategoria;
    }

    public int getOrdenItem() {
        return ordenItem;
    }

    public void setOrdenItem(int ordenItem) {
        this.ordenItem = ordenItem;
    }

}
