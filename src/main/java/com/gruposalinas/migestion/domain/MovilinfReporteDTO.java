package com.gruposalinas.migestion.domain;

public class MovilinfReporteDTO {

    private String mes;
    private String anio;
    private int idUsuario;
    private String nombreUsuario;
    private String idCeco;
    private String nombreCeco;
    private int numSesion;
    private String fechaUltimaSesion;

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public int getNumSesion() {
        return numSesion;
    }

    public void setNumSesion(int numSesion) {
        this.numSesion = numSesion;
    }

    public String getFechaUltimaSesion() {
        return fechaUltimaSesion;
    }

    public void setFechaUltimaSesion(String fechaUltimaSesion) {
        this.fechaUltimaSesion = fechaUltimaSesion;
    }

    @Override
    public String toString() {
        return "MovilinfReporteDTO [mes=" + mes + ", anio=" + anio + ", idUsuario=" + idUsuario + ", nombreUsuario="
                + nombreUsuario + ", idCeco=" + idCeco + ", nombreCeco=" + nombreCeco + ", numSesion=" + numSesion
                + ", fechaUltimaSesion=" + fechaUltimaSesion + "]";
    }

}
