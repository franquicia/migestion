package com.gruposalinas.migestion.domain;

public class DetallePlantillaRHDTO {

    private String nombres;
    private String apellidoPat;
    private String apellidoMat;
    private String funcionDesc;
    private String empleado;
    private String indicador;
    private String funcion;

    public DetallePlantillaRHDTO() {
        super();
    }

    public DetallePlantillaRHDTO(String nombres, String apellidoPat, String apellidoMat, String funcionDesc,
            String empleado, String indicador) {
        super();
        this.nombres = nombres;
        this.apellidoPat = apellidoPat;
        this.apellidoMat = apellidoMat;
        this.funcionDesc = funcionDesc;
        this.empleado = empleado;
        this.indicador = indicador;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPat() {
        return apellidoPat;
    }

    public void setApellidoPat(String apellidoPat) {
        this.apellidoPat = apellidoPat;
    }

    public String getApellidoMat() {
        return apellidoMat;
    }

    public void setApellidoMat(String apellidoMat) {
        this.apellidoMat = apellidoMat;
    }

    public String getFuncionDesc() {
        return funcionDesc;
    }

    public void setFuncionDesc(String funcionDesc) {
        this.funcionDesc = funcionDesc;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getIndicador() {
        return indicador;
    }

    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

}
