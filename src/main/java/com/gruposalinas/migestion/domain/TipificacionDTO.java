package com.gruposalinas.migestion.domain;

public class TipificacionDTO {

    private int idOpeProd;
    private String desc;
    private int padre;

    public int getIdOpeProd() {
        return idOpeProd;
    }

    public void setIdOpeProd(int idOpeProd) {
        this.idOpeProd = idOpeProd;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPadre() {
        return padre;
    }

    public void setPadre(int padre) {
        this.padre = padre;
    }

}
