package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class HorasPicoDTO {

    @SerializedName("IDSucursal")
    private Integer fiSucursal;

    @SerializedName("Sucursal")
    private String fcSucursal;

    @SerializedName("IDPais")
    private Integer fiPais;

    @SerializedName("IDCanal")
    private Integer fiCanal;

    @SerializedName("Canal")
    private String fcCanal;

    @SerializedName("Anio")
    private Integer fiAnio;

    @SerializedName("Semana")
    private Integer fiSemana;

    @SerializedName("HorasMeta")
    private Integer fiHorasMeta;

    @SerializedName("HorasCumplidas")
    private Integer fiHorasCump;

    @SerializedName("HorasPorCumplir")
    private BigDecimal fiHorasPorCump;

    @SerializedName("UsuarioModificacion")
    private transient String fcUsuarioMod;

    @SerializedName("FechaModificacion")
    private transient Timestamp fdFechaMod;

    public Integer getFiSucursal() {
        return fiSucursal;
    }

    public void setFiSucursal(Integer fiSucursal) {
        this.fiSucursal = fiSucursal;
    }

    public String getFcSucursal() {
        return fcSucursal;
    }

    public void setFcSucursal(String fcSucursal) {
        this.fcSucursal = fcSucursal;
    }

    public Integer getFiPais() {
        return fiPais;
    }

    public void setFiPais(Integer fiPais) {
        this.fiPais = fiPais;
    }

    public Integer getFiCanal() {
        return fiCanal;
    }

    public void setFiCanal(Integer fiCanal) {
        this.fiCanal = fiCanal;
    }

    public String getFcCanal() {
        return fcCanal;
    }

    public void setFcCanal(String fcCanal) {
        this.fcCanal = fcCanal;
    }

    public Integer getFiAnio() {
        return fiAnio;
    }

    public void setFiAnio(Integer fiAnio) {
        this.fiAnio = fiAnio;
    }

    public Integer getFiSemana() {
        return fiSemana;
    }

    public void setFiSemana(Integer fiSemana) {
        this.fiSemana = fiSemana;
    }

    public Integer getFiHorasMeta() {
        return fiHorasMeta;
    }

    public void setFiHorasMeta(Integer fiHorasMeta) {
        this.fiHorasMeta = fiHorasMeta;
    }

    public Integer getFiHorasCump() {
        return fiHorasCump;
    }

    public void setFiHorasCump(Integer fiHorasCump) {
        this.fiHorasCump = fiHorasCump;
    }

    public BigDecimal getFiHorasPorCump() {
        return fiHorasPorCump;
    }

    public void setFiHorasPorCump(BigDecimal fiHorasPorCump) {
        this.fiHorasPorCump = fiHorasPorCump;
    }

    public String getFcUsuarioMod() {
        return fcUsuarioMod;
    }

    public void setFcUsuarioMod(String fcUsuarioMod) {
        this.fcUsuarioMod = fcUsuarioMod;
    }

    public Timestamp getFdFechaMod() {
        return fdFechaMod;
    }

    public void setFdFechaMod(Timestamp fdFechaMod) {
        this.fdFechaMod = fdFechaMod;
    }

    @Override
    public String toString() {
        return "HorasPicoDTO{" + "fiSucursal=" + fiSucursal + ", fcSucursal=" + fcSucursal + ", fiPais=" + fiPais + ", fiCanal=" + fiCanal + ", fcCanal=" + fcCanal + ", fiAnio=" + fiAnio + ", fiSemana=" + fiSemana + ", fiHorasMeta=" + fiHorasMeta + ", fiHorasCump=" + fiHorasCump + ", fiHorasPorCump=" + fiHorasPorCump + ", fcUsuarioMod=" + fcUsuarioMod + ", fdFechaMod=" + fdFechaMod + '}';
    }

}
