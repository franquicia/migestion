package com.gruposalinas.migestion.domain;

public class FotoPlantillaDTO {

    private int idPlanti;
    private String ceco;
    private int usuario;
    private String ruta;
    private String periodo;
    private String Nombre;
    private String Puesto;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPuesto() {
        return Puesto;
    }

    public void setPuesto(String puesto) {
        Puesto = puesto;
    }

    public int getIdPlanti() {
        return idPlanti;
    }

    public void setIdPlanti(int idPlanti) {
        this.idPlanti = idPlanti;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "FotoPlantillaDTO [idPlanti=" + idPlanti + ", ceco=" + ceco + ", usuario=" + usuario + ", ruta=" + ruta
                + ", periodo=" + periodo + ", Nombre=" + Nombre + ", Puesto=" + Puesto + "]";
    }

}
