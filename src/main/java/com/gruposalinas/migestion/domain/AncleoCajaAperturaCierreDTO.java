package com.gruposalinas.migestion.domain;

public class AncleoCajaAperturaCierreDTO {

    private String idTerritorio;
    private String idZona;
    private String idRegion;
    private String idSucursal;
    private int totalSucursales;
    private int sucConArqueoApertura;
    private int sucSinArqueoApertura;
    private int sucConArqueoAperturaSinDif;
    private int sucConArqueoAperturaConDif;
    private int sucConArqueoCierre;
    private int sucSinArqueoCierre;
    private int sucConArqueoCierreSinDif;
    private int sucConArqueoCierreConDif;

    /*Se agrega Porcentaje y nombre Ceco*/
    private String porcentaje;
    private String nombreCeco;
    private String color;
    private String cecoGlobal;

    public String getCecoGlobal() {
        return cecoGlobal;
    }

    public void setCecoGlobal(String cecoGlobal) {
        this.cecoGlobal = cecoGlobal;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(String idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getIdZona() {
        return idZona;
    }

    public void setIdZona(String idZona) {
        this.idZona = idZona;
    }

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public int getTotalSucursales() {
        return totalSucursales;
    }

    public void setTotalSucursales(int totalSucursales) {
        this.totalSucursales = totalSucursales;
    }

    public int getSucConArqueoApertura() {
        return sucConArqueoApertura;
    }

    public void setSucConArqueoApertura(int sucConArqueoApertura) {
        this.sucConArqueoApertura = sucConArqueoApertura;
    }

    public int getSucSinArqueoApertura() {
        return sucSinArqueoApertura;
    }

    public void setSucSinArqueoApertura(int sucSinArqueoApertura) {
        this.sucSinArqueoApertura = sucSinArqueoApertura;
    }

    public int getSucConArqueoAperturaSinDif() {
        return sucConArqueoAperturaSinDif;
    }

    public void setSucConArqueoAperturaSinDif(int sucConArqueoAperturaSinDif) {
        this.sucConArqueoAperturaSinDif = sucConArqueoAperturaSinDif;
    }

    public int getSucConArqueoAperturaConDif() {
        return sucConArqueoAperturaConDif;
    }

    public void setSucConArqueoAperturaConDif(int sucConArqueoAperturaConDif) {
        this.sucConArqueoAperturaConDif = sucConArqueoAperturaConDif;
    }

    public int getSucConArqueoCierre() {
        return sucConArqueoCierre;
    }

    public void setSucConArqueoCierre(int sucConArqueoCierre) {
        this.sucConArqueoCierre = sucConArqueoCierre;
    }

    public int getSucSinArqueoCierre() {
        return sucSinArqueoCierre;
    }

    public void setSucSinArqueoCierre(int sucSinArqueoCierre) {
        this.sucSinArqueoCierre = sucSinArqueoCierre;
    }

    public int getSucConArqueoCierreSinDif() {
        return sucConArqueoCierreSinDif;
    }

    public void setSucConArqueoCierreSinDif(int sucConArqueoCierreSinDif) {
        this.sucConArqueoCierreSinDif = sucConArqueoCierreSinDif;
    }

    public int getSucConArqueoCierreConDif() {
        return sucConArqueoCierreConDif;
    }

    public void setSucConArqueoCierreConDif(int sucConArqueoCierreConDif) {
        this.sucConArqueoCierreConDif = sucConArqueoCierreConDif;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

}
