package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;

public class TicketLimpiezaDTO {

    @SerializedName("ID_INCIDENTE")
    private int idIncidencia;

    @SerializedName("NO_SUCURSAL")
    private int noSucursal;

    @SerializedName("SUCURSAL")
    private String nombreSucursal;

    @SerializedName("CENTRO_COSTOS")
    private String centroCostos;

    @SerializedName("TEL_SUCURSAL")
    private String telefonoSucursal;

    @SerializedName("INCIDENCIA")
    private String incidencia;

    @SerializedName("FALLA")
    private String falla;

    @SerializedName("TIPO_FALLA")
    private String tipoFalla;

    @SerializedName("TICKET_ANTERIOR")
    private String ticketAnterior;

    @SerializedName("NUMEMP_CLIENTE")
    private String idCliente;

    @SerializedName("NOMBRE_CLIENTE")
    private String nombreCliente;

    @SerializedName("TEL_CLIENTE")
    private String telefonoCliente;

    @SerializedName("PUESTO_CLIENTE")
    private String puestoCliente;

    @SerializedName("AUTORIZACION_CLIENTE")
    private String autorizacionCliente;

    @SerializedName("CALIFICACION")
    private String evaluacionCliente;

    @SerializedName("USR_ALTERNO")
    private String usuarioAlterno;

    @SerializedName("PUESTO_ALTERNO")
    private String puestoAlterno;

    @SerializedName("TEL_USR_ALTERNO")
    private String telefonoContactoAlterno;

    @SerializedName("NUMEMP_SUPERVISOR")
    private String idSupervisor;

    @SerializedName("SUPERVISOR")
    private String nombreSupervisor;

    @SerializedName("TEL_SUPERVISOR")
    private String telefonoSupervisor;

    @SerializedName("NUMEMP_COORDINADOR")
    private String idCoordinador;

    @SerializedName("COORDINADOR")
    private String nombreCoordinador;

    @SerializedName("TEL_COORDINADOR")
    private String telefonoCoordinador;

    @SerializedName("ID_PROVEEDOR")
    private String idProveedor;

    @SerializedName("RAZON_SOCIAL")
    private String razonSocial;

    @SerializedName("MENU")
    private String menu;

    @SerializedName("NOMBRE_CORTO")
    private String nombreCortoProveedor;

    @SerializedName("PROVEEDOR")
    private String proveedor;

    @SerializedName("NO_TICKET_PROVEEDOR")
    private String noTicketProveedor;

    @SerializedName("AUTORIZACION_PROVEEDOR")
    private String autorizacionProveedor;

    @SerializedName("TIPO_CIERRE")
    private String tipoCierreProvedor;

    @SerializedName("AUTORIZACION_PIPA")
    private String autorizacionPipa;

    @SerializedName("COSTO_PIPA")
    private BigDecimal costoPipa;

    @SerializedName("NOTAS")
    private String notas;

    @SerializedName("NOTIFIACION")
    private String notificacion;

    @SerializedName("MONTO_ESTIMADO")
    private BigDecimal montoEstimado;

    @SerializedName("PRIORIDAD")
    private String prioridad;

    @SerializedName("TIPO_ATENCION")
    private String tipoAtencion;

    @SerializedName("ORIGEN")
    private String origen;

    @SerializedName("ESTADO")
    private String estado;

    @SerializedName("MOTIVO_ESTADO")
    private String motivoEstado;

    @SerializedName("DECLINAR_PROVEEDOR")
    private String declinarProveedor;

    @SerializedName("FECHA_CREACION")
    private String fechaCreacion;

    @SerializedName("FECHA_MODIFICACION")
    private String fechaModificacion;

    @SerializedName("FECHA_INICIO_ATENCION")
    private String fechaInicioAtencion;

    @SerializedName("FECHA_ULTIMO_TICKET")
    private String fechaUltimoTicket;

    @SerializedName("FECHA_PROGRAMADA")
    private String fechaEstimada;

    @SerializedName("FECHA_CIERRE_ADMIN")
    private String fechaCierreAdministrativo;

    @SerializedName("FECHA_CIERRE_TECNICO")
    private String fechaCierreTecnico;

    @SerializedName("FECHA_CIERRE_TECNICO_2")
    private String fechaCierreTecnico2;

    @SerializedName("FECHA_CIERRE")
    private String fechaCierre;

    @SerializedName("FECHA_RECHAZO_CLIENTE")
    private String rechazoCliente;

    @SerializedName("FECHA_ATENCION")
    private String fechaAtencion;

    @SerializedName("FECHA_CANCELACION")
    private String fechaCancelacion;

    @SerializedName("FECHA_AUTORIZA_PROVEEDOR")
    private String fechaAutorizaProveedor;

    @SerializedName("FECHA_RECHAZO_PROVEEDOR")
    private String fechaRechazoProveedor;

    @SerializedName("ID_CUADRILLA")
    private int idCuadrilla;

    @SerializedName("LIDER")
    private String lider;

    @SerializedName("CORREO")
    private String correo;

    @SerializedName("SEGUNDO")
    private String segundoMando;

    @SerializedName("ZONA")
    private int idZona;

    @SerializedName("MENSAJE_PARA_PROVEEDOR")
    private String mensajeProveedor;

    @SerializedName("RESOLUCION")
    private String resolucion;

    @SerializedName("CLIENTE_AVISADO")
    private String clienteAvisado;

    public String getClienteAvisado() {
        return clienteAvisado;
    }

    public void setClienteAvisado(String clienteAvisado) {
        this.clienteAvisado = clienteAvisado;
    }

    public int getIdIncidencia() {
        return idIncidencia;
    }

    public void setIdIncidencia(int idIncidencia) {
        this.idIncidencia = idIncidencia;
    }

    public int getNoSucursal() {
        return noSucursal;
    }

    public void setNoSucursal(int noSucursal) {
        this.noSucursal = noSucursal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getTelefonoSucursal() {
        return telefonoSucursal;
    }

    public void setTelefonoSucursal(String telefonoSucursal) {
        this.telefonoSucursal = telefonoSucursal;
    }

    public String getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    public String getFalla() {
        return falla;
    }

    public void setFalla(String falla) {
        this.falla = falla;
    }

    public String getTipoFalla() {
        return tipoFalla;
    }

    public void setTipoFalla(String tipoFalla) {
        this.tipoFalla = tipoFalla;
    }

    public String getTicketAnterior() {
        return ticketAnterior;
    }

    public void setTicketAnterior(String ticketAnterior) {
        this.ticketAnterior = ticketAnterior;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public String getPuestoCliente() {
        return puestoCliente;
    }

    public void setPuestoCliente(String puestoCliente) {
        this.puestoCliente = puestoCliente;
    }

    public String getAutorizacionCliente() {
        return autorizacionCliente;
    }

    public void setAutorizacionCliente(String autorizacionCliente) {
        this.autorizacionCliente = autorizacionCliente;
    }

    public String getUsuarioAlterno() {
        return usuarioAlterno;
    }

    public void setUsuarioAlterno(String usuarioAlterno) {
        this.usuarioAlterno = usuarioAlterno;
    }

    public String getPuestoAlterno() {
        return puestoAlterno;
    }

    public void setPuestoAlterno(String puestoAlterno) {
        this.puestoAlterno = puestoAlterno;
    }

    public String getTelefonoContactoAlterno() {
        return telefonoContactoAlterno;
    }

    public void setTelefonoContactoAlterno(String telefonoContactoAlterno) {
        this.telefonoContactoAlterno = telefonoContactoAlterno;
    }

    public String getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(String idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public String getNombreSupervisor() {
        return nombreSupervisor;
    }

    public void setNombreSupervisor(String nombreSupervisor) {
        this.nombreSupervisor = nombreSupervisor;
    }

    public String getTelefonoSupervisor() {
        return telefonoSupervisor;
    }

    public void setTelefonoSupervisor(String telefonoSupervisor) {
        this.telefonoSupervisor = telefonoSupervisor;
    }

    public String getIdCoordinador() {
        return idCoordinador;
    }

    public void setIdCoordinador(String idCoordinador) {
        this.idCoordinador = idCoordinador;
    }

    public String getNombreCoordinador() {
        return nombreCoordinador;
    }

    public void setNombreCoordinador(String nombreCoordinador) {
        this.nombreCoordinador = nombreCoordinador;
    }

    public String getTelefonoCoordinador() {
        return telefonoCoordinador;
    }

    public void setTelefonoCoordinador(String telefonoCoordinador) {
        this.telefonoCoordinador = telefonoCoordinador;
    }

    public String getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(String idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getNombreCortoProveedor() {
        return nombreCortoProveedor;
    }

    public void setNombreCortoProveedor(String nombreCortoProveedor) {
        this.nombreCortoProveedor = nombreCortoProveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getNoTicketProveedor() {
        return noTicketProveedor;
    }

    public void setNoTicketProveedor(String noTicketProveedor) {
        this.noTicketProveedor = noTicketProveedor;
    }

    public String getAutorizacionProveedor() {
        return autorizacionProveedor;
    }

    public void setAutorizacionProveedor(String autorizacionProveedor) {
        this.autorizacionProveedor = autorizacionProveedor;
    }

    public String getTipoCierreProvedor() {
        return tipoCierreProvedor;
    }

    public void setTipoCierreProvedor(String tipoCierreProvedor) {
        this.tipoCierreProvedor = tipoCierreProvedor;
    }

    public String getAutorizacionPipa() {
        return autorizacionPipa;
    }

    public void setAutorizacionPipa(String autorizacionPipa) {
        this.autorizacionPipa = autorizacionPipa;
    }

    public BigDecimal getCostoPipa() {
        return costoPipa;
    }

    public void setCostoPipa(BigDecimal costoPipa) {
        this.costoPipa = costoPipa;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(String notificacion) {
        this.notificacion = notificacion;
    }

    public BigDecimal getMontoEstimado() {
        return montoEstimado;
    }

    public void setMontoEstimado(BigDecimal montoEstimado) {
        this.montoEstimado = montoEstimado;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getTipoAtencion() {
        return tipoAtencion;
    }

    public void setTipoAtencion(String tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getFechaInicioAtencion() {
        return fechaInicioAtencion;
    }

    public void setFechaInicioAtencion(String fechaInicioAtencion) {
        this.fechaInicioAtencion = fechaInicioAtencion;
    }

    public String getFechaUltimoTicket() {
        return fechaUltimoTicket;
    }

    public void setFechaUltimoTicket(String fechaUltimoTicket) {
        this.fechaUltimoTicket = fechaUltimoTicket;
    }

    public String getFechaEstimada() {
        return fechaEstimada;
    }

    public void setFechaEstimada(String fechaEstimada) {
        this.fechaEstimada = fechaEstimada;
    }

    public String getFechaCierreAdministrativo() {
        return fechaCierreAdministrativo;
    }

    public void setFechaCierreAdministrativo(String fechaCierreAdministrativo) {
        this.fechaCierreAdministrativo = fechaCierreAdministrativo;
    }

    public String getFechaCierreTecnico() {
        return fechaCierreTecnico;
    }

    public void setFechaCierreTecnico(String fechaCierreTecnico) {
        this.fechaCierreTecnico = fechaCierreTecnico;
    }

    public String getFechaCierreTecnico2() {
        return fechaCierreTecnico2;
    }

    public void setFechaCierreTecnico2(String fechaCierreTecnico2) {
        this.fechaCierreTecnico2 = fechaCierreTecnico2;
    }

    public String getRechazoCliente() {
        return rechazoCliente;
    }

    public void setRechazoCliente(String rechazoCliente) {
        this.rechazoCliente = rechazoCliente;
    }

    public String getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(String fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public String getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(String fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public String getFechaRechazoProveedor() {
        return fechaRechazoProveedor;
    }

    public void setFechaRechazoProveedor(String fechaRechazoProveedor) {
        this.fechaRechazoProveedor = fechaRechazoProveedor;
    }

    public int getIdCuadrilla() {
        return idCuadrilla;
    }

    public void setIdCuadrilla(int idCuadrilla) {
        this.idCuadrilla = idCuadrilla;
    }

    public String getLider() {
        return lider;
    }

    public void setLider(String lider) {
        this.lider = lider;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSegundoMando() {
        return segundoMando;
    }

    public void setSegundoMando(String segundoMando) {
        this.segundoMando = segundoMando;
    }

    public int getIdZona() {
        return idZona;
    }

    public void setIdZona(int idZona) {
        this.idZona = idZona;
    }

    public String getMensajeProveedor() {
        return mensajeProveedor;
    }

    public void setMensajeProveedor(String mensajeProveedor) {
        this.mensajeProveedor = mensajeProveedor;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public String getDeclinarProveedor() {
        return declinarProveedor;
    }

    public void setDeclinarProveedor(String declinarProveedor) {
        this.declinarProveedor = declinarProveedor;
    }

    public String getFechaAutorizaProveedor() {
        return fechaAutorizaProveedor;
    }

    public void setFechaAutorizaProveedor(String fechaAutorizaProveedor) {
        this.fechaAutorizaProveedor = fechaAutorizaProveedor;
    }

    public String getEvaluacionCliente() {
        return evaluacionCliente;
    }

    public void setEvaluacionCliente(String evaluacionCliente) {
        this.evaluacionCliente = evaluacionCliente;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

}
