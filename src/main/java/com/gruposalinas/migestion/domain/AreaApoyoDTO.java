package com.gruposalinas.migestion.domain;

public class AreaApoyoDTO {

    private int idArea;
    private String ceco;
    private int idUsuario;
    private String folio;
    private String areaInv;
    private int prioridad;
    private int status;
    private String ruta;
    private int idEvid;
    private String nombreArc;
    private String periodo;
    private String descr;

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getAreaInv() {
        return areaInv;
    }

    public void setAreaInv(String areaInv) {
        this.areaInv = areaInv;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public int getIdEvid() {
        return idEvid;
    }

    public void setIdEvid(int idEvid) {
        this.idEvid = idEvid;
    }

    public String getNombreArc() {
        return nombreArc;
    }

    public void setNombreArc(String nombreArc) {
        this.nombreArc = nombreArc;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Override
    public String toString() {
        return "AreaApoyoDTO [idArea=" + idArea + ", ceco=" + ceco + ", idUsuario=" + idUsuario + ", folio=" + folio
                + ", areaInv=" + areaInv + ", prioridad=" + prioridad + ", status=" + status + ", ruta=" + ruta
                + ", idEvid=" + idEvid + ", nombreArc=" + nombreArc + ", periodo=" + periodo + ", descr=" + descr + "]";
    }

}
