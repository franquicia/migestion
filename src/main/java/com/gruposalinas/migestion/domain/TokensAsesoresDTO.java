package com.gruposalinas.migestion.domain;

public class TokensAsesoresDTO {

    private String token;
    private String nombreUsuario;
    private String compromisoPerso;
    private String avancePerso;
    private String compromisoActi;
    private String avanceActi;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getCompromisoPerso() {
        return compromisoPerso;
    }

    public void setCompromisoPerso(String compromisoPerso) {
        this.compromisoPerso = compromisoPerso;
    }

    public String getAvancePerso() {
        return avancePerso;
    }

    public void setAvancePerso(String avancePerso) {
        this.avancePerso = avancePerso;
    }

    public String getCompromisoActi() {
        return compromisoActi;
    }

    public void setCompromisoActi(String compromisoActi) {
        this.compromisoActi = compromisoActi;
    }

    public String getAvanceActi() {
        return avanceActi;
    }

    public void setAvanceActi(String avanceActi) {
        this.avanceActi = avanceActi;
    }

}
