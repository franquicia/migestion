package com.gruposalinas.migestion.domain;

public class DetalleAncleoCajaAperturaCierreDTO {

    private String fechaArqueo;
    private int idPais;
    private String idTerritorio;
    private String idZona;
    private String idRegion;
    private int idCanal;
    private int idSucursal;
    private int tipoArqueo;
    private String descTipoArqueo;
    private int idDivisa;
    private String descDivisa;
    private double importeSistema;
    private double importeArqueado;
    private double diferenciaSaldos;
    private String empValida;
    private int puestoValida;
    private int empAutoriza;
    private int puestoAutoriza;

    public String getFechaArqueo() {
        return fechaArqueo;
    }

    public void setFechaArqueo(String fechaArqueo) {
        this.fechaArqueo = fechaArqueo;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(String idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getIdZona() {
        return idZona;
    }

    public void setIdZona(String idZona) {
        this.idZona = idZona;
    }

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public int getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(int idCanal) {
        this.idCanal = idCanal;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public int getTipoArqueo() {
        return tipoArqueo;
    }

    public void setTipoArqueo(int tipoArqueo) {
        this.tipoArqueo = tipoArqueo;
    }

    public String getDescTipoArqueo() {
        return descTipoArqueo;
    }

    public void setDescTipoArqueo(String descTipoArqueo) {
        this.descTipoArqueo = descTipoArqueo;
    }

    public int getIdDivisa() {
        return idDivisa;
    }

    public void setIdDivisa(int idDivisa) {
        this.idDivisa = idDivisa;
    }

    public String getDescDivisa() {
        return descDivisa;
    }

    public void setDescDivisa(String descDivisa) {
        this.descDivisa = descDivisa;
    }

    public double getImporteSistema() {
        return importeSistema;
    }

    public void setImporteSistema(double importeSistema) {
        this.importeSistema = importeSistema;
    }

    public double getImporteArqueado() {
        return importeArqueado;
    }

    public void setImporteArqueado(double importeArqueado) {
        this.importeArqueado = importeArqueado;
    }

    public double getDiferenciaSaldos() {
        return diferenciaSaldos;
    }

    public void setDiferenciaSaldos(double diferenciaSaldos) {
        this.diferenciaSaldos = diferenciaSaldos;
    }

    public String getEmpValida() {
        return empValida;
    }

    public void setEmpValida(String empValida) {
        this.empValida = empValida;
    }

    public int getPuestoValida() {
        return puestoValida;
    }

    public void setPuestoValida(int puestoValida) {
        this.puestoValida = puestoValida;
    }

    public int getEmpAutoriza() {
        return empAutoriza;
    }

    public void setEmpAutoriza(int i) {
        this.empAutoriza = i;
    }

    public int getPuestoAutoriza() {
        return puestoAutoriza;
    }

    public void setPuestoAutoriza(int puestoAutoriza) {
        this.puestoAutoriza = puestoAutoriza;
    }

}
