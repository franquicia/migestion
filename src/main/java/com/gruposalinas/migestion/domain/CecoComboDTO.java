package com.gruposalinas.migestion.domain;

public class CecoComboDTO {

    public String ceco;
    public String nombre;
    public int cerca;

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCerca() {
        return cerca;
    }

    public void setCerca(int cerca) {
        this.cerca = cerca;
    }

}
