package com.gruposalinas.migestion.domain;

public class InfoVistaDTO {

    private int idInfoVista;
    private int idCatVista;
    private int idUsuario;
    private int plataforma;
    private String fecha;

    public int getIdInfoVista() {
        return this.idInfoVista;
    }

    public void setIdInfoVista(int idInfoVista) {
        this.idInfoVista = idInfoVista;
    }

    public int getIdCatVista() {
        return this.idCatVista;
    }

    public void setIdCatVista(int idCatVista) {
        this.idCatVista = idCatVista;
    }

    public int getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getPlataforma() {
        return this.plataforma;
    }

    public void setPlataforma(int plataforma) {
        this.plataforma = plataforma;
    }

    public String getFecha() {
        return this.fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

}
