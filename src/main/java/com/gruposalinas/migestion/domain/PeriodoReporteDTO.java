package com.gruposalinas.migestion.domain;

public class PeriodoReporteDTO {

    private int id_per_rep;
    private int id_periodo;
    private int id_reporte;
    private String horario;

    public int getId_per_rep() {
        return id_per_rep;
    }

    public void setId_per_rep(int id_per_rep) {
        this.id_per_rep = id_per_rep;
    }

    public int getId_periodo() {
        return id_periodo;
    }

    public void setId_periodo(int id_periodo) {
        this.id_periodo = id_periodo;
    }

    public int getId_reporte() {
        return id_reporte;
    }

    public void setId_reporte(int id_reporte) {
        this.id_reporte = id_reporte;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

}
