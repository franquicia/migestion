package com.gruposalinas.migestion.domain;

public class LoginDTO extends CecoDTO {

    private int noEmpleado;
    private String NombreEmpelado;
    private int idperfil;
    private String descPuesto;
    private int idPuesto;

    public int getNoEmpleado() {
        return noEmpleado;
    }

    public void setNoEmpleado(int noEmpleado) {
        this.noEmpleado = noEmpleado;
    }

    public String getNombreEmpelado() {
        return NombreEmpelado;
    }

    public void setNombreEmpelado(String nombreEmpelado) {
        NombreEmpelado = nombreEmpelado;
    }

    public int getIdperfil() {
        return idperfil;
    }

    public void setIdperfil(int idperfil) {
        this.idperfil = idperfil;
    }

    public String getDescPuesto() {
        return descPuesto;
    }

    public void setDescPuesto(String descPuesto) {
        this.descPuesto = descPuesto;
    }

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

}
