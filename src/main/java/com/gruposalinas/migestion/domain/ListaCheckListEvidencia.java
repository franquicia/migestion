package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;

public class ListaCheckListEvidencia {

    @SerializedName("idEvidencia")
    private String idEvidencia;
    @SerializedName("muestra")
    private String muestra;
    @SerializedName("comentarioEvidencia")
    private String comentarioEvidencia;

    public String getIdEvidencia() {
        return idEvidencia;
    }

    public void setIdEvidencia(String idEvidencia) {
        this.idEvidencia = idEvidencia;
    }

    public String getMuestra() {
        return muestra;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }

    public String getComentarioEvidencia() {
        return comentarioEvidencia;
    }

    public void setComentarioEvidencia(String comentarioEvidencia) {
        this.comentarioEvidencia = comentarioEvidencia;
    }
}
