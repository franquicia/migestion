package com.gruposalinas.migestion.domain;

public class DatosFiltroRHDTO {

    private int idCeco;
    private String nomCeco;
    private String opcion;
    private int valorOpcion;
    private int valorTendencia;

    public DatosFiltroRHDTO(int idCeco, String nomCeco, String opcion, int valorOpcion, int valorTendencia) {
        this.idCeco = idCeco;
        this.nomCeco = nomCeco;
        this.opcion = opcion;
        this.valorOpcion = valorOpcion;
        this.valorTendencia = valorTendencia;
    }

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public String getNomCeco() {
        return nomCeco;
    }

    public void setNomCeco(String nomCeco) {
        this.nomCeco = nomCeco;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public int getValorOpcion() {
        return valorOpcion;
    }

    public void setValorOpcion(int valorOpcion) {
        this.valorOpcion = valorOpcion;
    }

    public int getValorTendencia() {
        return valorTendencia;
    }

    public void setValorTendencia(int valorTendencia) {
        this.valorTendencia = valorTendencia;
    }

    @Override
    public String toString() {
        return "DatosFiltroRHDTO [idCeco=" + idCeco + ", nomCeco=" + nomCeco + ", opcion=" + opcion + ", valorOpcion="
                + valorOpcion + ", valorTendencia=" + valorTendencia + "]";
    }

}
