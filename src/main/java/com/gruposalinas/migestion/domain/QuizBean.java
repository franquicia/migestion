package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;

public class QuizBean {

    @SerializedName("idQuiz")
    private String idQuiz;
    @SerializedName("nombreQuiz")
    private String nombreQuiz;
    @SerializedName("scoreMinimo")
    private String scoreMinimo;
    @SerializedName("pasoRegresa")
    private String pasoRegresa;
    @SerializedName("activo")
    private String activo;
    @SerializedName("idPaso")
    private String idPaso;
    private String idPasoTem;
    @SerializedName("numPreguntas")
    private String numPreguntas;

    public String getIdQuiz() {
        return idQuiz;
    }

    public void setIdQuiz(String idQuiz) {
        this.idQuiz = idQuiz;
    }

    public String getNombreQuiz() {
        return nombreQuiz;
    }

    public void setNombreQuiz(String nombreQuiz) {
        this.nombreQuiz = nombreQuiz;
    }

    public String getScoreMinimo() {
        return scoreMinimo;
    }

    public void setScoreMinimo(String scoreMinimo) {
        this.scoreMinimo = scoreMinimo;
    }

    public String getPasoRegresa() {
        return pasoRegresa;
    }

    public void setPasoRegresa(String pasoRegresa) {
        this.pasoRegresa = pasoRegresa;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getIdPaso() {
        return idPaso;
    }

    public void setIdPaso(String idPaso) {
        this.idPaso = idPaso;
    }

    public String getIdPasoTem() {
        return idPasoTem;
    }

    public void setIdPasoTem(String idPasoTem) {
        this.idPasoTem = idPasoTem;
    }

    public String getNumPreguntas() {
        return numPreguntas;
    }

    public void setNumPreguntas(String numPreguntas) {
        this.numPreguntas = numPreguntas;
    }
}
