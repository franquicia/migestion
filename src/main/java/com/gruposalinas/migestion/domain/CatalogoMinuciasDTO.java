package com.gruposalinas.migestion.domain;

public class CatalogoMinuciasDTO {

    private int idMinucia;
    private int idUsu;
    private String ceco;
    private String lugarEchos;
    private String fecha;
    private String direccion;
    private String vobo;
    private String destruye;
    private String testigo1;
    private String testigo2;
    private int idevi;
    private String nombrearc;
    private String observac;
    private String ruta;

    public int getIdMinucia() {
        return idMinucia;
    }

    public void setIdMinucia(int idMinucia) {
        this.idMinucia = idMinucia;
    }

    public int getIdUsu() {
        return idUsu;
    }

    public void setIdUsu(int idUsu) {
        this.idUsu = idUsu;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getLugarEchos() {
        return lugarEchos;
    }

    public void setLugarEchos(String lugarEchos) {
        this.lugarEchos = lugarEchos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getVobo() {
        return vobo;
    }

    public void setVobo(String vobo) {
        this.vobo = vobo;
    }

    public String getDestruye() {
        return destruye;
    }

    public void setDestruye(String destruye) {
        this.destruye = destruye;
    }

    public String getTestigo1() {
        return testigo1;
    }

    public void setTestigo1(String testigo1) {
        this.testigo1 = testigo1;
    }

    public String getTestigo2() {
        return testigo2;
    }

    public void setTestigo2(String testigo2) {
        this.testigo2 = testigo2;
    }

    public int getIdevi() {
        return idevi;
    }

    public void setIdevi(int idevi) {
        this.idevi = idevi;
    }

    public String getNombrearc() {
        return nombrearc;
    }

    public void setNombrearc(String nombrearc) {
        this.nombrearc = nombrearc;
    }

    public String getObservac() {
        return observac;
    }

    public void setObservac(String observac) {
        this.observac = observac;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    @Override
    public String toString() {
        return "CatalogoMinuciasDTO [idMinucia=" + idMinucia + ", idUsu=" + idUsu + ", ceco=" + ceco + ", lugarEchos="
                + lugarEchos + ", fecha=" + fecha + ", direccion=" + direccion + ", vobo=" + vobo + ", destruye="
                + destruye + ", testigo1=" + testigo1 + ", testigo2=" + testigo2 + ", idevi=" + idevi + ", nombrearc="
                + nombrearc + ", observac=" + observac + ", ruta=" + ruta + "]";
    }

}
