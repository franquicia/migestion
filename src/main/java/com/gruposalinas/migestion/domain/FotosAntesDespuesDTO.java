package com.gruposalinas.migestion.domain;

public class FotosAntesDespuesDTO {

    private Integer idEvento;
    private String idCeco;
    private String lugarAntes;
    private String lugarDespues;
    private String idEvidantes;
    private String idEviddespues;
    private String fechaAntes;
    private String fechaDespues;
    private String periodo;
    private String usuarioModif;
    private String fechaModif;

    public Integer getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Integer idEvento) {
        this.idEvento = idEvento;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getLugarAntes() {
        return lugarAntes;
    }

    public void setLugarAntes(String lugarAntes) {
        this.lugarAntes = lugarAntes;
    }

    public String getLugarDespues() {
        return lugarDespues;
    }

    public void setLugarDespues(String lugarDespues) {
        this.lugarDespues = lugarDespues;
    }

    public String getIdEvidantes() {
        return idEvidantes;
    }

    public void setIdEvidantes(String idEvidantes) {
        this.idEvidantes = idEvidantes;
    }

    public String getIdEviddespues() {
        return idEviddespues;
    }

    public void setIdEviddespues(String idEviddespues) {
        this.idEviddespues = idEviddespues;
    }

    public String getFechaAntes() {
        return fechaAntes;
    }

    public void setFechaAntes(String fechaAntes) {
        this.fechaAntes = fechaAntes;
    }

    public String getFechaDespues() {
        return fechaDespues;
    }

    public void setFechaDespues(String fechaDespues) {
        this.fechaDespues = fechaDespues;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getUsuarioModif() {
        return usuarioModif;
    }

    public void setUsuarioModif(String usuarioModif) {
        this.usuarioModif = usuarioModif;
    }

    public String getFechaModif() {
        return fechaModif;
    }

    public void setFechaModif(String fechaModif) {
        this.fechaModif = fechaModif;
    }
}
