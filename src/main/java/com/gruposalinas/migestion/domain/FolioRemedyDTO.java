package com.gruposalinas.migestion.domain;

public class FolioRemedyDTO {

    private int ceco;
    private int noSucursal;
    private String nomSucursal;
    private String telSucursal;
    private String correo;
    private int numEmpleado;
    private String nombre;
    private String puesto;
    private String comentario;
    private String puestoAlterno;
    private String telUsrAlterno;
    private String nomUsrAlterno;
    private String Servicio;
    private String Ubicacion;
    private String incidencia;
    private String nomAdjunto;
    private String Adjunto;

    public String getAdjunto() {
        return Adjunto;
    }

    public void setAdjunto(String adjunto) {
        Adjunto = adjunto;
    }

    public int getCeco() {
        return ceco;
    }

    public void setCeco(int ceco) {
        this.ceco = ceco;
    }

    public int getNoSucursal() {
        return noSucursal;
    }

    public void setNoSucursal(int noSucursal) {
        this.noSucursal = noSucursal;
    }

    public String getNomSucursal() {
        return nomSucursal;
    }

    public void setNomSucursal(String nomSucursal) {
        this.nomSucursal = nomSucursal;
    }

    public String getTelSucursal() {
        return telSucursal;
    }

    public void setTelSucursal(String telSucursal) {
        this.telSucursal = telSucursal;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getPuestoAlterno() {
        return puestoAlterno;
    }

    public void setPuestoAlterno(String puestoAlterno) {
        this.puestoAlterno = puestoAlterno;
    }

    public String getTelUsrAlterno() {
        return telUsrAlterno;
    }

    public void setTelUsrAlterno(String telUsrAlterno) {
        this.telUsrAlterno = telUsrAlterno;
    }

    public String getNomUsrAlterno() {
        return nomUsrAlterno;
    }

    public void setNomUsrAlterno(String nomUsrAlterno) {
        this.nomUsrAlterno = nomUsrAlterno;
    }

    public String getServicio() {
        return Servicio;
    }

    public void setServicio(String servicio) {
        Servicio = servicio;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public String getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    public String getNomAdjunto() {
        return nomAdjunto;
    }

    public void setNomAdjunto(String nomAdjunto) {
        this.nomAdjunto = nomAdjunto;
    }
}
