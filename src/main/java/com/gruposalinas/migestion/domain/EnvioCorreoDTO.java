package com.gruposalinas.migestion.domain;

public class EnvioCorreoDTO {

    private int idEnvio;
    private int idArchvio;
    private String email;
    private String sucursal;
    private String fechaSolicitada;

    public int getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(int idEnvio) {
        this.idEnvio = idEnvio;
    }

    public int getIdArchvio() {
        return idArchvio;
    }

    public void setIdArchvio(int idArchvio) {
        this.idArchvio = idArchvio;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getFechaSolicitada() {
        return fechaSolicitada;
    }

    public void setFechaSolicitada(String fechaSolicitada) {
        this.fechaSolicitada = fechaSolicitada;
    }

}
