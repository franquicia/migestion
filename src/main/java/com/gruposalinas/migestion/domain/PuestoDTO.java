package com.gruposalinas.migestion.domain;

public class PuestoDTO {

    private int idPuesto;
    private String descripcion;

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "PuestoDTO [idPuesto=" + idPuesto + ", descripcion=" + descripcion + "]";
    }

}
