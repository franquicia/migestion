package com.gruposalinas.migestion.domain;

public class FranquiciaLigasDTO {

    private int idUbicacion;
    private String nombre;
    private String claveUbicacion;
    private String rutaFinal;
    private int idDashboard;
    private int idAlcance;
    private int anio;
    private int ceco;
    private int mes;

    public int getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(int idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClaveUbicacion() {
        return claveUbicacion;
    }

    public void setClaveUbicacion(String claveUbicacion) {
        this.claveUbicacion = claveUbicacion;
    }

    public String getRutaFinal() {
        return rutaFinal;
    }

    public void setRutaFinal(String rutaFinal) {
        this.rutaFinal = rutaFinal;
    }

    public int getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(int idDashboard) {
        this.idDashboard = idDashboard;
    }

    public int getIdAlcance() {
        return idAlcance;
    }

    public void setIdAlcance(int idAlcance) {
        this.idAlcance = idAlcance;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getCeco() {
        return ceco;
    }

    public void setCeco(int ceco) {
        this.ceco = ceco;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    @Override
    public String toString() {
        return "FranquiciaLigasDTO{" + "idUbicacion=" + idUbicacion + ", nombre=" + nombre + ", claveUbicacion=" + claveUbicacion + ", rutaFinal=" + rutaFinal + ", idDashboard=" + idDashboard + ", idAlcance=" + idAlcance + ", anio=" + anio + ", ceco=" + ceco + ", mes=" + mes + '}';
    }
}
