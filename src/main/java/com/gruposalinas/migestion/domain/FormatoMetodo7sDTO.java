package com.gruposalinas.migestion.domain;

public class FormatoMetodo7sDTO {

    private int idtab;
    private int idusuario;
    private int idSucursal;
    private String nombre;
    private int preg1;
    private int preg2;
    private int preg3;
    private String periodo;
    private String puesto;
    private int bandera;

    public int getBandera() {
        return bandera;
    }

    public void setBandera(int bandera) {
        this.bandera = bandera;
    }

    public int getIdtab() {
        return idtab;
    }

    public void setIdtab(int idtab) {
        this.idtab = idtab;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public int getPreg1() {
        return preg1;
    }

    public void setPreg1(int preg1) {
        this.preg1 = preg1;
    }

    public int getPreg2() {
        return preg2;
    }

    public void setPreg2(int preg2) {
        this.preg2 = preg2;
    }

    public int getPreg3() {
        return preg3;
    }

    public void setPreg3(int preg3) {
        this.preg3 = preg3;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "FormatoMetodo7sDTO [idtab=" + idtab + ", idusuario=" + idusuario + ", idSucursal=" + idSucursal
                + ", nombre=" + nombre + ", preg1=" + preg1 + ", preg2=" + preg2 + ", preg3=" + preg3 + ", periodo="
                + periodo + ", puesto=" + puesto + ", bandera=" + bandera + "]";
    }

}
