package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CheckListEvidenciasComentariosBean {

    @SerializedName("idPregunta")
    private String idPregunta;
    @SerializedName("idChecklist")
    private String idChecklist;
    @SerializedName("titulo")
    private String titulo;
    @SerializedName("ordenPregunta")
    private String ordenPregunta;
    @SerializedName("tipoPregunta")
    private String tipoPregunta;
    @SerializedName("pregunta")
    private String pregunta;
    private String conComentario;
    private String conEvidencia;
    @SerializedName("respuestas")
    private List<ListaChecklistEvidenciasComentariosBean> respuestas;

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(String idChecklist) {
        this.idChecklist = idChecklist;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getOrdenPregunta() {
        return ordenPregunta;
    }

    public void setOrdenPregunta(String ordenPregunta) {
        this.ordenPregunta = ordenPregunta;
    }

    public String getTipoPregunta() {
        return tipoPregunta;
    }

    public void setTipoPregunta(String tipoPregunta) {
        this.tipoPregunta = tipoPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getConComentario() {
        return conComentario;
    }

    public void setConComentario(String conComentario) {
        this.conComentario = conComentario;
    }

    public String getConEvidencia() {
        return conEvidencia;
    }

    public void setConEvidencia(String conEvidencia) {
        this.conEvidencia = conEvidencia;
    }

    public List<ListaChecklistEvidenciasComentariosBean> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<ListaChecklistEvidenciasComentariosBean> respuestas) {
        this.respuestas = respuestas;
    }
}
