package com.gruposalinas.migestion.domain;

public class CuadrillaDTO {

    private int idCuadrilla;
    private int idProveedor;
    private int zona;
    private String liderCuad;
    private String correo;
    private String segundo;
    private String passw;

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public int getIdCuadrilla() {
        return idCuadrilla;
    }

    public void setIdCuadrilla(int idCuadrilla) {
        this.idCuadrilla = idCuadrilla;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getZona() {
        return zona;
    }

    public void setZona(int zona) {
        this.zona = zona;
    }

    public String getLiderCuad() {
        return liderCuad;
    }

    public void setLiderCuad(String liderCuad) {
        this.liderCuad = liderCuad;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSegundo() {
        return segundo;
    }

    public void setSegundo(String segundo) {
        this.segundo = segundo;
    }

    @Override
    public String toString() {
        return "CuadrillaDTO [idCuadrilla=" + idCuadrilla + ", idProveedor=" + idProveedor + ", zona=" + zona
                + ", liderCuad=" + liderCuad + ", correo=" + correo + ", segundo=" + segundo + ", passw=" + passw + "]";
    }

}
