package com.gruposalinas.migestion.domain;

public class ReporteMensualMovilDTO {

    private int idUsuario;
    private String nombreUsuario;
    private String idCeco;
    private String nombreCeco;
    private String idCecoSuperior;
    private String nombreCecoSuperior;
    private int totalSesiones;
    private String fechaUltimaSesion;
    private int idPuesto;
    private String desPuesto;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getIdCecoSuperior() {
        return idCecoSuperior;
    }

    public void setIdCecoSuperior(String idCecoSuperior) {
        this.idCecoSuperior = idCecoSuperior;
    }

    public String getNombreCecoSuperior() {
        return nombreCecoSuperior;
    }

    public void setNombreCecoSuperior(String nombreCecoSuperior) {
        this.nombreCecoSuperior = nombreCecoSuperior;
    }

    public int getTotalSesiones() {
        return totalSesiones;
    }

    public void setTotalSesiones(int totalSesiones) {
        this.totalSesiones = totalSesiones;
    }

    public String getFechaUltimaSesion() {
        return fechaUltimaSesion;
    }

    public void setFechaUltimaSesion(String fechaUltimaSesion) {
        this.fechaUltimaSesion = fechaUltimaSesion;
    }

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    public String getDesPuesto() {
        return desPuesto;
    }

    public void setDesPuesto(String desPuesto) {
        this.desPuesto = desPuesto;
    }

    @Override
    public String toString() {
        return "MovilinfReporteDTO [idUsuario=" + idUsuario + ", nombreUsuario=" + nombreUsuario + ", idCeco=" + idCeco
                + ", nombreCeco=" + nombreCeco + ", idCecoSuperior=" + idCecoSuperior + ", nombreCecoSuperior="
                + nombreCecoSuperior + ", totalSesiones=" + totalSesiones + ", fechaUltimaSesion=" + fechaUltimaSesion
                + "]";
    }

}
