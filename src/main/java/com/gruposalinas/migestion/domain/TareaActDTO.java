package com.gruposalinas.migestion.domain;

public class TareaActDTO {

    private int idPkTarea;
    private int idTarea;
    private String nombreTarea;
    private int estatus;
    private int estatusVac;
    private int idUsuario;
    private int idCeco;
    private String nombreCeco;
    private int conteo;
    private int nivel;
    private int idPuesto;
    private String desPuesto;
    private String nombreUsuario;
    private String tipoTarea;
    private int porcentaje;
    private int porcentajeUsua;
    private int commit;

    public int getIdPkTarea() {
        return idPkTarea;
    }

    public void setIdPkTarea(int idPkTarea) {
        this.idPkTarea = idPkTarea;
    }

    public int getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public String getNombreTarea() {
        return nombreTarea;
    }

    public void setNombreTarea(String nombreTarea) {
        this.nombreTarea = nombreTarea;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public int getEstatusVac() {
        return estatusVac;
    }

    public void setEstatusVac(int estatusVac) {
        this.estatusVac = estatusVac;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public int getConteo() {
        return conteo;
    }

    public void setConteo(int conteo) {
        this.conteo = conteo;
    }

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    public String getDesPuesto() {
        return desPuesto;
    }

    public void setDesPuesto(String desPuesto) {
        this.desPuesto = desPuesto;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getTipoTarea() {
        return tipoTarea;
    }

    public void setTipoTarea(String tipoTarea) {
        this.tipoTarea = tipoTarea;
    }

    public int getCommit() {
        return commit;
    }

    public void setCommit(int commit) {
        this.commit = commit;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }

    public int getPorcentajeUsua() {
        return porcentajeUsua;
    }

    public void setPorcentajeUsua(int porcentajeUsua) {
        this.porcentajeUsua = porcentajeUsua;
    }

}
