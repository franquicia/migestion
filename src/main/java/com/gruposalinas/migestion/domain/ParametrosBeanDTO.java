package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;

public class ParametrosBeanDTO {

    @SerializedName("ID_PARAMETRO")
    private int ID_PARAMETRO;
    @SerializedName("DESCRIPCION")
    private String DESCRIPCION;
    @SerializedName("VALOR")
    private int VALOR;

    public int getID_PARAMETRO() {
        return ID_PARAMETRO;
    }

    public void setID_PARAMETRO(int iD_PARAMETRO) {
        ID_PARAMETRO = iD_PARAMETRO;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String dESCRIPCION) {
        DESCRIPCION = dESCRIPCION;
    }

    public int getVALOR() {
        return VALOR;
    }

    public void setVALOR(int vALOR) {
        VALOR = vALOR;
    }
}
