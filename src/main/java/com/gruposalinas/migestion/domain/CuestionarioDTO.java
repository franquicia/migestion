package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CuestionarioDTO {

    @SerializedName("idCheckList")
    private String idCheckList;
    @SerializedName("activo")
    private String activo;
    @SerializedName("idPaso")
    private String idPaso;
    private String idPasoTem;
    @SerializedName("listaPreguntas")
    private List<PreguntasDTO> listaPreguntas;
    private List<Checklist> checkDetalle;

    public String getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(String idCheckList) {
        this.idCheckList = idCheckList;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getIdPaso() {
        return idPaso;
    }

    public void setIdPaso(String idPaso) {
        this.idPaso = idPaso;
    }

    public String getIdPasoTem() {
        return idPasoTem;
    }

    public void setIdPasoTem(String idPasoTem) {
        this.idPasoTem = idPasoTem;
    }

    public List<PreguntasDTO> getListaPreguntas() {
        return listaPreguntas;
    }

    public void setListaPreguntas(List<PreguntasDTO> listaPreguntas) {
        this.listaPreguntas = listaPreguntas;
    }

    public List<Checklist> getCheckDetalle() {
        return checkDetalle;
    }

    public void setCheckDetalle(List<Checklist> checkDetalle) {
        this.checkDetalle = checkDetalle;
    }

    public class Checklist {

        @SerializedName("idCheckList")
        private String idCheckList;
        @SerializedName("idPregunta")
        private String idPregunta;
        @SerializedName("tipoPregunta")
        private String tipoPregunta;
        @SerializedName("pregunta")
        private String pregunta;
        @SerializedName("posible_respuesta")
        private String posible_respuesta;
        @SerializedName("url_reference")
        private String url_reference;
        @SerializedName("activo")
        private int activo;
        private List<String> respList;
        private List<String> respCorrList;

        public String getIdCheckList() {
            return idCheckList;
        }

        public void setIdCheckList(String idCheckList) {
            this.idCheckList = idCheckList;
        }

        public String getIdPregunta() {
            return idPregunta;
        }

        public void setIdPregunta(String idPregunta) {
            this.idPregunta = idPregunta;
        }

        public String getTipoPregunta() {
            return tipoPregunta;
        }

        public void setTipoPregunta(String tipoPregunta) {
            this.tipoPregunta = tipoPregunta;
        }

        public String getPregunta() {
            return pregunta;
        }

        public void setPregunta(String pregunta) {
            this.pregunta = pregunta;
        }

        public String getPosible_respuesta() {
            return posible_respuesta;
        }

        public void setPosible_respuesta(String posible_respuesta) {
            this.posible_respuesta = posible_respuesta;
        }

        public String getUrl_reference() {
            return url_reference;
        }

        public void setUrl_reference(String url_reference) {
            this.url_reference = url_reference;
        }

        public int getActivo() {
            return activo;
        }

        public void setActivo(int activo) {
            this.activo = activo;
        }

        public List<String> getRespList() {
            return respList;
        }

        public void setRespList(List<String> respList) {
            this.respList = respList;
        }

        public List<String> getRespCorrList() {
            return respCorrList;
        }

        public void setRespCorrList(List<String> respCorrList) {
            this.respCorrList = respCorrList;
        }
    }
}
