package com.gruposalinas.migestion.domain;

public class AncleoCajaSeguimientoDTO {

    private int idPais;
    private String idTerritorio;
    private String idZona;
    private String idRegion;
    private String idSucursal;
    private int totalSucursales;
    private int totalSucSinArqueo;
    private int totalSucConArqueo;
    private int sucConUnArqueo;
    private int totalArqueosConUnArqueo;
    private int arqueosConUnArqueoSinDif;
    private int arqueosConUnArqueoConDif;
    private int sucConDosArqueos;
    private int totalArqueosConDosArqueos;
    private int arqueosConDosArqueosSinDif;
    private int arqueosConDosArqueosConDif;
    private int sucConMasDeDosArqueos;
    private int totalArqueosConMasDeDos;
    private int arqueosConMasDeDosSinDif;
    private int arqueosConMasDeDosConDif;

    /*Se agrega Porcentaje y nombre Ceco*/
    private String porcentaje;
    private String nombreCeco;
    private String color;
    private String cecoGlobal;

    public String getCecoGlobal() {
        return cecoGlobal;
    }

    public void setCecoGlobal(String cecoGlobal) {
        this.cecoGlobal = cecoGlobal;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(String idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getIdZona() {
        return idZona;
    }

    public void setIdZona(String idZona) {
        this.idZona = idZona;
    }

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public int getTotalSucursales() {
        return totalSucursales;
    }

    public void setTotalSucursales(int totalSucursales) {
        this.totalSucursales = totalSucursales;
    }

    public int getTotalSucSinArqueo() {
        return totalSucSinArqueo;
    }

    public void setTotalSucSinArqueo(int totalSucSinArqueo) {
        this.totalSucSinArqueo = totalSucSinArqueo;
    }

    public int getTotalSucConArqueo() {
        return totalSucConArqueo;
    }

    public void setTotalSucConArqueo(int totalSucConArqueo) {
        this.totalSucConArqueo = totalSucConArqueo;
    }

    public int getSucConUnArqueo() {
        return sucConUnArqueo;
    }

    public void setSucConUnArqueo(int sucConUnArqueo) {
        this.sucConUnArqueo = sucConUnArqueo;
    }

    public int getTotalArqueosConUnArqueo() {
        return totalArqueosConUnArqueo;
    }

    public void setTotalArqueosConUnArqueo(int totalArqueosConUnArqueo) {
        this.totalArqueosConUnArqueo = totalArqueosConUnArqueo;
    }

    public int getArqueosConUnArqueoSinDif() {
        return arqueosConUnArqueoSinDif;
    }

    public void setArqueosConUnArqueoSinDif(int arqueosConUnArqueoSinDif) {
        this.arqueosConUnArqueoSinDif = arqueosConUnArqueoSinDif;
    }

    public int getArqueosConUnArqueoConDif() {
        return arqueosConUnArqueoConDif;
    }

    public void setArqueosConUnArqueoConDif(int arqueosConUnArqueoConDif) {
        this.arqueosConUnArqueoConDif = arqueosConUnArqueoConDif;
    }

    public int getSucConDosArqueos() {
        return sucConDosArqueos;
    }

    public void setSucConDosArqueos(int sucConDosArqueos) {
        this.sucConDosArqueos = sucConDosArqueos;
    }

    public int getTotalArqueosConDosArqueos() {
        return totalArqueosConDosArqueos;
    }

    public void setTotalArqueosConDosArqueos(int totalArqueosConDosArqueos) {
        this.totalArqueosConDosArqueos = totalArqueosConDosArqueos;
    }

    public int getArqueosConDosArqueosSinDif() {
        return arqueosConDosArqueosSinDif;
    }

    public void setArqueosConDosArqueosSinDif(int arqueosConDosArqueosSinDif) {
        this.arqueosConDosArqueosSinDif = arqueosConDosArqueosSinDif;
    }

    public int getArqueosConDosArqueosConDif() {
        return arqueosConDosArqueosConDif;
    }

    public void setArqueosConDosArqueosConDif(int arqueosConDosArqueosConDif) {
        this.arqueosConDosArqueosConDif = arqueosConDosArqueosConDif;
    }

    public int getSucConMasDeDosArqueos() {
        return sucConMasDeDosArqueos;
    }

    public void setSucConMasDeDosArqueos(int sucConMasDeDosArqueos) {
        this.sucConMasDeDosArqueos = sucConMasDeDosArqueos;
    }

    public int getTotalArqueosConMasDeDos() {
        return totalArqueosConMasDeDos;
    }

    public void setTotalArqueosConMasDeDos(int totalArqueosConMasDeDos) {
        this.totalArqueosConMasDeDos = totalArqueosConMasDeDos;
    }

    public int getArqueosConMasDeDosSinDif() {
        return arqueosConMasDeDosSinDif;
    }

    public void setArqueosConMasDeDosSinDif(int arqueosConMasDeDosSinDif) {
        this.arqueosConMasDeDosSinDif = arqueosConMasDeDosSinDif;
    }

    public int getArqueosConMasDeDosConDif() {
        return arqueosConMasDeDosConDif;
    }

    public void setArqueosConMasDeDosConDif(int arqueosConMasDeDosConDif) {
        this.arqueosConMasDeDosConDif = arqueosConMasDeDosConDif;
    }

    /*Porcentaje , Nombre Ceco*/
    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }
}
