package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;

public class ListaCheckListComentario {

    @SerializedName("idComentario")
    private String idComentario;
    @SerializedName("comentarioPregunta")
    private String comentarioPregunta;

    public String getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(String idComentario) {
        this.idComentario = idComentario;
    }

    public String getComentarioPregunta() {
        return comentarioPregunta;
    }

    public void setComentarioPregunta(String comentarioPregunta) {
        this.comentarioPregunta = comentarioPregunta;
    }
}
