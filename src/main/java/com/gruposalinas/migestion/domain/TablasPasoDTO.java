package com.gruposalinas.migestion.domain;

public class TablasPasoDTO {

    /**
     * ***********CECO PASO*******************
     */
    private String fcccid;
    private String fientidadid;
    private String finum_economico;
    private String fcnombrecc;
    private String fcnombre_ent;
    private String fcccid_padre;
    private String fientdid_padre;
    private String fcnombrecc_pad;
    private String fctipocanal;
    private String fcnombtipcanal;
    private String fccanal;
    private String fistatusccid;
    private String fcstatuscc;
    private String fitipocc;
    private String fiestadoid;
    private String fcmunicipio;
    private String fctipooperacion;
    private String fctiposucursal;
    private String fcnombretiposuc;
    private String fccalle;
    private String firesponsableid;
    private String fcresponsable;
    private String fccp;
    private String fctelefonos;
    private String ficlasif_sieid;
    private String fcclasif_sie;
    private String figastoxnegid;
    private String fcgastoxnegocio;
    private String fdapertura;
    private String fdcierre;
    private String fipaisid;
    private String fcpais;
    private String fdcarga;

    /**
     * ***********SUCURSAL PASO*******************
     */
    private String fisucursal_id;
    private String fipais;
    private String ficanal;
    private String fisucursal;
    private String fcnombreccSuc;
    private String fclongitude;
    private String fclatitude;

    /**
     * ***********USUARIO PASO*******************
     */
    private String fiempleado;
    private String fcnombre;
    private String ficc;
    private String fifuncion;
    private String fipuesto;
    private String fcdescfun;
    private String fcdescpuesto;
    private String fcpaisUsuario;
    private String fifechabaja;
    private String fiempleadorep;
    private String ficcemprep;
    private String fidisponible;

    /**
     * ***********TAREAS PASO*******************
     */
    private String fiid_tarea;
    private String fcnom_tarea;
    private String fiestatus;
    private String fiest_vac;
    private String fiid_usuario;

    public String getFcccid() {
        return fcccid;
    }

    public void setFcccid(String fcccid) {
        this.fcccid = fcccid;
    }

    public String getFientidadid() {
        return fientidadid;
    }

    public void setFientidadid(String fientidadid) {
        this.fientidadid = fientidadid;
    }

    public String getFinum_economico() {
        return finum_economico;
    }

    public void setFinum_economico(String finum_economico) {
        this.finum_economico = finum_economico;
    }

    public String getFcnombrecc() {
        return fcnombrecc;
    }

    public void setFcnombrecc(String fcnombrecc) {
        this.fcnombrecc = fcnombrecc;
    }

    public String getFcnombre_ent() {
        return fcnombre_ent;
    }

    public void setFcnombre_ent(String fcnombre_ent) {
        this.fcnombre_ent = fcnombre_ent;
    }

    public String getFcccid_padre() {
        return fcccid_padre;
    }

    public void setFcccid_padre(String fcccid_padre) {
        this.fcccid_padre = fcccid_padre;
    }

    public String getFientdid_padre() {
        return fientdid_padre;
    }

    public void setFientdid_padre(String fientdid_padre) {
        this.fientdid_padre = fientdid_padre;
    }

    public String getFcnombrecc_pad() {
        return fcnombrecc_pad;
    }

    public void setFcnombrecc_pad(String fcnombrecc_pad) {
        this.fcnombrecc_pad = fcnombrecc_pad;
    }

    public String getFctipocanal() {
        return fctipocanal;
    }

    public void setFctipocanal(String fctipocanal) {
        this.fctipocanal = fctipocanal;
    }

    public String getFcnombtipcanal() {
        return fcnombtipcanal;
    }

    public void setFcnombtipcanal(String fcnombtipcanal) {
        this.fcnombtipcanal = fcnombtipcanal;
    }

    public String getFccanal() {
        return fccanal;
    }

    public void setFccanal(String fccanal) {
        this.fccanal = fccanal;
    }

    public String getFistatusccid() {
        return fistatusccid;
    }

    public void setFistatusccid(String fistatusccid) {
        this.fistatusccid = fistatusccid;
    }

    public String getFcstatuscc() {
        return fcstatuscc;
    }

    public void setFcstatuscc(String fcstatuscc) {
        this.fcstatuscc = fcstatuscc;
    }

    public String getFitipocc() {
        return fitipocc;
    }

    public void setFitipocc(String fitipocc) {
        this.fitipocc = fitipocc;
    }

    public String getFiestadoid() {
        return fiestadoid;
    }

    public void setFiestadoid(String fiestadoid) {
        this.fiestadoid = fiestadoid;
    }

    public String getFcmunicipio() {
        return fcmunicipio;
    }

    public void setFcmunicipio(String fcmunicipio) {
        this.fcmunicipio = fcmunicipio;
    }

    public String getFctipooperacion() {
        return fctipooperacion;
    }

    public void setFctipooperacion(String fctipooperacion) {
        this.fctipooperacion = fctipooperacion;
    }

    public String getFctiposucursal() {
        return fctiposucursal;
    }

    public void setFctiposucursal(String fctiposucursal) {
        this.fctiposucursal = fctiposucursal;
    }

    public String getFcnombretiposuc() {
        return fcnombretiposuc;
    }

    public void setFcnombretiposuc(String fcnombretiposuc) {
        this.fcnombretiposuc = fcnombretiposuc;
    }

    public String getFccalle() {
        return fccalle;
    }

    public void setFccalle(String fccalle) {
        this.fccalle = fccalle;
    }

    public String getFiresponsableid() {
        return firesponsableid;
    }

    public void setFiresponsableid(String firesponsableid) {
        this.firesponsableid = firesponsableid;
    }

    public String getFcresponsable() {
        return fcresponsable;
    }

    public void setFcresponsable(String fcresponsable) {
        this.fcresponsable = fcresponsable;
    }

    public String getFccp() {
        return fccp;
    }

    public void setFccp(String fccp) {
        this.fccp = fccp;
    }

    public String getFctelefonos() {
        return fctelefonos;
    }

    public void setFctelefonos(String fctelefonos) {
        this.fctelefonos = fctelefonos;
    }

    public String getFiclasif_sieid() {
        return ficlasif_sieid;
    }

    public void setFiclasif_sieid(String ficlasif_sieid) {
        this.ficlasif_sieid = ficlasif_sieid;
    }

    public String getFcclasif_sie() {
        return fcclasif_sie;
    }

    public void setFcclasif_sie(String fcclasif_sie) {
        this.fcclasif_sie = fcclasif_sie;
    }

    public String getFigastoxnegid() {
        return figastoxnegid;
    }

    public void setFigastoxnegid(String figastoxnegid) {
        this.figastoxnegid = figastoxnegid;
    }

    public String getFcgastoxnegocio() {
        return fcgastoxnegocio;
    }

    public void setFcgastoxnegocio(String fcgastoxnegocio) {
        this.fcgastoxnegocio = fcgastoxnegocio;
    }

    public String getFdapertura() {
        return fdapertura;
    }

    public void setFdapertura(String fdapertura) {
        this.fdapertura = fdapertura;
    }

    public String getFdcierre() {
        return fdcierre;
    }

    public void setFdcierre(String fdcierre) {
        this.fdcierre = fdcierre;
    }

    public String getFipaisid() {
        return fipaisid;
    }

    public void setFipaisid(String fipaisid) {
        this.fipaisid = fipaisid;
    }

    public String getFcpais() {
        return fcpais;
    }

    public void setFcpais(String fcpais) {
        this.fcpais = fcpais;
    }

    public String getFdcarga() {
        return fdcarga;
    }

    public void setFdcarga(String fdcarga) {
        this.fdcarga = fdcarga;
    }

    public String getFisucursal_id() {
        return fisucursal_id;
    }

    public void setFisucursal_id(String fisucursal_id) {
        this.fisucursal_id = fisucursal_id;
    }

    public String getFipais() {
        return fipais;
    }

    public void setFipais(String fipais) {
        this.fipais = fipais;
    }

    public String getFicanal() {
        return ficanal;
    }

    public void setFicanal(String ficanal) {
        this.ficanal = ficanal;
    }

    public String getFisucursal() {
        return fisucursal;
    }

    public void setFisucursal(String fisucursal) {
        this.fisucursal = fisucursal;
    }

    public String getFcnombreccSuc() {
        return fcnombreccSuc;
    }

    public void setFcnombreccSuc(String fcnombreccSuc) {
        this.fcnombreccSuc = fcnombreccSuc;
    }

    public String getFclongitude() {
        return fclongitude;
    }

    public void setFclongitude(String fclongitude) {
        this.fclongitude = fclongitude;
    }

    public String getFclatitude() {
        return fclatitude;
    }

    public void setFclatitude(String fclatitude) {
        this.fclatitude = fclatitude;
    }

    public String getFiempleado() {
        return fiempleado;
    }

    public void setFiempleado(String fiempleado) {
        this.fiempleado = fiempleado;
    }

    public String getFcnombre() {
        return fcnombre;
    }

    public void setFcnombre(String fcnombre) {
        this.fcnombre = fcnombre;
    }

    public String getFicc() {
        return ficc;
    }

    public void setFicc(String ficc) {
        this.ficc = ficc;
    }

    public String getFifuncion() {
        return fifuncion;
    }

    public void setFifuncion(String fifuncion) {
        this.fifuncion = fifuncion;
    }

    public String getFipuesto() {
        return fipuesto;
    }

    public void setFipuesto(String fipuesto) {
        this.fipuesto = fipuesto;
    }

    public String getFcdescfun() {
        return fcdescfun;
    }

    public void setFcdescfun(String fcdescfun) {
        this.fcdescfun = fcdescfun;
    }

    public String getFcdescpuesto() {
        return fcdescpuesto;
    }

    public void setFcdescpuesto(String fcdescpuesto) {
        this.fcdescpuesto = fcdescpuesto;
    }

    public String getFcpaisUsuario() {
        return fcpaisUsuario;
    }

    public void setFcpaisUsuario(String fcpaisUsuario) {
        this.fcpaisUsuario = fcpaisUsuario;
    }

    public String getFifechabaja() {
        return fifechabaja;
    }

    public void setFifechabaja(String fifechabaja) {
        this.fifechabaja = fifechabaja;
    }

    public String getFiempleadorep() {
        return fiempleadorep;
    }

    public void setFiempleadorep(String fiempleadorep) {
        this.fiempleadorep = fiempleadorep;
    }

    public String getFiccemprep() {
        return ficcemprep;
    }

    public void setFiccemprep(String ficcemprep) {
        this.ficcemprep = ficcemprep;
    }

    public String getFidisponible() {
        return fidisponible;
    }

    public void setFidisponible(String fidisponible) {
        this.fidisponible = fidisponible;
    }

    public String getFiid_tarea() {
        return fiid_tarea;
    }

    public void setFiid_tarea(String fiid_tarea) {
        this.fiid_tarea = fiid_tarea;
    }

    public String getFcnom_tarea() {
        return fcnom_tarea;
    }

    public void setFcnom_tarea(String fcnom_tarea) {
        this.fcnom_tarea = fcnom_tarea;
    }

    public String getFiestatus() {
        return fiestatus;
    }

    public void setFiestatus(String fiestatus) {
        this.fiestatus = fiestatus;
    }

    public String getFiest_vac() {
        return fiest_vac;
    }

    public void setFiest_vac(String fiest_vac) {
        this.fiest_vac = fiest_vac;
    }

    public String getFiid_usuario() {
        return fiid_usuario;
    }

    public void setFiid_usuario(String fiid_usuario) {
        this.fiid_usuario = fiid_usuario;
    }

}
