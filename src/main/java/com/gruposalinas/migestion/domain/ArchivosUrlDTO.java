package com.gruposalinas.migestion.domain;

public class ArchivosUrlDTO {

    private int idArch;
    private String nombre;
    private String ruta;
    private String version;

    public int getIdArch() {
        return idArch;
    }

    public void setIdArch(int idArch) {
        this.idArch = idArch;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ArchivosUrlDTO [idArch=" + idArch + ", nombre=" + nombre + ", ruta=" + ruta + ", version=" + version
                + "]";
    }

}
