package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class JSONCuestionarioDTO {

    @SerializedName("Id")
    private String Id;
    @SerializedName("Id_paso")
    private String Id_paso;
    @SerializedName("Preguntas")
    private List<PreguntasDTO> Preguntas;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getId_paso() {
        return Id_paso;
    }

    public void setId_paso(String id_paso) {
        Id_paso = id_paso;
    }

    public List<PreguntasDTO> getPreguntas() {
        return Preguntas;
    }

    public void setPreguntas(List<PreguntasDTO> preguntas) {
        Preguntas = preguntas;
    }
}
