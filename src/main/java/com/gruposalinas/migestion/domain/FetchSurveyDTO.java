package com.gruposalinas.migestion.domain;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class FetchSurveyDTO {

    @SerializedName("QnrId")
    private String qnrId;
    @SerializedName("UnitId")
    private String unitId;
    @SerializedName("QnrType")
    private String qnrType;
    @SerializedName("Question")
    private List<FetchSurveyDTO> question;
    @SerializedName("QId")
    private String qId;
    @SerializedName("QType")
    private String qType;
    @SerializedName("QText")
    private String qText;
    @SerializedName("QResponse")
    private String qResponse;
    @SerializedName("QOption")
    private List<FetchSurveyDTO> qoption;
    @SerializedName("QOptSeq")
    private String qOptSeq;
    @SerializedName("QOptText")
    private String qOptText;

    public String getQnrId() {
        return qnrId;
    }

    public void setQnrId(String qnrId) {
        this.qnrId = qnrId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getQnrType() {
        return qnrType;
    }

    public void setQnrType(String qnrType) {
        this.qnrType = qnrType;
    }

    public List<FetchSurveyDTO> getQuestion() {
        return question;
    }

    public void setQuestion(List<FetchSurveyDTO> question) {
        this.question = question;
    }

    public String getqId() {
        return qId;
    }

    public void setqId(String qId) {
        this.qId = qId;
    }

    public String getqType() {
        return qType;
    }

    public void setqType(String qType) {
        this.qType = qType;
    }

    public String getqText() {
        return qText;
    }

    public void setqText(String qText) {
        this.qText = qText;
    }

    public String getqResponse() {
        return qResponse;
    }

    public void setqResponse(String qResponse) {
        this.qResponse = qResponse;
    }

    public List<FetchSurveyDTO> getQoption() {
        return qoption;
    }

    public void setQoption(List<FetchSurveyDTO> qoption) {
        this.qoption = qoption;
    }

    public String getqOptSeq() {
        return qOptSeq;
    }

    public void setqOptSeq(String qOptSeq) {
        this.qOptSeq = qOptSeq;
    }

    public String getqOptText() {
        return qOptText;
    }

    public void setqOptText(String qOptText) {
        this.qOptText = qOptText;
    }
}
