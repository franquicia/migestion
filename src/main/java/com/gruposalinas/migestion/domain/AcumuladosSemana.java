package com.gruposalinas.migestion.domain;

public class AcumuladosSemana {

    private int semana;
    private long acumuladoAlDia;
    private long totalCompromisoAlDia;
    private long totalRealSemana;
    private long totalCompromisoSemana;
    private long realAnioPasado;
    private boolean semanaActual;

    public int getSemana() {
        return semana;
    }

    public void setSemana(int semana) {
        this.semana = semana;
    }

    public long getAcumuladoAlDia() {
        return acumuladoAlDia;
    }

    public void setAcumuladoAlDia(long acumuladoAlDia) {
        this.acumuladoAlDia = acumuladoAlDia;
    }

    public long getTotalCompromisoAlDia() {
        return totalCompromisoAlDia;
    }

    public void setTotalCompromisoAlDia(long totalCompromisoAlDia) {
        this.totalCompromisoAlDia = totalCompromisoAlDia;
    }

    public long getTotalRealSemana() {
        return totalRealSemana;
    }

    public void setTotalRealSemana(long totalRealSemana) {
        this.totalRealSemana = totalRealSemana;
    }

    public long getTotalCompromisoSemana() {
        return totalCompromisoSemana;
    }

    public void setTotalCompromisoSemana(long totalCompromisoSemana) {
        this.totalCompromisoSemana = totalCompromisoSemana;
    }

    public long getRealAnioPasado() {
        return realAnioPasado;
    }

    public void setRealAnioPasado(long realAnioPasado) {
        this.realAnioPasado = realAnioPasado;
    }

    public boolean isSemanaActual() {
        return semanaActual;
    }

    public void setSemanaActual(boolean semanaActual) {
        this.semanaActual = semanaActual;
    }

}
