package com.gruposalinas.migestion.domain;

public class DetalleAsesoresRHDTO {

    private String empleado;
    private String nombre;
    private String apellidoPat;
    private String apellidoMat;
    private String indicador;
    private String cc;
    private String posicion;
    private String funcion;
    private String funcionDesc;
    private String pais;
    private String fechaNacimiento;

    public DetalleAsesoresRHDTO() {
        super();
    }

    public DetalleAsesoresRHDTO(String empleado, String nombre, String apellidoPat, String apellidoMat,
            String indicador, String cc, String posicion, String funcion, String funcionDesc, String pais,
            String fechaNacimiento) {
        super();
        this.empleado = empleado;
        this.nombre = nombre;
        this.apellidoPat = apellidoPat;
        this.apellidoMat = apellidoMat;
        this.indicador = indicador;
        this.cc = cc;
        this.posicion = posicion;
        this.funcion = funcion;
        this.funcionDesc = funcionDesc;
        this.pais = pais;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPat() {
        return apellidoPat;
    }

    public void setApellidoPat(String apellidoPat) {
        this.apellidoPat = apellidoPat;
    }

    public String getApellidoMat() {
        return apellidoMat;
    }

    public void setApellidoMat(String apellidoMat) {
        this.apellidoMat = apellidoMat;
    }

    public String getIndicador() {
        return indicador;
    }

    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public String getFuncionDesc() {
        return funcionDesc;
    }

    public void setFuncionDesc(String funcionDesc) {
        this.funcionDesc = funcionDesc;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

}
