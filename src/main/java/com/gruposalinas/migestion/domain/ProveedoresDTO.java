package com.gruposalinas.migestion.domain;

public class ProveedoresDTO {

    private String menu;
    private int idProveedor;
    private String nombreCorto;
    private String razonSocial;
    private String status;
    private int tipoProveedor;

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTipoProveedor() {
        return tipoProveedor;
    }

    public void setTipoProveedor(int tipoProveedor) {
        this.tipoProveedor = tipoProveedor;
    }

    public String toString() {
        return "ProveedoresDTO [menu=" + menu + ",idProveedor=" + idProveedor + ",nombreCorto=" + nombreCorto + ",razonSocial=" + razonSocial + ",tipoProveedor=" + tipoProveedor + ",status=" + status + "]";
    }

}
