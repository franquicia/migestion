package com.gruposalinas.migestion.domain;

public class CartaAsignacionAFDTO {

    private int idCarta;
    private int idUsuario;
    private String ceco;
    private String nombreArc;
    private String ruta;
    private String observ;
    private String perido;
    private int status;

    public int getIdCarta() {
        return idCarta;
    }

    public void setIdCarta(int idCarta) {
        this.idCarta = idCarta;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getNombreArc() {
        return nombreArc;
    }

    public void setNombreArc(String nombreArc) {
        this.nombreArc = nombreArc;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getObserv() {
        return observ;
    }

    public void setObserv(String observ) {
        this.observ = observ;
    }

    public String getPerido() {
        return perido;
    }

    public void setPerido(String perido) {
        this.perido = perido;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CartaAsignacionAFDTO [idCarta=" + idCarta + ", idUsuario=" + idUsuario + ", ceco=" + ceco
                + ", nombreArc=" + nombreArc + ", ruta=" + ruta + ", observ=" + observ + ", perido=" + perido
                + ", status=" + status + "]";
    }

}
