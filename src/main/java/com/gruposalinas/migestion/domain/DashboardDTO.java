package com.gruposalinas.migestion.domain;

public class DashboardDTO {

    private Integer idDashboard;
    private String nombreDashboard;
    private Integer idAlcance;
    private String nombreAlcance;
    private Integer anio;
    private Integer mes;

    public Integer getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(Integer idDashboard) {
        this.idDashboard = idDashboard;
    }

    public String getNombreDashboard() {
        return nombreDashboard;
    }

    public void setNombreDashboard(String nombreDashboard) {
        this.nombreDashboard = nombreDashboard;
    }

    public Integer getIdAlcance() {
        return idAlcance;
    }

    public void setIdAlcance(Integer idAlcance) {
        this.idAlcance = idAlcance;
    }

    public String getNombreAlcance() {
        return nombreAlcance;
    }

    public void setNombreAlcance(String nombreAlcance) {
        this.nombreAlcance = nombreAlcance;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    @Override
    public String toString() {
        return "DashboardDTO{" + "idDashboard=" + idDashboard + ", nombreDashboard=" + nombreDashboard + ", idAlcance=" + idAlcance + ", nombreAlcance=" + nombreAlcance + ", anio=" + anio + ", mes=" + mes + '}';
    }

}
